package com.sigma.samp.imp.ziggo.jmf_bean.CiscoBac40DataBean;

//=======================================================================
//FILE INFO
//$Id: CiscoBac40DataBean.java,v 1.6 2015/08/31 09:19:42 ricardol Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.lang.Integer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.stm.selection.SelectionPrcsException;
import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.vframe.util.ResUtil;
import com.sigma.samp.vframe.submodel.SubModel;

import java.sql.*;

import static com.sigma.samp.imp.ziggo.jmf_bean.CiscoBac40DataBean.CiscoBac40DataBeanConstants.*;

public class CiscoBac40DataBean implements SessionBean {

    private static final long serialVersionUID = 1L;
    private static Log        logger           = new Log(CiscoBac40DataBean.class.getName());
    private SessionContext    ctx;

    /**
     * This method will return the Z svc from an association with the svc A
     * 
     * @param sub subscriber
     * @param subSvc svc A
     * @param assocName
     * @return svc Z
     */

    private static SampSubSvcValue getZEndSvcFrmAEndSvc(final SampSubValue sub, final SampSubSvcValue subSvc,
            final String assocName) {
        logger.trace(3, "in getzend function");
        SampSubSvcValue zEndSvc = null;
        logger.trace(3, "SubSvc name = " + subSvc.getSvcNm() + " current is A End Service");
        final Collection col = subSvc.getEntityAssociationsByType(assocName);
        logger.trace(3, "collection = " + col);
        if (col != null) {
            logger.trace(3, "Size = " + col.size() + "  for association type = " + assocName);
            for (final Iterator itr = col.iterator(); itr.hasNext();) {
                logger.trace(3, "in side for in zend function");
                final SampSubEntityAssoc assoc = (SampSubEntityAssoc) itr.next();
                zEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc.getZEndEntityKey());
                return zEndSvc;
            }
        }
        return zEndSvc;
    }

    /***
     * This method will return the A svc from an association with the svc Z
     * 
     * @param sub
     * @param subSvc
     * @param assocName
     * @return svc A
     */

    private static SampSubSvcValue getAEndSvcFrmZEndSvc(final SampSubValue sub, final SampSubSvcValue subSvc,
            final String assocName) {
        logger.trace(3, "in getaend function");
        SampSubSvcValue aEndSvc = null;
        if (subSvc != null) {
            logger.trace(3, "Got the following association type: " + assocName);
            final Collection assoc_list = sub.queryAssociationRegistries(assocName, subSvc.getSubSvcKey());
            if (assoc_list != null) {
                for (final Iterator itr = assoc_list.iterator(); itr.hasNext();) {
                    final SampSubEntityAssoc assoc1 = (SampSubEntityAssoc) itr.next();
                    aEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc1.getAEndEntityKey());
                    logger.trace(3, "Got the following subsvc val: " + aEndSvc);
                    break;
                }
            } else {
                logger.trace(5, "Assoc does not exits");
            }
        }
        return aEndSvc;
    }

    /***
     * This method will return the Resi Device Control Service based on the Internet Access
     * 
     * @param sub
     * @param subSvc
     * @param assocName
     * @return svc ResiDeviceControl
     * @throws RemoteException
     */

    private static SampSubSvcValue getResiDeviceControlFromInternetAccess(final SampSubValue sub,
            final SampSubSvcValue subSvc, final String assocName) throws RemoteException {
        try {

            logger.trace(3, "in getResiDeviceControlFromInternetAccess function");

            SampSubSvcValue smp_data_port_svc = getZEndSvcFrmAEndSvc(sub, subSvc, assocName);

            String result = new String();

            if (!(smp_data_port_svc.getSvcNm().equalsIgnoreCase(SMP_DATA_PORT))
                    && !(smp_data_port_svc.getSvcNm().equalsIgnoreCase(EMTA_DATA_PORT))) {
                result = "Association InternetHasAccess not found!";
                throw new RemoteException(result);
            }

            SampSubSvcValue smp_emta_composed_svc = smp_data_port_svc.getParentSubSvc();

            if (!(smp_emta_composed_svc.getSvcNm().equalsIgnoreCase(SMP_EMTA_COMPOSED))) {
                result = "SMP EMTA Composed Service not found!";
                throw new RemoteException(result);
            }

            Collection smp_emta_composed_childSvcCollection = smp_emta_composed_svc.getChildSubSvcLst();

            Iterator itr1 = smp_emta_composed_childSvcCollection.iterator();
            SampSubSvcValue smp_resi_device_control_svc = null;

            while (itr1.hasNext()) {
                smp_resi_device_control_svc = (SampSubSvcValue) itr1.next();

                if (smp_resi_device_control_svc.getSvcNm().equalsIgnoreCase(EMTA_DEVICE_CONTROL)
                        || smp_resi_device_control_svc.getSvcNm().equalsIgnoreCase(SMP_RESI_DEVICE_CONTROL)) {

                    break;
                }
            }

            logger.trace(3, "getResiDeviceControlFromInternetAccess result: " + smp_resi_device_control_svc);

            return smp_resi_device_control_svc;

        } catch (Exception e) {
            logger.log("Exception:" + e.getMessage());
            throw new RemoteException("Exception:", e);

        }

    }

    /***
     * This method will return the Internet Access based on the Resi Device Control Service
     * 
     * @param sub
     * @param subSvc
     * @param assocName
     * @return svc InternetAccess
     * @throws RemoteException
     */

    private static SampSubSvcValue getInternetAccessFromResiDeviceControl(final SampSubValue sub,
            final SampSubSvcValue subSvc, final String assocName) throws RemoteException {
        try {

            logger.trace(3, "in getInternetAccessFromResiDeviceControl function");

            SampSubSvcValue smp_resi_device_control_svc = subSvc;

            String result = new String();

            if (!(smp_resi_device_control_svc.getSvcNm().equalsIgnoreCase(EMTA_DEVICE_CONTROL))
                    && !(smp_resi_device_control_svc.getSvcNm().equalsIgnoreCase(SMP_RESI_DEVICE_CONTROL))) {
                result = "EMTA Device Control not found!";
                throw new RemoteException(result);
            }

            SampSubSvcValue smp_emta_composed_svc = smp_resi_device_control_svc.getParentSubSvc();

            if (!(smp_emta_composed_svc.getSvcNm().equalsIgnoreCase(SMP_EMTA_COMPOSED))) {
                result = "SMP EMTA Composed Service not found!";
                throw new RemoteException(result);
            }

            Collection smp_emta_composed_childSvcCollection = smp_emta_composed_svc.getChildSubSvcLst();

            Iterator itr1 = smp_emta_composed_childSvcCollection.iterator();
            SampSubSvcValue smp_data_port_svc = null;

            while (itr1.hasNext()) {
                smp_data_port_svc = (SampSubSvcValue) itr1.next();

                if (smp_data_port_svc.getSvcNm().equalsIgnoreCase(EMTA_DATA_PORT)
                        || smp_data_port_svc.getSvcNm().equalsIgnoreCase(SMP_DATA_PORT)) {
                    break;
                }
            }

            SampSubSvcValue smp_internet_access_svc = getAEndSvcFrmZEndSvc(sub, smp_data_port_svc, assocName);

            if (!(smp_internet_access_svc.getSvcNm().equalsIgnoreCase(SMP_INTERNET_ACCESS))
                    && !(smp_internet_access_svc.getSvcNm().equalsIgnoreCase(INTERNET_ACCESS))) {
                result = "Association InternetHasAccess not found!";
                throw new RemoteException(result);
            }

            logger.trace(3, "getInternetAccessFromResiDeviceControl result: " + smp_internet_access_svc);

            return smp_internet_access_svc;

        } catch (Exception e) {
            logger.log("Exception:" + e.getMessage());
            throw new RemoteException("Exception:", e);

        }

    }

    /***
     * This method will return the Wifi Access based on the Internet Access
     * 
     * @param sub
     * @param subSvc
     * @return svc Wifi Access
     * @throws RemoteException
     */

    private static SampSubSvcValue getWifiAccessFromInternet(final SampSubValue sub,
            final SampSubSvcValue subSvc) throws RemoteException {
        try {

            logger.trace(3, "in getWifiAccessFromInternet function");

            SampSubSvcValue smp_internet_access_svc = subSvc;

            String result = new String();

            if (!(smp_internet_access_svc.getSvcNm().equalsIgnoreCase(INTERNET_ACCESS))
                    && !(smp_internet_access_svc.getSvcNm().equalsIgnoreCase(SMP_INTERNET_ACCESS))) {
                result = "Internet Access not found!";
                throw new RemoteException(result);
            }

            SampSubSvcValue internet_service_composed_svc = smp_internet_access_svc.getParentSubSvc();

            if (!(internet_service_composed_svc.getSvcNm().equalsIgnoreCase(INTERNET_SERVICE_COMPOSED))) {
                result = "Internet Composed Service not found!";
                throw new RemoteException(result);
            }

            Collection internet_service_composed_childSvcCollection = internet_service_composed_svc
                    .getChildSubSvcLst();

            Iterator itr1 = internet_service_composed_childSvcCollection.iterator();
            SampSubSvcValue smp_wifi_access_svc = null;

            while (itr1.hasNext()) {
                smp_wifi_access_svc = (SampSubSvcValue) itr1.next();

                if (smp_wifi_access_svc.getSvcNm().equalsIgnoreCase(WIFI_ACCESS)
                        || smp_wifi_access_svc.getSvcNm().equalsIgnoreCase(SMP_WIFI_ACCESS)) {

                    break;
                }
            }

            logger.trace(3, "getWifiAccessFromInternet result: " + smp_wifi_access_svc);

            return smp_wifi_access_svc;

        } catch (Exception e) {
            logger.log("Exception:" + e.getMessage());
            throw new RemoteException("Exception:", e);

        }

    }

    /***
     * This method will return the Internet Access based on the Wifi Access
     * 
     * @param sub
     * @param subSvc
     * @return svc InternetAccess
     * @throws RemoteException
     */

    private static SampSubSvcValue getInternetAccessFromWifi(final SampSubValue sub,
            final SampSubSvcValue subSvc) throws RemoteException {
        try {

            logger.trace(3, "in getInternetAccessFromWifi function");

            SampSubSvcValue smp_wifi_access_svc = subSvc;

            String result = new String();

            if (!(smp_wifi_access_svc.getSvcNm().equalsIgnoreCase(WIFI_ACCESS))
                    && !(smp_wifi_access_svc.getSvcNm().equalsIgnoreCase(SMP_WIFI_ACCESS))) {
                result = "Wifi Access not found!";
                throw new RemoteException(result);
            }

            SampSubSvcValue internet_service_composed_svc = smp_wifi_access_svc.getParentSubSvc();

            if (!(internet_service_composed_svc.getSvcNm().equalsIgnoreCase(INTERNET_SERVICE_COMPOSED))) {
                result = "Internet Composed Service not found!";
                throw new RemoteException(result);
            }

            Collection internet_service_composed_childSvcCollection = internet_service_composed_svc
                    .getChildSubSvcLst();

            Iterator itr1 = internet_service_composed_childSvcCollection.iterator();

            SampSubSvcValue smp_internet_access_svc = null;

            while (itr1.hasNext()) {
                smp_internet_access_svc = (SampSubSvcValue) itr1.next();

                if (smp_internet_access_svc.getSvcNm().equalsIgnoreCase(INTERNET_ACCESS)
                        || smp_internet_access_svc.getSvcNm().equalsIgnoreCase(SMP_INTERNET_ACCESS)) {
                    break;
                }
            }

            logger.trace(3, "getInternetAccessFromWifi result: " + smp_internet_access_svc);

            return smp_internet_access_svc;

        } catch (Exception e) {
            logger.log("Exception:" + e.getMessage());
            throw new RemoteException("Exception:", e);

        }

    }

    /***
     * This method will return a string based on the Provisioning Status
     * 
     * @param provStatus
     * @return result
     */

    private static String checkProvisionStatus(String provStatus, String accessType) {

        logger.trace(3, "in checkProvisionStatus method -- Received AccessType: " + accessType);

        String result = new String();

        if (provStatus.equalsIgnoreCase(ACTIVE) || provStatus.equalsIgnoreCase(MSO_BLOCK)
                || provStatus.equalsIgnoreCase(COURTESY_BLOCK)) {

            if (accessType.equalsIgnoreCase(INTERNET_SUSPEND)) {
                result = "WIFI=" + NOWIFI;
            } else {
                result = "WIFI=";
            }

        } else {
            result = "";
        }

        logger.trace(3, "Result of checkProvisionStatus method is: " + result);
        return result;
    }

    /***
     * This method will return a string based on the Wifi Parameters Values
     * 
     * @param capableWifiAccess
     * @param desiredWifiAccess
     * @param capableWifiHotspot
     * @param desiredWifiHotspot
     * @param quarantineProfile
     * @param provisionStatus
     * @param accessType
     * @return result
     */

    private static String checkWifiParameters(String capableWifiAccess, String desiredWifiAccess,
            String capableWifiHotspot, String desiredWifiHotspot, String quarantineProfile,
            String provisionStatus, String accessType) {

        logger.trace(3, "in checkWifiParameters method -- Received AccessType: " + accessType);

        String result = new String();

        if (accessType.equalsIgnoreCase(DEVICE_CHANGE_RDU) || accessType.equalsIgnoreCase(DEVICE_UPDATE)
                || accessType.equalsIgnoreCase(WIFI_CHANGE_WIFI_SETTINGS)) {

            if (provisionStatus.equalsIgnoreCase(MSO_BLOCK)
                    || provisionStatus.equalsIgnoreCase(COURTESY_BLOCK)) {

                result = NOWIFI;

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(YES) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiHotspot.equalsIgnoreCase(NO) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PRIVATEWIFI;

                }

                else if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)) {
                    if (quarantineProfile == null || quarantineProfile.equalsIgnoreCase("null")) {
                        result = PUBLICWIFI;
                    } else {
                        result = PRIVATEWIFI;
                    }
                }

                else {

                    result = NOWIFI;
                }

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(NO) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PRIVATEWIFI;

                }

                else {

                    result = NOWIFI;

                }

            }

            else {

                result = NOWIFI;

            }

        }

        else if (accessType.equalsIgnoreCase(INTERNET_RESUME)
                || accessType.equalsIgnoreCase(INTERNET_ACTIVATION)
                || accessType.equalsIgnoreCase(INTERNET_REPROV)
                || accessType.equalsIgnoreCase(DEVICE_CHANGE_POIP)
                || accessType.equalsIgnoreCase(INTERNET_CHANGE_COS)) {

            if (desiredWifiHotspot.equalsIgnoreCase(YES) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PUBLICWIFI;

                }

                else if (capableWifiHotspot.equalsIgnoreCase(NO) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PRIVATEWIFI;

                }

                else {

                    result = NOWIFI;

                }

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(NO) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PRIVATEWIFI;

                }

                else {

                    result = NOWIFI;

                }

            }

            else {

                result = NOWIFI;

            }

        }

        else if (accessType.equalsIgnoreCase(DEVICE_UPDATE_MTA)
                || accessType.equalsIgnoreCase(DEVICE_DELETE_FROM_DEVICE)
                || accessType.equalsIgnoreCase(INTERNET_QUARANTINE)) {

            if (desiredWifiHotspot.equalsIgnoreCase(YES) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiHotspot.equalsIgnoreCase(NO) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PRIVATEWIFI;

                }

                else if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)) {
                    if (quarantineProfile == null || quarantineProfile.equalsIgnoreCase("null")) {
                        result = PUBLICWIFI;
                    } else {
                        result = PRIVATEWIFI;
                    }
                }

                else {

                    result = NOWIFI;
                }

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(NO) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = PRIVATEWIFI;

                }

                else {

                    result = NOWIFI;

                }

            }

            else {

                result = NOWIFI;

            }

        }

        else {

            logger.trace(5, "Unknown Access Type!");
            result = "";

        }

        logger.trace(3, "Result of checkWifiParameters method is: " + result);
        return result;
    }

    /***
     * This method will return a string based on the Provisioning Status and on the Quarantine
     * Profile
     * 
     * @param provStatus
     * @param quarantineProfile
     * @param accessType
     * @return result
     */

    private static String checkNAC(String provStatus, String quarantineProfile, String accessType) {

        logger.trace(3, "in checkNAC method -- Received AccessType: " + accessType);

        String result = new String();

        if (accessType.equalsIgnoreCase(DEVICE_CHANGE_RDU) || accessType.equalsIgnoreCase(DEVICE_UPDATE)
                || accessType.equalsIgnoreCase(WIFI_CHANGE_WIFI_SETTINGS)) {
            if (provStatus.equalsIgnoreCase(MSO_BLOCK) || provStatus.equalsIgnoreCase(COURTESY_BLOCK)) {
                result = "2";
            } else {
                result = "1";
            }
        }

        else if (accessType.equalsIgnoreCase(INTERNET_RESUME)
                || accessType.equalsIgnoreCase(INTERNET_ACTIVATION)
                || accessType.equalsIgnoreCase(INTERNET_REPROV)
                || accessType.equalsIgnoreCase(DEVICE_CHANGE_POIP)
                || accessType.equalsIgnoreCase(DEVICE_UPDATE_MTA)
                || accessType.equalsIgnoreCase(DEVICE_DELETE_FROM_DEVICE)) {

            result = "1";
        }

        else if (accessType.equalsIgnoreCase(INTERNET_CHANGE_COS)
                || accessType.equalsIgnoreCase(INTERNET_QUARANTINE)) {

            if (provStatus.equalsIgnoreCase(COURTESY_BLOCK)
                    && (quarantineProfile == null || quarantineProfile.equalsIgnoreCase("null"))) {
                result = "2";
            } else {
                result = "1";
            }

        }

        else if (accessType.equalsIgnoreCase(INTERNET_SUSPEND)) {

            result = "2";
        }

        else {

            logger.trace(5, "Unknown Access Type!");
            result = "";

        }

        logger.trace(3, "Result of checkNAC method is: " + result);
        return result;
    }

    /***
     * This method will return a string based on the Number of the Voice Lines
     * 
     * @param numOfVoiceLines
     * @return result
     */

    private static String checkIP(String numOfVoiceLines) {

        logger.trace(3, "in checkIP method");

        String result = new String();
        int num = Integer.parseInt(numOfVoiceLines);

        if (num == 0) {
            result = "/ccc/dhcp/primary=0.0.0.0";
        } else {
            result = " ";
        }

        logger.trace(3, "Result of checkIP method is: " + result);
        return result;
    }

    /***
     * This method returns a string based on the wifi parameters.
     * 
     * @param capableWifiAccess
     * @param desiredWifiAccess
     * @param capableWifiHotspot
     * @param desiredWifiHotspot
     * @param quarantineProfile
     * @param provisionStatus
     * @param accessType
     * @return result
     */

    private static String checkEnableRouter(String capableWifiAccess, String desiredWifiAccess,
            String capableWifiHotspot, String desiredWifiHotspot, String quarantineProfile,
            String provisionStatus, String accessType) {

        logger.trace(3, "in checkEnableRouter method -- Received AccessType: " + accessType);

        String result = new String();

        if (accessType.equalsIgnoreCase(DEVICE_CHANGE_RDU) || accessType.equalsIgnoreCase(DEVICE_UPDATE)
                || accessType.equalsIgnoreCase(WIFI_CHANGE_WIFI_SETTINGS)) {

            if (provisionStatus.equalsIgnoreCase(MSO_BLOCK)
                    || provisionStatus.equalsIgnoreCase(COURTESY_BLOCK)) {

                result = "1";

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(YES) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)
                        && (quarantineProfile == null || quarantineProfile.equalsIgnoreCase("null"))) {

                    result = "2";

                }

                else if (capableWifiHotspot.equalsIgnoreCase(NO) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)
                        && (quarantineProfile != null && !(quarantineProfile.equalsIgnoreCase("null")))) {

                    result = "2";

                }

                else {

                    result = "1";
                }

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(NO) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else {

                    result = "1";

                }

            }

            else {

                result = "1";

            }

        }

        else if (accessType.equalsIgnoreCase(INTERNET_RESUME)
                || accessType.equalsIgnoreCase(INTERNET_ACTIVATION)
                || accessType.equalsIgnoreCase(INTERNET_REPROV)
                || accessType.equalsIgnoreCase(DEVICE_CHANGE_POIP)
                || accessType.equalsIgnoreCase(INTERNET_CHANGE_COS)) {

            if (desiredWifiHotspot.equalsIgnoreCase(YES) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else if (capableWifiHotspot.equalsIgnoreCase(NO) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else {

                    result = "1";

                }

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(NO) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else {

                    result = "1";

                }

            }

            else {

                result = "1";

            }

        }

        else if (accessType.equalsIgnoreCase(DEVICE_UPDATE_MTA)
                || accessType.equalsIgnoreCase(DEVICE_DELETE_FROM_DEVICE)
                || accessType.equalsIgnoreCase(INTERNET_QUARANTINE)) {

            if (desiredWifiHotspot.equalsIgnoreCase(YES) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)
                        && (quarantineProfile == null || quarantineProfile.equalsIgnoreCase("null"))) {

                    result = "2";

                }

                else if (capableWifiHotspot.equalsIgnoreCase(NO) && capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else if (capableWifiHotspot.equalsIgnoreCase(YES) && capableWifiAccess.equalsIgnoreCase(YES)
                        && (quarantineProfile != null && !(quarantineProfile.equalsIgnoreCase("null")))) {

                    result = "2";

                }

                else {

                    result = "1";
                }

            }

            else if (desiredWifiHotspot.equalsIgnoreCase(NO) && desiredWifiAccess.equalsIgnoreCase(YES)) {

                if (capableWifiAccess.equalsIgnoreCase(YES)) {

                    result = "2";

                }

                else {

                    result = "1";

                }

            }

            else {

                result = "1";

            }

        }

        else if (accessType.equalsIgnoreCase(INTERNET_SUSPEND)) {

            result = "1";

        }

        else {

            logger.trace(5, "Unknown Access Type!");
            result = "";

        }

        logger.trace(3, "Result of checkEnableRouter method is: " + result);
        return result;
    }

    /***
     * This method will return a string based on the Number of the Voice Lines
     * 
     * @param numOfVoiceLines
     * @return result
     */

    private static String checkNumOfVoiceLines(String numOfVoiceLines) {

        logger.trace(3, "in checkNumOfVoiceLines method");

        String result = new String();
        int num = Integer.parseInt(numOfVoiceLines);

        if (num > 0) {
            result = "1";
        } else if (num == 0) {
            result = "2";
        } else {
            result = "";
        }

        logger.trace(3, "Result of checkNumOfVoiceLines method is: " + result);
        return result;
    }

    /**
     * This method will return flavor as per the combination of COS, model and manufacture
     * 
     * @param qos
     * @param model
     * @param manufacturer
     * @return flavor
     * @throws RemoteException
     */

    private String getFilter(final String qos, final String model, final String manufacturer)
            throws RemoteException {
        logger.trace(5, "In getFilter function");
        String filter = null;
        ResultSet rset = null;
        Statement stmt = null;
        final Connection conn = ResUtil.getSmartDbConnection();
        try {
            stmt = conn.createStatement();
            rset = stmt
                    .executeQuery("SELECT FILTER FROM CUST_BAC_MODEM_MFG bmm, CUST_ALLOWED_MODEM_PROFILE amp, CUST_DIN_QOS dq, CUST_DIN_QOS_FLAVOR dqf WHERE bmm.MFG_ID=amp.MFG_ID and amp.DIN_QOS_ID=dq.DIN_QOS_ID and dq.DIN_QOS_FLV_ID=dqf.DIN_QOS_FLV_ID and bmm.COS_MANUFACTURER="
                            + "'"
                            + manufacturer
                            + "'"
                            + " and bmm.COS_MODEL= "
                            + "'"
                            + model
                            + "'"
                            + " and dq.QOS_NAME= " + "'" + qos + "'");
            while (rset.next()) {
                filter = rset.getString(FILTER);
                logger.trace(5, "got the filter" + filter);
            }
        } catch (SQLException se) {
            logger.trace(6, "SQL Exception: " + se.getMessage());
            se.printStackTrace(System.out);
            logger.trace(6, "Stack Trace: " + se.toString());
            logger.trace(6, "An exception occured while conecting to the DB.");
        } finally {
            if (rset != null) {
                ResUtil.closeDBResource(rset);
            }
            if (stmt != null) {
                ResUtil.closeDBResource(stmt);
            }
            ResUtil.closeConnection(conn);
        }
        logger.trace(3, "Result of filter = " + filter);
        if (filter == null) {
            logger.trace(5, "filter not found");
            filter = "filter Not Found";
        } else {
            logger.trace(5, "Filter found.");
        }
        logger.trace(3, "The result of GetFilter is: " + filter);
        return filter;
    }

    /***
     * This method will return a string based on the POIP
     * 
     * @param poip
     * @return result
     */

    private static String checkPOIP(String poip) {

        logger.trace(3, "in checkPOIP method. Poip value is:" + poip);
        String result = new String();

        if (poip.equalsIgnoreCase("null")) {
            result = "";
        }

        else {
            result = "POIP=";
            result = result.concat(poip);
        }

        logger.trace(3, "Result of checkPOIP method is: " + result);
        return result;
    }

    /***
     * This method will return a string based on the HRZ Internet Properties
     * 
     * @param provStatus
     * @param accessType
     * @param qosFlavor
     * @param filter
     * @param evalContext
     * @return result
     */

    private static String getPropertiesForHrz(String provStatus, String accessType, String qosFlavor,
            String filter, ParmEvalContext evalContext) throws IllegalArgumentException, RemoteException,
            SelectionPrcsException {

        try {

            logger.trace(3, "in getPropertiesForHrz method -- Received AccessType: " + accessType);

            String result = new String();

            if (provStatus.equalsIgnoreCase(ACTIVE) || provStatus.equalsIgnoreCase(MSO_BLOCK)
                    || provStatus.equalsIgnoreCase(COURTESY_BLOCK)) {

                result = "NAC=";
            }

            else {
                result = "";
                logger.trace(3, "Result of getPropertiesForHrz method is: " + result);
                return result;
            }

            if (accessType.equalsIgnoreCase(HRZ_STB_SUSPEND)) {
                result = result.concat("2");
            }

            else {
                result = result.concat("1");
            }

            result = result.concat("|").concat("/ccc/dhcp/primary=0.0.0.0").concat("|").concat("QOS=");

            String resultQOS = new String();
            String resultFilter = new String();

            resultQOS = qosFlavor;
            resultFilter = filter;

            result = result.concat(resultQOS).concat("|").concat("Enable_MTA=2").concat("|")
                    .concat("Filter=").concat(resultFilter);

            logger.trace(3, "Result of getPropertiesForHrz method is: " + result);
            return result;

        }

        catch (Exception e) {
            logger.log("Exception:" + e.getMessage());
            throw new RemoteException("Exception:", e);

        }
    }

    /**
     * Eval method
     * 
     * @param expression
     * @param inputLst
     * @param evalContext
     * @return expression to be used in xml according to the context
     * @throws IllegalArgumentException
     * @throws RemoteException
     * @throws SelectionPrcsException
     */

    public String eval(final String expression, final Map inputLst, final ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException, SelectionPrcsException {

        String result = new String();

        try {

            logger.trace(3, "In Eval Method");
            logger.trace(3, "Version 1.1 CiscoBac40DataBean : " + expression);

            SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);

            SampSubSvcValue subSvc = (SampSubSvcValue) evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);

            final StringTokenizer strToken = new StringTokenizer(expression, ".", false);

            final ArrayList keyLst = new ArrayList();

            String accessType = new String();

            while (strToken.hasMoreTokens()) {
                keyLst.add(strToken.nextToken());
            }
            final int size = keyLst.size();

            logger.trace(3, " Size : " + size);
            if (size > 0) {
                accessType = (String) keyLst.get(0);
                logger.trace(3, "access type ==" + accessType);
            }

            if (accessType.equalsIgnoreCase(DEVICE_CHANGE_RDU) || accessType.equalsIgnoreCase(DEVICE_UPDATE)
                    || accessType.equalsIgnoreCase(WIFI_CHANGE_WIFI_SETTINGS)
                    || accessType.equalsIgnoreCase(INTERNET_RESUME)
                    || accessType.equalsIgnoreCase(INTERNET_ACTIVATION)
                    || accessType.equalsIgnoreCase(INTERNET_REPROV)
                    || accessType.equalsIgnoreCase(DEVICE_CHANGE_POIP)
                    || accessType.equalsIgnoreCase(DEVICE_UPDATE_MTA)
                    || accessType.equalsIgnoreCase(DEVICE_DELETE_FROM_DEVICE)
                    || accessType.equalsIgnoreCase(INTERNET_CHANGE_COS)
                    || accessType.equalsIgnoreCase(INTERNET_QUARANTINE)
                    || accessType.equalsIgnoreCase(INTERNET_SUSPEND)) {

                if (subSvc == null)
                    subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();

                logger.trace(3, "context subsvc : " + subSvc);

                SampSubSvcValue smp_internet_access_svc = null;
                SampSubSvcValue smp_resi_device_control_svc = null;
                SampSubSvcValue smp_wifi_access_svc = null;

                // Context Service is INTERNET
                if (subSvc.getSvcNm().equalsIgnoreCase(INTERNET_ACCESS)
                        || subSvc.getSvcNm().equalsIgnoreCase(SMP_INTERNET_ACCESS)) {

                    logger.trace(5, "In Context Service - Internet.");

                    smp_internet_access_svc = subSvc;

                    logger.trace(3, "Internet Service is : " + smp_internet_access_svc.getSvcNm() + ".");

                    smp_resi_device_control_svc = getResiDeviceControlFromInternetAccess(sub,
                            smp_internet_access_svc, INTERNET_HAS_ACCESS);

                    smp_wifi_access_svc = getWifiAccessFromInternet(sub, smp_internet_access_svc);

                }

                // Context Service is WIFI
                else if (subSvc.getSvcNm().equalsIgnoreCase(WIFI_ACCESS)
                        || subSvc.getSvcNm().equalsIgnoreCase(SMP_WIFI_ACCESS)) {

                    logger.trace(5, "Context Service is Wifi.");

                    smp_wifi_access_svc = subSvc;

                    logger.trace(3, "Wifi Service is : " + smp_wifi_access_svc.getSvcNm());

                    smp_internet_access_svc = getInternetAccessFromWifi(sub, smp_wifi_access_svc);

                    logger.trace(3, "Internet Service is : " + smp_internet_access_svc.getSvcNm());

                    smp_resi_device_control_svc = getResiDeviceControlFromInternetAccess(sub,
                            smp_internet_access_svc, INTERNET_HAS_ACCESS);

                    logger.trace(3, "Device Service is : " + smp_resi_device_control_svc.getSvcNm());

                }

                // Context Service is SMP Resi Device Control
                else if (subSvc.getSvcNm().equalsIgnoreCase(EMTA_DEVICE_CONTROL)
                        || subSvc.getSvcNm().equalsIgnoreCase(SMP_RESI_DEVICE_CONTROL)) {

                    logger.trace(5, "Context Service is Device.");

                    smp_resi_device_control_svc = subSvc;

                    logger.trace(3, "Device Service is : " + smp_resi_device_control_svc.getSvcNm());

                    smp_internet_access_svc = getInternetAccessFromResiDeviceControl(sub,
                            smp_resi_device_control_svc, INTERNET_HAS_ACCESS);

                    logger.trace(3, "Internet Service is : " + smp_internet_access_svc.getSvcNm());

                    smp_wifi_access_svc = getWifiAccessFromInternet(sub, smp_internet_access_svc);

                    logger.trace(3, "Wifi Service is : " + smp_wifi_access_svc.getSvcNm());

                }

                else {
                    // Unknown Context Service
                    result = "Unknown Context Service";
                    throw new IllegalArgumentException(result);
                }

                String provisionStatus = smp_internet_access_svc.getParmVal(PROVISION_STATUS);

                String desiredWifiHotspot = smp_wifi_access_svc.getParmVal(DESIRED_WIFI_HOTSPOT);

                String desiredWifiAccess = smp_wifi_access_svc.getParmVal(DESIRED_WIFI_ACCESS);

                String capableWifiHotspot = smp_wifi_access_svc.getParmVal(CAPABLE_WIFI_HOTSPOT);

                String capableWifiAccess = smp_wifi_access_svc.getParmVal(CAPABLE_WIFI_ACCESS);

                String quarantineProfile = smp_internet_access_svc.getParmVal(QUARANTINE_PROFILE);

                String numOfVoiceLines = smp_resi_device_control_svc.getParmVal(NUM_OF_VOICE_LINES);

                String qosFlavor = smp_internet_access_svc.getParmVal(QOS_FLAVOR);

                String model = smp_resi_device_control_svc.getParmVal(MODEL);

                String manufacturer = smp_resi_device_control_svc.getParmVal(MANUFACTURER);

                String classOfService = smp_internet_access_svc.getParmVal(CLASS_OF_SERVICE);

                String poip = smp_internet_access_svc.getParmVal(POIP);

                String resultPS = checkProvisionStatus(provisionStatus, accessType);

                if (resultPS == "") {
                    result = resultPS;
                    return result;
                }

                String resultWIFI = checkWifiParameters(capableWifiAccess, desiredWifiAccess,
                        capableWifiHotspot, desiredWifiHotspot, quarantineProfile, provisionStatus,
                        accessType);

                String resultNAC = checkNAC(provisionStatus, quarantineProfile, accessType);

                String resultIP = checkIP(numOfVoiceLines);

                String resultQOS = "QOS=";
                resultQOS = resultQOS.concat(qosFlavor);

                String resultEnableRouter = checkEnableRouter(capableWifiAccess, desiredWifiAccess,
                        capableWifiHotspot, desiredWifiHotspot, quarantineProfile, provisionStatus,
                        accessType);

                String resultEnableMTA = checkNumOfVoiceLines(numOfVoiceLines);

                String filter = new String();

                if (quarantineProfile != null) {
                    filter = getFilter(quarantineProfile, model, manufacturer);
                } else {
                    filter = getFilter(classOfService, model, manufacturer);
                }

                String resultFilter = "Filter=";
                resultFilter = resultFilter.concat(filter);

                String resultPOIP = checkPOIP(poip);

                // Concat all the result strings
                result = resultPS.concat(resultWIFI).concat("|").concat("NAC=").concat(resultNAC).concat("|")
                        .concat(resultIP).concat("|").concat(resultQOS).concat("|").concat("Enable_Router=")
                        .concat(resultEnableRouter).concat("|").concat("Enable_MTA=").concat(resultEnableMTA)
                        .concat("|").concat(resultFilter).concat("|").concat(resultPOIP);

                logger.trace(3, "CiscoBac40DataBean - Result:" + result);
                return result;

            }

            else if (accessType.equalsIgnoreCase(HRZ_STB_REPROV) || accessType.equalsIgnoreCase(HRZ_STB_SWAP)
                    || accessType.equalsIgnoreCase(HRZ_STB_RESUME)
                    || accessType.equalsIgnoreCase(HRZ_STB_MOVE)
                    || accessType.equalsIgnoreCase(HRZ_STB_SUSPEND)) {

                if (subSvc == null)
                    subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();

                logger.trace(3, "context subsvc : " + subSvc);

                SampSubSvcValue smp_hrz_internet_access_svc = null;

                // Context Service is HRZ INTERNET
                if (subSvc.getSvcNm().equalsIgnoreCase(HRZ_INTERNET_ACCESS)
                        || subSvc.getSvcNm().equalsIgnoreCase(SMP_HRZ_INTERNET_ACCESS)) {

                    logger.trace(5, "Context Service is HRZ Internet.");

                    smp_hrz_internet_access_svc = subSvc;

                    String provisionStatus = smp_hrz_internet_access_svc.getParmVal(PROVISION_STATUS);

                    String qosFlavor = smp_hrz_internet_access_svc.getParmVal(QOS_FLAVOR);

                    String filter = smp_hrz_internet_access_svc.getParmVal(FILTER);

                    result = getPropertiesForHrz(provisionStatus, accessType, qosFlavor, filter, evalContext);

                    logger.trace(3, "CiscoBac40DataBean - Result:" + result);
                    return result;

                }

            }

            else {
                // Unknown Expression
                result = "Please Check Your Expression";
                throw new IllegalArgumentException(result);
            }

            return result;

        } catch (Exception e) {
            logger.log("Exception:" + e.getMessage());
            throw new RemoteException("Exception:", e);

        }

    }

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
        ctx = arg0;
    }

    public void ejbCreate() throws CreateException, EJBException, RemoteException {
    }

}
