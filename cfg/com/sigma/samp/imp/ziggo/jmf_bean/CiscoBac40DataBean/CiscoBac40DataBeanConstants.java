package com.sigma.samp.imp.ziggo.jmf_bean.CiscoBac40DataBean;

//=======================================================================
//FILE INFO
//$Id: CiscoBac40DataBeanConstants.java,v 1.2 2015/05/08 15:49:53 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class CiscoBac40DataBeanConstants {

    public static final String CONTEXT_PARM_SUBSVCMODEL = "svc";
    public static final String CONTEXT_PARM_SUBMODEL    = "sub";
    
    public static final String WIFI_ACCESS          	= "wifi_access";
    public static final String SMP_WIFI_ACCESS          = "smp_wifi_access";
    public static final String INTERNET_ACCESS          = "internet_access";
    public static final String SMP_INTERNET_ACCESS      = "smp_internet_access";
    public static final String EMTA_DEVICE_CONTROL      = "emta_device_control";
    public static final String SMP_RESI_DEVICE_CONTROL  = "smp_resi_device_control";
    public static final String EMTA_DATA_PORT 			= "emta_data_port";
    public static final String SMP_DATA_PORT 			= "smp_data_port";
    public static final String SMP_EMTA_COMPOSED 		= "smp_emta_composed";
    public static final String INTERNET_SERVICE_COMPOSED = "internet_service_composed";
    public static final String SMP_HRZ_INTERNET_ACCESS  = "smp_hrz_internet_access";
    public static final String HRZ_INTERNET_ACCESS  	= "hrz_internet_access";
    
    public static final String DESIRED_WIFI_HOTSPOT     = "desired_wifi_hotspot";
    public static final String DESIRED_WIFI_ACCESS      = "desired_wifi_access";
    public static final String CAPABLE_WIFI_ACCESS      = "capable_wifi_access";
    public static final String CAPABLE_WIFI_HOTSPOT     = "capable_wifi_hotspot";
    public static final String POIP                     = "poip";
    public static final String NUM_OF_VOICE_LINES       = "num_of_voice_lines";
    public static final String DT_HAS_EQUIPMENT         = "dt_has_equipment";
    public static final String QOS_FLAVOR               = "qos_flavor";
    public static final String PROVISION_STATUS         = "provision_status";
    public static final String INTERNET_HAS_ACCESS      = "internet_has_access";
    public static final String QUARANTINE_PROFILE		= "quarantine_profile";
    public static final String FILTER					= "filter";
    public static final String MODEL					= "model";
    public static final String MANUFACTURER				= "manufacturer";
    public static final String CLASS_OF_SERVICE			= "class_of_service";
    
    
    public static final String ACTIVE                   = "active";
    public static final String MSO_BLOCK                = "mso_block";
    public static final String COURTESY_BLOCK           = "courtesy_block";
    
    public static final String YES						= "y";
    public static final String NO						= "n";
    
    public static final String NOWIFI 					= "nowifi";
    public static final String PUBLICWIFI	 			= "publicwifi";
    public static final String PRIVATEWIFI				= "privatewifi";
    
    
    
    //List of AccessTypes
    public static final String DEVICE_CHANGE_RDU        = "device_change_rdu";
    public static final String DEVICE_UPDATE	        = "device_update";
    public static final String WIFI_CHANGE_WIFI_SETTINGS     = "wifi_access_change_wifi_settings";
    
    public static final String INTERNET_RESUME	        = "internet_access_resume";
    public static final String INTERNET_ACTIVATION 		= "internet_access_activation";
    public static final String INTERNET_REPROV	        = "internet_access_reprov";
    public static final String DEVICE_CHANGE_POIP       = "device_change_poip";

    public static final String DEVICE_UPDATE_MTA	    = "device_update_mta";
    public static final String DEVICE_DELETE_FROM_DEVICE     = "device_delete_from_device";
    
    public static final String INTERNET_CHANGE_COS	    = "internet_access_change_cos";
    
    public static final String INTERNET_QUARANTINE	    = "internet_access_quarantine";
    
    public static final String INTERNET_SUSPEND	        = "internet_access_suspend";
    
    public static final String HRZ_STB_REPROV			= "hrz_stb_reprov";
    public static final String HRZ_STB_SWAP				= "hrz_stb_swap";
    public static final String HRZ_STB_RESUME			= "hrz_stb_resume";
    public static final String HRZ_STB_MOVE				= "hrz_stb_move";
    public static final String HRZ_STB_SUSPEND			= "hrz_stb_suspend";

}
