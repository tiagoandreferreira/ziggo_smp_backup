//==========================================================================
// $Id: GetSvcChangeBean.java,v 1.4 2015/07/24 10:59:02 monicac Exp $
// (@) Copyright Sigma Systems (Canada)
// * Based on CVS log
//
// DESCRIPTION
//
//==========================================================================

package com.sigma.samp.imp.ziggo.jmf_bean.GetSvcChangeBean;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.sigma.hframe.jerror.SmpPrcsException;
import com.sigma.hframe.jerror.SmpResourceException;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.oss.sub.EntityAssociationValue;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubEntityAssocChgs;
import com.sigma.samp.cmn.sub.SampSubEntityKey;
import com.sigma.samp.cmn.sub.SampSubSvcKey;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.cmn.sub.SubKeyFactory;
import com.sigma.samp.stm.cmn.Subnetwork;
import com.sigma.samp.stm.ssn.SSNAgent;
import com.sigma.samp.stm.ssn.SSN;
import com.sigma.samp.stm.ssn.SSNAgentHome;
import com.sigma.samp.stm.ssn.SSNServiceLocator;
import com.sigma.samp.vframe.submodel.SubModel;
import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.vframe.joss.SampAssociationValue;
import com.sigma.samp.cmn.order.SampOrderLineItemValue;
import com.sigma.samp.cmn.order.SampOrderValue;

public class GetSvcChangeBean implements SessionBean {

    private static Log          logger                   = new Log(GetSvcChangeBean.class.getName());
    private SessionContext      ctx;
    private static final String CONTEXT_PARM_SUBSVCMODEL = "svc";
    private static final String CONTEXT_PARM_SUBMODEL    = "sub";

    // jmf_bean.GetSvcChangeBean
    public String eval(String expression, Map inputLst, ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException {
        try {
            logger.trace(5, "In GetSvcChangeBean.eval()\n Expression received: " + expression);
            String[] jmfParm = getJmfParm(expression);
            SampSubSvcValue subSvc = (SampSubSvcValue) evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
            if (subSvc == null) {
                subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
            }
            logger.trace(5, "context subsvc : " + subSvc);

            SampSubSvcValue parentSubSvc = subSvc.getParentSubSvc();
            logger.trace(5, "parentSubSvc : " + parentSubSvc);

            SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
            logger.trace(5, "context sub : " + sub);

            SampOrderValue ordr = (SampOrderValue) evalContext.getParmAccess("order");
            logger.trace(5, "context ordr : " + ordr.getLineItemLst().size());

            Iterator itemItr = ordr.getLineItemLst().iterator();
            SampOrderLineItemValue smpLineItem = null;
            String result = null;
            while (itemItr.hasNext()) {
                smpLineItem = (SampOrderLineItemValue) itemItr.next();
                logger.trace(5, "line Item action name : " + smpLineItem.getActionNm());
                logger.trace(5, "line Item : " + smpLineItem.getSampEntityKey().toString());
                logger.trace(5, "line Item : " + smpLineItem.getSampEntity().toString());
                String str = smpLineItem.getSampEntityKey().toString();
                String sub_svc_id = str.substring(str.indexOf("[") + 1, str.length() - 1);
                logger.trace(5, "svc key : " + sub_svc_id);
                // SampSubSvcValue svc = (SampSubSvcValue) sub
                // .getSubSvc((SampSubSvcKey)
                // SubKeyFactory.getInstance().makeSubSvcKey(new Long(
                // sub_svc_id)));
                SampSubSvcValue svc = getIfChildService(parentSubSvc, ((SampSubSvcKey) SubKeyFactory
                        .getInstance().makeSubSvcKey(new Long(sub_svc_id))));
                logger.trace(5, "svc  : " + svc);
                if (svc != null) {
                    logger.trace(5, "In != null svc ");
                    if (result == null)
                        result = getSvcChangeTag(svc, smpLineItem.getActionNm());
                    else
                        result = result + "\n" + getSvcChangeTag(svc, smpLineItem.getActionNm());
                }
            }
            result = "<svc_chng>\n" + result + "</svc_chng>";
            logger.trace(5, "result : " + result);
            return result;
        } catch (Exception e) {
            throw new RemoteException("Exception caught: ", e);
        }

    }

    private SampSubSvcValue getIfChildService(SampSubSvcValue parentSubSvc, SampSubSvcKey sampSubSvcKey) {
        logger.trace(5, "In side getIfChildService : " + parentSubSvc.getSvcNm());
        Iterator itr = parentSubSvc.getChildSubSvcLst().iterator();
        logger.trace(5, "size : " + parentSubSvc.getChildSubSvcLst().size());
        SampSubSvcValue subSvc = null;
        SampSubSvcValue svc = null;
        while (itr.hasNext()) {
            subSvc = (SampSubSvcValue) itr.next();
            logger.trace(5,
                    "while subSvc : " + subSvc.getSvcNm() + "subSvc.getSubSvcKey()  " + subSvc.getSubSvcKey());
            if (subSvc.getSubSvcKey().equals(sampSubSvcKey)) {
                svc = subSvc;
                logger.trace(5, "Svc : " + svc.getSvcNm());
                return svc;
            }

            if (subSvc.isComposed() && svc == null) {
                logger.trace(5, "composed subSvc : " + subSvc.getSvcNm());
                svc = getIfChildService(subSvc, sampSubSvcKey);
                if (svc != null)
                    return svc;
            }

        }
        return svc;
    }

    private String getSvcChangeTag(SampSubSvcValue svc, String action) {
        String svcChangeTag = null;
        String provisionStatus = svc.getProvisionStatus();
        logger.trace(5, "svcNm: " + svc.getProvisionableSvcNm() + "  state :  " + svc.getState());

        if (action.equalsIgnoreCase("update"))
            action = "change";

        logger.trace(5, "svcNm: " + svc.getProvisionableSvcNm() + "  action :  " + action);

        if (action.equals("add") && !svc.isComposed()) {
            Iterator parmItr = svc.getNonReservedParmNmList().iterator();
            String parmNm = null;
            svcChangeTag = "<" + svc.getProvisionableSvcNm() + ">\n" + "<action>" + action + "</action>\n";
            while (parmItr.hasNext()) {
                parmNm = (String) parmItr.next();
                logger.trace(5, "parmNm : " + parmNm);
                svcChangeTag = svcChangeTag + "<" + parmNm + ">" + svc.getParmVal(parmNm) + "</" + parmNm
                        + ">\n";
            }
            svcChangeTag = svcChangeTag + "</" + svc.getProvisionableSvcNm() + ">\n";
        } else if (action.equals("delete")) {
            Iterator parmItr = svc.getModifiedAttributeNames().iterator();
            String parmNm = null;
            if (svc.isComposed()) {
                Iterator svcItr = svc.getChildSubSvcLst().iterator();
                SampSubSvcValue subSvc = null;
                while (svcItr.hasNext()) {
                    subSvc = (SampSubSvcValue) svcItr.next();
                    if (subSvc.isComposed()) {
                        Iterator subSvcItr = subSvc.getChildSubSvcLst().iterator();
                        SampSubSvcValue subSvc_2 = null;
                        while (subSvcItr.hasNext()) {
                            subSvc_2 = (SampSubSvcValue) svcItr.next();
                            svcChangeTag = svcChangeTag + "\n<" + subSvc_2.getProvisionableSvcNm() + ">\n"
                                    + "<action>" + action + "</action>\n" + "</"
                                    + subSvc_2.getProvisionableSvcNm() + ">\n";
                        }

                    } else {
                        svcChangeTag = svcChangeTag + "\n<" + subSvc.getProvisionableSvcNm() + ">\n"
                                + "<action>" + action + "</action>\n" + "</" + subSvc.getProvisionableSvcNm()
                                + ">\n";
                    }

                }
            } else {
                svcChangeTag = "<" + svc.getProvisionableSvcNm() + ">\n" + "<action>" + action
                        + "</action>\n" + "</" + svc.getProvisionableSvcNm() + ">\n";
            }
        } else if (action.equals("change") && !svc.isComposed()) {
            Iterator parmItr = svc.getModifiedAttributeNames().iterator();
            String parmNm = null;
            svcChangeTag = "<" + svc.getProvisionableSvcNm() + ">\n" + "<action>" + action + "</action>\n";
            while (parmItr.hasNext()) {
                parmNm = (String) parmItr.next();
                logger.trace(5, "parmNm : " + parmNm);
                svcChangeTag = svcChangeTag + "<" + parmNm + ">" + svc.getParmVal(parmNm) + "</" + parmNm
                        + ">\n";
            }
            svcChangeTag = svcChangeTag + "</" + svc.getProvisionableSvcNm() + ">\n";
        }
        logger.trace(5, "svcChangeTag : " + svcChangeTag);
        return svcChangeTag;
    }

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
        ctx = arg0;
    }

    public void ejbCreate() throws CreateException, EJBException, RemoteException {
    }

    private static String[] getJmfParm(String expression) throws SmpPrcsException {
        String[] result = null;

        logger.trace(5, "Evaluate expression:" + expression);
        result = expression.split("\\.");
        for (int i = 0; i < result.length; i++) {
            logger.trace(5, "value:" + result[i]);
        }
        return result;
    }

}
