package com.sigma.samp.imp.ziggo.jmf_bean.IAQOSFlavoursDataBean;

//=======================================================================
//FILE INFO
//$Id: IAQOSFlavoursConstant.java,v 1.1 2015/05/07 17:19:52 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class IAQOSFlavoursConstant {

    public static final String CONTEXT_PARM_SUBSVCMODEL    = "svc";
    public static final String CONTEXT_PARM_SUBMODEL       = "sub";
    public static final String ACTUAL_WIFI_HOTSPOT         = "actual_Wifi_hotspot";
    public static final String DEVICE_NICKNAME             = "device_nickname";
    public static final String FETCHING_FILTER             = "fetching_filter";
    public static final String QUARANTINE                  = "QUARANTINE";
    public static final String ACTUAL_WIFI_ACCESS          = "actual_wifi_access";
    public static final String QOS_FLAVOR                  = "qos_flavor";
    public static final String SMP_INTERNET_ACCESS         = "smp_internet_access";
    public static final String INTERNET_ACCESS             = "internet_access";
    public static final String SMP_WIFI_ACCESS             = "smp_wifi_access";
    public static final String WIFI_ACCESS                 = "wifi_access";
    public static final String CLASS_OF_SERVICE            = "class_of_service";
    public static final String DESIRED_WIFI_ACCESS         = "desired_wifi_access";
    public static final String DESIRED_WIFI_HOTSPOT        = "desired_wifi_hotspot";
    public static final String EMTA_DEVICE_CONTROL         = "emta_device_control";
    public static final String MODEL                       = "model";
    public static final String MANUFACTURER                = "manufacturer";
    public static final String INTERNET_HAS_ACCESS         = "internet_has_access";
    public static final String WIFI_HAS_AIR_INTERFACE      = "wifi_has_air_interface";
    public static final String SMP_RESI_DEVICE_CONTROL     = "smp_resi_device_control";
    public static final String SMP_SWITCH_DIAL_TONE_ACCESS = "smp_switch_dial_tone_access";
    public static final String SMP_DIAL_TONE_ACCESS        = "smp_dial_tone_access";
    public static final String DT_HAS_EQUIPMENT            = "dt_has_equipment";
    public static final String QUARANTINE_QOS              = "quarantine_profile";
    public static final String YES                         = "Y";
    public static final String NO                          = "N";
    public static final String QOS_FLAVOR_NAME             = "QOS_FLAVOR_NAME";
    public static final String FILTER                      = "FILTER";
    public static final String IS_ROUTER                   = "IS_ROUTER";
    public static final String VOICE_ASSOC_CHECK           = "voice_assoc_check";
	public static final String CISCOBAC_PROFILE			   = "ciscobac_profile";
	public static final String PUBLIC_WIFI				   = "PUBLIC_WIFI";
    
}
