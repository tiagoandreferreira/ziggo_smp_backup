/* $Id IAQOSFlavoursDataBean.java ,v 1.0 2013/06/05 16:19:30 Prakash $ initial version $
   $Id IAQOSFlavoursDataBean.java ,v 1.1 2013/06/17 16:19:30 Prakash $ Code removed for cos_and_flavor and code added for desired_qos_flavor $ 
   $Id IAQOSFlavoursDataBean.java ,v 1.2 2013/07/11 15:05:30 Prakash $ code added for device_nickname $
   $Id IAQOSFlavoursDataBean.java ,v 1.3 2013/07/15 15:05:30 Prakash $ Replace desired_class_of_service with class_of_service and desired_qos_flavor with qos_flavor $
   $Id IAQOSFlavoursDataBean.java ,v 1.4 2013/10/16 15:05:30 Prakash $ code added for delete and deactivate voice scenarios. it is for checking association to handle bac impact on delete voice $
   $Id IAQOSFlavoursDataBean.java ,v 1.5 2014/01/07 16:19:30 Prakash $ code added fetching filter on based on flavor. $
   $Id IAQOSFlavoursDataBean.java ,v 1.6 2014/07/15 16:19:30 Prakash $ code changes done for DB connection for mta number$
   $Id IAQOSFlavoursDataBean.java ,v 1.7 2014/07/23 16:19:30 Prakash $ code added for quarantine_QOS for Walled Garden CR $
   $Id IAQOSFlavoursDataBean.java ,v 1.8 2014/08/13 16:19:30 Prakash $ create sequence used to generated unique mta number $ 
   $Id IAQOSFlavoursDataBean.java ,v 1.9 2015/07/22 16:19:30 Prakash $ code added to fetch ciscobac profile from DB $ 
 */

package com.sigma.samp.imp.ziggo.jmf_bean.IAQOSFlavoursDataBean;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.vframe.submodel.SubModel;
import com.sigma.vframe.jcmn.ParmEvalContext;
//import com.sigma.samp.vframe.utils.ResUtil;
import com.sigma.vframe.util.ResUtil;

import java.sql.*;

import static com.sigma.samp.imp.ziggo.jmf_bean.IAQOSFlavoursDataBean.IAQOSFlavoursConstant.*;

public class IAQOSFlavoursDataBean implements SessionBean {

	private static Log logger = new Log(IAQOSFlavoursDataBean.class.getName());
	private SessionContext ctx;
	SampSubSvcValue subSvc = null;
	
	/**
     * Eval Method
     * 
     * @param expression
     * @param inputLst
     * @param evalContext
     * @return expression to be used in xml according to the context
     * @throws IllegalArgumentException
     * @throws RemoteException
     */
	
	public String eval(final String expression, final Map inputLst,
			final ParmEvalContext evalContext) throws IllegalArgumentException,
			RemoteException {
		logger.trace(3, "In Eval Method");
		logger.trace(3, "Version 1.9 IAQOSFlavoursDataBean : " + expression);
		String result = null;
		String QosFlavor = null;
		String ciscobacProfile = null;
		String combination_of_QOS = null;
		String desired_wifi_access = null;
		String desired_wifi_hotspot = null;
		String model = null;
		String manufacture = null;
		String accessType = null;
		String filter = null;
		try {
			final StringTokenizer strToken = new StringTokenizer(expression,
					".", false);
			final ArrayList keyLst = new ArrayList();
			while (strToken.hasMoreTokens()) {
				keyLst.add(strToken.nextToken());
			}
			final int size = keyLst.size();
			logger.trace(3, " Size : " + size);
			if (size == 1) {
				accessType = (String) keyLst.get(size - 1);
				logger.trace(3, "access type ==" + accessType);
			}
			subSvc = (SampSubSvcValue) evalContext
					.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
			if (subSvc == null) {
				subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
			}
			logger.trace(5, "context subsvc: " + subSvc);
			final SubModel sub = (SubModel) evalContext
					.getParmAccess(CONTEXT_PARM_SUBMODEL);
			// This code is added to get value of flavor
			if (accessType.equalsIgnoreCase(QOS_FLAVOR)) {
				if (subSvc.getProvisionableSvcNm().equals(SMP_INTERNET_ACCESS)
						|| subSvc.getProvisionableSvcNm().equals(
								INTERNET_ACCESS)) {
					logger.trace(5,
							"Got internet access or smp internet access and Value of Quarantine qos is : "
									+ subSvc.getParmVal(QUARANTINE_QOS));
					if (subSvc.getParmVal(QUARANTINE_QOS) != null
							&& subSvc.getParmVal(QUARANTINE_QOS)
									.equalsIgnoreCase(QUARANTINE)) {
						combination_of_QOS = subSvc.getParmVal(QUARANTINE_QOS);
						logger.trace(5,
								"combination of qos : Quarantine QOS : "
										+ combination_of_QOS);
					} else {
						combination_of_QOS = subSvc
								.getParmVal(CLASS_OF_SERVICE);
						logger.trace(5, "combination of qos : "
								+ combination_of_QOS);
					}
					SampSubSvcValue dataPortSvc = null;
					dataPortSvc = getZEndSvcFrmAEndSvc(sub, subSvc,
							INTERNET_HAS_ACCESS);
					if (dataPortSvc != null) {
						logger.trace(5, "data port svc : " + dataPortSvc);
						final Iterator itr1 = dataPortSvc.getParentSubSvc()
								.getChildSubSvcLst().iterator();
						while (itr1.hasNext()) {
							final SampSubSvcValue svc = (SampSubSvcValue) itr1
									.next();
							logger.trace(5, "Child svc: " + svc.getSvcNm());
							if ((svc.getSvcNm().equals(EMTA_DEVICE_CONTROL))
									&& (svc.getProvisionableSvcNm()
											.equals(SMP_RESI_DEVICE_CONTROL))) {
								logger.trace(5,
										"Found emta device control or smp resi device control");
								model = svc.getParmVal(MODEL);
								manufacture = svc.getParmVal(MANUFACTURER);
								logger.trace(3, "Model = " + model
										+ "and Manufacture = " + manufacture);
								break;
							}
						}
						QosFlavor = getQOSFlavours(combination_of_QOS, model,
								manufacture);
						logger.trace(3, "Result of QosFlavor = " + QosFlavor);
						if (QosFlavor == null) {
							logger.trace(5, "QosFlavor not found");
							result = "Flavor Not Found";
						} else {
							logger.trace(5, "QosFlavor is not null");
							result = QosFlavor;
						}
					} else {
						result = "";
						logger.trace(5,
								"else added for deactivation problem fix and result is ="
										+ result);
					}

				}
				logger.trace(3, "After matching service name");
				return result;
			}
			// This code is added to get value of filter
			else if (accessType.equalsIgnoreCase(FETCHING_FILTER)) {
				SampSubSvcValue sampSub = null;
				SampSubSvcValue svc = null;
				logger.trace(5, "Total sub service" + sub.getSubSvcs().size());
				final Iterator itr = sub.getSubSvcs().iterator();
				while (itr.hasNext()) {
					sampSub = (SampSubSvcValue) itr.next();
					logger.trace(5, "Child svc: " + sampSub.getSvcNm());
					if (sampSub.getSvcNm().equals(INTERNET_ACCESS)
							&& sampSub.getProvisionableSvcNm().equals(
									SMP_INTERNET_ACCESS)) {
						logger.trace(5,
								"Got internet access or smp internet access and Value of Quarantine qos is : "
										+ sampSub.getParmVal(QUARANTINE_QOS));
						if (sampSub.getParmVal(QUARANTINE_QOS) != null
								&& sampSub.getParmVal(QUARANTINE_QOS)
										.equalsIgnoreCase(QUARANTINE)) {
							combination_of_QOS = sampSub
									.getParmVal(QUARANTINE_QOS);
							logger.trace(5,
									"Combination of qos : Quarantine QOS : "
											+ combination_of_QOS);
						} else {
							combination_of_QOS = sampSub
									.getParmVal(CLASS_OF_SERVICE);
							logger.trace(5, "Combination of qos : "
									+ combination_of_QOS);
						}
						SampSubSvcValue dataPortSvc = null;
						dataPortSvc = getZEndSvcFrmAEndSvc(sub, sampSub,
								INTERNET_HAS_ACCESS);
						if (dataPortSvc != null) {
							logger.trace(5, "data port svc : " + dataPortSvc);
							final Iterator itr1 = dataPortSvc.getParentSubSvc()
									.getChildSubSvcLst().iterator();
							while (itr1.hasNext()) {
								svc = (SampSubSvcValue) itr1.next();
								logger.trace(5, "Child svc: " + svc.getSvcNm());
								if ((svc.getSvcNm().equals(EMTA_DEVICE_CONTROL))
										&& (svc.getProvisionableSvcNm()
												.equals(SMP_RESI_DEVICE_CONTROL))) {
									logger.trace(5,
											"Found emta device control or smp resi device control");
									model = svc.getParmVal(MODEL);
									manufacture = svc.getParmVal(MANUFACTURER);
									logger.trace(3, "Model = " + model
											+ "and Manufacture = "
											+ manufacture);
									break;
								}
							}
							filter = getFilter(combination_of_QOS, model,
									manufacture);
							logger.trace(3, "Result of filter = " + filter);
							if (filter == null) {
								logger.trace(5, "filter not found");
								result = "filter Not Found";
							} else {
								logger.trace(5, "filter is not null");
								result = filter;
							}
						} else {
							logger.trace(5,
									"else added for deactivation problem fix and result is ="
											+ result);
						}
					}
				}
				logger.trace(5, "No Internet Service Found");
				return result;
			}
			// This code is added to get actual wifi access.
			else if (accessType.equalsIgnoreCase(ACTUAL_WIFI_ACCESS)) {
				if (subSvc.getProvisionableSvcNm().equals(SMP_WIFI_ACCESS)
						|| subSvc.getProvisionableSvcNm().equals(WIFI_ACCESS)) {
					logger.trace(3, "Wifi Service Found");
					SampSubSvcValue dataPortSvc = null;
					desired_wifi_access = subSvc
							.getParmVal(DESIRED_WIFI_ACCESS);
					logger.trace(5, "Desired wifi access : "
							+ desired_wifi_access);
					dataPortSvc = getZEndSvcFrmAEndSvc(sub, subSvc,
							WIFI_HAS_AIR_INTERFACE);
					if (dataPortSvc != null) {
						logger.trace(5,
								"data port svc of desired wifi access : "
										+ dataPortSvc);
						final Iterator itr1 = dataPortSvc.getParentSubSvc()
								.getChildSubSvcLst().iterator();
						while (itr1.hasNext()) {
							final SampSubSvcValue svc = (SampSubSvcValue) itr1
									.next();
							logger.trace(5, "Child svc: " + svc.getSvcNm());
							if ((svc.getSvcNm().equals(EMTA_DEVICE_CONTROL))
									&& (svc.getProvisionableSvcNm()
											.equals(SMP_RESI_DEVICE_CONTROL))) {
								logger.trace(3,
										"Found emta device control or smp resi device control");
								model = svc.getParmVal(MODEL);
								manufacture = svc.getParmVal(MANUFACTURER);
								logger.trace(3, "Model = " + model
										+ "and Manufacture = " + manufacture);
								break;
							}
						}
						result = getActualWifiAcess(model, manufacture);
						logger.trace(3, "Result of actual wifi access = "
								+ result);
						if (result != null && result.equalsIgnoreCase("1")) {
							logger.trace(3, "Result is 1");
							result = YES;
							return result;
						} else {
							logger.trace(3, "Result is not 1");
							result = NO;
							return result;
						}
					} else {
						result = "";
						logger.trace(3,
								"else added for deactivation problem fix and result is ="
										+ result);
					}
				}
				logger.trace(3, "After matching service name");
				return result;
			}
			// This code is added to get actual wifi hospot
			else if (accessType.equalsIgnoreCase(ACTUAL_WIFI_HOTSPOT)) {
				if (subSvc.getProvisionableSvcNm().equals(SMP_WIFI_ACCESS)
						|| subSvc.getProvisionableSvcNm().equals(WIFI_ACCESS)) {
					logger.trace(5, "WiFi Service Found");
					SampSubSvcValue dataPortSvc = null;
					desired_wifi_hotspot = subSvc
							.getParmVal(DESIRED_WIFI_HOTSPOT);
					logger.trace(5, "Desired wifi hostpot : "
							+ desired_wifi_hotspot);
					dataPortSvc = getZEndSvcFrmAEndSvc(sub, subSvc,
							WIFI_HAS_AIR_INTERFACE);
					if (dataPortSvc != null) {
						logger.trace(5,
								"data port svc of desired wifi hotspot : "
										+ dataPortSvc);
						final Iterator itr1 = dataPortSvc.getParentSubSvc()
								.getChildSubSvcLst().iterator();
						while (itr1.hasNext()) {
							final SampSubSvcValue svc = (SampSubSvcValue) itr1
									.next();
							logger.trace(5, "Child svc: " + svc.getSvcNm());
							if ((svc.getSvcNm().equals(EMTA_DEVICE_CONTROL))
									&& (svc.getProvisionableSvcNm()
											.equals(SMP_RESI_DEVICE_CONTROL))) {
								logger.trace(5,
										"Found emta device control or smp resi device control");
								model = svc.getParmVal(MODEL);
								manufacture = svc.getParmVal(MANUFACTURER);
								logger.trace(3, "Model = " + model
										+ "and Manufacture = " + manufacture);
								break;
							}
						}
						result = getActualWifiHotspot(model, manufacture);
						logger.trace(3, "Result of actual wifi hotspot = "
								+ result);
						if (result != null && result.equalsIgnoreCase("1")) {
							logger.trace(3, "Result is 1");
							result = YES;
							return result;
						} else {
							logger.trace(3, "Result is not 1");
							result = NO;
							return result;
						}
					} else {
						result = "";
						logger.trace(3,
								"else added for deactivation problem fix and result is ="
										+ result);
					}
				}
				logger.trace(3, "After matching service name");
				return result;
			}
			// This code will return device nickname
			else if (accessType.equalsIgnoreCase(DEVICE_NICKNAME)) {
				result = getMtaNumber();
				logger.trace(3, "Result of device_nickname = " + result);
				if (result != null) {
					logger.trace(3, "Result is not null");
				} else {
					logger.trace(3, "Result is null");
				}
				return result;
			}
			// This code will return the value as per Association exists or not.
			else if (accessType.equalsIgnoreCase(VOICE_ASSOC_CHECK)) {
				SampSubSvcValue dataPortSvc = null;
				if ((subSvc.getSvcNm().equals(SMP_SWITCH_DIAL_TONE_ACCESS))
						&& (subSvc.getProvisionableSvcNm()
								.equals(SMP_DIAL_TONE_ACCESS))) {
					logger.trace(5,
							"Found smp dial tone access or smp switch dial tone access");
					dataPortSvc = getZEndSvcFrmAEndSvc(sub, subSvc,
							DT_HAS_EQUIPMENT);
					logger.trace(5, "data port svc of voice line : "
							+ dataPortSvc);
					if (dataPortSvc != null) {
						result = "1";
						logger.trace(3, "Association exists");
						return result;
					} else {
						result = "0";
						logger.trace(3, "Association does not exist");
						return result;
					}
				} else {
					logger.trace(5, "Incoming service is not voice line");
					result = "null";
					return result;
				}
			}
			// This code will return the value of ciscobac profile from DB as
			// per model and manufacturer.
			else if (accessType.equalsIgnoreCase(CISCOBAC_PROFILE)) {
				final Iterator itrSub = sub.getSubSvcs().iterator();
				SampSubSvcValue service = null;
				while (itrSub.hasNext()) {
					logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itrSub.next();
					if (service.getSvcNm()
							.equalsIgnoreCase(EMTA_DEVICE_CONTROL)
							&& service.getProvisionableSvcNm()
									.equalsIgnoreCase(SMP_RESI_DEVICE_CONTROL)) {
						logger.trace(5, "Got emta device control");
						model = service.getParmVal(MODEL);
						manufacture = service.getParmVal(MANUFACTURER);
						logger.trace(3, "Model = " + model
								+ "and Manufacture = " + manufacture);
						break;
					}
				}
				if (model != null && manufacture != null) {
					ciscobacProfile = getCiscobacProfile(model, manufacture);
					logger.trace(3, "Result of ciscobacProfile = "
							+ ciscobacProfile);
					if (ciscobacProfile == null) {
						logger.trace(5, "CiscobacProfile not found");
						result = "CiscobacProfile Not Found";
					} else {
						logger.trace(5, "CiscobacProfile is not null");
						result = ciscobacProfile;
					}
				} else {
					result = "Either Model or Manufacturer is not present";
				}
				return result;
			} else {
				result = "Please Check Your Expression";
				return result;
			}
		} catch (Exception ex) {
			final String errMsg = "Unsupported Expression : " + expression;
			throw new IllegalArgumentException(errMsg);
		}
	}
	
	/**
     * This method will return the Z svc from a association with a the svc A
     * 
     * @param sub subscriber
     * @param subSvc svc A
     * @param assocName
     * @return svc Z
     */
	
	// This method will find out ZEnd from AEnd Service
	private static SampSubSvcValue getZEndSvcFrmAEndSvc(final SampSubValue sub,
			final SampSubSvcValue subSvc, final String assocName) {
		logger.trace(5, "Getzend function called");
		SampSubSvcValue zEndSvc = null;
		logger.trace(3, "SubSvc name = " + subSvc.getSvcNm()
				+ " current is A End Service");
		final Collection coll = subSvc.getEntityAssociationsByType(assocName);
		logger.trace(5, "collection = " + coll);
		if (coll != null) {
			logger.trace(3, "Size = " + coll.size()
					+ "  for association type = " + assocName);
			for (final Iterator itr = coll.iterator(); itr.hasNext();) {
				logger.trace(3, "in side for in zend function");
				final SampSubEntityAssoc assoc = (SampSubEntityAssoc) itr
						.next();
				zEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc
						.getZEndEntityKey());
				return zEndSvc;
			}
		}
		return zEndSvc;
	}

	
	/**
     * This method will return flavor as per the combination of COS, model and manufacture
     * 
     * @param qos
     * @param model
     * @param manufacture
     * @return
     * @throws RemoteException
     */
	
	// This method will return flavor as per the combination of COS, model and
	// manufacture
	private String getQOSFlavours(final String qos, final String model,
			final String manufacture) throws RemoteException {
		logger.trace(5, "In side flavors function");
		String flavor = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt
					.executeQuery("SELECT QOS_FLAVOR_NAME FROM CUST_BAC_MODEM_MFG bmm, CUST_ALLOWED_MODEM_PROFILE amp, CUST_DIN_QOS dq, CUST_DIN_QOS_FLAVOR dqf WHERE bmm.MFG_ID=amp.MFG_ID and amp.DIN_QOS_ID=dq.DIN_QOS_ID and dq.DIN_QOS_FLV_ID=dqf.DIN_QOS_FLV_ID and bmm.COS_MANUFACTURER="
							+ "'"
							+ manufacture
							+ "'"
							+ " and bmm.COS_MODEL= "
							+ "'"
							+ model
							+ "'"
							+ " and dq.QOS_NAME= "
							+ "'"
							+ qos + "'");
			while (rset.next()) {
				flavor = rset.getString(QOS_FLAVOR_NAME);
				logger.trace(5, "got the flavor" + flavor);
			}
		} catch (SQLException se) {
			logger.trace(6, "SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
		return flavor;
	}
	
	
	/**
     * This method will return flavor as per the combination of COS, model and manufacture
     * 
     * @param qos
     * @param model
     * @param manufacture
     * @return flavor
     * @throws RemoteException
     */
	
	
	// This method will return flavor as per the combination of COS, model and
	// manufacture
	private String getFilter(final String qos, final String model,
			final String manufacture) throws RemoteException {
		logger.trace(5, "In side Filter function");
		String filter = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt
					.executeQuery("SELECT FILTER FROM CUST_BAC_MODEM_MFG bmm, CUST_ALLOWED_MODEM_PROFILE amp, CUST_DIN_QOS dq, CUST_DIN_QOS_FLAVOR dqf WHERE bmm.MFG_ID=amp.MFG_ID and amp.DIN_QOS_ID=dq.DIN_QOS_ID and dq.DIN_QOS_FLV_ID=dqf.DIN_QOS_FLV_ID and bmm.COS_MANUFACTURER="
							+ "'"
							+ manufacture
							+ "'"
							+ " and bmm.COS_MODEL= "
							+ "'"
							+ model
							+ "'"
							+ " and dq.QOS_NAME= "
							+ "'"
							+ qos + "'");
			while (rset.next()) {
				filter = rset.getString(FILTER);
				logger.trace(5, "got the filter" + filter);
			}
		} catch (SQLException se) {
			logger.trace(6, "SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
		return filter;
	}

	/**
     * This method will return actual wifi access as per the combination model and manufacture
     * 
     * @param model
     * @param manufacture
     * @return actual wifi access
     * @throws RemoteException
     */
	
	
	// This method will return actual wifi acess as per the combination model
	// and manufacture
	private String getActualWifiAcess(final String model,
			final String manufacture) throws RemoteException {
		logger.trace(5, "In side actual wifi access function");
		String isRouter = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt
					.executeQuery("SELECT IS_ROUTER FROM CUST_BAC_MODEM_MFG WHERE COS_MANUFACTURER="
							+ "'"
							+ manufacture
							+ "'"
							+ " and COS_MODEL= "
							+ "'" + model + "'");
			while (rset.next()) {
				isRouter = rset.getString(IS_ROUTER);
				logger.trace(5, "got isRouter" + isRouter);
			}
		} catch (SQLException se) {
			logger.trace(6, "SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
		return isRouter;
	}
	
	/**
     * This method will return actual wifi hotspot as per the combination model and manufacture
     * 
     * @param model
     * @param manufacture
     * @return actual wifi hotspot
     * @throws RemoteException
     */
	
	
	// This method will return actual wifi hotspot as per the combination model
	// and manufacture
	private String getActualWifiHotspot(final String model,
			final String manufacture) throws RemoteException {
		logger.trace(5, "In side actual wifi hotspot function");
		String publicWiFi = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt
					.executeQuery("SELECT PUBLIC_WIFI FROM CUST_BAC_MODEM_MFG WHERE COS_MANUFACTURER="
							+ "'"
							+ manufacture
							+ "'"
							+ " and COS_MODEL= "
							+ "'" + model + "'");
			while (rset.next()) {
				publicWiFi = rset.getString(PUBLIC_WIFI);
				logger.trace(5, "got public wifi" + publicWiFi);
			}
		} catch (SQLException se) {
			logger.trace(6, "SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
		return publicWiFi;
	}
	
	/**
     * This method will return Mta Number from DB
     * 
     * @return Mta Number
     * @throws RemoteException
     */
	
	// This method will return Mta Number from DB
	private String getMtaNumber() throws RemoteException {
		logger.trace(5, "Get Mta Number method called");
		Connection conn = null;
		String mtaNumber = null;
		ResultSet rset = null;
		PreparedStatement selectStmt = null;
		try {
			// get connection
			conn = ResUtil.getNonXADbConnection();
			final String selectStmtStr = "Select mta_fqdn_seq.nextval from dual";
			selectStmt = conn.prepareStatement(selectStmtStr);
			logger.trace(5, "SQL statement 1: " + selectStmt.toString());
			rset = selectStmt.executeQuery();
			while (rset.next()) {
				mtaNumber = rset.getString(1);
				logger.trace(3, "Current MTA Number = " + mtaNumber);
				break;
			}
		} catch (SQLException se) {
			throw new RemoteException(se.toString());
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (selectStmt != null) {
				ResUtil.closeDBResource(selectStmt);
			}
			ResUtil.closeConnection(conn);
		}
		return mtaNumber;
	}
	
	/***
	 * 
	 * @param model
	 * @param manufacture
	 * @return bacProfile
	 * @throws RemoteException
	 */
	
	
	// This method will return bac profile as per the combination model and
	// manufacture
	private String getCiscobacProfile(final String model,
			final String manufacture) throws RemoteException {
		logger.trace(5, "In side getCiscobacProfile function");
		String bacProfile = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt
					.executeQuery("SELECT CISCOBAC_PROFILE FROM CUST_BAC_MODEM_MFG WHERE COS_MANUFACTURER='"
							+ manufacture + "' AND COS_MODEL= '" + model + "'");
			while (rset.next()) {
				bacProfile = rset.getString("CISCOBAC_PROFILE");
				logger.trace(5, "got the bacProfile" + bacProfile);
			}
		} catch (SQLException se) {
			logger.trace(6, "SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
		return bacProfile;
	}

	public void ejbActivate() throws RemoteException, EJBException {
	}

	public void ejbPassivate() throws RemoteException, EJBException {
	}

	public void ejbRemove() throws RemoteException, EJBException {
	}

	public void setSessionContext(final SessionContext context)
			throws RemoteException, EJBException {
		this.ctx = context;
	}

	public void ejbCreate() throws CreateException, EJBException,
			RemoteException {
	}
}
