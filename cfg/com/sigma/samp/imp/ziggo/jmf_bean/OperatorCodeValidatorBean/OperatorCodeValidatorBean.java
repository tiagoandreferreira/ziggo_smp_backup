package com.sigma.samp.imp.ziggo.jmf_bean.OperatorCodeValidatorBean;
/* $Id OperatorCodeValidatorBean.java ,v 1.0 2014/11/11 16:19:30 Nidhin $ initial version $ */
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.vframe.submodel.SubModel;
import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.vframe.util.ResUtil;

import static com.sigma.samp.imp.ziggo.jmf_bean.OperatorCodeValidatorBean.OperatorCodeValidatorConstant.*;

import java.sql.*;

public class OperatorCodeValidatorBean implements SessionBean {

	private static Log logger = new Log(OperatorCodeValidatorBean.class.getName());
	private SessionContext ctx;
	SampSubSvcValue subSvc = null;
	
    /**
     * Eval Method
     * 
     * @param expression
     * @param inputLst
     * @param evalContext
     * @return expression to be used in xml according to the context
     * @throws IllegalArgumentException
     * @throws RemoteException
     */
    public String eval(final String expression, final Map inputLst, final ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException {
		logger.log("In Eval Method");
		logger.trace(3, "Version 1.0 OperatorCodeValidatorBean : " + expression);
		String result = null;
		String accessType = null;
		
		try {
			final StringTokenizer strToken = new StringTokenizer(expression,
					".", false);
			final ArrayList keyLst = new ArrayList();
			while (strToken.hasMoreTokens()) {
				keyLst.add(strToken.nextToken());
			}
			final int size = keyLst.size();
			logger.trace(3, " Size : " + size);
			if (size == 1) {
				accessType = (String) keyLst.get(size - 1);
				logger.trace(3, "access type ==" + accessType);
			}
			subSvc = (SampSubSvcValue) evalContext
					.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
			if (subSvc == null) {
				subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
			}
			logger.trace(5, "context subsvc: " + subSvc);
			final SubModel sub = (SubModel) evalContext
					.getParmAccess(CONTEXT_PARM_SUBMODEL);
			
			// This code is added to check the operator code
            if (accessType.equalsIgnoreCase(OPERATOR_CODE_VALIDATOR)) {
                final String operator_code = subSvc.getParmVal(OPERATOR_CODE);
				logger.trace(5, "Operator Code in request : " + operator_code);
				
				result = validateOperatorCode(operator_code);
				return result;
				
			}

			else {
				result = "Please Check Your Expression";
				return result;
			}
		} catch (Exception ex) {
			final String errMsg = "Unsupported Expression : " + expression;
            logger.log(errMsg);
			throw new IllegalArgumentException(errMsg);
		}
	}

    /**
     * This method is to validate the operator code
     * 
     * @param operator_code
     * @return true if operator code match, otherwise false
     * @throws RemoteException
     */
    private String validateOperatorCode(String operator_code) throws RemoteException {
		logger.log("Inside Operator Code function");
		String operator_code_from_table = null;
		ResultSet rset = null;
		Statement stmt = null;
		String validity = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			logger.log("SELECT OPERATOR_CODE FROM OPERATOR_CODE_VALIDATOR_TABLE where OPERATOR_CODE = "
					+ "'" + operator_code + "'");
			rset = stmt
					.executeQuery("SELECT OPERATOR_CODE FROM OPERATOR_CODE_VALIDATOR_TABLE");
			while (rset.next()) {
                operator_code_from_table = rset.getString(OPERATOR_CODE);
				logger.log("OPERATOR CODE FOUND IN TABLE " + operator_code_from_table);
				if(operator_code.equalsIgnoreCase(operator_code_from_table)){
					logger.log("the operator code match found in table");
                    validity = YES;
				}
				
			}
			if(validity == null){
                validity = NO;
			}
		} catch (SQLException se) {
            logger.log("SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
            logger.log("Stack Trace: " + se.toString());
            logger.log("An exception occured while conecting to the DB.");
		} finally {
			if (rset != null) {
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
		return validity;

	}

	public void ejbActivate() throws RemoteException, EJBException {
	}

	public void ejbPassivate() throws RemoteException, EJBException {
	}

	public void ejbRemove() throws RemoteException, EJBException {
	}

    public void setSessionContext(final SessionContext context) throws RemoteException, EJBException {
		this.ctx = context;
	}

	public void ejbCreate() throws CreateException, EJBException,
			RemoteException {
	}
}
