package com.sigma.samp.imp.ziggo.jmf_bean.OperatorCodeValidatorBean;

//=======================================================================
//FILE INFO
//$Id: OperatorCodeValidatorConstant.java,v 1.1 2015/05/07 17:32:46 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class OperatorCodeValidatorConstant {

    public static final String CONTEXT_PARM_SUBSVCMODEL = "svc";
    public static final String CONTEXT_PARM_SUBMODEL    = "sub";
    public static final String YES                      = "Y";
    public static final String NO                       = "N";
    public static final String OPERATOR_CODE            = "operator_code";
    public static final String OPERATOR_CODE_VALIDATOR  = "operator_code_validator";

}
