package com.sigma.samp.imp.ziggo.jmf_bean.SIQOSFlavoursDataBean;
import static com.sigma.samp.imp.ziggo.jmf_bean.SIQOSFlavoursDataBean.SIQOSFlavoursDataBeanConst.*;
/* $Id SIQOSFlavoursDataBean.java ,v 1.0 2014/11/18 16:19:30 Nidhin $ initial version $ */

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.vframe.submodel.SubModel;
import com.sigma.vframe.jcmn.ParmEvalContext;

import com.sigma.vframe.util.ResUtil;
import java.sql.*;



public class SIQOSFlavoursDataBean implements SessionBean {

	private static Log logger = new Log(SIQOSFlavoursDataBean.class.getName());
	private SessionContext ctx;
	SampSubSvcValue subSvc = null;

	/***
	 * 
	 * @param expression
	 * @param inputLst
	 * @param evalContext
	 * @return expression to be used in xml according to the context
	 * @throws IllegalArgumentException
	 * @throws RemoteException
	 */
	public String eval(final String expression, final Map inputLst,final ParmEvalContext evalContext) throws IllegalArgumentException,RemoteException {
		logger.trace(3,"In Eval Method");
		logger.trace(3, "Version 1.0 SIQOSFlavoursDataBean : "+ expression);
		String result = null;
		String QosFlavor=null;
		String combination_of_QOS = null;
		String model = null;
		String manufacture = null;
		String accessType = null;
		String filter = null;
		try {
			final StringTokenizer strToken = new StringTokenizer(expression, ".", false);
			final ArrayList keyLst = new ArrayList();
			while (strToken.hasMoreTokens()) {
				keyLst.add(strToken.nextToken());
			}
			final int size = keyLst.size();
			logger.trace(3, " Size : " + size);
			if (size == 1) {
				accessType = (String) keyLst.get(size - 1);
				logger.trace(3, "access type ==" + accessType);
			}
			subSvc = (SampSubSvcValue) evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
			if (subSvc == null) {
				subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
			}
			logger.trace(5, "context subsvc: " + subSvc);
			final SubModel sub = (SubModel) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
			//This code is added to get value of flavor
			if (accessType.equalsIgnoreCase(SUNRISE_QOS_FLAVOR)) {
				if (subSvc.getSvcNm().equals(HRZ_INTERNET_ACCESS)|| subSvc.getSvcNm().equals(SMP_HRZ_INTERNET_ACCESS)) {
					
					
						combination_of_QOS = subSvc.getParmVal(CLASS_OF_SERVICE);
						logger.trace(5, "combination of qos : "+ combination_of_QOS);
					
					SampSubSvcValue dataPortSvc = null;
					dataPortSvc = getZEndSvcFrmAEndSvc(sub, subSvc,HRZ_INTERNET_HAS_HRZ_STB);
					if(dataPortSvc !=null)
					{
						logger.trace(5, "data port svc : " + dataPortSvc);
						final Iterator itr1 = dataPortSvc.getParentSubSvc().getChildSubSvcLst().iterator();
						while (itr1.hasNext()) {
							final SampSubSvcValue svc = (SampSubSvcValue) itr1.next();
							logger.trace(5, "Child svc: " + svc.getSvcNm());
							if ((svc.getSvcNm().equals(HRZ_STB_DEVICE_CONTROL)) && (svc.getProvisionableSvcNm().equals(SMP_HZR_DEVICE_CONTROL))) {
							    logger.trace(5,"Found hrz stb device control or smp hrz device control");
								model = svc.getParmVal(MODEL);
								manufacture = svc.getParmVal(MANUFACTURER);
								logger.trace(3, "Model = " + model + "and Manufacture = "+manufacture);
								break;
							}
						}
							QosFlavor = getQOSFlavours(combination_of_QOS, model, manufacture);
							logger.trace(3, "Result of QosFlavor = " + QosFlavor);
							if (QosFlavor == null) {
								logger.trace(5,"QosFlavor not found");
								result="Flavor Not Found";
							} else {	
								logger.trace(5,"QosFlavor is not null");
								result=QosFlavor;
							}
					} else {
						result="";
						logger.trace(5,"else added for deactivation problem fix and result is ="+result);
					}
					
				}
				logger.trace(5,"After matching service name");
				return result;
			}
			//This code is added to get value of filter 
			else if (accessType.equalsIgnoreCase(SUNRISE_FETCHING_FILTER)) {
				SampSubSvcValue sampSub = null;
				SampSubSvcValue svc = null;
				logger.trace(5, "Total sub service" + sub.getSubSvcs().size());
				final Iterator itr = sub.getSubSvcs().iterator();
				while (itr.hasNext()) {
					sampSub = (SampSubSvcValue) itr.next();
					logger.trace(5, "Child svc: " + sampSub.getSvcNm());
					if (sampSub.getSvcNm().equals(HRZ_INTERNET_ACCESS) && sampSub.getProvisionableSvcNm().equals(SMP_HRZ_INTERNET_ACCESS)) {
						
							
							combination_of_QOS = sampSub.getParmVal(CLASS_OF_SERVICE);
							logger.trace(5, "Combination of qos : "+ combination_of_QOS);
						
						SampSubSvcValue dataPortSvc = null;
					    dataPortSvc = getZEndSvcFrmAEndSvc(sub, sampSub,HRZ_INTERNET_HAS_HRZ_STB);
						if(dataPortSvc !=null)
						{
							logger.trace(5, "data port svc : " + dataPortSvc);
							final Iterator itr1 = dataPortSvc.getParentSubSvc().getChildSubSvcLst().iterator();
							while (itr1.hasNext()) {
								svc = (SampSubSvcValue) itr1.next();
								logger.trace(5, "Child svc: " + svc.getSvcNm());
								if ((svc.getSvcNm().equals(HRZ_STB_DEVICE_CONTROL)) && (svc.getProvisionableSvcNm().equals(SMP_HZR_DEVICE_CONTROL))) {
								    logger.trace(5,"Found hrz stb device control or smp hrz device control");
									model = svc.getParmVal(MODEL);
									manufacture = svc.getParmVal(MANUFACTURER);
									logger.trace(3, "Model = " + model + "and Manufacture = "+manufacture);
									break;
								}
							}
								filter = getFilter(combination_of_QOS, model, manufacture);
								logger.trace(3, "Result of filter = " + filter);
								if (filter == null) {
									logger.trace(5,"filter not found");
									result="filter Not Found";
								} else {	
									logger.trace(5,"filter is not null");
									result=filter;
								}
						} else {
							logger.trace(5,"else added for deactivation problem fix and result is ="+result);
						}
					} 
				}
				logger.trace(6,"No Internet Service Found");
				return result;
			}

			else {
				result = "Please Check Your Expression";
				return result;
			}
		} catch (Exception ex) {
			final String errMsg = "Unsupported Expression : "+expression;
			logger.log(errMsg+"\n"+ex.getMessage());
			throw new IllegalArgumentException(errMsg);
		}
	}
	
	/***
	 * This method will find out ZEnd from AEnd Service
	 * @param sub subscriber
	 * @param subSvc svc A
	 * @param assocName
	 * @return svc Z
	 */
	
	private static SampSubSvcValue getZEndSvcFrmAEndSvc(final SampSubValue sub,final SampSubSvcValue subSvc,final String assocName) {
		logger.trace(5,"Getzend function called");
		SampSubSvcValue zEndSvc = null;
		logger.trace(3, "SubSvc name = " + subSvc.getSvcNm()+ " current is A End Service");
		final Collection coll = subSvc.getEntityAssociationsByType(assocName);
		logger.trace(5, "collection = " + coll);
		if (coll != null) {
			logger.trace(3, "Size = " + coll.size() + "  for association type = "+ assocName);
			for (final Iterator itr = coll.iterator(); itr.hasNext();) {
				logger.trace(5,"in side for in zend function");
				final SampSubEntityAssoc assoc = (SampSubEntityAssoc) itr.next();
				zEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc.getZEndEntityKey());
				return zEndSvc;
			}
		}
		return zEndSvc;
	}
	
	/***
	 * This method will return flavor as per the combination of COS, model and manufacture
	 * @param qos
	 * @param model
	 * @param manufacture
	 * @return flavor per combination of COS
	 * @throws RemoteException
	 */

	private String getQOSFlavours(final String qos,final String model,final String manufacture)
	throws RemoteException {
		logger.trace(5,"In side flavors function");
		String flavor = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		logger.trace(5, "qos = "+qos + " model = "+model+" manufacturer = "+manufacture);
		try {
			
			if (model == null || manufacture==null || qos == null){
				flavor = "Flavor not Found";
			}
			else {
			stmt = conn.createStatement();
			rset = stmt
			.executeQuery("SELECT QOS_FLAVOR_NAME FROM CUST_BAC_MODEM_MFG bmm, CUST_ALLOWED_MODEM_PROFILE amp, CUST_DIN_QOS dq, CUST_DIN_QOS_FLAVOR dqf WHERE bmm.MFG_ID=amp.MFG_ID and amp.DIN_QOS_ID=dq.DIN_QOS_ID and dq.DIN_QOS_FLV_ID=dqf.DIN_QOS_FLV_ID and bmm.COS_MANUFACTURER="
					+ "'"
					+ manufacture
					+ "'"
					+ " and bmm.COS_MODEL= "
					+ "'"
					+ model
					+ "'"
					+ " and dq.QOS_NAME= "
					+ "'"
					+ qos + "'");
			while (rset.next()) {
				flavor = rset.getString("QOS_FLAVOR_NAME");
				logger.trace(5,"got the flavor" + flavor);
				
			}
			}
			
		} catch (SQLException se) {
			
			logger.trace(6,"SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		}finally {
			if(rset != null){
				ResUtil.closeDBResource(rset);
			}
			if(stmt != null){
				ResUtil.closeDBResource(stmt);
			}ResUtil.closeConnection(conn);
		}
		return flavor;
	}
	/***
	 * This method will return filter as per the combination of COS, model and manufacture
	 * @param qos
	 * @param model
	 * @param manufacture
	 * @return
	 * @throws RemoteException
	 */
	private String getFilter(final String qos, final String model,final String manufacture)
	throws RemoteException {
		logger.trace(5,"In side Filter function");
		String filter = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		logger.trace(5, "qos = "+qos + " model = "+model+" manufacturer = "+manufacture);
		try {
			if (model == null || manufacture==null || qos == null){
				filter = "Filter not Found";
			}
			else {
			stmt = conn.createStatement();
			rset = stmt
			.executeQuery("SELECT FILTER FROM CUST_BAC_MODEM_MFG bmm, CUST_ALLOWED_MODEM_PROFILE amp, CUST_DIN_QOS dq, CUST_DIN_QOS_FLAVOR dqf WHERE bmm.MFG_ID=amp.MFG_ID and amp.DIN_QOS_ID=dq.DIN_QOS_ID and dq.DIN_QOS_FLV_ID=dqf.DIN_QOS_FLV_ID and bmm.COS_MANUFACTURER="
					+ "'"
					+ manufacture
					+ "'"
					+ " and bmm.COS_MODEL= "
					+ "'"
					+ model
					+ "'"
					+ " and dq.QOS_NAME= "
					+ "'"
					+ qos + "'");
			while (rset.next()) {
				filter = rset.getString("FILTER");
				logger.trace(5,"got the filter" + filter);
			}
			}
		} catch (SQLException se) {
			logger.trace(6,"SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		}finally {
			if(rset != null){
				ResUtil.closeDBResource(rset);
			}
			if(stmt != null){
				ResUtil.closeDBResource(stmt);
			}ResUtil.closeConnection(conn);
		}
		return filter;
	}

	public void ejbActivate() throws RemoteException, EJBException {
	}
	public void ejbPassivate() throws RemoteException, EJBException {
	}
	public void ejbRemove() throws RemoteException, EJBException {
	}
	public void setSessionContext(final SessionContext context)
	throws RemoteException, EJBException {
		this.ctx = context;
	}
	public void ejbCreate() throws CreateException, EJBException,
	RemoteException {
	}
}
