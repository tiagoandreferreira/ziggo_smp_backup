package com.sigma.samp.imp.ziggo.jmf_bean.SIQOSFlavoursDataBean;

//=======================================================================
//FILE INFO
//$Id: VMailConst.java,v 1.2 2015/05/08 15:56:46 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class SIQOSFlavoursDataBeanConst {

    public static final String CONTEXT_PARM_SUBSVCMODEL = "svc";
    public static final String CONTEXT_PARM_SUBMODEL = "sub";
    public static final String SMP_HRZ_INTERNET_ACCESS = "smp_hrz_internet_access";
    public static final String HRZ_INTERNET_ACCESS = "hrz_internet_access";
    public static final String CLASS_OF_SERVICE = "class_of_service";
    public static final String HRZ_STB_DEVICE_CONTROL = "hrz_stb_device_control";
    public static final String MODEL = "model";
    public static final String MANUFACTURER = "manufacturer";
    public static final String HRZ_INTERNET_HAS_HRZ_STB = "hrz_internet_has_hrz_stb";
    public static final String SMP_HZR_DEVICE_CONTROL = "smp_hrz_device_control";
    public static final String SUNRISE_QOS_FLAVOR       = "sunrise_qos_flavor";
    public static final String SUNRISE_FETCHING_FILTER= "sunrise_fetching_filter";


}
