package com.sigma.samp.imp.ziggo.jmf_bean.VMailDataBean;

//=======================================================================
//FILE INFO
//$Id: VMailConst.java,v 1.2 2015/05/08 15:56:46 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class VMailConst {

    public static final String GROUP                             = "group";
    public static final String DEVICE_NICKNAME                   = "device_nickname";
    public static final String CHECK_FORWARD_TO_NUMBER           = "check_forward_to_number";
    public static final String HIQ                               = "hiq";
    public static final String BROADSOFT                         = "broadsoft";
    public static final String QUERY_SS_PORT_NUMBER              = "query_ss_port_number";
    public static final String PORT_NUMBER                       = "port_number";
    public static final String EVENT_BASED_FEATURE_TELEPHONE_CHK = "event_based_feature_telephone_chk";
    public static final String HIQ_GATEWAY_NAME                  = "hiq_gateway_name";
    public static final String SMP_EMTA_COMPOSED                 = "smp_emta_composed";
    public static final String SW_CFV_ACTIVATED                  = "sw_cfv_activated";
    public static final String FORWARD_TO_NUMBER                 = "forward_to_number";
    public static final String SW_CFV_SUBSCRIBED                 = "sw_cfv_subscribed";
    public static final String SW_CFVM_SUBSCRIBED                = "sw_cfvm_subscribed";
    public static final String NUMBER_OF_RINGS                   = "number_of_rings";
    public static final String CHECK_MOVE                        = "check_move";
    public static final String SMP_SWITCH_DIAL_TONE_ACCESS       = "smp_switch_dial_tone_access";
    public static final String EVENT_BASED_FEATURE               = "event_based_feature";

    public static final String CONTEXT_PARM_SUBSVCMODEL          = "svc";
    public static final String CONTEXT_PARM_SUBMODEL             = "sub";
    public static final String SMP_VOICE_LINE                    = "smp_voice_line";
    public static final String TELEPHONE_NUMBER                  = "telephone_number";
    public static final String SMP_DIAL_TONE_ACCESS              = "smp_dial_tone_access";
    public static final String VOIP_DIAL_TONE                    = "voip_dial_tone";
    public static final String SUB_SVC_ID                        = "sub_svc_id";
    public static final String CALL_SERVER                       = "call_server";
    public static final String DT_HAS_EQUIPMENT                  = "dt_has_equipment";
    public static final String VOIP_HAS_SIM_RING                 = "voip_has_sim_ring";
    public static final String SWITCH_TYPE                       = "switch_type";
    public static final String SMP_RESI_DEVICE_CONTROL           = "smp_resi_device_control";
    public static final String EMTA_DEVICE_CONTROL               = "emta_device_control";
    public static final String SMP_SWITCH_FEATURE                = "smp_switch_feature";
    public static final String SMP_SWITCH_FEATURE_PKG_STD        = "smp_switch_feature_pkg_std";
    public static final String SMP_EVENT_SWITCH_FEATURE_PACKAGE  = "smp_event_switch_feature_package";
    public static final String CHECK_SIMRING                     = "check_simring";
    public static final String MOVE_TO                           = "move_to";
    public static final String EVENT_BASED_OR_PORT_NUMBER        = "event_based_or_port_number";
    public static final String YES                               = "y";
    public static final String NULLFORWARDNUMBER                 = "0000000000";

}
