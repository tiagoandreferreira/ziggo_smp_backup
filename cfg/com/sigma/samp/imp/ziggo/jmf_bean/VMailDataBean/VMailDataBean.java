package com.sigma.samp.imp.ziggo.jmf_bean.VMailDataBean;


/* $Id VMailDataBean.java ,v 1.0 2013/05/16 16:19:30 Prakash $ initial version $
   $Id VMailDataBean.java ,v 1.1 2013/07/01 17:18:18 Prakash $ check_status functionality added $
   $Id VMailDataBean.java ,v 1.2 2013/07/04 12:1:17 Prakash $ check_status functionality modified $
   $Id VMailDataBean.java ,v 1.3 2013/07/12 10:20:08 Prakash $ code added to handle broadsoft subnetwork $
   $Id VMailDataBean.java ,v 1.4 2013/07/18 07:25:00 Prakash $ event_based_feature functionality added to get port number in case of BS $
   $Id VMailDataBean.java ,v 1.5 2013/08/06 15:35:01 Prakash $ event_based_feature functionality modify to check that telephone number from request is same with the telephone number of voice line $
   $Id VMailDataBean.java ,v 1.6 2013/08/21 16:05:31 Prakash $ check_status functionality modified, If both VL is active then return 2.This specific change is made for change RDU(Amit gupta) $
   $Id VMailDataBean.java ,v 1.7 2013/08/28 13:09:27 Prakash $ code changed in getExtractedSubNetWork function, event_based_feature_telephone_chk functionality added $
   $Id VMailDataBean.java ,v 1.8 2013/09/12 19:21:25 Prakash $ group and event_based_feature_telephone_chk added functionality modified, getExtractedSubNetWork and getIfChildServiceTelephone method removed $
   $Id VMailDataBean.java ,v 1.9 2013/10/04 20:40:21 Prakash $ check_status functionality modified, return null if no voice line is present on subscriber or more than 2 voice line is present $
   $Id VMailDataBean.java ,v 1.10 2013/10/31 08:50:40 Prakash $ check_simring functionality added, check_status functionality removed $
   $Id VMailDataBean.java ,v 1.11 2013/12/19 10:51:20 Prakash $ query_ss_port_number functionality added $
   $Id VMailDataBean.java ,v 1.12 2014/01/27 06:56:13 Prakash $ code added to throw sync error while change feature,code added to pick correct voice line while adding event based service $
   $Id VMailDataBean.java ,v 1.13 2014/05/09 16:01:3 Yashaswini/Prakash $ check_move functionality added, To throw sync error while moving voice line on same switch $
   $Id VMailDataBean.java ,v 1.14 2014/07/10 14:19:13 Prakash $ code added to pick number of ring from voice line while adding event based service $
*/

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.sigma.hframe.jerror.SmpResourceException;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.stm.cmn.Subnetwork;
import com.sigma.samp.stm.ssn.SSNAgent;
import com.sigma.samp.stm.ssn.SSNAgentHome;
import com.sigma.samp.stm.ssn.SSNServiceLocator;
import com.sigma.samp.vframe.utils.ResUtil;
import com.sigma.vframe.jcmn.ParmEvalContext;
import static com.sigma.samp.imp.ziggo.jmf_bean.VMailDataBean.VMailConst.*;

public class VMailDataBean implements SessionBean {
	private static Log logger = new Log(VMailDataBean.class.getName());
	private SessionContext ctx;

    /**
     * Eval Method
     * 
     * @param expression
     * @param inputLst
     * @param evalContext
     * @return expression to be used in xml according to the context
     * @throws IllegalArgumentException
     * @throws RemoteException
     */
    public String eval(final String expression, final Map inputLst, final ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException {
		try {
			
			String accessType = null;
			String subNetwork = null;
			String groupID = null;
            String simring_str = "null";
            logger.trace(5, "In Eval Method");
			logger.trace(5, "In VMailDataBean.eval()\n Expression received: "+ expression);
			final StringTokenizer strToken = new StringTokenizer(expression, ".", false);
			final ArrayList keyLst = new ArrayList();
			while (strToken.hasMoreTokens()) {
				keyLst.add(strToken.nextToken());
			}
			final int size = keyLst.size();
			logger.trace(3, " Size : " + size);
			if (size == 1) {
				accessType = (String) keyLst.get(size - 1);
				logger.trace(5, "access type : " + accessType);
			}
			logger.trace(3,"Version 1.14");
			SampSubSvcValue subSvc = (SampSubSvcValue) evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
			if (subSvc == null) {
				subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
			}
			logger.trace(5, "context subsvc : " + subSvc);
			final SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
			logger.trace(6, "context sub : " + sub);
			logger.trace(5, "Total sub service" + sub.getSubSvcs().size());
			final Iterator itr = sub.getSubSvcs().iterator();
			//code is added to get group_id of respective subnetwork
            if (accessType.equalsIgnoreCase(GROUP)) {
				final String telephone_number = subSvc.getParmVal(TELEPHONE_NUMBER);
				logger.trace(5, "Telephone Number from Voice Mail Box: "+ telephone_number);
				SampSubSvcValue service = null;
				SampSubSvcValue svc = null;
                String subnetwork=null;
				while (itr.hasNext()) {
                    logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itr.next();
                    if (service.getSvcNm().equalsIgnoreCase(SMP_VOICE_LINE)) {
                        logger.trace(5, "After Voice Service Match Found");
                        svc = getIfChildService(service, telephone_number);
						if (svc != null) {
                            logger.trace(5, "svc not null");
                            subnetwork = getSubNetwork(svc);
                            logger.trace(5, "subnet work : " + subnetwork);
                            groupID = getGroupID(subnetwork);
							logger.trace(5, "Group Id From DB : " + groupID);
							break;
						} else {
                            logger.trace(5, "svc is null");
						}
					} else {
                        logger.trace(5, "Found the following service: " + service.getSvcNm());
					}
				}
				if(groupID == null){
					groupID="Telephone Number does not match with the telephone number of voice line";
					throw new SyncErrorException(groupID);
				}
				return groupID;
			} 		
            // code added to check whether Voice line is provisioned on HiQ/BS while adding Simring
            // functionality.
			else if (accessType.equalsIgnoreCase(CHECK_SIMRING)) {
                SampSubSvcValue dailtoneSvc = null;
				String switchType = null;
                if (subSvc.getSvcNm().equals(VOIP_DIAL_TONE)
                        && subSvc.getProvisionableSvcNm().equals(SMP_DIAL_TONE_ACCESS)) {
                    dailtoneSvc = getZEndSvcFrmAEndSvc(sub, subSvc, VOIP_HAS_SIM_RING);
                    if (dailtoneSvc != null) {
                        switchType = dailtoneSvc.getParmVal(SWITCH_TYPE);
						logger.trace(5,"switch type is = "+switchType);
						if (switchType != null && switchType.equalsIgnoreCase(HIQ)) {
                            simring_str = "hiq";
                        }else if (switchType != null && switchType.equalsIgnoreCase(BROADSOFT))                        {
                            simring_str = "broadsoft";
                        }
                        else
                        {
                            simring_str = "false";
                        }
                    }
                }
                return simring_str;
			}
            // code added to get port number while executing event based query or event based
            // feature change.This code is used in BS TI(query_secnario) parm sourcing.
            else if (accessType.equalsIgnoreCase(EVENT_BASED_FEATURE) || accessType.equalsIgnoreCase(QUERY_SS_PORT_NUMBER)) {
				final String telephone_number = subSvc.getParmVal(TELEPHONE_NUMBER);
				logger.trace(5, "Telephone Number from event based service or query_ss: "+ telephone_number);
				SampSubSvcValue service = null;
				SampSubSvcValue svc = null;
                String portnumber = null;
				while (itr.hasNext()) {
                    logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itr.next();
                    if (service.getSvcNm().equalsIgnoreCase(SMP_VOICE_LINE)) {
                        logger.trace(5, "After Voice Service Match Found");
                        svc = getIfChildService(service, telephone_number);
						if (svc != null) {
                            logger.trace(5, "svc is not null");
                            SampSubSvcValue voicePortSvc = null;
                            voicePortSvc = getZEndSvcFrmAEndSvc(sub, svc, DT_HAS_EQUIPMENT);
                            portnumber = voicePortSvc.getParmVal(PORT_NUMBER);
                            logger.trace(5, "Port Number is : " + portnumber);
							break;
						} else {
                            logger.trace(5, "svc is null");
						}
					} else {
                        logger.trace(5, "Found the following service: " + service.getSvcNm());
					}
				}
                if (portnumber == null) {
                    portnumber = "Voice line is not provisioned or "
                            + "telephone Number does not match with the telephone number of voice line";
                    logger.log("Inside SyncErrorException");
                    throw new SyncErrorException(portnumber);
				}
                return portnumber;
			}
            // code added to check that telephone number from Tibco Request is same with the
            // telephone number of provisioned Voice line
            else if (accessType.equalsIgnoreCase(EVENT_BASED_FEATURE_TELEPHONE_CHK)) {
				final String telephone_number = subSvc.getParmVal(TELEPHONE_NUMBER);
				logger.trace(5, "Telephone Number from event based service telephone_chk: "	+ telephone_number);
				SampSubSvcValue service = null;
				SampSubSvcValue svc = null;
                String exception = null;
				while (itr.hasNext()) {
                    logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itr.next();
                    if (service.getSvcNm().equalsIgnoreCase(SMP_VOICE_LINE)) {
                        logger.trace(5, "After Voice Service Match Found");
                        svc = getIfChildService(service, telephone_number);
						if (svc != null) {
                            logger.trace(5, "svc is not null");
                            SampSubSvcValue voicePortSvc = null;
                            voicePortSvc = getZEndSvcFrmAEndSvc(sub, svc, DT_HAS_EQUIPMENT);
                            if (voicePortSvc != null) {
								logger.trace(5,"svc is provisioned");
								exception=telephone_number;
                                logger.trace(5, "Telephone number is : " + telephone_number);
								break;
							} 
						} else {
                            logger.trace(5, "svc is null");
						}
					} else {
                        logger.trace(5, "Found the following service: " + service.getSvcNm());
					}
				}
				if(exception == null) {
				    exception="Voice line is not provisioned or telephone Number does not match with the telephone number of voice line";
				    logger.log("Inside SyncErrorException");
					throw new SyncErrorException(exception);
				} 
                return telephone_number;
			}
			//code added to retrieve device nickname. This name is used in HiQ.
            else if (accessType.equalsIgnoreCase(HIQ_GATEWAY_NAME)) {
				SampSubSvcValue service = null;
				SampSubSvcValue svc = null;
                String device_nickname = null;
				while (itr.hasNext()) {
                    logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itr.next();
                    if (service.getSvcNm().equalsIgnoreCase(SMP_EMTA_COMPOSED)) {
                        logger.trace(5, "After Emta Service Match Found");
						svc = getChildService(service);
						if (svc != null) {
                            logger.trace(5, "svc is not null");
                            device_nickname = svc.getParmVal(DEVICE_NICKNAME);
                            logger.trace(5, "device nick name is : " + device_nickname);
							break;
						} else {
                            logger.trace(5, "svc is null");
						}
					} else {
                        logger.trace(5, "Found the following service: " + service.getSvcNm());
					}
				}
                if (device_nickname == null) {
                    device_nickname = "Mta Name Not Present";
				} 
                return device_nickname;
				
			}
			//code added to check forward number is provided while cfv activated is set to Y/y. 
            else if (accessType.equalsIgnoreCase(CHECK_FORWARD_TO_NUMBER)) {
                String forwardToNumber = null;
				if(subSvc.getSvcNm().equals(SMP_EVENT_SWITCH_FEATURE_PACKAGE)){
                    String cfvActivated = subSvc.getParmVal(SW_CFV_ACTIVATED);
                    forwardToNumber = subSvc.getParmVal(FORWARD_TO_NUMBER);
                    logger.trace(5, "Cfv activated == " + cfvActivated + " Forward to number == "
                            + forwardToNumber);
					if(cfvActivated == null){
                        logger.trace(3, "Cfv Activated is null and forward number is == " + forwardToNumber);
					} else {
						logger.trace(3,"Cfv activated is not null");
						cfvActivated=cfvActivated.toUpperCase().toString();
						if(forwardToNumber == null){
							logger.trace(3,"forward to number is null");
						} else {
							logger.trace(3,"forward to number is not null");
							forwardToNumber=forwardToNumber.toUpperCase().toString();
						}
                        if (cfvActivated.equalsIgnoreCase(YES)) {
							if(forwardToNumber == null) {
                                forwardToNumber = "Forward To Number is required to activate call"
                                        + " forwarding variable service";
								logger.log("Throw error forward to number null");
                                throw new SyncErrorException(forwardToNumber);
                            } else if (forwardToNumber.equals(NULLFORWARDNUMBER)) {
                                forwardToNumber = "Forward To Number is required to activate call"
                                        + " forwarding variable service";
								logger.log("default value for forward to number");
                                throw new SyncErrorException(forwardToNumber);
							} else	{
                                logger.trace(3, "Forward number == " + forwardToNumber);
							}
						} else {

                            logger.trace(3, "Cfv Activated is N and Result is == " + forwardToNumber);
						}
					}
				}
                return forwardToNumber;
			}
            // code added to pick perfect value of event based parm from respective voice line while
            // changing event based feature.
            // code added to pick perfect value of number of rings from respective voice line while
            // changing event based feature.
            else if (accessType.equalsIgnoreCase(SW_CFV_SUBSCRIBED)
                    || accessType.equalsIgnoreCase(SW_CFVM_SUBSCRIBED)
                    || accessType.equalsIgnoreCase(NUMBER_OF_RINGS)) {
				final String telephone_number = subSvc.getParmVal(TELEPHONE_NUMBER);
				logger.trace(5, "Telephone Number from event based service : "	+ telephone_number);
				SampSubSvcValue service = null;
				SampSubSvcValue svc = null;
                String getFeatureParmVal = null;
				while (itr.hasNext()) {
                    logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itr.next();
                    if (service.getSvcNm().equalsIgnoreCase(SMP_VOICE_LINE)) {
                        logger.trace(5, "After Voice Service Match Found");
                        svc = getIfChildService(service, telephone_number);
						if (svc != null) {
                            logger.trace(5, "svc is not null");
							svc=getChildService(service);
							if(svc != null) {
                                if (accessType.equalsIgnoreCase(SW_CFV_SUBSCRIBED)) {
                                    getFeatureParmVal = svc.getParmVal(SW_CFV_SUBSCRIBED);
                                    logger.trace(5, "Value of cfv subscribed = " + getFeatureParmVal);
									break;
                                } else if (accessType.equalsIgnoreCase(SW_CFVM_SUBSCRIBED)) {
                                    getFeatureParmVal = svc.getParmVal(SW_CFVM_SUBSCRIBED);
                                    logger.trace(5, "Value of cfvm subscribed = " + getFeatureParmVal);
									break;
                                } else if (accessType.equalsIgnoreCase(NUMBER_OF_RINGS)) {
                                    getFeatureParmVal = svc.getParmVal(NUMBER_OF_RINGS);
                                    logger.trace(5, "Value of number of rings = " + getFeatureParmVal);
									break;
								}
							} else {
                                logger.trace(5, "svc is null");
							}
						} else {
                            logger.trace(5, "Not found required voice line");
						}
					} else {
                        logger.trace(5, "Found the following service: " + service.getSvcNm());
					}
				}
                return getFeatureParmVal;
			}
			//code added to restrict move of voice line on same switch
            else if (accessType.equalsIgnoreCase(CHECK_MOVE)) {
                String moveTo = subSvc.getParmVal(MOVE_TO);
				logger.trace(5, "Move To Switch : "+ moveTo );
				SampSubSvcValue service = null;
				SampSubSvcValue svc = null;

				while (itr.hasNext()) {
                    logger.trace(5, "In sub service iterators");
					service = (SampSubSvcValue) itr.next();
                    if (service.getSvcNm().equalsIgnoreCase(SMP_VOICE_LINE)) {
                        logger.trace(5, "After Voice Service Match Found");
                        svc = getIfChildServiceMoveTo(service, moveTo);
						if (svc != null) {
                            moveTo = "Line cannot be moved to the same switch";
                            logger.log(moveTo);
                            throw new SyncErrorException(moveTo);
					       
						} else {
                            logger.trace(5, "svc is null");
						}
					} else {
                        logger.trace(5, "Found the following service: " + service.getSvcNm());
					}
				}
                return moveTo;
            } else {
				subNetwork = "Please Check Your Expression";
				logger.log(subNetwork);
				throw new IllegalArgumentException(subNetwork);
			}
		} catch (Exception e) {
            logger.log("Exception caught: " + e.getMessage());
			throw new RemoteException("Exception caught: ", e);
		}
	}

    public class SyncErrorException extends Exception {
		final private String msg;

        /**
         * This method is added to throw error in sync.
         * 
         * @param msg
         */
		public SyncErrorException(final String msg) {
			this.msg = msg;
		}
		public String toString() {
			logger.trace(5, "Inside Sync Error Exception");
			return msg;
		}
	}


    /***
     * This method is used to find out proper voice line as per the telephonenumber
     * @param service
     * @param telephoneNumber
     * @return
     */
  	private SampSubSvcValue getIfChildService(final SampSubSvcValue service,final String telephoneNumber) {
  		logger.log("In Child Service");
  		final Iterator itr = service.getChildSubSvcLst().iterator();
  		logger.trace(5, "size : " + service.getChildSubSvcLst().size());
  		SampSubSvcValue subSvc = null;
  		SampSubSvcValue svc = null;
  		while (itr.hasNext()) {
  			subSvc = (SampSubSvcValue) itr.next();
  			logger.trace(5, "while subSvc : " + subSvc.getSvcNm()+ "subSvc.getSubSvcKey()  " + subSvc.getSubSvcKey());
  			if (((subSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) || (subSvc.getSvcNm().equalsIgnoreCase("SMP_SWITCH_DIAL_TONE_ACCESS"))) && subSvc.getParmVal(TELEPHONE_NUMBER) != null) {
  				logger.log("telephone number is not null and inside if");
  				final String telNumber = subSvc.getParmVal(TELEPHONE_NUMBER);
  				logger.trace(5, "telephone number from smp_voice_line : " + telNumber);
  				if (telNumber.equalsIgnoreCase(telephoneNumber)) {
  					logger.log("both telephone number is same");
  					svc = subSvc;
  					return svc;
  				}
  			}
  		}
  		return svc;
  	}
  	


  	/***
  	 * This method is used in check_move functionality
  	 * @param service
  	 * @param moveTo
  	 * @return
  	 */
  	private SampSubSvcValue getIfChildServiceMoveTo(final SampSubSvcValue service,final String moveTo) {
  		logger.log("In Child Service");
  		final Iterator itr = service.getChildSubSvcLst().iterator();
  		logger.trace(5, "size : " + service.getChildSubSvcLst().size());
  		SampSubSvcValue subSvc = null;
  		SampSubSvcValue svc = null;
  		while (itr.hasNext()) {
  			subSvc = (SampSubSvcValue) itr.next();
  			logger.trace(5, "while subSvc : " + subSvc.getSvcNm()+ "subSvc.getSubSvcKey()  " + subSvc.getSubSvcKey());
  			if (((subSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) || (subSvc.getSvcNm().equalsIgnoreCase("SMP_SWITCH_DIAL_TONE_ACCESS"))) && subSvc.getParmVal(SWITCH_TYPE) != null) {
  				logger.log("Switch_Type parm is not null and inside if");
  				final String switchType = subSvc.getParmVal(SWITCH_TYPE);
  				logger.trace(5, "Switch_Type parameter in the inventory  : " + switchType);
  				if (switchType.equalsIgnoreCase(moveTo)) {                                                 
  					logger.log("value of the move_to parameter in the request and the Switch_Type parameter in the inventory are same ");
  					svc = subSvc;
  					return svc;
  				}
  			}
  		}
  		return svc;
  	}

    /**
     * This method will iterate service and return a child service
     * 
     * @param service
     * @return smp switch feature service
     */
	private SampSubSvcValue getChildService(final SampSubSvcValue service) {
        logger.trace(5, "In Child Service");
		final Iterator itr = service.getChildSubSvcLst().iterator();
		logger.trace(5, "size : " + service.getChildSubSvcLst().size());
		SampSubSvcValue subSvc = null;
		SampSubSvcValue svc = null;
		while (itr.hasNext()) {
			subSvc = (SampSubSvcValue) itr.next();
            logger.trace(5,"while subSvc : " + subSvc.getSvcNm() + "subSvc.getSubSvcKey()  " + subSvc.getSubSvcKey());
            if ((subSvc.getSvcNm().equalsIgnoreCase(SMP_RESI_DEVICE_CONTROL))
                    || (subSvc.getSvcNm().equalsIgnoreCase(EMTA_DEVICE_CONTROL))) {
                logger.trace(5, "found emta device control");
				svc = subSvc;
				break;
            } else if ((subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_FEATURE))
                    || (subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_FEATURE_PKG_STD))) {
                logger.trace(5, "found smp switch feature service");
				svc = subSvc;
				break;
			}
			}
		return svc;
	}

    /**
     * This method is used to get subnetwork
     * 
     * @param svc
     * @return subnetwork name
     */
    private String getSubNetwork(final SampSubSvcValue svc) {
        logger.trace(5, "In Find Subnetwork Function");
		String subNtWk = null;
		try {
			final SSNAgent ssnAgent = getSSNAgent();
            final Map subNtwkRoleMap = ssnAgent.getSnwsRoles(new BigDecimal(svc.getParmVal(SUB_SVC_ID)));

			for (final Iterator iterator = subNtwkRoleMap.entrySet().iterator(); iterator.hasNext();) {
				final Map.Entry entry = (Map.Entry) iterator.next();
				final String roleId = entry.getValue().toString();
                final Subnetwork subntwk = ssnAgent.getSnw(new BigDecimal(svc.getParmVal(SUB_SVC_ID)),
                        new BigDecimal(roleId));
				logger.trace(5,"getNetworkComponentSubtype="+ subntwk.getNetworkComponentSubtype());
				if (subntwk.getNetworkComponentSubtype().equals(CALL_SERVER)) {
					subNtWk = subntwk.getName();
                    logger.trace(5, "getSubnetworkType=" + subntwk.getSubnetworkType() + "getUniqueID="
                            + subntwk.getUniqueID());
					return subNtWk;
				}
			}
		} catch (ObjectNotFoundException onfe) {
            logger.log("STM doesn't have such ssn, skip");
		} catch (Exception ex) {
			logger.alarm(AlarmLevel.eError,	AlarmCategory.eAppl,"Fail to get ssn status for subSvc:"+ svc.getParmVal(SUB_SVC_ID), ex);
		}
		return subNtWk;
	}

    /**
     * This method is used to find ssn agent.
     * 
     * @return SSNAgent
     * @throws SmpResourceException
     */
	private SSNAgent getSSNAgent() throws SmpResourceException {
        logger.trace(5, "In ssn_agent Function");
		SSNAgent ssn_Agent = null;
		if (ssn_Agent == null) {
			try {
				final SSNAgentHome ssn_AgentHome = (SSNAgentHome) SSNServiceLocator.getInstance().getHome(SSNServiceLocator.Services.SSN_AGENT);
				// get remote interface
				ssn_Agent = ssn_AgentHome.create();
			} catch (Exception ex) {
				logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,"Failed finding SSNAgent " + ex, ex);
				throw new SmpResourceException(ex.getMessage(), ex);
			}
		}
		return ssn_Agent;
	}

    /**
     * This method will return group id from DB as per subnetwork
     * 
     * @param subnetwork
     * @return group ID
     * @throws RemoteException
     */
    private String getGroupID(final String subnetwork) throws RemoteException {
        logger.trace(5, "In get group id function");
		String groupID = null;
		ResultSet rset = null;
		Statement stmt = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt.executeQuery("select val from subntwk_parm where parm_id =(select parm_id from parm where parm_nm ='grp_id') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm = '"+subnetwork+"')");
			while (rset.next()) {
				groupID = rset.getString("val");
                logger.trace(5, "got group id" + groupID);
			}
		} catch (SQLException se) {
            logger.log("SQL Exception: " + se.getMessage());
			se.printStackTrace(System.out);
            logger.log("Stack Trace: " + se.toString());
            logger.log("An exception occured while conecting to the DB.");
		} finally {
            if(rset != null) {
            	           ResUtil.closeDBResource(rset);
            }
            if(stmt != null) {
            	           ResUtil.closeDBResource(stmt);
            }ResUtil.closeConnection(conn);
		}
		return groupID;
	}

    /**
     * This method will return the Z svc from a association with a the svc A
     * 
     * @param sub subscriber
     * @param subSvc svc A
     * @param assocName
     * @return svc Z
     */
    private static SampSubSvcValue getZEndSvcFrmAEndSvc(final SampSubValue sub, final SampSubSvcValue subSvc,
            final String assocName) {
        logger.trace(5, "in getzend function");
		SampSubSvcValue zEndSvc = null;
		logger.trace(3, "SubSvc name = " + subSvc.getSvcNm()+ " current is A End Service");
		final Collection col = subSvc.getEntityAssociationsByType(assocName);
		logger.trace(3, "collection = " + col);
		if (col != null) {
			logger.trace(3, "Size = " + col.size() + "  for association type = "+ assocName);
			for (final Iterator itr = col.iterator(); itr.hasNext();) {
                logger.trace(5, "in side for in zend function");
				final SampSubEntityAssoc assoc = (SampSubEntityAssoc) itr.next();
				zEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc.getZEndEntityKey());
				return zEndSvc;
			}
		}
		return zEndSvc;
	}
	public void ejbActivate() throws EJBException, RemoteException {
	}
	public void ejbPassivate() throws EJBException, RemoteException {
	}
	public void ejbRemove() throws EJBException, RemoteException {
	}
	public void setSessionContext(final SessionContext arg0) throws EJBException,	RemoteException {
		ctx = arg0;
	}
	public void ejbCreate() throws CreateException, EJBException,RemoteException {
	}
}
