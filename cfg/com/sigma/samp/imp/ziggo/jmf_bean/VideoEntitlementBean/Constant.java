/* $Id Constant.java ,v 1.0 2014/02/14 16:19:30 Prakash $ initial version $ */
package com.sigma.samp.imp.ziggo.jmf_bean.VideoEntitlementBean;

/**
 * All Service Definition Names used by the application, organised by top service, as follow:
 * <ol>
 * <li>Top or Main Service Name: SVC_VIDEO_SERVICES</li>
 * <li>Dependent Provisionable Service Name from top Service Name</li>
 * <li>Dependent Service Name from top Service Name</li>
 * <li>Dependent Service Name from top Service Name</li>
 * <li>Attributes organised by top Service Name: ATTR_</li>
 * <li>Parameters organised by top Service Name: PARM_</li>
 * <li>Values organised by top Service Name: VAL_</li>
 * <li>Association to top Service Name: ASSOC_</li>
 * <li>Suffix from top Service Name: *_SUFFIX</li>
 * <li>Format from top Service Name: *_FORMAT</li>
 * </ol>
 */

interface Constant {

    // /////////////////////////////////////////////////////////////////////////
    // Top or Main Service Name
    String SVC_VIDEO_SERVICES               = "video_services";

    String CONTEXT_PARM_SUBMODEL            = "sub";
    String CONTEXT_PARM_SUBSVCMODEL         = "svc";

    // /////////////////////////////////////////////////////////////////////////
    // Dependent Provisionable Service Name from SVC_VIDEO_SERVICES
    String PSVC_SMP_VIDEO_ENTITLEMENT       = "smp_video_entitlement";

    // /////////////////////////////////////////////////////////////////////////
    // Dependent Service Name from SVC_VIDEO_SERVICES
    String SVC_STB_CAS                      = "stb_cas";
    String SVC_VIDEO_ACCESS                 = "video_access";
    String SVC_VIDEO_DEFN_TOPOLOGY_OVERRIDE = "video_defn_topology_override";
    String SVC_VIDEO_SERVICE_PLAN           = "video_service_plan";

    // /////////////////////////////////////////////////////////////////////////
    // Attribute from SVC_VIDEO_SERVICES and sub-services

    // /////////////////////////////////////////////////////////////////////////
    // Parameter from SVC_VIDEO_SERVICES and sub-services
    String PARM_BEGIN_DATE                  = "begin_date";
    String PARM_END_DATE                    = "end_date";
    String PARM_PROVISION_STATUS            = "provision_status";
    String PARM_STATUS                      = "status";
    String PARM_TAPING_AUTHORIZATION        = "taping_authorization";

    // /////////////////////////////////////////////////////////////////////////
    // Association from SVC_VIDEO_SERVICES and sub-services
    String ASSOC_VIDEO_SERVICE_DEFN_HAS_STB = "video_service_defn_has_stb";

    // /////////////////////////////////////////////////////////////////////////
    // Format from SVC_VIDEO_SERVICES and sub-services
    String PROV_TAGS_CHG_DATE_FORMAT        = "yyyy.MM.dd HH:mm:ss S";

    // /////////////////////////////////////////////////////////////////////////
    // Enum from SVC_VIDEO_SERVICES and sub-services
    enum AccessType {
        BC("bc_video_prov_tags", "bc_video_prov_tags_chg_date"), //
        PPV("ppv_video_prov_tags", "ppv_video_prov_tags_chg_date"), //
        VOD("vod_prov_tags", "vod_prov_tags_chg_date");

        private final String parmTag;
        private final String parmDate;

        AccessType(String parmTag, String parmDate) {
            this.parmTag = parmTag;
            this.parmDate = parmDate;
        }

        String getParmTag() {
            return parmTag;
        }

        String getParmDate() {
            return parmDate;
        }
    }
}
