/* $Id VideoEntitlementBean.java ,v 1.0 2014/02/14 10:29:36 Prakash $ initial version $
   $Id VideoEntitlementBean.java ,v 1.1 2014/03/07 06:10:35 Prakash $ Code added to get current date of video subscription as per subscription_type parm $
   $Id VideoEntitlementBean.java ,v 1.2 2014/05/19 09:11:31 Prakash $ subscription_type parm removed from service so code changes are done accordingly $  */

package com.sigma.samp.imp.ziggo.jmf_bean.VideoEntitlementBean;

import static com.sigma.samp.imp.ziggo.jmf_bean.VideoEntitlementBean.Constant.CONTEXT_PARM_SUBMODEL;
import static com.sigma.samp.imp.ziggo.jmf_bean.VideoEntitlementBean.Constant.CONTEXT_PARM_SUBSVCMODEL;
import static com.sigma.samp.imp.ziggo.jmf_bean.VideoEntitlementBean.Constant.SVC_VIDEO_SERVICE_PLAN;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.vframe.jcmn.ParmEvalContext;

public class VideoEntitlementBean implements SessionBean {
	private static Log                    logger = new Log(VideoEntitlementBean.class.getName());
	private static final String           STATUS_DELETED = "deleted";
	private static final String           STATUS_INACTIVE = "inactive";
	private static final String           STATUS_ACTIVE = "active";
	private static final String           STATUS = "provision_status";
	private static final String			  VIDEO_SUBSCRIPTION = "video_subscription";
	private SessionContext                ctx;

	public void ejbActivate() throws EJBException, RemoteException {
	}
	public void ejbPassivate() throws EJBException, RemoteException {
	}
	public void ejbRemove() throws EJBException, RemoteException {
	}
	public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
		ctx = arg0;
	}
	public void ejbCreate() throws CreateException, EJBException, RemoteException {
	}
	public String eval(final String expression,final Map inputLst,final ParmEvalContext evalContext) throws IllegalArgumentException, RemoteException {
		final String result = null;
		logger.trace(5, "In Eval of VideoEntitlementBean");
		logger.log("Version 1.2");
		try {
			final SampSubSvcValue sssvVideoSvcPlan = getSvcVideoServicePlan(evalContext);
			logger.trace(5, "sssvVideoSvcPlan = "+sssvVideoSvcPlan);
			if (sssvVideoSvcPlan != null) {
				final String[] jmfParm = getJmfParm(expression);
				logger.log("Lenth : "+jmfParm.length);
				if (jmfParm.length != 0) {
					if (checkSubSvc(sssvVideoSvcPlan, jmfParm)) {
						return getCurrDateTime();
					}
				} else {
					throw new IllegalArgumentException("Invalid operation:" + jmfParm[0]);
				}
			} else {
				throw new IllegalArgumentException("invalid " + SVC_VIDEO_SERVICE_PLAN);
			}
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			logger.trace(3, "Exception caught:" + e.toString());
		}
		logger.trace(3, "end of evaluation:" + expression + ", result=" + result);
		return result;
	}

	private static SampSubSvcValue getSvcVideoServicePlan(final ParmEvalContext evalContext) {
		final String mNm = "getSvcVideoServicePlan ";
		SampSubSvcValue result = null;
		// sub not needed for the time being
		final SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
		if (sub == null) {
			logger.log(mNm + "cannot get SampSubValue from evalContext");
			return result;
		}
		final SampSubSvcValue sssvVideoAccess = (SampSubSvcValue) evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
		if (sssvVideoAccess == null) {
			logger.log(mNm + "cannot get " + SVC_VIDEO_SERVICE_PLAN + " from evalContext");
			return result;
		}
		result = sssvVideoAccess.getParentSubSvc();
		return result;
	}

	private static String[] getJmfParm(final String expression) throws IllegalArgumentException {
		String[] result = null;
		logger.trace(3, "evaluate expression:" + expression);
		result = expression.split("\\.");
		for (int i = 0; i < result.length; i++) {
			logger.trace(3, "value:" + result[i]);
		}
		if (result.length == 0) {
			throw new IllegalArgumentException("no parameter:" + expression);
		}
		logger.log("result=" + result.length);
		return result;
	}

	private boolean checkSubSvc(final SampSubSvcValue parent, final String[] jmfExpr) 
	{
		boolean ret = false;
		logger.trace(5, "Check for service = " + jmfExpr[0]);
		final Collection childList = parent.getChildSubSvcLst();
		if (childList.size() > 0) {
			final Iterator itr = childList.iterator();
			while (itr.hasNext()) {
				final SampSubSvcValue child = (SampSubSvcValue)itr.next();
				if (child.getSvcNm().equals(jmfExpr[0])) {
					logger.log("Name of Child Service : "+child.getSvcNm());
					if(child.getSvcNm().equals(VIDEO_SUBSCRIPTION))
					{
							logger.log("Found video subscription and Subscription Type : "+jmfExpr[1]);
							ret = checkSubSvcInternal(child);
							logger.trace(5, "Return = "+ret);
							if(ret)
							{
							logger.trace(5, "break");
							// there may be multiple existing services so, 'ret' will be false in iteration
							// to avoid this break once we got change.
							break;
							}
					}
					else// assuming it is video event service
					{
						ret = checkSubSvcInternal(child);
						logger.trace(5, "Return = "+ret);
						if(ret)
						{
						logger.trace(5, "break");
						// there may be multiple existing services so, 'ret' will be false in iteration
						// to avoid this break once we got change.
						break;
						}
					}
				}
			}
		}
		return ret;
	}

	private boolean checkSubSvcInternal(final SampSubSvcValue child)
	{
		logger.log("checkSubSvcInternal is called");
		boolean result = false;
		final String status = child.getParmVal(STATUS);
		final String oldStatus = child.getOldParmVal(STATUS);
		logger.trace(5, "Current status = " + status);
		if (oldStatus != null) {
			logger.trace(5, "Old status = " + oldStatus);
		}
		else {
			logger.trace(5, "Old status is null");
		}
		if ((oldStatus != null && oldStatus.equalsIgnoreCase(STATUS_INACTIVE) 
				&& status.equalsIgnoreCase(STATUS_ACTIVE))
				|| status.equalsIgnoreCase(STATUS_INACTIVE)
				|| status.equalsIgnoreCase(STATUS_DELETED)) {
			result = true;
		} 
		final Collection modAttribute = child.getModifiedAttributeNames();
		final Iterator itrAttr = modAttribute.iterator();
		while (itrAttr.hasNext()) {
			final String parmNm = (String)itrAttr.next();
			if (parmNm.equals(STATUS)) continue;
			final String newParmValue = child.getParmVal(parmNm);
			final String oldParmValue = child.getOldParmVal(parmNm);
			logger.trace(5, "New value :"+newParmValue+" Old Value :"+oldParmValue);
			logger.trace(5, "Compare parm " + parmNm);
			if (parmNm.equals("video_entitlement_uniqueness_check")) continue;
			if (newParmValue != null) {
				logger.trace(5, "New value:" + newParmValue);
			} else {
				logger.trace(5, "New value is null");
			}
			if (oldParmValue != null) {
				logger.trace(5, "Old value:" + oldParmValue);
			} else {
				logger.trace(5, "Old value is null");
			}
			if (newParmValue != null) {
				if (!newParmValue.equals(oldParmValue)) {
					result = true;
					break;
				}
			} 
			else if (oldParmValue != null) {
				result = true;
				break;
			}
		}
		return result;
	}
	private String getCurrDateTime() {
		final Date currDate=new Date();
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		final String formattedDate = formatter.format(currDate);
		logger.trace(5, "Formatted Date : "+formattedDate);
		return formattedDate;
	}
}
