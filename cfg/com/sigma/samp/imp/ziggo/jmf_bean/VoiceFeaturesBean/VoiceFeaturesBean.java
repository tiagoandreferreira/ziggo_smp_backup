package com.sigma.samp.imp.ziggo.jmf_bean.VoiceFeaturesBean;

//=======================================================================
//FILE INFO
//$Id: VoiceFeaturesBean.java,v 1.1 2015/06/04 10:13:29 ricardol Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.vframe.jcmn.ParmEvalContext;

import static com.sigma.samp.imp.ziggo.jmf_bean.VoiceFeaturesBean.VoiceFeaturesConstant.*;

public class VoiceFeaturesBean implements SessionBean {

    private static Log     logger = new Log(VoiceFeaturesBean.class.getName());
    private SessionContext ctx;

    /**
     * Eval Method
     * 
     * @param expression
     * @param inputLst
     * @param evalContext
     * @return expression to be used in xml according to the context
     * @throws IllegalArgumentException
     * @throws RemoteException
     */

    public String eval(final String expression, final Map inputLst, final ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException {
        try {

            String accessType = null;
            String subNetwork = null;
            logger.trace(5, "In Eval Method");
            logger.trace(3, "In VoiceFeaturesBean.eval()\n Expression received: " + expression);
            final StringTokenizer strToken = new StringTokenizer(expression, ".", false);
            final ArrayList keyLst = new ArrayList();
            while (strToken.hasMoreTokens()) {
                keyLst.add(strToken.nextToken());
            }
            final int size = keyLst.size();
            logger.trace(5, " Size : " + size);
            if (size == 1) {
                accessType = (String) keyLst.get(size - 1);
                logger.trace(5, "access type : " + accessType);
            }
            logger.trace(5, "Version 1.14");
            SampSubSvcValue subSvc = (SampSubSvcValue) evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL);
            if (subSvc == null) {
                subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
            }
            logger.trace(5, "context subsvc : " + subSvc);
            final SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
            logger.trace(6, "context sub : " + sub);
            logger.trace(5, "Total sub service" + sub.getSubSvcs().size());
            // code is added to get group_id of respective subnetwork

            if (accessType.equalsIgnoreCase(CHANGE_FEATURES)) {

                String[] parameter_names = { SW_CW_S, SW_CW_A, SW_DNPPS_S, SW_DNPPS_A, SW_SCR_S, SW_CND_S,
                        SW_CND_A, SW_TWC_S, SW_CFV_S, SW_CFVM_S, SW_SCR_DN_L };

                String[] new_parameters = { subSvc.getParmVal(SW_CW_S), subSvc.getParmVal(SW_CW_A),
                        subSvc.getParmVal(SW_DNPPS_S), subSvc.getParmVal(SW_DNPPS_A),
                        subSvc.getParmVal(SW_SCR_S), subSvc.getParmVal(SW_CND_S),
                        subSvc.getParmVal(SW_CND_A), subSvc.getParmVal(SW_TWC_S),
                        subSvc.getParmVal(SW_CFV_S), subSvc.getParmVal(SW_CFVM_S),
                        subSvc.getParmVal(SW_SCR_DN_L) };

                String[] old_parameters = { subSvc.getOldParmVal(SW_CW_S), subSvc.getOldParmVal(SW_CW_A),
                        subSvc.getOldParmVal(SW_DNPPS_S), subSvc.getOldParmVal(SW_DNPPS_A),
                        subSvc.getOldParmVal(SW_SCR_S), subSvc.getOldParmVal(SW_CND_S),
                        subSvc.getOldParmVal(SW_CND_A), subSvc.getOldParmVal(SW_TWC_S),
                        subSvc.getOldParmVal(SW_CFV_S), subSvc.getOldParmVal(SW_CFVM_S),
                        subSvc.getOldParmVal(SW_SCR_DN_L) };

                for (int count = 0; count < new_parameters.length; count++) {
                    if (new_parameters[count] != old_parameters[count] && new_parameters[count] != null) {
                        logger.trace(5, "The parameter: " + parameter_names[count] + " have changes");
                        return "true";
                    }
                }
                logger.trace(5,"No paramenters change");
                return "false";
            }

            else {
                subNetwork = "Please Check Your Expression";
                throw new IllegalArgumentException(subNetwork);
            }
        } catch (Exception e) {
            logger.log("Exception caught: " + e.getMessage());
            throw new RemoteException("Exception caught: ", e);
        }
    }

    public void ejbActivate() throws EJBException, RemoteException {

    }

    public void ejbPassivate() throws EJBException, RemoteException {

    }

    public void ejbRemove() throws EJBException, RemoteException {

    }

    public void setSessionContext(final SessionContext arg0) throws EJBException, RemoteException {
        ctx = arg0;
    }

    public void ejbCreate() throws CreateException, EJBException, RemoteException {
    }

}
