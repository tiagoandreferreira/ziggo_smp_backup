package com.sigma.samp.imp.ziggo.jmf_bean.VoiceFeaturesBean;

//=======================================================================
//FILE INFO
//$Id: VoiceFeaturesConstant.java,v 1.1 2015/06/04 10:13:29 ricardol Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class VoiceFeaturesConstant {

    public static final String CONTEXT_PARM_SUBSVCMODEL = "svc";
    public static final String CONTEXT_PARM_SUBMODEL    = "sub";

    // Created for the new bean method
    public static final String CHANGE_FEATURES          = "change_features";
    public static final String SW_CW_S = "sw_cw_subscribed";
    public static final String SW_CW_A = "sw_cw_activated";
    public static final String SW_DNPPS_S = "sw_dnpps_subscribed";
    public static final String SW_DNPPS_A = "sw_dnpps_activated ";
    public static final String SW_SCR_S = "sw_scr_subscribed";
    public static final String SW_CND_S = "sw_cnd_subscribed";
    public static final String SW_CND_A = "sw_cnd_activated";
    public static final String SW_TWC_S = "sw_twc_subscribed";
    public static final String SW_CFV_S = "sw_cfv_subscribed";
    public static final String SW_CFVM_S = "sw_cfvm_subscribed";
    public static final String SW_SCR_DN_L = "sw_scr_dn_list";

}
