package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoSPNameBean;
/*$Id ZiggoSPNameBean.java ,v 1.0 2013/06/27 16:19:30 Prakash $ initial version $
  $Id ZiggoSPNameBean.java ,v 1.1 2013/08/30 18:17:30 Prakash $ code added for move scenario $
  $Id ZiggoSPNameBean.java ,v 1.2 2013/09/26 06:19:30 Prakash $ svcProvider format changed $
  $Id ZiggoSPNameBean.java ,v 1.3 2014/03/24 17:39:30 Prakash $ code added to handle can not calculate error while placing event based order after event based service change $
  $Id ZiggoSPNameBean.java ,v 1.4 2014/07/03 16:11:30 Prakash $ code added to get areacode startcode for hiq $
  $Id ZiggoSPNameBean.java ,v 1.5 2014/07/11 16:11:30 Prakash $ code added to get old areacode for hiq and old service provider id for bs while doing change TN $
  $Id ZiggoSPNameBean.java ,v 1.6 2018/08/06 16:19:30 Prakash $ code added to handle change TN CR on BS $ */
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.sigma.hframe.jlog.Log;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.samp.stm.cmn.Subnetwork;
import com.sigma.samp.stm.ssn.SSNAgent;
import com.sigma.samp.stm.ssn.SSNAgentHome;
import com.sigma.samp.stm.ssn.SSNServiceLocator;
import com.sigma.hframe.jerror.SmpResourceException;
import com.sigma.samp.vframe.utils.ResUtil;
import static com.sigma.samp.imp.ziggo.jmf_bean.ZiggoSPNameBean.ZiggoSPNameBeanConst.*;

public class ZiggoSPNameBean implements SessionBean {

	private SSNAgent ssnAgent;
	public String svcProvider;
	private static Log logger = new Log(ZiggoSPNameBean.class.getName());
	private SessionContext ctx;
	SampSubSvcValue subSvc = null;

	public void ejbActivate()
	throws EJBException, RemoteException
	{
	}

	public void ejbPassivate()
	throws EJBException, RemoteException
	{
	}

	public void ejbRemove()
	throws EJBException, RemoteException
	{
	}

	public void setSessionContext(SessionContext arg0)
	throws EJBException, RemoteException
	{
		ctx = arg0;
	}
	public void ejbCreate() throws CreateException, EJBException, RemoteException {
	}

    /**
     * Eval Method
     * 
     * @param expression
     * @param inputLst
     * @param evalContext
     * @return expression to be used in xml according to the context
     * @throws IllegalArgumentException
     * @throws RemoteException
     */

    public String eval(final String expression, final Map inputLst, final ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException {
		logger.trace(5, "Version 1.6 ZiggoSPNameBean.eval()\n Expression received: " + expression);
		String accessType = null;
		String beanReturn = null;
		final StringTokenizer strTokenizer = new StringTokenizer(expression, ".", false);
		final ArrayList keyLst = new ArrayList();
		while (strTokenizer.hasMoreTokens()) {
			keyLst.add(strTokenizer.nextToken());
		}
		final int size = keyLst.size();
		logger.trace(3, " Size : " + size);
		if (size == 1) {
			accessType = (String) keyLst.get(size - 1);
			logger.trace(5, "Access Type : " + accessType);
		}
		else
		{
			beanReturn = "Please Check Your Expression";
			throw new IllegalArgumentException(beanReturn);
		}	
		subSvc = ((SampSubSvcValue)evalContext.getParmAccess(CONTEXT_PARM_SUBSVCMODEL));
		if (subSvc == null) {
			subSvc = ((SampSubSvcValue)evalContext.getDfltParmAccess());
		}
		logger.trace(5, "context subsvc : " + subSvc);
		if ((subSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) || (subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS))) {
			logger.trace(5, "smp dial tone access service received: " + subSvc);
		} else if ((subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_FEATURE)) || (subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_FEATURE_PKG_STD))) {
			final SampSubSvcValue parentSubSvc = subSvc.getParentSubSvc();
			final Collection childSvcCollection = parentSubSvc.getChildSubSvcLst();
			final Iterator itr = childSvcCollection.iterator();
			while (itr.hasNext()) {
				final SampSubSvcValue childSvc = (SampSubSvcValue)itr.next();
				if ((childSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)) || (childSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS))) {
					subSvc = childSvc;
					logger.trace(5, "smp dial tone access service is sibling of smp swich feature packag servicee: " + subSvc);
					break;
				}
			}
		}
		String telephoneNumber = "";
		String areaCode = "";
        if (accessType.equalsIgnoreCase(HOME_DN)) {
            telephoneNumber = subSvc.getParmVal(TELEPHONE_NUMBER);
        } else if (accessType.equalsIgnoreCase(OLD_HOME_DN) || accessType.equalsIgnoreCase(OLDSVCPROVIDER)) {
            telephoneNumber = subSvc.getOldParmVal(TELEPHONE_NUMBER);
        } else {
            telephoneNumber = subSvc.getParmVal(TELEPHONE_NUMBER);
		}
		if(!accessType.equalsIgnoreCase("check_voice_line"))
		{
			areaCode = getAreaCodeProvider(telephoneNumber);
		}
        if (accessType.equalsIgnoreCase(SVCPROVIDER) || accessType.equalsIgnoreCase(OLDSVCPROVIDER)) {
			if (areaCode != null) {
				logger.trace(5, "Valid Area Code value received in eval() :" + areaCode);
				areaCode = areaCode.replaceFirst("^0+(?!$)", "");
				logger.trace(5, "Removing first 0 from area code:" + areaCode);
				try {
					String subnetwork_nm = "";
					String subnetwork_nm_changed = "";
					String move_to = null;
					move_to = subSvc.getParmVal(MOVE_TO);
					logger.trace(5, "move value is ==" + move_to);
                    if ((move_to != null) && (move_to != "") && (move_to.equalsIgnoreCase(HIQ))) {
						logger.trace(5, "Move from BS to HIQ scenario");
						subnetwork_nm = subSvc.getParmVal(SWITCH_ID);
						logger.trace(5, "Valid subnetwork value received from service == " + subnetwork_nm);
						subnetwork_nm_changed = subnetwork_nm.replace("_", "-");
						logger.trace(5, "Subnetwork after Replace =" + subnetwork_nm_changed);
						final int index = subnetwork_nm_changed.lastIndexOf("-");
						subnetwork_nm_changed = subnetwork_nm_changed.substring(0, index);
						svcProvider = (subnetwork_nm_changed + "-" + areaCode);
						logger.trace(5, subnetwork_nm_changed + "-" + areaCode);
						logger.trace(5, "Service Provider for move scenario " + svcProvider);
					} else {
						logger.trace(5, "Not a Move Scenario");
						//code added for version 1.3
						if (subSvc.getSvcNm().equals(SMP_EVENT_SWITCH_FEATURE_PACKAGE)) {
							logger.log("Got event based service");
                            final SampSubValue sub = (SampSubValue) evalContext
                                    .getParmAccess(CONTEXT_PARM_SUBMODEL);
							logger.trace(5, "Total sub service " + sub.getSubSvcs().size());
							SampSubSvcValue service = null;
							SampSubSvcValue svc = null;
							String result = null;
							final Iterator itr = sub.getSubSvcs().iterator();
                            final String telephone = subSvc.getParmVal(TELEPHONE_NUMBER);
							while (itr.hasNext()) {
								logger.trace(5, "In sub service iterators");
								service = (SampSubSvcValue)itr.next();
                                if (service.getSvcNm().equalsIgnoreCase(SMP_VOICE_LINE)) {
									logger.log("got smp voice line ");
									svc = getIfChildService(service, telephone);
									if (svc != null) {
										logger.trace(5, "found svc :" + svc);
                                        result = svc.getParmVal(SWITCH_ID);
										break;
									}
									logger.trace(5,"in else where svc is null"); continue;
								}

								logger.log("Found the following service: " + service.getSvcNm());
							}

							if (result != null) {
								logger.trace(5, "result is not null");
								subnetwork_nm_changed = result.replace("_", "-");
								logger.trace(5, "Subnetwork after Replace =" + subnetwork_nm_changed);
								final int index = subnetwork_nm_changed.lastIndexOf("-");
								subnetwork_nm_changed = subnetwork_nm_changed.substring(0, index);
								svcProvider = (subnetwork_nm_changed + "-" + areaCode);
								logger.trace(5, subnetwork_nm_changed + "-" + areaCode);
								logger.trace(5, "Service Provider for event based " + svcProvider);
							} else {
								logger.trace(5, "svcProvider is ==" + svcProvider);
							}
						}
						//code ended for version 1.3
						else
						{
							final SSNAgent ssnAgent = getSSNAgent();
							final BigDecimal subSvcId = new BigDecimal(subSvc.getSubSvcKey().getInventoryId().longValue());
							logger.log("subSvcId: " + subSvcId);
							final Map roleMap = ssnAgent.getSnwsRoles(subSvcId);
							logger.log("roleMap: " + roleMap.toString());
							final Collection fullSntwks = roleMap.keySet();
							logger.log("Size of replicated subntwk elements: " + fullSntwks.size());
							for (final Iterator is = fullSntwks.iterator(); is.hasNext(); ) {
								final Subnetwork subnt = (Subnetwork)is.next();
								final String typeName = subnt.getSubnetworkTypeValue().getName();
                                if (typeName.equalsIgnoreCase(CALL_SERVER)) {
									subnetwork_nm = subnt.getName();
									logger.log("Matching :" + typeName + " from dial tone SSN definition");
									break;
								}
							}
							logger.trace(5, "Valid subnetwork value received in eval():: " + subnetwork_nm);
							subnetwork_nm_changed = subnetwork_nm.replace("_", "-");
							logger.trace(5, "Subnetwork after Replace =" + subnetwork_nm_changed);
							final int index = subnetwork_nm_changed.lastIndexOf("-");
							subnetwork_nm_changed = subnetwork_nm_changed.substring(0, index);
							svcProvider = (subnetwork_nm_changed + "-" + areaCode);
							logger.trace(5, subnetwork_nm_changed + "-" + areaCode);
							logger.trace(5, "Service Provider inside the loop:: " + svcProvider);
						}
					}
				}
				catch (Exception ex) {
					logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl, "Fail to get ssn id for subSvc:" + subSvc.getSubSvcKey(), ex);
				}
				logger.trace(5, "Service Provider outside the loop:: " + svcProvider);
				return svcProvider;
			}
		}
        else if(accessType.equalsIgnoreCase(HOME_DN) || accessType.equalsIgnoreCase(OLD_HOME_DN)) 
		{
			if (areaCode != null && areaCode.length()==3) {
				logger.trace(5, "3 digit code :" + areaCode);
				svcProvider=areaCode.substring(1,3);
				logger.trace(5, "Area code:" + svcProvider);

			}
			else if(areaCode != null && areaCode.length()==4)
			{
				logger.trace(5, "4 digit code:" + areaCode);
				svcProvider=areaCode.substring(1,4);
				logger.trace(5, "Area code:" + svcProvider);
			}
			return svcProvider;
		}
		else if(accessType.equalsIgnoreCase(CHECK_VOICE_LINE))
		{
			final String groupID = subSvc.getParmVal(CURRENT_GROUP_ID);
			final SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
			logger.trace(5, "Total sub service" + sub.getSubSvcs().size());
			final Iterator itr = sub.getSubSvcs().iterator();
			SampSubSvcValue service = null;
			String result = "single";
			while (itr.hasNext()) {
				logger.trace(5,"In sub service iterators");
				service = (SampSubSvcValue) itr.next();
				if (service.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)) {
					final String serviceTelNo = service.getParmVal(TELEPHONE_NUMBER);
					logger.trace(5,"Telephone Number of Voice Service is : "+serviceTelNo);
					if (!serviceTelNo.equalsIgnoreCase(telephoneNumber)) {
						logger.trace(5,"svc not null");
						final String newGroupId = service.getParmVal(CURRENT_GROUP_ID);
						logger.trace(5, "newGroupId of other voice line : " + newGroupId);
						if(newGroupId.equalsIgnoreCase(groupID))
						{
							result = "additional";
							logger.trace(5,"result ="+result);
						}
						else
						{
							result = "last";
							logger.trace(5,"result ="+result);
						}
						break;
					} else {
						logger.trace(5,"svc is null");
					}
				} else {
					logger.trace(5,"Found the following service: "
							+ service.getSvcNm());
				}
			}
			return result;
		}
		else if(accessType.equalsIgnoreCase("area_code"))
		{
			logger.trace(3,"area code is : "+areaCode);
			return areaCode;
		}
		logger.trace(5, "Valid Area code value not received in eval()");
		return null;
	}

    /**
     * This method will return the SSN Agent
     * 
     * @return SSNAgent
     * @throws SmpResourceException
     */
    private SSNAgent getSSNAgent() throws SmpResourceException {
		logger.trace(5, "lookup for SSNAgentHome");
		if (ssnAgent == null) {
			try {
				final SSNAgentHome ssn_AgentHome = (SSNAgentHome)SSNServiceLocator.getInstance().getHome(2);

				ssnAgent = ssn_AgentHome.create();
			} catch (Exception ex) {
				logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl, "Failed finding SSNAgent " + ex, ex);
				throw new SmpResourceException(ex.getMessage(), ex);
			}
		}
		return ssnAgent;
	}

    /**
     * This method will return the area code according to the telephone number
     * 
     * @param telephoneNumber
     * @return area code
     * @throws RemoteException
     */
	public String getAreaCodeProvider(final String telephoneNumber) throws RemoteException {
		logger.trace(5, "Got telephone number parm value from smp dial tone service :" + telephoneNumber);
		final String telNum_3Digit = telephoneNumber.substring(0, 3);
		logger.trace(5, "First three digit of telephone number got:" + telNum_3Digit);
		ResultSet rset = null;
		Statement stmt = null;
		String areaCode = null;
		final Connection conn = ResUtil.getSmartDbConnection();
		try {
			stmt = conn.createStatement();
			rset = stmt.executeQuery("select area_code from service_provider_3_digit where area_code='" + telNum_3Digit + "' order by area_code");

			logger.trace(5, "select area_code from service_provider_3_digit where area_code='" + telNum_3Digit + "' order by area_code;");

			while (rset.next()) {
				areaCode = rset.getString(1);
				logger.log("Three digit is match with area code in the DB :" + areaCode);
			}
			if (areaCode == null) {
				final String telNum_4Digit = telephoneNumber.substring(0, 4);
				logger.trace(5, "First four digit of telephone number get:" + telNum_4Digit);
				rset = stmt.executeQuery("select area_code from service_provider_4_digit where area_code='" + telNum_4Digit + "' order by area_code");

				logger.trace(5, "select area_code from service_provider_4_digit where area_code='" + telNum_4Digit + "' order by area_code;");

				while (rset.next()) {
					areaCode = rset.getString(1);
					logger.trace(5, "Four digit is match with area code in the DB :" + areaCode);
				}
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			logger.trace(6, "Stack Trace: " + e.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		} finally {
			if (rset != null)
			{
				ResUtil.closeDBResource(rset);
			}
			if (stmt != null)
			{
				ResUtil.closeDBResource(stmt);
			}ResUtil.closeConnection(conn);
		}
		return areaCode;
	}

    /**
     * This method will return a child service according to the telephone number
     * 
     * @param service
     * @param telephoneNumber
     * @return sub svc
     */
	private SampSubSvcValue getIfChildService(final SampSubSvcValue service,final String telephoneNumber) {
		logger.log("In Child Service");
		final Iterator itr = service.getChildSubSvcLst().iterator();
		logger.trace(5, "size : " + service.getChildSubSvcLst().size());
		SampSubSvcValue subSvc = null;
		while (itr.hasNext()) {
			subSvc = (SampSubSvcValue)itr.next();
            logger.trace(5,
                    "while subSvc : " + subSvc.getSvcNm() + "subSvc.getSubSvcKey()  " + subSvc.getSubSvcKey());
            if (((subSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) || (subSvc.getSvcNm()
                    .equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)))
                    && (subSvc.getParmVal(TELEPHONE_NUMBER) != null)) {
				logger.trace(5,"telephone number is not null and inside if");
                final String telno = subSvc.getParmVal(TELEPHONE_NUMBER);
				logger.trace(5, "telephone number from smp_voice_line : " + telno);
				if (telno.equalsIgnoreCase(telephoneNumber)) {
					logger.trace(5,"both telephone number is same");
					return subSvc;
				}
			}
		}
        return null;
	}
}