package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoSPNameBean;

//=======================================================================
//FILE INFO
//$Id: ZiggoSPNameBeanConst.java,v 1.1 2015/05/08 15:58:24 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================


public class ZiggoSPNameBeanConst {
    public static final String HOME_DN                          = "home_dn";
    public static final String TELEPHONE_NUMBER                 = "telephone_number";
    public static final String OLD_HOME_DN                      = "old_home_dn";
    public static final String OLDSVCPROVIDER                   = "oldsvcProvider";
    public static final String SVCPROVIDER                      = "svcProvider";
    public static final String HIQ                              = "hiq";
    public static final String SMP_VOICE_LINE                   = "smp_voice_line";
    public static final String CALL_SERVER                      = "call_server";
    public static final String CONTEXT_PARM_SUBSVCMODEL         = "svc";
    public static final String CONTEXT_PARM_SUBMODEL            = "sub";
    public static final String MOVE_TO                          = "move_to";
    public static final String SWITCH_ID                        = "switch_id";
    public static final String SMP_EVENT_SWITCH_FEATURE_PACKAGE = "smp_event_switch_feature_package";
    public static final String SMP_SWITCH_FEATURE               = "smp_switch_feature";
    public static final String SMP_SWITCH_FEATURE_PKG_STD       = "smp_switch_feature_pkg_std";
    public static final String SMP_DIAL_TONE_ACCESS             = "smp_dial_tone_access";
    public static final String SMP_SWITCH_DIAL_TONE_ACCESS      = "smp_switch_dial_tone_access";
    public static final String CURRENT_GROUP_ID                 = "bw_current_group_id";
	public static final String CHECK_VOICE_LINE                 = "check_voice_line";

}
