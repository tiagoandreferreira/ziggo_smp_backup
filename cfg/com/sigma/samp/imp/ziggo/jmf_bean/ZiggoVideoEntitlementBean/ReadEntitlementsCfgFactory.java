package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean;

/* $Id ReadEntitlementsCfgFactory.java ,v 1.0 2014/03/18 16:19:30 Prakash $ code added to ReadEntitlementsMapping from DB $ */

import java.util.HashMap;
import java.util.logging.Logger;

import com.sigma.vframe.jcfgmgr.CfgMgrException;
import com.sigma.vframe.jcfgmgr.ICfgFactory;
import com.sigma.hframe.jlog.Log;
import com.sigma.hframe.jlog.LogFactory;

public class ReadEntitlementsCfgFactory implements ICfgFactory {
	private final static Log LOGGER = LogFactory.create(ReadEntitlementsCfgFactory.class.getName());
	public ReadEntitlementsCfgFactory() {		
	}
	@Override
	public Object create(final String docNm,final int version,final ClassLoader cloader)	throws CfgMgrException {
		LOGGER.log(" ReadEntitlementsCfgFactory create() : : docNm = " + docNm + ",version=" + version);
		HashMap<String,String> entitlements = new HashMap<String,String>();
		final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
		try {
			entitlements = (HashMap<String, String>) readEntitlementsMapping.getEntitlementsMapping();
		} catch (Exception e) {
			final String msg = "Could not ReadEntitlements for docName " +
			docNm + ": " + e.getMessage();
			LOGGER.log(" ERROR : " + msg);
			LOGGER.log("Exception Message = "+ e.getMessage());
			throw new CfgMgrException(msg,e);
		}
		return entitlements;
	}

	@Override
	public int getLatestVersionNum(String arg0) {
		return -1;
	}
}
