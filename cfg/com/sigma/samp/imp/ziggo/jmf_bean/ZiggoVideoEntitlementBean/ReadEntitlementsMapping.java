package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean;
/* $Id ReadEntitlementsMapping.java ,v 1.0 2014/03/18 16:19:30 Prakash $ code added to ReadEntitlementsMapping from DB $ */

import java.util.HashMap;
import java.util.Map;
import com.sigma.hframe.jerror.SmpCfgRtException;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;

public class ReadEntitlementsMapping {
	private static final Log LOGGER = new Log(ReadEntitlementsMapping.class.getName());
	private static HashMap<String,String> entitlementMap = null;
	private static ReadEntitlementsMappingData instance = null;
	public ReadEntitlementsMapping() {
	}

    /**
     * 
     * @return
     */
	private static synchronized ReadEntitlementsMappingData getInstance() {
		LOGGER.trace(5, "#getInstance() - start");
		try {
			LOGGER.log(" instance " + instance );
			if(instance ==null){
				LOGGER.log("before ReadEntitlementsMappingData instance " + instance );
				instance = new ReadEntitlementsMappingData();
				LOGGER.log("after ReadEntitlementsMappingData instance " + instance );
			}
			LOGGER.trace(5, "#getInstance() - end");
			return instance;
		} catch (Exception e) {
			final String msg = "Could not get configuration object for ReadEntitlementsMappingData.";
			LOGGER.log(" ERROR : " + msg + " : " + e);
			LOGGER.alarm(AlarmLevel.eError, AlarmCategory.eAppl, msg, e);
			throw new SmpCfgRtException(msg, e);
		}
	}

    /**
     * This method will return the entitlements mapping
     * 
     * @return entitlement map
     * @throws IllegalArgumentException
     */
	public Map getEntitlementsMapping() throws IllegalArgumentException{
		LOGGER.trace(5, "getEntitlementsMapping() - start");
		if( entitlementMap == null ){
			LOGGER.log( " entitlementMap is null so, call loadEntitlementsMapping()");
			entitlementMap = (HashMap<String, String>) getInstance().loadEntitlementsMapping();
			LOGGER.trace(5, " entitlementMap = " + entitlementMap);
		}
		LOGGER.trace(5, "getEntitlementsMapping() - end");
		return entitlementMap;				
	}

    /**
     * 
     * @throws IllegalArgumentException
     */
	public void updateEntitlementMap() throws IllegalArgumentException{
		LOGGER.trace(5," clear current entitlementMap = " + entitlementMap);
		entitlementMap = null;
		LOGGER.trace(5," before reload entitlementMap = " + entitlementMap);
        /*
         * Do not make map as final because its creating problem for cfgupd if we run it more than
         * one time
         */
		HashMap<String,String> map = (HashMap<String, String>) getInstance().loadEntitlementsMapping();
		LOGGER.log(" updated entitlementMap = " + map);
		entitlementMap = map;
	}
}
