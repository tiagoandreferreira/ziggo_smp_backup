package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean;

/* $Id ReadEntitlementsMappingData.java ,v 1.0 2014/03/18 16:19:30 Prakash $ code added to ReadEntitlementsMapping from DB $ */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import com.sigma.hframe.jlog.Log;
import com.sigma.vframe.util.ResUtil;

import static com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean.ZiggoVideoEntitlementBeanConst.*;

public class ReadEntitlementsMappingData {
	private static final Log LOGGER = new Log(ReadEntitlementsMappingData.class.getName());
	public ReadEntitlementsMappingData() {		
	}
	public synchronized Map<String,String> loadEntitlementsMapping() throws IllegalArgumentException{
		LOGGER.trace(5, "loadEntitlementsMapping() - start");
		final HashMap<String,String> map = new HashMap<String,String>();
		PreparedStatement spStmt = null;
		ResultSet rss = null;
		Connection dbConn = null;
		final String query = "SELECT * FROM CUST_SIGMA_MAPPING_TABLE";
		try {
			dbConn = ResUtil.getSmartDbConnection();
			spStmt = dbConn.prepareStatement(query);                       
			rss = spStmt.executeQuery();
			while(rss.next()) {
				final String entitlement = rss.getString(SIGMA_PRODUCT_ENTITLEMENT);
				final String pysisCode = rss.getString(PISYS_CODE);
                final String prodisCode = rss.getString(PRODIS_CODE);
                final String tvixCode = rss.getString(TVIX_CODE);
				LOGGER.trace(5, " entitlement = " + entitlement );
				LOGGER.trace(5, " pysisCode = "+ pysisCode);
				LOGGER.trace(5, " prodisCode " + prodisCode);
				LOGGER.trace(5, " tvixCode " + tvixCode);
				if( entitlement != null ){
					if( pysisCode != null && pysisCode.length() > 0 ){
                        final String key = entitlement.concat(PISYSKEY);
						map.put(key, pysisCode);
					}
					if( prodisCode != null && prodisCode.length() > 0 ){
                        final String key = entitlement.concat(PRODISKEY);
						map.put(key, prodisCode);
					}
					if( tvixCode != null && tvixCode.length() > 0 ){
                        final String key = entitlement.concat(TVIXKEY);
						map.put(key, tvixCode);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.log( " Exception : " + e);
			throw new IllegalArgumentException(" Exception : " + e.getMessage() );
		} finally {
			if(rss != null){
				ResUtil.closeDBResource(rss);
			}
			if(spStmt != null){
				ResUtil.closeDBResource(spStmt);
			} ResUtil.closeConnection(dbConn);
		}
		LOGGER.trace(5, "loadentitlementsMapping() - end");
		return map;
	}
}
