package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean;

/* $Id ZiggoVideoEntitlementBean.java ,v 1.0 2014/02/07 16:19:30 Yashaswini $ initial version $
 $Id ZiggoVideoEntitlementBean.java ,v 1.1 2014/03/18 16:19:30 Prakash $ code added to ReadEntitlementsMapping from DB $ 
 $Id ZiggoVideoEntitlementBean.java ,v 1.2 2014/05/05 15:05:30 Prakash $ code added to handle interactivefulllist in reprov order and to handle delete smartcard after suspend$
 $Id ZiggoVideoEntitlementBean.java ,v 1.3 2014/05/19 15:05:30 Prakash $ code added for suspend_interactivefulllist and no_impact_eventaddlist $
 $Id ZiggoVideoEntitlementBean.java ,v 1.4 2014/05/21 15:05:30 Prakash $ subscription_type,bc_video_prov_tags,vod_prov_tags and ppv_video_prov_tags parms are removed from service.suspend_interactivefulllist is added for prodis suspend scenario. Code for TVIX is added,Hence code is changed accordingly $
 $Id ZiggoVideoEntitlementBean.java ,v 1.5 2014/06/12 15:05:30 Prakash $ code added for linearaddlist and lineardeletelist, it will used in change subscription scenario$
 $Id ZiggoVideoEntitlementBean.java ,v 1.6 2014/06/24 16:19:30 Prakash $ code added to handle with new mapping table(new CR) (Standard, Plus and Extra Package) $
 $Id ZiggoVideoEntitlementBean.java ,v 1.7 2014/09/11 16:19:30 Prakash $ code separation done of addlist and activelist $
 $Id ZiggoVideoEntitlementBean.java ,v 1.8 2014/12/15 16:19:30 Srishti $ code changes done for LIN list $ */
import static com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean.ZiggoVideoEntitlementBeanConst.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.vframe.jcfgmgr.CfgMgr.ICfgUpdateListener;
import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.vframe.jcfgmgr.CfgUpdate;
import com.sigma.vframe.jcfgmgr.CfgMgr;

public class ZiggoVideoEntitlementBean implements SessionBean, ICfgUpdateListener {
    private static Log     logger = new Log(ZiggoVideoEntitlementBean.class.getName());
    private SessionContext ctx;

    public String eval(final String expression, final Map inputLst, final ParmEvalContext evalContext)
            throws IllegalArgumentException, RemoteException {
        try {
            String accessType = null;
            String beanReturn = null;
            String entitlementId = null;
            String provStatus = null;
            String result = null;
            SampSubSvcValue parentSubSvc = null;
            Collection childSvcCollection = null;
            SampSubSvcValue childSvc = null;
            logger.trace(5, "In Eval Method");
            logger.trace(5, "In ZiggoVideoEntitlementBean.eval()\n Expression received: " + expression);
            final StringTokenizer strTokenizer = new StringTokenizer(expression, ".", false);
            final ArrayList keyLst = new ArrayList();
            final ArrayList entitlement = new ArrayList();
            while (strTokenizer.hasMoreTokens()) {
                keyLst.add(strTokenizer.nextToken());
            }
            final int size = keyLst.size();
            logger.trace(3, " Size : " + size);
            if (size == 1) {
                accessType = (String) keyLst.get(size - 1);
                logger.trace(5, "Access Type : " + accessType);
            } else {
                beanReturn = "Please Check Your Expression";
                throw new IllegalArgumentException(beanReturn);
            }
            SampSubSvcValue subSvc = (SampSubSvcValue) evalContext.getParmAccess(SUB_SVC_MODEL);
            final SampSubValue subVal = (SampSubValue) evalContext.getParmAccess(SUB_MODEL);
            Iterator itr = subVal.getSubSvcs().iterator();
            if (subSvc == null) {
                subSvc = (SampSubSvcValue) evalContext.getDfltParmAccess();
            }
            logger.trace(5, "Context Subsvc : " + subSvc);
            logger.trace(6, "Context Sub : " + subVal);
            logger.trace(5, "Version 1.8");
            // code it added to get all linear Entitlements along with video
            // event
            if (accessType.equalsIgnoreCase(FULLLIST)) {
                SampSubSvcValue videoAccessReceive = null;
                videoAccessReceive = getAEndSvcFrmZEndSvc(subVal, subSvc, VIDEO_SERVICE_DEFN_HAS_CAS);
                logger.trace(5, "Video Access received for fulllist " + videoAccessReceive);
                if (videoAccessReceive != null) {
                    parentSubSvc = videoAccessReceive.getParentSubSvc();
                    logger.trace(6, "parent : " + parentSubSvc);
                    childSvcCollection = parentSubSvc.getChildSubSvcLst();
                    itr = childSvcCollection.iterator();
                    while (itr.hasNext()) {
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)) {
                            logger.trace(5, "Found video subscription");
                            // code for linear
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
                            final HashMap<String, String> pisysMap = (HashMap<String, String>) readEntitlementsMapping
                                    .getEntitlementsMapping();
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "pisys key = " + pisysKey);
                            final boolean containsPisysChk = pisysMap.containsKey(pisysKey);
                            final boolean containsProdisChk = pisysMap.containsKey(prodisKey);
                            final boolean containsTvixChk = pisysMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " does not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsPisysChk) {
                                String pisysVal = pisysMap.get(pisysKey);
                                logger.trace(5, "pisys val = " + pisysVal);
                                if (pisysVal.contains("_")) {
                                    pisysVal = pisysVal.replace("_", ",");
                                    logger.trace(5, "After Change = " + pisysVal);
                                }
                                provStatus = childSvc.getProvisionStatus();
                                logger.trace(5, "Provision Status : " + provStatus);
                                if (provStatus.equalsIgnoreCase(STATUS_ACTIVE)) {
                                    entitlement.add(pisysVal);
                                }
                            }
                        }
                    }
                    final Iterator itrEvent = childSvcCollection.iterator();
                    while (itrEvent.hasNext()) {
                        childSvc = (SampSubSvcValue) itrEvent.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_EVENT)) {
                            logger.trace(5, "Found video event subscription");
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            logger.trace(5, "Found " + entitlementId + " video event");
                            provStatus = childSvc.getProvisionStatus();
                            logger.trace(5, "Provision Status : " + provStatus);
                            if (provStatus.equalsIgnoreCase(STATUS_ACTIVE)) {
                                entitlement.add(entitlementId);
                            }
                        }
                    }
                } else {
                    logger.trace(5, "Video Access Service not found");
                }
                result = getEntitlement(entitlement);
            }
            // code is added to get linear entitlements
            else if (accessType.equalsIgnoreCase(LINEARADDLIST)
                    || accessType.equalsIgnoreCase(LINEARDELETELIST)) {
                SampSubSvcValue videoAccessReceive = null;
                videoAccessReceive = getAEndSvcFrmZEndSvc(subVal, subSvc, VIDEO_SERVICE_DEFN_HAS_CAS);
                logger.trace(5, "Video Access received for linear add/delete list " + videoAccessReceive);
                if (videoAccessReceive != null) {
                    parentSubSvc = videoAccessReceive.getParentSubSvc();
                    logger.trace(6, "parent : " + parentSubSvc);
                    childSvcCollection = parentSubSvc.getChildSubSvcLst();
                    itr = childSvcCollection.iterator();

                }
                // To handle add and delete in Same order
                if (!accessType.equalsIgnoreCase(LINEARDELETELIST)) {
                    logger.trace(5, "Inside linearaddlist");
                    while (itr.hasNext()) {
                        logger.trace(5, "Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)) {
                            logger.trace(5, "Found video subscription");
                            final String status = childSvc.getParmVal(STATUS);
                            final String oldStatus = childSvc.getOldParmVal(STATUS);
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
                            final HashMap<String, String> pisysMap = (HashMap<String, String>) readEntitlementsMapping
                                    .getEntitlementsMapping();
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "pisys key = " + pisysKey);
                            final boolean containsPisysChk = pisysMap.containsKey(pisysKey);
                            final boolean containsProdisChk = pisysMap.containsKey(prodisKey);
                            final boolean containsTvixChk = pisysMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " does not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsPisysChk) {
                                String pisysVal = pisysMap.get(pisysKey);
                                logger.trace(5, "pisys val = " + pisysVal);
                                if (pisysVal.contains("_")) {
                                    pisysVal = pisysVal.replace("_", ",");
                                    logger.trace(5, "After Change = " + pisysVal);
                                }
                                logger.trace(5, "provision status is : " + status);
                                logger.trace(5, "old provision status is : " + oldStatus);
                                // code is added to get all interactive
                                // entitlements
                                if (accessType.equalsIgnoreCase(LINEARFULLLIST)
                                        && (status.equalsIgnoreCase(STATUS_ACTIVE))) {
                                    entitlement.add(pisysVal);
                                }
                                // code is added to get all interactive
                                // entitlements which come in TIBCO request
                                if (accessType.equalsIgnoreCase(LINEARADDLIST)
                                        && (oldStatus != null && oldStatus.equalsIgnoreCase(ADD_IN_PROGRESS) && status
                                                .equalsIgnoreCase(STATUS_ACTIVE))) {
                                    entitlement.add(pisysVal);
                                }
                            }
                        }
                    }
                } else {
                    // code is added to get all linear entitlements which come
                    // in delete request
                    logger.trace(5, "Inside linear Delete List");
                    while (itr.hasNext()) {
                        logger.trace(5, "Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)) {
                            logger.trace(5, "Found video subscription");
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
                            final HashMap<String, String> pisysMap = (HashMap<String, String>) readEntitlementsMapping
                                    .getEntitlementsMapping();
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "pisys key = " + pisysKey);
                            final boolean containsPisysChk = pisysMap.containsKey(pisysKey);
                            final boolean containsProdisChk = pisysMap.containsKey(prodisKey);
                            final boolean containsTvixChk = pisysMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " does not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsPisysChk) {
                                String pisysVal = pisysMap.get(pisysKey);
                                logger.trace(5, "pisys val = " + pisysVal);
                                if (pisysVal.contains("_")) {
                                    pisysVal = pisysVal.replace("_", ",");
                                    logger.trace(5, "After Change = " + pisysVal);
                                }
                                provStatus = childSvc.getProvisionStatus();
                                final String provStatusFrmSvc = childSvc.getParmVal(STATUS);
                                logger.trace(5, "provision status is : " + provStatus);
                                logger.trace(5, "provision status from svc is : " + provStatusFrmSvc);
                                if ((childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_DELETED) || childSvc
                                        .getProvisionStatus().equalsIgnoreCase(DELETE_IN_PROGRESS))) {
                                    entitlement.add(pisysVal);
                                }
                            }
                        }
                    }
                }
                result = getEntitlement(entitlement);
            }
            // code is added to get interactive entitlements
            else if (accessType.equalsIgnoreCase(INTERACTIVEFULLLIST)
                    || accessType.equalsIgnoreCase(INTERACTIVEADDLIST)
                    || accessType.equalsIgnoreCase(INTERACTIVEDELETELIST)
                    || accessType.equalsIgnoreCase(SUSPEND_INTERACTIVEFULLLIST)) {
                final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
                final HashMap<String, String> prodisMap = (HashMap<String, String>) readEntitlementsMapping
                        .getEntitlementsMapping();
                String addInProgressList = "";
                String activeList = "";
                String suspendList = "";
                String deleteInProgressList = "";
                itr = subVal.getSubSvcs().iterator();
                logger.trace(3, "size .... : " + subVal.getSubSvcs().size());
                while (itr.hasNext()) {
                    logger.trace(5, "Child service iterator");
                    childSvc = (SampSubSvcValue) itr.next();
                    if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && childSvc.getOldParmVal(STATUS) != null
                            && childSvc.getOldParmVal(STATUS).equalsIgnoreCase(ADD_IN_PROGRESS)
                            && childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_ACTIVE)) {
                        addInProgressList = addInProgressList + ","
                                + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "AIP list" + addInProgressList);
                    } else if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && (childSvc.getProvisionStatus().equalsIgnoreCase(DELETE_IN_PROGRESS) || childSvc
                                    .getProvisionStatus().equalsIgnoreCase(STATUS_DELETED))) {
                        deleteInProgressList = deleteInProgressList + ","
                                + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "DIP list" + deleteInProgressList);
                    } else if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && childSvc.getProvisionStatus().equalsIgnoreCase(COURTESY_BLOCK)
                            && childSvc.getOldParmVal(STATUS).equalsIgnoreCase(STATUS_ACTIVE)) {
                        suspendList = suspendList + "," + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "Suspend list" + suspendList);
                    } else if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_ACTIVE)) {
                        activeList = activeList + "," + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "Active list" + activeList);
                    }
                }
                addInProgressList = addInProgressList + ",";
                logger.trace(5, "Final AIP list" + addInProgressList);
                activeList = activeList + ",";
                suspendList = suspendList + ",";
                deleteInProgressList = deleteInProgressList + ",";
                // Code added to add interactive entitlement
                if (accessType.equalsIgnoreCase(INTERACTIVEADDLIST)) {
                    if (addInProgressList.contains(",1004,")) {
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                    // Condition is added to handle upgradation to 1003 DTV
                    // Package
                    else if (addInProgressList.contains(",1003,")) {
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                    // Condition is added to set 1002 DTV Package.
                    else if (addInProgressList.contains(",1002,")
                            || (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,") && deleteInProgressList
                                    .contains(",1004,"))
                            || (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,"))) {
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                }
                if (accessType.equalsIgnoreCase(INTERACTIVEFULLLIST)
                        || accessType.equalsIgnoreCase(SUSPEND_INTERACTIVEFULLLIST)) {
                    // Condition is added to handle upgradation to 1004 DTV
                    // Package
                    if (activeList.contains(",1004,") || suspendList.contains(",1004,")) {
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                    // Condition is added to handle upgradation to 1003 DTV
                    // Package
                    else if (activeList.contains(",1003,") || suspendList.contains(",1003,")) {
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                    // Condition is added to set 1002 DTV Package.
                    else if (activeList.contains(",1002,") || suspendList.contains(",1002,")) {
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                }
                // code added for version 1.2
                if (subSvc.getSvcNm().equalsIgnoreCase(STB_CAS)) {
                    SampSubSvcValue videoAccessReceive = null;
                    videoAccessReceive = getAEndSvcFrmZEndSvc(subVal, subSvc, VIDEO_SERVICE_DEFN_HAS_CAS);
                    logger.trace(5, "Video Access received for reprov order " + videoAccessReceive);
                    if (videoAccessReceive != null) {
                        parentSubSvc = videoAccessReceive.getParentSubSvc();
                        logger.trace(5, "reprov order parent : " + parentSubSvc);
                    } else {
                        logger.trace(5, "Video Access Service not found");
                    }
                } else {
                    parentSubSvc = subSvc.getParentSubSvc();
                }
                // code ended for version 1.2
                logger.trace(5, "Current Status of subSvc : " + subSvc.getProvisionStatus());
                logger.trace(5, "parent : " + parentSubSvc);
                childSvcCollection = parentSubSvc.getChildSubSvcLst();
                itr = childSvcCollection.iterator();
                // To handle add and delete in Same order
                if (!(accessType.equalsIgnoreCase(INTERACTIVEDELETELIST))) {
                    logger.trace(5,
                            "Inside interactiveaddlist or interactivefulllist suspend_interactivefulllist");
                    while (itr.hasNext()) {
                        logger.trace(5, "Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                                && !(childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1002")
                                        || childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1003") || childSvc
                                        .getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1004"))) {
                            logger.trace(5, "Found video subscription");
                            final String status = childSvc.getParmVal(STATUS);
                            final String oldStatus = childSvc.getOldParmVal(STATUS);
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "prodis key = " + prodisKey);
                            final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                            final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                            final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " is not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsProdisChk) {
                                final String prodisVal = prodisMap.get(prodisKey);
                                logger.trace(5, "prodis val = " + prodisVal);
                                final String prodisValChange = prodisVal.replace("_", ",");
                                logger.trace(5, "After Changes " + prodisValChange);
                                logger.trace(5, "provision status is : " + status);
                                logger.trace(5, "old provision status is : " + oldStatus);
                                // code is added to get all interactive
                                // entitlements which come in TIBCO request
                                if (accessType.equalsIgnoreCase(INTERACTIVEDELETELIST)
                                        && (oldStatus != null && oldStatus.equalsIgnoreCase(ADD_IN_PROGRESS) && status
                                                .equalsIgnoreCase(STATUS_ACTIVE))) {
                                    entitlement.add(prodisValChange);
                                }
                                // code is added to get all the active
                                // interactive entitlements.
                                else if (accessType.equalsIgnoreCase(INTERACTIVEFULLLIST)
                                        && (status.equalsIgnoreCase(STATUS_ACTIVE))) {
                                    entitlement.add(prodisValChange);
                                }
                                // code is added to get all interactive
                                // entitlements in case of suspend scenario
                                else if (accessType.equalsIgnoreCase(SUSPEND_INTERACTIVEFULLLIST)
                                        && (oldStatus != null && oldStatus.equalsIgnoreCase(STATUS_ACTIVE) && status
                                                .equalsIgnoreCase(COURTESY_BLOCK))) {
                                    logger.trace(5, SUSPEND_INTERACTIVEFULLLIST + " is called");
                                    entitlement.add(prodisValChange);
                                }
                            }
                        }
                    }
                } else {
                    // code is added to get all interactive entitlements which
                    // come in delete request
                    logger.trace(5, "Inside Interactive Delete List");
                    if (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,")
                            && deleteInProgressList.contains(",1004,")) {
                        logger.trace(3, "downgraded to standard from tvextra");
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    } else if ((activeList.contains(",1002,") && addInProgressList.contains(",1003,")
                            && deleteInProgressList.contains(",1003,") && addInProgressList
                                .contains(",1004,"))
                            || (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,"))) {
                        logger.trace(3, "upgraded from plus to tvextra or downgrade from plus to standard");
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    } else if ((activeList.contains(",1002,") && addInProgressList.contains(",1003,") && addInProgressList
                            .contains(",1004,"))
                            || (activeList.contains(",1002,") && addInProgressList.contains(",1003,"))) {
                        logger.trace(3, "upgraded from standard to extra or plus");
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }

                    } else if (deleteInProgressList.contains(",1004,")) {
                        logger.trace(3, "Delete video plan when sub has tvextra");
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    } else if (deleteInProgressList.contains(",1003,")) {
                        logger.trace(3, "Delete video plan when sub has plus");
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    } else if (deleteInProgressList.contains(",1002,")) {
                        logger.trace(3, "Delete video plan when sub has standard");
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "prodis key = " + prodisKey);
                        final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                        final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                        final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsProdisChk) {
                            final String prodisVal = prodisMap.get(prodisKey);
                            logger.trace(5, "prodis val = " + prodisVal);
                            final String prodisValChange = prodisVal.replace("_", ",");
                            logger.trace(5, "After Changes " + prodisValChange);
                            entitlement.add(prodisValChange);
                        }
                    }
                    childSvcCollection = parentSubSvc.getChildSubSvcLst();
                    itr = childSvcCollection.iterator();
                    while (itr.hasNext()) {
                        logger.log("Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                                && !(childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1002")
                                        || childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1003") || childSvc
                                        .getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1004"))) {
                            logger.trace(5, "Found video subscription");
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "prodis key = " + prodisKey);
                            final boolean containsPisysChk = prodisMap.containsKey(pisysKey);
                            final boolean containsProdisChk = prodisMap.containsKey(prodisKey);
                            final boolean containsTvixChk = prodisMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " does not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsProdisChk) {
                                final String prodisVal = prodisMap.get(prodisKey);
                                logger.trace(5, "prodis val = " + prodisVal);
                                final String prodisValChange = prodisVal.replace("_", ",");
                                provStatus = childSvc.getProvisionStatus();
                                final String provStatusFrmSvc = childSvc.getParmVal("provision_status");
                                logger.trace(5, "provision status is : " + provStatus);
                                logger.trace(5, "provision status from svc is : " + provStatusFrmSvc);
                                if ((childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_DELETED) || childSvc
                                        .getProvisionStatus().equalsIgnoreCase(DELETE_IN_PROGRESS))) {
                                    entitlement.add(prodisValChange);
                                }
                            }
                        }
                    }
                }
                result = getEntitlement(entitlement);
            }
            // code is added to get pay per view(video event) entitlement
            else if (accessType.equalsIgnoreCase(EVENTFULLLIST) || accessType.equalsIgnoreCase(EVENTADDLIST)
                    || accessType.equalsIgnoreCase(EVENTDELETELIST)
                    || accessType.equalsIgnoreCase(NO_IMPACT_EVENTADDLIST)) {
                SampSubSvcValue videoAccessReceive = null;
                videoAccessReceive = getAEndSvcFrmZEndSvc(subVal, subSvc, VIDEO_SERVICE_DEFN_HAS_CAS);
                logger.trace(5, "Video Access received " + videoAccessReceive);
                if (videoAccessReceive != null) {
                    parentSubSvc = videoAccessReceive.getParentSubSvc();
                    logger.trace(5, "parent : " + parentSubSvc);
                    childSvcCollection = parentSubSvc.getChildSubSvcLst();
                    itr = childSvcCollection.iterator();
                    while (itr.hasNext()) {
                        logger.trace(5, "Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_EVENT)) {
                            logger.trace(5, "Found video event");
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            logger.trace(5, "Found " + entitlementId + " video event");
                            final String status = childSvc.getParmVal(STATUS);
                            final String oldStatus = childSvc.getOldParmVal(STATUS);
                            logger.trace(5, "provision status is : " + status);
                            logger.trace(5, "old provision status is : " + oldStatus);
                            // code is added to get all pay per view
                            // entitlements
                            if (accessType.equalsIgnoreCase(EVENTFULLLIST)
                                    && (status.equalsIgnoreCase(STATUS_ACTIVE))) {
                                entitlement.add(entitlementId);
                            }
                            // code is added to get all pay per view
                            // entitlements which come in TIBCO request
                            else if (accessType.equalsIgnoreCase(EVENTADDLIST)
                                    && (oldStatus != null && oldStatus.equalsIgnoreCase(ADD_IN_PROGRESS) && status
                                            .equalsIgnoreCase(STATUS_ACTIVE))) {
                                if (childSvc.getParmVal(NO_IMPACT).equalsIgnoreCase(NO_IMPACT_VALUE)) {
                                    entitlement.add(entitlementId);
                                }
                            }
                            // code is added to get all pay per view
                            // entitlements which come in delete request
                            else if (accessType.equalsIgnoreCase(EVENTDELETELIST)
                                    && (status.equalsIgnoreCase(STATUS_DELETED) || status
                                            .equalsIgnoreCase(DELETE_IN_PROGRESS))) {
                                entitlement.add(entitlementId);
                            }
                        }
                    }
                }
                result = getEntitlement(entitlement);
            }
            // code is added to check whether video subscription is suspended or
            // active
            else if (accessType.equalsIgnoreCase(SUSPENDCHECK)) {
                logger.trace(5, "Found status  " + subSvc.getParmVal(STATUS));
                logger.trace(5, "Found old status  " + subSvc.getOldParmVal(STATUS));
                logger.trace(5, "Found prov status  " + subSvc.getProvisionStatus());
                logger.trace(5, "Found state  " + subSvc.getState());
                result = subSvc.getOldParmVal(STATUS);
            }
            // code is added to get tvix entitlements
            else if (accessType.equalsIgnoreCase(TVIXFULLLIST) || accessType.equalsIgnoreCase(TVIXADDLIST)
                    || accessType.equalsIgnoreCase(TVIXDELETELIST)
                    || accessType.equalsIgnoreCase(SUSPEND_TVIXFULLLIST)) {
                final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
                final HashMap<String, String> tvixMap = (HashMap<String, String>) readEntitlementsMapping
                        .getEntitlementsMapping();
                String addInProgressList = "";
                String activeList = "";
                String suspendList = "";
                String deleteInProgressList = "";
                itr = subVal.getSubSvcs().iterator();
                logger.trace(3, "size tvix .... : " + subVal.getSubSvcs().size());
                while (itr.hasNext()) {
                    logger.trace(5, "Child service iterator");
                    childSvc = (SampSubSvcValue) itr.next();
                    if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && childSvc.getOldParmVal(STATUS) != null
                            && childSvc.getOldParmVal(STATUS).equalsIgnoreCase(ADD_IN_PROGRESS)
                            && childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_ACTIVE)) {
                        addInProgressList = addInProgressList + ","
                                + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "AIP list" + addInProgressList);
                    } else if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && (childSvc.getProvisionStatus().equalsIgnoreCase(DELETE_IN_PROGRESS) || childSvc
                                    .getProvisionStatus().equalsIgnoreCase(STATUS_DELETED))) {
                        deleteInProgressList = deleteInProgressList + ","
                                + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "DIP list" + deleteInProgressList);
                    } else if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && childSvc.getProvisionStatus().equalsIgnoreCase(COURTESY_BLOCK)
                            && childSvc.getOldParmVal(STATUS).equalsIgnoreCase(STATUS_ACTIVE)) {
                        suspendList = suspendList + "," + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "Suspend list" + suspendList);
                    } else if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                            && childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_ACTIVE)) {
                        activeList = activeList + "," + childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                        logger.trace(5, "Active list" + activeList);
                    }
                }
                addInProgressList = addInProgressList + ",";
                logger.trace(5, "Final AIP list" + addInProgressList);
                activeList = activeList + ",";
                suspendList = suspendList + ",";
                deleteInProgressList = deleteInProgressList + ",";
                // code added to add tvix entitlements
                if (accessType.equalsIgnoreCase(TVIXADDLIST)) {
                    // Condition is added to handle upgradation in DTV Package
                    if (addInProgressList.contains(",1004,")) {
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                    // Condition is added to handle upgradation in DTV Package
                    else if (addInProgressList.contains(",1003,")) {
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                    // Condition is added to set standard DTV Package.
                    else if (addInProgressList.contains(",1002,")
                            || (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,") && deleteInProgressList
                                    .contains(",1004,"))
                            || (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,"))) {
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                }
                if (accessType.equalsIgnoreCase(TVIXFULLLIST)
                        || accessType.equalsIgnoreCase(SUSPEND_TVIXFULLLIST)) {
                    // Condition is added to handle upgradation in DTV Package
                    if (activeList.contains(",1004,") || suspendList.contains(",1004,")) {
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                    // Condition is added to handle upgradation in DTV Package
                    else if (activeList.contains(",1003,") || suspendList.contains(",1003,")) {
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                    // Condition is added to set standard DTV Package.
                    else if (activeList.contains(",1002,") || suspendList.contains(",1002,")) {
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                }
                if (subSvc.getSvcNm().equalsIgnoreCase(STB_CAS)) {
                    SampSubSvcValue videoAccessReceive = null;
                    videoAccessReceive = getAEndSvcFrmZEndSvc(subVal, subSvc, VIDEO_SERVICE_DEFN_HAS_CAS);
                    logger.trace(5, "Video Access received for reprov order " + videoAccessReceive);
                    if (videoAccessReceive != null) {
                        parentSubSvc = videoAccessReceive.getParentSubSvc();
                        logger.trace(5, "reprov order parent : " + parentSubSvc);
                    } else {
                        logger.trace(5, "Video Access Service not found");
                    }
                } else {
                    parentSubSvc = subSvc.getParentSubSvc();
                }
                logger.trace(5, "Current Status of subSvc : " + subSvc.getProvisionStatus());
                logger.trace(5, "parent : " + parentSubSvc);
                childSvcCollection = parentSubSvc.getChildSubSvcLst();
                itr = childSvcCollection.iterator();
                // To handle add and delete in Same order
                if (!(accessType.equalsIgnoreCase(TVIXDELETELIST))) {
                    logger.trace(5, "Inside tvixaddlist or tvixfulllist or suspend_tvixfulllist");
                    while (itr.hasNext()) {
                        logger.trace(5, "Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                                && !(childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1002")
                                        || childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1003") || childSvc
                                        .getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1004"))) {
                            logger.trace(5, "Found video subscription");
                            final String status = childSvc.getParmVal(STATUS);
                            final String oldStatus = childSvc.getOldParmVal(STATUS);
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "tvix key = " + tvixKey);
                            final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                            final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                            final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " does not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsTvixChk) {
                                final String tvixVal = tvixMap.get(tvixKey);
                                logger.trace(5, "tvix val = " + tvixVal);
                                final String tvixValChange = tvixVal.replace("_", ",");
                                logger.trace(5, "After change " + tvixValChange);
                                logger.trace(5, "provision status is : " + status);
                                logger.trace(5, "old provision status is : " + oldStatus);
                                // code is added to get all tvix entitlements
                                // which come in TIBCO request
                                if (accessType.equalsIgnoreCase(TVIXADDLIST)
                                        && (oldStatus != null && oldStatus.equalsIgnoreCase(ADD_IN_PROGRESS) && status
                                                .equalsIgnoreCase(STATUS_ACTIVE))) {
                                    entitlement.add(tvixValChange);
                                }
                                // code is added to get all the active tvix
                                // entitlements.
                                else if (accessType.equalsIgnoreCase(TVIXFULLLIST)
                                        && (status.equalsIgnoreCase(STATUS_ACTIVE))) {
                                    entitlement.add(tvixValChange);
                                }
                                // code is added to get all tvix entitlements in
                                // case of suspend scenario
                                else if (accessType.equalsIgnoreCase(SUSPEND_TVIXFULLLIST)
                                        && (oldStatus != null && oldStatus.equalsIgnoreCase(STATUS_ACTIVE) && status
                                                .equalsIgnoreCase(COURTESY_BLOCK))) {
                                    logger.trace(5, "suspend_tvixfulllist is called");
                                    entitlement.add(tvixValChange);
                                }
                            }
                        }
                    }
                } else {
                    // code is added to get all tvix entitlements which come in
                    // delete request
                    logger.trace(5, "Inside tvix Delete List");
                    if (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,")
                            && deleteInProgressList.contains(",1004,")) {
                        logger.trace(3, "downgraded to standard from tvextra");
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }

                    } else if ((activeList.contains(",1002,") && addInProgressList.contains(",1003,")
                            && deleteInProgressList.contains(",1003,") && addInProgressList
                                .contains(",1004,"))
                            || (activeList.contains(",1002,") && deleteInProgressList.contains(",1003,"))) {
                        logger.trace(3, "upgraded from plus to tvextra or downgrade from plus to standard");
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }

                    } else if ((activeList.contains(",1002,") && addInProgressList.contains(",1003,") && addInProgressList
                            .contains(",1004,"))
                            || (activeList.contains(",1002,") && addInProgressList.contains(",1003,"))) {
                        logger.trace(3, "upgraded from standard to extra or plus");
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    } else if (deleteInProgressList.contains(",1004,")) {
                        logger.trace(3, "Delete video plan when sub has tvextra");
                        entitlementId = "1004";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    } else if (deleteInProgressList.contains(",1003,")) {
                        logger.trace(3, "Delete video plan when sub has plus");
                        entitlementId = "1003";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    } else if (deleteInProgressList.contains(",1002,")) {
                        logger.trace(3, "Delete video plan when sub has standard");
                        entitlementId = "1002";
                        final String prodisKey = entitlementId.concat(PISYSKEY);
                        final String pisysKey = entitlementId.concat(PISYSKEY);
                        final String tvixKey = entitlementId.concat(TVIXKEY);
                        logger.trace(5, "tvix key = " + tvixKey);
                        final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                        final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                        final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                        if (containsPisysChk == false && containsProdisChk == false
                                && containsTvixChk == false) {
                            beanReturn = "Requested Entitlement " + entitlementId
                                    + " does not present in DB. Please provide correct Entitlement";
                            throw new IllegalArgumentException(beanReturn);
                        }
                        if (containsTvixChk) {
                            final String tvixVal = tvixMap.get(tvixKey);
                            logger.trace(5, "tvix val = " + tvixVal);
                            final String tvixValChange = tvixVal.replace("_", ",");
                            logger.trace(5, "After Changes " + tvixValChange);
                            entitlement.add(tvixValChange);
                        }
                    }
                    childSvcCollection = parentSubSvc.getChildSubSvcLst();
                    itr = childSvcCollection.iterator();
                    while (itr.hasNext()) {
                        logger.trace(5, "Child service iterator");
                        childSvc = (SampSubSvcValue) itr.next();
                        if (childSvc.getSvcNm().equalsIgnoreCase(VIDEO_SUBSCRIPTION)
                                && !(childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1002")
                                        || childSvc.getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1003") || childSvc
                                        .getParmVal(VIDEO_ENTITLEMENT_ID).equalsIgnoreCase("1004"))) {
                            logger.trace(5, "Found video subscription");
                            entitlementId = childSvc.getParmVal(VIDEO_ENTITLEMENT_ID);
                            final String prodisKey = entitlementId.concat(PISYSKEY);
                            final String pisysKey = entitlementId.concat(PISYSKEY);
                            final String tvixKey = entitlementId.concat(TVIXKEY);
                            logger.trace(5, "tvix key = " + tvixKey);
                            final boolean containsPisysChk = tvixMap.containsKey(pisysKey);
                            final boolean containsProdisChk = tvixMap.containsKey(prodisKey);
                            final boolean containsTvixChk = tvixMap.containsKey(tvixKey);
                            if (containsPisysChk == false && containsProdisChk == false
                                    && containsTvixChk == false) {
                                beanReturn = "Requested Entitlement " + entitlementId
                                        + " does not present in DB. Please provide correct Entitlement";
                                throw new IllegalArgumentException(beanReturn);
                            }
                            if (containsTvixChk) {
                                final String tvixVal = tvixMap.get(tvixKey);
                                logger.trace(5, "tvix val = " + tvixVal);
                                final String tvixValChange = tvixVal.replace("_", ",");
                                logger.trace(5, "After Changes " + tvixValChange);
                                provStatus = childSvc.getProvisionStatus();
                                final String provStatusFrmSvc = childSvc.getParmVal("provision_status");
                                logger.trace(5, "provision status is : " + provStatus);
                                logger.trace(5, "provision status from svc is : " + provStatusFrmSvc);
                                if ((childSvc.getProvisionStatus().equalsIgnoreCase(STATUS_DELETED) || childSvc
                                        .getProvisionStatus().equalsIgnoreCase(DELETE_IN_PROGRESS))) {
                                    entitlement.add(tvixValChange);
                                }
                            }
                        }
                    }
                }
                result = getEntitlement(entitlement);
            } else {
                beanReturn = "Please Check Your Expression";
                throw new IllegalArgumentException(beanReturn);
            }
            return result;
        } catch (Exception e) {
            logger.log(e.getMessage());
            throw new RemoteException("Exception caught: ", e);
        }

    }

    /**
     * This method will return comma separated entitlement id's based on requirement
     * 
     * @param entitlement
     * @return
     */
    private String getEntitlement(final ArrayList entitlement) {
        String result = null;
        logger.trace(5, "Entitlement array length==" + entitlement.size());
        if (entitlement.size() > 1) {
            result = (String) entitlement.get(0);
            logger.trace(5, "result of index 0 is : " + result);
            for (int j = 1; j < entitlement.size(); j++) {
                logger.trace(5, " j == " + j + " size = " + entitlement.size());
                result = result + "," + (String) entitlement.get(j);
            }
            logger.trace(5, "result=" + result);
        } else if (entitlement.size() == 1) {
            result = (String) entitlement.get(0);
            logger.trace(5, "result=" + result);
        } else {
            result = "null";
            logger.trace(5, "result=" + result);
        }
        return result;
    }

    /**
     * This method is used to get AEnd Service from ZEnd Service
     * 
     * @param sub
     * @param subSvc
     * @param assocName
     * @return
     */
    private static SampSubSvcValue getAEndSvcFrmZEndSvc(final SampSubValue sub, final SampSubSvcValue subSvc,
            final String assocName) {
        SampSubSvcValue aEndSvc = null;
        if (subSvc != null) {
            logger.trace(3, "Got the following association type: " + assocName);
            if (assocName.equalsIgnoreCase(VIDEO_SERVICE_DEFN_HAS_CAS)) {
                if (subSvc.getSvcNm().equalsIgnoreCase(STB_CAS)
                        || subSvc.getProvisionableSvcNm().equalsIgnoreCase(SMP_CPE_CAS)) {
                    final Collection assoc_list = sub.queryAssociationRegistries(VIDEO_SERVICE_DEFN_HAS_CAS,
                            subSvc.getSubSvcKey());
                    if (assoc_list != null) {
                        for (final Iterator itr = assoc_list.iterator(); itr.hasNext();) {
                            final SampSubEntityAssoc assoc1 = (SampSubEntityAssoc) itr.next();
                            aEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc1.getAEndEntityKey());
                            logger.trace(3, "Got the following subsvc val: " + aEndSvc);
                            break;
                        }
                    } else {
                        logger.trace(5, "Assoc does not exits");
                    }
                } else {
                    logger.trace(5, "Smart card not found");
                }
            }
        }
        return aEndSvc;
    }

    @Override
    public void updated(final CfgUpdate updateEvent) {
        if ("cust_sigma_mapping".equalsIgnoreCase(updateEvent.getDocNm())) {
            logger.trace(5, "Got cfgupd for cust_sigma_mapping");
            try {
                /*
                 * Do not make readEntitlementsMapping as final because its creating problem for
                 * cfgupd if we run it more than one time
                 */
                final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
                readEntitlementsMapping.updateEntitlementMap();
            } catch (Exception e) {
                logger.log(" Error occurred while loading : " + e);
            }
        }
    }

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void setSessionContext(final SessionContext arg0) throws EJBException, RemoteException {
        ctx = arg0;
    }

    public void ejbCreate() throws CreateException, EJBException, RemoteException {

        try {
            logger.trace(5, " In ejbCreate for ZiggoVideoEntitlementBean ");
            CfgMgr.registerListener("cust_sigma_mapping", this);
            logger.trace(5, " registerListener done ");
            CfgMgr.getCfg("cust_sigma_mapping", ZiggoVideoEntitlementBean.class.getClassLoader());
            logger.trace(5, " getCfg done ");
            /*
             * Do not make readEntitlementsMapping as final because its creating problem for cfgupd
             * if we run it more than one time
             */
            final ReadEntitlementsMapping readEntitlementsMapping = new ReadEntitlementsMapping();
            readEntitlementsMapping.getEntitlementsMapping();
            logger.trace(5, "** Mapping done **");
        } catch (Exception e) {
            logger.log(" Error occurred in ejbCreate while loading : " + e);
            throw new CreateException(" Error occurred in ejbCreate in --- : " + e.getMessage());
        }
    }
}
