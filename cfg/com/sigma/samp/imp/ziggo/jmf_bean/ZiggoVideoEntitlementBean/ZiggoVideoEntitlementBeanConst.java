package com.sigma.samp.imp.ziggo.jmf_bean.ZiggoVideoEntitlementBean;

//=======================================================================

//FILE INFO
//$Id: ZiggoVideoEntitlementBeanConst.java,v 1.1 2015/05/08 16:02:30 tiagof Exp $
//Copyright (c) 2002 Sigma Systems (Canada) Inc.
//All rights reserved.
//
//DESCRIPTION
//
//
//REVISION HISTORY
//* Based on CVS log
//==========================================================================

public class ZiggoVideoEntitlementBeanConst {
    public static final String SUB_SVC_MODEL               = "svc";
    public static final String SUB_MODEL                   = "sub";
    public static final String FULLLIST                    = "fulllist";
    public static final String LINEARADDLIST               = "linearaddlist";
    public static final String LINEARDELETELIST            = "lineardeletelist";
    public static final String INTERACTIVEFULLLIST         = "interactivefulllist";
    public static final String INTERACTIVEADDLIST          = "interactiveaddlist";
    public static final String INTERACTIVEDELETELIST       = "interactivedeletelist";
    public static final String SUSPEND_INTERACTIVEFULLLIST = "suspend_interactivefulllist";
    public static final String EVENTADDLIST                = "eventaddlist";
    public static final String EVENTDELETELIST             = "eventdeletelist";
    public static final String SUSPENDCHECK                = "suspendcheck";
    public static final String TVIXFULLLIST                = "tvixfulllist";
    public static final String TVIXADDLIST                 = "tvixaddlist";
    public static final String TVIXDELETELIST              = "tvixdeletelist";
    public static final String SUSPEND_TVIXFULLLIST        = "suspend_tvixfulllist";
    public static final String VIDEO_SUBSCRIPTION          = "video_subscription";
    public static final String VIDEO_EVENT                 = "video_event";
    public static final String VIDEO_ENTITLEMENT_ID        = "video_entitlement_id";
    public static final String COURTESY_BLOCK              = "courtesy_block";
    public static final String VIDEO_SERVICE_DEFN_HAS_CAS  = "video_service_defn_has_cas";
    public static final String STB_CAS                     = "stb_cas";
    public static final String SMP_CPE_CAS                 = "smp_cpe_cas";
    public static final String STATUS                      = "provision_status";
    public static final String ADD_IN_PROGRESS             = "add_in_progress";
    public static final String STATUS_ACTIVE               = "active";
    public static final String DELETE_IN_PROGRESS          = "delete_in_progress";
    public static final String STATUS_DELETED              = "deleted";
    public static final String NO_IMPACT                   = "no_impact";
    public static final String NO_IMPACT_VALUE             = "false";
    public static final String PRODISKEY                   = "-PRODIS";
    public static final String PISYSKEY                    = "-PISYS";
    public static final String TVIXKEY                     = "-TVIX";
    public static final String SIGMA_PRODUCT_ENTITLEMENT   = "SIGMA_PRODUCT_ENTITLEMENT";
    public static final String PISYS_CODE                  = "PISYS_CODE";
    public static final String PRODIS_CODE                 = "PRODIS_CODE";
    public static final String TVIX_CODE                   = "TVIX_CODE";
    public static final String LINEARFULLLIST              = "linearfulllist";
    public static final String EVENTFULLLIST               = "eventfulllist";
    public static final String NO_IMPACT_EVENTADDLIST      = "no_impact_eventaddlist";

}
