package com.sigma.samp.imp.ziggo.loadBalancer;

/* $Id LoadBalancingPickAlgorithmBean.java ,v 1.0 2013/06/14 16:19:30 Rushmi $ initial version $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.1 2013/06/21 17:18:18 Prakash $ Code added for migrate order and delete voice line scenario. If voice line is preactivated then decreament num of voice count by 1 while deleting voice line $
$If voice line is not preactivated then do not decreament num of voice line count by 1 while deleting voice line$
$Id LoadBalancingPickAlgorithmBean.java ,v 1.2 2013/06/27 16:19:30 Prakash $ Code added for Deactivate voice line scenario $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.3 2013/07/10 16:19:30 Prakash $ Code added to handle increment of voice line in activate voice line scenario $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.4 2013/07/19 16:19:30 Prakash $ Code added to handle event based voice features $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.5 2013/08/02 16:19:30 Prakash $ Code added to handle reprovision voice line scenario $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.6 2013/08/16 16:19:30 Prakash $ Code added to handle Voip Orders $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.7 2013/08/28 16:19:30 Prakash $ Changes in move condition check and code added to handle same feature package while event based feature order $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.8 2013/09/12 16:19:30 Prakash $ External key compare code is removed from event based feature code $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.9 2013/09/17 16:19:30 Prakash $ Code for preactivate scenario. If switch is out of service then voice line on that switch should not get provision. $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.10 2013/12/19 16:19:30 Prakash $ Code added to handle event based query_ss and query_nsn $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.11 2014/01/31 16:19:30 Prakash $ Voip order logic removed from bean. p_number parm removed from bean. $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.12 2014/09/30 16:19:30 Prakash $ issue fix for hint for old voip subscriber and internet access for migrate order $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.13 2014/09/30 16:19:30 Prakash $ cms_fqdn in move switch $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.14 2014/10/06 16:19:30 Prakash $ code added to handle emta service $
$Id LoadBalancingPickAlgorithmBean.java ,v 1.15 2014/11/06 16:19:30 Prakash $ code changes done for move migration window $
 */

import static com.sigma.samp.imp.ziggo.loadBalancer.LoadBalancingPickAlgorithmBeanConst.*;
import com.sigma.hframe.jerror.SmpResourceException;
import com.sigma.hframe.jerror.SmpUnknownException;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;
import com.sigma.hframe.jlog.SysTimer;
import com.sigma.samp.cmn.order.RefOrderEvents;
import com.sigma.samp.cmn.order.SampOrderValue;
import com.sigma.samp.cmn.stm.SubnetworkSearchCriteria;
import com.sigma.samp.cmn.stm.TopologyPrcsException;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubEntityAssocChgs;
import com.sigma.samp.cmn.sub.SampSubEntityKey;
import com.sigma.samp.cmn.sub.SampSubSvcKey;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.ordermgr.cmn.OrderContext;
import com.sigma.samp.stm.cmn.ServiceLocator;
import com.sigma.samp.stm.cmn.Subnetwork;
import com.sigma.samp.stm.cmn.SubnetworkArray;
import com.sigma.samp.stm.cmn.TopologyModel;
import com.sigma.samp.stm.cmn.TopologyModelHome;
import com.sigma.samp.stm.selection.SelectionAlgValue;
import com.sigma.samp.stm.selection.SelectionPrcsException;
import com.sigma.samp.stm.selection.SelectionSubntwkTypeValue;
import com.sigma.samp.stm.ssn.SSNAgent;
import com.sigma.samp.stm.ssn.SSNAgentHome;
import com.sigma.samp.stm.ssn.SSNServiceLocator;
import com.sigma.samp.stm.topology.walk.TopologyAgent;
import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.vframe.util.ResUtil;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

public class LoadBalancingPickAlgorithmBean implements SessionBean {
	private static final long serialVersionUID = -6718229803181314480L;
	private SessionContext ctx;
	private static final Log logger = new Log("stm.topology.walk.LoadBalancingPickAlgorithmBean");

	private ParmEvalContext evalContext;
	private transient TopologyModel topologyModel;
	SysTimer mystimer;
	SysTimer stimer = null;
	public void setSessionContext(SessionContext context)throws RemoteException, EJBException {
		this.ctx = context;
	}
	public void ejbActivate() throws RemoteException, EJBException {
	}
	public void ejbPassivate() throws RemoteException, EJBException {
	}
	public void ejbRemove() throws RemoteException, EJBException {
		this.mystimer.close();
	}
	public void ejbCreate() throws CreateException, RemoteException,EJBException {
		this.mystimer = new SysTimer("Stm",LOADBALANCINGPICKALGORITHMBEAN ,"?");
	}
	public Collection selectSubntwk(Collection alreadySelected,ParmEvalContext orderCtx, SampSubSvcValue subsvc,SelectionAlgValue subntwkTypes, Collection hints)throws SelectionPrcsException,TelephoneNumberException {
		logger.trace(5,"Version 1.15 LoadBalancer");
		logger.trace(5,new StringBuilder().append("The incoming subsvc : ").append(subsvc.getSvcNm()).toString());
		logger.trace(5,"Selecting subnetworks with custom voice walk algorithm ...");
		this.evalContext = orderCtx;
		Collection foundSubnetworkNodes = new HashSet();
		TopologyAgent topologyAgent = new TopologyAgent();
		SubnetworkSearchCriteria searchCriteria = new SubnetworkSearchCriteria();
		//code added in version 1.11
		String incomingService=subsvc.getSvcNm().toString();
		if(incomingService.equalsIgnoreCase(QUERY_NSN)) {
			logger.trace(5, "context subsvc of sync nsn: " + subsvc);
			String switch_sync_query=BOX_1;
			String switch_sync_query_Array[] = {switch_sync_query};
			logger.trace(5, "switch_sync_query : " + switch_sync_query);
			Collection castToCollection_nsn = new ArrayList();
			castToCollection_nsn.add(switch_sync_query_Array[0]);
			logger.trace(5,"collection castToCollection_nsn === "+castToCollection_nsn);
			try {
				foundSubnetworkNodes = getSubNtwksForSwitchId(castToCollection_nsn,switch_sync_query);
				logger.trace(5, "foundSubnetworkNodes : " + foundSubnetworkNodes);
			} catch(Exception e){
			    logger.log("Exception:" + e.getMessage());
			}
			return foundSubnetworkNodes;
		}
		//code ended for version 1.11
		else {
			logger.trace(5,"not a sync query");
			logger.trace(5, "context subsvc: " + subsvc);
			SampSubValue subValue = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
			Collection subsvcs = subValue.getSubSvcs();
			Subnetwork callserver = null;
			Collection ntwks = null;
			boolean ignoringStatus = false;
			boolean isMove = false;
			Map algParms = new HashMap();
			SelectionSubntwkTypeValue firstType = (SelectionSubntwkTypeValue) subntwkTypes.getSubntwkTypeSelectList().get(0);
			if (firstType != null) {
				algParms = firstType.getParms();
			} else {
				logger.trace(5,"The required subnetwork type list was empty "
						+ "returning an empty Collection of subnetworks.");
				return foundSubnetworkNodes;
			}

			// Added for Migrated Subscriber Change by AJ
			logger.trace(5,"===== Check whether Sub is Migrated!!! =====");
			logger.trace(3, "Order Parm Name : " + IS_MIGRATED_SUB);
			SampOrderValue ordr = (SampOrderValue) orderCtx.getParmAccess(ORDER);
			String mgrVal = ordr.getParmVal(IS_MIGRATED_SUB);
			logger.trace(3, "Value of Migrated Sub = " + mgrVal);
			{
				if (mgrVal != null && mgrVal.equalsIgnoreCase(Y))
					logger.trace(5, "Subscriber is Migrated ");
			}

			// Added for Activate VL Change by AJ
			logger.trace(5,"=====Check whether request is on Cisco for Activating VL =====");
			logger.trace(3, "Ordr Parm Nm : " + IS_ACTIVATE_VL_ORDER);
			String activateOrdr = ordr.getParmVal(IS_ACTIVATE_VL_ORDER);
			logger.trace(3, "Value of Activate Order parm = " + activateOrdr);
			// Entry Ends

			// Added for Reprovision VL Change by Prakash Patel
			logger.trace(5,"=====Check whether request is on Cisco for Reprovision VL =====");
			logger.trace(3, "Ordr Parm Nm : " + IS_REPROV_VL_ORDER);
			String reprovOrdr = ordr.getParmVal(IS_REPROV_VL_ORDER);
			logger.trace(3, "Value of Reprovision Order parm = " + reprovOrdr);
			// Entry Ends

			logger.trace(1, new StringBuilder().append("Selecting SSN for provisionable service with name \"").append(subsvc.getProvisionableSvcNm()).append("\"").toString());
			if ((orderCtx instanceof OrderContext.OrderJmfEvalContext)) {
				this.stimer = ((OrderContext.OrderJmfEvalContext) orderCtx).getStimer();
			} else {
				logger.alarm(AlarmLevel.eWarn,AlarmCategory.eAppl,"context is not order context: "
						+ "This bean is designedto select SSNs, which happens in an order context. "
						+ "How is this bean being invoked?");
				this.stimer = this.mystimer;
				this.stimer.setId(subsvc.getRef().toString());
				this.stimer.start(null, new String[0]);
			}
			this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".LoadBalancingPickAlgorithmBean-BEG").toString(),
					new String[0]);
			try {
				logger.trace(1,"=============VOICE SUBNETWORK TYPES !!!=====================");
				Collection subnetworkTypeList = subntwkTypes.getSubntwkTypeSelectList();
				if (logger.isTraceLevel(3)) {
					logger.trace(3, new StringBuilder().append("The initial size of subnetwork types is: ").append(subnetworkTypeList.size()).toString());
				}
				Set snwTypeNames = new HashSet(subnetworkTypeList.size());
				Iterator it = subnetworkTypeList.iterator();
				while (it.hasNext()) {
					snwTypeNames.add(((SelectionSubntwkTypeValue) it.next()).getSubntwkTypeNm());
				}
				if (logger.isTraceLevel(3)) {
					logger.trace(1, new StringBuilder().append("The Set of subnetwork types is: ").append(snwTypeNames.toString()).toString());
				}
				searchCriteria.addSubnetworkType(CALL_SERVER);
				searchCriteria.addSubnetworkNameLike(null);
				if (subsvc.getSvcNm().equalsIgnoreCase("smp_resi_voice_port")) {
					logger.trace(5,"Voice port service found : ");
					String nvp = subsvc.getParmVal(NUM_OF_VOICE_LINES);
					logger.trace(5,"No of Voice lines :" + nvp);
				}
				searchCriteria.addSubnetworkStatus(IN_SERVICE);

				// For De-activate VL Added by AJ
				SampSubValue sub_assoc = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
				String switch_id = subsvc.getParmVal(SWITCH_ID);
				logger.trace(6, "Switch ID on VL : ");
				//loop for Deactivate Voice line.
				if ((subsvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS) || subsvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS))
						&& (checkOldAssociationExists(sub_assoc, subsvc,DT_HAS_EQUIPMENT)) && (switch_id != null) && 
						(activateOrdr == null || activateOrdr.equalsIgnoreCase(N)) && (reprovOrdr == null || reprovOrdr.equalsIgnoreCase(N))) {

					logger.trace(5,"Request is Either Deactivate VL or Add VL");
					//String switch_id = subsvc.getParmVal(SWITCH_ID);
					if(switch_id != null){
						logger.trace(5,"Found Switch from VL svc so its a Deactivate Request");
					}
					logger.trace(6, "Switch subnetwork "+ switch_id+" present on VL service "+ subsvc.getSvcNm());
					foundSubnetworkNodes = getSubNtwksForSwitchId(subsvcs, switch_id);
					callserver = (Subnetwork) foundSubnetworkNodes.iterator().next();
					logger.trace(6,"Call server : : "+callserver.getName().toString());
					if (foundSubnetworkNodes != null && callserver != null){
						logger.trace(5," Request looks for Deactivate VL. So decrement the Count on " +callserver.getName().toString()+ " switch");
						updateResv(callserver);
					}
				}
				// For Add sub
				else if (subsvc.getSvcNm().equalsIgnoreCase(SAMP_SUB)|| subsvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)|| subsvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) {
					ignoringStatus = true;
					SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
					logger.trace(5,"Got the service state as: " + subsvc.getState());
					if (subsvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)|| subsvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) {
						subsvcs = sub.getSubSvcs();
						logger.trace(3, "SubSvc Is: " + subsvc);
						//String switch_id = subsvc.getParmVal(SWITCH_ID);
						String switch_type = subsvc.getParmVal(SWITCH_TYPE);
						String move_to = subsvc.getParmVal(MOVE_TO);
						// Check if it is a VL request for existing VOIP sub on the same VL
						// If yes then we need to get the value of the switch_id parameter from the service
						if (switch_id != null && !switch_id.equalsIgnoreCase("")) {
							if (move_to != null && !move_to.equalsIgnoreCase(NULL)) {
								logger.trace(5,"Switch Id is present and is a move scenario.");
								isMove = true;
							} // is a move scenario
							else {
								logger.trace(5,"Switch Id is present and is not a move scenario.");
								foundSubnetworkNodes = getSubNtwksForSwitchId(subsvcs, switch_id);
								callserver = (Subnetwork) foundSubnetworkNodes.iterator().next();
								if (foundSubnetworkNodes != null && callserver != null && (activateOrdr == null || activateOrdr.equalsIgnoreCase(N)) && (reprovOrdr == null || reprovOrdr.equalsIgnoreCase(N))) {
									logger.trace(3,"neither Activate ord nor reprovision order ");
									if (mgrVal != null && mgrVal.equalsIgnoreCase(Y)) {
										logger.trace(3, "Migrated Subscriber");
									} else {
										logger.trace(5,"As sub is Non-Migrated Increment the VL count from '"+ callserver.toString()+ "' subnetwork");
										updateResvLeft(callserver);
									}
								}
								this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".CLEANUP-END").toString(), new String[0]);
								this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".LoadBalancingPickAlgorithmBean-END").toString(), new String[0]);
								logger.trace(1,"selected the following subnetworks:... "+ foundSubnetworkNodes.size());
								excludeOutOfService(foundSubnetworkNodes,!ignoringStatus);
								logger.trace(5,"foundSubnetworkNodes size: "+ foundSubnetworkNodes.size());
								Iterator foundIter = foundSubnetworkNodes.iterator();
								while (foundIter.hasNext()) {
									Subnetwork currFound = (Subnetwork) foundIter.next();
									logger.trace(1, new StringBuilder().append("Selected Subnetwork Type: '").append(currFound.getSubnetworkTypeValue().getName()).append("' Name of Selected Subnetwork:").append(currFound.getName()).append("'").toString());
								}
								return foundSubnetworkNodes;
							} // not a move scenario
						} // switch_id is not null
						else {
							// If its not an update on the same VL of an existing sub then
							// we need to get the value from the previous VL using hint or from load balancing.
							if (snwTypeNames.contains(CALL_SERVER)) {
								logger.trace(5,"Selecting call server..............");
								// If hint present then we select the subnetwork from hint.
								// For voice_line hint will have some not null value.
								// If the vendor name of the subnetwork selected in
								// hint is not same as that of the vendor name on the service then its a move scenario and load
								// Based on the selected subnetwork increment the value of num_of_lines_provisioned parameter.
								List foundSBNTWKList = new ArrayList();
								ntwks = getSubNtwksForSubSvc(subsvcs, snwTypeNames,foundSBNTWKList);
								if ((ntwks != null) && (ntwks.size() != 0)) {
									logger.trace(5,"Picking from hints....");
									// Check if its a move scenario for BS to HiQ
									if (move_to != null && !move_to.equalsIgnoreCase(NULL)) {
										isMove = true;
									} else {
										Iterator hintsIter = ntwks.iterator();
										Subnetwork hint=null;
										while (hintsIter.hasNext()) {
											hint = (Subnetwork) hintsIter.next();
											if (CALL_SERVER.equals(hint.getSubnetworkType())) {
												callserver = hint;
												break;
											}
										}
										//SampSubValue sub_assoc = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
										if (callserver != null && checkAssociationExists(sub_assoc, subsvc,DT_HAS_EQUIPMENT)  && (activateOrdr == null || activateOrdr.equalsIgnoreCase(N)) && (reprovOrdr == null || reprovOrdr.equalsIgnoreCase(N))) {
											logger.trace(3,"neither Activate ord nor reprovision order ");
											logger.trace(3, "switch name =="+hint.getName());
											foundSubnetworkNodes = getSubNtwksForSwitchId(subsvcs, hint.getName());
											callserver = (Subnetwork) foundSubnetworkNodes.iterator().next();
											logger.trace(6,"Call server == "+callserver.getName().toString());
											if (callserver != null){
												if (mgrVal != null && mgrVal.equalsIgnoreCase(Y)) {
													logger.trace(3,"Migrated Subscriber");
												} else {
													logger.trace(5,"Updating the DB as association was found.");
													updateResvLeft(callserver);
												}
											}
										}
									}
								} // Got Hints
								else {
									// if hints are null then we need to apply load
									// balancing logic here and get the value.
									// Get the complete list of call server
									// subnetworks.
									// Get the parm values of in_service, vendor and
									// num_of_lines_provisioned
									// Select the subnetwork which has the least
									// value for num_of_lines_provisioned
									// Increment the value of
									// num_of_lines_provisioned parameter.
									logger.trace(5," Hint is null.... ");
									searchCriteria.addSubnetworkType(CALL_SERVER);
									searchCriteria.addSubnetworkNameLike(null);
									searchCriteria.addSubnetworkStatus(IN_SERVICE);
									SubnetworkArray callSvrSbntwkArray = findSubnetwork(null, searchCriteria);
									Iterator csIter = callSvrSbntwkArray.iterator();
									BigDecimal switchId = null;
									double selectedSwitchs_numVLProvisioned = 0;
									while (csIter.hasNext()) {
										Subnetwork currcs = (Subnetwork) csIter.next();
										String inService = currcs.getPropertyValue(IN_SERVICE);
										String numVLProvisioned = currcs.getPropertyValue(NUM_OF_LINES_PROVISIONED);
										String vendor = currcs.getPropertyValue(VENDOR);
										logger.trace(5,"The subnetwork parm values are: ");
										logger.trace(5,"In Service: " + inService+ " Number Of VL Provisioned: "+ numVLProvisioned + " vendor: "+ vendor);
										if (inService.equalsIgnoreCase(Y)) {
											if (switchId == null) {
												selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
												switchId = currcs.getSubnetworkId();
												callserver = currcs;
											} else if (selectedSwitchs_numVLProvisioned > Double.parseDouble(numVLProvisioned)) {
												selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
												switchId = currcs.getSubnetworkId();
												callserver = currcs;
											}
										}
										logger.trace(5,new StringBuilder().append("call server found... ").append(currcs).toString());
									} // iterator loop
									logger.trace(5,"The selected switch is:: "+ callserver);
									//SampSubValue sub_assoc = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
									if (callserver != null && checkAssociationExists(sub_assoc,subsvc, DT_HAS_EQUIPMENT) && (activateOrdr == null || activateOrdr.equalsIgnoreCase(N)) && (reprovOrdr == null || reprovOrdr.equalsIgnoreCase(N))) {
										logger.trace(3,"neither Activate ord nor reprovision order ");
										if (mgrVal != null && mgrVal.equalsIgnoreCase(Y)) {
											logger.trace(3, "Migrated Subscriber");
										} else {
											logger.trace(3,"As sub is Non-Migrated decrement the VL count from '"+ callserver+ "' subnetwork");
											updateResvLeft(callserver);
										}
									}
								} // Hints were null
								if (isMove) {
									// This scenario is when there is a move from BS
									// to HiQ OR
									// from HiQ to BS.
									logger.trace(5," Its a move scenario.... ");
									String migrateSwitch = migrationWindowSwitch();
									if(migrateSwitch != NULL)
									{
										foundSubnetworkNodes = getSubNtwksForSwitchId(subsvcs, migrateSwitch);
										callserver = (Subnetwork) foundSubnetworkNodes.iterator().next();
									}
									else
									{	
										searchCriteria.addSubnetworkType(CALL_SERVER);
										searchCriteria.addSubnetworkNameLike(null);
										searchCriteria.addSubnetworkStatus(IN_SERVICE);
										SubnetworkArray callSvrSbntwkArray = findSubnetwork(null, searchCriteria);
										Iterator csIter = callSvrSbntwkArray.iterator();
										BigDecimal switchId = null;
										double selectedSwitchs_numVLProvisioned = 0;
										while (csIter.hasNext()) {
											Subnetwork currcs = (Subnetwork) csIter.next();
											String inService = currcs.getPropertyValue(IN_SERVICE);
											String numVLProvisioned = currcs.getPropertyValue(NUM_OF_LINES_PROVISIONED);
											String vendor = currcs.getPropertyValue(VENDOR);
											logger.trace(5,"The subnetwork parm values are for subntwk "+ currcs.getName() + ":");
											logger.trace(5,"In Service: " + inService+ " Number Of VL Provisioned: "+ numVLProvisioned + " vendor: "+ vendor);
											if (vendor.equalsIgnoreCase(move_to)) {
												if (switchId == null) {
													selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
													switchId = currcs.getSubnetworkId();
													callserver = currcs;
												} else if (selectedSwitchs_numVLProvisioned > Double.parseDouble(numVLProvisioned)) {
													selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
													switchId = currcs.getSubnetworkId();
													callserver = currcs;
												}
											}
											logger.trace(5,new StringBuilder().append("call server found..... ").append(currcs).toString());
										}
									}
									logger.trace(5,"The selected switch is: "+ callserver);
									if (callserver != null && (activateOrdr == null || activateOrdr.equalsIgnoreCase(N)) && (reprovOrdr == null || reprovOrdr.equalsIgnoreCase(N))) {
										logger.trace(3,"neither  Activate ord nor reprovision order ");
										if (mgrVal != null && mgrVal.equalsIgnoreCase(Y)) {
											logger.trace(3, "Migrated Subscriber");
										} else {
											logger.trace(3,"As sub is Non-Migrated decrement the VL count from '"+ callserver+ "' subnetwork");
											// Increment the counter for the new instance selected
											updateResvLeft(callserver);
											// Decrement the counter for the old instance
											logger.trace(3,"As sub is Non-Migrated incremented the VL count from '"+ switch_id+ "' subnetwork");
											updateResv((Subnetwork) getSubNtwksForSwitchId(subsvcs, switch_id).iterator().next());
										}
									}
								} // is Move
								foundSubnetworkNodes.add(callserver);
								if ((foundSubnetworkNodes == null) || foundSubnetworkNodes.size() != subnetworkTypeList.size()) {
									logger.alarm(AlarmLevel.eError,AlarmCategory.eAppl,"Could not find all the requested subnetwork types!");
									logger.trace(5,"types found were",foundSubnetworkNodes);
									throw new SelectionPrcsException("LoadBalancingPickAlgorithmBean: "
											+"Could not find all requested subnetwork types!");
								}
								this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".CLEANUP-END").toString(), new String[0]);
								this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".LoadBalancingPickAlgorithmBean-END").toString(), new String[0]);
								logger.trace(1,"selected the following subnetworks:... "+ foundSubnetworkNodes.size());
								excludeOutOfService(foundSubnetworkNodes,!ignoringStatus);
								logger.trace(5,"foundSubnetworkNodes size: "+ foundSubnetworkNodes.size());
								Iterator foundIter = foundSubnetworkNodes.iterator();
								while (foundIter.hasNext()) {
									Subnetwork currFound = (Subnetwork) foundIter.next();
									logger.trace(1, new StringBuilder().append("Selected Subnetwork Type: '").append(currFound.getSubnetworkTypeValue().getName()).append(
									"'Name of Selected Subnetwork:").append(currFound.getName()).append("'").toString());
								}
								return foundSubnetworkNodes;
							} // call server
						} // Else for not an existing VOIP sub
						if (isMove) {
							// This scenario is when there is a move from BS to HiQ OR from HiQ to Bs
							String migrateSwitch = migrationWindowSwitch();
							if(migrateSwitch != NULL)
							{
								foundSubnetworkNodes = getSubNtwksForSwitchId(subsvcs, migrateSwitch);
								callserver = (Subnetwork) foundSubnetworkNodes.iterator().next();
							}
							else
							{								
								logger.trace(5," Its a move scenario.... ");
								searchCriteria.addSubnetworkType(CALL_SERVER);
								searchCriteria.addSubnetworkNameLike(null);
								searchCriteria.addSubnetworkStatus(IN_SERVICE);
								SubnetworkArray callSvrSbntwkArray = findSubnetwork(null, searchCriteria);
								Iterator csIter = callSvrSbntwkArray.iterator();
								BigDecimal switchId = null;
								double selectedSwitchs_numVLProvisioned = 0;
								while (csIter.hasNext()) {
									Subnetwork currcs = (Subnetwork) csIter.next();
									String inService = currcs.getPropertyValue(IN_SERVICE);
									String numVLProvisioned = currcs.getPropertyValue(NUM_OF_LINES_PROVISIONED);
									String vendor = currcs.getPropertyValue(VENDOR);
									logger.trace(5,"The subnetwork parm values are for subntwk "+ currcs.getName() + ":");
									logger.trace(5,"In Service: " + inService+ " Number Of VL Provisioned: "+ numVLProvisioned + " vendor: " + vendor);
									if (vendor.equalsIgnoreCase(move_to)) {
										if (switchId == null) {
											selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
											switchId = currcs.getSubnetworkId();
											callserver = currcs;
										} else if (selectedSwitchs_numVLProvisioned > Double.parseDouble(numVLProvisioned)) {
											selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
											switchId = currcs.getSubnetworkId();
											callserver = currcs;
										}
									}
									logger.trace(5,new StringBuilder().append("call server found..... ").append(currcs).toString());
								}
							}
							logger.trace(5,"The selected switch is: " + callserver);
							if (callserver != null  && (activateOrdr == null || activateOrdr.equalsIgnoreCase(N)) && (reprovOrdr == null || reprovOrdr.equalsIgnoreCase(N))) {
								logger.trace(3,"Neither Activate order nor reprovision order");
								if (mgrVal != null && mgrVal.equalsIgnoreCase(Y)) {
									logger.trace(3, "Migrated Subscriber");
								} else {
									logger.trace(3,"As sub is Non-Migrated decrement the VL count from '"+ callserver + "' subnetwork");
									// Increment the counter for the new instance selected
									updateResvLeft(callserver);
									// Decrement the counter for the old instance.
									updateResv((Subnetwork) getSubNtwksForSwitchId(subsvcs, switch_id).iterator().next());
								}
							}
						} // is Move
						foundSubnetworkNodes.add(callserver);
						if ((foundSubnetworkNodes == null)|| foundSubnetworkNodes.size() != subnetworkTypeList.size()) {
							logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,"Could not find all the requested subnetwork types!");
							logger.trace(5,"types found were", foundSubnetworkNodes);
							throw new SelectionPrcsException("LoadBalancingPickAlgorithmBean: "
									+ "Could not find all requested subnetwork types!");
						}

						this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".CLEANUP-END").toString(), new String[0]);
						this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".LoadBalancingPickAlgorithmBean-END").toString(),new String[0]);
						logger.trace(1, "selected the following subnetworks:... "+ foundSubnetworkNodes.size());
						excludeOutOfService(foundSubnetworkNodes, !ignoringStatus);
						logger.trace(5,"foundSubnetworkNodes size: "+ foundSubnetworkNodes.size());
						Iterator foundIter = foundSubnetworkNodes.iterator();
						while (foundIter.hasNext()) {
							Subnetwork currFound = (Subnetwork) foundIter.next();
							logger.trace(1, new StringBuilder().append("Selected Subnetwork Type: '").append(currFound.getSubnetworkTypeValue().getName())
									.append("'Name of Selected Subnetwork:").append(currFound.getName()).append("'").toString());
						}
						return foundSubnetworkNodes;
					} // dial tone service
					else {
						// Its sub service in context here.
						// if (hints == null) then we need to apply load balancing
						// logic here and get the value.
						// Get the complete list of call server subnetworks.
						// Get the parm values of in_service, vendor and
						// num_of_lines_provisioned
						// Select the subnetwork which has the least value for
						// num_of_lines_provisioned
						// The number of provisioned VL parameter is not updated
						// here in this context of samp sub service.
						logger.trace(5," Hint is null for sub.... ");
						searchCriteria.addSubnetworkType(CALL_SERVER);
						searchCriteria.addSubnetworkNameLike(null);
						searchCriteria.addSubnetworkStatus(IN_SERVICE);
						SubnetworkArray callSvrSbntwkArray = findSubnetwork(null,searchCriteria);
						logger.trace(5,"No of instances found: "+ callSvrSbntwkArray.size());
						Iterator csIter = callSvrSbntwkArray.iterator();
						BigDecimal switchId = null;
						double selectedSwitchs_numVLProvisioned = 0;
						while (csIter.hasNext()) {
							Subnetwork currcs = (Subnetwork) csIter.next();
							String inService = currcs.getPropertyValue(IN_SERVICE);
							String numVLProvisioned = currcs.getPropertyValue(NUM_OF_LINES_PROVISIONED);
							String vendor = currcs.getPropertyValue(VENDOR);
							logger.trace(5,"The subnetwork parm values are for subntwk "+ currcs.getName() + ":");
							logger.trace(5,"In Service: " + inService+ " Number Of VL Provisioned: "+ numVLProvisioned + " vendor: " + vendor);
							if (inService.equalsIgnoreCase(Y)) {
								if (switchId == null) {
									selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
									switchId = currcs.getSubnetworkId();
									callserver = currcs;
								} else if (selectedSwitchs_numVLProvisioned > Double.parseDouble(numVLProvisioned)) {
									selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
									switchId = currcs.getSubnetworkId();
									callserver = currcs;
								}
							}
						} // iterator loop
						logger.trace(5,"The selected switch is: " + callserver);
						foundSubnetworkNodes.add(callserver);
						if ((foundSubnetworkNodes == null) || foundSubnetworkNodes.size() != subnetworkTypeList.size()) {
							logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,"Could not find all the requested subnetwork types!");
							logger.trace(5,"types found were", foundSubnetworkNodes);
							throw new SelectionPrcsException("LoadBalancingPickAlgorithmBean: "
									+ "Could not find all requested subnetwork types!");
						}
						this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".CLEANUP-END").toString(), new String[0]);
						this.stimer.snap(new StringBuilder().append(RefOrderEvents.NETIMPACT).append(".LoadBalancingPickAlgorithmBean-END").toString(),new String[0]);
						logger.trace(1, "selected the following subnetworks:... "+ foundSubnetworkNodes.size());
						excludeOutOfService(foundSubnetworkNodes, !ignoringStatus);
						logger.trace(5,"foundSubnetworkNodes size: "+ foundSubnetworkNodes.size());
						Iterator foundIter = foundSubnetworkNodes.iterator();
						while (foundIter.hasNext()) {
							Subnetwork currFound = (Subnetwork) foundIter.next();
							logger.trace(1, new StringBuilder().append("Selected Subnetwork Type: '").append(currFound.getSubnetworkTypeValue().getName())
									.append("'Name of Selected Subnetwork:").append(currFound.getName()).append("'").toString());
						}
						return foundSubnetworkNodes;
					} // if samp_sub
				} // if samp sub or dial tone
				else {
					// If the service is not samp_sub service or voice line service then
					// Check if its a request on an old sub by checking if the Voice
					// service has the switch_id parameter populated on it.
					// OR if its a voip dial tone service then select the hard coded switch instance.
					// If not then its a new sub's device service then get the value from SSN table.
					// We do not increment the value of the no_of_lines_provisioned parameter in this case.
					SampSubValue sub = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
					subsvcs = sub.getSubSvcs();
					String switch_id_old = isAnExistingOldSub(sub, subsvc,subsvcs,snwTypeNames);
					logger.trace(6, "Switch Id of Old " + switch_id_old);
					if (switch_id_old != null) {
						logger.trace(6,"Found Switch Id");
						if(switch_id_old.equalsIgnoreCase("Telephone Number does not match with the telephone number of voice line")){
							logger.trace(5,"Found switch id telephone number");
							throw new TelephoneNumberException(switch_id_old);
						} else {
							foundSubnetworkNodes = getSubNtwksForSwitchId(subsvcs,switch_id_old);
						}
					} else {
						logger.trace(5,new StringBuilder().append("provisionable service is:").append(subsvc).append(" which is not samp_sub...").toString());
						List foundSBNTWKList = new ArrayList();
						foundSubnetworkNodes = getSubNtwksForSubSvc(subsvcs,snwTypeNames, foundSBNTWKList);
						if ((foundSubnetworkNodes != null) && (foundSubnetworkNodes.size() != 0)) {
							logger.trace(5,"Returning the network elements from SSN. Not walking topology");
						} else {
							throw new SelectionPrcsException("No network elements found from SSN...");
						}
					}
				}// not a sub or dial tone service
			}// try
			catch(TelephoneNumberException msg){
				logger.log("Inside Telephone Number Catch");
				throw msg;
			}
			catch (SelectionPrcsException spe) {
				logger.log("Inside SelectionPrcsException");
				throw spe;
			} catch (RemoteException re) {
				logger.log("Inside RemoteException");
				throw new SelectionPrcsException("Can not select subnetworks with voice walk algorithm!", re);
			}
			catch (Exception e) {
				logger.log("Inside Exception");
				throw new SelectionPrcsException("Can not select subnetworks with voice walk algorithm!", e);
			}
			logger.log("returing: " + foundSubnetworkNodes.size()+ " subnetworks..");
			return foundSubnetworkNodes;
		}
	}

	/***
	 * To select switch from migration window table
	 * @return The migrateSwitch
	 * @throws RemoteException
	 */
	private String migrationWindowSwitch() throws RemoteException {
		logger.trace(5,"migrationWindowSwitch method called");
		Connection conn = null;
		String migrateSwitch = NULL;
		String migrateWindowCheck = null;
		ResultSet rset = null;
		PreparedStatement selectStmt = null;
		try {
			// get connection
			conn = ResUtil.getNonXADbConnection();
			final String selectStmtStr = "Select * from CUST_MIGRATION_MOVE_CHECK";
			selectStmt = conn.prepareStatement(selectStmtStr);
			logger.trace(5, "SQL statement 1: " + selectStmt.toString());
			rset = selectStmt.executeQuery();
			while (rset.next()) {
				migrateWindowCheck = rset.getString(1);
				logger.trace(3, "migrateWindowCheck = " + migrateWindowCheck);
				if(migrateWindowCheck.equalsIgnoreCase(YES))
				{
					migrateSwitch = rset.getString(2);
					logger.trace(3, "migrateSwitch = " + migrateSwitch);
					break;
				}
			}
		} catch (SQLException se) {
		    logger.log(se.getMessage());
			throw new RemoteException(se.toString());
		} finally {
			if(rset != null){
				ResUtil.closeDBResource(rset);
			}
			if (selectStmt != null) {
				ResUtil.closeDBResource(selectStmt);
			}
			ResUtil.closeConnection(conn);
		}
		return migrateSwitch;
	}
	
	/***
	 * Increment VL count
	 * @param ssn
	 * @throws RemoteException
	 */
	private void updateResvLeft(Subnetwork ssn) throws RemoteException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement selectStmt = null;
		PreparedStatement stmt = null;
		try {
			// get connection
			conn = ResUtil.getNonXADbConnection();
			// lock the row for update
			logger.trace(3, "Select the row for update...");
			String selectStmtStr = "select val from subntwk_parm "
				+ "where parm_id = (select parm_id from parm where parm_nm = ?) "
				+ "and subntwk_id = ? for update";
			selectStmt = conn.prepareStatement(selectStmtStr);
			selectStmt.setString(1, "num_of_lines_provisioned");
			selectStmt.setString(2, ssn.getSubnetworkId().toString());
			logger.trace(3, "SQL statement 1: " + selectStmt.toString());
			rs = selectStmt.executeQuery();
			String curVal = null;
			while (rs.next()) {
				curVal = rs.getString("VAL");
				logger.trace(3, "Current num of lines provisioned = " + curVal);
				break;
			}
			// convert the string to double, and increase the weight
			double curValD = Double.valueOf(curVal).doubleValue();
			logger.trace(3, "Current num of lines provisioned in double: "+ curValD);
			double val = curValD + 1;
			logger.trace(3, "New num of lines provisioned in double: " + val);
			String newVal = String.valueOf(val);
			logger.trace(3, "New num of lines provisioned = " + newVal);
			// execute DB update statement
			logger.trace(3, "Update num of lines provisioned on CS...");
			String updtStmtStr = "update subntwk_parm set val = ? "
				+ "where parm_id = (select parm_id from parm where parm_nm = ?) "
				+ "and subntwk_id = ?";
			stmt = conn.prepareStatement(updtStmtStr);
			stmt.setString(1, newVal);
			stmt.setString(2, NUM_OF_LINES_PROVISIONED);
			stmt.setString(3, ssn.getSubnetworkId().toString());
			logger.trace(3, "SQL statement 2: " + stmt.toString());
			stmt.execute();
			// commit the change
			conn.commit();
		} catch (Exception e) {
		    logger.log(e.getMessage());
			throw new RemoteException(e.toString());
		} finally {
			if (rs != null) {
				ResUtil.closeDBResource(rs);
			}
			if (selectStmt != null) {
				ResUtil.closeDBResource(selectStmt);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
	}

	/***
	 * Decrement VL count
	 * @param ssn
	 * @throws RemoteException
	 */
	private void updateResv(Subnetwork ssn) throws RemoteException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement selectStmt = null;
		PreparedStatement stmt = null;
		try {
			// get connection
			conn = ResUtil.getNonXADbConnection();
			// lock the row for update
			logger.trace(3, "Select the row for update...");
			String selectStmtStr = "select val from subntwk_parm "
				+ "where parm_id = (select parm_id from parm where parm_nm = ?) "
				+ "and subntwk_id = ? for update";
			selectStmt = conn.prepareStatement(selectStmtStr);
			selectStmt.setString(1, "num_of_lines_provisioned");
			selectStmt.setString(2, ssn.getSubnetworkId().toString());
			logger.trace(3, "SQL statement 1: " + selectStmt.toString());
			rs = selectStmt.executeQuery();
			String curVal = null;
			while (rs.next()) {
				curVal = rs.getString("VAL");
				logger.trace(3, "Current num of lines provisioned = " + curVal);
				break;
			}
			// convert the string to double, and decrease the weight
			double curValD = Double.valueOf(curVal).doubleValue();
			logger.trace(3, "Current num of lines provisioned in double: "+ curValD);
			double val = curValD - 1;
			logger.trace(3, "New num of lines provisioned in double: " + val);
			String newVal = String.valueOf(val);
			logger.trace(3, "New num of lines provisioned = " + newVal);
			// execute DB update statement
			logger.trace(3, "Update num of lines provisioned on CS...");
			String updtStmtStr = "update subntwk_parm set val = ? "
				+ "where parm_id = (select parm_id from parm where parm_nm = ?) "
				+ "and subntwk_id = ?";
			stmt = conn.prepareStatement(updtStmtStr);
			stmt.setString(1, newVal);
			stmt.setString(2, NUM_OF_LINES_PROVISIONED);
			stmt.setString(3, ssn.getSubnetworkId().toString());
			logger.trace(3, "SQL statement 2: " + stmt.toString());
			stmt.execute();
			// commit the change
			conn.commit();
		} catch (Exception e) {
		    logger.log(e.getMessage());
			throw new RemoteException(e.toString());
		} finally {
			if (rs != null) {
				ResUtil.closeDBResource(rs);
			}
			if (selectStmt != null) {
				ResUtil.closeDBResource(selectStmt);
			}
			if (stmt != null) {
				ResUtil.closeDBResource(stmt);
			}
			ResUtil.closeConnection(conn);
		}
	}
/***
 * 
 * @param sub
 * @param subSvc
 * @param subsvcs
 * @param snwTypeNames
 * @return the SwitchID
 * @throws Exception
 */
	private String isAnExistingOldSub(SampSubValue sub, SampSubSvcValue subSvc, Collection subsvcs,Set snwTypeNames) throws Exception {
		String result = null;
		SampSubSvcValue voiceSubSvc = null;
		// If the service in context is the port service then via association get the voice line service
		// and if the service in context is the switch service then via the parent get the sibling service.
		if (subSvc.getSvcNm().equalsIgnoreCase(EMTA_VOICE_PORT) || subSvc.getSvcNm().equalsIgnoreCase(SMP_RESI_VOICE_PORT)) {
			voiceSubSvc = getAEndSvcFrmZEndSvc(sub, subSvc, DT_HAS_EQUIPMENT);
			result = voiceSubSvc.getParmVal(SWITCH_ID);
		}
		if (subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)|| subSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) {
			voiceSubSvc = subSvc;
			result = voiceSubSvc.getParmVal(SWITCH_ID);
		}
		if (subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_FEATURE) || subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_FEATURE_PKG_STD)) {
			logger.trace(5,"In switch feature Service");
			SampSubSvcValue service = null;
			Iterator itr = sub.getSubSvcs().iterator();
			while (itr.hasNext()) {
				logger.trace(5,"In sub service iterators");
				service = (SampSubSvcValue) itr.next();
				if (service.getSvcNm().equalsIgnoreCase("SMP_SWITCH_DIAL_TONE_ACCESS")) {
					logger.trace(5,"got smp switch dial tone access ");
					result = service.getParmVal(SWITCH_ID);
					if (result != null) {
						logger.trace(5,"result =  "+result);
						break;
					}
				} 
				else {
					logger.trace(3,"Found the following service: "+ service.getSvcNm());
				}
			}
		}
		//Added by Prakash Patel
		//Code added For Event Based Service
		if (subSvc.getSvcNm().equalsIgnoreCase(SMP_EVENT_SWITCH_FEATURE_PKG) || subSvc.getSvcNm().equalsIgnoreCase(QUERY_SS)) {
			logger.trace(5,"Got event based service");
			logger.trace(5, "Total sub service " + sub.getSubSvcs().size());
			SampSubSvcValue service = null;
			SampSubSvcValue svc = null;
			Iterator itr = sub.getSubSvcs().iterator();
			String telephone=subSvc.getParmVal(TELEPHONE_NUMBER);
			while (itr.hasNext()) {
				logger.trace(5,"In sub service iterators");
				service = (SampSubSvcValue) itr.next();
				if (service.getSvcNm().equalsIgnoreCase("smp_voice_line")) {
					logger.trace(5,"got smp voice line ");
					svc = getIfChildService(service, telephone);
					if (svc != null) {
						logger.trace(5,"found svc :"+svc);
						result = svc.getParmVal(SWITCH_ID);
						break;
					} else {
						logger.trace(5,"svc is null");
					}
				} 
				else {
					logger.trace(5,"Found the following service: "+ service.getSvcNm());
				}
			}
			if(result != null) {
				logger.trace(5,"result is not null");
				return result;
			} else {
				result="Telephone Number does not match with the telephone number of voice line";
				logger.trace(5,"Result=="+result);
				//throw new TelephoneNumberException(result);
			}
		}
		if (subSvc.getSvcNm().equalsIgnoreCase(VOIP_DIAL_TONE)) {
			logger.trace(5,"In Voip Dial Tone");
			result = BREEZZ_PORTAONE_1;
			logger.trace(5,"Switch id set to breezz_portaone_1");
		}

		if (subSvc.getSvcNm().equalsIgnoreCase(SMP_INTERNET_ACCESS)|| subSvc.getSvcNm().equalsIgnoreCase(INTERNET_ACCESS)) {
			logger.trace(5,"In Internet Service");
			SampSubSvcValue service = null;
			Iterator itr = sub.getSubSvcs().iterator();
			while (itr.hasNext()) {
				logger.trace(5,"In sub service iterators");
				service = (SampSubSvcValue) itr.next();
				if (service.getSvcNm().equalsIgnoreCase("SMP_SWITCH_DIAL_TONE_ACCESS")) {
					logger.trace(5,"got smp switch dial tone access ");
					if (service.getParmVal("move_to") != null && !service.getParmVal("move_to").equalsIgnoreCase(NULL)) {
						logger.trace(5,"internet service in move scenario.");
						result = getSwitchInMove(subsvcs,snwTypeNames);
						if (result != null) {
							logger.trace(5,"Result for internet =  "+result);
							break;
						}
					}
					else
					{
						result = service.getParmVal(SWITCH_ID);
						if (result != null) {
							logger.trace(5,"result =  "+result);
							break;
						}
					}
				} 
				else {
					logger.trace(3,"Found the following service: "+ service.getSvcNm());
				}
			}
		}
		
		if (subSvc.getSvcNm().equalsIgnoreCase(EMTA_DEVICE_CONTROL)|| subSvc.getSvcNm().equalsIgnoreCase(SMP_RESI_DEVICE_CONTROL)) {
			logger.trace(5,"In emta device control");
			SampSubSvcValue service = null;
			Iterator itr = sub.getSubSvcs().iterator();
			while (itr.hasNext()) {
				logger.trace(5,"In sub service iterators");
				service = (SampSubSvcValue) itr.next();
				if (service.getSvcNm().equalsIgnoreCase("SMP_SWITCH_DIAL_TONE_ACCESS")) {
					logger.trace(5,"got smp switch dial tone access ");
						result = service.getParmVal(SWITCH_ID);
						if (result != null) {
							logger.trace(5,"result =  "+result);
							break;
						}
				} 
				else {
					logger.trace(3,"Found the following service: "+ service.getSvcNm());
				}
			}
		}
		
		logger.trace(5,"Switch Id : " + result);
		return result;
	}


	public class TelephoneNumberException extends Exception {
		private String msg;
		public TelephoneNumberException(String msg) {
			this.msg = msg;
		}
		public String toString(){
			logger.trace(5, "Inside Telephone Number Exception!!!");
			return msg;
		}
	}

	/***
	 * 
	 * @param service
	 * @param telephoneNumber
	 * @return The child service from a telephoneNumber
	 */
	private SampSubSvcValue getIfChildService(SampSubSvcValue service,String telephoneNumber) {
		logger.trace(5,"In Child Service");
		Iterator itr = service.getChildSubSvcLst().iterator();
		logger.trace(5, "size : " + service.getChildSubSvcLst().size());
		SampSubSvcValue subSvc = null;
		SampSubSvcValue svc = null;
		while (itr.hasNext()) {
			subSvc = (SampSubSvcValue) itr.next();
			logger.trace(5, "while subSvc : " + subSvc.getSvcNm()+ "subSvc.getSubSvcKey()  " + subSvc.getSubSvcKey());
			if (((subSvc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) || (subSvc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS))) && subSvc.getParmVal(TELEPHONE_NUMBER) != null) {
				logger.trace(5,"telephone number is not null and inside if");
				String telNo = subSvc.getParmVal(TELEPHONE_NUMBER);
				logger.trace(5, "telephone number from smp_voice_line : " + telNo);
				if (telNo.equalsIgnoreCase(telephoneNumber)) {
					logger.trace(5,"both telephone number is same");
					svc = subSvc;
					return svc;
				}
			}
		}
		return svc;
	}

	/***
	 * This method will return the Z svc from a association with a the svc A
	 * @param sub
	 * @param subSvc
	 * @param assocName
	 * @return subsvc
	 */
	private static SampSubSvcValue getAEndSvcFrmZEndSvc(SampSubValue sub,SampSubSvcValue subSvc, String assocName) {
		SampSubSvcValue aEndSvc = null;
		if (subSvc != null) {
			logger.trace(3, "Got the following association type: " + assocName);
			if (assocName.equalsIgnoreCase(DT_HAS_EQUIPMENT)) {
				if ((subSvc.getSvcNm().equalsIgnoreCase(EMTA_VOICE_PORT) || subSvc.getSvcNm().equalsIgnoreCase(SMP_RESI_VOICE_PORT))) {
					Collection assoc_list = sub.queryAssociationRegistries(DT_HAS_EQUIPMENT, subSvc.getSubSvcKey());
					if (assoc_list != null) {
						for (Iterator itr4 = assoc_list.iterator(); itr4.hasNext();) {
							SampSubEntityAssoc assoc1 = (SampSubEntityAssoc) itr4.next();
							aEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc1.getAEndEntityKey());
							logger.trace(3, "Got the following subsvc val: "+ aEndSvc);
							break;
						}
					}
				}
			}
		}
		return aEndSvc;
	}
	
	/***
	 * 
	 * @param sub
	 * @param subSvc
	 * @param assocName
	 * @return true/false if the association exists
	 */
	private static boolean checkAssociationExists(SampSubValue sub,SampSubSvcValue subSvc, String assocName) {
		logger.trace(3, "Inside checkAssociation");
		logger.trace(3, "SubSvc name = " + subSvc.getSvcNm()+ " current is A End Service");
		Collection col = subSvc.getEntityAssociationsByType(assocName);
		if (col != null && col.size() != 0) {
			logger.trace(3, "Size = " + col.size() + "  for association type = "+ assocName);
			return true;
		}
		return false;
	}
	
	/***
	 * 
	 * @param sub
	 * @param subSvc
	 * @param assocName
	 * @return true/false if the old association exists
	 */
	private static boolean checkOldAssociationExists(SampSubValue sub,SampSubSvcValue subSvc, String assocName) {

		SampSubSvcValue oldzend = null;
		logger.trace(3, "Inside checkOldAssociationExists");
		logger.trace(3, "SubSvc name = " + subSvc.getSvcNm()+ " currently is A End Service");
		Collection col = subSvc.getEntityAssociationsByType(assocName);
		logger.trace(3, "Size = " + col.size() + "  for association type = "+ assocName);
		if (col != null && col.size() != 0) {
			logger.trace(5, "DT Assoc. exist");
			for (Iterator itr = col.iterator(); itr.hasNext();) {
				SampSubEntityAssoc assoc = (SampSubEntityAssoc) itr.next();
				logger.trace(5,"Name of the Association :"+ assoc.getAssociationKey());
				SampSubEntityKey key = assoc.getAEndEntityKey();
				logger.trace(5, "Assoc A end Entity Key :" + key);
				SampSubEntityKey zkey = assoc.getZEndEntityKey();
				logger.trace(5, "Assoc Z end Entity Key : " + zkey);
				logger.trace(5, "Z end Svc Name : " + zkey.getEntitySpecNm());
				if (key == null)
					continue;
				SampSubSvcValue assocSubSvc = (SampSubSvcValue) sub.getEntityByKey(key);
				oldzend = (SampSubSvcValue) sub.getEntityByKey(zkey);
				logger.trace(5, "getAssociationChanges : "+ assocSubSvc.getAssociationChanges().toString());
				SampSubEntityAssocChgs assocChngCol = subSvc.getAssociationChanges();
				logger.trace(5, "assocChngCol : " + assocChngCol);
				logger.trace(5, "assocChngCol.getChanges()   : "+ assocChngCol.getChanges().toString());
				String action = (String) assocChngCol.getChanges().get(assoc);
				logger.trace(5, "Association Action : " + action);
				logger.trace(5,"Prov status : " +subSvc.getProvisionStatus());
				if(subSvc.getProvisionStatus().equalsIgnoreCase("deleted") || subSvc.getProvisionStatus().equalsIgnoreCase("delete_in_progress")){
					logger.trace(6, "Request is for service delete as prov status is deleted");
					return true;
				}
			}
			return false;
		} else {
			logger.trace(5, "Assoc Collection is empty for VL as size = "+ col.size() + " So, request is for Deactivate VL");
			SampSubEntityAssocChgs assocChngCol = subSvc.getAssociationChanges();
			logger.trace(5, "assocChngCol.getChanges()   : "+ assocChngCol.getChanges().toString());
			String assocAction = (String) assocChngCol.getChanges().toString();
			String action = assocAction.substring(assocAction.lastIndexOf("=")+1, assocAction.length()-1);
			logger.trace(5, "Association Action : " + action);
			if (action.equalsIgnoreCase(DELETE)){
				return true;
			}
			return false;
		}
	}
	
	/***
	 * 
	 * @param startFrom
	 * @param criteria
	 * @return 
	 * @throws SelectionPrcsException
	 * @throws RemoteException
	 */
	private SubnetworkArray findSubnetwork(Subnetwork startFrom,SubnetworkSearchCriteria criteria) throws SelectionPrcsException,RemoteException {
		SubnetworkArray results = null;
		try {
			if (this.topologyModel == null) {
				try {
					TopologyModelHome home = ServiceLocator.getTopologyModelHome();
					this.topologyModel = home.create();
				} catch (Exception se) {
				    logger.log(se.getMessage());
					throw new RemoteException(new StringBuilder().append("LoadBalancingPickAlgorithmBean: Could not find TopologyModel bean home: ").append(se).toString());
				}
			}
			results = this.topologyModel.findSubnetworks(startFrom, criteria);
		} catch (SmpUnknownException e) {
		    logger.log(e.getMessage());
			throw new SelectionPrcsException(new StringBuilder().append("LoadBalancingPickAlgorithmBean: Error finding networks matching ").append(criteria).toString(), e);
		} catch (TopologyPrcsException e) {
		    logger.log(e.getMessage());
			throw new SelectionPrcsException(new StringBuilder().append("LoadBalancingPickAlgorithmBean: Error finding networks matching ").append(criteria).toString(), e);
		}
		return results;
	}
	/***
	 * 
	 * @param sntwks
	 * @param enforce
	 * @return The excluded subnetwork
	 * @throws Exception
	 */
	private Collection excludeOutOfService(Collection sntwks, boolean enforce) throws Exception {
		if (!enforce) {
			return sntwks;
		}
		for (Iterator cit = sntwks.iterator(); cit.hasNext();) {
			Subnetwork sntwk = (Subnetwork) cit.next();
			if (!sntwk.getStatus().equalsIgnoreCase(IN_SERVICE)) {
				logger.trace(5,new StringBuilder().append("Subnetwork ").append(sntwk.getName()).append(" is out of service. Excluded").toString());
				cit.remove();
			}
		}
		return sntwks;
	}
	/***
	 * 
	 * @param subsvcsString
	 * @param switch_id
	 * @return subNtwks for a Switch ID
	 * @throws Exception
	 */
	
	private Collection getSubNtwksForSwitchId(Collection subsvcsString,String switch_id) throws Exception {
		logger.trace(3, "Inside getSubnetworkForSwitchId method :");
		Collection sntwks = new HashSet();
		SubnetworkSearchCriteria searchCriteria = new SubnetworkSearchCriteria();
		searchCriteria.addSubnetworkType(CALL_SERVER);
		searchCriteria.addSubnetworkStatus(IN_SERVICE);
		searchCriteria.addSubnetworkNameLike(switch_id);
		SubnetworkArray callSvrSbntwkArray = findSubnetwork(null,searchCriteria);
		for (Iterator itr = callSvrSbntwkArray.iterator(); itr.hasNext();) {
			Subnetwork subNtwk = (Subnetwork) itr.next();
			logger.trace(3, "Got SN as: " + subNtwk.getName());
			String typeName = subNtwk.getSubnetworkTypeValue().getName();
			String name = subNtwk.getName();
			logger.trace(5,"Switch name for VL " + name + "Switch_id for VL " + switch_id);
			if (CALL_SERVER.contains(typeName) && name.equalsIgnoreCase(switch_id)) {
				logger.trace(5,"adding " + typeName + " name " + name);
				sntwks.add(subNtwk);
				break;
			}
		}
		logger.trace(5,"Switch_id for VL" + sntwks.toString().toString());
		return sntwks;
	}
	/***
	 * 
	 * @param subsvcs
	 * @param snwTypeNames
	 * @param gwcList
	 * @return subNtwks for a subscriber service
	 * @throws Exception
	 */
	private Collection getSubNtwksForSubSvc(Collection subsvcs,Set snwTypeNames, List gwcList) throws Exception {
		logger.trace(5,"Inside getSubNtwksFromSubSvc");
		Collection sntwks = new HashSet();
		SampSubSvcKey samp_sub_svc_key = null;
		logger.trace(3, "Searching from sub");
		for (Iterator itr = subsvcs.iterator(); itr.hasNext();) {
			SampSubSvcValue svc = (SampSubSvcValue) itr.next();
			if (svc.getSvcNm().equalsIgnoreCase("samp_sub")) {
				logger.trace(5,"found samp_sub service in subModel...");
				samp_sub_svc_key = svc.getSubSvcKey();
				break;
			}
		}
		logger.trace(3, "Searching from dial tone");
		List dtSubSvc = new ArrayList();
		for (Iterator itr = subsvcs.iterator(); itr.hasNext();) {
			SampSubSvcValue svc = (SampSubSvcValue) itr.next();
			if (svc.getSvcNm().equalsIgnoreCase(SMP_DIAL_TONE_ACCESS) || svc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)) {
				logger.trace(5,"found dial tone service in subModel...");
				dtSubSvc.add(svc);
			}
		}
		if (dtSubSvc != null && dtSubSvc.size() > 1) {
			logger.trace(3, "Got more than one dial tone service");
			SampSubSvcValue dt_svc_1 = (SampSubSvcValue) dtSubSvc.get(0);
			logger.trace(5, "Found 1st VL Service " + dt_svc_1);
			SampSubSvcKey dt_svc_key_1 = dt_svc_1.getSubSvcKey();
			String switch_id = (String) dt_svc_1.getParmVal(SWITCH_ID);
			if (switch_id == null) {
				SampSubSvcValue dt_svc_2 = (SampSubSvcValue) dtSubSvc.get(1);
				SampSubSvcKey dt_svc_key_2 = dt_svc_2.getSubSvcKey();
				switch_id = (String) dt_svc_2.getParmVal(SWITCH_ID);
				logger.trace(5, "Found 2nd VL Service " + dt_svc_2);
				logger.trace(3, "Samp Sub Service :" + dt_svc_2 + "("+ dt_svc_key_2 + ")" + "Present on Switch id "+ switch_id);
				// This if is for add 2nd VL when 1st VL is not Pre-activated.
				if (switch_id == null) {
					logger.trace(5,"As Switch id doesnt present on any VL using Hint of Samp Sub");
					logger.trace(3, "Sub Svc key using Hint : "+ samp_sub_svc_key);
					BigDecimal subSvcId = new BigDecimal(samp_sub_svc_key.getInventoryId());
					logger.trace(5,new StringBuilder().append("subSvcId using Hint: ").append(subSvcId).toString());
					SSNAgent ssnAgent = getSSNAgent();
					logger.trace(5,"getSSNAgent"+ssnAgent);
					try
					{
						logger.trace(5,"trying to get hints");
						Map roleMap = ssnAgent.getSnwsRoles(subSvcId);
						logger.trace(5,"roleMap"+roleMap);
						logger.trace(5,new StringBuilder().append("Role Map: ").append(roleMap.toString()).toString());
						Collection fullSntwks = roleMap.keySet();
						logger.trace(5,new StringBuilder().append("Size of replicated subntwk elements: ").append(fullSntwks.size()).toString());
						for (Iterator itr = fullSntwks.iterator(); itr.hasNext();) {
							Subnetwork subntwk = (Subnetwork) itr.next();
							logger.trace(5,"Subntwk found from role Map "+ subntwk.getName());
							String typeName = subntwk.getSubnetworkTypeValue().getName();
							if (snwTypeNames.contains(typeName)) {
								sntwks.add(subntwk);
								logger.trace(5,"adding " + typeName + " from samp_sub SSN definition and Name is :"+subntwk.getName());
							}
						}
					}
					catch(Exception e)
					{
						logger.log("Exception caught and hint is not present");
						sntwks = getSubNtwksId();
					}
					// return sntwks;
				} 
				else {
					logger.trace(5, "In else calling getSubNtwksForSwitchId");
					sntwks = getSubNtwksForSwitchId(subsvcs, switch_id);
				}
			} else {
				logger.trace(5, "calling getSubNtwksForSwitchId");
				sntwks = getSubNtwksForSwitchId(subsvcs, switch_id);
				// return sntwks;
			}
			return sntwks;
		} else {
			logger.trace(5,"Got the Sub svc key : " + samp_sub_svc_key);
			BigDecimal subSvcId = new BigDecimal(samp_sub_svc_key.getInventoryId());
			logger.trace(5,new StringBuilder().append("subSvcId: ").append(subSvcId).toString());
			SSNAgent ssnAgent = getSSNAgent();
			logger.trace(5,"ssnAgent"+ssnAgent);
			try
			{
				logger.trace(5,"trying to get hint");
				Map roleMap = ssnAgent.getSnwsRoles(subSvcId);
				logger.trace(5,"roleMap"+roleMap);
				logger.trace(5,new StringBuilder().append("roleMap: ").append(roleMap.toString()).toString());
				Collection fullSntwks = roleMap.keySet();
				logger.trace(5,new StringBuilder().append("Size of replicated subntwk elements: ").append(fullSntwks.size()).toString());
				for (Iterator itr = fullSntwks.iterator(); itr.hasNext();) {
					Subnetwork subNtwk = (Subnetwork) itr.next();
					String typeName = subNtwk.getSubnetworkTypeValue().getName();
					if (snwTypeNames.contains(typeName)) {
						sntwks.add(subNtwk);
						logger.trace(5,"adding " + typeName	+ " from samp_sub SSN definition and Name is :"+subNtwk.getName());
					}
				}
			}
			catch(Exception e)
			{
				logger.log("Exception caught and hint is not present");
				sntwks=getSubNtwksId();
			}
			
			return sntwks;
		}
	}
	/***
	 * 
	 * @return subNtwksID
	 * @throws Exception
	 */
	private Collection getSubNtwksId() throws Exception {
		logger.trace(5," Hint is null for voip subscriber.... ");
		Collection sntwks = new HashSet();
		Subnetwork subntwk = null;
		SubnetworkSearchCriteria searchCriteria = new SubnetworkSearchCriteria();
		searchCriteria.addSubnetworkType(CALL_SERVER);
		searchCriteria.addSubnetworkNameLike(null);
		searchCriteria.addSubnetworkStatus(IN_SERVICE);
		SubnetworkArray callSvrSbntwkArray = findSubnetwork(null,searchCriteria);
		logger.trace(5,"No of instances found: "+ callSvrSbntwkArray.size());
		Iterator csIter = callSvrSbntwkArray.iterator();
		BigDecimal switchId = null;
		double selectedSwitchs_numVLProvisioned = 0;
		while (csIter.hasNext()) {
			Subnetwork currcs = (Subnetwork) csIter.next();
			String inService = currcs.getPropertyValue(IN_SERVICE);
			String numVLProvisioned = currcs.getPropertyValue(NUM_OF_LINES_PROVISIONED);
			String vendor = currcs.getPropertyValue(VENDOR);
			logger.trace(5,"The subnetwork parm values are for subntwk "+ currcs.getName() + ":");
			logger.trace(5,"In Service: " + inService+ " Number Of VL Provisioned: "+ numVLProvisioned + " vendor: " + vendor);
			if (inService.equalsIgnoreCase(Y)) {
				if (switchId == null) {
					selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
					switchId = currcs.getSubnetworkId();
					subntwk = currcs;
				} else if (selectedSwitchs_numVLProvisioned > Double.parseDouble(numVLProvisioned)) {
					selectedSwitchs_numVLProvisioned = Double.parseDouble(numVLProvisioned);
					switchId = currcs.getSubnetworkId();
					subntwk = currcs;
				}
			}
		} // iterator loop
		logger.trace(5,"The selected subnetwork is: " + subntwk);
		sntwks.add(subntwk);
		logger.trace(5,"Switch_id for VL" + sntwks.toString().toString());
		return sntwks;
	}

	/***
	 * 
	 * @param subsvcs
	 * @param snwTypeNames
	 * @return get the SSN in voice line
	 * @throws Exception
	 */

	private String getSwitchInMove(Collection subsvcs,Set snwTypeNames) throws Exception {
		logger.trace(5,"getSwitchInMove");
		String result = null;
		Collection sntwks = new HashSet();
		SampSubSvcKey dial_tone_svc_key = null;
		logger.trace(3, "Searching dial tone");
		for (Iterator itr = subsvcs.iterator(); itr.hasNext();) {
			SampSubSvcValue svc = (SampSubSvcValue) itr.next();
			if (svc.getSvcNm().equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS)) {
				logger.trace(5,"found dial tone service in subModel...");
				dial_tone_svc_key = svc.getSubSvcKey();
				break;
			}
		}
			logger.trace(5,"Got the dial tone key : " + dial_tone_svc_key);
			BigDecimal diaToneSvcId = new BigDecimal(dial_tone_svc_key.getInventoryId());
			logger.trace(5,new StringBuilder().append("diaToneSvcId: ").append(diaToneSvcId).toString());
			SSNAgent ssnAgent = getSSNAgent();
			logger.trace(5,"ssnAgent"+ssnAgent);
			try
			{
				logger.trace(5,"trying to get hint");
				Map roleMap = ssnAgent.getSnwsRoles(diaToneSvcId);
				logger.trace(5,"roleMap"+roleMap);
				logger.trace(5,new StringBuilder().append("roleMap: ").append(roleMap.toString()).toString());
				Collection fullSntwks = roleMap.keySet();
				logger.trace(5,new StringBuilder().append("Size of replicated subntwk elements: ").append(fullSntwks.size()).toString());
				for (Iterator itr = fullSntwks.iterator(); itr.hasNext();) {
					Subnetwork subNtwk = (Subnetwork) itr.next();
					String typeName = subNtwk.getSubnetworkTypeValue().getName();
					if (snwTypeNames.contains(typeName)) {
						sntwks.add(subNtwk);
						String name = subNtwk.getName();
						result=name;
						logger.trace(5,"adding " + typeName	+ " from dial tone SSN definition and result is "+result);
					}
				}
			}
			catch(Exception e)
			{
				logger.log("Exception caught and ssn not found from voice line ");
				result = null;
			}
			
			return result;
		
	}
	
	/***
	 * 
	 * @return the SSNAgent
	 * @throws SmpResourceException
	 */
	private SSNAgent getSSNAgent() throws SmpResourceException {
		SSNAgent ssnAgent = null;
		try {
			SSNAgentHome ssn_AgentHome = (SSNAgentHome) SSNServiceLocator.getInstance().getHome(2);
			ssnAgent = ssn_AgentHome.create();
		} catch (Exception ex) {
			logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,new StringBuilder().append("Failed finding SSNAgent ").append(ex).toString(), ex);
			throw new SmpResourceException(ex.getMessage(), ex);
		}
		return ssnAgent;
	}
}
