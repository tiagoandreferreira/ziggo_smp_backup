package com.sigma.samp.imp.ziggo.loadBalancer;

public class LoadBalancingPickAlgorithmBeanConst {

    public static final String CONTEXT_PARM_SUBMODEL          = "sub";
    public static final String CALL_SERVER                    = "call_server";
    public static final String IN_SERVICE                     = "in_service";
    public static final String SAMP_SUB                       = "samp_sub";
    public static final String SMP_SWITCH_DIAL_TONE_ACCESS    = "smp_switch_dial_tone_access";
    public static final String SMP_DIAL_TONE_ACCESS           = "smp_dial_tone_access";
    public static final String INTERNET_ACCESS                = "internet_access";
    public static final String SMP_INTERNET_ACCESS            = "smp_internet_access";
    public static final String SWITCH_ID                      = "switch_id";
    public static final String SWITCH_TYPE                    = "switch_type";
    public static final String MOVE_TO                        = "move_to";
    public static final String VENDOR                         = "vendor";
    public static final String NUM_OF_LINES_PROVISIONED       = "num_of_lines_provisioned";
    public static final String EMTA_VOICE_PORT                = "emta_voice_port";
    public static final String SMP_RESI_VOICE_PORT            = "smp_resi_voice_port";
    public static final String DT_HAS_EQUIPMENT               = "dt_has_equipment";
    public static final String SMP_SWITCH_FEATURE_PKG_STD     = "smp_switch_feature_pkg_std";
    public static final String SMP_SWITCH_FEATURE             = "smp_switch_feature";
    public static final String SMP_EVENT_SWITCH_FEATURE_PKG   = "smp_event_switch_feature_package";
    public static final String SMP_RESI_DEVICE_CONTROL        = "smp_resi_device_control";
    public static final String EMTA_DEVICE_CONTROL            = "emta_device_control";
    public static final String VOIP_DIAL_TONE                 = "voip_dial_tone";
    public static final String BREEZZ_PORTAONE_1              = "Breezz_PortaOne_1";
    public static final String BOX_1                          = "box_1";
    public static final String IS_MIGRATED_SUB                = "migrate_order";
    public static final String IS_ACTIVATE_VL_ORDER           = "activate_order";
    public static final String IS_REPROV_VL_ORDER             = "reprov_order";
    public static final String TELEPHONE_NUMBER               = "telephone_number";
    public static final String QUERY_SS                       = "query_ss";
    public static final String QUERY_NSN                      = "query_nsn";
    public static final String LOADBALANCINGPICKALGORITHMBEAN = "LoadBalancingPickAlgorithmBean";
    public static final String NUM_OF_VOICE_LINES             = "num_of_voice_lines";
    public static final String NULL                           = "null";
    public static final String ORDER                          = "order";
    public static final String Y                              = "y";
    public static final String N                              = "n";
    public static final String YES                            = "yes";
    public static final String DELETE                         = "delete";

}
