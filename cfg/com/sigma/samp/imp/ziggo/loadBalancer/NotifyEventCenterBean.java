/*
* This software is the confidential and proprietary information of Sigma
* Systems Group (Canada) Inc. ("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Sigma.
*
*  FILE INFO
* $Id:$
*
* SAMP 1.0
*
* REVISION HISTORY
* Based on CVS log
*/
package com.sigma.samp.imp.ziggo.loadBalancer;

import com.sigma.hframe.jlog.*;
import com.sigma.hframe.jerror.*;
import com.sigma.vframe.jnotify.*;
import com.sigma.vframe.joss.SampEvent;
import javax.ejb.*;
import java.util.*;
import javax.naming.*;


public class NotifyEventCenterBean implements RemoteNotifyEventCenterBean {
  private final static Log logger=LogFactory.create(NotifyEventCenterBean.class.getName());
  private Map procClassMap=new HashMap();

  public NotifyEventCenterBean() {
  }
  /**
   * Get a dedicated event processor and forward the event to it
   * @param event
   * @return
   * @throws SmpException
   */
  public boolean processEvent(SampEvent event) throws SmpException{
    NotifyEventProcessor proc=null;
    try {
      if (logger.isTraceLevel(3))
        logger.trace(3,"get a event:"+event.toString());
      Class procClass =
          NotifyEventCenterBean.class.getClassLoader().loadClass(
          "com.sigma.samp.imp.ziggo.loadBalancer.OrderStateNotifyPreProcessor");
      //not expected event
      if (procClass==null) {
        logger.trace(6,"Can't find a processor for this event type: OrderStateNotifyPreProcessor");
        return false;
      }
      proc=(NotifyEventProcessor)procClass.newInstance();
    }
    catch (Exception e) {
      logger.log(e.getMessage());
      throw new SmpPrcsException(e);
    }
    if (logger.isTraceLevel(3))
      logger.trace(3,"forward to processor:"+proc.getClass().getName());
    proc.notify(event);
    return true;
  }

  
  public void setSessionContext(SessionContext ctx) {
    //sessionCtx=ctx;
  };
  public void ejbRemove() {};
  public void ejbCreate() {};
  public void ejbActivate() {};
  public void ejbPassivate() {};

}
