/*
* This software is the confidential and proprietary information of Sigma
* Systems Group (Canada) Inc. ("Confidential Information").  You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with Sigma.
*
*  FILE INFO
* $Id$
*
* SAMP 1.0
*
* REVISION HISTORY
* Based on CVS log
*/

package com.sigma.samp.imp.ziggo.loadBalancer;

import static com.sigma.samp.imp.ziggo.loadBalancer.OrderStateNotifyPreProcessorConst.*;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.ejb.ObjectNotFoundException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sigma.hframe.jerror.SmpException;
import com.sigma.hframe.jerror.SmpResourceException;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;

import com.sigma.vframe.jcmn.ParmEvalContext;
import com.sigma.vframe.jnotify.NotifyEventProcessor;
import com.sigma.vframe.joss.SampEvent;
import com.sigma.vframe.util.ResUtil;

import com.sigma.samp.cmn.order.RefOrderStatus;
import com.sigma.samp.cmn.order.SampLineItemStateChangeEvent;
import com.sigma.samp.cmn.stm.ImpactPrcsException;
import com.sigma.samp.cmn.stm.TopologyPrcsException;
import com.sigma.samp.stm.cmn.Subnetwork;
import com.sigma.samp.stm.ssn.SSN;
import com.sigma.samp.stm.ssn.SSNAgent;
import com.sigma.samp.stm.ssn.SSNAgentBean;
import com.sigma.samp.stm.ssn.SSNAgentHome;
import com.sigma.samp.stm.ssn.SSNServiceLocator;

import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubMgr;
import com.sigma.samp.cmn.sub.SampSubMgrLocator;
import com.sigma.samp.cmn.sub.SampSubSvcKey;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.cmn.sub.SubKeyFactory;


public class OrderStateNotifyPreProcessor
    implements NotifyEventProcessor {

  private ParmEvalContext evalContext;


  private SSNAgent ssn_Agent;
  private Context ic = null;
  private final static Log logger = new Log(OrderStateNotifyPreProcessor.class.
                                            getName());
  private boolean VERBOSE = true;
  private static HashMap rollbackOrder = new HashMap();
  private static Integer IMPACT_ORDER = new Integer(1);
  private static Integer REVERSE_ORDER = new Integer( -1);
  static {
    rollbackOrder.put(SSN.ACTIVE + ":" + CANCEL, IMPACT_ORDER);
    rollbackOrder.put(SSN.ACTIVE + ":" + SUCCESS, IMPACT_ORDER);
    rollbackOrder.put(SSN.INVALID + ":" + CANCEL, IMPACT_ORDER);
    rollbackOrder.put(SSN.INVALID + ":" + SUCCESS, IMPACT_ORDER);
    rollbackOrder.put(SSN.ADD_IN_PROGRESS + ":" + CANCEL, REVERSE_ORDER);
    rollbackOrder.put(SSN.UPDATE_IN_PROGRESS + ":" + CANCEL, IMPACT_ORDER);
    rollbackOrder.put(SSN.DELETE_IN_PROGRESS + ":" + CANCEL, IMPACT_ORDER);
    rollbackOrder.put(SSN.ACTIVE + ":" + CANCEL, IMPACT_ORDER);
    rollbackOrder.put(SSN.ADD_IN_PROGRESS + ":" + SUCCESS, IMPACT_ORDER);
    rollbackOrder.put(SSN.UPDATE_IN_PROGRESS + ":" + SUCCESS, IMPACT_ORDER);
    rollbackOrder.put(SSN.DELETE_IN_PROGRESS + ":" + SUCCESS, IMPACT_ORDER);
    rollbackOrder.put(SSN.ACTIVE + ":" + SUCCESS, IMPACT_ORDER);
  }

  private void log(String s) {
    if (VERBOSE) {
      logger.log(s);
    }
  }

  private void trace3(String s) {
    if (logger.isTraceLevel(3)) {
      logger.trace(3, s);
    }
  }

   /**
   * constructor of NTCS2KOrderStateNotifyPreProcessor
   *  @throws SmpResourceException - if couldn't find InitialContext
   */
  public OrderStateNotifyPreProcessor() throws SmpResourceException {
    try {
      ic = new InitialContext();
    }
    catch (NamingException e) {
      logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,
                   "Can not get initial context" + e, e);
      throw new SmpResourceException(e.getExplanation(), e);
    }
  }

  /**
   * constructor of NTCS2KOrderStateNotifyPreProcessor
   *  @param Context  if ReprovTrack located in different context
   */
  public OrderStateNotifyPreProcessor(Context ic) {
    this.ic = ic;
  }

  /***
   * 
   * @param event
   * @return Line item info
   * @throws SmpException
   */
  private ItemInfo[] getLineItemInfo(SampLineItemStateChangeEvent event) throws
      SmpException {
    String[] itemKey = event.getLineItemKeySet();
    ArrayList itemInfoC = new ArrayList();
    for (int i = 0; i < itemKey.length; i++) {
      ItemInfo itemInfo = new ItemInfo();
      Properties props = event.getLineItemProperties(itemKey[i]);
      if (logger.isTraceLevel(3)) {
        logger.trace(3, "Samp lineItem properties:" + props);
      }
      String itemSeq = (String) props.get(NOTIFYSEQNUM);
      String itemStatus = (String) props.get(STATUS);
      String itemSubSvcId = (String) props.get(ENTITYID);
      logger.trace(3,
              "lineItemId=" + itemKey[i] + ",notifySeqNum=" + itemSeq + ",status=" +
              itemStatus + ",subSvcId=" + itemSubSvcId);
      if (itemKey[i] == null || itemSeq == null || itemSubSvcId == null ) {
          logger.trace(3, "Required value/s doesn't carry the valid data, lineItemId=" + itemKey[i] + 
                  ",notifySeqNum=" + itemSeq + 
                  ",subSvcId=" + itemSubSvcId);
          continue;
      }
      
      try {
          itemInfo.seqNum = Integer.parseInt(itemSeq);
          itemInfo.subSvcId = new BigDecimal(itemSubSvcId);
          itemInfo.lineItemId = new BigDecimal(itemKey[i]);
      } catch (NumberFormatException e) {
          logger.trace(3, "Required value/s doesn't carry the valid data, lineItemId=" + itemKey[i] + 
                  ",notifySeqNum=" + itemSeq + 
                  ",subSvcId=" + itemSubSvcId);
          continue;
      }
      
      if (itemStatus.startsWith(RefOrderStatus.ABORTED)) {
        itemInfo.action = CANCEL;
      }
      else {
        itemInfo.action = SUCCESS;
      }
      try {
        SSNAgent ssnAgent = getSSN_Agent();
        SSN ssn = ssnAgent.getSsnBySubSvc(itemInfo.subSvcId );
        String ssnStatus = ssn.getStatus();
        String key = ssnStatus + ":" + itemInfo.action;
        Integer seqAdj = (Integer) rollbackOrder.get(key);
        if (logger.isTraceLevel(3)) {
          logger.trace(3,
                       "get a seq factor: " + seqAdj + " (key=" + key + ")");
        }
        itemInfo.seqNum *= seqAdj.intValue();
      }
      catch (ObjectNotFoundException onfe) {
        logger.trace(3,"STM doesn't have such ssn, skip");
        continue;
      }
      catch (Exception ex) {
        logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,
                     "Fail to get ssn status for subSvc:" + itemInfo.subSvcId, ex);
        throw new SmpException(ex);
      }
      itemInfoC.add(itemInfo);
    }
    Collections.sort(itemInfoC, new Comparator() {
      public int compare(Object o1, Object o2) {
        ItemInfo item1 = (ItemInfo) o1;
        ItemInfo item2 = (ItemInfo) o2;
        if (item1.seqNum > item2.seqNum) {
          return 1;
        }
        else if (item1.seqNum < item2.seqNum) {
          return -1;
        }
        else {
          return 0;
        }
      }
    }
    );
    ItemInfo[] itemInfos = new ItemInfo[itemInfoC.size()];
    itemInfoC.toArray(itemInfos);
    return itemInfos;
  }
  public void notify(SampEvent eventObj) throws SmpException {
    if (SampLineItemStateChangeEvent.class.isInstance(eventObj))
      notifyLineItemEvent(eventObj);
    else
      throw new SmpException("Unknown event type:"+eventObj.getClass().getName());
  }
/***
 * 
 * @param eventObj
 * @throws SmpException
 */
  public void notifyLineItemEvent(SampEvent eventObj) throws SmpException {
    SampLineItemStateChangeEvent event = (SampLineItemStateChangeEvent)
        eventObj;
    if (logger.isTraceLevel(3)) {
      trace3("get SampLineItemStateChangeEvent:" + event.getAttributeNames());
      trace3(eventObj.toString());
    }
    logger.trace(3, "Event properties :"+event.getEventProperties().toString());
    String apiClientId=(String)event.getEventProperties().get(
        "order.apiClientId");
    if (logger.isTraceLevel(3)) {
      trace3("the apiClientId =" + apiClientId);
    }
    
    ItemInfo[] itemInfos = getLineItemInfo(event);

    if (itemInfos == null ||itemInfos.length==0) {
      logger.trace(3,"There isn't any line item need to be process.");
      return;
    }

    for (int i = 0; i < itemInfos.length; i++) {
      if (logger.isTraceLevel(3)) {
        trace3(itemInfos[i].action + " subSvc:" + itemInfos[i].subSvcId);
      }
      commitProvision(itemInfos[i].subSvcId, itemInfos[i].lineItemId,
                      itemInfos[i].action);
    }
  }

/***
 * 
 * @param subSvcId
 * @return
 * @throws Exception
 */
  private SampSubSvcValue getSvcBySubSvcId(BigDecimal subSvcId)
  throws Exception{
        Context ic = new InitialContext();
        SampSubMgr subMgr = SampSubMgrLocator.locate(ic);
        SampSubSvcKey subSvcKey = SubKeyFactory.getInstance().makeSubSvcKey(new Long(subSvcId.longValue()));
        SampSubSvcValue svc = (SampSubSvcValue) subMgr.getSubSvc(subSvcKey);
    return svc;
  }
  
 
  /**
   ** When SPM receives a reply from SB for provision order, we rollback the
   ** failed subsvcs and update the project status or just update status for
   ** succeeded ones.
   */
  private void commitProvision(BigDecimal subsvcId,
                               BigDecimal lineItemId,
                               String action) throws ImpactPrcsException {
    log("Preparing for finalizing provision order ...");


    if (subsvcId == null) {
      logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,
                   "SubSvc Id can not be null!");
      throw new ImpactPrcsException("SubSvc Id can not be null " +
                                    "when finalizing reprovision order!");
    }
    if (action == null || action.length() == 0) {
      logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,
                   "Provision commit action can not be null!");
      throw new ImpactPrcsException(
          "Provision commit action can not be null!");
    }

    try {
      SSNAgent ssnAgent = getSSN_Agent();
      SampSubSvcValue subSvc = getSvcBySubSvcId(subsvcId);
      String svcName = subSvc.getSvcNm();
      String provSvc = subSvc.getProvisionableSvcNm();
      if (provSvc == null) {
          logger.trace(3, "Not a provisionable service, ignore...");
          return;
      }
      logger.trace(3, "Provisionable service: " + provSvc);

      log("Pre-committing the subsvc " + subsvcId + " of service " + svcName +
          " in action " + action);
      if (action.trim().compareTo(SUCCESS) == 0) {
        log("Before committing the subsvc.");
        if (svcName.equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS) || 
                        svcName.equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) {
          SSN ssn = ssnAgent.getSsnBySubSvc(subsvcId);
          String status = ssn.getStatus();
          trace3("SSN status: " + status);
          if (status.compareTo(SSNAgentBean.SSN_DEL_STATUS) == 0) {
            trace3("In delete");
            Collection snwc = ssnAgent.getSnwsByType(subsvcId, CALL_SERVER);
            Subnetwork cs = (Subnetwork) (snwc.iterator().next());                    
            logger.log("Got the Call Server Instance As: " + cs.getSubnetworkId());
            logger.log("Sub Svc is "+ subSvc);
            if (checkAssociationExists(subSvc, DT_HAS_EQUIPMENT)){
                        logger.log("Association exists!! but no need to decrement count should already get decremented by LBBean");
                        //commented for deactivate VL 
                        //updateResvLeft(cs);
            }
            else
            {
                        logger.log("Association does not exist!!");
            }
          }
        }
      }
      if (action.trim().compareTo(CANCEL) == 0) {
        log("Before rollback the subsvc:" + subsvcId);
        if (svcName.equalsIgnoreCase(SMP_SWITCH_DIAL_TONE_ACCESS) || 
                        svcName.equalsIgnoreCase(SMP_DIAL_TONE_ACCESS)) {
          SSN ssn = ssnAgent.getSsnBySubSvc(subsvcId);
          String status = ssn.getStatus();
          if (status.compareTo(SSNAgentBean.SSN_ADD_STATUS) == 0) {
            trace3("In dd rollack");
            Collection snwc = ssnAgent.getSnwsByType(subsvcId, CALL_SERVER);
            Subnetwork cs = (Subnetwork) (snwc.iterator().next());
            logger.log("Got the Call Server Instance As: " + cs.getSubnetworkId());
            SampSubValue sub_assoc = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
            logger.log("Sub Assoc"+sub_assoc);
            logger.log("Sub Svc is "+ subSvc);
            if (checkAssociationExists(subSvc, DT_HAS_EQUIPMENT)){
                        logger.log("Association exists!!");
                        updateResvLeft(cs);
            }
            else
            {
                        logger.log("Association does not exist!!");
            }
          }
        }
      }
      else {
        log("Ignore the notification. Action = " + action);
      }
    }
    catch (ImpactPrcsException ipe) {
      log("Got STMImpact error: " + ipe.getMessage());

      ipe.printStackTrace();
      throw ipe;
    }
    catch (SmpResourceException se) {
      log("Got SMP Resource error: " + se.getMessage());

      se.printStackTrace();
      throw new ImpactPrcsException
          ("SMP Resource exception while Finalizing reprov order " +
           se, se);
    }
    catch (TopologyPrcsException te) {
      log("Got SSN error: " + te.getMessage());

      te.printStackTrace();
      throw new ImpactPrcsException
          ("Topology exception on ssn while Finalizing reprov order " +
           te, te);
    }
    catch (Exception e) {
      log("Got other error: " + e.getMessage());

      e.printStackTrace();
      throw new ImpactPrcsException
          ("Can not process reprov jms message " + e, e);
    }
  }

  /**
   * get SSNAgent remote interface if found
   * throws STMImpactException
   */
  private SSNAgent getSSN_Agent() throws
      SmpResourceException {

    trace3("lookup for SSN_AgentHome");

    if (ssn_Agent == null) {
      try {
        SSNAgentHome ssn_AgentHome =
            (SSNAgentHome) SSNServiceLocator.getInstance().getHome(
            SSNServiceLocator.Services.SSN_AGENT);
        // get remote interface
        ssn_Agent = ssn_AgentHome.create();
      }
      catch (Exception ex) {
        logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,
                     "Failed finding SSNAgent " + ex, ex);
        throw new SmpResourceException(ex.getMessage(), ex);
      }
    }

    return ssn_Agent;

  }
  
   /***
   * Added logic for Delete Activated
   * @param subSvc
   * @param assocName
   * @return
   */
  private static boolean checkAssociationExists(SampSubSvcValue subSvc, String assocName) {      
      logger.trace(3, "Inside checkAssociation of Listener :");
      logger.trace(3, "SubSvc name = " + subSvc.getSvcNm() + " current is A End Service");
      Collection c = subSvc.getEntityAssociationsByType(assocName);
      if (c != null && c.size()!=0) {
          logger.trace(3, "Size = " + c.size() + "  for association type = " + assocName);
          return true;          
      }
      return false;
  }

  /***
   * Decrement VL Count
   * @param ssn
   * @throws RemoteException
   */
  private void updateResvLeft(Subnetwork ssn) 
          throws RemoteException
  {          
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement selectStmt = null;
    PreparedStatement stmt = null;
    try {
      // get connection
      conn = ResUtil.getNonXADbConnection();      
      // lock the row for update
      logger.trace(3, "Select the row for update...");
      String selectStmtStr = "select val from subntwk_parm "
          + "where parm_id = (select parm_id from parm where parm_nm = ?) " 
          + "and subntwk_id = ? for update";
      selectStmt = conn.prepareStatement(selectStmtStr);
      selectStmt.setString(1, NUM_OF_LINES_PROVISIONED);
      selectStmt.setString(2, ssn.getSubnetworkId().toString());
      logger.trace(3, "SQL statement 1: " + selectStmt.toString());
      rs = selectStmt.executeQuery();
      String curVal = null;
      while (rs.next()) {
          curVal = rs.getString("VAL");
          logger.trace(3, "Current num of lines provisioned = " + curVal);
          break;
      }
      
      
      // convert the string to double, and decrease the weight
      double curValD = Double.valueOf(curVal).doubleValue();
      logger.trace(3, "Current num of lines provisioned in double: " + curValD);
      double val = curValD - 1;
      logger.trace(3, "New num of lines provisioned in double: " + val);
      String newVal = String.valueOf(val);
      logger.trace(3, "New num of lines provisioned = " + newVal);
      
      // execute DB update statement
      logger.trace(3, "Update num of lines provisioned on CS...");
      String updtStmtStr = 
          "update subntwk_parm set val = ? "
          + "where parm_id = (select parm_id from parm where parm_nm = ?) " 
          + "and subntwk_id = ?";
      stmt = conn.prepareStatement(updtStmtStr);
      stmt.setString(1, newVal);
      stmt.setString(2, NUM_OF_LINES_PROVISIONED);
      stmt.setString(3, ssn.getSubnetworkId().toString());
      logger.trace(3, "SQL statement 2: " + stmt.toString());
      stmt.execute();
      
      // commit the changes
      conn.commit();

    }
    catch (Exception e) {
      throw new RemoteException(e.toString());
    }
    finally {
      if (rs != null) {
          ResUtil.closeDBResource(rs);
      }
      if (selectStmt != null) {
          ResUtil.closeDBResource(selectStmt);
      }
      if (stmt != null) {
          ResUtil.closeDBResource(stmt);
      }
      ResUtil.closeConnection(conn);
    }
  }

  private static class ItemInfo {
    BigDecimal subSvcId;
    BigDecimal lineItemId;
    int seqNum;
    String action;
  }
}



