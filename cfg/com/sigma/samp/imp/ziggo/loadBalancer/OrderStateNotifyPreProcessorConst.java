package com.sigma.samp.imp.ziggo.loadBalancer;

public class OrderStateNotifyPreProcessorConst {

    public final static String SUCCESS = "success";
    public final static String CANCEL  = "cancel";

    public static final String NUM_OF_LINES_PROVISIONED    = "num_of_lines_provisioned";
    public static final String CALL_SERVER                 = "call_server";
    public static final String SMP_SWITCH_DIAL_TONE_ACCESS = "smp_switch_dial_tone_access";
    public static final String SMP_DIAL_TONE_ACCESS        = "smp_dial_tone_access";
    public static final String CONTEXT_PARM_SUBMODEL       = "sub";
    public static final String DT_HAS_EQUIPMENT            = "dt_has_equipment";
    public static final String NOTIFYSEQNUM                = "notifySeqNum";
    public static final String STATUS                      = "status";
    public static final String ENTITYID                    = "entityId";
    public static final String VAL                         = "VAL";

}
