package com.sigma.samp.imp.ziggo.pisysInstancePickAlgo;

import static com.sigma.samp.imp.ziggo.pisysInstancePickAlgo.PisysInstancePickAlgorithmBeanConst.*;

/* $Id PisysInstancePickAlgorithmBean.java ,v 1.0 2014/02/10 16:19:30 Prakash $ initial version $
   $Id PisysInstancePickAlgorithmBean.java ,v 1.1 2014/03/26 16:19:30 Prakash $ code added to handle move scenario (drop 2) $
   $Id PisysInstancePickAlgorithmBean.java ,v 1.2 2014/05/06 10:00:10 Prakash $ code added to handle delete smart card scenario (drop 2) $ */
import com.sigma.hframe.jerror.SmpUnknownException;
import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;
import com.sigma.samp.cmn.stm.SubnetworkSearchCriteria;
import com.sigma.samp.cmn.stm.TopologyPrcsException;
import com.sigma.samp.cmn.sub.SampSubEntityAssoc;
import com.sigma.samp.cmn.sub.SampSubSvcValue;
import com.sigma.samp.cmn.sub.SampSubValue;
import com.sigma.samp.stm.cmn.ServiceLocator;
import com.sigma.samp.stm.cmn.Subnetwork;
import com.sigma.samp.stm.cmn.SubnetworkArray;
import com.sigma.samp.stm.cmn.TopologyModel;
import com.sigma.samp.stm.cmn.TopologyModelHome;
import com.sigma.samp.stm.selection.SelectionAlgValue;
import com.sigma.samp.stm.selection.SelectionPrcsException;
import com.sigma.samp.stm.selection.SelectionSubntwkTypeValue;
import com.sigma.vframe.util.ResUtil;
import com.sigma.vframe.jcmn.ParmEvalContext;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

public class PisysInstancePickAlgorithmBean implements SessionBean {
	private static final long serialVersionUID = -6718229803181314480L;
	private SessionContext ctx;
	private static Log logger = new Log(PisysInstancePickAlgorithmBean.class.getName());

	private ParmEvalContext evalContext;
	private transient TopologyModel topologyModel;
	public void setSessionContext(SessionContext context)throws RemoteException, EJBException {
		this.ctx = context;
	}
	public void ejbActivate() throws RemoteException, EJBException {
	}
	public void ejbPassivate() throws RemoteException, EJBException {
	}
	public void ejbRemove() throws RemoteException, EJBException {		
	}
	public void ejbCreate() throws CreateException, RemoteException,EJBException {
	}
	
	/***
	 * 
	 * @param alreadySelected
	 * @param orderCtx
	 * @param subsvc
	 * @param subntwkTypes
	 * @param hints
	 * @returnThis collection will return pisys subnetwork instance
	 * @throws SelectionPrcsException
	 */
	public Collection selectSubntwk(final Collection alreadySelected,final ParmEvalContext orderCtx, final SampSubSvcValue subsvc,final SelectionAlgValue subntwkTypes, final Collection hints)throws SelectionPrcsException {
		logger.trace(5,"Version 1.2 Pisys Instance");
		logger.trace(5,new StringBuilder().append("The incoming subsvc : ").append(subsvc.getSvcNm()).toString());
		logger.trace(5,"Selecting subnetworks with custom video walk algorithm ...");
		this.evalContext = orderCtx;
		final Collection foundPisysInstance = new HashSet();
		String instanceFromSTB = null;
		final SubnetworkSearchCriteria searchCriteria = new SubnetworkSearchCriteria();
		final SampSubValue subValue = (SampSubValue) evalContext.getParmAccess(CONTEXT_PARM_SUBMODEL);
		final Collection subsvcs = subValue.getSubSvcs();
		Subnetwork videocas = null;
		boolean ignoringStatus = false;
		Map algParms = new HashMap();
		final SelectionSubntwkTypeValue firstType = (SelectionSubntwkTypeValue) subntwkTypes.getSubntwkTypeSelectList().get(0);
		if (firstType != null) {
			algParms = firstType.getParms();
		} else {
			logger.trace(5,"The required subnetwork type list was empty "
					+ "returning an empty Collection of subnetworks.");
			return foundPisysInstance;
		}
		logger.trace(1, new StringBuilder().append("Selecting SSN for provisionable service with name \"").append(subsvc.getProvisionableSvcNm()).append("\"").toString());
		try {
			logger.trace(1,"=============VIDEO SUBNETWORK TYPES !!!=====================");
			final Collection subnetworkTypeList = subntwkTypes.getSubntwkTypeSelectList();
			logger.trace(5, "get subnetworkTypeList");
			if (logger.isTraceLevel(3)) {
				logger.trace(3, new StringBuilder().append("The initial size of subnetwork types is: ").append(subnetworkTypeList.size()).toString());
			}
			final Set snwTypeNames = new HashSet(subnetworkTypeList.size());
			final Iterator itr = subnetworkTypeList.iterator();
			while (itr.hasNext()) {
				snwTypeNames.add(((SelectionSubntwkTypeValue) itr.next()).getSubntwkTypeNm());
			}
			if (logger.isTraceLevel(3)) {
				logger.trace(1, new StringBuilder().append("The Set of subnetwork types is: ").append(snwTypeNames.toString()).toString());
			}
			searchCriteria.addSubnetworkType(VIDEO_CAS);
			searchCriteria.addSubnetworkStatus(IN_SERVICE);
			searchCriteria.addSubnetworkNameLike(null);
			if (subsvc.getSvcNm().equalsIgnoreCase(STB_CAS) || subsvc.getProvisionableSvcNm().equalsIgnoreCase(SMP_CPE_CAS)) {
				instanceFromSTB = getSubNtwksForSubSvc(subsvcs);
				if ((instanceFromSTB != null) && isMove(subValue,subsvc) == false ) {
					videocas = findSubntwk(instanceFromSTB, searchCriteria);
				}
				else {
					ignoringStatus = true;
					logger.trace(5,"Got the service state as: " + subsvc.getState());
					String colour = subsvc.getParmVal(COLOUR);
					logger.trace(5, "Colour from service is : "+colour);
					String region = null;
					String fetchInstance= null;
					SampSubSvcValue videoAccessSvc = null;
					videoAccessSvc = getAEndSvcFrmZEndSvc(subValue,subsvc,VIDEO_SERVICE_DEFN_HAS_CAS);
					if(videoAccessSvc != null) {
						region = videoAccessSvc.getParmVal(REGION);
						logger.trace(5, "Region is : "+region);
					} else {
						logger.trace(5, "A End is not found");
					}
					if(region != null && colour != null) {
						colour = colour.toLowerCase();
						region = region.toLowerCase();
						logger.trace(5, "Region or Colour is not null");
						fetchInstance = getPisysInstance(region, colour);
						logger.trace(5, "Feteched Instance from DB is :"+fetchInstance);
						if(fetchInstance != null) {
							videocas = findSubntwk(fetchInstance, searchCriteria);
						} else {
							logger.trace(5,"Instace is not found from DB");
							throw new SelectionPrcsException("PisysInstancePickAlgorithmBean: "
									+ "Instance not found from DB as per region and colour combination");
						}
					} else {
						logger.trace(5, "Region or Colour is null");
						final SubnetworkArray callSvrSbntwkArray = findSubnetwork(null,searchCriteria);
						logger.trace(5,"No of instances found: "+ callSvrSbntwkArray.size());
						final Iterator csIter = callSvrSbntwkArray.iterator();
						while (csIter.hasNext()) {
							final Subnetwork currcs = (Subnetwork) csIter.next();
							videocas = currcs;
							logger.trace(5, "Picked 1st video cas instance");
							break;
						}
					}
				}
				logger.trace(5,"The Selected Pisys Instance is " + videocas);
				foundPisysInstance.add(videocas);
				if ((foundPisysInstance == null) || foundPisysInstance.size() != subnetworkTypeList.size()) {
					logger.alarm(AlarmLevel.eError, AlarmCategory.eAppl,"Could not find all the requested subnetwork types!");
					logger.trace(5,"types found were", foundPisysInstance);
					throw new SelectionPrcsException("PisysInstancePickAlgorithmBean: "
							+ "Could not find all requested subnetwork types!");
				}
				logger.trace(5, "selected the following subnetworks:... "+ foundPisysInstance.size());
				excludeOutOfService(foundPisysInstance, !ignoringStatus);
				logger.trace(5,"foundPisysInstance size: "+ foundPisysInstance.size());
				final Iterator foundIter = foundPisysInstance.iterator();
				while (foundIter.hasNext()) {
					final Subnetwork currFound = (Subnetwork) foundIter.next();
					logger.trace(1, new StringBuilder().append("Selected Subnetwork Type: '").append(currFound.getSubnetworkTypeValue().getName())
							.append("'Name of Selected Subnetwork:").append(currFound.getName()).append("'").toString());
				}
				return foundPisysInstance;
			} // if smp_cpe_cas
			else
			{
				logger.trace(5, "found "+subsvc.getSvcNm()+"service");
			} // Other then smp_cpe_cas service
		}// try
		catch (SelectionPrcsException spe) {
			logger.log("Inside SelectionPrcsException");
			throw spe;
		} catch (RemoteException re) {
			logger.log("Inside RemoteException");
			throw new SelectionPrcsException("Can not select subnetworks with voice walk algorithm!", re);
		}
		catch (Exception e) {
			logger.log("Inside Exception");
			throw new SelectionPrcsException("Can not select subnetworks with voice walk algorithm!", e);
		}
		logger.trace(5,"returing: " + foundPisysInstance.size()+ " subnetworks..");
		return foundPisysInstance;

	}
	
	/***
	 * 
	 * @param subValue
	 * @param subsvc
	 * @return This method is used in to identify whether it is move scenarios or not
	 */
	private boolean isMove(final SampSubValue subValue,final SampSubSvcValue subsvc) {
		logger.trace(5,"Inside isMove");
		boolean isMove = false;
		logger.trace(5,"Got the service state as: " + subsvc.getState());
		final String colour = subsvc.getParmVal(COLOUR);
		logger.trace(5, "Colour from service is : "+colour);
		String region = null;
		String oldRegion = null;
		SampSubSvcValue videoAccessSvc = null;
		videoAccessSvc = getAEndSvcFrmZEndSvc(subValue,subsvc,VIDEO_SERVICE_DEFN_HAS_CAS);
		if(videoAccessSvc != null) {
			region = videoAccessSvc.getParmVal(REGION);
			oldRegion = videoAccessSvc.getOldParmVal(REGION);
			logger.trace(5, "Region is : "+region);
			logger.trace(5, "Old Region is : "+oldRegion);
			if(region != null && oldRegion != null &&  !region.equalsIgnoreCase(oldRegion))
			{
				isMove=true;
			}
		} else {
			logger.trace(5, "A End is not found");
		}
		return isMove;
	}
	
	

	/***
	 * 
	 * @param sub
	 * @param subSvc
	 * @param assocName
	 * @return This method is used to get AEnd Service from ZEnd Service
	 */
	private static SampSubSvcValue getAEndSvcFrmZEndSvc(final SampSubValue sub,final SampSubSvcValue subSvc, final String assocName) {
		SampSubSvcValue aEndSvc = null;
		if (subSvc != null) {
			logger.trace(5, "Got the following association type: " + assocName);
			if (assocName.equalsIgnoreCase(VIDEO_SERVICE_DEFN_HAS_CAS)) {
				logger.trace(5, "static assoc name :  " + VIDEO_SERVICE_DEFN_HAS_CAS);
				if (subSvc.getSvcNm().equalsIgnoreCase(STB_CAS) || subSvc.getProvisionableSvcNm().equalsIgnoreCase(SMP_CPE_CAS)) {
					final Collection assoc_list = sub.queryAssociationRegistries(VIDEO_SERVICE_DEFN_HAS_CAS, subSvc.getSubSvcKey());
					logger.trace(5, "assoc list :  " + assoc_list);
					if (assoc_list != null) {
						for (final Iterator itr = assoc_list.iterator(); itr.hasNext();) {
							final SampSubEntityAssoc assoc1 = (SampSubEntityAssoc) itr.next();
							aEndSvc = (SampSubSvcValue) sub.getEntityByKey(assoc1.getAEndEntityKey());
							logger.trace(5,"Got the following subsvc val: "+ aEndSvc);
							break;
						}
					} else {
						logger.trace(5, "Assoc does not exits");
					}
				} else {
					logger.trace(5,"Smart card not found");
				}
			}
		}
		return aEndSvc;
	}

	
	/***
	 * 
	 * @param instanceId
	 * @param criteria
	 * @return This method is used to get particular subnetwork instance as per search criteria
	 * @throws SelectionPrcsException
	 * @throws RemoteException
	 */
	private Subnetwork findSubntwk(final String instanceId,final SubnetworkSearchCriteria criteria) throws SelectionPrcsException,RemoteException {
		Subnetwork currcs=null;
		final SubnetworkArray callSvrSbntwkArray = findSubnetwork(null,criteria);
		logger.trace(5,"No of instances found: "+ callSvrSbntwkArray.size());
		final Iterator csIter = callSvrSbntwkArray.iterator();
		while (csIter.hasNext()) {
			currcs = (Subnetwork) csIter.next();
			final String typeName = currcs.getSubnetworkTypeValue().getName();
			logger.trace(5,"The subnetwork name is : "+ currcs.getName()+" and subnetwork type is : "+typeName);
			if (VIDEO_CAS.contains(typeName) && currcs.getName().equalsIgnoreCase(instanceId)) {
				logger.trace(5, "Required Instance found");
				break;
			}
		}// iterator loop
		return currcs;
	}
	
	/***
	 * 
	 * @param startFrom
	 * @param criteria
	 * @return This method is used to get all the subnetwork instance as per search criteria
	 * @throws SelectionPrcsException
	 * @throws RemoteException
	 */
	private SubnetworkArray findSubnetwork(final Subnetwork startFrom,final SubnetworkSearchCriteria criteria) throws SelectionPrcsException,RemoteException {
		SubnetworkArray results = null;
		try {
			if (this.topologyModel == null) {
				try {
					final TopologyModelHome home = ServiceLocator.getTopologyModelHome();
					this.topologyModel = home.create();
				} catch (Exception se) {
				    logger.log("PisysInstancePickAlgorithmBean: Could not find TopologyModel bean home: "+se.getMessage());
					throw new RemoteException(new StringBuilder().append("PisysInstancePickAlgorithmBean: Could not find TopologyModel bean home: ").append(se).toString());
				}
			}
			results = this.topologyModel.findSubnetworks(startFrom, criteria);
		} catch (SmpUnknownException e) {
		    logger.log("PisysInstancePickAlgorithmBean: Error finding networks matching " + criteria.toString()+e.getMessage());
			throw new SelectionPrcsException(new StringBuilder().append("PisysInstancePickAlgorithmBean: Error finding networks matching ").append(criteria).toString(), e);
		} catch (TopologyPrcsException e) {
		    logger.log("PisysInstancePickAlgorithmBean: Error finding networks matching " + criteria.toString()+e.getMessage());
			throw new SelectionPrcsException(new StringBuilder().append("PisysInstancePickAlgorithmBean: Error finding networks matching ").append(criteria).toString(), e);
		}
		return results;
	}

	/***
	 * 
	 * @param sntwks
	 * @param enforce
	 * @return This method is used to exclude out of service subnetwork instance
	 * @throws Exception
	 */
	private Collection excludeOutOfService(final Collection sntwks, final boolean enforce) throws Exception {
		if (!enforce) {
			logger.trace(5, "Enforce :"+enforce);
			return sntwks;
		}
		for (final Iterator cit = sntwks.iterator(); cit.hasNext();) {
			final Subnetwork sntwk = (Subnetwork) cit.next();
			if (!sntwk.getStatus().equalsIgnoreCase(IN_SERVICE)) {
				logger.trace(5,new StringBuilder().append("Subnetwork ").append(sntwk.getName()).append(" is out of service. Excluded").toString());
				cit.remove();
			}
		}
		return sntwks;
	}
	
	/***
	 * 
	 * @param region
	 * @param colour
	 * @return This method is used to fetch subnetwork name from DB as per region and colour combination
	 * @throws RemoteException
	 */
	private String getPisysInstance(final String region, final String colour)
	throws RemoteException {
		logger.trace(5,"getSubntwk function");
		Connection conn = null;
		String instance = null;
		ResultSet rset = null;
		Statement stmt = null;
		try {
			conn = ResUtil.getNonXADbConnection();
			logger.trace(5, "conn=="+conn);
			stmt = conn.createStatement();
			rset = stmt
			.executeQuery("SELECT INSTANCE FROM CUST_PISYSINSTANCE WHERE LOWER(REGION)='"+region+"' AND LOWER(COLOUR)='"+colour+"'");
			while (rset.next()) {
				instance = rset.getString("INSTANCE");
				logger.trace(5,"Instance :" + instance);
			}
		} catch (SQLException se) {
			logger.trace(5,"Inside SQLException");
			logger.trace(5,"SQL Exception: " + se.getMessage());
			logger.trace(6, "Stack Trace: " + se.toString());
			logger.trace(6, "An exception occured while conecting to the DB.");
		}finally {
			if(rset != null){
				ResUtil.closeDBResource(rset);
			}
			if(stmt != null){
				ResUtil.closeDBResource(stmt);
			}ResUtil.closeConnection(conn);
		}
		return instance;
	}
	
	/***
	 * 
	 * @param subsvcs
	 * @return This method is used to fetch instance id from STB CAS
	 */
	private String getSubNtwksForSubSvc(final Collection subsvcs) {
		logger.trace(5,"Inside getSubNtwksFromSubSvc");
		String instance = null;
		logger.trace(5, "Searching from STB CAS");
		final List dtSubSvc = new ArrayList();
		for (final Iterator i = subsvcs.iterator(); i.hasNext();) {
			final SampSubSvcValue svc = (SampSubSvcValue) i.next();
			if (svc.getSvcNm().equalsIgnoreCase(STB_CAS) || svc.getProvisionableSvcNm().equalsIgnoreCase(SMP_CPE_CAS)) {
				logger.trace(5,"found STB CAS service in subModel...");
				dtSubSvc.add(svc);
			}
		}
		logger.trace(5, "dtSubSvc.size() is :"+dtSubSvc.size());
		//code change for version 1.2 (from dtSubSvc.size() > 1 to dtSubSvc.size() > 0)
		if (dtSubSvc != null && dtSubSvc.size() > 0) {
			logger.trace(3, "Got more than one STB CAS");
			for(int k=0;k<dtSubSvc.size();k++) {
				final SampSubSvcValue dt_svc = (SampSubSvcValue) dtSubSvc.get(k);
				logger.trace(5, "Found "+(k+1)+"st STB CAS " + dt_svc);
				final String instance_id = (String) dt_svc.getParmVal(INSTANCE_ID);
				if (instance_id != null) {
					instance = instance_id;
					break;
				}
			}
		}
		return instance;
	}
}
