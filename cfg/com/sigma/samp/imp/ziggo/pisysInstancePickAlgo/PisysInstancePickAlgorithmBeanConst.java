package com.sigma.samp.imp.ziggo.pisysInstancePickAlgo;

public class PisysInstancePickAlgorithmBeanConst {

    public static final String CONTEXT_PARM_SUBMODEL = "sub";
    public static final String VIDEO_CAS = "video_cas";
    public static final String IN_SERVICE = "in_service";
    public static final String SMP_CPE_CAS = "smp_cpe_cas";
    public static final String STB_CAS="stb_cas";
    public static final String REGION = "region";
    public static final String VIDEO_SERVICE_DEFN_HAS_CAS = "video_service_defn_has_cas";
    public static final String COLOUR = "colour";
    public static final String INSTANCE_ID = "instance_id";

}
