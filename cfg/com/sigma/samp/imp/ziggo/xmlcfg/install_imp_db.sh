#!/usr/bin/ksh
#==============================================================================
#
#  FILE INFO
#    $Id: install_imp_db.sh,v 1.27 2012/03/06 10:01:39 bharath Exp $
#
#  DESCRIPTION
#
#    Make sure that any script you call/invoke from here has it's output
#    also copied into the $LOG_FILE. Otherwise, the ERROR detection at the
#    end won't work and you get confusing results.
#
#  REVISION HISTORY
#  * Based on CVS
#==============================================================================

USAGE="install_imp_smp_db.sh [-d] [-l]\
    -d means running from the development area\
    -l means load only, without re-creating the schema"

LOG_FILE=install_imp_db.log

. ../../util/setEnv.sh

chmod +x *.sh

echo "Starting creation of database..." > $LOG_FILE

BASE_DB_DIR=.
LOAD_ONLY=no

#------------------------------------------------------------------
# Process options
#------------------------------------------------------------------
while getopts dl PARM 2>/dev/null
do
    case $PARM in
        d) BASE_DB_DIR=$SRC_ROOT/out/dist/install/db
           ;;
        l) LOAD_ONLY=yes
           ;;
        \?) echo "$USAGE"
            exit 1;;
    esac
done

shift `expr $OPTIND - 1`

if [ -r $BASE_DB_DIR/../../util/setEnv.sh ]
then
    . $BASE_DB_DIR/../../util/setEnv.sh
else
    echo Cannot find $SRC_ROOT/util/setEnv.sh
    exit 1
fi

if [ $# -ne 0 ]
then
    echo $USAGE
    exit 1
fi

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    echo $USAGE
    exit 1
fi

LOGIN=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}
LOGIN_AM=${SAMP_DB_USER_ROOT}am/${SAMP_DB_PASSWORD}@${SAMP_DB}
HOST=`hostname`

echo "Using $LOGIN on $HOST" >> $LOG_FILE

#-------------------------------------------------------------------------
# Create the database
#-------------------------------------------------------------------------

if [ "$LOAD_ONLY" == "no" ]
then
    if [ ! -f $BASE_DB_DIR/install_smp_db.sh ]
    then
        echo Cannot find file $BASE_DB_DIR/install_smp_db.sh
        exit 1
    else
        CUR_DIR=`pwd`
        cd $BASE_DB_DIR
        ./install_smp_db.sh              | tee -a $LOG_FILE
        cd $CUR_DIR
    fi
fi


#-------------------------------------------------------------------------
# Load the implementaiton data
#-------------------------------------------------------------------------
echo .
echo Loading implementation data | tee -a $LOG_FILE

cat st_base_cfg.sql \
    sp_load_all.sql \
    | sqlplus $LOGIN   >> $LOG_FILE

#-------------------------------------------------------------------------
# Load the topology cfg xml configuration into DB
#-------------------------------------------------------------------------
#echo .
#echo Loading topology xml configuration... | tee -a $LOG_FILE
#java -classpath $CLASSPATH:TopologyCfgXMLDbInstaller.jar com.sigma.samp.stm.xmlcfg.TopologyCfgFactory \
#                        $SMP_CFG_PATH \
#                        ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${ORACLE_THIN_URL} thin >> $LOG_FILE

#-------------------------------------------------------------------------
# Synchronize xml configuration into DB
#-------------------------------------------------------------------------
echo .
echo Synchronizing xml configuration into DB ... | tee -a $LOG_FILE
CFGUPD_CLASSPATH=../../cfg:$CLASSPATH
CFGUPD_CLASSPATH=../../sdk/lib/samp.cmn.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=../../sdk/lib/samp.stm.cmn.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=../../sdk/lib/samp.vframe.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=../../sdk/lib/vframe.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=../../sdk/lib/hframe.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=../../tools/com.sigma.tooling.dist/distribution-tool.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=../../libs/samp.sdm.svccat.jar:$CFGUPD_CLASSPATH
CFGUPD_CLASSPATH=TopologyCfgXMLDbInstaller.jar:$CFGUPD_CLASSPATH

echo $CFGUPD_CLASSPATH

java -Xmx512m -classpath $CFGUPD_CLASSPATH \
  com.sigma.vframe.jxmlcfg.tools.XmlCfgToDBTool \
      -thin ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${ORACLE_THIN_URL} \
      $SMP_CFG_PATH all   >> $LOG_FILE

    
echo Continue loading implementation data | tee -a $LOG_FILE

# Topology types configuration populated from XML
cat topology/ziggo_topology.sql \
     load_topology.sql \
    load_consumable_resource_scripts.sql \
    load_consumable_resource_data.sql \
    CR_update_tn.sql \
    CR_update_ln.sql \
    CR_delete_all.sql \
    CR_add_tn.sql \
    CR_delete_tn.sql \
    CR_add_pool.sql \
    CR_update_rsrc_byid.sql \
    CR_get_rsrc_byid.sql \
    sub_typ_update.sql \
    company_id.sql \
    vp2resmap.sql \
    create_bulk.sql \
    | sqlplus $LOGIN   >> $LOG_FILE


# DOUBLE CHECK the significance of this script
#cat version.sql            \
#    | sqlplus $LOGIN   >> $LOG_FILE

echo Creating SPM implementation search views | tee -a $LOG_FILE
# DOUBLE CHECK the significance of this script
cat create_functions.sql | sqlplus $LOGIN   >> $LOG_FILE
cat sp_query_plsql.sql | sqlplus $LOGIN   >> $LOG_FILE
echo Creating named query views for TanbergBMS BSS Adapter and oracle sequence for cisco sa | tee -a $LOG_FILE
cat create_views.sql \
    cisco_trans_num.sql \
| sqlplus $LOGIN   >> $LOG_FILE

#-------------------------------------------------------------------------
# Load the stm projects data
#-------------------------------------------------------------------------
cd ./stm_proj
../../../tools/stm/install_stm_projects.sh $LOGIN     | tee -a $LOG_FILE
cd ..

#-------------------------------------------------------------------------
# Load the AM implementation data
#-------------------------------------------------------------------------
echo Loading AM implementation data | tee -a $LOG_FILE

for f in am_groups_load.sql \
    am_am_load.sql \
    am_spm_load.sql \
    am_spm_load_cable_solutions.sql \
    am_sdm_load.sql \
    am_dm_load.sql \
    am_impl.sql \
    am_spm_load_commercial_voice.sql \
    am_caweb_load.sql \
    csr_residential.sql \
    csr_commercial.sql \
    csr_all.sql \
    csr_admin.sql \
    am_video_load.sql 
do
    echo "Loading $f" >> $LOG_FILE
    cat $f | sqlplus $LOGIN_AM  >> $LOG_FILE
done


cat ./data/Grp.dat.sql               | sqlplus $LOGIN_AM >> $LOG_FILE
cat ./data/Usr.dat.sql               | sqlplus $LOGIN_AM >> $LOG_FILE
cat ./data/Grp_User.dat.sql          | sqlplus $LOGIN_AM >> $LOG_FILE
cat ./data/Secure_Obj.dat.sql        | sqlplus $LOGIN_AM >> $LOG_FILE
cat ./data/Permission.dat.sql        | sqlplus $LOGIN_AM >> $LOG_FILE
cat ./data/Access_Ctrl_Entry.dat.sql | sqlplus $LOGIN_AM >> $LOG_FILE
cat ./am_jwf_load.sql                | sqlplus $LOGIN_AM >> $LOG_FILE

#-------------------------------------------------------------------------
# Load the xml configuration into xml_repository
#-------------------------------------------------------------------------

echo .
echo Loading xml configuration...
for f in $DOMAIN_HOME/$WL_DOMAIN/cfg/*.xml $DOMAIN_HOME/$WL_DOMAIN/cfg/*.xsd; do
   ../../tools/xmlload/xmlload.sh $LOGIN $f          | tee -a $LOG_FILE
done



#-------------------------------------------------------------------------
# If the implementaion has sub manager specific database
# configuration then the script below will install that.
#-------------------------------------------------------------------------

if [ -f install_imp_submgr_db.sh ]
then
    ./install_imp_submgr_db.sh | tee -a $LOG_FILE
fi

#-------------------------------------------------------------------------
# Installing JWF schema
#-------------------------------------------------------------------------
echo "Installing JWF schema" | tee -a $LOG_FILE
cd jwf
#cat ./install.sql | sqlplus $LOGIN >> $LOG_FILE
./create_jwf_db.sh
cd ..

#-------------------------------------------------------------------------
# configure to network mode
#-------------------------------------------------------------------------
echo "Configure Solution to network mode" | tee -a $LOG_FILE
cd  $DOMAIN_HOME/$WL_DOMAIN/tools/clec
cat show_tree.sql \
    disable_clec_am.sql \
    | sqlplus $LOGIN_AM   >> $LOG_FILE
cd $CUR_DIR

#-------------------------------------------------------------------------
# Verify the result
#-------------------------------------------------------------------------

if egrep -s '^ERROR' $LOG_FILE || egrep -s '^Error' $LOG_FILE
then
    echo Errors found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s  ORA_* $LOG_FILE
then
    echo oracle error found
    exit 1
else
echo  Create and load DB successful |           tee -a $LOG_FILE
      exit 0
fi
