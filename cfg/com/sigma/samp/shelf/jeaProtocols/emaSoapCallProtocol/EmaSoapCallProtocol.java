//========================================================================
//FILE INFO: $Id: EmaSoapCallProtocol.java,v 1.8 2012/02/24 09:55:06 poonamm Exp $
//
//REVISION HISTORY
//* Based on CVS log
//========================================================================
package com.sigma.samp.shelf.jeaProtocols.emaSoapCallProtocol;

import com.sigma.jea.shelfProtocol.basicsoap.BasicSoapCallProtocol;
import com.sigma.jea.adpttask.AdptTask;
import com.sigma.jea.guiDesigner.*;
import com.sigma.jea.resource.BaseProtocol;
import com.sigma.jea.resource.CallArgMap;
import javax.xml.soap.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;
import javax.xml.parsers.ParserConfigurationException;
import java.net.MalformedURLException;
import java.net.URL;
import com.sigma.hframe.jlog.*;
import com.sigma.jea.resource.CallArg;
import com.sigma.jea.resource.CallArgException;
import com.sigma.jea.resource.ProtocolConfig;
import com.sigma.jea.cfg.ProtocolSpec;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.CryptoFactory;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSignature;
import org.w3c.dom.Document;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.activation.DataHandler;
import java.util.Iterator;
import java.io.FileInputStream;
import org.xml.sax.ErrorHandler;
import java.io.InputStream;
import com.sigma.vframe.designerSupport.*;
import com.sigma.vframe.sbsmp.jea.AdptConfigError;
import com.sigma.vframe.sbsmp.jea.TaskConfigError;
import com.sigma.vframe.sbsmp.jea.TaskExecException;
import com.sigma.vframe.sbsmp.jea.TaskFailedError;
import com.sigma.vframe.sbsmp.jea.TaskInternalError;
import com.sigma.vframe.sbsmp.jea.TaskConnFailedError;
import com.sigma.jea.shelfProtocol.basicsoap.CookieManager;
import com.sigma.jea.shelfProtocol.basicsoap.MimeHeaderManager;
import com.sigma.jea.shelfProtocol.basicsoap.Util;
import com.sun.org.apache.xpath.internal.CachedXPathAPI;
import com.sigma.hframe.jerror.SmpIllegalDataException;

public class EmaSoapCallProtocol extends BasicSoapCallProtocol {
    private static Log log = new Log(EmaSoapCallProtocol.class);

    private static final String VERSION = "3.2";
    private static final String DEFAULT = "default";

    class LogoutWorkerThread extends Thread
    {

        TaskExecException exception = null;


        @Override
        public void run()
        {
                MessageFactory messageFactory = null;
                SOAPMessage logoutMessage;
                SOAPMessage response;
                SOAPPart soapPart;
                SOAPEnvelope soapEnvelope;
                SOAPBody soapBody;
                SOAPHeader soapHeader;

                try {
                    if (log.isTraceLevel(3))
                        log.trace(3, "Inside close()");
                    // String closeSessionRequest = getCloseSessionRequest();
                    String logoutRequest = getLogoutRequest();
                    log.log("Logout Request is :: " + logoutRequest);

                    StringReader reader = new StringReader(logoutRequest);
                    InputSource is = new InputSource(reader);
                    Document doc = null;
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    dbf.setNamespaceAware(true);
                    dbf.setCoalescing(false);
                    dbf.setExpandEntityReferences(false);
                    dbf.setIgnoringElementContentWhitespace(true);
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    doc = db.parse(is);
                    if (!isValidateFlag) {
                        String msg = "Soap body is not valid";
                        log.log(msg);
                    }
                    log.log("Using MessageFactory = " + soapMessageFactory);
                    if (soapMessageFactory.startsWith(DEFAULT)) {
                        if (soapMessageFactory.startsWith(DEFAULT + ".")) {
                            String version = soapMessageFactory.substring(DEFAULT
                                    .length() + 1);
                            if (version.equals("soap12"))
                                messageFactory = MessageFactory
                                        .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
                            else if (version.equals("soap11"))
                                messageFactory = MessageFactory
                                        .newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
                        } else
                            messageFactory = MessageFactory.newInstance();
                    } else {
                        messageFactory = (MessageFactory) Class.forName(
                                soapMessageFactory).newInstance();
                    }
                    if (log.isTraceLevel(3))
                        log.trace(3, "MessageFactory class = "
                                + messageFactory.getClass().getName());
                    logoutMessage = messageFactory.createMessage();
                    soapPart = logoutMessage.getSOAPPart();
                    soapEnvelope = soapPart.getEnvelope();
                    soapBody = soapEnvelope.getBody();
                    soapHeader = soapEnvelope.getHeader();

                    DOMSource domSource = new DOMSource(doc);
                    soapPart = logoutMessage.getSOAPPart();
                    soapPart.setContent(domSource);
                    logoutMessage.saveChanges();

                    String logoutResponse = null;
                    // if (isSimulationMode) {
                    // closeSessionResponse = getSimulationCloseSessionResponse();
                    // } else {
                    // send the request to the server
                    ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                    logoutMessage.writeTo(outMsg);
                    log.log("Sending Logout request to server:: " + outMsg.toString());
                    response = connection.call(logoutMessage, sessionControlEndPoint);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.writeTo(out);
                    logoutResponse = out.toString();
                    log.log("Logout Response is ::" + logoutResponse);
          }  catch (Exception e){
              log.log("Logout worker thread exception ::: "+e.getMessage());
          }
        }
    };

    protected String sessionId = "";
    protected String sequenceId = "";

    private String userName = "";
    private String password = "";
    private String sessionControlUri = "";
    private String cai3Namespace = "";
    private String soapUrl = "";
    private URL sessionControlEndPoint = null;
    protected static final String LOGIN_NM = "login_nm";
    protected static final String PASSWORD = "password";
    protected static final String SESSIONCONTROLURI = "session_control_uri";
    protected static final String CAI3_NAMESPACE = "cai3_namespace";
    protected static final String SOAPURL = "soap_url";

    protected static final String SESSION_ID_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='LoginResponse']/*[local-name()='sessionId']";
    protected static final String SEQUENCE_ID_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='LoginResponse']/*[local-name()='baseSequenceId']";

    protected static final String INVALID_SESSIONID_FAULTCODE = "1001";
    protected static final String SESSION_TIMEOUT_FAULTCODE = "1002";
    protected static final String INVALID_SEQUENCEID_FAULTCODE = "1101";
    protected static final String INVALID_TRANSACTIONID_FAULTCODE = "1201";
    // protected static final String INVALID_LOGOUT_SESSIONID_FAULTCODE =
    // "3006";
    protected static final String LOGIN_FAILURE_FAULTCODE = "3014";
    protected static final String SESSIONID_NOT_CONSISTENT_FAULTCODE = "3015";

    private static ProtocolDesignerObjImpl designerObj = new ProtocolDesignerObjImpl();
    static {
        designerObj.setDescription("Basic Soap Protocol using SAAJ APIs");
        designerObj.addInitParm(DesignerSupportUtil
                .getInitParmArrayRecursively(EmaSoapCallProtocol.class));
        designerObj.set3rdPartyLibs(thirdPartyLibs);
        designerObj.setCallTextCfg(new CallTextCfg("protocol-specific",
                "annotation of the soap invocation ... format?"));
        designerObj.setDefaultXA(false);
        designerObj.setDefaultIdempotent(false);
        designerObj.setSharable(false);
        designerObj.setSync(true);
        designerObj.setSelfPool(false);
        designerObj.setVarExternalFunctions(false);
        designerObj.setExternalFunctions(toExternalFunctions(extFuncVec));
        designerObj.setNativeTypes(nativeTypeArray);
    }

    // ======================================================
    /*
     * init() getting the init parms from sbIntegration is done in this function
     * Two System properties have been set in this function,so as to use
     * saaj-api.jar and saaj-impl.jar instead of webservices.jar which is
     * generally used by weblogic bydefault
     *
     */
    public void init(ProtocolConfig config, ProtocolSpec protoSpecInJEA,
            HashMap dynamicParms) throws AdptConfigError {
        log.log("Calling init()");
        log.log("BASICSOAP JEA VERSION " + VERSION);
        // calling init method of BaseProtocol class
        super.init(config, protoSpecInJEA, dynamicParms);

        // Changes for EMA
        log.log("IN INIT::");// remove
        this.userName = config.getStrParm(LOGIN_NM);
        this.password = config.getStrParm(PASSWORD);
        this.sessionControlUri = config.getStrParm(SESSIONCONTROLURI);
        this.cai3Namespace = config.getStrParm(CAI3_NAMESPACE);
        log.log(" userName - > " + this.userName);
        log.log(" password - > " + this.password);
        log.log(" sessionControlUri - > " + this.sessionControlUri);
        log.log(" cai3Namespace - > " + this.cai3Namespace);
        log.log("Exit INIT !!!! ***"); // remove
    }

    /*
     * connect() This function creates an instance of SOAPConnectionFactory and
     * using this connection factory,it creates a connection object NOTE->This
     * function does not connect to the endpointURI, connection to the endpoint
     * URI will be done in connection.call(). For every request,there will be a
     * new connection(As SAAJ APIs does not have the facility to connect once
     * and send multiple requests) This is very inefficient
     */
    public boolean connect() throws AdptConfigError {
        if (isSimulationMode) {
            return true;
        }
        super.connect();
        log.log("*** In CONNECT !!!! ***"); // remove
        this.sessionId = "";
        this.sequenceId = "";
        log.log("*** Exit CONNECT !!!! ***"); // remove
        return true;
    }

    public void exec(String extFuncName, String callText,
            boolean ignoreCalltext, String calltextLanguage, CallArgMap args,
            AdptTask t) throws TaskExecException, TaskConnFailedError {
        log.log("*** In EXEC !!!! ***"); // remove
        log.log("*** Task getTaskTypeNm ==> " + t.getTaskTypeNm()); // remove
        printinfo(extFuncName, callText, ignoreCalltext, calltextLanguage,
                args, t, log);
        annotationCallText = null;
        envelopeXml = null;
        bodyXml = null;
        headerXml = null;
        xmlResponse = null;
        // for attachment
        objectType = null;
        contentType = null;
        contentId = null;
        objectValue = null;
        if (isSimulationMode) {
            // handle simulation mode
            log.log("Simulation mode is true");
            simulateExternFunction(extFuncName, callText, args, t);
            return;
        }
        if (extFuncName.indexOf(CLEAR_COOKIES) >= 0) {
            if (cookiesEnabled) {
                cookieManager.clearCookies();
                return;
            }
        }
        if (extFuncName.indexOf(NORMALIZE_XML) >= 0) {
            try {
                CallArg inxml = args.getArg(IN_XML);
                CallArg argRemoveEmptyTags = args.getArg(REMOVE_EMPTY_TAGS);
                CallArg outxml = args.getArg(OUT_XML);
                boolean removeEmptyTags = false;
                if (argRemoveEmptyTags.getValue().equalsIgnoreCase("true")
                        || argRemoveEmptyTags.getValue()
                                .equalsIgnoreCase("yes")
                        || argRemoveEmptyTags.getValue().equals("1")) {
                    removeEmptyTags = true;
                }
                outxml.setValue(Util.normalizeXml(inxml.getValue(),
                        removeEmptyTags));
            } catch (CallArgException ex) {
                String msg = "Proper arguments not provided";
                log.log(msg, ex);
                throw new TaskInternalError(msg, t.createTask());
            }
            return;
        }
        // parsing of calltext starts here
        parseCallText(callText, t);
        try {
            // create a URL object,will be used in connection.call

            endPoint = new URL(endPointUri);
            sessionControlEndPoint = new URL(this.sessionControlUri);
            log.log("Provisioning endPoint = " + endPoint);
            log.log("Session Control endPoint = " + sessionControlEndPoint);
        } catch (MalformedURLException ex) {
            // if soap_endpoint_uri is wrong
            String msg = "check soap_endpoint_uri";
            log.log(msg, ex);
            throw new TaskConfigError("check soap_endpoint_uri"
                    + ex.getMessage(), t.createTask());
        }
        MessageFactory messageFactory;
        SOAPMessage message;
        SOAPMessage loginMessage;
        SOAPPart soapPart;
        SOAPEnvelope soapEnvelope;
        SOAPBody soapBody;
        SOAPHeader soapHeader;
        SOAPMessage response;
        try {
            // create soap message and extract blank
            // soapPart,soapEnvelope,soapBody,soapHeader
            // messageFactory = MessageFactory.newInstance();
            log.log("Using MessageFactory = " + soapMessageFactory);
            if (soapMessageFactory.startsWith(DEFAULT)) {
                if (soapMessageFactory.startsWith(DEFAULT + ".")) {
                    String version = soapMessageFactory.substring(DEFAULT
                            .length() + 1);
                    if (version.equals("soap12"))
                        messageFactory = MessageFactory
                                .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
                    else if (version.equals("soap11"))
                        messageFactory = MessageFactory
                                .newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
                    else
                        throw new TaskInternalError("Unsupported soap version "
                                + version, t.createTask());
                } else
                    messageFactory = MessageFactory.newInstance();
            } else {
                messageFactory = (MessageFactory) Class.forName(
                        soapMessageFactory).newInstance();
            }
            if (log.isTraceLevel(3))
                log.trace(3, "MessageFactory class = "
                        + messageFactory.getClass().getName());
            message = messageFactory.createMessage();
            soapPart = message.getSOAPPart();
            soapEnvelope = soapPart.getEnvelope();
            soapBody = soapEnvelope.getBody();
            soapHeader = soapEnvelope.getHeader();
            MimeHeaders mimeHeaders = message.getMimeHeaders();
            String login_response = null;
            SOAPMessage xmlLoginResp = null;
            // Added to make it compatible with SOAP 1.1 servers with SAAJ 1.3
            // Libs
            mimeHeaders.setHeader("SOAPAction", "\"\"");
            // SachinD:07-05-2007:Support for Http Basic Authentication:Start
            if (authentication_parms != null) {
                String authentication = new sun.misc.BASE64Encoder()
                        .encode(authentication_parms.getBytes());
                mimeHeaders.addHeader("Authorization", "Basic "
                        + authentication);
            }
            // SachinD:07-05-2007:Support for Http Basic Authentication:End
            // CookiesSupport
            if (cookiesEnabled) {
                cookieManager.setCookies(message, endPoint);
            }
            // Set mime headers from init parms and task parms
            mimeHeaderManager.setMimeHeaders(args, mimeHeaders);

            // if external function is sendEnvelope
            if (extFuncName.equals(SEND_ENVELOPE)) {
                // create a document using envelopeXml and make a DOMSource
                // which will
                // be set
                // as content of soapPart
                envelopeXml = args.getArg(ENVELOPE_XML).getValue();
                log.log("Envelope is " + args.getArg(ENVELOPE_XML).getValue());

                // added for EMA login
                if (this.sessionId == "" && this.sequenceId == "") {
                    try {
                        String login_request = getLoginRequest();
                        log.log(" *** IN LOGIN IF CONDITION *** "); // remove
                        log.log("Login Request is :: " + login_request);

                        loginMessage = createSoapMessage(login_request, t);
                        log.log("After createSoapMessage ::"); // remove
                        log.log("endPoint = " + endPoint);

                        ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                        loginMessage.writeTo(outMsg);
                        log.log("sending login request to server ==> "
                                + outMsg.toString());

                        xmlLoginResp = connection.call(loginMessage,
                                sessionControlEndPoint);
                        log.log("After connection.call for login ::");
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        xmlLoginResp.writeTo(out);
                        login_response = out.toString();
                        log.log("login_response ==> " + login_response); // remove
                        checkErrorCodes(login_response,
                                "//detail/Cai3gFault/faultcode", t);
                        parseLoginResponse(login_response, args, t);

                        // un-comment for testing without stub
                        // String
                        // simulation_login_response=getSimulationLoginResponse();
                        // parseLoginResponse(simulation_login_response,args,t);
                        // comment ends here
                        log.log(" *** EXIT LOGIN IF CONDITION *** ");
                    } catch (Exception e) {
                        String msg = "Exception occurred!! : " + e.getMessage();
                        log.log(msg, e);
                        throw new TaskConnFailedError(msg, t.createTask());
                    }
                }

                log.log("this.sessionId ===> " + this.sessionId); // remove
                log.log("this.sequenceId ===> " + this.sequenceId); // remove
                if (this.sessionId != "" && this.sequenceId != "") {
                    String fullEnvelopeXml = appendEnvelopHeader(envelopeXml, t);
                    Document originalDoc = createDocument(fullEnvelopeXml, t,
                            false);
                    try {
                        if (isRequestVerification) {
                            log.log(" Encoding soap messages ");
                            Document encryptedDoc = signSoapMessage(originalDoc);
                            log.log("Signed Soap Message: "
                                    + getStringForDoc(encryptedDoc));
                            DOMSource domSource = new DOMSource(encryptedDoc);
                            soapPart.setContent(domSource);
                        } else {
                            DOMSource domSource = new DOMSource(originalDoc);
                            soapPart.setContent(domSource);
                        }
                    } catch (Exception e) {
                        String msg = "Message encryption failed";
                        log.log(msg, e);
                        throw new TaskInternalError(msg, t.createTask());
                    }
                }
            }

            if (this.sessionId != "" && this.sequenceId != "") {
                // add attachment
                log.log("Adding attachment");
                addAttachment(message, args, t);
                // save the soap message
                log.log("Saving changes to the soapMessage");
                message.saveChanges();
                // send the request to the server
                ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                message.writeTo(outMsg);
                log.trace(3, "sending request to server: " + outMsg.toString());
                log.log("In connection.call(sending request to the server) ");

                response = connection.call(message, endPoint);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.writeTo(out);
                log.log("response is :: " + out.toString()); // remove

                // added for tests
                // try{
                // String createResponse = getCreateResponse();
                // log.log("Simulated createResponse is ::"+createResponse);
                // response = createSoapMessage(createResponse);
                // log.log("Simulated response is ::"+response);
                // end here and below

                xmlResponse = out.toString();
                log.log(" Server response is: " + xmlResponse);
            } else {
                response = xmlLoginResp;
                xmlResponse = login_response;
            }

            log.log("Calling checkErrorCodes :: "); // remove
            String session_status_msg = checkErrorCodes(xmlResponse,
                    "//detail/Cai3gFault/faultcode", t);
            if (!session_status_msg.equalsIgnoreCase(null))
                log.log("Response contains error :: " + session_status_msg);

            log.log("Calling populateXmlResponse :: "); // remove
            populateXmlResponse(xmlResponse, args, t);
            String errorCode = SOAP_SUCCESS_CODE;
            String errorDesc = SOAP_SUCCESS_DESC;

            // added for tests
            // response = createSoapMessage(createResponse);
            // ends

            if (response.getSOAPBody().hasFault()) {
                errorCode = response.getSOAPBody().getFault().getFaultCode();
                errorDesc = response.getSOAPBody().getFault().getFaultString();
                log.log(" The soap response has some fault ");
            } else {
                if (cookiesEnabled) {
                    cookieManager.storeCookies(response, endPoint);
                }
            }

            log.log(" Setting the error codes ");
            log.log("errorCode :: " + errorCode); // remove
            log.log("errorDesc :: " + errorDesc); // remove

            setErrorCodeMsg(errorCode, errorDesc, args, t);

            // added for tests
            // }
            // catch (Exception e)
            // {
            // String msg = "Simulation Response failed";
            // log.log(msg);
            // throw new TaskFailedError(e.getMessage(), t.createTask());
            // }
            // ends here

            log.log("*** Exit EXEC !!!! ***"); // remove

        } catch (CallArgException ex) {
            String msg = "Proper arguments not provided";
            log.log(msg, ex);
            throw new TaskInternalError(msg, t.createTask());
        } catch (IOException ex) {
            String msg = "Unable to read the response";
            log.log(msg, ex);
            throw new TaskConnFailedError(msg, t.createTask());
        } catch (SOAPException ex) {
            String msg = "Can not create soap message or connection problem";
            log.log(msg, ex);
            throw new TaskConnFailedError(msg, t.createTask());
        } catch (java.lang.NoClassDefFoundError e) {
            throw new TaskInternalError(e.getMessage(), t.createTask());
        } catch (InstantiationException e) {
            String msg = "Can not instantiate MessageFactory "
                    + soapMessageFactory;
            log.log(msg);
            throw new TaskConnFailedError(msg, t.createTask());
        } catch (IllegalAccessException e) {
            String msg = "Can not instantiate MessageFactory "
                    + soapMessageFactory;
            log.log(msg);
            throw new TaskConnFailedError(msg, t.createTask());
        } catch (ClassNotFoundException e) {
            String msg = "Can not instantiate MessageFactory "
                    + soapMessageFactory;
            log.log(msg);
            throw new TaskConnFailedError(msg, t.createTask());
        }
    }

    public String getLoginRequest() {
        String login = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:cai3=\"http://schemas.ericsson.com/cai3g1.2/\"> "
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<cai3:Login>"
                + "<cai3:userId>"
                + this.userName
                + "</cai3:userId>"
                + "<cai3:pwd>"
                + this.password
                + "</cai3:pwd>"
                + "</cai3:Login>" + "</soapenv:Body>" + "</soapenv:Envelope>";

        log.log("Returning from login_request!");
        return login;
    }

    public void parseLoginResponse(String responseXML, CallArgMap args,
            AdptTask t) throws TaskConfigError, TaskInternalError,
            SmpIllegalDataException, TaskConnFailedError {
        DocumentBuilderFactory domFactory = null;
        DocumentBuilder domBuilder = null;
        Document document = null;
        try {
            log.log("In parseLoginResponse !!! " + responseXML); // remove
            StringReader xmlReader = new StringReader(responseXML);
            log.log("StringReader !!! "); // remove
            InputSource xmlInp = new InputSource(xmlReader);
            domFactory = DocumentBuilderFactory.newInstance();
            domBuilder = domFactory.newDocumentBuilder();
            document = domBuilder.parse(xmlInp);
            log.log("domBuilder.parse !!! "); // remove
            // Node rootElement = document.getDocumentElement();
            CachedXPathAPI xPathAPI = new CachedXPathAPI();
            this.sessionId = xPathAPI.eval(document, SESSION_ID_PATH).str();
            this.sequenceId = xPathAPI.eval(document, SEQUENCE_ID_PATH).str();
            log.log("this.sessionId :: " + this.sessionId); // remove
            log.log("this.sequenceId :: " + this.sequenceId); // remove

            if (this.sequenceId.equalsIgnoreCase(null)
                    || this.sessionId.equalsIgnoreCase(null)) {
                String errMsg = "Invalid sessionId or sequenceId !!";
                log.log("this.sessionId ==> " + this.sessionId); // remove
                log.log("this.sequenceId ==> " + this.sequenceId); // remove
                throw new TaskConnFailedError(errMsg, t.createTask());
            }

            return;
        } catch (Exception e) {
            String msg = "Exception occurs";
            log.log(msg, e);
            throw new SmpIllegalDataException();
        }
    }

    public String appendEnvelopHeader(String envelopeXml, AdptTask t) {
        // append the envelop-header to envelopeXml
        String fullEnvelopeXml = "";

        try {
            log.log("Task getTaskTypeNm ==> " + t.getTaskTypeNm()); // remove

            log.log("In appendEnvelopHeader !!!"); // remove
            log.log("SequenceID ===> " + this.sequenceId); // remove

            log.log("envelopeXml ===> " + envelopeXml); // remove

            // fullEnvelopeXml = "<soapenv:Envelope
            // xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"
            // xmlns:cai3=\"http://schemas.ericsson.com/cai3g1.2/\">"
            // + "<soapenv:Header>"
            // + "<cai3:SequenceId>"
            // + this.sequenceId
            // + "</cai3:SequenceId>"
            // + "<cai3:TransactionId/>"
            // + "<cai3:SessionId>"
            // + this.sessionId
            // + "</cai3:SessionId>"
            // + "</soapenv:Header>"
            // + "<soapenv:Body>"
            // + envelopeXml
            // + "</soapenv:Body>" + "</soapenv:Envelope>";

            fullEnvelopeXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                    + "xmlns:cai3=\""
                    + this.cai3Namespace
                    + "\">"
                    + "<soapenv:Header>"
                    + "<cai3:SequenceId>"
                    + this.sequenceId
                    + "</cai3:SequenceId>"
                    + "<cai3:TransactionId/>"
                    + "<cai3:SessionId>"
                    + this.sessionId
                    + "</cai3:SessionId>"
                    + "</soapenv:Header>"
                    + "<soapenv:Body>"
                    + envelopeXml
                    + "</soapenv:Body>" + "</soapenv:Envelope>";

            log.log("fullEnvelopeXml ===> " + fullEnvelopeXml); // remove

            Long tmp = Long.parseLong(this.sequenceId);
            tmp = tmp + 1;
            this.sequenceId = tmp.toString();
            log.log("Incremented SequenceID ===> " + this.sequenceId); // remove

        } catch (Exception ex) {
            new TaskFailedError(ex.getMessage(), t.createTask());
        }
        return fullEnvelopeXml;
    }

    public String checkErrorCodes(String responseXML, String path, AdptTask t)
            throws TaskConnFailedError, TaskInternalError {
        //
        String errMsg = "";
        // set errorCode errorDesc

        DocumentBuilderFactory domFactory = null;
        DocumentBuilder domBuilder = null;
        Document document = null;
        try {
            log.log("In checkErrorCodes *** !!! "); // remove
            log.log("responseXML :: " + responseXML);
            StringReader xmlReader = new StringReader(responseXML);
            InputSource xmlInp = new InputSource(xmlReader);
            domFactory = DocumentBuilderFactory.newInstance();
            domBuilder = domFactory.newDocumentBuilder();
            document = domBuilder.parse(xmlInp);
            CachedXPathAPI xPathAPI = new CachedXPathAPI();
            errMsg = xPathAPI.eval(document, path).str();
        } catch (TransformerException te) {
            log.log("TransformerException !!", te.getMessage());
        } catch (SAXException se) {
            log.log("SAXException!!", se.getMessage());
        } catch (ParserConfigurationException pe) {
            log.log("ParserConfigurationException!!", pe.getMessage());
        } catch (IOException io) {
            log.log("IOException!!", io.getMessage());
        }

        log.log("errMsg :: " + errMsg); // remove

        if (errMsg.equalsIgnoreCase(INVALID_SESSIONID_FAULTCODE)) {
            log.log("INVALID_SESSIONID_FAULTCODE error! :: Need to relogin!!");
            throw new TaskConnFailedError(errMsg, t.createTask());
        } else if (errMsg.equalsIgnoreCase(SESSION_TIMEOUT_FAULTCODE)) {
            log.log("SESSION_TIMEOUT_FAULTCODE error! :: Need to relogin!!");
            throw new TaskConnFailedError(errMsg, t.createTask());
        } else if (errMsg.equalsIgnoreCase(INVALID_SEQUENCEID_FAULTCODE)) {
            log.log("INVALID_SEQUENCEID_FAULTCODE error! :: Need to relogin!!");
            throw new TaskConnFailedError(errMsg, t.createTask());
        } else if (errMsg.equalsIgnoreCase(INVALID_TRANSACTIONID_FAULTCODE)) {
            log
                    .log("INVALID_TRANSACTIONID_FAULTCODE error! :: Need to relogin!!");
            throw new TaskConnFailedError(errMsg, t.createTask());
        } else
        // if(errMsg.equalsIgnoreCase(INVALID_LOGOUT_SESSIONID_FAULTCODE))
        // {
        // log.log("INVALID_LOGOUT_SESSIONID_FAULTCODE error! :: Need to
        // relogin!!");
        // throw new TaskConnFailedError(errMsg, t.createTask());
        // }
        if (errMsg.equalsIgnoreCase(LOGIN_FAILURE_FAULTCODE)) {
            log.log("LOGIN_FAILURE_FAULTCODE error! :: Need to relogin!!");
            throw new TaskConnFailedError(errMsg, t.createTask());
        } else if (errMsg.equalsIgnoreCase(SESSIONID_NOT_CONSISTENT_FAULTCODE)) {
            log
                    .log("SESSIONID_NOT_CONSISTENT_FAULTCODE error! :: Need to relogin!!");
            throw new TaskConnFailedError(errMsg, t.createTask());
        }
        log.log("Exit checkErrorCodes *** !!! "); // remove

        if (errMsg.equalsIgnoreCase(null)) // test whether is null
        {
            log.log("errMsg is null !!! "); // remove
            // errMsg="";
        }

        return errMsg;
    }

    protected SOAPMessage createSoapMessage(String finalSoapBody, AdptTask t)
            throws Exception {

        log.log("In createSoapMessage");
        log.log("Converting string to document,the string is " + finalSoapBody);
        MessageFactory messageFactory = null;
        SOAPMessage message = null;
        SOAPPart soapPart;
        try {
            StringReader reader = new StringReader(finalSoapBody);
            InputSource is = new InputSource(reader);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            dbf.setCoalescing(false);
            dbf.setExpandEntityReferences(false);
            dbf.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            log.log(" Parsing !! "); // remove
            doc = db.parse(is);
            if (!isValidateFlag) {
                String msg = "Soap body is not valid";
                log.log(msg);
            }
            log.log("Using MessageFactory = " + soapMessageFactory);
            if (soapMessageFactory.startsWith(DEFAULT)) {
                if (soapMessageFactory.startsWith(DEFAULT + ".")) {
                    String version = soapMessageFactory.substring(DEFAULT
                            .length() + 1);
                    if (version.equals("soap12"))
                        messageFactory = MessageFactory
                                .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
                    else if (version.equals("soap11"))
                        messageFactory = MessageFactory
                                .newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
                    else
                        throw new TaskInternalError("Unsupported soap version "
                                + version, t.createTask());

                } else
                    messageFactory = MessageFactory.newInstance();
            } else {
                messageFactory = (MessageFactory) Class.forName(
                        soapMessageFactory).newInstance();
            }
            if (log.isTraceLevel(3))
                log.trace(3, "MessageFactory class = "
                        + messageFactory.getClass().getName());
            message = messageFactory.createMessage();
            DOMSource domSource = new DOMSource(doc);
            soapPart = message.getSOAPPart();
            soapPart.setContent(domSource);
            message.saveChanges();

            log
                    .log("Message formed! Returning from createSoapMessage function."
                            + message.getSOAPPart().getContent().toString());

        } catch (Exception e) {
            String msg = "Exception occurred!! : " + e.getMessage();
            log.log(msg, e);
            throw new TaskFailedError(msg, t.createTask());
        }
        return message;
    }

    public String getSimulationLoginResponse() {
        // for tests without stub only
        String login_response = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:cai3=\"http://schemas.ericsson.com/cai3g1.2/\"> "
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<cai3:LoginResponse>"
                + "<cai3:sessionId>99999</cai3:sessionId>"
                + "<cai3:baseSequenceId>999000999</cai3:baseSequenceId>"
                + "</cai3:LoginResponse>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        return login_response;
    }

    public String getCreateResponse() {
        // for tests without stub only
        int dummy_muid = 100000000;
        // dummy_muid=dummy_muid+1;

        String create_response = "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cai3g=\"http://schemas.ericsson.com/cai3g1.2/\">"
                + "<S:Header>" + "<cai3g:SessionId>"
                + this.sessionId
                + "</cai3g:SessionId>"
                + "<cai3g:TransactionId/>"
                + "<cai3g:SequenceId>"
                + this.sequenceId
                + "</cai3g:SequenceId>"
                + "</S:Header>"
                + "<S:Body>"
                + "<ns2:CreateResponse xmlns:ns2=\"http://schemas.ericsson.com/cai3g1.2/\">"
                + "<ns2:MOId>"
                + "<operatorid:operatorid xmlns=\"http://schemas.ericsson.com/ma/mio/profile/\" xmlns:operatorid=\"http://schemas.ericsson.com/ma/mio/profile/\">yahoo</operatorid:operatorid>"
                + "<muid:muid xmlns=\"http://schemas.ericsson.com/ma/mio/profile/\" xmlns:muid=\"http://schemas.ericsson.com/ma/mio/profile/\">"
                + dummy_muid
                + "</muid:muid>"
                + "</ns2:MOId>"
                + "<ns2:MOAttributes>"
                + "<ns2:CreateMcdProfileResponse xmlns=\"http://schemas.ericsson.com/cai3g1.2/\">"
                + "<profile>"
                + "<identity>muid:"
                + dummy_muid
                + "</identity>"
                + "</profile>"
                + "</ns2:CreateMcdProfileResponse>"
                + "</ns2:MOAttributes>"
                + "</ns2:CreateResponse>"
                + "</S:Body>"
                + "</S:Envelope>";

        return create_response;
    }

    public String getLogoutRequest() {
        // String logout = "<soapenv:Envelope
        // xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
        // + "xmlns:cai3=\"http://schemas.ericsson.com/cai3g1.2/\">"
        // + "<soapenv:Header>"
        // + "<cai3:SessionId>"+this.sessionId+"</cai3:SessionId>"
        // + "</soapenv:Header>"
        // + "<soapenv:Body>"
        // + "<cai3:Logout>"
        // + "<cai3:sessionId>"+this.sessionId+"</cai3:sessionId>"
        // + "</cai3:Logout>"
        // + "</soapenv:Body>"
        // + "</soapenv:Envelope>";

        String logout = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:cai3=\""
                + this.cai3Namespace
                + "\">"
                + "<soapenv:Header>"
                + "<cai3:SessionId>"
                + this.sessionId
                + "</cai3:SessionId>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<cai3:Logout>"
                + "<cai3:sessionId>"
                + this.sessionId
                + "</cai3:sessionId>"
                + "</cai3:Logout>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        log.log("Returning from logout_request!");
        return logout;
    }

    public void close() {

        LogoutWorkerThread logoutWorker = new LogoutWorkerThread();

        logoutWorker.start();
        log.log("soap call in worker thread with timeout="+callTimeout);
        try {
            logoutWorker.join(callTimeout*1000);
          if(logoutWorker.isAlive()){
              log.log("LogoutWorkerThread is closed.......");
          }else{
              // finished
              if(logoutWorker.exception != null){
                  throw logoutWorker.exception;
              }
          }
      } catch (InterruptedException e) {
          // wont happen
      } catch (Exception e) {
            String msg = "Some exception occured during logout!";
            log.log(msg, e);
        }
        super.close();
    }

}
