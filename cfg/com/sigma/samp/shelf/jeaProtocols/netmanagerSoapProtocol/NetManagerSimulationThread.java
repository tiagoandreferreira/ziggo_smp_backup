//========================================================================
//FILE INFO: $Id: NetManagerSimulationThread.java,v 1.2 2012/04/04 13:55:58 poonamm Exp $
//
//REVISION HISTORY
//* Based on CVS log
//========================================================================
package com.sigma.samp.shelf.jeaProtocols.netmanagerSoapProtocol;

import java.util.Properties;

import com.sigma.hframe.jlog.AlarmCategory;
import com.sigma.hframe.jlog.AlarmLevel;
import com.sigma.hframe.jlog.Log;
import com.sigma.jea.adpttask.AdptTask;
import com.sigma.jea.cfgdata.AdptGlobals;
import com.sigma.jea.flow.AsyncFlow;
import com.sigma.jea.flow.AsyncReplyException;
import com.sigma.jea.util.PropUtil;
import com.sigma.vframe.sbsmp.jea.TaskExecException;

public class NetManagerSimulationThread extends Thread {
    String             jobId = "";
    private static Log log   = new Log(NetmanagerSoapProtocol.class);

    public NetManagerSimulationThread(String jobId) {
        this.jobId = jobId;
        log.log("job id " + jobId);
    }

    public String getAsyncSimulationResponse() {
        String response = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                + " xmlns:ns3=\"http://tempuri.org/ns3.xsd\">" + "<soapenv:Header/>" + "<soapenv:Body>"
                + "<ns3:ExecuteAsynchronousJob>" + "<sessionid>?</sessionid>" + "<str-xml-response>"
                + "<Job xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xsi:noNamespaceSchemaLocation=\"NPSchemaForServerResp.xsd\" "
                + "xsi:type=\"netmctJobEventJobStatus\">" + "<Id>job_id</Id>" + "<JobType>"
                + "<OperationType>Event</OperationType>" + "<NPJobType>Job Status</NPJobType>" + "</JobType>"
                + "<JobData>" + "<NPJobData>" + "<NPJobDisplayList>" + "<NPJobStatus>" + "<Id>10</Id>"
                + "<NPJobType>MANUAL INPUT</NPJobType>" + "<Progress>100</Progress>"
                + "<NPJobState>EXECUTED</NPJobState>" + "<Date>2004-12-27</Date>" + "<Time>23:43:55</Time>"
                + "<NPTranslatorKey>" + "<Id>1</Id>" + "</NPTranslatorKey>" + "<ClientInfo>"
                + "<User>ADMINISTRATOR</User>" + "<Usergroup>NetM NP USERS</Usergroup>"
                + "<Terminal>LEONARDO</Terminal>" + "</ClientInfo>" + "</NPJobStatus>"
                + "</NPJobDisplayList>" + "</NPJobData>" + "</JobData>" + "</Job>" + "</str-xml-response>"
                + "</ns3:ExecuteAsynchronousJob>" + "</soapenv:Body>" + "</soapenv:Envelope>";
        return response;
    }

    public void run() {
        log.log("Simulation Thread stated ");
        AsyncFlow flow = null;

        String asyncResponse = getAsyncSimulationResponse();
        asyncResponse = asyncResponse.replaceAll("job_id", this.jobId);
        log.trace(3, "Simulation async response " + asyncResponse);
        Properties replyParms = new Properties();
        replyParms.put("adapter_status", "completed");
        replyParms.put("xml_response", asyncResponse);

        try {
            Thread.sleep(2000);
            flow = (AsyncFlow) AdptGlobals.getFlowFactory().getFlow();
            if (log.isTraceLevel(3))
                log.trace(3, "Flow value:" + flow);
            try {
                if (flow != null) {
                    AdptTask adptTask = flow.processAsyncReplyTask(jobId,
                            PropUtil.properties2HashMap(replyParms));
                    log.log("Task updated sucessfully..");
                }
            } catch (AsyncReplyException e) {
                log.log("AsyncReplyException occured::", e);
                log.alarm(AlarmLevel.eError, AlarmCategory.eAppl, e.getMessage(), "", e);
            } catch (TaskExecException e) {
                log.log("TaskExecException occured::", e);
                log.alarm(AlarmLevel.eError, AlarmCategory.eAppl, e.getMessage(), "", e);
            }

        } catch (Exception e) {
            log.log("Exception occured::", e);
        }
        log.log("Simulation Thread ended ");
    }

}
