//========================================================================
//FILE INFO: $Id: NetmanagerSoapProtocol.java,v 1.15 2012/04/04 13:55:58 poonamm Exp $
//
//REVISION HISTORY
//* Based on CVS log
//========================================================================
package com.sigma.samp.shelf.jeaProtocols.netmanagerSoapProtocol;

import com.sigma.jea.shelfProtocol.basicsoap.BasicSoapCallProtocol;
import com.sigma.jea.adpttask.AdptTask;
import com.sigma.jea.guiDesigner.*;
import com.sigma.jea.resource.BaseProtocol;
import com.sigma.jea.resource.CallArgMap;
import javax.xml.soap.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;
import javax.xml.parsers.ParserConfigurationException;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import com.sigma.hframe.jlog.*;
import com.sigma.jea.resource.CallArg;
import com.sigma.jea.resource.CallArgException;
import com.sigma.jea.resource.ProtocolConfig;
import com.sigma.jea.cfg.ProtocolSpec;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.CryptoFactory;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSignature;
import org.w3c.dom.Document;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.activation.DataHandler;
import java.util.Iterator;
import java.io.FileInputStream;
import org.xml.sax.ErrorHandler;

import com.sigma.vframe.util.XmlUtil;

import java.io.InputStream;
import com.sigma.vframe.designerSupport.*;
import com.sigma.vframe.sbsmp.jea.AdptConfigError;
import com.sigma.vframe.sbsmp.jea.TaskConfigError;
import com.sigma.vframe.sbsmp.jea.TaskExecException;
import com.sigma.vframe.sbsmp.jea.TaskFailedError;
import com.sigma.vframe.sbsmp.jea.TaskInternalError;
import com.sigma.vframe.sbsmp.jea.TaskConnFailedError;
import com.sigma.jea.shelfProtocol.basicsoap.CookieManager;
import com.sigma.jea.shelfProtocol.basicsoap.MimeHeaderManager;
import com.sigma.jea.shelfProtocol.basicsoap.Util;
import com.sun.org.apache.xpath.internal.CachedXPathAPI;
import com.sigma.hframe.jerror.SmpIllegalDataException;

public class NetmanagerSoapProtocol extends BasicSoapCallProtocol {
    private static Log log = new Log(NetmanagerSoapProtocol.class);

    private static final String VERSION = "3.2";
    private static final String DEFAULT = "default";

    protected String sessionId = "";
    protected String publicKeyN = "";
    protected String publicKeyK = "";
    protected String faultCode = "";
    protected String faultReason = "";

    private String userName = "";
    private String password = "";
    private String userGroup = "";
    private String terminal = "";
    private String encrPassword = "";
    private String xsdUrl = "";
    private String soapUrl = "";
    private String simulation_job_id = "";
    private String headerUrl = "";
    private String listenerUrl = "";

    protected static final String LOGIN_NM = "login_nm";
    protected static final String PASSWORD = "password";
    protected static final String USER_GRP = "user_grp";
    protected static final String TERMINAL = "terminal";
    protected static final String XSD_URL = "xsd_url";
    protected static final String SOAP_URL = "soap_url";
    protected static final String HEADER_URL = "header_url";
    protected static final String LISTENER_URL = "listener_url";

    protected static final String SESSION_ID_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='ConnectResponse']/*[local-name()='session-id']";
    protected static final String PING_SERVER_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='PingServerResponse']/*[local-name()='status']";
    protected static final String PUBLIC_KEY_N_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='GetPublicKeyResponse']/*[local-name()='public-key-N']";
    protected static final String PUBLIC_KEY_K_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='GetPublicKeyResponse']/*[local-name()='public-key-K']";
    protected static final String CLOSE_SESSION_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='CloseSessionResponse']/*[local-name()='status']";
    protected static final String FAULT_CODE_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='Fault']/*[local-name()='Reason']/*[local-name()='Text']";
    protected static final String FAULT_REASON_PATH = "*[local-name()='Envelope']/*[local-name()='Body']/*"
            + "[local-name()='Fault']/*[local-name()='Detail']";

    protected static final String SOAP_SESSIONID_INVALID_FAULTCODE = "SOAP_SESSIONID_INVALID";

    protected static Vector<ExternalFuncDesignerObject> extFuncVec = new Vector<ExternalFuncDesignerObject>();

    private static ProtocolDesignerObjImpl designerObj = new ProtocolDesignerObjImpl();
    static {
        designerObj
                .setDescription("Netmanager Soap Protocol extending Basic Soap Protocol");
        designerObj.addInitParm(DesignerSupportUtil
                .getInitParmArrayRecursively(NetmanagerSoapProtocol.class));
        designerObj.set3rdPartyLibs(thirdPartyLibs);
        designerObj.setCallTextCfg(new CallTextCfg("protocol-specific",
                "annotation of the soap invocation ... format?"));
        designerObj.setDefaultXA(false);
        designerObj.setDefaultIdempotent(false);
        designerObj.setSharable(false);
        designerObj.setSync(true);
        designerObj.setSelfPool(false);
        designerObj.setVarExternalFunctions(false);
        designerObj.setExternalFunctions(toExternalFunctions(extFuncVec));
        designerObj.setNativeTypes(nativeTypeArray);
    }

    public void init(ProtocolConfig config, ProtocolSpec protoSpecInJEA,
            HashMap dynamicParms) throws AdptConfigError {
        if (log.isTraceLevel(3))
            log.trace(3, "Calling init() of base protocol");
        // calling init method of BaseProtocol class
        super.init(config, protoSpecInJEA, dynamicParms);
        // getting init parms
        this.userName = config.getStrParm(LOGIN_NM);
        this.password = config.getStrParm(PASSWORD);
        this.userGroup = config.getStrParm(USER_GRP);
        this.terminal = config.getStrParm(TERMINAL);
        this.xsdUrl = config.getStrParm(XSD_URL);
        this.soapUrl = config.getStrParm(SOAP_URL);
        this.headerUrl = config.getStrParm(HEADER_URL);
        this.listenerUrl = config.getStrParm(LISTENER_URL);

        if (log.isTraceLevel(3)) {
            log.trace(3, "Init parms-");
            log.trace(3, "User Name:- " + this.userName);
            log.trace(3, "Password:- " + this.password);
            log.trace(3, "User Group:- " + this.userGroup);
            log.trace(3, "Terminal:- " + this.terminal);
            log.trace(3, "XSD Url:- " + this.xsdUrl);
            log.trace(3, "Soap Url:- " + this.soapUrl);
            log.trace(3, "Header Url:- " + this.headerUrl);
            log.trace(3, "Listener Url:- " + this.listenerUrl);
        }
    }

    /*
     * protected String soapMessageFactory;
     *
     * protected String soapConnectionFactory;
     */
    /*
     * connect() This function creates an instance of SOAPConnectionFactory and
     * using this connection factory,it creates a connection object NOTE->This
     * function does not connect to the endpointURI, connection to the endpoint
     * URI will be done in connection.call(). For every request,there will be a
     * new connection(As SAAJ APIs does not have the facility to connect once
     * and send multiple requests) This is very inefficient
     */
    public boolean connect() throws AdptConfigError {
        if (log.isTraceLevel(3))
            log.trace(3, "Calling connect() of base protocol");
        super.connect();
        this.sessionId = "";
        return true;
    }

    public void exec(String extFuncName, String callText,
            boolean ignoreCalltext, String calltextLanguage, CallArgMap args,
            AdptTask t) throws TaskExecException, TaskConnFailedError {
        if (log.isTraceLevel(3))
            log.trace(3, "Inside exec()");
        printinfo(extFuncName, callText, ignoreCalltext, calltextLanguage,
                args, t, log);
        annotationCallText = null;
        envelopeXml = null;
        bodyXml = null;
        headerXml = null;
        xmlResponse = null;
        // for attachment
        objectType = null;
        contentType = null;
        contentId = null;
        objectValue = null;

        if (extFuncName.indexOf(CLEAR_COOKIES) >= 0) {
            if (cookiesEnabled) {
                cookieManager.clearCookies();
                return;
            }
        }
        if (extFuncName.indexOf(NORMALIZE_XML) >= 0) {
            try {
                CallArg inxml = args.getArg(IN_XML);
                CallArg argRemoveEmptyTags = args.getArg(REMOVE_EMPTY_TAGS);
                CallArg outxml = args.getArg(OUT_XML);
                boolean removeEmptyTags = false;
                if (argRemoveEmptyTags.getValue().equalsIgnoreCase("true")
                        || argRemoveEmptyTags.getValue()
                                .equalsIgnoreCase("yes")
                        || argRemoveEmptyTags.getValue().equals("1")) {
                    removeEmptyTags = true;
                }
                outxml.setValue(Util.normalizeXml(inxml.getValue(),
                        removeEmptyTags));
            } catch (CallArgException ex) {
                String msg = "Proper arguments not provided";
                log.log(msg, ex);
                throw new TaskInternalError(msg, t.createTask());
            }
            return;
        }
        // parsing of calltext starts here
        parseCallText(callText, t);
        try {
            // create a URL object,will be used in connection.call
            endPoint = new URL(endPointUri);
            if (log.isTraceLevel(3)) {
                log.trace(3, "Endpoint Url:" + endPoint);
                log.trace(3, "Simulation Mode:" + isSimulationMode);
            }

        } catch (MalformedURLException ex) {
            // if soap_endpoint_uri is wrong
            String msg = "check soap_endpoint_uri";
            log.log(msg, ex);
            throw new TaskConfigError("check soap_endpoint_uri"
                    + ex.getMessage(), t.createTask());
        }
        SOAPMessage message;
        SOAPMessage response;
        try {
            if (extFuncName.equals(SEND_ENVELOPE)) {

                envelopeXml = args.getArg(ENVELOPE_XML).getValue();
                if (log.isTraceLevel(3))
                    log.trace(3, "Job Request XML is: \n" + envelopeXml);
                String simulation_response = null;

                if (log.isTraceLevel(3))
                    log.trace(3, "Session Id value:- " + this.sessionId);
                if (this.sessionId == "") {
                    if (log.isTraceLevel(3))
                        log.trace(3, "Session Id is empty");
                    try {
                        String pingServerStatus = pingServer(t);
                        if (pingServerStatus.equals("FAULT")) {
                            log.log("PingServer returned FAULT");
                            setErrorCodeMsg(faultCode, faultReason, args, t);
                            return;
                        } else if (pingServerStatus.equals("false")) {
                            log.log("PingServer returned false");
                            setErrorCodeMsg("1",
                                    "PingServer call returned false", args, t);
                            return;
                        } else {
                            log.log("PingServer returned true");
                        }

                        getPublicKey(t);
                        if (publicKeyN.equals("FAULT")
                                || publicKeyK.equals("FAULT")) {
                            log.log("GetPublicKey returned FAULT");
                            setErrorCodeMsg(faultCode, faultReason, args, t);
                            return;
                        }
                        encrPassword = rsaEncrypt(Long.valueOf(publicKeyN),
                                Long.valueOf(publicKeyK), password);

                        String loginRequest = getLoginRequest();
                        log.log("Login Request is :-\n " + loginRequest);
                        message = createSoapMessage(loginRequest, t);

                        String loginResponse = "";
                        if (isSimulationMode) {
                            loginResponse = getSimulationLoginResponse();
                            if (log.isTraceLevel(3))
                                log
                                        .trace(3,
                                                "Calling parseResponse() to parse Login simulation response..");
                            this.sessionId = parseResponse(loginResponse,
                                    SESSION_ID_PATH, t);
                            log.log("Simulation Session Id received: "
                                    + this.sessionId);

                        } else {
                            ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                            message.writeTo(outMsg);
                            if (log.isTraceLevel(3))
                                log.trace(3,
                                        "Sending Login Request to server: "
                                                + outMsg.toString());
                            response = connection.call(message, endPoint);
                            ByteArrayOutputStream out = new ByteArrayOutputStream();
                            response.writeTo(out);
                            loginResponse = out.toString();
                            log.log(" Server response is: " + loginResponse);
                            log.log("Calling checkSessionStatus :: ");
                            if (log.isTraceLevel(3))
                                log
                                        .trace(3,
                                                "Calling parseResponse() to parse Login response..");
                            this.sessionId = parseResponse(loginResponse,
                                    SESSION_ID_PATH, t);
                            log.log("Session Id received from server: "
                                    + this.sessionId);
                        }
                        if (sessionId.equals("FAULT")) {
                            log.log("Login/Connect returned FAULT");
                            setErrorCodeMsg(faultCode, faultReason, args, t);
                            return;
                        }
                    } catch (TaskConnFailedError tcf) {
                        throw tcf;
                    }
                }
                envelopeXml = envelopeXml.replace("_sessionid", this.sessionId);
                envelopeXml = envelopeXml.replace("_soapurl", soapUrl);
                envelopeXml = envelopeXml.replace("_xsdurl", xsdUrl);
                envelopeXml = envelopeXml.replace("_headerurl", headerUrl);
                envelopeXml = envelopeXml.replace("_endpointuri", endPointUri);
                envelopeXml = envelopeXml.replace("_listenerurl", listenerUrl);
                envelopeXml = envelopeXml.replace("_messageid", getMessageID());

                message = createSoapMessage(envelopeXml, t);
                ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                message.writeTo(outMsg);
                if (isSimulationMode) {
                    simulation_response = getSimulationModeResponse();
                    log.log("Populating Simulation Job response:"
                            + simulation_response);
                    populateXmlResponse(simulation_response, args, t);
                    NetManagerSimulationThread simulationThread = new NetManagerSimulationThread(
                            this.simulation_job_id);
                    log.log("Invoking Simulation thread");
                    simulationThread.start();

                } else {
                    log
                            .log("Sending Job request to server"
                                    + outMsg.toString());
                    response = connection.call(message, endPoint);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.writeTo(out);
                    xmlResponse = out.toString();
                    log.log(" Server response is: " + xmlResponse);
                    xmlResponse = decodeResponse(xmlResponse);
                    log.log(" Server response after decoding: " + xmlResponse);
                    log
                            .log("Calling parseResponse to validate server response");
                    // parseResponse() is called only to check and handle in
                    // case of fault response
                    String jobRequestStatus = parseResponse(xmlResponse,
                            "//Job", t);
                    if (jobRequestStatus.equals("FAULT")) {
                        log.log("JobRequest call returned FAULT");
                        setErrorCodeMsg(faultCode, faultReason, args, t);
                        return;
                    }
                    log.log("Populating Job response");
                    populateXmlResponse(xmlResponse, args, t);

                }
            }

            if (log.isTraceLevel(3))
                log.trace(3, "Exiting exec");

        } catch (CallArgException ex) {
            String msg = "Proper arguments not provided";
            log.log(msg, ex);
            throw new TaskInternalError(msg, t.createTask());
        } catch (IOException ex) {
            String msg = "Unable to read the response";
            log.log(msg, ex);
            throw new TaskConnFailedError(msg, t.createTask());
        } catch (SOAPException ex) {
            String msg = "Can not create soap message or connection problem";
            log.log(msg, ex);
            throw new TaskConnFailedError(msg, t.createTask());
        } catch (java.lang.NoClassDefFoundError e) {
            throw new TaskInternalError(e.getMessage(), t.createTask());
        }
    }

    public String decodeResponse(String response) {
        response = response.replaceAll("&lt;", "<");
        response = response.replaceAll("&amp;", "&");
        response = response.replaceAll("&gt;", ">");
        response = response.replaceAll("&apos;", "\\");
        response = response.replaceAll("&quot;", "\"");
        return response;
    }

    private String getSimulationModeResponse() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        simulation_job_id = dateFormat.format(date);
        if (log.isTraceLevel(3))
            log.trace(6, "job_id: " + simulation_job_id);

        String simulation_xml_reponse = "<soapenv:Envelope "
                + "xmlns:soapenv=\""
                + soapUrl
                + "\" "
                + "xmlns:ns3=\""
                + xsdUrl
                + "\"> "
                + "<soapenv:Header/> "
                + "<soapenv:Body> "
                + "<Job xmlns:xsi=\""
                + soapUrl
                + "\" "
                + "xsi:noNamespaceSchemaLocation=\"NPSchemaForClientReq.xsd\" "
                + "xsi:type=\"netmctJobExecuteEntry\"> "
                + "    <Id>"
                + simulation_job_id
                + "</Id> "
                + "    <JobType> "
                + "        <OperationType>Execute</OperationType> "
                + "        <NPJobType>Entry</NPJobType> "
                + "    </JobType> "
                + "</Job>" + "</soapenv:Body> " + "</soapenv:Envelope>";

        return simulation_xml_reponse;
    }

    public String getLoginRequest() {
        String login = "<soapenv:Envelope " + "xmlns:soapenv=\""
                + soapUrl
                + "\" "
                + "xmlns:ns3=\""
                + xsdUrl
                + "\"> "
                + "<soapenv:Header xmlns:wsa=\""
                + headerUrl
                + "\"> <wsa:To>"
                + endPointUri
                + "</wsa:To> <wsa:MessageID>"
                + getMessageID()
                + "</wsa:MessageID> <wsa:Action>Connect</wsa:Action> </soapenv:Header> "
                + "<soapenv:Body> " + "<ns3:Connect> "
                + "<applnname>NP</applnname> " + "<user>" + userName
                + "</user> " + "<usergroup>" + userGroup + "</usergroup> "
                + "<public-key-N>" + publicKeyN + "</public-key-N> "
                + "<public-key-K>" + publicKeyK + "</public-key-K> "
                + "<password>" + encrPassword + "</password> " + "<terminal>"
                + terminal + "</terminal> " + "</ns3:Connect> "
                + "</soapenv:Body> " + "</soapenv:Envelope>";
        log.log("Returning from getLoginRequest!");
        return login;
    }

    public String parseResponse(String responseXML, String xpath, AdptTask t)
            throws TaskConfigError, TaskInternalError, TaskConnFailedError {
        DocumentBuilderFactory domFactory = null;
        DocumentBuilder domBuilder = null;
        Document document = null;
        try {
            if (log.isTraceLevel(6)) {
                log.trace(6, "Inside parseResponse()" + responseXML);
                log.trace(6, "responseXML value: " + responseXML);
                log.trace(6, "xpath value: " + xpath);
            }
            StringReader xmlReader = new StringReader(responseXML);
            InputSource xmlInp = new InputSource(xmlReader);
            domFactory = DocumentBuilderFactory.newInstance();
            domBuilder = domFactory.newDocumentBuilder();
            document = domBuilder.parse(xmlInp);
            CachedXPathAPI xPathAPI = new CachedXPathAPI();

            log.trace(6, "Checking for fault response..");
            faultCode = xPathAPI.eval(document, FAULT_CODE_PATH).str();
            if (faultCode != null && faultCode != "") {
                log.log("Fault response received!");
                log.log("Fault Code: " + faultCode);
                faultReason = xPathAPI.eval(document, FAULT_REASON_PATH).str();
                log.log("Fault Reason: " + faultReason);
                // handleFault(faultCode, t);
                if (faultCode
                        .equalsIgnoreCase(SOAP_SESSIONID_INVALID_FAULTCODE)) {
                    log.log("Session related fault, need to relogin!");
                    throw new TaskConnFailedError(faultCode, t.createTask());
                } else {
                    return "FAULT";
                }
            }
            String fetchedVal = xPathAPI.eval(document, xpath).str();
            if (log.isTraceLevel(6))
                log.trace(6, "Fetched String : " + fetchedVal);

            return fetchedVal;
        } catch (TaskConnFailedError tcf) {
            throw tcf;
        } catch (SAXParseException spe) {
            String msg = "Response file is not valid";
            log.log(msg, spe);
            // throw new TaskInternalError(msg, t.createTask(), spe);
            throw new TaskConnFailedError(msg, t.createTask(), spe);
        } catch (Exception e) {
            String msg = "Parse Response failed.";
            log.log(msg, e);
            // throw new TaskInternalError(msg, t.createTask(), e);
            throw new TaskConnFailedError(msg, t.createTask(), e);
        }
    }

    public String checkSessionStatus(String responseXML, String path, AdptTask t) {

        return "";
    }

    protected SOAPMessage createSoapMessage(String finalSoapBody, AdptTask t)
            throws TaskInternalError {

        if (log.isTraceLevel(6)) {
            log.trace(6, "In createSoapMessage");
            log.trace(6, "Converting string to document,the string is "
                    + finalSoapBody);
        }
        MessageFactory messageFactory = null;
        SOAPMessage message = null;
        SOAPPart soapPart;
        try {
            StringReader reader = new StringReader(finalSoapBody);
            InputSource is = new InputSource(reader);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            dbf.setCoalescing(false);
            dbf.setExpandEntityReferences(false);
            dbf.setIgnoringElementContentWhitespace(true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(is);
            if (!isValidateFlag) {
                String msg = "Soap body is not valid";
                log.log(msg);
            }

            log.log("Using MessageFactory = " + soapMessageFactory);
            if (soapMessageFactory.startsWith(DEFAULT)) {
                if (soapMessageFactory.startsWith(DEFAULT + ".")) {
                    String version = soapMessageFactory.substring(DEFAULT
                            .length() + 1);
                    if (version.equals("soap12"))
                        messageFactory = MessageFactory
                                .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
                    else if (version.equals("soap11"))
                        messageFactory = MessageFactory
                                .newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
                    else {
                        throw new TaskInternalError("Unsupported soap version "
                                + version, t.createTask());
                    }
                } else
                    messageFactory = MessageFactory.newInstance();
            } else {
                messageFactory = (MessageFactory) Class.forName(
                        soapMessageFactory).newInstance();
            }
            // messageFactory = (MessageFactory)
            // Class.forName(soapMessageFactory).newInstance();

            if (log.isTraceLevel(3))
                log.trace(3, "MessageFactory class:"
                        + messageFactory.getClass().getName());
            message = messageFactory.createMessage();
            // doc = createDocument(finalSoapBody,t,false);
            DOMSource domSource = new DOMSource(doc);
            soapPart = message.getSOAPPart();
            soapPart.setContent(domSource);
            message.saveChanges();

            if (log.isTraceLevel(6)) {
                log
                        .trace(6,
                                "Message formed, returning from createSoapMessage()"
                                        + message.getSOAPPart().getContent()
                                                .toString());
            }

        } catch (Exception e) {
            String msg = "Create SOAP Message failed";
            log.log(msg, e);
            throw new TaskInternalError(msg, t.createTask(), e);
        }
        return message;
    }

    public String getSimulationLoginResponse() {
        String login_response = "<soapenv:Envelope " + "xmlns:soapenv=\""
                + soapUrl + "\" " + "xmlns:ns3=\"" + xsdUrl + "\"> "
                + "<soapenv:Header/> " + "<soapenv:Body> "
                + "<ns3:ConnectResponse> " + "<session-id>123</session-id> "
                + "</ns3:ConnectResponse> " + " </soapenv:Body> "
                + " </soapenv:Envelope>";

        return login_response;
    }

    public String getPingServerRequest() {
        String pingServerRequest = "<soapenv:Envelope "
                + "xmlns:soapenv=\""
                + soapUrl
                + "\" "
                + "xmlns:ns3=\""
                + xsdUrl
                + "\"> "
                + "<soapenv:Header xmlns:wsa=\""
                + headerUrl
                + "\"> <wsa:To>"
                + endPointUri
                + "</wsa:To> <wsa:MessageID>"
                + getMessageID()
                + "</wsa:MessageID> <wsa:Action>PingServer</wsa:Action> </soapenv:Header> "
                + "<soapenv:Body> " + "<ns3:PingServer> "
                + "<applnname>NP</applnname> " + "</ns3:PingServer> "
                + " </soapenv:Body> " + " </soapenv:Envelope>";

        return pingServerRequest;
    }

    public String getSimulationPingServerResponse() {
        String pingServerResponse = "<soapenv:Envelope " + "xmlns:soapenv=\""
                + soapUrl + "\" " + "xmlns:ns3=\"" + xsdUrl + "\"> "
                + "<soapenv:Header/> " + "<soapenv:Body> "
                + "<ns3:PingServerResponse> " + "<status>TRUE</status> "
                + "</ns3:PingServerResponse> " + " </soapenv:Body> "
                + " </soapenv:Envelope>";

        return pingServerResponse;
    }

    public String getPublicKeyRequest() {
        String publicKeyRequest = "<soapenv:Envelope "
                + "xmlns:soapenv=\""
                + soapUrl
                + "\" "
                + "xmlns:ns3=\""
                + xsdUrl
                + "\"> "
                + "<soapenv:Header xmlns:wsa=\""
                + headerUrl
                + "\"> <wsa:To>"
                + endPointUri
                + "</wsa:To> <wsa:MessageID>"
                + getMessageID()
                + "</wsa:MessageID> <wsa:Action>GetPublicKey</wsa:Action> </soapenv:Header> "
                + "<soapenv:Body> " + "<ns3:GetPublicKey/> "
                + " </soapenv:Body> " + " </soapenv:Envelope>";

        return publicKeyRequest;
    }

    public String getSimulationPublicKeyResponse() {
        String pingServerResponse = "<soapenv:Envelope " + "xmlns:soapenv=\""
                + soapUrl + "\" " + "xmlns:ns3=\"" + xsdUrl + "\"> "
                + "<soapenv:Header/> " + "<soapenv:Body> "
                + "<ns3:GetPublicKeyResponse> "
                + "<public-key-N>456</public-key-N> "
                + "<public-key-K>789</public-key-K> "
                + "</ns3:GetPublicKeyResponse> " + " </soapenv:Body> "
                + " </soapenv:Envelope>";

        return pingServerResponse;
    }

    public String getCloseSessionRequest() {
        String closeSessionRequest = "<soapenv:Envelope "
                + "xmlns:soapenv=\""
                + soapUrl
                + "\" "
                + "xmlns:ns3=\""
                + xsdUrl
                + "\"> "
                + "<soapenv:Header xmlns:wsa=\""
                + headerUrl
                + "\"> <wsa:To>"
                + endPointUri
                + "</wsa:To> <wsa:MessageID>"
                + getMessageID()
                + "</wsa:MessageID> <wsa:Action>CloseSession</wsa:Action> </soapenv:Header> "
                + "<soapenv:Body> " + "<ns3:CloseSession> " + "<sessionid>"
                + this.sessionId + "</sessionid> " + "</ns3:CloseSession> "
                + " </soapenv:Body> " + " </soapenv:Envelope>";

        return closeSessionRequest;
    }

    public String getSimulationCloseSessionResponse() {
        String closeSessionResponse = "<soapenv:Envelope " + "xmlns:soapenv=\""
                + soapUrl + "\" " + "xmlns:ns3=\"" + xsdUrl + "\"> "
                + "<soapenv:Header/> " + "<soapenv:Body> "
                + "<ns3:CloseSessionResponse> " + "<status>TRUE</status> "
                + "</ns3:CloseSessionResponse> " + " </soapenv:Body> "
                + " </soapenv:Envelope>";

        return closeSessionResponse;
    }

    public String pingServer(AdptTask t) throws TaskConnFailedError {
        SOAPMessage pingServerMessage;
        SOAPMessage response;
        String status = "";
        if (log.isTraceLevel(3))
            log.trace(3, "Inside pingServer()");
        try {
            String pingServerRequest = getPingServerRequest();
            if (log.isTraceLevel(3))
                log
                        .trace(3, "Ping Server Request is :- \n"
                                + pingServerRequest);
            pingServerMessage = createSoapMessage(pingServerRequest, t);

            String pingServerResponse = null;
            if (isSimulationMode) {
                pingServerResponse = getSimulationPingServerResponse();
                if (log.isTraceLevel(3))
                    log
                            .trace(3,
                                    "Calling parseResponse() to parse Ping Server simulation response..");
                status = parseResponse(pingServerResponse, PING_SERVER_PATH, t);
                if (log.isTraceLevel(3))
                    log.trace(3, "Simulation Ping Server status:" + status);
            } else {
                // send the request to the server
                ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                pingServerMessage.writeTo(outMsg);
                log.log("Sending Ping Server request to server"
                        + outMsg.toString());
                response = connection.call(pingServerMessage, endPoint);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.writeTo(out);
                pingServerResponse = out.toString();
                log.log(" Server response is:-\n " + pingServerResponse);
                log
                        .trace(3,
                                "Calling parseResponse() to parse Ping Server response..");
                status = parseResponse(pingServerResponse, PING_SERVER_PATH, t);
                if (log.isTraceLevel(3))
                    log.trace(3, "Ping Server status:" + status);
            }

        } catch (TaskConnFailedError tcf) {
            throw tcf;
        } catch (Exception e) {
            String msg = "Ping Server failed";
            log.log(msg, e);
            throw new TaskConnFailedError(msg, t.createTask(), e);
        }
        return status;
    }

    public void getPublicKey(AdptTask t) throws TaskConnFailedError {
        SOAPMessage publicKeyMessage;
        SOAPMessage response;
        try {
            if (log.isTraceLevel(3))
                log.trace(3, "Inside getPublicKey()");
            String publicKeyRequest = getPublicKeyRequest();
            if (log.isTraceLevel(3))
                log.trace(3, "Public Key Request is : " + publicKeyRequest);
            publicKeyMessage = createSoapMessage(publicKeyRequest, t);

            String publicKeyResponse = null;
            if (isSimulationMode) {
                publicKeyResponse = getSimulationPublicKeyResponse();
                if (log.isTraceLevel(3))
                    log
                            .trace(3,
                                    "Calling parseResponse() to parse Public N Key simulation response..");
                publicKeyN = parseResponse(publicKeyResponse,
                        PUBLIC_KEY_N_PATH, t);
                if (log.isTraceLevel(3)) {
                    log
                            .trace(3, "Simulation Public N Key value: "
                                    + publicKeyN);
                    log
                            .trace(3,
                                    "Calling parseResponse() to parse Public K Key simulation response..");
                }
                publicKeyK = parseResponse(publicKeyResponse,
                        PUBLIC_KEY_K_PATH, t);
                if (log.isTraceLevel(3))
                    log
                            .trace(3, "Simulation Public K key value: "
                                    + publicKeyK);

            } else {
                // send the request to the server
                ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                publicKeyMessage.writeTo(outMsg);
                if (log.isTraceLevel(3))
                    log.trace(3, "Sending Public Key request to server"
                            + outMsg.toString());
                response = connection.call(publicKeyMessage, endPoint);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.writeTo(out);
                publicKeyResponse = out.toString();
                log.log(" Server response is: " + publicKeyResponse);
                if (log.isTraceLevel(3))
                    log
                            .trace(3,
                                    "Calling parseResponse() to parse Public N Key response..");
                publicKeyN = parseResponse(publicKeyResponse,
                        PUBLIC_KEY_N_PATH, t);
                if (log.isTraceLevel(3)) {
                    log.trace(3, "Public N Key value: " + publicKeyN);
                    log
                            .trace(3,
                                    "Calling parseResponse() to parse Public K Key response..");
                }
                publicKeyK = parseResponse(publicKeyResponse,
                        PUBLIC_KEY_K_PATH, t);
                if (log.isTraceLevel(3))
                    log.trace(3, "Public K Key value: " + publicKeyK);
            }

        } catch (TaskConnFailedError tcf) {
            throw tcf;
        } catch (Exception e) {
            String msg = "Get Public Key failed";
            log.log(msg, e);
            throw new TaskConnFailedError(msg, t.createTask(), e);
        }
    }

    public String rsaEncrypt(long publicKeyN, long publicKeyK, String password) {

        if (log.isTraceLevel(6)) {
            log.trace(6, "Encrypting the password...");
            log.trace(6, "publicKeyN value:-" + publicKeyN);
            log.trace(6, "publicKeyK value:-" + publicKeyK);
            log.trace(6, "password value:-" + password);
        }

        int size = password.length();
        String[] c = new String[size];
        String strFinal = "";
        long asciiValue;

        for (int i = 0; i < size; i++) {
            // convert char to ascii
            asciiValue = (long) password.charAt(i);

            // encrypt the char to a number by using formula: x^k mod n
            BigInteger x = new BigInteger(String.valueOf(asciiValue));
            BigInteger k = new BigInteger(String.valueOf(publicKeyK));
            BigInteger n = new BigInteger(String.valueOf(publicKeyN));
            BigInteger result = x.modPow(k, n);

            c[i] = String.valueOf(result.longValue());

            // add leading zeros if string length < key length
            int diff = String.valueOf(publicKeyN).length()
                    - String.valueOf(result.longValue()).length();

            for (int j = 0; j != diff; j++)
                c[i] = "0" + c[i];

            // build final string
            strFinal = strFinal + c[i];
        }
        if (log.isTraceLevel(6))
            log.trace(6, "The encrypted password is : " + strFinal);
        return strFinal;
    }

    public String getMessageID() {
        return "urn:uuid:" + java.util.UUID.randomUUID();
    }

    public void close() {
        LogoutWorkerThread logoutWorker = new LogoutWorkerThread();

        logoutWorker.start();
        log.log("soap call in worker thread with timeout=" + callTimeout);
        try {
            logoutWorker.join(callTimeout * 1000);
            if (logoutWorker.isAlive()) {
                log.log("LogoutWorkerThread is closed.......");
            } else {
                // finished
                log.log("LogoutWorkerThread is finished.......");
            }
        } catch (InterruptedException e) {
            // wont happen
        } catch (Exception e) {
            String msg = "Some exception occured during logout!";
            log.log(msg, e);
        }
        super.close();
    }

    class LogoutWorkerThread extends Thread {

        TaskExecException exception = null;

        @Override
        public void run() {
            MessageFactory messageFactory = null;
            SOAPMessage closeSessionMessage;
            SOAPMessage response;
            SOAPPart soapPart;
            SOAPEnvelope soapEnvelope;
            SOAPBody soapBody;
            SOAPHeader soapHeader;

            try {
                if (log.isTraceLevel(3))
                    log.trace(3, "Inside close()");
                String closeSessionRequest = getCloseSessionRequest();
                log.log("Close Session Request is : " + closeSessionRequest);

                StringReader reader = new StringReader(closeSessionRequest);
                InputSource is = new InputSource(reader);
                Document doc = null;
                DocumentBuilderFactory dbf = DocumentBuilderFactory
                        .newInstance();
                dbf.setNamespaceAware(true);
                dbf.setCoalescing(false);
                dbf.setExpandEntityReferences(false);
                dbf.setIgnoringElementContentWhitespace(true);
                DocumentBuilder db = dbf.newDocumentBuilder();
                doc = db.parse(is);
                if (!isValidateFlag) {
                    String msg = "Soap body is not valid";
                    log.log(msg);
                }

                log.log("Using MessageFactory = " + soapMessageFactory);
                if (soapMessageFactory.startsWith(DEFAULT)) {
                    if (soapMessageFactory.startsWith(DEFAULT + ".")) {
                        String version = soapMessageFactory.substring(DEFAULT
                                .length() + 1);
                        if (version.equals("soap12"))
                            messageFactory = MessageFactory
                                    .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
                        else if (version.equals("soap11"))
                            messageFactory = MessageFactory
                                    .newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
                        /*
                         * throw new TaskInternalError("Unsupported soap version " +
                         * version, t.createTask());
                         */
                    } else
                        messageFactory = MessageFactory.newInstance();
                } else {
                    messageFactory = (MessageFactory) Class.forName(
                            soapMessageFactory).newInstance();
                }
                // messageFactory = (MessageFactory)
                // Class.forName(soapMessageFactory).newInstance();
                if (log.isTraceLevel(3))
                    log.trace(3, "MessageFactory class = "
                            + messageFactory.getClass().getName());
                closeSessionMessage = messageFactory.createMessage();
                soapPart = closeSessionMessage.getSOAPPart();
                soapEnvelope = soapPart.getEnvelope();
                soapBody = soapEnvelope.getBody();
                soapHeader = soapEnvelope.getHeader();

                DOMSource domSource = new DOMSource(doc);
                soapPart = closeSessionMessage.getSOAPPart();
                soapPart.setContent(domSource);
                closeSessionMessage.saveChanges();

                String closeSessionResponse = null;
                if (isSimulationMode) {
                    closeSessionResponse = getSimulationCloseSessionResponse();
                } else {
                    // send the request to the server
                    ByteArrayOutputStream outMsg = new ByteArrayOutputStream();
                    closeSessionMessage.writeTo(outMsg);
                    log.log("Sending Close Session request to server: "
                            + outMsg.toString());
                    response = connection.call(closeSessionMessage, endPoint);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.writeTo(out);
                    closeSessionResponse = out.toString();
                }
                DocumentBuilderFactory domFactory = null;
                DocumentBuilder domBuilder = null;
                Document document = null;
                StringReader xmlReader = new StringReader(closeSessionResponse);
                InputSource xmlInp = new InputSource(xmlReader);
                domFactory = DocumentBuilderFactory.newInstance();
                domBuilder = domFactory.newDocumentBuilder();
                document = domBuilder.parse(xmlInp);
                // Node rootElement = document.getDocumentElement();
                CachedXPathAPI xPathAPI = new CachedXPathAPI();
                String status = xPathAPI.eval(document, CLOSE_SESSION_PATH)
                        .str();
                log.log("Close session status : " + status);

            } catch (Exception e) {
                String msg = "Some exception occured while closing session";
                log.log(msg, e);
            }
        }
    };
}
