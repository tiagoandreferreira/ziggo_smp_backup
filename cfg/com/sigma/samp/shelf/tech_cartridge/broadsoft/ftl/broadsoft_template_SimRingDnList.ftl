<#assign list = doc.List>
<#list list.GroupElement as gpe>
    <${gpe.@id}>
        <#list gpe.Element as elem>
		<#if elem.@name = "phoneNumber">
           	<phoneNumber>${elem}</phoneNumber>
		</#if>
		<#if elem.@name = "answerConfirmationRequired">
	   	<answerConfirmationRequired>${elem}</answerConfirmationRequired>
		</#if>
        </#list>
    </${gpe.@id}>
</#list>

