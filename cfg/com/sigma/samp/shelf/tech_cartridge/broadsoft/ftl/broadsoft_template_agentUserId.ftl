<#if doc.List.Element?size == 0>
<agentUserIdList xsi:nil="true"/>
<#else>
<agentUserIdList>
<#list doc.List.Element as e>
<userId>${e}</userId>
</#list>
</agentUserIdList>
</#if>