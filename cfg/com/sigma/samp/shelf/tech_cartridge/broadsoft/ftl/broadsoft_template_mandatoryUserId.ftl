<#if doc.List.Element?size == 0>
<mandatoryUsageUserIdList xsi:nil="true"/>
<#else>
<mandatoryUsageUserIdList><#list doc.List.Element as e><userId>${e}</userId></#list></mandatoryUsageUserIdList>
</#if>