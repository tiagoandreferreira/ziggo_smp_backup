<#if doc.List.Element?size == 0>
<optionalUsageUserIdList xsi:nil="true"/>
<#else>
<optionalUsageUserIdList><#list doc.List.Element as e><userId>${e}</userId></#list></optionalUsageUserIdList>
</#if>