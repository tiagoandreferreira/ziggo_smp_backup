<#assign list = doc.List>
<#list list.GroupElement as gpe>
<${gpe.@id}>
        <#list gpe.Element as elem>
        <#if elem.@name = "phoneNumber">
               <#if (elem.@nil?size != 0)>
<phoneNumber xsi:nil="true"/>
               <#else>
<phoneNumber>${elem}</phoneNumber>
                </#if>
        </#if>
        <#if elem.@name = "extension">
               <#if (elem.@nil?size != 0)>
<extension xsi:nil="true"/>
               <#else>
<extension>${elem}</extension>
                </#if>
        </#if>
        <#if elem.@name = "ringPattern">
                <#if (elem.@nil?size != 0)>
<ringPattern xsi:nil="true"/>
               <#else>
<ringPattern>${elem}</ringPattern>
                </#if>
        </#if>
        </#list>
</${gpe.@id}>
</#list>