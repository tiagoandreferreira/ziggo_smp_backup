<#assign list = doc.List>
<#list list.GroupElement as gpe>
<${gpe.@id}>
	<#list gpe.Element as elem>
	<#if elem.@name = "servicePackName">
	<servicePackName>${elem}</servicePackName>
	<#elseif elem.@name = "unauthorized">
	<unauthorized>${elem}</unauthorized>
	<#elseif elem.@name = "authorizedQuantity">
		<#if elem?number = -1>
        <authorizedQuantity>
		<unlimited>true</unlimited>
	</authorizedQuantity>
		</#if>
		<#if elem?number < 0>
		<#elseif elem?number=0>
		<unauthorized>true</unauthorized>
		<#else>
	<authorizedQuantity>
		<quantity>${elem}</quantity>
	</authorizedQuantity>
		</#if>
	</#if>
	</#list>
</${gpe.@id}>
</#list>