<?xml version="1.0" encoding="UTF-8"?>
<!--
//=============================================================================
//
//  FILE INFO
//    $Id: sb-config.xsd,v 1.24 2006/11/24 20:14:58 siarheig Exp $
//
//  DESCRIPTION
//
//  REVISION HISTORY
//  *  Based on CVS log
//
//  Copyright (c) 1993 - 2006 Sigma Systems Canada Inc. All rights reserved.
//=============================================================================
-->
<xs:schema xmlns:mf="http://www.sigma-systems.com/schemas/3.0/sb/mf" xmlns="http://www.sigma-systems.com/schemas/3.0/sb" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xi="http://www.w3.org/2001/XInclude" targetNamespace="http://www.sigma-systems.com/schemas/3.0/sb" elementFormDefault="qualified">
  <!-- ============================================================== -->
  <xs:simpleType name="DependencyCheckCode">
    <xs:restriction base="xs:string">
      <xs:enumeration value="success"/>
      <xs:enumeration value="fail"/>
      <xs:enumeration value="default"/>
      <xs:enumeration value="cancel"/>
      <xs:enumeration value="jeopardy"/>
      <xs:enumeration value="time_out"/>
    </xs:restriction>
  </xs:simpleType>
  <!-- ============================================================== -->
  <xs:simpleType name="basicType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="String"/>
      <xs:enumeration value="Integer"/>
      <xs:enumeration value="Float"/>
      <xs:enumeration value="Boolean"/>
      <xs:enumeration value="DateTime"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="yesno">
    <xs:restriction base="xs:string">
      <xs:enumeration value="yes"/>
      <xs:enumeration value="no"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="sourceInd">
    <xs:restriction base="xs:string">
      <xs:enumeration value="s"/>
      <xs:enumeration value="d"/>
      <xs:enumeration value="m"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="destInd">
    <xs:restriction base="xs:string">
      <xs:pattern value="s?d?t?p?g?"/>
    </xs:restriction>
  </xs:simpleType>
  <!-- ============================================================== -->
  <xs:element name="Holidays">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="holiday" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:attribute name="name" type="xs:string" use="required">
              <xs:annotation>
                <xs:documentation>The name of the holiday.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
            <xs:attribute name="expr" type="xs:string" use="required"/>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="sb.version" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Current version of the product (e.g. 3.0.0 build3)</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="file.version" type="xs:integer" use="required">
        <xs:annotation>
          <xs:documentation>Version of the file (i.e. 4), which is incremented at every save operation.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <!-- ============================================================== -->
  <xs:element name="SorbParm">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="RefParmType" type="RefParmTypeType" minOccurs="0"/>
        <xs:element name="RefSupplAdptParm" type="RefSupplAdptParmType" minOccurs="0"/>
        <xs:element name="Parameters" type="ParametersType"/>
      </xs:sequence>
      <xs:attribute name="sb.version" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Current version of the product (e.g. 3.0.0 build3)</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="file.version" type="xs:integer" use="required">
        <xs:annotation>
          <xs:documentation>Version of the file (i.e. 4), which is incremented at every save operation.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="RefSupplAdptParmType">
    <xs:sequence>
      <xs:element name="parm" type="parmTypeType" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="parmTypeType">
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="RefParmTypeType">
    <xs:sequence>
      <xs:element name="parm_type" type="refType" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="refType">
    <xs:attribute name="name" type="basicType" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <!-- ============================================================== -->
  <xs:complexType name="ParametersType">
    <xs:sequence>
      <xs:element name="parm" type="parmType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="parmType">
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="type" type="basicType" use="required">
      <xs:annotation>
        <xs:documentation>The type of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="reserved_ind" type="yesno" use="required">
      <xs:annotation>
        <xs:documentation>Indicates reserved parameter. Used internally by ServiceBroker. </xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="restrict_ind" type="yesno" use="required">
      <xs:annotation>
        <xs:documentation>Used to restrict a parameter from being viewed.
Values: 'Y' / 'N'
If Yes, the GUI will display asterisks instead of the value. </xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="descr" type="xs:string">
      <xs:annotation>
        <xs:documentation>Description of the parameter. </xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="ParmFromAdptType">
    <xs:sequence>
      <xs:element name="parm" type="parmType1" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ParmToAdptType">
    <xs:sequence>
      <xs:element name="parm" type="parmType1" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:element name="SorbTaskTypes">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tsk_type" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
      <xs:attribute name="sb.version" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Current version of the product (e.g. 3.0.0 build3)</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="file.version" type="xs:integer" use="required">
        <xs:annotation>
          <xs:documentation>Version of the file (i.e. 4), which is incremented at every save operation.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="parmType1">
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter. </xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="type" type="basicType" use="required">
      <xs:annotation>
        <xs:documentation>Specified the type of the parameter. </xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="is_required" type="yesno" use="required">
      <xs:annotation>
        <xs:documentation>Indicates if the value of the parameter must be supplied.Some parameters are required before we begin provisioning or impact analysis. These parameter values are captured either from the user or by some alternate source (delivery platform). Optional parameters don't have to be captured.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="tsk_typeType">
    <xs:sequence>
      <xs:element name="ParmToAdpt" type="ParmToAdptType"/>
      <xs:element name="ParmFromAdpt" type="ParmFromAdptType"/>
    </xs:sequence>
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the task type.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="is_manual" type="yesno" use="required">
      <xs:annotation>
        <xs:documentation>Yes/no attribute. "Yes" means the  task type is manual, and "No" means the task type is automatic (not manual).</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="is_one_way" type="yesno" use="optional">
      <xs:annotation>
        <xs:documentation>Yes/no attribute. "Yes" means the task type is "one way", meaning that the engine sends the task to the EA and does not wait for a reply.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="bus_rule" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>Name of the business rule (MF) assigned to the task type.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="duration" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Temporal Expression for the duration of the task type; it must be in IntvExpr format (i.e. 3 sec). Based on it, the Service Order may be computed and the Service Order may be scheduled.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="timeout" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>A temporal expression that indicates the length of time before timeout for the task type, must be in IntvExpr format (e.g.,3 days &amp; 4 hours)</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="descr" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>Task type descripton.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="mf" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>MF expression assigned to the task type.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <!-- ============================================================== -->
  <xs:complexType name="TmpltParametersType">
    <xs:sequence>
      <xs:element name="parm" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:complexContent>
            <xs:extension base="TmpltParmType"/>
          </xs:complexContent>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TaskParametersType">
    <xs:sequence>
      <xs:element name="parm" type="TaskParmType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TaskParmFromAdptType">
    <xs:sequence>
      <xs:element name="fromAdptParm" type="TaskParmType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TaskParmSupplToAdptType">
    <xs:sequence>
      <xs:element name="parm" type="TaskParmSupplType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TaskParmToAdptType">
    <xs:sequence>
      <xs:element name="toAdptParm" type="TaskParmType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TaskDepyType">
    <xs:sequence>
      <xs:element name="depy" type="depyType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TasksType">
    <xs:sequence>
      <xs:element name="tsk" type="tskType" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="pointType">
    <xs:attribute name="x" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Coordinate on the X-axis of the graphical task object.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="y" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Coordinate on Y-axis of the graphical task object.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="depyType">
    <xs:sequence>
      <xs:element name="point" type="pointType" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
    <xs:attribute name="tsk_seq_num" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Current DB sequence of the Service Order Detail's Task.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="succ_tsk_seq_num" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Successor’s DB sequence of the Service Order Detail's task.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="depy_chk_cd" type="DependencyCheckCode" use="required">
      <xs:annotation>
        <xs:documentation>Dependency check code for this transition (from one task to another). </xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="TmpltParmType">
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="dflt_val" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The default value of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="is_required" type="yesno" use="required">
      <xs:annotation>
        <xs:documentation>Indicates if the value of the parameter must be supplied.Some parameters are required before we begin provisioning or impact analysis. These parameter values are captured either from the user or by some alternate source (delivery platform). Optional parameters don't have to be captured.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="type" use="required">
      <xs:annotation>
        <xs:documentation>The type of the parameter.</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="String"/>
          <xs:enumeration value="Integer"/>
          <xs:enumeration value="Float"/>
          <xs:enumeration value="Boolean"/>
          <xs:enumeration value="DateTime"/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="direction" use="required">
      <xs:annotation>
        <xs:documentation/>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="in"/>
          <xs:enumeration value="out"/>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="TaskParmType">
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="adpt_parm_name" type="xs:string">
      <xs:annotation>
        <xs:documentation>Attribute of the adapter parameter name associated with the workflow parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="dflt_val" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The default value of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="mf" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Temporal Expression for the duration of the task type; it must be in IntvExpr format (i.e. 3 sec). Based on it, the Service Order may be computed and the Service Order may be scheduled.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="src_ind" type="sourceInd">
      <xs:annotation>
        <xs:documentation>Indicator of the source parameters (s - service order, d - detail, t - task, g - group , etc.)</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="dest_ind" type="destInd">
      <xs:annotation>
        <xs:documentation>Indicator of the destination parameter, which comes from the EA (s - service order, d - detail, t - task, etc).</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="TaskParmSupplType">
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="dflt_val" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The default value of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="src_ind" type="sourceInd">
      <xs:annotation>
        <xs:documentation>Indicator of the source parameters (s - service order, d - detail, t - task, g - group , etc.)</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="mf" type="xs:string">
      <xs:annotation>
        <xs:documentation>MF expression that calculates the default value of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <xs:element name="tmplt">
    <xs:complexType>
          <xs:sequence>
            <xs:element name="description" type="xs:string" minOccurs="0"/>
            <xs:element name="Parameters" type="TmpltParametersType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="Tasks" type="TasksType"/>
            <xs:element name="TaskDepy" type="TaskDepyType"/>
          </xs:sequence>
          <xs:attribute name="name" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>The name of the template.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="effective_dtm" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Effective date of the template (e.g., 20051010020202).</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="bus_rule" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Business rule (MF expression) assigned to the template, base on which the engine will execute or not that template.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="duration" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>This is the expected duration of the task.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="timeout" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>A temporal expression that indicates the length of time before the Service Order goes into timeout. </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="sb.version" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>Current version of the product (e.g. 3.0.0 build3).</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="file.version" type="xs:integer" use="required">
            <xs:annotation>
              <xs:documentation>Version of the file (i.e. 4), which is incremented at every save operation.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="type" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>The type of template.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="tskType">
    <xs:sequence>
      <xs:element name="ParmToAdpt" type="TaskParmToAdptType"/>
      <xs:element name="ParmFromAdpt" type="TaskParmFromAdptType"/>
      <xs:element name="ParmSupplToAdpt" type="TaskParmSupplToAdptType"/>
    </xs:sequence>
    <xs:attribute name="seq_num" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Sequence number of the parameter. </xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="name" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The name of the task.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="bus_rule" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>Name of the business rule (MF) assigned to the task type.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="mf" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>MF expression that calculates the default value of the parameter.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="x" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Coordinate on the X-axis of the graphical task object.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="y" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>Coordinate on Y-axis of the graphical task object.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="width" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>The width of the graphical task object.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="height" type="xs:integer" use="required">
      <xs:annotation>
        <xs:documentation>The height of the graphical task.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
  <!-- ============================================================== -->
  <xs:element name="tsk_type" type="tsk_typeType"/>
  <!--
**********************************************************************
   Sorb MF expressions
**********************************************************************
-->
  <xs:element name="SbMF">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="exp" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
      <xs:attribute name="sb.version" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Current version of the product (e.g. 3.0.0 build3).</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="file.version" type="xs:integer" use="optional">
        <xs:annotation>
          <xs:documentation>Version of the file (i.e. 4), which is incremented at every save operation.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="exp">
    <xs:complexType>
      <xs:sequence>
      </xs:sequence>
      <xs:attribute name="name" use="required">
        <xs:annotation>
          <xs:documentation>Unique name for expression.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="type" use="optional">
        <xs:annotation>
          <xs:documentation>The type of expression.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="descr" use="optional">
        <xs:annotation>
          <xs:documentation>The description of the expression.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
</xs:schema>
