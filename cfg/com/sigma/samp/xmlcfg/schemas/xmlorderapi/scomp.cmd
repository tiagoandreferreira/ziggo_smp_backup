##
#==================================================================
#
#  FILE INFO
# $Id: scomp.cmd,v 1.4.14.1 2011/02/28 17:27:10 davidx Exp $
#
#==================================================================
#Schema compiler
#Builds XBean types from xsd files.
set JAVA_HOME=c:\program files\java\jdk1.6.0_23
set cp=..\..\..\..\..\..\..\..\extjars\xbean.jar;..\..\..\..\..\..\..\..\extjars\jsr173_1.0_api.jar;"%JAVA_HOME%\lib\tools.jar"
set path=%JAVA_HOME%\bin;%path%


java -Xmx256m -classpath %cp% org.apache.xmlbeans.impl.tool.SchemaCompiler -out ..\..\..\..\..\..\..\..\extjars\SmpXmlOrderApi.jar  Xml*.xsd  xmlapi.xsdconfig
