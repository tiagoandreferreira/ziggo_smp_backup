#!/bin/sh
#==================================================================
#  FILE INFO
# $Id: scomp.sh,v 1.2 2006/04/12 15:52:38 davidx Exp $
#==============================================================
#Schema compiler
#Builds XBean types from xsd files.

cp=../../../util/extjars/xbean.jar:../../../util/extjars/jsr173_1.0_api.jar:$JAVA_HOME/lib/tools.jar


java -Xmx256m -classpath $cp org.apache.xmlbeans.impl.tool.SchemaCompiler -out ../../../util/extjars/SmpXmlOrderApi.jar Xml*.xsd  xmlapi.xsdconfig
