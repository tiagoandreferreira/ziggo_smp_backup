//==========================================================================
// FILE INFO
// $Id: Authentication.java,v 1.4 2006/09/26 20:23:18 alexeyk Exp $
//
//    SAMP 1.0
//    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
//    All rights reserved.
//
// REVISION HISTORY
// * Based on CVS log
//==========================================================================

package com.sigma.samp.util.sqls.dba;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Authentication
{
    public static String authenticate(String user, String pwdToCheck,
                                      String passwdDB)
    {

        String pwd = passwdDB;

        MessageDigest md = null;

        if(passwdDB != null)
        {
            int rightCurly = passwdDB.indexOf("}");

            if(rightCurly > 0 && passwdDB.charAt(0) == '{')
            {
                pwd = passwdDB.substring(rightCurly + 1);

                String algorithm = passwdDB.substring(1, rightCurly);

                try
                {
                    md = MessageDigest.getInstance(algorithm.toUpperCase());
                }
                catch(NoSuchAlgorithmException ignore)
                {
                }
            }
        }
        String hashed = md != null ? hash(md, pwdToCheck) : pwdToCheck;

        if(hashed.equals(pwd))
        {
            return "y";
        }
        return "f";
    }

    /**
     * Hashes the given plain text with the given digest algorithm, and
     * base64-encode the result.
     *
     * @param md message digest algorithm to hash with
     * @param plaintext text to hash
     * @return base64-encoded hashed text
     */
    static protected String hash(MessageDigest md, String plaintext)
    {
        BASE64Encoder enc = new BASE64Encoder();

        return enc.encodeBuffer(md.digest(plaintext.getBytes()));
    }

    private final static class BASE64Encoder
        extends CharacterEncoder
    {

        public BASE64Encoder()
        {
        }

        int bytesPerAtom()
        {
            return 3;
        }

        int bytesPerLine()
        {
            return 57;
        }

        void encodeAtom(OutputStream outputstream, byte abyte0[], int i, int
                        j)
            throws IOException
        {
            if(j == 1)
            {
                byte byte0 = abyte0[i];
                int k = 0;
                boolean flag = false;
                outputstream.write(pem_array[byte0 >>> 2 & 0x3f]);
                outputstream.write(pem_array[(byte0 << 4 & 0x30) +
                                   (k >>> 4 & 0xf)]);
                outputstream.write(61);
                outputstream.write(61);
            }
            else
            if(j == 2)
            {
                byte byte1 = abyte0[i];
                byte byte3 = abyte0[i + 1];
                int l = 0;
                outputstream.write(pem_array[byte1 >>> 2 & 0x3f]);
                outputstream.write(pem_array[(byte1 << 4 & 0x30) +
                                   (byte3 >>> 4 & 0xf)]);
                outputstream.write(pem_array[(byte3 << 2 & 0x3c) +
                                   (l >>> 6
                                    & 3)]);
                outputstream.write(61);
            }
            else
            {
                byte byte2 = abyte0[i];
                byte byte4 = abyte0[i + 1];
                byte byte5 = abyte0[i + 2];
                outputstream.write(pem_array[byte2 >>> 2 & 0x3f]);
                outputstream.write(pem_array[(byte2 << 4 & 0x30) +
                                   (byte4 >>> 4 & 0xf)]);
                outputstream.write(pem_array[(byte4 << 2 & 0x3c) +
                                   (byte5 >>> 6 & 3)]);
                outputstream.write(pem_array[byte5 & 0x3f]);
            }
        }

        void encodeLineSuffix(OutputStream outputstream)
            throws IOException
        {
        }

        private static final char pem_array[] =
            {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', '+', '/'
        };

    }

    public static abstract class CharacterEncoder
    {

        public CharacterEncoder()
        {
        }

        abstract int bytesPerAtom();

        abstract int bytesPerLine();

        void encodeBufferPrefix(OutputStream outputstream)
            throws IOException
        {
            pStream = new PrintStream(outputstream);
        }

        void encodeBufferSuffix(OutputStream outputstream)
            throws IOException
        {
        }

        void encodeLinePrefix(OutputStream outputstream, int i)
            throws IOException
        {
        }

        void encodeLineSuffix(OutputStream outputstream)
            throws IOException
        {
            pStream.println();
        }

        abstract void encodeAtom(OutputStream outputstream, byte abyte0[],
                                 int i, int j)
            throws IOException;

        protected int readFully(InputStream inputstream, byte abyte0[])
            throws IOException
        {
            for(int i = 0; i < abyte0.length; i++)
            {
                int j = inputstream.read();
                if(j == -1)
                    return i;
                abyte0[i] = (byte)j;
            }
            return abyte0.length;
        }

        public void encodeBuffer(InputStream inputstream,
                                 OutputStream outputstream)
            throws IOException
        {
            byte abyte0[] = new byte[bytesPerLine()];
            encodeBufferPrefix(outputstream);
            int j;

            do
            {
                j = readFully(inputstream, abyte0);
                if(j == -1)
                    break;
                encodeLinePrefix(outputstream, j);
                for(int i = 0; i < j; i += bytesPerAtom())
                    if(i + bytesPerAtom() <= j)
                        encodeAtom(outputstream, abyte0, i, bytesPerAtom());

                    else
                        encodeAtom(outputstream, abyte0, i, j - i);

                encodeLineSuffix(outputstream);
            }
            while(j >= bytesPerLine());
            encodeBufferSuffix(outputstream);
        }

        public void encodeBuffer(byte abyte0[], OutputStream outputstream)
            throws IOException
        {
            ByteArrayInputStream bytearrayinputstream = new
                ByteArrayInputStream(abyte0);
            encodeBuffer(((InputStream)(bytearrayinputstream)), outputstream);
        }

        public String encodeBuffer(byte abyte0[])
        {
            ByteArrayOutputStream bytearrayoutputstream = new
                ByteArrayOutputStream();
            ByteArrayInputStream bytearrayinputstream = new
                ByteArrayInputStream(abyte0);

            try
            {
                encodeBuffer(((InputStream)(bytearrayinputstream)),
                             ((OutputStream)(bytearrayoutputstream)));
            }
            catch(Exception exception)
            {
                throw new Error("ChracterEncoder::encodeBuffer internal error");
            }
            String s;
            try
            {
                s = bytearrayoutputstream.toString("UTF8");
            }
            catch(UnsupportedEncodingException unsupportedencodingexception)
            {
                throw new Error(
                    "CharacterEncoder::encodeBuffer internal error(2)");
            }
            return s;
        }

        protected PrintStream pStream;
    }

}
