update CUST_DIN_QOS_FLAVOR  set FILTER = 'FILTERC' where DIN_QOS_FLV_ID = (select DIN_QOS_FLV_ID from CUST_DIN_QOS where QOS_NAME='QOSIPVPN_ZZ1_MT_SIP');
update CUST_DIN_QOS_FLAVOR  set FILTER = 'FILTERC'  where DIN_QOS_FLV_ID = (select DIN_QOS_FLV_ID from CUST_DIN_QOS where QOS_NAME='QOSIPVPN_ZZ2_MT_SIP');
update CUST_DIN_QOS_FLAVOR  set FILTER = 'FILTERC'  where DIN_QOS_FLV_ID = (select DIN_QOS_FLV_ID from CUST_DIN_QOS where QOS_NAME='QOSIPVPN_ZZ3_MT_SIP');

commit;
delete from CUST_ALLOWED_MODEM_PROFILE where DIN_QOS_ID = ( select DIN_QOS_ID from CUST_DIN_QOS where QOS_NAME='SYM_ZZ1');
delete from CUST_ALLOWED_MODEM_PROFILE where DIN_QOS_ID = ( select DIN_QOS_ID from CUST_DIN_QOS where QOS_NAME='SYM_ZZ2');
delete from CUST_ALLOWED_MODEM_PROFILE where DIN_QOS_ID = ( select DIN_QOS_ID from CUST_DIN_QOS where QOS_NAME='SYM_ZZ3');
delete from CUST_ALLOWED_MODEM_PROFILE where DIN_QOS_ID = ( select DIN_QOS_ID from CUST_DIN_QOS where QOS_NAME='SYM_ZZ1_POIP');
delete from CUST_ALLOWED_MODEM_PROFILE where DIN_QOS_ID = ( select DIN_QOS_ID from CUST_DIN_QOS where QOS_NAME='SYM_ZZ2_POIP');
delete from CUST_ALLOWED_MODEM_PROFILE where DIN_QOS_ID = ( select DIN_QOS_ID from CUST_DIN_QOS where QOS_NAME='SYM_ZZ3_POIP');

commit;

delete from CUST_DIN_QOS where QOS_NAME='SYM_ZZ1';
delete from CUST_DIN_QOS where QOS_NAME='SYM_ZZ2';
delete from CUST_DIN_QOS where QOS_NAME='SYM_ZZ3';
delete from CUST_DIN_QOS where QOS_NAME='SYM_ZZ1_POIP';
delete from CUST_DIN_QOS where QOS_NAME='SYM_ZZ2_POIP';
delete from CUST_DIN_QOS where QOS_NAME='SYM_ZZ3_POIP';

commit;

delete from CUST_DIN_QOS_FLAVOR where QOS_FLAVOR_NAME='lightred';
delete from CUST_DIN_QOS_FLAVOR where QOS_FLAVOR_NAME='darkred';
delete from CUST_DIN_QOS_FLAVOR where QOS_FLAVOR_NAME='bloodred';

commit;