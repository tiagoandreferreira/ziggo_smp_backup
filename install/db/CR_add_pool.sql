--=======================================================================
-- FILE
-- $Id: CR_add_pool.sql,v 1.1 2012/03/06 09:53:20 bharath Exp $
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for deleting tn

SPOOL CR_add_pool.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure add_pool  ---------')
CREATE OR REPLACE PROCEDURE add_pool (
                    rsrc_cate IN VARCHAR2,
                    rsrc_base IN NUMBER,
                    rsrc_num IN NUMBER,
                    rsrc_step IN NUMBER,
                    rsrc_status IN VARCHAR2,
                    resource_typ_nm IN VARCHAR2,
                    err_code OUT NUMBER,
                    err_desc OUT VARCHAR2)
IS
 resource_state  NTWK_RESOURCE.rsrc_state%TYPE;

 i NUMBER;
 num_of_rec NUMBER;
 BEGIN
    err_code := 1;
    err_desc:='Success';
    i := rsrc_base;


    WHILE  i <= rsrc_num
    LOOP
        INSERT INTO NTWK_RESOURCE (RSRC_ID, RSRC_NM, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, RSRC_CATEGORY, CREATED_DTM, CREATED_BY)
        VALUES(NTWK_RESOURCE_SEQ.NEXTVAL, i, resource_typ_nm, rsrc_status, rsrc_cate, SYSDATE, 'ADD_POOL');

	i := i + rsrc_step;

    END LOOP;

EXCEPTION
	    WHEN OTHERS THEN
            err_code:= SQLCODE;
            err_desc:= SQLERRM;
END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure add_tn created sucessfully  ---------')
spool off
