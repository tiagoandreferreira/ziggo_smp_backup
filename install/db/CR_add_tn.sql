--=======================================================================
-- FILE
-- $Id: CR_add_tn.sql,v 1.11 2009/01/23 01:34:58 michaell Exp $
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for deleting tn

SPOOL CR_add_tn.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure add_tn  ---------')

CREATE OR REPLACE
PROCEDURE add_tn (rsrc_cate IN VARCHAR2,
                  rsrc_n IN VARCHAR2,
                  rsrc_status IN VARCHAR2,
                  resource_typ_nm IN VARCHAR2,
                  porting_type IN VARCHAR2,
                  err_code OUT NUMBER,
                  err_desc OUT VARCHAR2) 
IS
 resource_id  NTWK_RESOURCE.rsrc_id%TYPE;
 resource_state  NTWK_RESOURCE.rsrc_state%TYPE;
 duplicate_tn EXCEPTION;
 invalid_parm EXCEPTION;
 num_of_rec NUMBER;
 insert_ind BOOLEAN;
 update_ind BOOLEAN;
 BEGIN
    err_code := 1;
    num_of_rec := 0;
    insert_ind := FALSE;
    update_ind := FALSE;
    
    IF resource_typ_nm = 'Extension' THEN
        SELECT count(*) INTO num_of_rec
        FROM NTWK_RESOURCE
        WHERE RSRC_NM = rsrc_n
          AND CONSUMABLE_RSRC_TYP_NM = resource_typ_nm 
          AND RSRC_CATEGORY = rsrc_cate;
        IF num_of_rec > 0 THEN
            update_ind := TRUE;
        ELSE
            insert_ind := TRUE;
        END IF;
    ELSIF resource_typ_nm = 'Telephone Number' THEN
        SELECT count(*) INTO num_of_rec
        FROM NTWK_RESOURCE
        WHERE RSRC_NM = rsrc_n
          AND CONSUMABLE_RSRC_TYP_NM = resource_typ_nm;
        IF num_of_rec > 0 THEN
            SELECT count(*) INTO num_of_rec
            FROM NTWK_RESOURCE
            WHERE RSRC_NM = rsrc_n
              AND CONSUMABLE_RSRC_TYP_NM = resource_typ_nm
              AND RSRC_CATEGORY <> rsrc_cate;
            IF num_of_rec > 0 THEN
                RAISE duplicate_tn;
            ELSE 
                SELECT rsrc_state INTO resource_state
                FROM NTWK_RESOURCE
                WHERE RSRC_NM = rsrc_n
                  AND CONSUMABLE_RSRC_TYP_NM = resource_typ_nm;
                IF resource_state <> 'Active' THEN
                    update_ind := TRUE; 
                END IF;
            END IF;
        ELSE
            insert_ind := TRUE;
        END IF;

    ELSE
        RAISE invalid_parm;
    END IF;
    IF insert_ind THEN
        SELECT NTWK_RESOURCE_SEQ.nextval INTO resource_id FROM DUAL;
        INSERT INTO NTWK_RESOURCE (rsrc_id, rsrc_category, rsrc_nm, rsrc_state, consumable_rsrc_typ_nm, created_dtm, created_by) 
            VALUES(resource_id, rsrc_cate, rsrc_n, rsrc_status, resource_typ_nm, SYSDATE, 'SMP');
        INSERT INTO NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
             VALUES (resource_id, get_parm_id('portingStatus', get_class_id ('stm_resource')), porting_type, SYSDATE, 'SMP');
    END IF;
    IF update_ind THEN
        update_tn_status(rsrc_status, rsrc_cate, rsrc_n, err_code, err_desc);
    END IF;
    err_desc:='Success';
 EXCEPTION
        WHEN invalid_parm THEN
            err_code:=-3;
            err_desc:='Invalid value for resource_typ_nm parameter';
        WHEN duplicate_tn THEN
            err_code:=-2;
            err_desc:='Failed to insert record into NTWK_RESOURCE. Duplicate TN';
        WHEN OTHERS THEN
            err_code:=SQLCODE;
            err_desc:='Failed to create ntwrk_resource: ' || SQLERRM;
            ROLLBACK;
 END;
/

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure add_tn created sucessfully  ---------')
spool off
