 --=======================================================================
-- FILE
-- $Id: CR_delete_tn.sql,v 1.4 2009/01/23 01:34:58 michaell Exp $
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for deleting tn

SPOOL CR_delete_tn.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure delete_tn  ---------')

 CREATE OR REPLACE
 PROCEDURE delete_tn (rsrc_cate IN VARCHAR2,rsrc_n IN VARCHAR2,err_code OUT NUMBER,err_desc OUT VARCHAR2) is
 begin
    select count(*) into err_code from NTWK_RESOURCE where rsrc_category=rsrc_cate and rsrc_nm=rsrc_n;

    IF err_code > 0 THEN
       delete from NTWK_RESOURCE_PARM where rsrc_id in (select rsrc_id from NTWK_RESOURCE where rsrc_category=rsrc_cate and rsrc_nm=rsrc_n);
       delete from NTWK_RESOURCE where rsrc_category=rsrc_cate and rsrc_nm=rsrc_n;
    ELSE
       err_code := 1;
    END IF;
  EXCEPTION
        when OTHERS then
        err_code:=SQLCODE;
        err_desc:=SQLERRM;
 end;
/

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure delete_tn created sucessfully  ---------')
spool off
