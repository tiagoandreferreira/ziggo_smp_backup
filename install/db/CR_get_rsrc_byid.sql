--=======================================================================
-- FILE
-- $Id: CR_get_rsrc_byid.sql,v 1.2 2012/04/16 06:50:53 bharath Exp $
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for deleting tn

SPOOL CR_get_rsrc_byid.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure get_rsrc_byid  ---------')


CREATE OR REPLACE PROCEDURE get_rsrc_byid (
   rsrc_cate         IN       VARCHAR2,
   rsrc_status       IN       VARCHAR2,
   resource_typ_nm   IN       VARCHAR2,
   subsvc_id         IN       NUMBER,
   rsrc_st_old       IN       VARCHAR2,
   err_code          OUT      NUMBER,
   err_desc          OUT      VARCHAR2,
   rsrc_n            OUT      VARCHAR2
)
IS
   rs_nm   VARCHAR2 (30);
   rs_id   NUMBER;
BEGIN
   err_code := 1;
   err_desc := 'Success';

   SELECT rsrc_nm, rsrc_id
         INTO rs_nm, rs_id
         FROM (SELECT   rsrc_nm, rsrc_id
                   FROM ntwk_resource
                  WHERE consumable_rsrc_typ_nm = resource_typ_nm
                    AND rsrc_category = rsrc_cate
                    AND rsrc_state = rsrc_st_old
                     )
        WHERE ROWNUM < 2
   FOR UPDATE;

   IF rs_nm IS NULL
   THEN
      err_code := TO_CHAR ('-1');
      err_desc := TO_CHAR ('No record found for update');
   ELSE
      rsrc_n := rs_nm;

      UPDATE ntwk_resource
         SET rsrc_state = rsrc_status,
             sub_svc_id = subsvc_id,
             modified_by = 'get_rsrc_byid'
       WHERE rsrc_id = rs_id;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      err_code := SQLCODE;
      err_desc := SQLERRM;
END; 
/

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure add_tn created sucessfully  ---------')
spool off
