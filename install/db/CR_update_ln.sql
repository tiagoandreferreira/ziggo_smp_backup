 --=======================================================================
-- FILE
-- $$Id: CR_update_ln.sql,v 1.6 2012/03/28 14:31:09 zhenl Exp $$
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for updating line status

SPOOL CR_update_ln.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure update_ln_status  ---------')

CREATE OR REPLACE
PROCEDURE update_len_status (rsrc_st IN VARCHAR2,rsrc_n IN VARCHAR2,err_code OUT NUMBER,err_desc OUT VARCHAR2) is
begin
       update NTWK_RESOURCE set rsrc_state = rsrc_st, modified_by='update_ln' where replace(rsrc_nm, ' ') = replace(rsrc_n, ' ');
       select count(*) into err_code from NTWK_RESOURCE where modified_by='update_ln' and replace(rsrc_nm, ' ') = replace(rsrc_n, ' ');
EXCEPTION
       when OTHERS then
       err_code:=SQLCODE;
       err_desc:=SQLERRM;
end;
/

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure update_ln_status is created successfully  ---------')
spool off
