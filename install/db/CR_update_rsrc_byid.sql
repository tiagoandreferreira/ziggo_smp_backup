
--=======================================================================
-- FILE
-- $Id: CR_update_rsrc_byid.sql,v 1.1 2012/03/06 09:53:21 bharath Exp $
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for deleting tn

SPOOL CR_update_rsrc_byid.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure update_rsrc_byid  ---------')

CREATE OR REPLACE PROCEDURE update_rsrc_byid (
                            rsrc_st IN VARCHAR2,
                            subsvc_id IN NUMBER,
                            resource_typ_nm IN VARCHAR2,
                            err_code OUT NUMBER,
                            err_desc OUT VARCHAR2,
							rsrc_n OUT VARCHAR2)
IS
rs_nm VARCHAR2(30);

BEGIN
err_code := 1;
err_desc:='Success';

SELECT RSRC_NM into rs_nm from NTWK_RESOURCE WHERE sub_svc_id=subsvc_id AND resource_typ_nm = resource_typ_nm;

IF rs_nm is NULL THEN
    err_code := TO_CHAR('-1');
    err_desc := TO_CHAR('No record found for update');
ELSE
rsrc_n := rs_nm;
    UPDATE NTWK_RESOURCE
    SET rsrc_state = rsrc_st,sub_svc_id = null, modified_by='update_rsrc_byid'
    WHERE sub_svc_id=subsvc_id
      AND resource_typ_nm=resource_typ_nm;
END IF;
EXCEPTION
    WHEN OTHERS THEN
    err_code:=SQLCODE;
    err_desc:=SQLERRM;
END;
/

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure add_tn created sucessfully  ---------')
spool off