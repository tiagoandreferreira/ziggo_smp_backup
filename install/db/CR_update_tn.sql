--=======================================================================
-- FILE
-- $$Id: CR_update_tn.sql,v 1.3 2009/01/23 01:34:58 michaell Exp $$
--
-- REVISON HISTORY
-- * Based on CVS log
--=======================================================================
-- oracle procedure for updating tn status

SPOOL CR_update_tn.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   creating procedure update_tn_status  ---------')
CREATE OR REPLACE
PROCEDURE update_tn_status (rsrc_st IN VARCHAR2, 
                            rsrc_cat IN VARCHAR2, 
                            rsrc_n IN VARCHAR2,
                            err_code OUT NUMBER,
                            err_desc OUT VARCHAR2) 
IS
BEGIN
    UPDATE NTWK_RESOURCE 
    SET rsrc_state = rsrc_st, modified_by='update_tn' 
    WHERE rsrc_category=rsrc_cat 
      AND rsrc_nm=rsrc_n;
    err_code := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
    err_code:=SQLCODE;
    err_desc:=SQLERRM;
        ROLLBACK;
END;
/

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   procedure update_tn_status is created successfully  ---------')
spool off
