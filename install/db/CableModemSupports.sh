#!/usr/bin/ksh
#--============================================================================
#--    $Id: CableModem.sh,v 1.3 2013/05/10 07:49:28 sunil Exp $
#--		$Id: CableModem.sh,v 1.4 2013/12/06 07:49:28 Rujuta Exp $
#--		$Id: CableModem.sh,v 1.5 2014/12/31 18:30:00 Divya Exp $
#--  DESCRIPTION
#--
#--  RELATED SCRIPTS
#--
#--  REVISION HISTORY
#--  * Based on CVS log
#--============================================================================

CUR_DIR=`pwd`

echo ${CUR_DIR}

LOG_DATE=`date '+%Y%m%d.%H%M%S'`
LOG_FILE=${CUR_DIR}/CableModemSupports.$LOG_DATE.log

. ../../util/setEnv.sh

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    exit 1
fi

LOGIN=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}

#Creating Table
echo
echo Creating CUST_SEQUENCE TABLE   | tee -a $LOG_FILE

echo Executing CUST_SEQUENCE.sql | tee -a $LOG_FILE
cat  CableModemSupports/CUST_SEQUENCE.sql | sqlplus $LOGIN >> $LOG_FILE

echo Creating BAC_MODEM_MFG TABLE   | tee -a $LOG_FILE

echo Executing BAC_MODEM_MFG.sql | tee -a $LOG_FILE
cat  CableModemSupports/BAC_MODEM_MFG.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Creating DIN_QOS TABLE   | tee -a $LOG_FILE

echo Executing DIN_QOS.sql | tee -a $LOG_FILE
cat  CableModemSupports/DIN_QOS.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Creating Allowed_Modem_Profile TABLE   | tee -a $LOG_FILE

echo Executing Allowed_Modem_Profile.sql | tee -a $LOG_FILE
cat  CableModemSupports/Allowed_Modem_Profile.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Creating DIN_QOS_FLAVOR TABLE   | tee -a $LOG_FILE

echo Executing DIN_QOS_FLAVOR.sql | tee -a $LOG_FILE
cat  CableModemSupports/DIN_QOS_FLAVOR.sql | sqlplus $LOGIN >> $LOG_FILE

echo Executing Service_Provider_Digits.sql
cat  CableModemSupports/service_provider_digits.sql | sqlplus $LOGIN >> $LOG_FILE

#Creating procedures

echo
echo Creating INSERT_BAC_MODEM_MFG Procedure   | tee -a $LOG_FILE

echo Executing INSERT_BAC_MODEM_MFG.sql | tee -a $LOG_FILE
cat  CableModemSupports/INSERT_BAC_MODEM_MFG.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Creating INSERT_DIN_QOS procedure  | tee -a $LOG_FILE

echo Executing INSERT_DIN_QOS.sql | tee -a $LOG_FILE
cat  CableModemSupports/INSERT_DIN_QOS.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Creating INSERT_ALLOWED_MODEM_PROFILE Procedure   | tee -a $LOG_FILE

echo Executing INSERT_ALLOWED_MODEM_PROFILE.sql | tee -a $LOG_FILE
cat  CableModemSupports/INSERT_ALLOWED_MODEM_PROFILE.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Creating INSERT_DIN_QOS_FLAVOR Procedure   | tee -a $LOG_FILE

echo Executing INSERT_DIN_QOS_FLAVOR.sql | tee -a $LOG_FILE
cat  CableModemSupports/INSERT_DIN_QOS_FLAVOR.sql | sqlplus $LOGIN >> $LOG_FILE

#Inserting Values to Tables

echo
echo Inserting values BAC_MODEM_MFG Table   | tee -a $LOG_FILE

echo Executing ADD_BAC_MODEM_MFG.sql | tee -a $LOG_FILE
cat  CableModemSupports/ADD_BAC_MODEM_MFG.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Inserting to DIN_QOS Table  | tee -a $LOG_FILE

echo Executing ADD_DIN_QOS.sql | tee -a $LOG_FILE
cat  CableModemSupports/ADD_DIN_QOS.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Inserting to Allowed_Modem_Profile Table   | tee -a $LOG_FILE

echo Executing ADD_Allowed_Modem_Profile.sql | tee -a $LOG_FILE
cat  CableModemSupports/ADD_Allowed_Modem_Profile.sql | sqlplus $LOGIN >> $LOG_FILE

echo
echo Inserting to DIN_QOS_FLAVOR Table  | tee -a $LOG_FILE

echo Executing ADD_DIN_QOS_FLAVOR.sql | tee -a $LOG_FILE
cat  CableModemSupports/ADD_DIN_QOS_FLAVOR.sql | sqlplus $LOGIN >> $LOG_FILE

#Creating view for cm_mac_view

echo Executing cm_mac_view.sql | tee -a $LOG_FILE
cat  CableModemSupports/cm_mac_view.sql | sqlplus $LOGIN >> $LOG_FILE

#Creating view for new BAC4.2 view

echo Executing BAC4.2_CableModemSupports.sql | tee -a $LOG_FILE
cat  CableModemSupports/BAC4.2_CableModemSupports.sql | sqlplus $LOGIN >> $LOG_FILE

#Creating view for new HRZ view

echo Executing hrz_qos_entries.sql | tee -a $LOG_FILE
cat  CableModemSupports/hrz_qos_entries.sql | sqlplus $LOGIN >> $LOG_FILE

#-------------------------------------------------------------------------
# Verify the result
#-------------------------------------------------------------------------

if egrep -s '^ERROR' $LOG_FILE || egrep -s '^Error' $LOG_FILE
then
    echo Errors found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s  ORA_* $LOG_FILE
then
    echo Oracle errors found. See \'$LOG_FILE\' for details
    exit 1
else
echo  Script execution completed successfully. | tee -a $LOG_FILE
	  echo See \'$LOG_FILE\' for details
      exit 0
fi


