
  CREATE TABLE "CUST_BAC_MODEM_MFG" 
   (	"MFG_ID" NUMBER NOT NULL ENABLE, 
	"COS_MODEL" VARCHAR2(40 BYTE), 
	"COS_MANUFACTURER" VARCHAR2(40 BYTE), 
	"DOCSIS_VERSION" VARCHAR2(20 BYTE), 
	"MTA_LINES" NUMBER, 
	"IS_MTA" NUMBER NOT NULL ENABLE, 
	"SNMP_MODEL" VARCHAR2(40 BYTE), 
	"SNMP_VENDOR" VARCHAR2(50 BYTE), 
	"PHONE_LINES" NUMBER, 
	"IS_ROUTER" NUMBER, 
	"PUBLIC_WIFI" NUMBER, 
	 CONSTRAINT "BAC_MODEM_MFG_PK" PRIMARY KEY ("MFG_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
commit;
 
