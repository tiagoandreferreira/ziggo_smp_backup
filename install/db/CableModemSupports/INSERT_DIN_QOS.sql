
  CREATE OR REPLACE PROCEDURE "INSERT_DIN_QOS" 
( P_DIN_QOS_ID IN CUST_DIN_QOS.DIN_QOS_ID%TYPE
, P_QOS_NAME IN CUST_DIN_QOS.QOS_NAME%TYPE
, P_DIN_QOS_FLV_ID IN CUST_DIN_QOS.DIN_QOS_FLV_ID%TYPE
) IS

BEGIN
  
  INSERT INTO CUST_DIN_QOS (
  DIN_QOS_ID,QOS_NAME,DIN_QOS_FLV_ID )
  values(P_DIN_QOS_ID,P_QOS_NAME,P_DIN_QOS_FLV_ID);
  
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating table : ' || SQLERRM ||
          P_DIN_QOS_ID );
          

END;
/
commit;
 
