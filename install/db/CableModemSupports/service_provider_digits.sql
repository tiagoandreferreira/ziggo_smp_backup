--============================================================================
-- $Id: service_provider_digits.sql,v 1.1 2011;06;6 14:02:19 krishnan Exp $
--============================================================================

spool service_provder_digits.log

-- DDL script for creating service_provider_3_digit and service_provider_4_digit table

create table service_provider_3_digit (area_code varchar2(50));

create table service_provider_4_digit (area_code varchar2(50));

-- DML script for inserting data into service_provider_3_digit table

insert into service_provider_3_digit values ('010');
insert into service_provider_3_digit values ('013');
insert into service_provider_3_digit values ('015');
insert into service_provider_3_digit values ('020');
insert into service_provider_3_digit values ('023');
insert into service_provider_3_digit values ('024');
insert into service_provider_3_digit values ('026');
insert into service_provider_3_digit values ('030');
insert into service_provider_3_digit values ('033');
insert into service_provider_3_digit values ('035');
insert into service_provider_3_digit values ('036');
insert into service_provider_3_digit values ('038');
insert into service_provider_3_digit values ('040');
insert into service_provider_3_digit values ('043');
insert into service_provider_3_digit values ('045');
insert into service_provider_3_digit values ('046');
insert into service_provider_3_digit values ('050');
insert into service_provider_3_digit values ('053');
insert into service_provider_3_digit values ('055');
insert into service_provider_3_digit values ('058');
insert into service_provider_3_digit values ('070');
insert into service_provider_3_digit values ('071');
insert into service_provider_3_digit values ('072');
insert into service_provider_3_digit values ('073');
insert into service_provider_3_digit values ('074');
insert into service_provider_3_digit values ('075');
insert into service_provider_3_digit values ('076');
insert into service_provider_3_digit values ('077');
insert into service_provider_3_digit values ('078');
insert into service_provider_3_digit values ('079');
insert into service_provider_3_digit values ('088');

-- DML script for inserting data into service_provider_4_digit table

insert into service_provider_4_digit values ('0111');
insert into service_provider_4_digit values ('0113');
insert into service_provider_4_digit values ('0114');
insert into service_provider_4_digit values ('0115');
insert into service_provider_4_digit values ('0117');
insert into service_provider_4_digit values ('0118');
insert into service_provider_4_digit values ('0161');
insert into service_provider_4_digit values ('0162');
insert into service_provider_4_digit values ('0164');
insert into service_provider_4_digit values ('0165');
insert into service_provider_4_digit values ('0166');
insert into service_provider_4_digit values ('0167');
insert into service_provider_4_digit values ('0168');
insert into service_provider_4_digit values ('0172');
insert into service_provider_4_digit values ('0174');
insert into service_provider_4_digit values ('0180');
insert into service_provider_4_digit values ('0181');
insert into service_provider_4_digit values ('0182');
insert into service_provider_4_digit values ('0183');
insert into service_provider_4_digit values ('0184');
insert into service_provider_4_digit values ('0186');
insert into service_provider_4_digit values ('0187');
insert into service_provider_4_digit values ('0222');
insert into service_provider_4_digit values ('0223');
insert into service_provider_4_digit values ('0224');
insert into service_provider_4_digit values ('0226');
insert into service_provider_4_digit values ('0227');
insert into service_provider_4_digit values ('0228');
insert into service_provider_4_digit values ('0229');
insert into service_provider_4_digit values ('0251');
insert into service_provider_4_digit values ('0252');
insert into service_provider_4_digit values ('0255');
insert into service_provider_4_digit values ('0294');
insert into service_provider_4_digit values ('0297');
insert into service_provider_4_digit values ('0299');
insert into service_provider_4_digit values ('0313');
insert into service_provider_4_digit values ('0314');
insert into service_provider_4_digit values ('0315');
insert into service_provider_4_digit values ('0316');
insert into service_provider_4_digit values ('0317');
insert into service_provider_4_digit values ('0318');
insert into service_provider_4_digit values ('0320');
insert into service_provider_4_digit values ('0321');
insert into service_provider_4_digit values ('0341');
insert into service_provider_4_digit values ('0342');
insert into service_provider_4_digit values ('0343');
insert into service_provider_4_digit values ('0344');
insert into service_provider_4_digit values ('0345');
insert into service_provider_4_digit values ('0346');
insert into service_provider_4_digit values ('0347');
insert into service_provider_4_digit values ('0348');
insert into service_provider_4_digit values ('0411');
insert into service_provider_4_digit values ('0412');
insert into service_provider_4_digit values ('0413');
insert into service_provider_4_digit values ('0416');
insert into service_provider_4_digit values ('0418');
insert into service_provider_4_digit values ('0475');
insert into service_provider_4_digit values ('0478');
insert into service_provider_4_digit values ('0481');
insert into service_provider_4_digit values ('0485');
insert into service_provider_4_digit values ('0486');
insert into service_provider_4_digit values ('0487');
insert into service_provider_4_digit values ('0488');
insert into service_provider_4_digit values ('0492');
insert into service_provider_4_digit values ('0493');
insert into service_provider_4_digit values ('0495');
insert into service_provider_4_digit values ('0497');
insert into service_provider_4_digit values ('0499');
insert into service_provider_4_digit values ('0511');
insert into service_provider_4_digit values ('0512');
insert into service_provider_4_digit values ('0513');
insert into service_provider_4_digit values ('0514');
insert into service_provider_4_digit values ('0515');
insert into service_provider_4_digit values ('0516');
insert into service_provider_4_digit values ('0517');
insert into service_provider_4_digit values ('0518');
insert into service_provider_4_digit values ('0519');
insert into service_provider_4_digit values ('0521');
insert into service_provider_4_digit values ('0522');
insert into service_provider_4_digit values ('0523');
insert into service_provider_4_digit values ('0524');
insert into service_provider_4_digit values ('0525');
insert into service_provider_4_digit values ('0527');
insert into service_provider_4_digit values ('0528');
insert into service_provider_4_digit values ('0529');
insert into service_provider_4_digit values ('0541');
insert into service_provider_4_digit values ('0543');
insert into service_provider_4_digit values ('0544');
insert into service_provider_4_digit values ('0545');
insert into service_provider_4_digit values ('0546');
insert into service_provider_4_digit values ('0547');
insert into service_provider_4_digit values ('0548');
insert into service_provider_4_digit values ('0561');
insert into service_provider_4_digit values ('0562');
insert into service_provider_4_digit values ('0566');
insert into service_provider_4_digit values ('0570');
insert into service_provider_4_digit values ('0571');
insert into service_provider_4_digit values ('0572');
insert into service_provider_4_digit values ('0573');
insert into service_provider_4_digit values ('0575');
insert into service_provider_4_digit values ('0577');
insert into service_provider_4_digit values ('0578');
insert into service_provider_4_digit values ('0591');
insert into service_provider_4_digit values ('0592');
insert into service_provider_4_digit values ('0593');
insert into service_provider_4_digit values ('0594');
insert into service_provider_4_digit values ('0595');
insert into service_provider_4_digit values ('0596');
insert into service_provider_4_digit values ('0597');
insert into service_provider_4_digit values ('0598');
insert into service_provider_4_digit values ('0599');

commit;

spool off