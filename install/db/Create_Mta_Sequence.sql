-- $Id: Create_Mta_Sequence.sql,v 1.0 2014/08/13 Prakash Exp $ It creates Sequence for MTA_FQDN.

CREATE SEQUENCE mta_fqdn_seq
START WITH  310000000
INCREMENT BY   1
NOCACHE
NOCYCLE;

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Mta Sequence generated successfully ----');
commit;