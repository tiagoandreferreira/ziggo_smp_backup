-- $Id: CUST_PISYSINSTANCE.sql,v 1.0 2014/02/14  Prakash Exp $ This script is used to make entry of pisys instance into DB. 
-- $Id: CUST_PISYSINSTANCE.sql,v 1.1 2015/06/01  Prakash Exp $ This script is used to make entry of pisys instance into DB as a part of Derby Project Phase 1. 
  
  DROP TABLE CUST_PISYSINSTANCE;
  CREATE TABLE "CUST_PISYSINSTANCE" 
   (	"REGION" VARCHAR2(35 BYTE) NOT NULL ENABLE,
		"COLOUR" VARCHAR2(35 BYTE) NOT NULL ENABLE,
		"INSTANCE" VARCHAR2(35 BYTE) NOT NULL ENABLE
   );
   
   Insert into CUST_PISYSINSTANCE (REGION,COLOUR,INSTANCE) values ('NL','ORANGE','AtHome');
   Insert into CUST_PISYSINSTANCE (REGION,COLOUR,INSTANCE) values ('NL','BLUE','Casema');
   
   commit;