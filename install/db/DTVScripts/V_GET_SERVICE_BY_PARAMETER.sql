create or replace view V_GET_SERVICE_BY_PARAMETER
as
select
ss.sub_id,
ss.sub_svc_id,
ss.external_key,
ss.parent_sub_svc_id,
s.svc_nm,
p.parm_nm,
ssp.val
from
parm p,
sub_svc_parm ssp,
sub_svc ss,
svc s
where
s.svc_id = ss.svc_id
and ss.sub_svc_id = ssp.sub_svc_id
and ssp.parm_id = p.parm_id
and ss.sub_svc_status_id not in (
      fn_get_status_id(fn_cm_get_class_id('SubSvcSpec'),'delete_in_progress'),
      fn_get_status_id(fn_cm_get_class_id('SubSvcSpec'),'deleted'),
      fn_get_status_id(fn_cm_get_class_id('SubSvcSpec'),'deactivation_in_progress'),
      fn_get_status_id(fn_cm_get_class_id('SubSvcSpec'),'inactive'));

commit;

exit;