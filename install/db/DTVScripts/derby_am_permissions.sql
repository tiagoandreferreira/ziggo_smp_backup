spool derby_am_permissions.log

-- ============================================================================
--  Id: derby_am_permissions.sql,v 1.0 2015/06/12 14:45:00 Rahul Exp $
--								,v 1.0 2015/06/29 14:45:00 Prakash Exp $

exec am_add_secure_obj('smp.query.subbrief.QueryByDesiredCmMac', 'Query Object for desired Device Id search.');
exec am_add_secure_obj('smp.query.subbrief.QueryByDesiredCmMac.desired_device_id', 'Query Object for desired Device Id search.');

exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac.desired_device_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac.desired_device_id', 'y', 'csr_admin', 'view');

exec am_add_secure_obj('smp.query.subbrief.QueryByHorizonCmMac', 'Query Object for Horizon Device Id search.');
exec am_add_secure_obj('smp.query.subbrief.QueryByHorizonCmMac.device_id', 'Query Object for Horizon Device Id search.');

exec am_add_privilege('smp.query.subbrief.QueryByHorizonCmMac', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByHorizonCmMac', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.QueryByHorizonCmMac.device_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByHorizonCmMac.device_id', 'y', 'csr_admin', 'view');

exec am_add_secure_obj('smp.query.subbrief.SubSvcSearchByService', 'Query Object for subscriber service search.');
exec am_add_secure_obj('smp.query.subbrief.SubSvcSearchByService.service', 'Query Object for subscriber service search.');
exec am_add_secure_obj('smp.query.subbrief.SubSvcSearchByService.parameter', 'Query Object for subscriber service search.');
exec am_add_secure_obj('smp.query.subbrief.SubSvcSearchByService.value', 'Query Object for subscriber service search.');

exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService.service', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService.service', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService.parameter', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService.parameter', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService.value', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByService.value', 'y', 'csr_admin', 'view');

exec am_add_secure_obj('smp.query.subbrief.SubSearchByParameter', 'Query Object for subscriber search.');
exec am_add_secure_obj('smp.query.subbrief.SubSearchByParameter.service', 'Query Object for subscriber search.');
exec am_add_secure_obj('smp.query.subbrief.SubSearchByParameter.parameter', 'Query Object for subscriber search.');
exec am_add_secure_obj('smp.query.subbrief.SubSearchByParameter.value', 'Query Object for subscriber search.');

exec am_add_privilege('smp.query.subbrief.SubSearchByParameter', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSearchByParameter', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubSearchByParameter.service', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSearchByParameter.service', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubSearchByParameter.parameter', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSearchByParameter.parameter', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubSearchByParameter.value', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubSearchByParameter.value', 'y', 'csr_admin', 'view');

exec am_add_secure_obj('smp.query.subbrief.SubParentSvcSearchByService', 'Query Object for subscriber search.');
exec am_add_secure_obj('smp.query.subbrief.SubParentSvcSearchByService.service', 'Query Object for subscriber search.');
exec am_add_secure_obj('smp.query.subbrief.SubParentSvcSearchByService.parameter', 'Query Object for subscriber search.');
exec am_add_secure_obj('smp.query.subbrief.SubParentSvcSearchByService.value', 'Query Object for subscriber search.');

exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService.service', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService.service', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService.parameter', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService.parameter', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService.value', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubParentSvcSearchByService.value', 'y', 'csr_admin', 'view');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.hidden_sunrise_qos', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.hidden_sunrise_qos', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.hidden_sunrise_qos', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.hidden_sunrise_filter', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.hidden_sunrise_filter', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.hidden_sunrise_filter', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.hrz_activated', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.hrz_activated', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.hrz_activated', 'y', 'csr_admin', 'edit');

commit;

spool off

exit
