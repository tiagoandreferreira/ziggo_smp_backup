CREATE OR REPLACE FORCE VIEW "V_HORIZON_CM_MAC_SEARCH" ("SUB_ID", "SUB_ORDR_ID", "CREATED_BY", "ORDR_STATUS", "DEVICE_ID", "CREATED_DTM", "MODIFIED_DTM", "EXTERNAL_KEY") AS 
  SELECT
       distinct o.sub_id,o.sub_ordr_id, o.created_by,
       s.status,
       a.device_id,
       o.created_dtm, o.modified_dtm,o.external_key
  FROM sub_ordr o, ref_status s,
       (SELECT distinct SB.SUB_ID, SSP.VAL device_id FROM PARM P, SUB_SVC SS, SUB_SVC_PARM SSP, SUB SB, SVC S
WHERE SB.SUB_ID = SS.SUB_ID
AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
AND P.PARM_ID = SSP.PARM_ID
AND S.SVC_ID = SS.SVC_ID
AND P.PARM_NM = 'device_id'
AND S.SVC_NM = 'hrz_stb_device_control') a
where o.sub_id = a.sub_id and o.ORDR_STATUS_ID=s.status_id;

commit;

exit;