#!/usr/bin/ksh
#--============================================================================
#--    $Id: DTVSupports.sh,v 1.3 2014/03/21 07:49:28 Sarmistha Exp $
#--  DESCRIPTION
#--
#--  RELATED SCRIPTS
#--
#--  REVISION HISTORY
#--  * Based on CVS log
#--============================================================================

CUR_DIR=`pwd`

echo ${CUR_DIR}

LOG_DATE=`date '+%Y%m%d.%H%M%S'`
LOG_FILE=${CUR_DIR}/DTVSupports.$LOG_DATE.log

. ../../util/setEnv.sh

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    exit 1
fi

LOGIN=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}

#Creating Table

echo Executing CUST_PISYSINSTANCE.sql
cat  DTVScripts/CUST_PISYSINSTANCE.sql | sqlplus $LOGIN >> $LOG_FILE

echo Executing CUST_SIGMA_MAPPING_TABLE.sql
cat  DTVScripts/CUST_SIGMA_MAPPING_TABLE.sql | sqlplus $LOGIN >> $LOG_FILE


#-------------------------------------------------------------------------
# Verify the result
#-------------------------------------------------------------------------

if egrep -s '^ERROR' $LOG_FILE || egrep -s '^Error' $LOG_FILE
then
    echo Errors found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s  ORA_* $LOG_FILE
then
    echo Oracle errors found. See \'$LOG_FILE\' for details
    exit 1
else
echo  Script execution completed successfully. | tee -a $LOG_FILE
	  echo See \'$LOG_FILE\' for details
      exit 0
fi
