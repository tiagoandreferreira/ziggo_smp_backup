	--============================================================================
-- $Id: InService_Broadsoft,v 1.1 2013/11/12 Prakash Exp $ Query added to make broadsoft in service.
--  REVISION HISTORY

--============================================================================
		spool InService_Broadsoft.log
		set escape on
		set serveroutput on

update subntwk_parm set val='y' where parm_id = (select parm_id from parm where parm_nm ='in_service') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%');	
		
update subntwk_parm set val='n' where parm_id = (select parm_id from parm where parm_nm ='in_service') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like '%TELEFONIE.ZIGGO.LOCAL');

EXECUTE DBMS_OUTPUT.PUT_LINE('---- InService Broadsoft loaded successfuly ----');
commit;

spool off
exit;