	--============================================================================
-- $Id: InService_Hiq,v 1.1 2013/11/12 Prakash Exp $ Query added to make Hiq in service.
--  REVISION HISTORY

--============================================================================
		spool InService_Hiq.log
		set escape on
		set serveroutput on

update subntwk_parm set val='n' where parm_id = (select parm_id from parm where parm_nm ='in_service') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%');	
		
update subntwk_parm set val='y' where parm_id = (select parm_id from parm where parm_nm ='in_service') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like '%TELEFONIE.ZIGGO.LOCAL');

EXECUTE DBMS_OUTPUT.PUT_LINE('---- InService Hiq loaded successfuly ----');
commit;

spool off
exit;