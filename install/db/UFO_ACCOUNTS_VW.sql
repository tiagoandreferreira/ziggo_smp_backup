select temp.sub_id, temp.pnummer,temp1.cdm_mac,temp3.mta_mac, temp4.lijn, temp5.telefoonnummer , temp6.mta_fqdn, temp7.switch_fqdn,temp8.modem,temp9.qos_flavor, temp10.pakket_nu, temp11.pakket_wens, temp12.status
from  

(
select distinct sp.sub_id as sub_id ,sp.val as pnummer from sub_parm sp
        where sp.parm_id in (select parm_id from parm where parm_nm like 'p_number') 
       order by sub_id desc     
) temp, 

(
	select distinct ss.sub_id as sub_id , ssp.val as cdm_mac,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'device_id') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'emta_device_control')
       and ss.sub_svc_status_id not in (29)
       
) temp1,

(
	select distinct ss.sub_id as sub_id ,ssp.val as mta_mac,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'device_id2') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'emta_device_control')
      and ss.sub_svc_status_id not in (29)
     
) temp3,

(
	select distinct ss.sub_id as sub_id ,ssp.val as lijn,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'port_number') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'smp_switch_dial_tone_access')
      and ss.sub_svc_status_id not in (29)
       
) temp4,

(
	select distinct ss.sub_id as sub_id , ssp.val as telefoonnummer,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'telephone_number') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'smp_switch_dial_tone_access')
       and ss.sub_svc_status_id not in (29)
             
) temp5,

(
	select distinct ss.sub_id as sub_id , ssp.val as mta_fqdn,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'mta_fqdn') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'emta_device_control')
       and ss.sub_svc_status_id not in (29)
             
) temp6,

(
	select distinct ss.sub_id as sub_id , ssp.val as switch_fqdn,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'switch_id') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'smp_switch_dial_tone_access')
       and ss.sub_svc_status_id not in (29)
             
) temp7,

(
	select distinct ss.sub_id as sub_id ,ssp.val as modem ,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'model') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'emta_device_control')
        and ss.sub_svc_status_id not in (29)
) temp8,

(
	select distinct ss.sub_id as sub_id ,ssp.val as qos_flavor ,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'qos_flavor') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'internet_access')
        and ss.sub_svc_status_id not in (29)
) temp9,

(
	select distinct ss.sub_id as sub_id ,ssp.val as pakket_nu ,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'desired_wifi_access') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'wifi_access')
        and ss.sub_svc_status_id not in (29)
) temp10,

(
	select distinct ss.sub_id as sub_id ,ssp.val as pakket_wens ,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'desired_wifi_hotspot') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'wifi_access')
        and ss.sub_svc_status_id not in (29)
) temp11,

(
	select distinct ss.sub_id as sub_id ,ssp.val as status ,ss.sub_svc_id from sub_svc_parm ssp,  sub_svc ss ,svc s
        where ssp.parm_id in (select parm_id from parm where parm_nm like 'internet_status') 
        and ssp.sub_svc_id = ss.sub_svc_id
	and ss.svc_id = (select s.svc_id from svc s where s.svc_nm = 'internet_access')
        and ss.sub_svc_status_id not in (29)
) temp12

where   

temp1.sub_svc_id= temp3.sub_svc_id and
temp1.sub_svc_id= temp6.sub_svc_id and
temp4.sub_svc_id= temp5.sub_svc_id and
temp4.sub_svc_id= temp7.sub_svc_id and
temp1.sub_svc_id= temp8.sub_svc_id and
temp9.sub_svc_id= temp12.sub_svc_id and
temp10.sub_svc_id= temp11.sub_svc_id and 
temp.sub_id = temp1.sub_id
and temp.sub_id = temp3.sub_id
and temp.sub_id = temp4.sub_id
and temp.sub_id = temp5.sub_id
and temp.sub_id = temp6.sub_id
and temp.sub_id = temp7.sub_id
and temp.sub_id = temp8.sub_id
and temp.sub_id = temp9.sub_id
and temp.sub_id = temp10.sub_id
and temp.sub_id = temp11.sub_id 
and temp.sub_id = temp12.sub_id

order by temp.sub_id desc