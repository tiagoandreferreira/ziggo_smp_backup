CREATE UNIQUE INDEX ADM_ANSWER_PK ON ADM_ANSWER
(
       answer_id                      ASC
);

CREATE INDEX ADM_ANSWER_IX1 ON ADM_ANSWER
(
       Fact_ID                        ASC
);

CREATE INDEX ADM_ANSWER_IX2 ON ADM_ANSWER
(
       answer_typ                     ASC
);

CREATE UNIQUE INDEX ADM_ANSWER_TYP_PK ON ADM_ANSWER_TYP
(
       answer_typ                     ASC
);

CREATE UNIQUE INDEX ADM_ANSWER_TYP_UK ON ADM_ANSWER_TYP
(
       pkg_id                         ASC,
       question_id                    ASC,
       answer_tag                     ASC
);

CREATE INDEX ADM_ANSWER_TYP_IX2 ON ADM_ANSWER_TYP
(
       question_id                    ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_FACT_LOG_PK ON ADM_FACT_LOG
(
       Fact_ID                        ASC
);

CREATE INDEX ADM_FACT_LOG_IX1 ON ADM_FACT_LOG
(
       Session_ID                     ASC
);

CREATE UNIQUE INDEX ADM_FACT_TYPE_PK ON ADM_FACT_TYPE
(
       fact_typ                       ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_FACT_TYPE_UK1 ON ADM_FACT_TYPE
(
       pkg_id                         ASC,
       Fact_Typ_NM                    ASC
);

CREATE UNIQUE INDEX ADM_FAULT_TREE_PK ON ADM_FAULT_TREE
(
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_FAULT_TREE_UK1 ON ADM_FAULT_TREE
(
       pkg_nm                         ASC
);

CREATE UNIQUE INDEX ADM_FAULT_TREE_NODE_PK ON ADM_FAULT_TREE_NODE
(
       Node_ID                        ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_FAULT_TREE_NODE_UK1 ON ADM_FAULT_TREE_NODE
(
       pkg_id                         ASC,
       node_nm                        ASC
);

CREATE UNIQUE INDEX ADM_NODE_TRASITION_PK ON ADM_NODE_TRANSITION
(
       Transition_ID                  ASC
);

CREATE UNIQUE INDEX ADM_NODE_TRASITION_UK1 ON ADM_NODE_TRANSITION
(
       Node_to_ID                     ASC,
       Node_from_ID                   ASC
);

CREATE INDEX ADM_NODE_TRASITION_IX2 ON ADM_NODE_TRANSITION
(
       Node_from_ID                   ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_QUESTION_PK ON ADM_QUESTION
(
       question_id                    ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_QUESTION_UK ON ADM_QUESTION
(
       pkg_id                         ASC,
       question_tag                   ASC
);

CREATE UNIQUE INDEX ADM_SESSION_PK ON ADM_SESSION
(
       Session_ID                     ASC
);

CREATE INDEX ADM_SESSION_IX2 ON ADM_SESSION
(
       sub_id                         ASC
);

CREATE UNIQUE INDEX ADM_SESSION_EVENT_PK ON ADM_SESSION_EVENT
(
       Session_ID                     ASC,
       seq                            ASC
);

CREATE INDEX ADM_SESSION_EVENT_IX2 ON ADM_SESSION_EVENT
(
       node_ID                        ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_SESSION_TRANSITION_PK ON ADM_SESSION_TRANSITION
(
       Session_ID                     ASC,
       seq                            ASC
);

CREATE INDEX ADM_SESSION_TRANSITION_IX2 ON ADM_SESSION_TRANSITION
(
       Transition_ID                  ASC
);

CREATE INDEX ADM_SESSION_TRANSITION_IX3 ON ADM_SESSION_TRANSITION
(
       jump_to_node_id                ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_SYMPTOM_PK ON ADM_SYMPTOM
(
       symptom_id                     ASC
);

CREATE INDEX ADM_SYMPTOM_IX1 ON ADM_SYMPTOM
(
       pkg_id                         ASC,
       symptom_typ                    ASC
);

CREATE INDEX ADM_SYMPTOM_IX2 ON ADM_SYMPTOM
(
       Fact_ID                        ASC
);

CREATE INDEX ADM_SYMPTOM_IX3 ON ADM_SYMPTOM
(
       solution_id                    ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_SYMPTOM_TYP_PK ON ADM_SYMPTOM_TYP
(
       pkg_id                         ASC,
       symptom_typ                    ASC
);

CREATE INDEX ADM_SYMPTOM_TYP_IX2 ON ADM_SYMPTOM_TYP
(
       start_node_id                  ASC,
       pkg_id                         ASC
);

CREATE UNIQUE INDEX ADM_VAL_LIST_PK ON ADM_VAL_LIST
(
       val_list_id                    ASC,
       Seq                            ASC
);

CREATE INDEX ADM_VAL_LIST_IX1 ON ADM_VAL_LIST
(
       Fact_ID                        ASC
);


CREATE UNIQUE INDEX ADM_FACT_DEF_LIBRARY_PK ON ADM_FACT_DEF_LIBRARY
(
       FACT_DEF_ID                    ASC
);

CREATE INDEX ADM_FACT_DEF_LIBRARY_FK3 ON ADM_FACT_DEF_LIBRARY
(
       NODE_ID                        ASC,
       PKG_ID                         ASC
);

CREATE UNIQUE INDEX XPKADM_FAULT_TREE_SCRIPT ON ADM_FAULT_TREE_SCRIPT
(
       SCRIPT_ID                      ASC
);

CREATE INDEX ADM_FAULT_TREE_SC_FK1 ON ADM_FAULT_TREE_SCRIPT
(
       PKG_ID                         ASC
);

CREATE UNIQUE INDEX ADM_LBL_ANSWER_PK ON ADM_LBL_ANSWER
(
       PARM_NM                        ASC,
       ANSWER_TYP                     ASC,
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_ANSWER_FK1 ON ADM_LBL_ANSWER
(
       ANSWER_TYP                     ASC
);

CREATE INDEX ADM_LBL_ANSWER_FK2 ON ADM_LBL_ANSWER
(
       LOCALE_ID                      ASC
);

CREATE UNIQUE INDEX ADM_LBL_FACT_PARM_PK ON ADM_LBL_FACT_PARM
(
       FACT_DEF_ID                    ASC,
       PARM_NM                        ASC,
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_FACT_PARM_FK1 ON ADM_LBL_FACT_PARM
(
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_FACT_PARM_FK2 ON ADM_LBL_FACT_PARM
(
       FACT_DEF_ID                    ASC
);

CREATE UNIQUE INDEX ADM_LBL_NODE_PK ON ADM_LBL_NODE
(
       NODE_ID                        ASC,
       PKG_ID                         ASC,
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_NODE_FK1 ON ADM_LBL_NODE
(
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_NODE_FK2 ON ADM_LBL_NODE
(
       PKG_ID                         ASC,
       NODE_ID                        ASC
);

CREATE UNIQUE INDEX ADM_LBL_QUESTION_PK ON ADM_LBL_QUESTION
(
       PKG_ID                         ASC,
       QUESTION_ID                    ASC,
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_QUESTION_FK1 ON ADM_LBL_QUESTION
(
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_QUESTION_FK2 ON ADM_LBL_QUESTION
(
       PKG_ID                         ASC,
       QUESTION_ID                    ASC
);

CREATE UNIQUE INDEX ADM_LBL_SYMPTOM_PK ON ADM_LBL_SYMPTOM
(
       PKG_ID                         ASC,
       SYMPTOM_TYP                    ASC,
       LOCALE_ID                      ASC
);

CREATE INDEX ADM_LBL_SYMPTOM_FK1 ON ADM_LBL_SYMPTOM
(
       PKG_ID                         ASC,
       SYMPTOM_TYP                    ASC
);

CREATE INDEX ADM_LBL_SYMPTOM_FK2 ON ADM_LBL_SYMPTOM
(
       LOCALE_ID                      ASC
);

CREATE UNIQUE INDEX ADM_LOCALE_PK ON ADM_LOCALE
(
       LOCALE_ID                      ASC
);

CREATE UNIQUE INDEX ADM_LOCALE_UK1 ON ADM_LOCALE
(
       PKG_ID                         ASC,
       LANGUAGE                       ASC,
       COUNTRY                        ASC
);

CREATE INDEX ADM_LOCALE_FK1 ON ADM_LOCALE
(
       PKG_ID                         ASC
);


CREATE INDEX ADM_FACT_LOG_IDX_003 ON ADM_FACT_LOG
(
       CAPTURE_NODE_ID                ASC,
       FACT_TYP                       ASC
);


CREATE INDEX ADM_FACT_TYPE_IDX_002 ON ADM_FACT_TYPE
(
       FACT_TYP_NM                    ASC,
       FACT_TYP                       ASC
);


CREATE INDEX ADM_FAULT_TREE_UK1 ON ADM_FAULT_TREE
(
       PKG_NM                         ASC,
       PKG_ID                         ASC
);
	 

CREATE INDEX ADM_FAULT_TREE_SCRIP_IDX_001 ON ADM_FAULT_TREE_SCRIPT
(
      PKG_ID                          ASC,
      SCRIPT_ID                       ASC
);


CREATE INDEX ADM_NODE_TRASITION_IX2 ON ADM_NODE_TRANSITION
(
      NODE_FROM_ID                    ASC,
      PKG_ID                          ASC,
      TRANSITION_ID                   ASC
);


CREATE INDEX ADM_SESSION_TRANSITI_IDX_004 ON ADM_SESSION_TRANSITION
(
      PKG_ID                          ASC,
      CREATED_DTM                     ASC
);


