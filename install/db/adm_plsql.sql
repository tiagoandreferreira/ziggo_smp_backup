---------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ADM_ADD_SESSION_EVENT(
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
--
-- Revision History
-- * Based on CVS log
--------------------------------------------------------------------------
   i_session_id                 IN       ADM_SESSION.SESSION_ID%TYPE,
   i_current_node_id            IN       ADM_SESSION_EVENT.NODE_ID%TYPE,
   i_event_type                 IN       ADM_SESSION_EVENT.EVENT_TYPE%TYPE,
   i_pkg_id                     IN       ADM_FAULT_TREE.PKG_ID%TYPE,
   i_owner_id                   IN       VARCHAR2,
   o_session_event_seq_no       OUT      ADM_SESSION_EVENT.SEQ%TYPE
)
AS
BEGIN
     INSERT INTO ADM_SESSION_EVENT
       (SESSION_ID, SEQ, EVENT_TYPE, NODE_ID,
                 PKG_ID, CREATED_DTM, CREATED_BY)
       VALUES (i_session_id,
               (SELECT NVL(MAX(SEQ) + 1, 0)
                            FROM ADM_SESSION_EVENT WHERE session_id = i_session_id),
               i_event_type, i_current_node_id, i_pkg_id, SYSDATE, i_owner_id)
       RETURNING SEQ INTO o_session_event_seq_no;

END;
/


CREATE OR REPLACE PROCEDURE ADM_ADD_SESSION_TRANSITION (
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistence
--
-- Revision History
-- * Based on CVS log
-- * 2003/05/22 SameetB added occurred_dtm parameter, removed pkg_id, using node names
--------------------------------------------------------------------------
   i_session_id                     IN       ADM_SESSION.SESSION_ID%TYPE,
   i_from_node_nm                   IN       ADM_FAULT_TREE_NODE.NODE_NM%TYPE,
   i_to_node_nm                     IN       ADM_FAULT_TREE_NODE.NODE_NM%TYPE,
   i_occurred_dtm                   IN       ADM_SESSION_TRANSITION.CREATED_DTM%TYPE,
   i_created_by                     IN       VARCHAR2,
   o_seq_no                         IN OUT   ADM_SESSION_TRANSITION.SEQ%TYPE
)
AS
   v_seq_no                         ADM_SESSION_TRANSITION.SEQ%TYPE;
   i_pkg_id                         ADM_FAULT_TREE.PKG_ID%TYPE;
   i_from_node_id                   ADM_FAULT_TREE_NODE.NODE_ID%TYPE;
   i_to_node_id                     ADM_FAULT_TREE_NODE.NODE_ID%TYPE;
   i_transition_id                  ADM_NODE_TRANSITION.TRANSITION_ID%TYPE;
BEGIN

    SELECT PKG_ID INTO i_pkg_id
            FROM ADM_SESSION WHERE session_id = i_session_id;
    begin
      SELECT t.TRANSITION_ID into i_transition_id
        FROM ADM_NODE_TRANSITION t, ADM_FAULT_TREE_NODE fn, ADM_FAULT_TREE_NODE tn
            WHERE t.NODE_FROM_ID = fn.node_id and fn.PKG_ID = i_pkg_id AND fn.NODE_NM = i_from_node_nm
            AND t.NODE_TO_ID = tn.node_id and tn.PKG_ID = i_pkg_id AND tn.NODE_NM = i_to_node_nm
            AND t.PKG_ID = i_pkg_id;
    exception
     when no_data_found then
        raise_application_error(-20008, 'Transition from "'||i_from_node_nm||'" to "'||i_to_node_nm||
            '" not found in the package "'||i_pkg_id||'"');
    end ;

    INSERT INTO ADM_SESSION_TRANSITION (SESSION_ID, SEQ, TRANSITION_ID,
           PKG_ID, CREATED_DTM, CREATED_BY)
           VALUES(i_session_id,
                  (SELECT NVL(MAX(SEQ)+1,0) FROM ADM_SESSION_TRANSITION
                    WHERE SESSION_ID = i_session_id),
                     i_transition_id     ,
                  i_pkg_id, i_occurred_dtm, i_created_by)
           RETURNING SEQ INTO o_seq_no;
END;
/

CREATE OR REPLACE PROCEDURE ADM_SESSION_JUMP_TRANSITION (
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistence
--
-- Revision History
-- * Based on CVS log
-- * 2003/05/22 SameetB added occurred_dtm parameter, removed pkg_id, using node names
--------------------------------------------------------------------------
   i_session_id                     IN       ADM_SESSION.SESSION_ID%TYPE,
   i_to_node_nm                     IN       ADM_FAULT_TREE_NODE.NODE_NM%TYPE,
   i_occurred_dtm                   IN       ADM_SESSION_TRANSITION.CREATED_DTM%TYPE,
   i_created_by                     IN       VARCHAR2,
   o_seq_no                         IN OUT   ADM_SESSION_TRANSITION.SEQ%TYPE
)
AS
   v_seq_no                         ADM_SESSION_TRANSITION.SEQ%TYPE;
   i_pkg_id                         ADM_FAULT_TREE.PKG_ID%TYPE;
   i_to_node_id                     ADM_FAULT_TREE_NODE.NODE_ID%TYPE;
BEGIN

    SELECT PKG_ID INTO i_pkg_id
            FROM ADM_SESSION WHERE session_id = i_session_id;
    begin
      SELECT NODE_ID into i_to_node_id
        FROM ADM_FAULT_TREE_NODE
            WHERE PKG_ID = i_pkg_id AND NODE_NM = i_to_node_nm;
    exception
     when no_data_found then
        raise_application_error(-20008, 'Node "'||i_to_node_nm||
            '" not found in the package "'||i_pkg_id||'"');
    end ;

    INSERT INTO ADM_SESSION_TRANSITION (SESSION_ID, SEQ, JUMP_TO_NODE_ID,
           PKG_ID, CREATED_DTM, CREATED_BY)
           VALUES(i_session_id,
                  (SELECT NVL(MAX(SEQ)+1,0) FROM ADM_SESSION_TRANSITION
                    WHERE SESSION_ID = i_session_id),
                     i_to_node_id     ,
                  i_pkg_id, i_occurred_dtm, i_created_by)
           RETURNING SEQ INTO o_seq_no;
END;
/


CREATE OR REPLACE PROCEDURE Adm_Create_Session (
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistamce
--
-- Revision History
-- * Based on CVS log
--------------------------------------------------------------------------
   i_session_id                 IN      ADM_SESSION.SESSION_ID%TYPE,
   i_sub_id                     IN       NUMBER,
   i_pkg_nm                     IN       ADM_FAULT_TREE.PKG_NM%TYPE,
   i_owner_id                   IN       VARCHAR2,
   i_current_node_nm            IN       ADM_FAULT_TREE_NODE.NODE_NM%TYPE
)
AS
   v_pkg_id                     ADM_FAULT_TREE.PKG_ID%TYPE;
   v_session_event_seq_no       ADM_SESSION_EVENT.SEQ%TYPE;
   i_current_node_id            ADM_SESSION_EVENT.NODE_ID%TYPE;
BEGIN

     begin
       SELECT PKG_ID INTO v_pkg_id
         FROM ADM_FAULT_TREE WHERE PKG_NM = i_pkg_nm;
     exception
       when no_data_found then
         raise_application_error(-20008, 'Package "'||i_pkg_nm||'" not found');
     end;
     begin
       SELECT NODE_ID INTO i_current_node_id FROM ADM_FAULT_TREE_NODE
                    WHERE PKG_ID = v_pkg_id AND NODE_NM = i_current_node_nm;
     exception
       when no_data_found then
         raise_application_error(-20008, 'Node "'||i_current_node_nm||'" not found in package "'||i_pkg_nm||'"');
     end;

     INSERT INTO ADM_SESSION
       (SESSION_ID, SUB_ID, CURRENT_NODE_ID,
                 OWNED_BY, PKG_ID, STATUS)
       VALUES (i_session_id, i_sub_id, i_current_node_id,
                          i_owner_id, v_pkg_id, 'r');

     ADM_ADD_SESSION_EVENT(i_session_id, i_current_node_id, 'n', v_pkg_id,
                    i_owner_id, v_session_event_seq_no);

END;
/

CREATE OR REPLACE PROCEDURE Adm_Suspend_Session (
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistence
--
-- Revision History
-- * Based on CVS log
-- * 2003-05-22 SameetB removed unnecessary arguments
--------------------------------------------------------------------------
   i_session_id                 IN       ADM_SESSION.SESSION_ID%TYPE,
   i_current_node_nm            IN       ADM_FAULT_TREE_NODE.NODE_NM%TYPE,
   i_owner_nm                   IN       VARCHAR2,
   i_suspend_notes              IN       ADM_SESSION_EVENT.NOTES%TYPE DEFAULT NULL
)
AS
   v_pkg_id                     ADM_FAULT_TREE.PKG_ID%TYPE;
   v_node_id                    ADM_FAULT_TREE_NODE.NODE_ID%TYPE;
BEGIN

     IF i_session_id IS NULL THEN
        RAISE NO_DATA_FOUND;
     END IF;

     SELECT PKG_ID INTO v_pkg_id
            FROM ADM_SESSION WHERE session_id = i_session_id;

     begin
       SELECT NODE_ID INTO v_node_id FROM ADM_FAULT_TREE_NODE
            WHERE PKG_ID = v_pkg_id AND NODE_NM = i_current_node_nm;
     exception
       when no_data_found then
         raise_application_error(-20008, 'Node "'||i_current_node_nm||'" not found in package "'||v_pkg_id||'"');
     end;

     UPDATE ADM_SESSION SET CURRENT_NODE_ID = v_node_id,
                STATUS= 's' WHERE session_id = i_session_id;

     INSERT INTO ADM_SESSION_EVENT
       (SESSION_ID, SEQ, EVENT_TYPE, NODE_ID,
          PKG_ID, CREATED_DTM, CREATED_BY, NOTES)
       SELECT i_session_id, nvl(max(SEQ)+1, 0), 's',
          v_node_id, v_pkg_id, SYSDATE, i_owner_nm, i_suspend_notes
          FROM ADM_SESSION_EVENT
          WHERE PKG_ID = v_pkg_id AND SESSION_ID = i_session_id;

END;
/

CREATE OR REPLACE PROCEDURE Adm_Resume_Session (
---------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistence
--
-- Revision History
-- * Based on CVS log
--------------------------------------------------------------------------

   i_session_id                 IN       ADM_SESSION.SESSION_ID%TYPE,
   i_owner_id                   IN       VARCHAR2
)
AS
   v_pkg_id                     ADM_FAULT_TREE.PKG_ID%TYPE;
   v_session_event_seq_no       ADM_SESSION_EVENT.SEQ%TYPE;
   v_session_status             ADM_SESSION.STATUS%TYPE;
   v_current_node_id            ADM_SESSION_EVENT.NODE_ID%TYPE;
BEGIN

     IF i_session_id IS NULL THEN
        RAISE_application_error(-20008, 'Invalid Session Id');
     END IF;

     SELECT STATUS, CURRENT_NODE_ID, PKG_ID INTO v_session_status, v_current_node_id, v_pkg_id
            FROM ADM_SESSION WHERE SESSION_ID = i_session_id FOR UPDATE;
            --lock the row until the end of this transaction so that
            --concurrent accesses dont occur

     IF v_session_status = 's' THEN
         UPDATE ADM_SESSION SET STATUS = 'r' WHERE SESSION_ID = i_session_id;
         ADM_ADD_SESSION_EVENT(i_session_id, v_current_node_id, 'r',
                                        v_pkg_id, i_owner_id, v_session_event_seq_no);
     ELSE
         RAISE_application_error(-20008, 'Session is currently not suspended');
     END IF;

END;
/

CREATE OR REPLACE PROCEDURE ADM_ADD_ANSWER  (
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistamce
--
-- Revision History
-- * Based on CVS log
--------------------------------------------------------------------------
   o_answer_id                  OUT      ADM_ANSWER.ANSWER_ID%TYPE,
   i_answer_tag                 IN       VARCHAR2,
   i_answer_text                IN       ADM_ANSWER.TEXT%TYPE,
   i_list_id                    IN       ADM_VAL_LIST.FACT_ID%TYPE,
   i_list_seq_no                IN       ADM_VAL_LIST.SEQ%TYPE
)
AS
     v_val_list_id              ADM_VAL_LIST.VAL_LIST_ID%TYPE;

BEGIN
 /*
     INSERT INTO ADM_ANSWER (ANSWER_ID, ANSWER_TAG, TEXT)
            VALUES(ADM_ANSWER_SEQ.NEXTVAL, i_answer_tag, i_answer_text)
            RETURNING ANSWER_ID INTO o_answer_id;

     IF i_list_id IS NOT NULL THEN
        INSERT INTO ADM_VAL_LIST(VAL_LIST_ID, FACT_ID, SEQ, VAL)
            VALUES(ADM_VAL_LIST_SEQ.NEXTVAL, i_list_id, i_list_seq_no, o_answer_id);
     END IF;
   */
   raise_application_error(-20010, 'Not implemented');
END;
/


CREATE OR REPLACE PROCEDURE Adm_Close_Session (
--------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_plsql.sql,v 1.3 2005/07/04 17:25:11 vasyly Exp $
--
-- DESCRIPTION
--
-- ADM session persistamce
--
-- Revision History
-- * Based on CVS log
--------------------------------------------------------------------------
   i_session_id                 IN      ADM_SESSION.SESSION_ID%TYPE,
   i_owner_nm                   IN      VARCHAR2,
   i_close_node_nm              IN      ADM_FAULT_TREE_NODE.NODE_NM%TYPE,
   i_close_notes                IN      ADM_SESSION_EVENT.NOTES%TYPE DEFAULT NULL
)
AS
   v_pkg_id                     ADM_FAULT_TREE.PKG_ID%TYPE;
   v_close_node_id              ADM_SESSION_EVENT.NODE_ID%TYPE;
BEGIN

     IF i_session_id IS NULL THEN
        RAISE NO_DATA_FOUND;
     END IF;

     UPDATE ADM_SESSION SET STATUS = 'c' WHERE SESSION_ID = i_session_id;


     SELECT PKG_ID INTO v_pkg_id FROM ADM_SESSION
            WHERE SESSION_ID = i_session_id;

     begin
       SELECT NODE_ID INTO v_close_node_id FROM ADM_FAULT_TREE_NODE
                WHERE PKG_ID = v_pkg_id AND NODE_NM = i_close_node_nm;
     exception
       when no_data_found then
         raise_application_error(-20008, 'Node "'||i_close_node_nm||'" not found in package "'||v_pkg_id||'"');
     end;

     INSERT INTO ADM_SESSION_EVENT
       (SESSION_ID, SEQ, EVENT_TYPE, NODE_ID,
          PKG_ID, CREATED_DTM, CREATED_BY, NOTES)
       SELECT i_session_id, nvl(max(SEQ)+1, 0), 'c',
          v_close_node_id, v_pkg_id, SYSDATE, i_owner_nm, i_close_notes
          FROM ADM_SESSION_EVENT
          WHERE PKG_ID = v_pkg_id AND SESSION_ID = i_session_id;

END;
/

CREATE OR REPLACE PROCEDURE ADM_TEST_GET_SESS_DETAIL
   (
baseDate     IN VARCHAR2,
deltaIncre   IN NUMBER,
nrOfSessions OUT NUMBER) AS
BEGIN

select count(distinct(session_id)) into nrOfSessions from adm_session_event S1
where
    (
        created_dtm > to_date(baseDate,'dd-mon-yyyy hh24:mi:ss')
        and
        created_dtm < to_date(baseDate,'dd-mon-yyyy hh24:mi:ss') + deltaIncre/1440
    )
    OR
    (
       (S1.event_type='n' or S1.event_type='r') and (S1.created_dtm < to_date(baseDate,'dd-mon-yyyy hh24:mi:ss') )
       and
       exists
        (
            select S2.session_id from adm_session_event S2 where
            (S2.event_type='s' or S2.event_type='c') and (S2.created_dtm > to_date(baseDate,'dd-mon-yyyy hh24:mi:ss') + deltaIncre/1440)
            and S1.session_id = S2.session_id
        )
    );

END ADM_TEST_GET_SESS_DETAIL
;
/

----------------------------------------------------------------
-- This function finds the second largest time a during which a 
-- perticular node was executed.  
-- This function should be executed from the CM Data Base 
----------------------------------------------------------------

create or replace function max2no(nodeid in number, tsrnm in varchar2) return integer as
max2 integer;
begin
select MAXT into max2 from
(
select rownum RN, MAXT from
(
select MAXT from(
select abs(CREATED_DTM-
 greatest(CREATED_DTM,
 (select CREATED_DTM from ADM_SESSION_TRANSITION where SESSION_ID = a.SESSION_ID and
  SEQ = (a.SEQ+1))))*24*60*60 MAXT
from ADM_SESSION_TRANSITION a
where
(select substr(a.CREATED_BY, instr(a.CREATED_BY,'.')+1) from dual) = tsrnm
and TRANSITION_ID in (
select TRANSITION_ID
from ADM_NODE_TRANSITION
where NODE_FROM_ID in
(select NODE_ID
  from
  ADM_FAULT_TREE_NODE
  where NODE_ID=nodeid)) order by MAXT desc
)
) where MAXT is not null
) where RN = 2;
return max2;
end;
/



----------------------------------------------------------------
-- This function finds the second smallest time a during which a 
-- perticular node was executed.  
-- This function should be executed from the CM Data Base 
----------------------------------------------------------------

create or replace function min2no(nodeid in number, tsrnm in varchar2) return integer as
min2 integer;
begin
select MAXT into min2 from
(
select rownum RN, MAXT from
(
select MAXT from(
select abs(CREATED_DTM-
 greatest(CREATED_DTM,
 (select CREATED_DTM from ADM_SESSION_TRANSITION where SESSION_ID = a.SESSION_ID and
  SEQ = (a.SEQ+1))))*24*60*60 MAXT
from ADM_SESSION_TRANSITION a
where
(select substr(a.CREATED_BY, instr(a.CREATED_BY,'.')+1) from dual) = tsrnm
and TRANSITION_ID in (
select TRANSITION_ID
from ADM_NODE_TRANSITION
where NODE_FROM_ID in
(select NODE_ID
  from
  ADM_FAULT_TREE_NODE
  where NODE_ID=nodeid)) order by MAXT
)
) where MAXT is not null
) where RN = 2;
return min2;
end;
/




----------------------------------------------------------------
-- This function finds the 90% maximum Quantile 
-- This function should be executed from the CM Data Base
----------------------------------------------------------------

create or replace function max90quantile(nodeid in integer) return number
as
cnt number(4):=0;
tcnt number(4):=0;
hnos1 number(6,2);
hnos2 number(6,2);
max90quant number(6,2);
cursor c1 is select * from (select round(abs(CREATED_DTM- 
 greatest(CREATED_DTM,
 (select CREATED_DTM from ADM_SESSION_TRANSITION where SESSION_ID = a.SESSION_ID and SEQ = (a.SEQ+1))))*24*60*60) TIME_T 
from ADM_SESSION_TRANSITION a
where TRANSITION_ID in (
select TRANSITION_ID   
from ADM_NODE_TRANSITION
where NODE_FROM_ID in 
(select NODE_ID
from
ADM_FAULT_TREE_NODE
where NODE_ID=nodeid)) order by TIME_T)
where TIME_T is not null;
c2 c1%ROWTYPE;
begin
 open c1;
 loop
   fetch c1 into c2;
   exit when c1%NOTFOUND;
   cnt := cnt + 1;  
 end loop; 
 close c1;
 hnos1 := (cnt/10) * 9;
 if (cnt mod 10) = 0 then
   open c1;
   loop
     fetch c1 into c2;
     exit when c1%NOTFOUND;   
     tcnt := tcnt + 1;
     if(tcnt= hnos1) then
       max90quant := c2.TIME_T;
     end if;      
    end loop; 
   close c1;
   return max90quant;
 else 
  hnos1 := trunc(hnos1);
  hnos2 := hnos1 + 1;
  if(hnos1=0) then
     hnos1 := 1;
  end if;
  open c1;
   loop
     fetch c1 into c2;
     exit when c1%NOTFOUND;
     tcnt := tcnt + 1;
     if(tcnt= hnos1) then
       max90quant := c2.TIME_T;
     end if;      
     if(tcnt= hnos2) then
       max90quant := (max90quant + c2.TIME_T)/2;
     end if;      
    end loop; 
   close c1;
   return max90quant;
 end if;
end;
/




----------------------------------------------------------------
-- This function finds the 10% minimum Quantile 
-- This function should be executed from the CM Data Base
----------------------------------------------------------------

create or replace function min10quantile(nodeid in integer) return number
as
cnt number(4):=0;
tcnt number(4):=0;
lnos1 number(6,2);
lnos2 number(6,2);
min10quant number(6,2);
cursor c1 is select * from (select round(abs(CREATED_DTM- 
 greatest(CREATED_DTM,
 (select CREATED_DTM from ADM_SESSION_TRANSITION where SESSION_ID = a.SESSION_ID and SEQ = (a.SEQ+1))))*24*60*60) TIME_T 
from ADM_SESSION_TRANSITION a
where TRANSITION_ID in (
select TRANSITION_ID   
from ADM_NODE_TRANSITION
where NODE_FROM_ID in 
(select NODE_ID
from
ADM_FAULT_TREE_NODE
where NODE_ID=nodeid)) order by TIME_T)
where TIME_T is not null;
c2 c1%ROWTYPE;
begin
 open c1;
 loop
   fetch c1 into c2;
   exit when c1%NOTFOUND;
   cnt := cnt + 1;  
 end loop; 
 close c1;
 lnos1 := cnt/10;
 if (cnt mod 10) = 0 then
   open c1;
   loop
     fetch c1 into c2;
     exit when c1%NOTFOUND;   
     tcnt := tcnt + 1;
     if(tcnt = lnos1) then
       min10quant := c2.TIME_T;
     end if;
    end loop; 
   close c1;
   return min10quant; 
 else 
  lnos1 := trunc(lnos1);
  lnos2 := lnos1 + 1;
  if(lnos1=0) then
     lnos1 := 1; 
  end if; 
   open c1;
   loop
     fetch c1 into c2;
     exit when c1%NOTFOUND;
     tcnt := tcnt + 1;
     if(tcnt= lnos1) then
       min10quant := c2.TIME_T;
     end if;
     if(tcnt= lnos2) then
       min10quant := (min10quant + c2.TIME_T)/2;
     end if;
    end loop; 
   close c1;
   return min10quant; 
 end if;
end;
/



------------------------------------------------------------------------
--  This function returns the count of SESSION_ID for the given   
--  SESSION_ID for Symptom-Solution Report.
--  Execute this function from the CM Data-Base 
------------------------------------------------------------------------

create or replace function cnt_sym_sol(session_id in number) return number as
 cnt_ret number(6);
 begin
 select SCNT into cnt_ret from
 (
 select SID, count(SID) SCNT from
  (
   select distinct c.SESSION_ID sid,d.NODE_TO_ID
   from adm_symptom a,
        adm_session_event b ,
        adm_session_transition c ,
     adm_node_transition d
   where
   a.PKG_ID = b.PKG_ID and
   d.TRANSITION_ID = c.TRANSITION_ID and
   b.SESSION_ID = c.SESSION_ID and
   d.NODE_TO_ID = a.SOLUTION_ID and
   c.TRANSITION_ID is not null
 )
 group by SID
 )where SID = session_id;
 return (cnt_ret);
 end;
/



------------------------------------------------------------------------
--  This function finds the Sum of Time for the given NODE_ID  
--  for Symptom-Solution Report.
--  Execute this function from the CM Data-Base 
------------------------------------------------------------------------

create or replace function sum_time_solution(nodeid in number) return number as
avg_t number(10,2);
begin
select AVG_TIM into avg_t from
(
select NODE_TO_ID, sum(AVG_TIM) AVG_TIM from
 (
   select distinct d.NODE_TO_ID,
       (
       ( select
               sum(
           (
         (select TO_NUMBER(TO_CHAR(b.created_dtm, 'SSSSS'))
                FROM adm_session_event b
          WHERE b.SESSION_ID = a2.SESSION_ID
                and  b.EVENT_TYPE = 'c'
                   )
              -
        nvl(
                (select TO_NUMBER(TO_CHAR(b.created_dtm, 'SSSSS'))
             from adm_session_event b
         where b.SESSION_ID = a2.SESSION_ID
         and b.EVENT_TYPE = 'r'
         ),0 )
         )
        +
         (
        nvl(
         (select TO_NUMBER(TO_CHAR(b.created_dtm, 'SSSSS'))
                FROM adm_session_event b
          WHERE  b.SESSION_ID = a2.SESSION_ID
                and  b.EVENT_TYPE = 's'
                   ) ,0)
            -
                        (select TO_NUMBER(TO_CHAR(b.created_dtm, 'SSSSS'))
             from adm_session_event b
         where b.SESSION_ID = a2.SESSION_ID
         and b.EVENT_TYPE = 'n'
         )
          )
       )
     from ADM_SESSION_EVENT a2
        where a2.SESSION_ID = b.SESSION_ID and
        a2.EVENT_TYPE = 'c'
 )
 /(select cnt_sym_sol(c.SESSION_ID) from dual)
 )
 AVG_TIM
 from adm_symptom a,
   adm_session_event b ,
      adm_session_transition c,
   adm_node_transition d
 where
 a.PKG_ID = b.PKG_ID and
 d.TRANSITION_ID = c.TRANSITION_ID  and
 b.SESSION_ID = c.SESSION_ID and
 c.TRANSITION_ID is not null) group by NODE_TO_ID
) where NODE_TO_ID = nodeid;
return (avg_t);
end;
/
