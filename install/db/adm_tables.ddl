
CREATE TABLE ADM_ANSWER (
       answer_id            NUMBER(12) NOT NULL,
       answer_typ           NUMBER(12) NOT NULL,
       Fact_ID              NUMBER(12) NOT NULL,
       text                 VARCHAR2(2000) NULL
);


CREATE TABLE ADM_ANSWER_TYP (
       answer_typ           NUMBER(12) NOT NULL,
       pkg_id               NUMBER(12) NOT NULL,
       question_id          NUMBER(12) NOT NULL,
       answer_tag           VARCHAR2(255) NOT NULL
);


CREATE TABLE ADM_FACT_LOG (
       Fact_ID              NUMBER(12) NOT NULL,
       Session_ID           NUMBER(12) NOT NULL,
       Status               VARCHAR2(1) NOT NULL
                                   CONSTRAINT adm_fact_status18
                                          CHECK (Status IN ('p', 'e', 'a', 't', 'n', 's')),
       Log_status           VARCHAR2(1) NOT NULL
                                   CONSTRAINT adm_log_status18
                                          CHECK (Log_status IN ('v', 'i', 'd')),
       fact_typ             NUMBER(12) NOT NULL,
       obj_id               VARCHAR2(2000) NULL,
       Capture_Node_ID      INTEGER NOT NULL,
       Val                  VARCHAR2(2000) NULL,
       pkg_id               NUMBER(12) NOT NULL,
       fact_nm              VARCHAR2(255) NULL,
       err_msg              VARCHAR2(4000) NULL
);


CREATE TABLE ADM_FACT_SNAP (
       Fact_ID              NUMBER(12) NOT NULL,
       Nm                   VARCHAR2(255) NOT NULL,
       Status               VARCHAR2(1) DEFAULT 'u' NOT NULL
            CONSTRAINT adm_status_permitted_values3
                 CHECK (status IN ('s', 'f', 'i', 'u', 'e', 'h', 'l')),
       Display              VARCHAR2(1) DEFAULT 'y' NOT NULL
            CONSTRAINT adm_disp_permitted_values3
                 CHECK (display IN ('y', 'n')),
       Unit_of_Measure      VARCHAR2(20) NULL,
       Val                  VARCHAR2(2000) NULL
);


CREATE TABLE ADM_FACT_TYPE (
       fact_typ             NUMBER(12) NOT NULL,
       pkg_id               NUMBER(12) NOT NULL,
       obj_class_nm         VARCHAR2(255) NULL,
       fact_val_class_nm    VARCHAR2(255) NOT NULL,
       Fact_Typ_NM          VARCHAR2(255) NOT NULL
);


CREATE TABLE ADM_FAULT_TREE (
       pkg_id               NUMBER(12) NOT NULL,
       pkg_nm               VARCHAR2(255) NOT NULL
);


CREATE TABLE ADM_FAULT_TREE_NODE (
       Node_ID              NUMBER(12) NOT NULL,
       pkg_id               NUMBER(12) NOT NULL,
       node_nm              VARCHAR2(255) NOT NULL,
       script_id            NUMBER(12) NOT NULL
);


CREATE TABLE ADM_NODE_TRANSITION (
       Transition_ID        INTEGER NOT NULL,
       Node_to_ID           INTEGER NOT NULL,
       Node_from_ID         INTEGER NOT NULL,
       pkg_id               NUMBER(12) NOT NULL
);


CREATE TABLE ADM_QUESTION (
       question_id          NUMBER(12) NOT NULL,
       pkg_id               NUMBER(12) NOT NULL,
       question_tag         VARCHAR2(255) NOT NULL
);


CREATE TABLE ADM_SESSION (
       Session_ID           NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL,
       Current_Node_ID      NUMBER(12) NULL,
       Owned_by             VARCHAR2(255) NULL,
       pkg_id               NUMBER(12) NULL,
       Status               VARCHAR2(1) NOT NULL
                                   CONSTRAINT adm_sess_status14
                                          CHECK (Status IN ('r', 'c', 's'))
);


CREATE TABLE ADM_SESSION_EVENT (
       Session_ID           NUMBER(12) NOT NULL,
       seq                  NUMBER(12) NOT NULL,
       Event_Type           VARCHAR(1) NOT NULL
                                   CONSTRAINT event_type14
                                          CHECK (Event_Type IN ('n', 'c', 's', 'r')),
       node_ID              NUMBER(12) NULL,
       pkg_id               NUMBER(12) NOT NULL,
       Notes                VARCHAR2(2000) NULL,
       Created_dtm          DATE NOT NULL,
       Created_by           VARCHAR2(255) NOT NULL
);


CREATE TABLE ADM_SESSION_TRANSITION (
       Session_ID           NUMBER(12) NOT NULL,
       seq                  INTEGER NOT NULL,
       Transition_ID        INTEGER NULL,
       Created_dtm          DATE NOT NULL,
       Created_by           VARCHAR2(255) NOT NULL,
       jump_to_node_id      NUMBER(12) NULL,
       pkg_id               NUMBER(12) NULL
);


CREATE TABLE ADM_SYMPTOM (
       symptom_id           NUMBER(12) NOT NULL,
       pkg_id               NUMBER(12) NOT NULL,
       symptom_typ          NUMBER(12) NOT NULL,
       Fact_ID              NUMBER(12) NOT NULL,
       text                 VARCHAR2(400) NULL,
       status               VARCHAR2(20) NOT NULL
                                   CONSTRAINT symptom_status9
                                          CHECK (status IN ('d', 'c', 'p')),
       is_primary           VARCHAR2(1) NOT NULL
                                   CONSTRAINT yes_no7
                                          CHECK (is_primary IN ('y', 'n')),
       solution_id          NUMBER(12) NULL
);


CREATE TABLE ADM_SYMPTOM_TYP (
       pkg_id               NUMBER(12) NOT NULL,
       symptom_typ          NUMBER(12) NOT NULL,
       symptom_tag          VARCHAR2(255) NOT NULL,
       priority             NUMBER(12) NULL,
       start_node_id        NUMBER(12) NULL
);


CREATE TABLE ADM_VAL_LIST (
       val_list_id          NUMBER(12) NOT NULL,
       Seq                  NUMBER(12) NOT NULL,
       Fact_ID              NUMBER(12) NOT NULL,
       Val                  VARCHAR2(2000) NULL,
       nm                   VARCHAR2(2000) NULL
);


CREATE TABLE ADM_FACT_DEF_LIBRARY (
       FACT_DEF_ID          NUMBER(12) NOT NULL,
       FACT_NM              VARCHAR2(255) NULL,
       NODE_ID              NUMBER(12) NOT NULL,
       PKG_ID               NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_FAULT_TREE_SCRIPT (
       SCRIPT_NM            VARCHAR2(255) NULL,
       SCRIPT_ID            NUMBER(12) NOT NULL,
       PKG_ID               NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_LBL_ANSWER (
       ANSWER_TYP           NUMBER(12) NOT NULL,
       PARM_NM              VARCHAR2(255) NOT NULL,
       TEXT                 VARCHAR2(2000) NULL,
       LOCALE_ID            NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_LBL_FACT_PARM (
       FACT_DEF_ID          NUMBER(12) NOT NULL,
       PARM_NM              VARCHAR2(255) NOT NULL,
       TEXT                 VARCHAR2(2000) NULL,
       LOCALE_ID            NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_LBL_NODE (
       TEXT                 VARCHAR2(2000) NULL,
       LOCALE_ID            NUMBER(12) NOT NULL,
       PKG_ID               NUMBER(12) NOT NULL,
       NODE_ID              NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_LBL_QUESTION (
       TEXT                 VARCHAR2(2000) NULL,
       LOCALE_ID            NUMBER(12) NOT NULL,
       PKG_ID               NUMBER(12) NOT NULL,
       QUESTION_ID          NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_LBL_SYMPTOM (
       TEXT                 VARCHAR2(2000) NULL,
       SYMPTOM_TYP          NUMBER(12) NOT NULL,
       LOCALE_ID            NUMBER(12) NOT NULL,
       PKG_ID               NUMBER(12) NOT NULL
);
  
  
CREATE TABLE ADM_LOCALE (
       LOCALE_ID            NUMBER(12) NOT NULL,
       LANGUAGE             VARCHAR2(2) NULL,
       COUNTRY              VARCHAR2(2) NULL,
       PKG_ID               NUMBER(12) NOT NULL
);
  

