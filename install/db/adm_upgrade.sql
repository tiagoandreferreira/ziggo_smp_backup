---------------------------------------------------------------------------
-- FILE INFO
--    $Id: adm_upgrade.sql,v 1.2 2004/04/07 18:10:24 caling Exp $
--
-- DESCRIPTION
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------


-- This is required to add the status column in the adm_fact_snap table

ALTER TABLE ADM_FACT_SNAP 
    ADD (Status VARCHAR2(1) DEFAULT 'u' NULL
         CONSTRAINT adm_fs_status_permitted_values
              CHECK (status IN ('s', 'f', 'i', 'u')));


