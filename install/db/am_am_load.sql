--============================================================================
--    $Id: am_am_load.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

exec am_add_privilege('samp.web.exception', 'n', 'am_mgr', 'view');
exec am_add_privilege('samp.web.menu.admin', 'n', 'am_mgr', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'am_mgr', 'view');
exec am_add_privilege('smp.admin.group', 'n', 'am_mgr', 'view');
exec am_add_privilege('smp.admin.group', 'n', 'am_mgr', 'create');
exec am_add_privilege('smp.admin.group', 'n', 'am_mgr', 'delete');
exec am_add_privilege('smp.admin.group', 'n', 'am_mgr', 'update');
exec am_add_privilege('smp.admin.group', 'n', 'am_mgr', 'assign');
exec am_add_privilege('smp.admin.group', 'n', 'am_mgr', 'exclude');
exec am_add_privilege('smp.admin.permission', 'n', 'am_mgr', 'view');
exec am_add_privilege('smp.admin.permission', 'n', 'am_mgr', 'create');
exec am_add_privilege('smp.admin.permission', 'n', 'am_mgr', 'update');
exec am_add_privilege('smp.admin.permission', 'n', 'am_mgr', 'delete');
exec am_add_privilege('smp.admin.privilege', 'n', 'am_mgr', 'view');
exec am_add_privilege('smp.admin.privilege', 'n', 'am_mgr', 'create');
exec am_add_privilege('smp.admin.privilege', 'n', 'am_mgr', 'delete');
exec am_add_privilege('smp.admin.privilege', 'n', 'am_mgr', 'update');
exec am_add_privilege('smp.admin.resource', 'n', 'am_mgr', 'view');
exec am_add_privilege('smp.admin.resource', 'n', 'am_mgr', 'create');
exec am_add_privilege('smp.admin.resource', 'n', 'am_mgr', 'delete');
exec am_add_privilege('smp.admin.resource', 'n', 'am_mgr', 'update');
exec am_add_privilege('smp.admin.user', 'n', 'am_mgr', 'view');
exec am_add_privilege('smp.admin.user', 'n', 'am_mgr', 'create');
exec am_add_privilege('smp.admin.user', 'n', 'am_mgr', 'update');
exec am_add_privilege('smp.admin.user', 'n', 'am_mgr', 'delete');
exec am_add_privilege('smp.admin.user', 'n', 'am_mgr', 'changeOthersPassword');
exec am_add_privilege('smp.admin.user', 'n', 'am_mgr', 'changeOwnPassword');
exec am_add_privilege('smp.admin.parm', 'n', 'am_mgr', 'view');

exec am_add_privilege('smp.management', 'n', 'csr_supervisor', 'execute');

exec am_create_user('am1' , 'pwam1', 'manager user');
exec am_add_grp_user('am1' , 'am_mgr');

exec am_create_user('jpam' , 'jpam' , ' jp manager user');
exec am_add_user_parm('jpam', 'country', 'JP');
exec am_add_user_parm('jpam', 'language', 'ja');
exec am_add_grp_user('jpam' , 'am_mgr');

