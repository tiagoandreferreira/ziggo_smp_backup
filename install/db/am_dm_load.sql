--============================================================================
--    $Id: am_dm_load.sql,v 1.1 2007/11/29 21:15:28 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

exec am_add_secure_obj('smp.dm.test.synopsis','synopsis test phase resource');
exec am_add_secure_obj('smp.dm.test.intensity_1','lowest test phase resource');
exec am_add_secure_obj('smp.dm.test.intensity_2','second lowest test phase resource');
exec am_add_secure_obj('smp.dm.test.intensity_3','third lowest test phase resource');

exec am_add_privilege('samp.web.testresult', 'n', 'dm_mgr', 'view');
exec am_add_privilege('samp.web.svcadvisory', 'n', 'dm_mgr', 'view');
exec am_add_privilege('smp.dm.test', 'n', 'dm_mgr', 'view');
exec am_add_privilege('smp.dm.test_parm', 'n', 'dm_mgr', 'view');
exec am_add_privilege('samp.web.exception', 'n', 'dm_mgr', 'view');
exec am_add_privilege('samp.web.menu.diagnostics', 'n', 'dm_mgr', 'view');
exec am_add_privilege('smp.dm.service', 'n', 'dm_mgr', 'reset');
exec am_add_privilege('smp.dm.test.synopsis', 'n', 'dm_mgr', 'execute');
exec am_add_privilege('smp.dm.test.intensity_1', 'n', 'dm_mgr', 'execute');
exec am_add_privilege('smp.dm.test.intensity_2', 'n', 'dm_mgr', 'execute');
exec am_add_privilege('smp.dm.test.intensity_3', 'n', 'dm_mgr', 'execute');
exec am_add_privilege('smp.dm.svcAdvisory', 'n', 'dm_mgr', 'update');
exec am_add_privilege('smp.dm.troubleTicket', 'n', 'dm_mgr', 'create');
exec am_add_privilege('smp.dm.testResults', 'n', 'dm_mgr', 'viewDetails');
exec am_add_privilege('smp.dm.testResults', 'n', 'dm_mgr', 'viewHistorical');
exec am_add_privilege('smp.dm.subscriber', 'n', 'dm_mgr', 'search');
exec am_add_privilege('smp.dm', 'n', 'dm_mgr', 'logout');

exec am_create_user('dm1' , 'pwdm1', 'manager user');
exec am_add_grp_user('dm1' , 'dm_mgr');
