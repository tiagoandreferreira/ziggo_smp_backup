--==============================================================================
--  $Id: am_excluded_ui_parms.sql,v 1.15 2012/01/30 05:05:07 nehah Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--      sql commands to setup consumable resources
--  REVISION HISTORY
--  * Based on CVS log
--==============================================================================

--Start User Service
EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.users.SUB_SVC_ID','y','csr_residential','edit');

-- Start trunking Services
EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','  Parameter: group sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','  Parameter: group sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','  Parameter: group sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','  Parameter: group sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service.GROUP_SUB_SVC_ID','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','  Parameter: child sub svc');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','  Parameter: child sub svc');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','  Parameter: child sub svc');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','  Parameter: child sub svc');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.CHILD_SUB_SVC','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.sip_trunking_service_member.samp_sub_key','y','csr_residential','edit');

-- End trunking Services

-- hide the PARENT_SUB_SVC_ID to disaply in  search result.

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','  Parameter:parent  sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.PARENT_SUB_SVC_ID','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','  Parameter: parent  sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','  Parameter:parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.PARENT_SUB_SVC_ID','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','  Parameter: parent sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','  Parameter:parent  sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','  Parameter:parent  sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.PARENT_SUB_SVC_ID','y','csr_residential','edit');


-- hide samp_sub_key to display in seach by criteria

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.hunt_group.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_commercial','edit');

\EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_pickup_group.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.call_park_group.samp_sub_key','y','csr_residential','edit');


-- Added for voice services

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.sub_svc_id','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.voice_service.samp_sub_key','y','csr_residential','edit');

-- Added for commercial equipment services

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.sub_svc_id','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','  Parameter: samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.samp_sub_key','y','csr_residential','edit');

-- hide the samp_sub_key,managedEntityKey in  batch search result.
EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','Managed Entity Key');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','sub svc id');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','Managed Entity Key');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','sub svc id');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','Managed Entity Key');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_all','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','sub svc id');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','Managed Entity Key');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_residential','view');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId.managedEntityKey','y','csr_residential','edit');

-- Added for commercial equipment services byDeviceName

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.sub_svc_id','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','  Parameter: sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.SubSvc.commercial_cpe_equipment.byDeviceName.samp_sub_key','y','csr_residential','edit');

-- hide the samp_sub_key,sub svc id in  batch voice Lines search .
EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.voiceLines.sub_svc_id','y','csr_residential','edit');

-- hide bulk.batchQueryByBatchId query on core batch search page
EXEC am_add_secure_obj('smp.query.batchorder.bulk.batchQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_supervisor','execute');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_supervisor','view');

EXEC am_add_secure_obj('smp.query.batchorder.bulk.batchQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_commercial','execute');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_commercial','view');

EXEC am_add_secure_obj('smp.query.batchorder.bulk.batchQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_all','execute');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_all','view');

EXEC am_add_secure_obj('smp.query.batchorder.bulk.batchQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_residential','execute');
EXEC am_add_privilege('smp.query.batchorder.bulk.batchQueryByBatchId','y','csr_residential','view');

-- Solution am cofig for bulk.batchQueryByBatchId
EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_supervisor','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_supervisor','view');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_commercial','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_commercial','view');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_all','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_all','view');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_residential','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.bulk.batchQueryByBatchId','n','csr_residential','view');

-- hide BulkOrderQueryByBatchId query on core batch search page
EXEC am_add_secure_obj('smp.query.batchorder.BulkOrderQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_supervisor','execute');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_supervisor','view');

EXEC am_add_secure_obj('smp.query.batchorder.BulkOrderQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_commercial','execute');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_commercial','view');

EXEC am_add_secure_obj('smp.query.batchorder.BulkOrderQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_all','execute');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_all','view');

EXEC am_add_secure_obj('smp.query.batchorder.BulkOrderQueryByBatchId','Query Form on core page');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_residential','execute');
EXEC am_add_privilege('smp.query.batchorder.BulkOrderQueryByBatchId','y','csr_residential','view');

-- Solution am cofig for bulk.BulkOrderQueryByBatchId
EXEC am_add_secure_obj('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_supervisor','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_supervisor','view');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_commercial','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_commercial','view');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_all','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_all','view');

EXEC am_add_secure_obj('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','Query Form on solution search page');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_residential','execute');
EXEC am_add_privilege('smp.query.bulk.batchorder.BulkOrderQueryByBatchId','n','csr_residential','view');


-- hide the samp_sub_key,sub svc id in  batch voice change feature package
EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_supervisor','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_supervisor','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_supervisor','edit');


EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_commercial','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_commercial','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_all','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_all','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','samp sub key');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.samp_sub_key','y','csr_residential','edit');

EXEC am_add_secure_obj('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','sub svc id');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_residential','view');
EXEC am_add_privilege('smp.query.subbrief_jsr144.bulk.changefeaturepackage.sub_svc_id','y','csr_residential','edit');

COMMIT;
