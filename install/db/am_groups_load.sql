--============================================================================
--    $Id: am_groups_load.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

exec am_add_grp('dm_mgr' , 'manager group role');
exec am_add_grp('am_mgr' , 'admin manager group role');
exec am_add_grp('csr', 'Customer Sales Representatives Group');
exec am_add_grp('report_usr', 'User Group for report application login');
exec am_add_grp('sub', 'Subscriber - Self Service Mode');
exec am_add_grp('csr_supervisor', 'Customer Sales Representatives supervisor Group');
exec am_add_grp('sub.secondary','Secondary selfcare login');

exec am_add_grp('scm_csr' , 'SCM Sales Representative Group Users');
exec am_add_grp('scm_mgr' , 'SCM Manager Group Users');
exec am_add_grp('scm_sigma', 'SCM Sigma Group Users');
exec am_add_grp('scm_admin', 'SCM Admin Group Users');

exec am_add_grp('csr_admin','Customer Sales Representatives Administration Group');
exec am_add_grp_grp('csr_admin', 'csr_supervisor');
exec am_add_grp_grp('csr_admin', 'am_mgr');

exec am_add_grp_grp('csr_supervisor', 'report_usr');
exec am_add_grp_grp('csr', 'report_usr');

exec am_add_grp('csr_view','Customer Sales Representatives View Only Group');
exec am_add_grp_grp('csr_view', 'csr_supervisor');

exec am_add_grp('Administrators', 'Members of this group will have administrative privileges for weblogic server');
