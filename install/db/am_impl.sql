---------------------------------------------------------------------------
-- FILE INFO
--    $Id: am_impl.sql,v 1.17 2012/02/07 19:23:21 zhenl Exp $
--
-- DESCRIPTION
--
-- Core upgrade 2.4 to 3.0
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------

exec am_add_secure_obj('samp.web.subparm.contacts.primary.address.type', 'Subscriber address parameter: contacts.primary.address.type');

exec am_add_secure_obj('samp.web.orders.capture_header_parm.debug_order', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.has_groups', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.pre_processor_info', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.pre_processor_jndiname', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.sub_version', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.user_id', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.description', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.priority', '');

-- For CSC
exec am_add_secure_obj('samp.web.orders.capture_header_parm.switch_response', ' Switch Response Order Level parm for CSC');


exec am_add_secure_obj('samp.web.chgstatus', 'Delete entity');

-- to add entry for "samp.web.subparm.default"
exec am_add_secure_obj('samp.web.subparm.default', 'Default property of sub addresses');

-- buttons on batch order results page
exec am_add_privilege('samp.web.batch', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.admin_batch.release_ownership', 'n', 'csr_admin', 'view');

exec am_add_privilege('samp.web.orders.actions.edit', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.actions.replicate', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.batch', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders', 'n', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.display_children_info', 'n', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.subparm.contacts.primary.address.type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.subparm.contacts.primary.address.pref_nm', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.subparm.contacts.primary.address.type', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.subparm.contacts.primary.address.pref_nm', 'y', 'csr_supervisor', 'view');


-- For  CSC
exec am_add_privilege('samp.web.orders.capture_header_parm.switch_response', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.switch_response', 'y', 'csr_admin', 'view');

-- Core configuration for order header parms permissions
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.actualCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.apiClientId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.created_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.created_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.debug_order', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.err_code', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.err_reason', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.failedServices', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.has_groups', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.managedEntityKey', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.modified_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.modified_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.need_partitioned_loading', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.pre_processor_info', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.pre_processor_jndiname', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.purchaseOrder', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queue_request_flag', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.samp_sub_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.src_nm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.state', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.sub_version', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.user_id', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.sub_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.requestedCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.orderDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.description', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.order_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.priority', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.order_type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.external_batch_id', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.orders.update_header_parm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.actualCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.apiClientId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.created_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.created_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.debug_order', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.err_code', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.err_reason', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.failedServices', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.has_groups', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.managedEntityKey', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.modified_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.modified_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.need_partitioned_loading', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.pre_processor_info', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.pre_processor_jndiname', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.purchaseOrder', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.queue_request_flag', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.samp_sub_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.src_nm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.state', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.sub_version', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.user_id', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.sub_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.requestedCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.orderDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.description', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.order_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.priority', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.order_type', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.ordr_owner', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.external_batch_id', 'y', 'csr_supervisor', 'view');

-- to exclude edit permission of "samp.web.subparm.default" for csr_supervisor--
exec am_add_privilege('samp.web.subparm.default', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.subparm.default', 'y', 'csr_supervisor', 'view');

--------------------------------------------------------------------------
-- hide the left tree config
--------------------------------------------------------------------------

-- to add entry for Voice Ala Carte Features
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:anonymous_call_rejection', 'Voice Ala Carte Features: anonymous_call_rejection');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:attendant_console', 'Voice Ala Carte Features: attendant_console');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:authentication', 'Voice Ala Carte Features: authentication');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:automatic_call_back', 'Voice Ala Carte Features: automatic_call_back');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:automatic_hold_retrieve', 'Voice Ala Carte Features: automatic_hold_retrieve');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:automatic_recall', 'Voice Ala Carte Features: automatic_recall');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:barge-in_exempt', 'Voice Ala Carte Features: barge-in_exempt');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_busy', 'Voice Ala Carte Features: call_forward_busy');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_dont_answer', 'Voice Ala Carte Features: call_forward_dont_answer');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_fixed', 'Voice Ala Carte Features: call_forward_fixed');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_universal', 'Voice Ala Carte Features: call_forward_universal');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_option', 'Voice Ala Carte Features: call_forward_option');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_hold', 'Voice Ala Carte Features: call_hold');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:basic_call_logs', 'Voice Ala Carte Features: basic_call_logs');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_notify', 'Voice Ala Carte Features: call_notify');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_park', 'Voice Ala Carte Features: call_park');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_transfer', 'Voice Ala Carte Features: call_transfer');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_waiting', 'Voice Ala Carte Features: call_waiting');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cancel_call_waiting', 'Voice Ala Carte Features: cancel_call_waiting');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_blocking', 'Voice Ala Carte Features: calling_identity_delivery_blocking');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_on_call_waiting', 'Voice Ala Carte Features: calling_identity_delivery_on_call_waiting');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_name_delivery', 'Voice Ala Carte Features: calling_name_delivery');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_name_retrieval', 'Voice Ala Carte Features: calling_name_retrieval');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_number_delivery', 'Voice Ala Carte Features: calling_number_delivery');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:commpilot_call_manager', 'Voice Ala Carte Features: commpilot_call_manager');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:commpilot_express', 'Voice Ala Carte Features: commpilot_express');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:conference', 'Voice Ala Carte Features: conference');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback_user', 'Voice Ala Carte Features: custom_ringback_user');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:customer_originated_trace', 'Voice Ala Carte Features: customer_originated_trace');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:deny_call_waiting_tone', 'Voice Ala Carte Features: deny_call_waiting_tone');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:dialable_directory_number', 'Voice Ala Carte Features: dialable_directory_number');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:directed_call_park', 'Voice Ala Carte Features: directed_call_park');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:directed_call_pickup', 'Voice Ala Carte Features: directed_call_pickup');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ring', 'Voice Ala Carte Features: distinctive_ring');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ringing_call_waiting', 'Voice Ala Carte Features: distinctive_ringing_call_waiting');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:diversion_inhibit', 'Voice Ala Carte Features: diversion_inhibit');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:do_not_disturb', 'Voice Ala Carte Features: do_not_disturb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:enhanced_call_logs', 'Voice Ala Carte Features: enhanced_call_logs');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:hotline', 'Voice Ala Carte Features: hotline');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:last_number_redial', 'Voice Ala Carte Features: last_number_redial');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:long_distance', 'Voice Ala Carte Features: long_distance');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:make_set_busy', 'Voice Ala Carte Features: make_set_busy');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:malicious_call_trace', 'Voice Ala Carte Features: malicious_call_trace');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:multiple_appearance_directory_number', 'Voice Ala Carte Features: multiple_appearance_directory_number');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outlook_integration', 'Voice Ala Carte Features: outlook_integration');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outside_calling_area_alerting', 'Voice Ala Carte Features: outside_calling_area_alerting');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:remote_activation_of_call_forwarding', 'Voice Ala Carte Features: remote_activation_of_call_forwarding');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:remote_office', 'Voice Ala Carte Features: remote_office');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:selective_call_acceptance', 'Voice Ala Carte Features: selective_call_acceptance');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:selective_call_forwarding', 'Voice Ala Carte Features: selective_call_forwarding');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:selective_call_rejection', 'Voice Ala Carte Features: selective_call_rejection');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:sequential_ring', 'Voice Ala Carte Features: sequential_ring');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:simultaneous_ring', 'Voice Ala Carte Features: simultaneous_ring');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:sip_package', 'Voice Ala Carte Features: sip_package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_short', 'Voice Ala Carte Features: speed_calling_short');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_long', 'Voice Ala Carte Features: speed_calling_long');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:third_party_mwi_control', 'Voice Ala Carte Features: third_party_mwi_control');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:third_party_voice_mail_support', 'Voice Ala Carte Features: third_party_voice_mail_support');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:three_way_calling', 'Voice Ala Carte Features: three_way_calling');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_defaults', 'Voice Ala Carte Features: voice_mail_defaults');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_portal_calling', 'Voice Ala Carte Features: voice_portal_calling');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:warm_line', 'Voice Ala Carte Features: warm_line');

-- to add entry for Voice Profile - Ala Carte Features
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:anonymous_call_rejection_profile', 'Ala Carte Features: anonymous_call_rejection_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:attendant_console_profile', 'Ala Carte Features: attendant_console_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:authentication_profile', 'Ala Carte Features: authentication_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:automatic_call_back_profile', 'Ala Carte Features: automatic_call_back_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:automatic_hold_retrieve_profile', 'Ala Carte Features: automatic_hold_retrieve_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:automatic_recall_profile', 'Ala Carte Features: automatic_recall_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:barge-in_exempt_profile', 'Ala Carte Features: barge-in_exempt_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:basic_call_logs_profile', 'Ala Carte Features: basic_call_logs_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_busy_profile', 'Ala Carte Features: call_forward_busy_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_dont_answer_profile', 'Ala Carte Features: call_forward_dont_answer_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_fixed_profile', 'Ala Carte Features: call_forward_fixed_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_universal_profile', 'Ala Carte Features: call_forward_universal_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_forward_option_profile', 'Ala Carte Features: call_forward_option_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_hold_profile', 'Ala Carte Features: call_hold_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_notify_profile', 'Ala Carte Features: call_notify_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_park_profile', 'Ala Carte Features: call_park_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_transfer_profile', 'Ala Carte Features: call_transfer_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_waiting_profile', 'Ala Carte Features: call_waiting_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cancel_call_waiting_profile', 'Ala Carte Features: cancel_call_waiting_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_blocking_profile', 'Ala Carte Features: calling_identity_delivery_blocking_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_on_call_waiting_profile', 'Ala Carte Features: calling_identity_delivery_on_call_waiting_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_name_delivery_profile', 'Ala Carte Features: calling_name_delivery_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_name_retrieval_profile', 'Ala Carte Features: calling_name_retrieval_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_number_delivery_profile', 'Ala Carte Features: warm_line');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:commpilot_call_manager_profile', 'Ala Carte Features: commpilot_call_manager_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:commpilot_express_profile', 'Ala Carte Features: commpilot_express_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:conference_profile', 'Ala Carte Features: conference_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback_user_profile', 'Ala Carte Features: custom_ringback_user_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:customer_originated_trace_profile', 'Ala Carte Features: customer_originated_trace_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:deny_call_waiting_tone_profile', 'Ala Carte Features: deny_call_waiting_tone_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:dialable_directory_number_profile', 'Ala Carte Features: dialable_directory_number_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:directed_call_park_profile', 'Ala Carte Features: directed_call_park_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:directed_call_pickup_profile', 'Ala Carte Features: directed_call_pickup_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ring_profile', 'Ala Carte Features: distinctive_ring_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ringing_call_waiting_profile', 'Ala Carte Features: distinctive_ringing_call_waiting_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:diversion_inhibit_profile', 'Ala Carte Features: diversion_inhibit_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:do_not_disturb_profile', 'Ala Carte Features: do_not_disturb_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:enhanced_call_logs_profile', 'Ala Carte Features: enhanced_call_logs_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:hotline_profile', 'Ala Carte Features: hotline_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:last_number_redial_profile', 'Ala Carte Features: last_number_redial_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:long_distance_profile', 'Ala Carte Features: long_distance_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:make_set_busy_profile', 'Ala Carte Features: make_set_busy_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:malicious_call_trace_profile', 'Ala Carte Features: malicious_call_trace_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:multiple_appearance_directory_number_profile', 'Ala Carte Features: multiple_appearance_directory_number_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outlook_integration_profile', 'Ala Carte Features: outlook_integration_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outside_calling_area_alerting_profile', 'Ala Carte Features: outside_calling_area_alerting_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:primary_voice_mail_profile', 'Ala Carte Features: primary_voice_mail_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:remote_activation_of_call_forwarding_profile', 'Ala Carte Features: remote_activation_of_call_forwarding_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:remote_office_profile', 'Ala Carte Features: remote_office_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:selective_call_acceptance_profile', 'Ala Carte Features: selective_call_acceptance_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:selective_call_forwarding_profile', 'Ala Carte Features: selective_call_forwarding_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:selective_call_rejection_profile', 'Ala Carte Features: selective_call_rejection_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:sequential_ring_profile', 'Ala Carte Features: sequential_ring_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:simultaneous_ring_profile', 'Ala Carte Features: simultaneous_ring_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:sip_package_profile', 'Ala Carte Features: sip_package_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_short_profile', 'Ala Carte Features: speed_calling_short_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_long_profile', 'Ala Carte Features: speed_calling_long_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:third_party_mwi_control_profile', 'Ala Carte Features: third_party_mwi_control_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:third_party_voice_mail_support_profile', 'Ala Carte Features: third_party_voice_mail_support_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:three_way_calling_profile', 'Ala Carte Features: three_way_calling_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:transfer_voice_mail_profile', 'Ala Carte Features: transfer_voice_mail_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:unified_messaging_profile', 'Ala Carte Features: unified_messaging_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_defaults_profile', 'Ala Carte Features: voice_mail_defaults_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_portal_calling_profile', 'Ala Carte Features: voice_portal_calling_profile');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:warm_line_profile', 'Ala Carte Features: warm_line_profile');

-- to add entry for Commercial EMTA
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:port', 'Commercial EMTA port service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:emta_voice_device', 'Commercial EMTA emta_voice_device service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:commercial_hsd_device', 'Commercial EMTA commercial_hsd_device service');

-- to add entry for Commercial IAD
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:iad_device_control', 'Commercial IAD iad_device_control service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:iad_commercial_hsd_device', 'Commercial IAD iad_commercial_hsd_device service');

-- to add entry for Commercial ATA
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:ata_device_control', 'Commercial ATA ata_device_control service');

-- to add entry for Commercial Hosted IVR
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:ivr_definition_device_control', 'Commercial Hosted IVR ivr_definition_device_control service');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:comm_soft_phone_device_control', 'Commercial soft phone device_control service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:ip_phone_device_control', 'Commercial ip phone device_control service');

-- to add entry for Company Features
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:account_codes', 'Company Features account_codes service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:authorization_codes', 'Company Features authorization_codes service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_park_allowed', 'Company Features call_park_allowed service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_allowed', 'Company Features call_pickup_allowed service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:calling_card_allowed', 'Company Features calling_card_allowed service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:company_auto_attendant', 'Company Features company_auto_attendant service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback', 'Company Features custom_ringback service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:extensions', 'Company Features extensions service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:hunt_group_allowed', 'Company Features hunt_group_allowed service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:instant_group_call', 'Company Features instant_group_call service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:ldap_integration', 'Company Features ldap_integration service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:music_on_hold', 'Company Features music_on_hold service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:series_completion', 'Company Features series_completion service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_messaging_group', 'Company Features voice_messaging_group service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:company_external_topology', 'Company Features company_external_topology service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:trunk_group_allowed', 'Company Features trunk_group_allowed service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_park_group_allowed', 'Company Features call_park_group_allowed service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:inventory_report_profile', 'Company Features inventory_report_profile service');

-- to exclude Ala Carte Feature Services for csr_supervisor--
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:anonymous_call_rejection', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:anonymous_call_rejection', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:attendant_console', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:attendant_console', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:authentication', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:authentication', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_call_back', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_call_back', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_hold_retrieve', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_hold_retrieve', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_recall', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_recall', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:barge-in_exempt', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:barge-in_exempt', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_busy', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_busy', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_dont_answer', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_dont_answer', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_fixed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_fixed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_universal', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_universal', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_option', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_option', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_hold', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_hold', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:basic_call_logs', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:basic_call_logs', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_notify', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_notify', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_transfer', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_transfer', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_waiting', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_waiting', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cancel_call_waiting', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cancel_call_waiting', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_blocking', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_blocking', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_on_call_waiting', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_on_call_waiting', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_delivery', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_delivery', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_retrieval', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_retrieval', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_number_delivery', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_number_delivery', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_call_manager', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_call_manager', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_express', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_express', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:conference', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:conference', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback_user', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback_user', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:customer_originated_trace', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:customer_originated_trace', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deny_call_waiting_tone', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deny_call_waiting_tone', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:dialable_directory_number', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:dialable_directory_number', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_park', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_park', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_pickup', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_pickup', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ring', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ring', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ringing_call_waiting', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ringing_call_waiting', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:diversion_inhibit', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:diversion_inhibit', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:do_not_disturb', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:do_not_disturb', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:enhanced_call_logs', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:enhanced_call_logs', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hotline', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hotline', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:last_number_redial', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:last_number_redial', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:long_distance', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:long_distance', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:make_set_busy', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:make_set_busy', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:malicious_call_trace', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:malicious_call_trace', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:multiple_appearance_directory_number', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:multiple_appearance_directory_number', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outlook_integration', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outlook_integration', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outside_calling_area_alerting', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outside_calling_area_alerting', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_activation_of_call_forwarding', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_activation_of_call_forwarding', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_office', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_office', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_acceptance', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_acceptance', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_forwarding', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_forwarding', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_rejection', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_rejection', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sequential_ring', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sequential_ring', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:simultaneous_ring', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:simultaneous_ring', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sip_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sip_package', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_short', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_short', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_long', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_long', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_mwi_control', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_mwi_control', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_voice_mail_support', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_voice_mail_support', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:three_way_calling', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:three_way_calling', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_defaults', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_defaults', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_portal_calling', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_portal_calling', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:warm_line', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:warm_line', 'y', 'csr_supervisor', 'view');


-- to add entry for Voice Profile - Ala Carte Features
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:anonymous_call_rejection_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:anonymous_call_rejection_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:attendant_console_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:attendant_console_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:authentication_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:authentication_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_call_back_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_call_back_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_hold_retrieve_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_hold_retrieve_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_recall_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:automatic_recall_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:barge-in_exempt_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:barge-in_exempt_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:basic_call_logs_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:basic_call_logs_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_busy_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_busy_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_dont_answer_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_dont_answer_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_fixed_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_fixed_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_universal_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_universal_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_option_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_forward_option_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_hold_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_hold_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_notify_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_notify_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_transfer_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_transfer_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_waiting_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_waiting_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cancel_call_waiting_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cancel_call_waiting_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_blocking_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_blocking_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_on_call_waiting_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_identity_delivery_on_call_waiting_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_delivery_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_delivery_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_retrieval_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_name_retrieval_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_number_delivery_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_number_delivery_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_call_manager_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_call_manager_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_express_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commpilot_express_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:conference_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:conference_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback_user_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback_user_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:customer_originated_trace_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:customer_originated_trace_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deny_call_waiting_tone_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deny_call_waiting_tone_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:dialable_directory_number_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:dialable_directory_number_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_park_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_park_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_pickup_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directed_call_pickup_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ring_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ring_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ringing_call_waiting_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:distinctive_ringing_call_waiting_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:diversion_inhibit_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:diversion_inhibit_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:do_not_disturb_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:do_not_disturb_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:enhanced_call_logs_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:enhanced_call_logs_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hotline_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hotline_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:last_number_redial_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:last_number_redial_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:long_distance_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:long_distance_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:make_set_busy_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:make_set_busy_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:malicious_call_trace_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:malicious_call_trace_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:multiple_appearance_directory_number_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:multiple_appearance_directory_number_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outlook_integration_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outlook_integration_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outside_calling_area_alerting_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outside_calling_area_alerting_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:primary_voice_mail_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:primary_voice_mail_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_activation_of_call_forwarding_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_activation_of_call_forwarding_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_office_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:remote_office_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_acceptance_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_acceptance_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_forwarding_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_forwarding_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_rejection_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:selective_call_rejection_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sequential_ring_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sequential_ring_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:simultaneous_ring_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:simultaneous_ring_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sip_package_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:sip_package_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_short_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_short_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_long_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:speed_calling_long_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_mwi_control_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_mwi_control_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_voice_mail_support_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:third_party_voice_mail_support_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:three_way_calling_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:three_way_calling_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:transfer_voice_mail_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:transfer_voice_mail_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:unified_messaging_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:unified_messaging_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_defaults_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_defaults_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_portal_calling_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_portal_calling_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:warm_line_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:warm_line_profile', 'y', 'csr_supervisor', 'view');


-- to add entry for Commercial EMTA
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:port', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:port', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:emta_voice_device', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:emta_voice_device', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commercial_hsd_device', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:commercial_hsd_device', 'y', 'csr_supervisor', 'view');

-- to add entry for Commercial IAD
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:iad_device_control', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:iad_device_control', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:iad_commercial_hsd_device', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:iad_commercial_hsd_device', 'y', 'csr_supervisor', 'view');

-- to add entry for Commercial ATA
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ata_device_control', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ata_device_control', 'y', 'csr_supervisor', 'view');

-- to add entry for Commercial Hosted IVR
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ivr_definition_device_control', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ivr_definition_device_control', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:comm_soft_phone_device_control', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:comm_soft_phone_device_control', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ip_phone_device_control', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ip_phone_device_control', 'y', 'csr_supervisor', 'view');

-- to add entry for Company Features
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:account_codes', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:account_codes', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:authorization_codes', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:authorization_codes', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park_allowed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park_allowed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_allowed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_allowed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_card_allowed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:calling_card_allowed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:company_auto_attendant', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:company_auto_attendant', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:custom_ringback', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:extensions', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:extensions', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hunt_group_allowed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hunt_group_allowed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:instant_group_call', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:instant_group_call', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ldap_integration', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:ldap_integration', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:music_on_hold', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:music_on_hold', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:series_completion', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:series_completion', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_messaging_group', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_messaging_group', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:company_external_topology', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:company_external_topology', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:trunk_group_allowed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:trunk_group_allowed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park_group_allowed', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_park_group_allowed', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:inventory_report_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:inventory_report_profile', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:pic_codes_profile', 'COS Profiles-Package');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:pic_codes_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:pic_codes_profile', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_primary_profile', 'COS Profiles-Primary VM');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_primary_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_primary_profile', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_unified_profile', 'COS Profiles-Unified VM');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_unified_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_unified_profile', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_transfer_profile', 'COS Profiles-Transfer VM');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_transfer_profile', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_mail_transfer_profile', 'y', 'csr_supervisor', 'view');


-- to add entry for COS Profiles-Basic Package

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_basic_package', 'COS Profiles-Basic Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_basic', 'COS Profiles-Basic Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_basic_package', 'COS Profiles-Basic Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_basic_package', 'COS Profiles-Basic Package');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_basic', 'COS Profiles-Basic Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_basic', 'COS Profiles-Basic Package');

-- to add privilege entry for COS Profiles-Basic Package (  Edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_basic', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_basic_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_basic_package', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_basic', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_basic', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Basic Package (  View)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_basic', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_basic_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_basic_package', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_basic', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_basic', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Advanced Package

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_advanced_package', 'COS Profiles- Advanced Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_advanced', 'COS Profiles- Advanced Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_advanced_package', 'COS Profiles- Advanced Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_advanced_package', 'COS Profiles- Advanced Package');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_advanced', 'COS Profiles- Advanced Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_advanced', 'COS Profiles- Advanced Package');

-- to add privilege entry for COS Profiles-Advanced Package (  Edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_advanced', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_advanced_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_advanced_package', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_advanced', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_advanced', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Advanced Package (  View)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_advanced', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_advanced_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_advanced_package', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_advanced', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_advanced', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Complete Package
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_complete_package', 'Complete Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_complete_package', 'Complete Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_complete_package', 'Complete Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_complete', 'Complete Package');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_complete', 'Complete Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_complete', 'Complete Package');

-- to add privilege entry for COS Profiles-Complete Package (  Edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_complete_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_complete_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_complete', 'y', 'csr_supervisor', 'edit');


exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_complete', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_complete', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Complete Package ( view)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_complete_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_complete_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_complete', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_complete', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_complete', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Lobby Phone
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_lobby_phone', 'COS Profiles- Lobby Phone');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_lobby_phone', 'COS Profiles- Lobby Phone');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_lobby', 'COS Profiles- Lobby Phone');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_lobby_phone', 'COS Profiles- Lobby Phone');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_lobby_phone', 'COS Profiles- Lobby Phone');

-- to add privilege entry for COS Profiles-Lobby Phone ( edit)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_lobby_phone', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_lobby', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_lobby_phone', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_lobby_phone', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Lobby Phone ( view)


exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_lobby_phone', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_lobby', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_lobby_phone', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_lobby_phone', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Conference Room
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_conference_room', 'COS Profiles- Conference Room');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_conference_room', 'COS Profiles- Conference Room');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_conference_room', 'COS Profiles- Conference Room');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_conference_room', 'COS Profiles- Conference Room');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_conference_room', 'COS Profiles- Conference Room');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_conference_room', 'COS Profiles- Conference Room');


-- to add privilege entry for COS Profiles-Conference Room ( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_conference_room', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_conference_room', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_conference_room', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_conference_room', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_conference_room', 'y', 'csr_supervisor', 'edit');


-- to add privilege entry for COS Profiles-Conference Room (view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_conference_room', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_conference_room', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_conference_room', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_conference_room', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_conference_room', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Remote Worker

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_transient_worker', 'COS Profiles- Remote Worker');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_transient_worker', 'COS Profiles- Remote Worker');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_transient_worker', 'COS Profiles- Remote Worker');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_transient_worker', 'COS Profiles- Remote Worker');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_transient_worker', 'COS Profiles- Remote Worker');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_transient_worker', 'COS Profiles- Remote Worker');

-- to add privilege entry for COS Profiles-Remote Worker( edit)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_transient_worker', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_transient_worker', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_transient_worker', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_transient_worker', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_transient_worker', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Remote Worker( view)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_transient_worker', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_transient_worker', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_transient_worker', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_transient_worker', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_transient_worker', 'y', 'csr_supervisor', 'view');


-- to add entry for COS Profiles- Hosted IVR
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_hosted_ivr', 'COS Profiles- Hosted IVR');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_hosted_ivr', 'COS Profiles- Hosted IVR');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_hosted_ivr', 'COS Profiles- Hosted IVR');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_hosted_ivr', 'COS Profiles- Hosted IVR');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_hosted_ivr', 'COS Profiles- Hosted IVR');


-- to add privilege entry for COS Profiles-Hosted IVR ( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_hosted_ivr', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_hosted_ivr', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_hosted_ivr', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_hosted_ivr', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Hosted IVR ( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_hosted_ivr', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_hosted_ivr', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_hosted_ivr', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_hosted_ivr', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Fax Service
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_fax_service', 'COS Profiles- Fax Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_fax_service', 'COS Profiles- Fax Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_fax_service', 'COS Profiles- Fax Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_fax_service', 'COS Profiles- Fax Service');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_fax_service', 'COS Profiles- Fax Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_fax_service', 'COS Profiles- Fax Service');


-- to add privilege entry for COS Profiles-Fax Service ( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_fax_service', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_fax_service', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_fax_service', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_fax_service', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_fax_service', 'y', 'csr_supervisor', 'edit');


-- to add privilege entry for COS Profiles-Fax Service ( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_fax_service', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_fax_service', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_fax_service', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_fax_service', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_fax_service', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Auto Attendant
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_auto_attendant', 'COS Profiles- Auto Attendant');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_auto_attendant', 'COS Profiles- Auto Attendant');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_auto_attendant', 'COS Profiles- Auto Attendant');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_auto_attendant', 'COS Profiles- Auto Attendant');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_auto_attendant', 'COS Profiles- Auto Attendant');

-- to add privilege entry for COS Profiles-Auto Attendant ( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_auto_attendant', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_auto_attendant', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_auto_attendant', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_auto_attendant', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Auto Attendant (view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_auto_attendant', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_auto_attendant', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_auto_attendant', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_auto_attendant', 'y', 'csr_supervisor', 'view');


-- to add entry for COS Profiles- Key Features Package
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_key_features', 'COS Profiles- Key Features Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_key_features', 'COS Profiles- Key Features Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_key_features_package', 'COS Profiles- Key Features Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_key_features_package', 'COS Profiles- Key Features Package');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_key_features', 'COS Profiles- Key Features Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_key_features', 'COS Profiles- Key Features Package');


-- to add privilege entry for COS Profiles-Key Features Package ( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_key_features', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_key_features_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_key_features_package', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_key_features', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_key_features', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Key Features Package ( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_key_features', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_key_features_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_key_features_package', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_key_features', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_key_features', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Solutions Package

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_profile_solutions_package', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_solutions_package', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_solutions_package', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_solutions', 'COS Profiles- Solutions Package');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:pic_codes_profile', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_solutions', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_solutions', 'COS Profiles- Solutions Package');

-- to add privilege entry for COS Profiles-Solutions Package( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_solutions_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_solutions_package', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_solutions', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_solutions', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_solutions', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Solutions Package( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_solutions_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_solutions_package', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_solutions', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_solutions', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_solutions', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- Universal Package
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_universal_features', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_universal_features', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_universal_features', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_profile', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_universal_features', 'COS Profiles- Solutions Package');

-- to add privilege entry for COS Profiles-Universal Package( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_universal_features', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_universal_features', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_profile', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_universal_features', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_universal_features', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Universal Package( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_universal_features', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_universal_features', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_profile', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_universal_features', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_universal_features', 'y', 'csr_supervisor', 'view');

-- to add entry for COS Profiles- hunt_group_pilot Package

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_hunt_group_pilot', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_hunt_group_pilot', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_hunt_group_pilot', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan_profile', 'COS Profiles- Solutions Package');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cos_features_hunt_group_pilot', 'COS Profiles- Solutions Package');

-- to add privilege entry for COS Profiles-hunt_group_pilot Package( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_hunt_group_pilot', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_hunt_group_pilot', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_hunt_group_pilot', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_hunt_group_pilot', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for COS Profiles-Universal Package( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_definition_hunt_group_pilot', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_calling_plan_hunt_group_pilot', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan_hunt_group_pilot', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:cos_features_hunt_group_pilot', 'y', 'csr_supervisor', 'view');

-- to add entry for Voice Service
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_calling_card', 'Voice Services-calling_card');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_service', 'Voice Services-voice service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_toll_free', 'Voice Services-toll freee');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing', 'Voice Services-complex direcory listing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_identifier', 'Voice Services-complex directory listing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:listed_telephone_number', 'Voice Services-complex directory listing');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:directory_listing', 'Voice Services-directory listing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_service_class_of_service', 'Voice Services-voice service class of service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:primary_voice_line', 'Voice Services-primary voice line');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_feature_instances', 'Voice Services-voice service class of service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_class_of_service_association', 'Voice Services-voice service class of service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:composed_commercial_voice_mail', 'Voice Services-voice service class of service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_calling_plan_instances', 'Voice Services-voice service class of service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:a_la_carte_feature_instances', 'Voice Services-voice service class of service');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:pic_codes', 'Voice Services-calling plan instance');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:external_topology', 'Voice Services-calling plan instance');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan', 'Voice Services-calling plan instance');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan', 'Voice Services-calling plan instance');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:a_la_carte_voice_mail', 'Voice Services-voice mail');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_voice_mail', 'Voice Services-voice mail');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:unified_messaging', 'Voice Services-voice mail');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:primary_commercial_voice_mail', 'Voice Services-voice mail');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:transfer_commercial_voice_mail', 'Voice Services-voice mail');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:multiple_listing_group', 'Voice Services-complex directory listing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:listing_details', 'Voice Services-complex directory listing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:caption_slu_indent', 'Voice Services-complex directory listing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:listing_text', 'Voice Services-complex directory listing');

-- to add privilege entry for Voice Services( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_calling_card', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_service', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_toll_free', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_identifier', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listed_telephone_number', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directory_listing', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_service_class_of_service', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:primary_voice_line', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_feature_instances', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_class_of_service_association', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:composed_commercial_voice_mail', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_calling_plan_instances', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:a_la_carte_feature_instances', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:pic_codes', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:external_topology', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:a_la_carte_voice_mail', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_voice_mail', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:unified_messaging', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:primary_commercial_voice_mail', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:transfer_commercial_voice_mail', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:multiple_listing_group', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listing_details', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:caption_slu_indent', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listing_text', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for Voice Services( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_calling_card', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_service', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_toll_free', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_identifier', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listed_telephone_number', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:directory_listing', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:voice_service_class_of_service', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:primary_voice_line', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_feature_instances', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_class_of_service_association', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:composed_commercial_voice_mail', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_calling_plan_instances', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:a_la_carte_feature_instances', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:pic_codes', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:external_topology', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:outgoing_call_plan', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:incoming_call_plan', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:a_la_carte_voice_mail', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:class_of_service_voice_mail', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:unified_messaging', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:primary_commercial_voice_mail', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:transfer_commercial_voice_mail', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:multiple_listing_group', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listing_details', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:caption_slu_indent', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listing_text', 'y', 'csr_supervisor', 'view');

-- to add entry for CLEC Order
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_services', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:deactivate_voice_services', 'Clec Order-Deactivate Voice Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:clec_order_header', 'Clec Order-clec order header');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:activate_voice_services', 'Clec Order-activate voice service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_complex_directory_listing', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_address', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_atn', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_voice_line', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_order_header', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_association', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_listed_telephone_number', 'Clec Order-Change Services');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:add_complex_directory_listing', 'Clec Order-activate voice service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:add_voice_line', 'Clec Order-activate voice service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:activate_order_header', 'Clec Order-activate voice service');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:add_directory_listing', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_features', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_cnam', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:change_directory_listing', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:delete_directory_listing', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:telephone_number_association', 'Clec Order-calling_card');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:address_info', 'Clec Order-Change Services');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:clec_change_feature_info', 'Clec Order-Change Services');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:delete_voice_line', 'Clec Order-Deactivate Voice Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:deactivate_order_header', 'Clec Order-Deactivate Voice Service');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:delete_complex_directory_listing', 'Clec Order-Deactivate Voice Service');

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:listed_telephone_number_association', 'Clec Order-Change Services');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_association', 'Clec Order-Change Services');
-- to add privilege entry for CLEC Order( edit)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:clec_order_header', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_services', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deactivate_voice_services', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:activate_voice_services', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_complex_directory_listing','y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_address', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_atn', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_voice_line', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_order_header', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_association', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_listed_telephone_number', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:add_complex_directory_listing','y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:add_voice_line','y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:activate_order_header','y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:add_directory_listing', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_features', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_cnam', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_directory_listing', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:delete_directory_listing', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:telephone_number_association', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:address_info', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:clec_change_feature_info', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:delete_voice_line', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deactivate_order_header', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:delete_complex_directory_listing', 'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listed_telephone_number_association', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_association', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for CLEC Order( view)
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:clec_order_header', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deactivate_voice_services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:activate_voice_services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_complex_directory_listing','y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_address', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_atn', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_voice_line', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_order_header', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_association', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_listed_telephone_number', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:add_complex_directory_listing','y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:add_voice_line','y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:activate_order_header','y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:add_directory_listing', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_features', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_cnam', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:change_directory_listing', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:delete_directory_listing', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:telephone_number_association', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:address_info', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:clec_change_feature_info', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:delete_voice_line', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:deactivate_order_header', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:delete_complex_directory_listing', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:listed_telephone_number_association', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:complex_directory_listing_association', 'y', 'csr_supervisor', 'view');

-- to add entry for Voice Group
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:voice_groups', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_group', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:hunt_group', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_group_first_member', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_group_member', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_group_definition', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:hunt_group_definition', 'Voice Group');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:call_group_pilot', 'Voice Group');

-- to add privilege entry for Voice Group( edit)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_group_first_member', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_group_member', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_group_definition', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hunt_group_definition', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_group_pilot', 'y', 'csr_supervisor', 'edit');

-- to add privilege entry for Voice Group( view)

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_group_first_member', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_group_member', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_pickup_group_definition', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:hunt_group_definition', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:call_group_pilot', 'y', 'csr_supervisor', 'view');

----------------------------------------------------
-- AM config
----------------------------------------------------

-- Add subscriber status entry
exec am_add_secure_obj('samp.web.chgsubstatus.courtesy_block', 'Change subscriber status from courtesy_block');
exec am_add_secure_obj('samp.web.chgsubstatus.mso_block', 'Change subscriber status from mso_block');
exec am_add_secure_obj('samp.web.chgsubstatus.deleted', 'Change subscriber status from deleted');
exec am_add_secure_obj('samp.web.chgsubstatus.active', 'Change subscriber status from active');
exec am_add_secure_obj('samp.web.chgsubstatus.active.deleted', 'Change subscriber status from active to deleted');
exec am_add_secure_obj('samp.web.chgsubstatus.courtesy_block.deleted', 'Change subscriber status from courtesy block to deleted');

-- Exclude privileges for change subscriber status
--exec am_add_privilege('samp.web.chgsubstatus.mso_block', 'y', 'csr_supervisor', 'view');
--exec am_add_privilege('samp.web.chgsubstatus.active.deleted', 'y', 'csr_supervisor', 'view');
--exec am_add_privilege('samp.web.chgsubstatus.courtesy_block.deleted', 'y', 'csr_supervisor', 'view');


-- Exclude privileges for change subscriber status
--exec am_add_privilege('samp.web.chgsubstatus.mso_block', 'y', 'csr_supervisor', 'edit');
--exec am_add_privilege('samp.web.chgsubstatus.active.deleted', 'y', 'csr_supervisor', 'edit');
--exec am_add_privilege('samp.web.chgsubstatus.courtesy_block.deleted', 'y', 'csr_supervisor', 'edit');

-- Add voice services status entry
exec am_add_secure_obj('samp.web.chgsvcstatus.active.deleted.voice_services', 'Change voice services svc status from active to deleted');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.voice_services', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.voice_services', 'y', 'csr_supervisor', 'view');

-- Add clec orders and clec order entry
exec am_add_secure_obj('samp.web.chgsvcstatus.active.deleted.clec_order', 'Change clec_order svc status from active to deleted');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.clec_order', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.clec_order', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.chgsvcstatus.active.deleted.clec_orders', 'Change clec_orders svc status from active to deleted');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.clec_orders', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.clec_orders', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.chgsvcstatus.active.deleted.clec_services_information', 'Change clec_services_information svc status from active to deleted');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.clec_services_information', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.chgsvcstatus.active.deleted.clec_services_information', 'y', 'csr_supervisor', 'view');

--AM entry for consumable resource menu and form 
exec am_add_secure_obj('samp.web.svcparm.consumable.mnubar', 'Comsumable Resource Menu');
exec am_add_secure_obj('samp.web.svcparm.consumable.mnubar.addTNNative', 'Comsumable Resource Menu Item -- add TN Native');
exec am_add_secure_obj('samp.web.svcparm.consumable.mnubar.addTNPorted', 'Comsumable Resource Menu Item -- add TN Ported');
exec am_add_secure_obj('samp.web.svcparm.consumable.mnubar.addExt', 'Comsumable Resource Menu Item -- add Extension');
exec am_add_secure_obj('samp.web.svcparm.consumable.mnubar.delete', 'Comsumable Resource Menu Item -- delete');
exec am_add_secure_obj('samp.web.svcparm.consumable.mnubar.next', 'Comsumable Resource Menu Item -- next');
exec am_add_secure_obj('samp.web.svcparm.consumable.form', 'Comsumable Resource Form');

-- Exclude privileges for comsumable resource meanu (edit)
exec am_add_privilege('samp.web.svcparm.consumable.mnubar.addTNNative', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.consumable.mnubar.addTNPorted', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.consumable.mnubar.addExt', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.consumable.mnubar.delete', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.consumable.form', 'y', 'csr_supervisor', 'edit');

-- Add subscriber address entry
exec am_add_secure_obj('samp.web.svcparm.addressUI', 'Address');

exec am_add_privilege('samp.web.svcparm.addressUI', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.addressUI', 'n', 'csr_supervisor', 'view');

-- Add Customized voice services UI entry

exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist', 'Voice Services-voice line list');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.delete', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.copy', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.tn', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.atn', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.extension', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.portingtype', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.featurepackage', 'Voice Services-voice line list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons.addline', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons.linedetails', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons.featuredetails', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons.alacartfeatures', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons.voicemail', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tnlist.buttons.users', 'Voice Services-voice line list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.ccdlist', 'Voice Services-calling_card list');
exec am_add_secure_obj('samp.web.svcparm.voice_services.ccdlist.delete', 'Voice Services-calling_card list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.ccdlist.buttons', 'Voice Services-calling_card list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.ccdlist.buttons.addlisting', 'Voice Services-calling_card list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tflist', 'Voice Services-toll free list');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tflist.delete', 'Voice Services-toll free list column');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tflist.buttons', 'Voice Services-toll free list buttons');
exec am_add_secure_obj('samp.web.svcparm.voice_services.tflist.buttons.addlisting', 'Voice Services-toll free list buttons');


exec am_add_privilege('samp.web.svcparm.voice_services.tnlist.delete',  'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.voice_services.tnlist.copy',  'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.voice_services.tnlist.buttons.addline',  'y', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.svcparm.voice_services.tnlist.delete',  'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svcparm.voice_services.tnlist.copy',  'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svcparm.voice_services.tnlist.buttons.addline',  'y', 'csr_supervisor', 'view');

-- primary voice line paramenter : CName
exec am_add_secure_obj('samp.web.svcparm.custUI.voice_services.line_details.clid_name_combo', 'Primary Voice Line parameter: CName');
exec am_add_privilege('samp.web.svcparm.custUI.voice_services.line_details.clid_name_combo', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.custUI.voice_services.line_details.clid_name_combo', 'n', 'csr_supervisor', 'view');

--clec service infomation parameter : ATN 
exec am_add_secure_obj('samp.web.svcparm.clec_services_information.primary_account_telephone_number', 'Primary Voice Line parameter: CName');
exec am_add_privilege('samp.web.svcparm.clec_services_information.primary_account_telephone_number', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.clec_services_information.primary_account_telephone_number', 'n', 'csr_supervisor', 'view');

--calling plan feature instance  
exec am_add_secure_obj('samp.web.svcparm.custUI.featuresIns.pic_codes', 'class_of_service_calling_plan_instances.pic_codes');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.pic_codes', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.pic_codes', 'n', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.svcparm.custUI.featuresIns.incoming_call_plan', 'class_of_service_calling_plan_instances.incoming_call_plan');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.incoming_call_plan', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.incoming_call_plan', 'n', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.svcparm.custUI.featuresIns.outgoing_call_plan', 'class_of_service_calling_plan_instances.outgoing_call_plan');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.outgoing_call_plan', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.outgoing_call_plan', 'n', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.svcparm.custUI.featuresIns.external_topology', 'class_of_service_calling_plan_instances.external_topology');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.external_topology', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.custUI.featuresIns.external_topology', 'n', 'csr_supervisor', 'view');

-- to add entry for tn_association for Clec Detail page
exec am_add_secure_obj('samp.web.svcparm.telephone_number_association.assocFormElems.SubSvc.any_entity_type.clec_order_has_tn', 'Telephone Number Association');
exec am_add_privilege('samp.web.svcparm.telephone_number_association.assocFormElems.SubSvc.any_entity_type.clec_order_has_tn', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.telephone_number_association.assocFormElems.SubSvc.any_entity_type.clec_order_has_tn', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.orders.capture_header_parm.order_number', 'order_number');
exec am_add_privilege('samp.web.orders.capture_header_parm.order_number', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.submitted', 'submitted');
exec am_add_privilege('samp.web.orders.capture_header_parm.submitted', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.batch_sub_id', 'batch_sub_id');
exec am_add_privilege('samp.web.orders.capture_header_parm.batch_sub_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.canceled', 'canceled');
exec am_add_privilege('samp.web.orders.capture_header_parm.canceled', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.hold_ordr_flag', 'hold_ordr_flag');
exec am_add_privilege('samp.web.orders.capture_header_parm.hold_ordr_flag', 'y', 'csr_supervisor', 'view');

