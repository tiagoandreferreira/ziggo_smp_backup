CREATE UNIQUE INDEX ACCESS_CTRL_ENTRY_PK ON ACCESS_CTRL_ENTRY
(
       grp_nm                         ASC,
       secure_obj_nm                  ASC,
       permission_nm                  ASC
);

CREATE UNIQUE INDEX ACCESS_CTRL_ENTRY_IX1 ON ACCESS_CTRL_ENTRY
(
       secure_obj_nm                  ASC,
       grp_nm                         ASC,
       is_excluded                    ASC,
       permission_nm                  ASC
);

CREATE UNIQUE INDEX AM_PARM_PK ON AM_PARM
(
       parm_nm                        ASC
);

CREATE UNIQUE INDEX GRP_PK ON GRP
(
       grp_nm                         ASC
);

CREATE INDEX XIF352GRP ON GRP
(
       adm_secure_obj_nm              ASC
);

CREATE UNIQUE INDEX GRP_HIERARCHY_PK ON GRP_HIERARCHY
(
       parent_grp_nm                  ASC,
       member_grp_nm                  ASC
);

CREATE UNIQUE INDEX GRP_HIERARCHY_IDX1 ON GRP_HIERARCHY
(
       member_grp_nm                  ASC,
       parent_grp_nm                  ASC
);

CREATE UNIQUE INDEX GRP_PARM_PK ON GRP_PARM
(
       grp_nm                         ASC,
       parm_nm                        ASC
);

CREATE INDEX GRP_PARM_IX1 ON GRP_PARM
(
       parm_nm                        ASC,
       grp_nm                         ASC
);

CREATE UNIQUE INDEX GRP_USR_PK ON GRP_USR
(
       usr_nm                         ASC,
       grp_nm                         ASC
);

CREATE UNIQUE INDEX GRP_USR_IX1 ON GRP_USR
(
       grp_nm                         ASC,
       usr_nm                         ASC
);

CREATE UNIQUE INDEX PERMISSION_PK ON PERMISSION
(
       permission_nm                  ASC
);

CREATE UNIQUE INDEX REF_USER_STATUS_PK ON REF_USR_STATUS
(
       usr_status                     ASC
);

CREATE UNIQUE INDEX SECURE_OBJ_PK ON SECURE_OBJ
(
       secure_obj_nm                  ASC
);

CREATE UNIQUE INDEX REJECT_PWD_PK ON REJECT_PWD
(
       pwd                  ASC
);

CREATE UNIQUE INDEX USR_PK ON USR
(
       usr_nm                         ASC
);

CREATE INDEX XIF354USR ON USR
(
       usr_status                     ASC
);

CREATE UNIQUE INDEX USR_PARM_PK ON USR_PARM
(
       usr_nm                         ASC,
       parm_nm                        ASC
);

CREATE INDEX USR_PARM_IX1 ON USR_PARM
(
       parm_nm                        ASC,
       usr_nm                         ASC
);

