--============================================================================
--    $Id: am_jwf_load.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================



--Group
exec am_add_grp('mq_mgr', 'Manual Queue Group Users');
exec am_add_grp('mq_mgr1', 'Manual Queue Group Users');


--Group parms 
exec am_add_grp_parm('mq_mgr','service_provider_id', 'Liberate');
exec am_add_grp_parm('mq_mgr','new_sub_service_provider_id', 'Liberate');

exec am_add_grp_parm('mq_mgr1','service_provider_id', 'Liberate');
exec am_add_grp_parm('mq_mgr1','new_sub_service_provider_id', 'Liberate');

--Group priviledges
exec am_add_privilege('smp.manualqueue','n', 'mq_mgr', 'view');
exec am_add_privilege('smp.manualqueue','n', 'mq_mgr', 'edit');

exec am_add_privilege('smp.manualqueue','n', 'mq_mgr1', 'view');
exec am_add_privilege('smp.manualqueue','y', 'mq_mgr1', 'edit');

exec am_add_privilege('smp.inventory.WfProcess','n', 'csr_supervisor', 'view');
exec am_add_privilege('smp.inventory.WfProcess','n', 'csr_supervisor', 'execute');

exec am_add_privilege('smp.inventory.WfProcess','n', 'csr_admin', 'view');
exec am_add_privilege('smp.inventory.WfProcess','n', 'csr_admin', 'execute');

exec am_add_privilege('smp.inventory.WfProcess','n', 'csr', 'view');
exec am_add_privilege('smp.inventory.WfProcess','y', 'csr', 'execute');

--user 
exec am_create_user('mquser' , 'pwmquser', 'manual queue user');

exec am_create_user('mqread' , 'pwmqread', 'manual queue user');

--user parms 
exec am_add_user_parm('mquser', 'country', 'CA');
exec am_add_user_parm('mquser', 'language', 'en');

exec am_add_user_parm('mqread', 'country', 'CA');
exec am_add_user_parm('mqread', 'language', 'en');



--User group
exec am_add_grp_user('mquser' , 'mq_mgr');
exec am_add_grp_user('mquser' , 'csr_supervisor');

exec am_add_grp_user('mqread' , 'mq_mgr1');
exec am_add_grp_user('mqread' , 'csr_supervisor');


