--============================================================================
--    $Id: am_permissions.sql,v 1.8 2012/08/08 17:09:36 Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

--pending am permissions
exec am_add_secure_obj('samp.web.menu.ManualTask', 'ManualTask');
exec am_add_privilege('samp.web.menu.ManualTask', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.ManualTask', 'n', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.menu.admin', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.admin', 'n', 'csr_admin', 'edit');


exec am_add_secure_obj('samp.web.orders.actions.suspend', 'supsend button');
exec am_add_privilege('samp.web.orders.actions.suspend', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.actions.suspend', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.orders.actions.cancel', 'cancel button');
exec am_add_privilege('samp.web.orders.actions.cancel', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.actions.cancel', 'y', 'csr_admin', 'edit');

--exec am_add_grp_user('csra1', 'mq_mgr');
exec am_add_privilege('smp.manualqueue', 'n', 'csr_admin', 'view');
exec am_add_privilege('smp.manualqueue', 'n', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'csr_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'csr_admin', 'view');
commit;


---For disabeling first name,last name,subscriber id search option on smpweb 19 april 2012 
exec  am_add_secure_obj('smp.query.subbrief.SubNameSearch' , 'Order Subscriber key Search Labels');
exec  am_add_secure_obj('smp.query.order.OrderQueryBySubId' , 'Order key Search Labels');
exec  am_add_privilege('smp.query.subbrief.SubNameSearch', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SubNameSearch', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SubNameSearch', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SubNameSearch', 'y', 'stm.SuperUser', 'view');
exec  am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'stm.SuperUser', 'view');
commit;


---For disabeling first name,last name columns by search by IMSI option on subscriber tab on smpweb 22 july 2012

exec  am_add_secure_obj('smp.query.subbrief.SearchIMSI.last_name','IMSI Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.last_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.last_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.last_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.last_name', 'y', 'stm.SuperUser', 'view');
commit;

exec  am_add_secure_obj('smp.query.subbrief.SearchIMSI.first_name','IMSI Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.first_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.first_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.first_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchIMSI.first_name', 'y', 'stm.SuperUser', 'view');
commit;

--For disabeling first name,last name columns by search by account number option on subscriber tab on smpweb 20 august 2012
exec  am_add_secure_obj('smp.query.subbrief.SubAcctSearch.last_name','account number Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.last_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.last_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.last_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.last_name', 'y', 'stm.SuperUser', 'view');
commit;

exec  am_add_secure_obj('smp.query.subbrief.SubAcctSearch.first_name','account number Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.first_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.first_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.first_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SubAcctSearch.first_name', 'y', 'stm.SuperUser', 'view');
commit;

--For disabeling first name,last name columns by search by additional imsi option on subscriber tab on smpweb 20 august 2012

exec  am_add_secure_obj('smp.query.subbrief.SearchByAdntIMSI.last_name','additional IMSI Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.last_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.last_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.last_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.last_name', 'y', 'stm.SuperUser', 'view');
commit;

exec  am_add_secure_obj('smp.query.subbrief.SearchByAdntIMSI.first_name','additional IMSI Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.first_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.first_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.first_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchByAdntIMSI.first_name', 'y', 'stm.SuperUser', 'view');
commit;

--For disabeling first name,last name columns by search by msisdn option on subscriber tab on smpweb 20 august 2012
exec  am_add_secure_obj('smp.query.subbrief.SearchMSISDN.last_name','msisdn Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.last_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.last_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.last_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.last_name', 'y', 'stm.SuperUser', 'view');
commit;

exec  am_add_secure_obj('smp.query.subbrief.SearchMSISDN.first_name','msisdn Search Labels ');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.first_name', 'y', 'csr_admin', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.first_name', 'y', 'am_mgr', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.first_name', 'y', 'Administrators', 'view');
exec  am_add_privilege('smp.query.subbrief.SearchMSISDN.first_name', 'y', 'stm.SuperUser', 'view');
commit;

--voice service related parameters

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_abr_name' , 'abr bame');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr_name', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr_name', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr_name', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ain_group' , 'ain group');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ain_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ain_group', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ain_group', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ain_group', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_aindn' , 'aindn ');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_aul_dn' , 'aul dn');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn' , 'cfbl forward to dn');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfda_forward_to_dn' , 'cfda forward to dn');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_idp_voice_digits' , 'idp voice digits');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_voice_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_voice_digits', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_voice_digits', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_voice_digits', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_scf_forward_to_dn' , 'scf forward to dn');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_sippkg' , 'sw sippkg');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sippkg', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sippkg', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sippkg', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sippkg', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_spcall' , 'sw spcall');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_wml_dn' , 'sw wml dn');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_wml_timeout' , 'wml timeout');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_vmeadn' , 'vmeadn');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ar_per_use' , 'ar per use');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ac_per_use' , 'ac per use');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac_per_use', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_abr' , 'abr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_abr', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_acr' , 'acr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_acr_active' , 'acr active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_acr_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ar' , 'automatic recalling');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ar_per_use' , 'ar per use');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_aul' , 'automatic line');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aul', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_bcl' , 'basic call logs');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_bcl', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_bcl', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_bcl', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_bcl', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ccw' , 'cancel call waiting');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ccw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ccw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ccw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ccw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfbl' , 'call forward busy line');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfbl_active' , 'cfbl active indicator');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfbl_forward_to_always' , 'cfbl forward to always');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfbl_unrest' , 'cfbl unrest');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_unrest', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_unrest', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_unrest', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfbl_numcalls' , 'cfbl numcalls');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfda_active' , 'cfda active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.cfda_forward_to_always' , 'cfda forward to always');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_forward_to_always', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_forward_to_always', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_forward_to_always', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.cfda_ring_period' , 'cfda ring period');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_ring_period', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_ring_period', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.cfda_ring_period', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfda_unrest' , 'cfda unrest');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_unrest', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_unrest', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_unrest', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfda_numcalls' , 'cfda numcalls');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_numcalls', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_numcalls', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda_numcalls', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfv' , 'cfv');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfv_active' , 'cfv active indicator');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cidb' , 'cidb');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cidb_pub_anon' , 'cidb pub anon');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cidcw' , 'cidb call waiting');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidcw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cidcw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cnd' , 'cnd');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnd', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cot' , 'cot');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cot_per_use' , 'cot per use');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot_per_use', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cpcm' , 'calling manager');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cpcm', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cpcm', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cpcm', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cpcm', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cnam' , 'cname');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnam', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnam', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnam', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cnam', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cwr' , 'cwr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cwr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cwr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cwr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cwr', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cxr' , 'cwr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cxr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cxr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cxr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cxr', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_ac' , 'deny ac');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_ar' , 'deny ar');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_cidsdlv' , 'deny cidsdlv');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_cidssup' , 'deny cidssup');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidssup', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidssup', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidssup', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cidssup', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_cnb' , 'deny cnb');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cnb', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cnb', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cnb', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cnb', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_cot' , 'deny cot');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_cot', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_drcw' , 'deny drcw');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_drcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_drcw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_drcw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_sca' , 'deny sca');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_sca', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_sca', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_sca', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_scf' , 'deny scf');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scf', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scf', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scf', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_scrj' , 'deny scrj');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scrj', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scrj', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scrj', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_scrj', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_ucfv' , 'deny ucfv');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_deny_utwc' , 'deny utwc');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_dnd' , 'dnd');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_dnd_active' , 'dnd active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_dnd_dndgrp' , 'dnd dngrp');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_drcw' , 'drcw');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_drcw_active' , 'drcw active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_idp_rcc' , 'idp rcc');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_rcc', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_rcc', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_rcc', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_idp_rcc', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_int' , 'int');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_int', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_int', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_int', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_int', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_fani_digits' , 'fani digits');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani_digits', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani_digits', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani_digits', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_fani' , 'fani digits');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_fani', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lnr' , 'lnr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lnr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lnr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lnr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lnr', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr' , 'lsr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance' , 'lsr block domestic longdistance');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance' , 'lsr block domestic long 

distance');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call' , 'lsr block pay per call');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag' , 'lsr directory aflag'); 
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag' , 'lsr block toll free flag');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_lsr_pin' , 'lsr pin');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_lsr_pin', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_mct' , 'sw mct');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_mct', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_mct', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_mct', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_mct', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_msb' , 'sw msb');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_msb', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_msb', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_msb', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_msb', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ocaa' , 'ocaa');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ocaa', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ocaa', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ocaa', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ocaa', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_racf' , 'racf');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_racf_pin' , 'racf pin');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_rchd' , 'racf pin');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_rchd' , 'racf pin');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rchd', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_rmi' , 'rmi');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rmi', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rmi', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rmi', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_rmi', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_sca' , 'rmi');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_sca_active' , 'sca active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_sca_dndonly' , 'sca dndonly');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_scf' , 'scf');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_scf_active' , 'scf active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scf_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_scr' , 'scr');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_scr_active' , 'scr active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_scr_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_simrg' , 'sw simring');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_simrg_pin' , 'sw simring pin');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_pin', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_simrg_redirect' , 'sw simring redirect');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_redirect', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_redirect', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_redirect', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_simrg_redirect', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_spcall' , 'spcall');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcall', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_spcal2' , 'spcall 2');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcal2', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcal2', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcal2', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_spcal2', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_tdn' , 'spcall 2');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_tdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_tdn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_tdn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_tdn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_twc' , 'twc');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_twc', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_twc', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_twc', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_twc', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_vmwi' , 'vmsi');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmwi', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmwi', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmwi', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmwi', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_wml' , 'wml');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_wml_active' , 'wml active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_wml_active', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_vmeadn' , 'wml active');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_vm_ring_period' , 'vm ring period');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vm_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vm_ring_period', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vm_ring_period', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_vm_ring_period', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cfda' , 'cfda');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cfda', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cw' , 'cw');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cw', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_ac' , 'ac');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_ac', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_cot' , 'cot');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_cot', 'y', 'stm.SuperUser', 'view');
commit;




exec am_add_secure_obj('smp_switch_feature_pkg_std.sw_aindn_group' , 'aindn group');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn_group', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn_group', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature_pkg_std.sw_aindn_group', 'y', 'stm.SuperUser', 'view');
commit;



--voip service related parameters



exec am_add_secure_obj('smp_switch_feature.sw_abr_name' , 'abr bame');
exec am_add_privilege('smp_switch_feature.sw_abr_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_abr_name', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_abr_name', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_abr_name', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_abr_name' , 'abr bame');
exec am_add_privilege('voip_feature_pkg.sw_abr_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_abr_name', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_abr_name', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_abr_name', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_ain' , 'ain ');
exec am_add_privilege('smp_switch_feature.sw_ain', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ain', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ain', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ain', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ain' , 'ain ');
exec am_add_privilege('voip_feature_pkg.sw_ain', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ain', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ain', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ain', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_ain_group' , 'ain group');
exec am_add_privilege('smp_switch_feature.sw_ain_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ain_group', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ain_group', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ain_group', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ain_group' , 'ain group');
exec am_add_privilege('voip_feature_pkg.sw_ain_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ain_group', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ain_group', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ain_group', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_aindn' , 'aindn ');
exec am_add_privilege('smp_switch_feature.sw_aindn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_aindn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_aindn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_aindn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_aindn' , 'aindn ');
exec am_add_privilege('voip_feature_pkg.sw_aindn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aindn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aindn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aindn', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_aul_dn' , 'aul dn');
exec am_add_privilege('smp_switch_feature.sw_aul_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_aul_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_aul_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_aul_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_aul_dn' , 'aul dn');
exec am_add_privilege('voip_feature_pkg.sw_aul_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aul_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aul_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aul_dn', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfbl_forward_to_dn' , 'cfbl forward to dn');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfbl_forward_to_dn' , 'cfbl forward to dn');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_dn', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_cfda_forward_to_dn' , 'cfda forward to dn');
exec am_add_privilege('smp_switch_feature.sw_cfda_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_forward_to_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfda_forward_to_dn' , 'cfda forward to dn');
exec am_add_privilege('voip_feature_pkg.sw_cfda_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_forward_to_dn', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_idp_voice_digits' , 'idp voice digits');
exec am_add_privilege('smp_switch_feature.sw_idp_voice_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_voice_digits', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_voice_digits', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_voice_digits', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_idp_voice_digits' , 'idp voice digits');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice_digits', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice_digits', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice_digits', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_sippkg' , 'sw sippkg');
exec am_add_privilege('smp_switch_feature.sw_sippkg', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_sippkg', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_sippkg', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_sippkg', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_sippkg' , 'sw sippkg');
exec am_add_privilege('voip_feature_pkg.sw_sippkg', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sippkg', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sippkg', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sippkg', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_spcall' , 'sw spcall');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_spcall' , 'sw spcall');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_wml_dn' , 'sw wml dn');
exec am_add_privilege('smp_switch_feature.sw_wml_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_wml_dn' , 'sw wml dn');
exec am_add_privilege('voip_feature_pkg.sw_wml_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_dn', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_wml_timeout' , 'wml timeout');
exec am_add_privilege('smp_switch_feature.sw_wml_timeout', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_timeout', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_timeout', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_timeout', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_wml_timeout' , 'wml timeout');
exec am_add_privilege('voip_feature_pkg.sw_wml_timeout', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_timeout', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_timeout', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_timeout', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_vmeadn' , 'vmeadn');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_vmeadn' , 'vmeadn');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'stm.SuperUser', 'view');

commit;






exec am_add_secure_obj('smp_switch_feature.sw_scf_forward_to_dn' , 'scf forward to dn');
exec am_add_privilege('smp_switch_feature.sw_scf_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf_forward_to_dn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_scf_forward_to_dn' , 'scf forward to dn');
exec am_add_privilege('voip_feature_pkg.sw_scf_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf_forward_to_dn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf_forward_to_dn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf_forward_to_dn', 'y', 'stm.SuperUser', 'view');

commit;







exec am_add_secure_obj('smp_switch_feature.sw_abr' , 'abr');
exec am_add_privilege('smp_switch_feature.sw_abr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_abr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_abr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_abr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_abr' , 'abr');
exec am_add_privilege('voip_feature_pkg.sw_abr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_abr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_abr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_abr', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_acr' , 'acr');
exec am_add_privilege('smp_switch_feature.sw_acr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_acr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_acr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_acr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_acr' , 'acr');
exec am_add_privilege('voip_feature_pkg.sw_acr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_acr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_acr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_acr', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_acr_active' , 'acr active');
exec am_add_privilege('smp_switch_feature.sw_acr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_acr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_acr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_acr_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_acr_active' , 'acr active');
exec am_add_privilege('voip_feature_pkg.sw_acr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_acr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_acr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_acr_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_ar_per_use' , 'ar per use');
exec am_add_privilege('smp_switch_feature.sw_ar_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ar_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ar_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ar_per_use', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ar_per_use' , 'ar per use');
exec am_add_privilege('voip_feature_pkg.sw_ar_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ar_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ar_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ar_per_use', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_aul' , 'automatic line');
exec am_add_privilege('smp_switch_feature.sw_aul', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_aul', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_aul', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_aul', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_aul' , 'automatic line');
exec am_add_privilege('voip_feature_pkg.sw_aul', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aul', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aul', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aul', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_bcl' , 'basic call logs');
exec am_add_privilege('smp_switch_feature.sw_bcl', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_bcl', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_bcl', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_bcl', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_bcl' , 'basic call logs');
exec am_add_privilege('voip_feature_pkg.sw_bcl', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_bcl', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_bcl', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_bcl', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_ccw' , 'cancel call waiting');
exec am_add_privilege('smp_switch_feature.sw_ccw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ccw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ccw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ccw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ccw' , 'cancel call waiting');
exec am_add_privilege('voip_feature_pkg.sw_ccw', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ccw', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ccw', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ccw', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfbl' , 'call forward busy line');
exec am_add_privilege('smp_switch_feature.sw_cfbl', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfbl' , 'call forward busy line');
exec am_add_privilege('voip_feature_pkg.sw_cfbl', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_cfbl_active' , 'cfbl active indicator');
exec am_add_privilege('smp_switch_feature.sw_cfbl_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfbl_active' , 'cfbl active indicator');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_active', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_cfbl_forward_to_always' , 'cfbl forward to always');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_always', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_always', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_forward_to_always', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfbl_forward_to_always' , 'cfbl forward to always');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_always', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_always', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_forward_to_always', 'y', 'stm.SuperUser', 'view');

commit;












exec am_add_secure_obj('smp_switch_feature.sw_cfbl_unrest' , 'cfbl unrest');
exec am_add_privilege('smp_switch_feature.sw_cfbl_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_unrest', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_unrest', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_unrest', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfbl_unrest' , 'cfbl unrest');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_unrest', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_unrest', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_unrest', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfbl_numcalls' , 'cfbl numcalls');
exec am_add_privilege('smp_switch_feature.sw_cfbl_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_numcalls', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_numcalls', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfbl_numcalls', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfbl_numcalls' , 'cfbl numcalls');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_numcalls', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_numcalls', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfbl_numcalls', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfda_active' , 'cfda active');
exec am_add_privilege('smp_switch_feature.sw_cfda_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfda_active' , 'cfda active');
exec am_add_privilege('voip_feature_pkg.sw_cfda_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_active', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.cfda_forward_to_always' , 'cfda forward to always');
exec am_add_privilege('smp_switch_feature.cfda_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.cfda_forward_to_always', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.cfda_forward_to_always', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.cfda_forward_to_always', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.cfda_forward_to_always' , 'cfda forward to always');
exec am_add_privilege('voip_feature_pkg.cfda_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.cfda_forward_to_always', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.cfda_forward_to_always', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.cfda_forward_to_always', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_ac_per_use' , 'ac per use');
exec am_add_privilege('smp_switch_feature.sw_ac_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ac_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ac_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ac_per_use', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ac_per_use' , 'ac per use');
exec am_add_privilege('voip_feature_pkg.sw_ac_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ac_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ac_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ac_per_use', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.cfda_ring_period' , 'cfda ring period');
exec am_add_privilege('smp_switch_feature.cfda_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.cfda_ring_period', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.cfda_ring_period', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.cfda_ring_period', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.cfda_ring_period' , 'cfda ring period');
exec am_add_privilege('voip_feature_pkg.cfda_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.cfda_ring_period', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.cfda_ring_period', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.cfda_ring_period', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_ar' , 'auromatic recalling');
exec am_add_privilege('smp_switch_feature.sw_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ar', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ar', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ar', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ar' , 'automatic recalling');
exec am_add_privilege('voip_feature_pkg.sw_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ar', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ar', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ar', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfda_unrest' , 'cfda unrest');
exec am_add_privilege('smp_switch_feature.sw_cfda_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_unrest', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_unrest', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_unrest', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfda_unrest' , 'cfda unrest');
exec am_add_privilege('voip_feature_pkg.sw_cfda_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_unrest', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_unrest', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_unrest', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfda_numcalls' , 'cfda numcalls');
exec am_add_privilege('smp_switch_feature.sw_cfda_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_numcalls', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_numcalls', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda_numcalls', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfda_numcalls' , 'cfda numcalls');
exec am_add_privilege('voip_feature_pkg.sw_cfda_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_numcalls', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_numcalls', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda_numcalls', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cfv' , 'cfv');
exec am_add_privilege('smp_switch_feature.sw_cfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfv', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfv', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfv', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfv' , 'cfv');
exec am_add_privilege('voip_feature_pkg.sw_cfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfv', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfv', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfv', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_cfv_active' , 'cfv active indicator');
exec am_add_privilege('smp_switch_feature.sw_cfv_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfv_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfv_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfv_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfv_active' , 'cfv active indicator');
exec am_add_privilege('voip_feature_pkg.sw_cfv_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfv_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfv_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfv_active', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_cidb' , 'cidb');
exec am_add_privilege('smp_switch_feature.sw_cidb', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidb', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidb', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidb', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cidb' , 'cidb');
exec am_add_privilege('voip_feature_pkg.sw_cidb', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidb', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidb', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidb', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cidb_pub_anon' , 'cidb pub anon');
exec am_add_privilege('smp_switch_feature.sw_cidb_pub_anon', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidb_pub_anon', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidb_pub_anon', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidb_pub_anon', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cidb_pub_anon' , 'cidb pub anon');
exec am_add_privilege('voip_feature_pkg.sw_cidb_pub_anon', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidb_pub_anon', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidb_pub_anon', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidb_pub_anon', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cidcw' , 'cidb call waiting');
exec am_add_privilege('smp_switch_feature.sw_cidcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidcw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cidcw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cidcw' , 'cidb call waiting');
exec am_add_privilege('voip_feature_pkg.sw_cidcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidcw', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cidcw', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cnd' , 'cnd');
exec am_add_privilege('smp_switch_feature.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cnd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cnd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cnd', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cnd' , 'cnd');
exec am_add_privilege('voip_feature_pkg.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cnd', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cnd', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cnd', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_cnam' , 'cname');
exec am_add_privilege('smp_switch_feature.sw_cnam', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cnam', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cnam', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cnam', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cnam' , 'cname');
exec am_add_privilege('voip_feature_pkg.sw_cnam', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cnam', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cnam', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cnam', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_cwr' , 'cname');
exec am_add_privilege('smp_switch_feature.sw_cwr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cwr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cwr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cwr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cwr' , 'cname');
exec am_add_privilege('voip_feature_pkg.sw_cwr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cwr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cwr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cwr', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_cxr' , 'cxr');
exec am_add_privilege('smp_switch_feature.sw_cxr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cxr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cxr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cxr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cxr' , 'cxr');
exec am_add_privilege('voip_feature_pkg.sw_cxr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cxr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cxr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cxr', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_deny_ac' , 'deny ac');
exec am_add_privilege('smp_switch_feature.sw_deny_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ac', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ac', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ac', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_ac' , 'deny ac');
exec am_add_privilege('voip_feature_pkg.sw_deny_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ac', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ac', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ac', 'y', 'stm.SuperUser', 'view');

commit;
exec am_add_secure_obj('smp_switch_feature.sw_deny_ar' , 'deny ar');
exec am_add_privilege('smp_switch_feature.sw_deny_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ar', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ar', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ar', 'y', 'stm.SuperUser', 'view');
commit;




exec am_add_secure_obj('voip_feature_pkg.sw_deny_ar' , 'deny ar');
exec am_add_privilege('voip_feature_pkg.sw_deny_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ar', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ar', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ar', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_deny_cidsdlv' , 'deny cidsdlv');
exec am_add_privilege('smp_switch_feature.sw_deny_cidsdlv', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cidsdlv', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cidsdlv', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cidsdlv', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_cidsdlv' , 'deny cidsdlv');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidsdlv', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidsdlv', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidsdlv', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidsdlv', 'y', 'stm.SuperUser', 'view');

commit;
exec am_add_secure_obj('smp_switch_feature.sw_deny_cidssup' , 'deny cidssup');
exec am_add_privilege('smp_switch_feature.sw_deny_cidssup', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cidssup', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cidssup', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cidssup', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_cidssup' , 'deny cidssup');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidssup', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidssup', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidssup', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cidssup', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_deny_cnb' , 'deny cnb');
exec am_add_privilege('smp_switch_feature.sw_deny_cnb', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cnb', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cnb', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cnb', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_cnb' , 'deny cnb');
exec am_add_privilege('voip_feature_pkg.sw_deny_cnb', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cnb', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cnb', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cnb', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_deny_cot' , 'deny cot');
exec am_add_privilege('smp_switch_feature.sw_deny_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_cot', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_cot' , 'deny cot');
exec am_add_privilege('voip_feature_pkg.sw_deny_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_cot', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_deny_drcw' , 'deny drcw');
exec am_add_privilege('smp_switch_feature.sw_deny_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_drcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_drcw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_drcw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_drcw' , 'deny drcw');
exec am_add_privilege('voip_feature_pkg.sw_deny_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_drcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_drcw', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_drcw', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_deny_sca' , 'deny sca');
exec am_add_privilege('smp_switch_feature.sw_deny_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_sca', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_sca', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_sca', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_sca' , 'deny sca');
exec am_add_privilege('voip_feature_pkg.sw_deny_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_sca', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_sca', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_sca', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_deny_scf' , 'deny scf');
exec am_add_privilege('smp_switch_feature.sw_deny_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_scf', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_scf', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_scf', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_scf' , 'deny scf');
exec am_add_privilege('voip_feature_pkg.sw_deny_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_scf', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_scf', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_scf', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_deny_scrj' , 'deny scrj');
exec am_add_privilege('smp_switch_feature.sw_deny_scrj', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_scrj', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_scrj', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_scrj', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_scrj' , 'deny scrj');
exec am_add_privilege('voip_feature_pkg.sw_deny_scrj', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_scrj', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_scrj', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_scrj', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_deny_ucfv' , 'deny ucfv');
exec am_add_privilege('smp_switch_feature.sw_deny_ucfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ucfv', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ucfv', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_ucfv', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_ucfv' , 'deny ucfv');
exec am_add_privilege('voip_feature_pkg.sw_deny_ucfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ucfv', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ucfv', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_ucfv', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_deny_utwc' , 'deny utwc');
exec am_add_privilege('smp_switch_feature.sw_deny_utwc', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_utwc', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_utwc', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_deny_utwc', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_deny_utwc' , 'deny utwc');
exec am_add_privilege('voip_feature_pkg.sw_deny_utwc', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_utwc', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_utwc', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_deny_utwc', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_dnd' , 'dnd');
exec am_add_privilege('smp_switch_feature.sw_dnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_dnd' , 'dnd');
exec am_add_privilege('voip_feature_pkg.sw_dnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_dnd_active' , 'dnd active');
exec am_add_privilege('smp_switch_feature.sw_dnd_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_dnd_active' , 'dnd active');
exec am_add_privilege('voip_feature_pkg.sw_dnd_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_dnd_dndgrp' , 'dnd dndgrp');
exec am_add_privilege('smp_switch_feature.sw_dnd_dndgrp', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd_dndgrp', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd_dndgrp', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_dnd_dndgrp', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_dnd_dndgrp' , 'dnd dndgrp');
exec am_add_privilege('voip_feature_pkg.sw_dnd_dndgrp', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd_dndgrp', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd_dndgrp', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_dnd_dndgrp', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_drcw' , 'drcw');
exec am_add_privilege('smp_switch_feature.sw_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_drcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_drcw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_drcw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_drcw' , 'drcw');
exec am_add_privilege('voip_feature_pkg.sw_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_drcw', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_drcw', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_drcw', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_drcw_active' , 'drcw active');
exec am_add_privilege('smp_switch_feature.sw_drcw_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_drcw_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_drcw_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_drcw_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_drcw_active' , 'drcw active');
exec am_add_privilege('voip_feature_pkg.sw_drcw_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_drcw_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_drcw_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_drcw_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_idp_rcc' , 'idp rcc');
exec am_add_privilege('smp_switch_feature.sw_idp_rcc', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_rcc', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_rcc', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_rcc', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_idp_rcc' , 'idp rcc');
exec am_add_privilege('voip_feature_pkg.sw_idp_rcc', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_rcc', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_rcc', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_rcc', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_int' , 'idp rcc');
exec am_add_privilege('smp_switch_feature.sw_int', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_int', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_int', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_int', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('voip_feature_pkg.sw_int' , 'int');
exec am_add_privilege('voip_feature_pkg.sw_int', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_int', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_int', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_int', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_fani_digits' , 'fani digits');
exec am_add_privilege('smp_switch_feature.sw_fani_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_fani_digits', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_fani_digits', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_fani_digits', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('voip_feature_pkg.sw_fani_digits' , 'fani digits');
exec am_add_privilege('voip_feature_pkg.sw_fani_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_fani_digits', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_fani_digits', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_fani_digits', 'y', 'stm.SuperUser', 'view');

commit;





exec am_add_secure_obj('smp_switch_feature.sw_fani' , 'fani');
exec am_add_privilege('smp_switch_feature.sw_fani', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_fani', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_fani', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_fani', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('voip_feature_pkg.sw_fani' , 'fani');
exec am_add_privilege('voip_feature_pkg.sw_fani', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_fani', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_fani', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_fani', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_lnr' , 'lnr');
exec am_add_privilege('smp_switch_feature.sw_lnr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lnr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lnr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lnr', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('voip_feature_pkg.sw_lnr' , 'lnr');
exec am_add_privilege('voip_feature_pkg.sw_lnr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lnr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lnr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lnr', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_lsr' , 'lsr');
exec am_add_privilege('smp_switch_feature.sw_lsr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr' , 'lsr');
exec am_add_privilege('voip_feature_pkg.sw_lsr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_lsr_active' , 'lsr active');
exec am_add_privilege('smp_switch_feature.sw_lsr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_active' , 'lsr active');
exec am_add_privilege('voip_feature_pkg.sw_lsr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_lsr_block_domestic_long_distance' , 'lsr block domestic long distance');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_domestic_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_domestic_long_distance', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_domestic_long_distance', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_domestic_long_distance', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_block_domestic_long_distance' , 'lsr block domestic long distance');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_domestic_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_domestic_long_distance', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_domestic_long_distance', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_domestic_long_distance', 'y', 'stm.SuperUser', 'view');

commit;





exec am_add_secure_obj('smp_switch_feature.sw_lsr_block_international_long_distance' , 'lsr block international distance');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_international_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_international_long_distance', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_international_long_distance', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_international_long_distance', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_block_international_long_distance' , 'lsr block international longdistance');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_international_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_international_long_distance', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_international_long_distance', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_international_long_distance', 'y', 'stm.SuperUser', 'view');

commit;





exec am_add_secure_obj('smp_switch_feature.sw_lsr_block_pay_per_call' , 'lsr block pay per call');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_pay_per_call', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_pay_per_call', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_pay_per_call', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_pay_per_call', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_block_pay_per_call' , 'lsr block pay per call');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_pay_per_call', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_pay_per_call', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_pay_per_call', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_pay_per_call', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_lsr_block_directory_assistance_flag' , 'lsr block directory assistance flag');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_block_directory_assistance_flag' , 'lsr block directory assistance flag');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_directory_assistance_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_directory_assistance_flag', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_directory_assistance_flag', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_directory_assistance_flag', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_lsr_block_toll_free_flag' , 'lsr block toll free flag');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_toll_free_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_toll_free_flag', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_toll_free_flag', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_block_toll_free_flag', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_block_toll_free_flag' , 'lsr block toll free flag');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_toll_free_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_toll_free_flag', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_toll_free_flag', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_block_toll_free_flag', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_lsr_pin' , 'lsr pin');
exec am_add_privilege('smp_switch_feature.sw_lsr_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_lsr_pin', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_lsr_pin' , 'lsr pin');
exec am_add_privilege('voip_feature_pkg.sw_lsr_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_lsr_pin', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_mct' , 'sw mct');
exec am_add_privilege('smp_switch_feature.sw_mct', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_mct', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_mct', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_mct', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_mct' , 'sw mct');
exec am_add_privilege('voip_feature_pkg.sw_mct', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_mct', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_mct', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_mct', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_msb' , 'sw msb');
exec am_add_privilege('smp_switch_feature.sw_msb', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_msb', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_msb', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_msb', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_msb' , 'sw msb');
exec am_add_privilege('voip_feature_pkg.sw_msb', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_msb', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_msb', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_msb', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_ocaa' , 'ocaa');
exec am_add_privilege('smp_switch_feature.sw_ocaa', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ocaa', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ocaa', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ocaa', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ocaa' , 'ocaa');
exec am_add_privilege('voip_feature_pkg.sw_ocaa', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ocaa', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ocaa', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ocaa', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_racf' , 'racf');
exec am_add_privilege('smp_switch_feature.sw_racf', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_racf', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_racf', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_racf', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_racf' , 'racf');
exec am_add_privilege('voip_feature_pkg.sw_racf', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_racf', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_racf', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_racf', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_racf_pin' , 'racf pin');
exec am_add_privilege('smp_switch_feature.sw_racf_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_racf_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_racf_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_racf_pin', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_racf_pin' , 'racf pin');
exec am_add_privilege('voip_feature_pkg.sw_racf_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_racf_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_racf_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_racf_pin', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_rchd' , 'racf pin');
exec am_add_privilege('smp_switch_feature.sw_rchd', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_rchd', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_rchd', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_rchd', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_rchd' , 'racf pin');
exec am_add_privilege('voip_feature_pkg.sw_rchd', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rchd', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rchd', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rchd', 'y', 'stm.SuperUser', 'view');

commit;


exec am_add_secure_obj('smp_switch_feature.sw_rmi' , 'rmi');
exec am_add_privilege('smp_switch_feature.sw_rmi', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_rmi', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_rmi', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_rmi', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_rmi' , 'rmi');
exec am_add_privilege('voip_feature_pkg.sw_rmi', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rmi', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rmi', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rmi', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_sca' , 'sca');
exec am_add_privilege('smp_switch_feature.sw_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_sca' , 'sca');
exec am_add_privilege('voip_feature_pkg.sw_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_sca_active' , 'sca active');
exec am_add_privilege('smp_switch_feature.sw_sca_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_sca_active' , 'sca active');
exec am_add_privilege('voip_feature_pkg.sw_sca_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca_active', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_sca_dndonly' , 'sca dndonly');
exec am_add_privilege('smp_switch_feature.sw_sca_dndonly', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca_dndonly', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca_dndonly', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_sca_dndonly', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_sca_dndonly' , 'sca dndonly');
exec am_add_privilege('voip_feature_pkg.sw_sca_dndonly', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca_dndonly', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca_dndonly', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_sca_dndonly', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_scf' , 'scf');
exec am_add_privilege('smp_switch_feature.sw_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_scf' , 'scf');
exec am_add_privilege('voip_feature_pkg.sw_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_scf_active' , 'scf active');
exec am_add_privilege('smp_switch_feature.sw_scf_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_scf_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_scf_active' , 'scf active');
exec am_add_privilege('voip_feature_pkg.sw_scf_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scf_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_scr' , 'scr');
exec am_add_privilege('smp_switch_feature.sw_scr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_scr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_scr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_scr', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_scr' , 'scr');
exec am_add_privilege('voip_feature_pkg.sw_scr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scr', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_scr_active' , 'scr active');
exec am_add_privilege('smp_switch_feature.sw_scr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_scr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_scr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_scr_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_scr_active' , 'scr active');
exec am_add_privilege('voip_feature_pkg.sw_scr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scr_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scr_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_scr_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_simrg_pin' , 'scr pin');
exec am_add_privilege('smp_switch_feature.sw_simrg_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_simrg_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_simrg_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_simrg_pin', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_simrg_pin' , 'scr pin');
exec am_add_privilege('voip_feature_pkg.sw_simrg_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_simrg_pin', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_simrg_pin', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_simrg_pin', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_simrg_redirect' , 'simring redirect');
exec am_add_privilege('smp_switch_feature.sw_simrg_redirect', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_simrg_redirect', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_simrg_redirect', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_simrg_redirect', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_simrg_redirect' , 'simring redirect');
exec am_add_privilege('voip_feature_pkg.sw_simrg_redirect', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_simrg_redirect', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_simrg_redirect', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_simrg_redirect', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_spcall' , 'spcall');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcall', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_spcall' , 'spcall');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcall', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_spcal2' , 'spcal2');
exec am_add_privilege('smp_switch_feature.sw_spcal2', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcal2', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcal2', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_spcal2', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_spcal2' , 'spcal2');
exec am_add_privilege('voip_feature_pkg.sw_spcal2', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcal2', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcal2', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_spcal2', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_tdn' , 'tdn');
exec am_add_privilege('smp_switch_feature.sw_tdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_tdn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_tdn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_tdn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_tdn' , 'tdn');
exec am_add_privilege('voip_feature_pkg.sw_tdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_tdn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_tdn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_tdn', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_twc' , 'twc');
exec am_add_privilege('smp_switch_feature.sw_twc', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_twc', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_twc', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_twc', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_twc' , 'twc');
exec am_add_privilege('voip_feature_pkg.sw_twc', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_twc', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_twc', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_twc', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_vmwi' , 'vmwi');
exec am_add_privilege('smp_switch_feature.sw_vmwi', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmwi', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmwi', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmwi', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_vmwi' , 'vmwi');
exec am_add_privilege('voip_feature_pkg.sw_vmwi', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmwi', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmwi', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmwi', 'y', 'stm.SuperUser', 'view');

commit;





exec am_add_secure_obj('smp_switch_feature.sw_wml' , 'wml');
exec am_add_privilege('smp_switch_feature.sw_wml', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_wml' , 'wml');
exec am_add_privilege('voip_feature_pkg.sw_wml', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_wml_active' , 'wml active');
exec am_add_privilege('smp_switch_feature.sw_wml_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_wml_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_wml_active' , 'wml active');
exec am_add_privilege('voip_feature_pkg.sw_wml_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_wml_active', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_vmeadn' , 'vmeadn');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_vmeadn', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_vmeadn' , 'vmeadn');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vmeadn', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_vm_ring_period' , 'vm ring period');
exec am_add_privilege('smp_switch_feature.sw_vm_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_vm_ring_period', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_vm_ring_period', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_vm_ring_period', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_vm_ring_period' , 'vm ring period');
exec am_add_privilege('voip_feature_pkg.sw_vm_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vm_ring_period', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vm_ring_period', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vm_ring_period', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_cfda' , 'cfda');
exec am_add_privilege('smp_switch_feature.sw_cfda', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cfda', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cfda' , 'cfda');
exec am_add_privilege('voip_feature_pkg.sw_cfda', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cfda', 'y', 'stm.SuperUser', 'view');

commit;




exec am_add_secure_obj('smp_switch_feature.sw_cot_per_use' , 'cot per use');
exec am_add_privilege('smp_switch_feature.sw_cot_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cot_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cot_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cot_per_use', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cot_per_use' , 'cot per use');
exec am_add_privilege('voip_feature_pkg.sw_cot_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cot_per_use', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cot_per_use', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cot_per_use', 'y', 'stm.SuperUser', 'view');

commit;

exec am_add_secure_obj('smp_switch_feature.sw_cpcm' , 'cpcm');
exec am_add_privilege('smp_switch_feature.sw_cpcm', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cpcm', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cpcm', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cpcm', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cpcm' , 'cpcm');
exec am_add_privilege('voip_feature_pkg.sw_cpcm', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cpcm', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cpcm', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cpcm', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cw' , 'cw');
exec am_add_privilege('smp_switch_feature.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cw', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cw', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cw', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cw' , 'cw');
exec am_add_privilege('voip_feature_pkg.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cw', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cw', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cw', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_ac' , 'ac');
exec am_add_privilege('smp_switch_feature.sw_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_ac', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_ac', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_ac', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_ac' , 'ac');
exec am_add_privilege('voip_feature_pkg.sw_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ac', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ac', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_ac', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_cot' , 'cot');
exec am_add_privilege('smp_switch_feature.sw_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_cot', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_cot' , 'cot');
exec am_add_privilege('voip_feature_pkg.sw_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cot', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cot', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_cot', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_vm' , 'sw_vm');
exec am_add_privilege('smp_switch_feature.sw_vm', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_vm', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_vm', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_vm', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_vm' , 'sw_vm');
exec am_add_privilege('voip_feature_pkg.sw_vm', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vm', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vm', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_vm', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_rmi_active' , 'rmi active');
exec am_add_privilege('smp_switch_feature.sw_rmi_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_rmi_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_rmi_active', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_rmi_active', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_rmi_active' , 'rmi active');
exec am_add_privilege('voip_feature_pkg.sw_rmi_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rmi_active', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rmi_active', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_rmi_active', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_aindn_group' , 'rmi active);
exec am_add_privilege('smp_switch_feature.sw_aindn_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_aindn_group', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_aindn_group', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_aindn_group', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_aindn_group' , 'rmi active');
exec am_add_privilege('voip_feature_pkg.sw_aindn_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aindn_group', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aindn_group', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_aindn_group', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_feature.sw_idp_voice' , 'idp voice');
exec am_add_privilege('smp_switch_feature.sw_idp_voice', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_voice', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_voice', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_feature.sw_idp_voice', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('voip_feature_pkg.sw_idp_voice' , 'idp voice');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_feature_pkg.sw_idp_voice', 'y', 'stm.SuperUser', 'view');

commit;



exec am_add_secure_obj('smp_switch_dial_tone_access.primary_telephone_number' , 'primary telephone number');
exec am_add_privilege('smp_switch_dial_tone_access.primary_telephone_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.primary_telephone_number', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.primary_telephone_number', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.primary_telephone_number', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.primary_telephone_number' , 'telephone number');
exec am_add_privilege('voip_dial_tone.primary_telephone_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.primary_telephone_number', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.primary_telephone_number', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.primary_telephone_number', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.line_type' , 'line type');
exec am_add_privilege('smp_switch_dial_tone_access.line_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.line_type', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.line_type', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.line_type', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.line_type' , 'line type');
exec am_add_privilege('voip_dial_tone.line_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.line_type', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.line_type', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.line_type', 'y', 'stm.SuperUser', 'view');
commit;




exec am_add_secure_obj('smp_switch_dial_tone_access.sip_username' , 'sip username');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.sip_username' , 'sip username');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('smp_switch_dial_tone_access.lec_commitment_date' , 'lec commitment date');
exec am_add_privilege('smp_switch_dial_tone_access.lec_commitment_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.lec_commitment_date', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.lec_commitment_date', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.lec_commitment_date', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.lec_commitment_date' , 'lec commitment date');
exec am_add_privilege('voip_dial_tone.lec_commitment_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.lec_commitment_date', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.lec_commitment_date', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.lec_commitment_date', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.announce_time_zone' , 'announce time zone');
exec am_add_privilege('smp_switch_dial_tone_access.announce_time_zone', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.announce_time_zone', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.announce_time_zone', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.announce_time_zone', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.announce_time_zone' , 'announce time zone');
exec am_add_privilege('voip_dial_tone.announce_time_zone', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.announce_time_zone', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.announce_time_zone', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.announce_time_zone', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.inter_cic' , 'inter cic');
exec am_add_privilege('smp_switch_dial_tone_access.inter_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.inter_cic', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.inter_cic', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.inter_cic', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.inter_cic' , 'inter cic');
exec am_add_privilege('voip_dial_tone.inter_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.inter_cic', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.inter_cic', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.inter_cic', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.intra_cic' , 'inter cic');
exec am_add_privilege('smp_switch_dial_tone_access.intra_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.intra_cic', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.intra_cic', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.intra_cic', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.intra_cic' , 'inter cic');
exec am_add_privilege('voip_dial_tone.intra_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.intra_cic', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.intra_cic', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.intra_cic', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.intl_cic' , 'intl cic');
exec am_add_privilege('smp_switch_dial_tone_access.intl_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.intl_cic', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.intl_cic', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.intl_cic', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.intl_cic' , 'intl cic');
exec am_add_privilege('voip_dial_tone.intl_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.intl_cic', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.intl_cic', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.intl_cic', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.device_id' , 'device id');
exec am_add_privilege('smp_switch_dial_tone_access.device_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.device_id', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.device_id', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.device_id', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.device_id' , 'device id');
exec am_add_privilege('voip_dial_tone.device_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.device_id', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.device_id', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.device_id', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.port_number' , 'port number');
exec am_add_privilege('smp_switch_dial_tone_access.port_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.port_number', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.port_number', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.port_number', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.port_number' , 'port number');
exec am_add_privilege('voip_dial_tone.port_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.port_number', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.port_number', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.port_number', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.tn_change_counterpart' , 'tn change counterpart');
exec am_add_privilege('smp_switch_dial_tone_access.tn_change_counterpart', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.tn_change_counterpart', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.tn_change_counterpart', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.tn_change_counterpart', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.tn_change_counterpart' , 'tn change counterpart');
exec am_add_privilege('voip_dial_tone.tn_change_counterpart', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.tn_change_counterpart', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.tn_change_counterpart', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.tn_change_counterpart', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.switch_clli' , 'switch clli');
exec am_add_privilege('smp_switch_dial_tone_access.switch_clli', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.switch_clli', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.switch_clli', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.switch_clli', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.switch_clli' , 'switch clli');
exec am_add_privilege('voip_dial_tone.switch_clli', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.switch_clli', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.switch_clli', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.switch_clli', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.ltg' , 'ltg');
exec am_add_privilege('smp_switch_dial_tone_access.ltg', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.ltg', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.ltg', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.ltg', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.ltg' , 'ltg');
exec am_add_privilege('voip_dial_tone.ltg', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.ltg', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.ltg', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.ltg', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.lataname' , 'lataname');
exec am_add_privilege('smp_switch_dial_tone_access.lataname', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.lataname', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.lataname', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.lataname', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.lataname' , 'lataname');
exec am_add_privilege('voip_dial_tone.lataname', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.lataname', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.lataname', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.lataname', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.line_attr' , 'line attr');
exec am_add_privilege('smp_switch_dial_tone_access.line_attr', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.line_attr', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.line_attr', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.line_attr', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.line_attr' , 'line attr');
exec am_add_privilege('voip_dial_tone.line_attr', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.line_attr', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.line_attr', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.line_attr', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.rate_area' , 'rate area');
exec am_add_privilege('smp_switch_dial_tone_access.rate_area', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.rate_area', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.rate_area', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.rate_area', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.rate_area' , 'rate area');
exec am_add_privilege('voip_dial_tone.rate_area', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.rate_area', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.rate_area', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.rate_area', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.xlaplan' , 'xlaplan');
exec am_add_privilege('smp_switch_dial_tone_access.xlaplan', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.xlaplan', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.xlaplan', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.xlaplan', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.xlaplan' , 'xlaplan');
exec am_add_privilege('voip_dial_tone.xlaplan', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.xlaplan', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.xlaplan', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.xlaplan', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.gitn_npa' , 'gitn npa');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_npa', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_npa', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_npa', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_npa', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.gitn_npa' , 'gitn npa');
exec am_add_privilege('voip_dial_tone.gitn_npa', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.gitn_npa', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.gitn_npa', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.gitn_npa', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.gitn_nxx' , 'gitn nxx');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_nxx', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_nxx', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_nxx', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_nxx', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.gitn_nxx' , 'gitn nxx');
exec am_add_privilege('voip_dial_tone.gitn_nxx', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.gitn_nxx', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.gitn_nxx', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.gitn_nxx', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.gitn_state' , 'gitn state');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_state', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_state', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_state', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_state', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.gitn_state' , 'gitn state');
exec am_add_privilege('voip_dial_tone.gitn_state', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.gitn_state', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.gitn_state', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.gitn_state', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.gitn_zip' , 'gitn zip');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_zip', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_zip', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_zip', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.gitn_zip', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.gitn_zip' , 'gitn zip');
exec am_add_privilege('voip_dial_tone.gitn_zip', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.gitn_zip', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.gitn_zip', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.gitn_zip', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.uri_sip_id' , 'uri sip id');
exec am_add_privilege('smp_switch_dial_tone_access.uri_sip_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.uri_sip_id', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.uri_sip_id', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.uri_sip_id', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.uri_sip_id' , 'uri sip id');
exec am_add_privilege('voip_dial_tone.uri_sip_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.uri_sip_id', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.uri_sip_id', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.uri_sip_id', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.sip_password' , 'sip password');
exec am_add_privilege('smp_switch_dial_tone_access.sip_password', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_password', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_password', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_password', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.sip_password' , 'sip password');
exec am_add_privilege('voip_dial_tone.sip_password', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.sip_password', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.sip_password', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.sip_password', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.sip_username' , 'sip username');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_username', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.sip_username' , 'sip username');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.sip_username', 'y', 'stm.SuperUser', 'view');
commit;

exec am_add_secure_obj('smp_switch_dial_tone_access.len' , 'len');
exec am_add_privilege('smp_switch_dial_tone_access.len', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.len', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.len', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.len', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.len' , 'len');
exec am_add_privilege('voip_dial_tone.len', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.len', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.len', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.len', 'y', 'stm.SuperUser', 'view');
commit;


exec am_add_secure_obj('smp_switch_dial_tone_access.sip_subscriber_profile' , 'sip subscriber profile');
exec am_add_privilege('smp_switch_dial_tone_access.sip_subscriber_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_subscriber_profile', 'y', 'am_mgr', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_subscriber_profile', 'y', 'Administrators', 'view');
exec am_add_privilege('smp_switch_dial_tone_access.sip_subscriber_profile', 'y', 'stm.SuperUser', 'view');
commit;



exec am_add_secure_obj('voip_dial_tone.sip_subscriber_profile' , 'sip subscriber profile');
exec am_add_privilege('voip_dial_tone.sip_subscriber_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('voip_dial_tone.sip_subscriber_profile', 'y', 'am_mgr', 'view');
exec am_add_privilege('voip_dial_tone.sip_subscriber_profile', 'y', 'Administrators', 'view');
exec am_add_privilege('voip_dial_tone.sip_subscriber_profile', 'y', 'stm.SuperUser', 'view');
commit;

--For disabeling help,reports and batch tab from search tab on smp UI
execute am_add_secure_obj('samp.web.menu.search.help', 'help tab');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'csr_admin', 'view');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'csr_admin', 'edit');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'am_mgr', 'view');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'am_mgr', 'edit');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'Administrators', 'view');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'Administrators', 'edit');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'stm.SuperUser', 'view');
execute am_add_privilege('samp.web.menu.search.help', 'y', 'Administrators', 'edit');
commit;

execute am_add_secure_obj('samp.web.menu.search.batchorder', 'Batch Order Tab');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'csr_admin', 'view');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'csr_admin', 'edit');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'am_mgr', 'view');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'am_mgr', 'edit');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'Administrators', 'view');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'Administrators', 'edit');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'stm.SuperUser', 'view');
execute am_add_privilege('samp.web.menu.search.batchorder', 'y', 'stm.SuperUser', 'edit');
commit;

execute am_add_secure_obj('samp.web.menu.reports', 'reports tab');
execute am_add_privilege('samp.web.menu.reports', 'y', 'csr_admin', 'view');
execute am_add_privilege('samp.web.menu.reports', 'y', 'csr_admin', 'view');
execute am_add_privilege('samp.web.menu.reports', 'y', 'am_mgr', 'view');
execute am_add_privilege('samp.web.menu.reports', 'y', 'am_mgr', 'view');
execute am_add_privilege('samp.web.menu.reports', 'y', 'Administrators', 'edit');
execute  am_add_privilege('samp.web.menu.reports', 'y', 'Administrators', 'edit');
execute am_add_privilege('samp.web.menu.reports', 'y', 'stm.SuperUser', 'edit');
execute am_add_privilege('samp.web.menu.reports', 'y', 'stm.SuperUser', 'edit');
commit;

-- hiding first page displayed on UI
exec am_add_secure_obj('samp.web.entity.ConsumableResource', 'Consumable resource entity');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y', 'am_mgr', 'view');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y', 'am_mgr', 'edit');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y', 'Administrators', 'view');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y', 'Administrators', 'edit');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y',  'stm.SuperUser', 'view');
exec am_add_privilege('samp.web.entity.ConsumableResource', 'y',  'stm.SuperUser', 'edit');

-- hiding last page

exec am_add_secure_obj('samp.web.entity.SubUser', 'Desc'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_supervisor', 'view'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_supervisor', 'edit'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_admin', 'view'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'am_mgr', 'view'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'Administrators', 'view'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'stm.SuperUser', 'view'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_admin', 'edit'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'am_mgr', 'edit'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'Administrators', 'edit'); 
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'stm.SuperUser', 'edit'); 
commit; 

-- hide profile tab

exec am_add_secure_obj('samp.web.menu.profile', 'Profile Tab');
exec am_add_privilege('samp.web.menu.profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.profile', 'y', 'csr_admin', 'edit');
commit;

-- hiding View Log button
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_super_admin', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_super_admin', 'edit');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_tibco', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_tibco', 'edit'); 
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_tsr_ro', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_tsr_ro', 'edit');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_tsr', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'ziggo_tsr', 'edit');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'csr_admin', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'csr_admin', 'edit');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'am_mgr', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'am_mgr', 'edit');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'Administrators', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'Administrators', 'edit');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'stm.SuperUser', 'view');
execute am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'stm.SuperUser', 'edit');
commit;