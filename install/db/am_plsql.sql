---------------------------------------------------------------------------
-- FILE INFO
--    $Id: am_plsql.sql,v 1.12 2006/01/27 22:18:31 dant Exp $
--
-- DESCRIPTION
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE am_create_user(i_user_nm in varchar2, i_user_psw in varchar2, i_usr_desc in varchar2) is
    l_usr_nm usr.usr_nm%TYPE;
begin
    insert into usr (usr_nm, usr_pswd, usr_descr, usr_status, created_dtm, created_by)
        values (i_user_nm, i_user_psw, i_usr_desc, 'a', sysdate, user);
exception
when dup_val_on_index then
    raise_application_error(-20000, 'User ' || i_user_nm || ' already exists');
end;
/
CREATE OR REPLACE PROCEDURE am_delete_user(i_user_nm in varchar2) is
        l_usr_nm usr.usr_nm%TYPE;
begin
    -- make sure that user exists, if not raise an exception
    select usr_nm into l_usr_nm from usr where usr_nm=i_user_nm for update;
    delete from grp_usr where usr_nm=i_user_nm;
    delete from usr_parm where usr_nm=i_user_nm;
    delete from usr where usr_nm=i_user_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'User ' || i_user_nm || ' does not exist');
end;
/
CREATE OR REPLACE PROCEDURE am_add_grp_user(i_user_nm in varchar2, i_grp_nm in varchar2) is
    l_usr_nm usr.usr_nm%TYPE;
    l_grp_nm grp.GRP_NM%TYPE;
begin
    begin
        select usr_nm into l_usr_nm from usr where usr_nm=i_user_nm;
    exception
    when no_data_found then
        raise_application_error(-20000, 'User ' || i_user_nm || ' does not exist');
    end;

    begin
        select grp_nm into l_grp_nm from grp where grp_nm=i_grp_nm;
    exception
    when no_data_found then
        raise_application_error(-20000, 'Group ' || i_grp_nm || ' does not exist');
    end;
    insert into grp_usr (usr_nm, grp_nm, created_dtm, created_by)
        values (i_user_nm, i_grp_nm, sysdate, user);
                
exception
when DUP_VAL_ON_INDEX then
    raise_application_error(-20000, 'User ' || i_user_nm || ' is already member of group ' || i_grp_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_delete_grp_user(i_user_nm in varchar2, i_grp_nm in varchar2) is
    l_usr_nm usr.usr_nm%TYPE;
begin
    --make sure that user is member of this group, if not raise an exception
    select usr_nm into l_usr_nm from grp_usr
        where usr_nm=i_user_nm and grp_nm=i_grp_nm for update;
    delete from grp_usr where usr_nm=i_user_nm and grp_nm=i_grp_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'User ' || i_user_nm || ' is not member of group ' || i_grp_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_add_grp(i_grp_nm in varchar2, i_grp_desc in varchar2) is
begin
    insert into grp (grp_nm, grp_descr, created_dtm, created_by)
        values (i_grp_nm, i_grp_desc, sysdate, user);
exception
when dup_val_on_index then
        dbms_output.put_line('Warning: Group ' || i_grp_nm || ' already exists');
end;
/
CREATE OR REPLACE PROCEDURE am_delete_grp(i_grp_nm in varchar2) is
    l_grp_nm varchar2(1000);
begin
    --make sure that group exists, if not raise an exception
    select grp_nm into l_grp_nm from grp where grp_nm=i_grp_nm for update;
    delete from grp_parm where grp_nm=i_grp_nm;
    delete from grp_hierarchy where parent_grp_nm=i_grp_nm or member_grp_nm=i_grp_nm;
    delete from grp where grp_nm=i_grp_nm;
exception
when no_data_found then
        raise_application_error(-20000, 'Group ' || i_grp_nm || ' does not exist');
end;
/
CREATE OR REPLACE PROCEDURE am_add_permission(i_permission_nm in varchar2, i_permission_desc in varchar2) is
begin
    insert into permission (permission_nm, permission_descr, created_dtm, created_by)
        values (i_permission_nm, i_permission_desc, sysdate, user);
exception
when DUP_VAL_ON_INDEX then
    dbms_output.put_line('Warning: Permission ' || i_permission_nm || ' already exists');
end;
/
CREATE OR REPLACE PROCEDURE am_delete_permission(i_permission_nm in varchar2) is
    l_permission_nm permission.permission_nm%type;
begin
    --make sure that group exists, if not raise an exception
    select permission_nm into l_permission_nm from permission
        where permission_nm=i_permission_nm for update;
        delete from access_ctrl_entry where permission_nm=i_permission_nm;
    delete from permission where permission_nm=i_permission_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'Permission ' || i_permission_nm || ' does not exist');
end;
/
CREATE OR REPLACE PROCEDURE am_add_secure_obj(i_secure_obj_nm in varchar2, i_secure_obj_desc in varchar2) is
begin
    insert into secure_obj (secure_obj_nm, secure_obj_descr, created_dtm, created_by)
        values (i_secure_obj_nm, i_secure_obj_desc, sysdate, user);
exception
when DUP_VAL_ON_INDEX then
    dbms_output.put_line('Warning: Resource ' || i_secure_obj_nm || ' already exists');
end;
/
CREATE OR REPLACE PROCEDURE am_delete_secure_obj(i_secure_obj_nm in varchar2) is
    l_secure_obj_nm varchar2(1000);
begin
    --make sure that group exists, if not raise an exception
    select secure_obj_nm into l_secure_obj_nm from secure_obj
        where secure_obj_nm=i_secure_obj_nm for update;
    delete from secure_obj where secure_obj_nm=i_secure_obj_nm;
exception
when no_data_found then
    raise_application_error(-20000,'Resource ' || i_secure_obj_nm || ' does not exist');
end;
/
CREATE OR REPLACE PROCEDURE am_add_privilege(i_secure_obj_nm in varchar2, i_is_excluded char, i_grp_nm in varchar2, i_permission_nm in varchar2) is
begin
    insert into access_ctrl_entry (grp_nm, permission_nm, secure_obj_nm, is_excluded, created_dtm, created_by)
                  values (i_grp_nm, i_permission_nm, i_secure_obj_nm, i_is_excluded, sysdate, user);
exception
when dup_val_on_index then
    dbms_output.put_line('Warning: privilege already exists');
end;
/
CREATE OR REPLACE PROCEDURE am_delete_privilege(i_secure_obj_nm in varchar2, i_grp_nm in varchar2, i_permission_nm in varchar2) is
    l_grp_nm grp.grp_nm%type;
begin
    --make sure that user has this parm, if not raise an exception
    select distinct grp_nm into l_grp_nm from access_ctrl_entry
        where grp_nm=i_grp_nm and permission_nm=i_permission_nm and secure_obj_nm=i_secure_obj_nm;
    delete from access_ctrl_entry
        where grp_nm=i_grp_nm and permission_nm=i_permission_nm and secure_obj_nm=i_secure_obj_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'This privilege does not exist');
end;
/
CREATE OR REPLACE PROCEDURE am_add_user_parm(i_user_nm in varchar2, i_parm_nm in varchar2, i_parm_val in varchar2) is
begin
    insert into usr_parm (usr_nm, parm_nm, val, created_dtm, created_by)
                  values (i_user_nm, i_parm_nm, i_parm_val, sysdate, user);
exception
when dup_val_on_index then
    raise_application_error(-20000, 'User ' || i_user_nm || ' already has parameter ' || i_parm_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_delete_user_parm(i_user_nm in varchar2, i_parm_nm in varchar2) is
    l_usr_nm varchar2(1000);
begin
    --make sure that user has this parm, if not raise an exception
    select usr_nm into l_usr_nm from usr_parm
        where usr_nm=i_user_nm and parm_nm=i_parm_nm for update;
    delete from usr_parm
        where usr_nm=i_user_nm and parm_nm=i_parm_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'User ' || i_user_nm || ' does not have parameter ' || i_parm_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_add_parm(i_parm_nm in varchar2, i_parm_desc in varchar2) is
begin
    insert into am_parm (parm_nm, parm_descr, created_dtm, created_by)
        values (i_parm_nm, i_parm_desc, sysdate, user);
exception
when DUP_VAL_ON_INDEX then
    dbms_output.put_line('Warning: Parameter ' || i_parm_nm || ' already exists');
end;
/
CREATE OR REPLACE PROCEDURE am_delete_parm(i_parm_nm in varchar2) is
    l_parm_nm varchar2(1000);
begin
    --make sure that parm exists, if not raise an exception
    select parm_nm into l_parm_nm from am_parm
        where parm_nm=i_parm_nm for update;
    delete from am_parm where parm_nm=i_parm_nm;
exception
when no_data_found then
    raise_application_error(-20000,'Parameter ' || i_parm_nm || ' does not exist');
end;
/
CREATE OR REPLACE PROCEDURE am_add_grp_parm(i_grp_nm in varchar2, i_parm_nm in varchar2, i_parm_val in varchar2) is
begin
    insert into grp_parm (grp_nm, parm_nm, val, created_dtm, created_by)
                  values (i_grp_nm, i_parm_nm, i_parm_val, sysdate, user);
exception
when dup_val_on_index then
    raise_application_error(-20000, 'Group ' || i_grp_nm || ' already has parameter ' || i_parm_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_delete_grp_parm(i_grp_nm in varchar2, i_parm_nm in varchar2) is
    l_grp_nm varchar2(1000);
begin
    --make sure that group has this parm, if not raise an exception
    select grp_nm into l_grp_nm from grp_parm
        where grp_nm=i_grp_nm and parm_nm=i_parm_nm for update;
    delete from grp_parm
        where grp_nm=i_grp_nm and parm_nm=i_parm_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'Group ' || i_grp_nm || ' does not have parameter ' || i_parm_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_add_grp_grp(i_mgrp_nm in varchar2, i_grp_nm in varchar2) is
    l_grp_nm grp.GRP_NM%TYPE;
begin
    begin
        select grp_nm into l_grp_nm from grp where grp_nm=i_mgrp_nm;
    exception
    when no_data_found then
        raise_application_error(-20000, 'Group ' || i_mgrp_nm || ' does not exist');
    end;

    begin
        select grp_nm into l_grp_nm from grp where grp_nm=i_grp_nm;
    exception
    when no_data_found then
        raise_application_error(-20000, 'Group ' || i_grp_nm || ' does not exist');
    end;
    insert into grp_hierarchy (parent_grp_nm, member_grp_nm, created_dtm, created_by)
        values (i_grp_nm, i_mgrp_nm, sysdate, user);
                
exception
when DUP_VAL_ON_INDEX then
    raise_application_error(-20000, 'Group ' || i_mgrp_nm || ' is already member of group ' || i_grp_nm);
end;
/
CREATE OR REPLACE PROCEDURE am_delete_grp_grp(i_mgrp_nm in varchar2, i_grp_nm in varchar2) is
    l_mgrp_nm grp.grp_nm%TYPE;
begin
    --make sure that mgrp is member of this group, if not raise an exception
    select member_grp_nm into l_mgrp_nm from grp_hierarchy
        where member_grp_nm=i_mgrp_nm and parent_grp_nm=i_grp_nm for update;
    delete from grp_hierarchy 
        where member_grp_nm=i_mgrp_nm and parent_grp_nm=i_grp_nm;
exception
when no_data_found then
    raise_application_error(-20000, 'Group ' || i_mgrp_nm || ' is not member of group ' || i_grp_nm);
end;
/
CREATE OR REplace FUNCTION authenticate(username IN VARCHAR2,
                                        password IN VARCHAR2,
										passwordReal IN VARCHAR2) 
RETURN char
AS LANGUAGE JAVA
NAME 'com.sigma.samp.util.sql.dba.Authentication.authenticate(java.lang.String,
	                             java.lang.String,
								 java.lang.String) 
      return java.lang.String';
/
CREATE OR REplace FUNCTION am_authenticate_user(username IN VARCHAR2,
                                        password IN VARCHAR2) 
RETURN char is
  passwordReal varchar2(255);
  status varchar2(30);
begin
	 select usr_status, usr_pswd into status, passwordReal from usr where usr_nm = username;
	 if status != 'a' then
	   return 'd';
	 end if;
  	 return authenticate(username,password,passwordReal);
exception
  when others then
    return 'f';	 
end;
/

