--============================================================================
--    $Id: am_regression_load.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
--security object 
exec am_add_secure_obj('smp.regression.Regression' , 'Default resource for regression test');
--regression permissions
exec am_add_privilege('smp.regression.Regression','n', 'csr_admin', 'view');
exec am_add_privilege('smp.regression.Regression','n', 'csr_admin', 'execute');
exec am_add_privilege('smp.regression.Regression','n', 'csr_admin', 'create');
