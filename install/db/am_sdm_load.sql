--============================================================================
--    $Id: am_sdm_load.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

exec am_create_user('scmcsr' , 'pwscmcsr', 'SCM Sales Representative Users');
exec am_create_user('scmprodmgr' , 'pwscmprodmgr', 'SCM Product Manager Users');
exec am_create_user('scmsigma' , 'pwscmsigma', 'SCM Sigma Users');
exec am_create_user('scmadmin', 'pwscmadmin', 'SCM  Admin Users');

exec am_add_grp_user('scmcsr' , 'scm_csr');
exec am_add_grp_user('scmprodmgr' , 'scm_mgr');
exec am_add_grp_user('scmsigma' , 'scm_sigma');
exec am_add_grp_user('scmadmin', 'scm_admin');

