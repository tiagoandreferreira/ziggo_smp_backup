--============================================================================
--    $Id: am_spm_load.sql,v 1.2 2011/12/27 15:25:05 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================



exec am_add_secure_obj('samp.web.menu.subscriber.add_edit', 'menu');
exec am_add_secure_obj('samp.web.menu.subscriber.new_sub', 'menu');
exec am_add_secure_obj('samp.web.menu.subscriber.actions', 'menu');
exec am_add_secure_obj('samp.web.menu.subscriber.svc_migrate', 'menu');
exec am_add_secure_obj('samp.web.menu.subscriber.view_cart', 'menu');
exec am_add_secure_obj('samp.web.menu.manual_task_search', 'menu');
exec am_add_secure_obj('samp.web.menu.external', 'menu');

exec am_add_parm('new_sub_service_provider_id' , 'The service provider for new subscribers created by a user/group');
exec am_add_parm('service_provider_id' , 'The service provider for subscribers created by a user/group');
exec am_add_parm('access_scope' , 'The root service visible to user in the tree');

exec am_add_permission('edit' , 'Allows the user to edit the resources in SPMWEB');

--============================================================================
-- Create resources (secure objects) for spmweb
--============================================================================
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'Service samp_sub in the tree');

exec am_add_secure_obj('samp.web.chgsvcstatus.courtesy_block', 'Change service status from courtesy_block');
exec am_add_secure_obj('samp.web.chgsvcstatus.mso_block', 'Change service status from mso_block');
exec am_add_secure_obj('samp.web.chgsvcstatus.deleted', 'Change service status from deleted');
exec am_add_secure_obj('samp.web.chgsvcstatus.active', 'Change service status from active');
exec am_add_secure_obj('samp.web.chgsvcstatus.active.mso_block', 'Change svc status from active to mso block');
exec am_add_secure_obj('samp.web.chgsvcstatus.active.mso_block.samp_access', 'Change svc status from active to mso block');
exec am_add_secure_obj('samp.web.chgsvcstatus.active.courtesy_block.samp_access', 'Change svc status from active to courtesy block');
exec am_add_secure_obj('samp.web.chgsvcstatus.active.deleted.samp_access', 'Change svc status from active to deleted');
exec am_add_secure_obj('samp.web.chgsvcstatus.courtesy_block.active.samp_access', 'Change svc status from courtesy block to active');
exec am_add_secure_obj('samp.web.chgsvcstatus.courtesy_block.mso_block', 'Change svc status from courtesy block to mso block');
exec am_add_secure_obj('samp.web.chgsvcstatus.courtesy_block.deleted.samp_access', 'Change svc status from courtesy block to deleted');

exec am_add_secure_obj('samp.web.chgsubstatus.courtesy_block', 'Change subscriber status from courtesy_block');
exec am_add_secure_obj('samp.web.chgsubstatus.mso_block', 'Change subscriber status from mso_block');
exec am_add_secure_obj('samp.web.chgsubstatus.deleted', 'Change subscriber status from deleted');
exec am_add_secure_obj('samp.web.chgsubstatus.active', 'Change subscriber status from active');
exec am_add_secure_obj('samp.web.chgsubstatus.active.deleted', 'Change subscriber status from active to deleted');
exec am_add_secure_obj('samp.web.chgsubstatus.courtesy_block.deleted', 'Change subscriber status from courtesy block to deleted');

exec am_add_secure_obj('samp.web.menu.services', 'Menu Services');
exec am_add_secure_obj('samp.web.menu.subscriber', 'Menu subscriber');
exec am_add_secure_obj('samp.web.menu.subscriber.view', 'Menu Subscriber-View');
exec am_add_secure_obj('samp.web.menu.subscriber.edit', 'Menu Subscriber-Edit');
exec am_add_secure_obj('samp.web.menu.subscriber.add', 'Menu Add');
exec am_add_secure_obj('samp.web.menu.subscriber.actions', 'Menu Actions');

exec am_add_secure_obj('samp.web.menu.services.view', 'Menu Services-View');
exec am_add_secure_obj('samp.web.menu.services.add_edit', 'Menu Services-Add/Edit');
exec am_add_secure_obj('samp.web.menu.services.change_status', 'Menu Services-Change Status');
exec am_add_secure_obj('samp.web.menu.services.actions', 'Actions');
exec am_add_secure_obj('samp.web.menu.services.svc_migrate', 'Migrations');
exec am_add_secure_obj('samp.web.menu.app_cfg', 'Application Configuration');
exec am_add_secure_obj('samp.web.menu.admin', 'Access Control');
exec am_add_secure_obj('samp.web.menu.profile', 'Profile Menu Tab');
exec am_add_secure_obj('samp.web.menu.external', 'External Menu Tab');
exec am_add_secure_obj('samp.web.menu.ManualTask', 'Manual Task Menu Tab');

exec am_add_secure_obj('samp.web.orders.queued_order_detail', 'View Queued Order Details');
exec am_add_secure_obj('samp.web.orders.actions', 'Actions on orders');
exec am_add_secure_obj('samp.web.orders.actions.edit', 'Edit an order');
exec am_add_secure_obj('samp.web.orders.line_items.actions', 'Actions on line items');

exec am_add_secure_obj('samp.web.view_cart', 'View Cart item in menu frame');
exec am_add_secure_obj('samp.web.view_cart.analyze_order', 'Analyze order button in the view cart page');

exec am_add_secure_obj('samp.web.subparm.acct', 'Subscriber aparameter: account');
exec am_add_secure_obj('samp.web.subparm.sub_type', 'Subscriber aparameter: sub_type');
exec am_add_secure_obj('samp.web.subparm.contacts.primary.address.type', 'Subscriber address parameter: contacts.primary.address.type');
exec am_add_secure_obj('samp.web.subparm.contacts.primary.address.pref_nm', 'Subscriber address parameter: contacts.primary.address.pref_nm');

exec am_add_secure_obj('samp.web.orders.capture_header_parm.debug_order', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.has_groups', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.pre_processor_info', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.pre_processor_jndiname', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.sub_version', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.user_id', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.description', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.priority', '');

--============================================================================
-- Create users
--============================================================================
exec am_create_user('csrs1', 'pwcsrs1', 'Canadian Customer Sales Representative supervisor');
exec am_create_user('jpcsr', 'jpcsr', 'Japanese Customer Sales Representative');
exec am_create_user('zhcsr', 'zhcsr', 'Chinese Customer Sales Representative');
exec am_create_user('frcsr', 'frcsr', 'French Customer Sales Representative');
exec am_create_user('tstsub1', 'tstsub1', 'Subscriber used to test selfcare');
exec am_create_user('selfprov_admin', 'selfprov', 'User that execute selfprov actions');
exec am_create_user('csra1', 'pwcsra1', 'Canadian Customer Sales Representative Administrator');
exec am_create_user('csrv1', 'pwcsrv1', 'Canadian Customer Sales Representative Administrator');


exec am_add_user_parm('csrs1', 'country', 'CA');
exec am_add_user_parm('csrs1', 'language', 'en');
exec am_add_user_parm('jpcsr', 'country', 'JP');
exec am_add_user_parm('jpcsr', 'language', 'ja');
exec am_add_user_parm('zhcsr', 'country', 'CN');
exec am_add_user_parm('zhcsr', 'language', 'zh');
exec am_add_user_parm('frcsr', 'country', 'CA');
exec am_add_user_parm('frcsr', 'language', 'fr');

exec am_add_user_parm('selfprov_admin', 'country', 'CA');
exec am_add_user_parm('selfprov_admin', 'language', 'en');

exec am_add_user_parm('tstsub1', 'country', 'CA');
exec am_add_user_parm('tstsub1', 'language', 'en');
exec am_add_user_parm('tstsub1', 'access_scope', '0');
exec am_add_user_parm('csra1', 'country', 'CA');
exec am_add_user_parm('csra1', 'language', 'en');
-- exec am_add_user_parm('csra1', 'max_pwd_age_days', '5');
exec am_add_user_parm('csrv1', 'country', 'CA');
exec am_add_user_parm('csrv1', 'language', 'en');

exec am_add_grp_user('csrs1', 'csr_supervisor');
exec am_add_grp_user('csrs1', 'Administrators');
exec am_add_grp_user('jpcsr', 'csr_supervisor');
exec am_add_grp_user('jpcsr', 'Administrators');
exec am_add_grp_user('zhcsr', 'csr_supervisor');
exec am_add_grp_user('zhcsr', 'Administrators');
exec am_add_grp_user('frcsr', 'csr_supervisor');
exec am_add_grp_user('frcsr', 'Administrators');
exec am_add_grp_user('selfprov_admin', 'csr');

exec am_add_grp_user('tstsub1', 'sub');
exec am_add_grp_user('csra1', 'csr_admin');
exec am_add_grp_user('csra1', 'Administrators');
exec am_add_grp_user('csrv1', 'csr_view');
exec am_add_grp_user('csrv1', 'Administrators');

exec am_add_grp_parm('csr_supervisor', 'new_sub_service_provider_id', 'Liberate');
exec am_add_grp_parm('csr_supervisor', 'service_provider_id', 'Liberate');
exec am_add_grp_parm('csr', 'new_sub_service_provider_id', 'Liberate');
exec am_add_grp_parm('csr', 'service_provider_id', 'Liberate');
exec am_add_grp_parm('sub', 'new_sub_service_provider_id', 'Liberate');
exec am_add_grp_parm('sub', 'service_provider_id', 'Liberate');
exec am_add_grp_parm('csr_admin', 'new_sub_service_provider_id', 'Liberate');
exec am_add_grp_parm('csr_admin', 'service_provider_id', 'Liberate');
-- exec am_add_grp_parm('csr_admin', 'max_pwd_age_days', '30');
exec am_add_grp_parm('csr_view', 'new_sub_service_provider_id', 'Liberate');
exec am_add_grp_parm('csr_view', 'service_provider_id', 'Liberate');

--============================================================================
-- Configure privileges
--============================================================================

-- buttons on batch order results page
exec am_add_privilege('samp.web.batch', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.admin_batch.release_ownership', 'n', 'csr_admin', 'view');

exec am_add_privilege('samp.web.chgstatus', 'n', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.menu', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.menu.admin', 'y', 'csr', 'view');
exec am_add_privilege('samp.web.view_cart', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.view_cart.analyze_order', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.view_cart', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.view_cart.analyze_order', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.view_cart', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.view_cart.analyze_order', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.view_cart', 'n', 'sub', 'view');
exec am_add_privilege('samp.web.view_cart.analyze_order', 'n', 'sub', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'sub', 'view');
exec am_add_privilege('samp.web.orders.queued_order_detail', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.queued_order_detail', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.actions', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.actions.edit', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.actions.replicate', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.actions.repair', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.line_items.actions', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.batch', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders', 'n', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.entity', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.entity', 'n', 'csr', 'edit');
exec am_add_privilege('samp.web.entity', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity', 'n', 'csr_supervisor', 'edit');

exec am_add_privilege('samp.web.svc', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.svc', 'n', 'csr', 'edit');
exec am_add_privilege('samp.web.inactive_svc', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.inactive_svc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm', 'n', 'csr', 'edit');
exec am_add_privilege('samp.web.subparm', 'n', 'csr', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'csr', 'view');

exec am_add_privilege('samp.web.exception', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.exception', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.menu.admin', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.menu.admin', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svc', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svc', 'n', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.inactive_svc', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.inactive_svc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.subparm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svcparm', 'n', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.subparm', 'n', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.menu.external', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.ManualTask', 'y', 'csr_admin', 'view');

exec am_add_privilege('samp.web.chgsvcstatus', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.chgsvcstatus', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.chgsvcstatus', 'n', 'sub', 'view');
exec am_add_privilege('samp.web.chgsvcstatus.deleted', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.chgsvcstatus.deleted', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.chgsvcstatus.deleted', 'y', 'sub', 'view');

exec am_add_privilege('samp.web.chgsubstatus', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.chgsubstatus', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.chgsubstatus', 'n', 'sub', 'view');
exec am_add_privilege('samp.web.chgsubstatus.deleted', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.chgsubstatus.deleted', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.chgsubstatus.deleted', 'y', 'sub', 'view');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('smp.dm.test', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('smp.dm.test_parm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('smp.dm.testResults', 'n', 'csr_supervisor', 'viewDetails');
exec am_add_privilege('samp.web.testresult', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svcadvisory', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.display_children_info', 'n', 'csr_supervisor', 'view');

exec am_add_privilege('smp.admin.user', 'n', 'csr_supervisor', 'changeOwnPassword');
exec am_add_privilege('samp.web.menu.app_cfg', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.app_cfg', 'y', 'csr_supervisor', 'view');


exec am_add_privilege('samp.web.subparm.contacts.primary.address.type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.subparm.contacts.primary.address.pref_nm', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.subparm.contacts.primary.address.type', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.subparm.contacts.primary.address.pref_nm', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('smp.query', 'n', 'csr_supervisor', 'execute');
exec am_add_privilege('smp.query.order', 'n', 'csr_supervisor', 'execute');
exec am_add_privilege('smp.query.subbrief', 'n', 'csr_supervisor', 'execute');
exec am_add_privilege('smp.query', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByStatusSubId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByOwner', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('smp.query.order.LineItemQueryByOrdrId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('smp.query.subbrief.SubTmpltSearch', 'y', 'csr_supervisor', 'view');





-- XmlCfg
exec am_add_privilege('smp.xmlcfg', 'n', 'csr_supervisor', 'update');



-- Core configuration for order header parms permissions
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.actualCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.apiClientId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.created_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.created_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.debug_order', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.err_code', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.err_reason', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.failedServices', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.has_groups', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.managedEntityKey', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.modified_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.modified_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.need_partitioned_loading', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.pre_processor_info', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.pre_processor_jndiname', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.purchaseOrder', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queue_request_flag', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.samp_sub_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.src_nm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.state', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.sub_version', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.user_id', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.sub_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.requestedCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.orderDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.description', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.order_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.priority', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.order_type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.external_batch_id', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.resolved_indicator', 'y', 'csr_supervisor', 'view');

exec am_add_privilege('samp.web.orders.update_header_parm', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.actualCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.apiClientId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.created_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.created_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.debug_order', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.err_code', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.err_reason', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.failedServices', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.has_groups', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.managedEntityKey', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.modified_by', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.modified_dtm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.need_partitioned_loading', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.pre_processor_info', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.pre_processor_jndiname', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.purchaseOrder', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.queue_request_flag', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.samp_sub_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.services', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.src_nm', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.state', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.sub_version', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.user_id', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.sub_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.requestedCompletionDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.orderDate', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.description', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.order_ext_key', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.priority', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.order_type', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.ordr_owner', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.external_batch_id', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm.resolved_indicator', 'y', 'csr_supervisor', 'view');

-- Associations' permissions
exec am_add_privilege('samp.web.assoc', 'n', 'csr_supervisor', 'view');



