--============================================================================
--    $Id: am_spm_load_cable_solutions.sql,v 1.3 2012/01/11 14:27:06 christinz Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

--============================================================================
-- Create groups
--============================================================================
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: create groups   ---------')

exec am_add_grp('csr_tier1','Customer Sales Representatives Tier 1 Group');
exec am_add_grp_grp('csr_tier1', 'csr_supervisor');

exec am_add_grp('csr_tier2', 'Customer Sales Representatives Tier 2 Group');
exec am_add_grp_grp('csr_tier2', 'csr_tier1');

--============================================================================
-- Create users
--============================================================================
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: create users  ---------')

exec am_create_user('tier1', 'pwtier1', 'Advanced User - Customer Sales Rep with ability to change states');
exec am_create_user('tier2', 'pwtier2', 'Normal User - Customer Sales Rep with no ability to change states');
exec am_create_user('sprintbss', 'pwsprintbss', 'Bss adapter user - User for Sprint bss adapter');

exec am_add_user_parm('tier1', 'country', 'CA');
exec am_add_user_parm('tier1', 'language', 'en');
exec am_add_user_parm('tier2', 'country', 'CA');
exec am_add_user_parm('tier2', 'language', 'en');
exec am_add_user_parm('sprintbss', 'country', 'CA');
exec am_add_user_parm('sprintbss', 'language', 'en');

exec am_add_grp_user('tier1', 'csr_tier1');
exec am_add_grp_user('tier2', 'csr_tier2');
exec am_add_grp_user('sprintbss', 'csr_tier2');

exec am_add_grp_parm('csr_tier1', 'new_sub_service_provider_id', 'Default');
exec am_add_grp_parm('csr_tier1', 'service_provider_id', 'Default');
exec am_add_grp_parm('csr_tier2', 'new_sub_service_provider_id', 'Default');
exec am_add_grp_parm('csr_tier2', 'service_provider_id', 'Default');

--============================================================================
-- Configure privileges
--============================================================================
-- create secure objects for services
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: secure objects for services  ---------')

exec am_add_secure_obj('samp.web.svc.smp_emta_composed', 'Service: eMTA Composed');
exec am_add_secure_obj('samp.web.svc.smp_cm_hsd_docsis_access', 'Service: Cable Modem');
exec am_add_secure_obj('samp.web.svc.pc_voip_access', 'Service: MTA');
exec am_add_secure_obj('samp.web.svc.smp_intercept_disconnect', 'Service: Disconnect Intercept');
exec am_add_secure_obj('samp.web.svc.smp_intercept_port_out', 'Service: Port Out Intercept');
exec am_add_secure_obj('samp.web.svc.primary_selfcare_access', 'Service: Primary Self Care Access');
exec am_add_secure_obj('samp.web.svc.smp_secondary_voice_line', 'Service: Secondary Voice Line');
exec am_add_secure_obj('samp.web.svc.secondary_voicemail_box', 'Service: Secondary Voice Mail Box');

exec am_add_secure_obj('samp.web.svc.smp_switch_dial_tone_access', 'Service: Dial Tone Access');
exec am_add_secure_obj('samp.web.svc.primary_voicemail_box', 'Service: Primary Voicemail Box');
exec am_add_secure_obj('samp.web.svc.primary_directory_listing', 'Service: Primary Directory Listing');
exec am_add_secure_obj('samp.web.svc.smp_switch_feature_pkg_std', 'Service: Switch Feature Package');
exec am_add_secure_obj('samp.web.svc.smp_emta_inv_cm_hsd_access', 'Service: eMTA Inventory Cable Modem');
exec am_add_secure_obj('samp.web.svc.smp_emta_inv_pc_voip_access', 'Service: eMTA Inventory MTA');
exec am_add_secure_obj('samp.web.svc.primary_clec', 'Service: Primary CLEC Access');
exec am_add_secure_obj('samp.web.svc.internet_access', 'Service: Internet Access');

-- create secure objects for specific services
-- smp_switch_feature
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: secure objects: smp_switch_feature  ---------')

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_subscribed', 'Service Parameter: Subscribed Flag');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_admin_status', 'Service Parameter: Admin Status');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_ccw', 'Service Parameter: Cancel Call Waiting');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfv_active', 'Service Parameter: CFV Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_ar', 'Service Parameter: Automatic Recall');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_ac', 'Service Parameter: Automatic Callback');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_vmwi', 'Service Parameter: Visual Message Waiting Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cot', 'Service Parameter: Customer Originated Trace');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_racf', 'Service Parameter: Remote Activation of Call Forwarding');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_ocaa', 'Service Parameter: Outside Calling Area Alerting');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_acr_active', 'Service Parameter: ACR Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cidb', 'Service Parameter: CIDB');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfbl', 'Service Parameter: Call Forward Busy Line');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfbl_active', 'Service Parameter: CFBL Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfbl_forward_to_always', 'Service Parameter: CFBL Forward To Always Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfbl_forward_to_dn', 'Service Parameter: CFBL Forward To DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfda', 'Service Parameter: Call Forwarding Do Not Answer');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfda_active', 'Service Parameter: CFDA Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfda_forward_to_always', 'Service Parameter: CFDA Forward To Always Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfda_ring_period', 'Service Parameter: CFDA Ring Period');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cfda_forward_to_dn', 'Service Parameter: CFDA Forward To DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_scf', 'Service Parameter: Selective Call Forwarding');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_scf_active', 'Service Parameter: SCA Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_scf_forward_to_dn', 'Service Parameter: SCF Forward To DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_scr', 'Service Parameter: Selective Call Rejection');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_scr_active', 'Service Parameter: SCR Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_sca', 'Service Parameter: Selective Call Acceptance');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_sca_active', 'Service Parameter: SCA Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_drcw', 'Service Parameter: Distinctive Ringing/Call Waiting');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_drcw_active', 'Service Parameter: DRCW Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_spcall', 'Service Parameter: Speed Calling');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_rda', 'Service Parameter: Residential Distinctive Alerting Service');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr', 'Service Parameter: Line Service Restriction');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_block_domestic_long_distance', 'Service Parameter: LSR Block Domestic Long Distance');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_block_international_long_distance', 'Service Parameter: LSR Block International Long Distance');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_block_pay_per_call', 'Service Parameter: LSR Block Pay Per Call');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_block_operator_assistance_flag', 'Service Parameter: LSR Block Operator Assistance Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'Service Parameter: LSR Block Directory Assistance Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_block_toll_free_flag', 'Service Parameter: LSR Block Toll Free Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_dnd', 'Service Parameter: Do Not Disturb');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_dnd_active', 'Service Parameter: DND Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_rchd', 'Service Parameter: Residential Call Hold');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cwr', 'Service Parameter: Call Waiting Ringback');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cxr', 'Service Parameter: Call Transfer');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_wml', 'Service Parameter: Warm Line');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_wml_active', 'Service Parameter: WML Active');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_wml_timeout', 'Service Parameter: WML Timeout');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_wml_dn', 'Service Parameter: WML DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_aul', 'Service Parameter: Automatic Line');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_aul_dn', 'Service Parameter: AUL DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_rmi', 'Service Parameter: Remote Message Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_rmi_active', 'Service Parameter: RMI Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_vmeadn', 'Service Parameter: VM EasyAccess');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_vmeadn_dn', 'Service Parameter: VM EasyAccess DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_tdn', 'Service Parameter: Toll Deny');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_deny_ac', 'Service Parameter: Deny Automatic Call Back');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_deny_ar', 'Service Parameter: Deny Automatic Return');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_deny_ucfv', 'Service Parameter: Deny Usage Call Forwarding');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_deny_utwc', 'Service Parameter: Deny Usage 3 Way Calling');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_sca_dndonly', 'Service Parameter: SCA DND Only');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_lsr_active', 'Service Parameter: LSR Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_dnd_dndgrp', 'Service Parameter: DND Group');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_racf_pin', 'Service Parameter: RACF PIN');

-- primary_voicemail_box
exec am_add_secure_obj('samp.web.svcparm.primary_voicemail_box.preReqSvc', 'Service Parameter: Primary voice mail Service Dependancy');

-- smp_switch_feature_pkg_std
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: secure objects: smp_switch_feature_pkg_std  ---------')

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_subscribed', 'Service Parameter: Subscribed Flag');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_admin_status', 'Service Parameter: Admin Status');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ccw', 'Service Parameter: Cancel Call Waiting');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv_active', 'Service Parameter: CFV Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar', 'Service Parameter: Automatic Recall');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac', 'Service Parameter: Automatic Callback');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmwi', 'Service Parameter: Visual Message Waiting Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot', 'Service Parameter: Customer Originated Trace');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf', 'Service Parameter: Remote Activation of Call Forwarding');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ocaa', 'Service Parameter: Outside Calling Area Alerting');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr_active', 'Service Parameter: ACR Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidb', 'Service Parameter: CIDB ');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl', 'Service Parameter: Call Forward Busy Line');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_active', 'Service Parameter: CFBL Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'Service Parameter: CFBL Forward To Always Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'Service Parameter: CFBL Forward To DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda', 'Service Parameter: Call Forwarding Do Not Answer');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_active', 'Service Parameter: CFDA Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_always', 'Service Parameter: CFDA Forward To Always Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_ring_period', 'Service Parameter: CFDA Ring Period');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'Service Parameter: CFDA Forward To DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf', 'Service Parameter: Selective Call Forwarding');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_active', 'Service Parameter: SCA Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'Service Parameter: SCF Forward To DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'Service Parameter: Selective Call Rejection');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr_active', 'Service Parameter: SCR Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca', 'Service Parameter: Selective Call Acceptance');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_active', 'Service Parameter: SCA Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw', 'Service Parameter: Distinctive Ringing/Call Waiting');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw_active', 'Service Parameter: DRCW Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcall', 'Service Parameter: Speed Calling');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rda', 'Service Parameter: Residential Distinctive Alerting Service');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr', 'Service Parameter: Line Service Restriction');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'Service Parameter: LSR Block Domestic Long Distance');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'Service Parameter: LSR Block International Long Distance');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'Service Parameter: LSR Block Pay Per Call');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_operator_assistance_flag', 'Service Parameter: LSR Block Operator Assistance Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'Service Parameter: LSR Block Directory Assistance Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'Service Parameter: LSR Block Toll Free Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd', 'Service Parameter: Do Not Disturb');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_active', 'Service Parameter: DND Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rchd', 'Service Parameter: Residential Call Hold');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cwr', 'Service Parameter: Call Waiting Ringback');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cxr', 'Service Parameter: Call Transfer');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml', 'Service Parameter: Warm Line');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_active', 'Service Parameter: WML Active');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_timeout', 'Service Parameter: WML Timeout');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_dn', 'Service Parameter: WML DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul', 'Service Parameter: Automatic Line');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul_dn', 'Service Parameter: AUL DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi', 'Service Parameter: Remote Message Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi_active', 'Service Parameter: RMI Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn', 'Service Parameter: VM EasyAccess');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn_dn', 'Service Parameter: VM EasyAccess DN');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_tdn', 'Service Parameter: Toll Deny');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ac', 'Service Parameter: Deny Automatic Call Back');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ar', 'Service Parameter: Deny Automatic Return');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ucfv', 'Service Parameter: Deny Usage Call Forwarding');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_utwc', 'Service Parameter: Deny Usage 3 Way Calling');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_dndonly', 'Service Parameter: SCA DND Only');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_active', 'Service Parameter: LSR Active Indicator');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'Service Parameter: DND Group');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf_pin', 'Service Parameter: RACF PIN');

-- smp_clec_access
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : secure_objects: smp_clec_access   ---------')

exec am_add_secure_obj('samp.web.svcparm.smp_clec_access.confirmed_activation_date', 'Service Parameter: Activation Date Confirmed by Donor');
exec am_add_secure_obj('samp.web.svcparm.smp_clec_access.external_order_capture_id', 'Service Parameter: External Order Capture ID');
exec am_add_secure_obj('samp.web.svcparm.smp_clec_access.lec_order_id', 'Service Parameter: LEC Order ID');
exec am_add_secure_obj('samp.web.svcparm.smp_clec_access.lec_version_number', 'Service Parameter: LEC Version Number');

-- primary_clec
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : secure_objects: primary_clec   ---------')

exec am_add_secure_obj('samp.web.svcparm.primary_clec.confirmed_activation_date', 'Service Parameter: Activation Date Confirmed by Donor');
exec am_add_secure_obj('samp.web.svcparm.primary_clec.external_order_capture_id', 'Service Parameter: External Order Capture ID');
exec am_add_secure_obj('samp.web.svcparm.primary_clec.lec_order_id', 'Service Parameter: LEC Order ID');
exec am_add_secure_obj('samp.web.svcparm.primary_clec.lec_version_number', 'Service Parameter: LEC Version Number');

-- secondary_clec
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : secure_objects: primary_clec   ---------')

exec am_add_secure_obj('samp.web.svcparm.secondary_clec.confirmed_activation_date', 'Service Parameter: Activation Date Confirmed by Donor');
exec am_add_secure_obj('samp.web.svcparm.secondary_clec.external_order_capture_id', 'Service Parameter: External Order Capture ID');
exec am_add_secure_obj('samp.web.svcparm.secondary_clec.lec_order_id', 'Service Parameter: LEC Order ID');
exec am_add_secure_obj('samp.web.svcparm.secondary_clec.lec_version_number', 'Service Parameter: LEC Version Number');


--============================================================================
-- Assign privileges to Tier1
--============================================================================
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: assign priviledges: tier1  ---------')

exec am_add_privilege('samp.web.menu.subscriber.add', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.menu.subscriber.edit', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.menu.subscriber.actions', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.menu.services.add_edit', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.menu.services.change_status', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.menu.services.actions', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.menu.services.svc_migrate', 'y', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svc.smp_emta_composed', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_cm_hsd_docsis_access', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.pc_voip_access', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_intercept_disconnect', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_intercept_port_out', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.primary_selfcare_access', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_secondary_voice_line', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.secondary_voicemail_box', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_switch_dial_tone_access', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.primary_voicemail_box', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.primary_directory_listing', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_switch_feature_pkg_std', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_emta_inv_cm_hsd_access', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.smp_emta_inv_pc_voip_access', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.primary_clec', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svc.internet_access', 'n', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svc.smp_emta_composed', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_cm_hsd_docsis_access', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.pc_voip_access', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_intercept_disconnect', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_intercept_port_out', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.primary_selfcare_access', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_secondary_voice_line', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.secondary_voicemail_box', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_switch_dial_tone_access', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.primary_voicemail_box', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.primary_directory_listing', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_switch_feature_pkg_std', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_emta_inv_cm_hsd_access', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.smp_emta_inv_pc_voip_access', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.primary_clec', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svc.internet_access', 'y', 'csr_tier1', 'edit');

-- smp_switch_feature
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_subscribed', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_admin_status', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ccw', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfv_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ar', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ac', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_vmwi', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cot', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_racf', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ocaa', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_acr_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cidb', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl_forward_to_always', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl_forward_to_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_forward_to_always', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_ring_period', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_forward_to_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scf', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scf_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scf_forward_to_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scr_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_sca', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_sca_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_drcw', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_drcw_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_spcall', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rda', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_domestic_long_distance', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_international_long_distance', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_pay_per_call', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_operator_assistance_flag', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_toll_free_flag', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_dnd', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_dnd_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rchd', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cwr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cxr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml_timeout', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_aul', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_aul_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rmi', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rmi_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_vmeadn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_vmeadn_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_tdn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_ac', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_ar', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_ucfv', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_utwc', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_sca_dndonly', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_dnd_dndgrp', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_racf_pin', 'y', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_subscribed', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_admin_status', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ccw', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfv_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ar', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ac', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_vmwi', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cot', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_racf', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_ocaa', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_acr_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cidb', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl_forward_to_always', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfbl_forward_to_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_forward_to_always', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_ring_period', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cfda_forward_to_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scf', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scf_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scf_forward_to_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_scr_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_sca', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_sca_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_drcw', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_drcw_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_spcall', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rda', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_domestic_long_distance', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_international_long_distance', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_pay_per_call', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_operator_assistance_flag', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_directory_assistance_flag', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_block_toll_free_flag', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_dnd', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_dnd_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rchd', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cwr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cxr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml_timeout', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_wml_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_aul', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_aul_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rmi', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_rmi_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_vmeadn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_vmeadn_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_tdn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_ac', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_ar', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_ucfv', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_deny_utwc', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_sca_dndonly', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_lsr_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_dnd_dndgrp', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_racf_pin', 'y', 'csr_tier1', 'edit');

-- smp_switch_feature_pkg_std
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_subscribed', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_admin_status', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ccw', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmwi', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ocaa', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidb', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_always', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_ring_period', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcall', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rda', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_operator_assistance_flag', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rchd', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cwr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cxr', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn_dn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_tdn', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_active', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_subscribed', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_admin_status', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ccw', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmwi', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ocaa', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidb', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_always', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_ring_period', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcall', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rda', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_operator_assistance_flag', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rchd', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cwr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cxr', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn_dn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_tdn', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_active', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'csr_tier1', 'edit');

-- smp_clec_access
exec am_add_privilege('samp.web.svcparm.smp_clec_access.confirmed_activation_date', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_clec_access.external_order_capture_id', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_clec_access.lec_order_id', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.smp_clec_access.lec_version_number', 'n', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svcparm.smp_clec_access.confirmed_activation_date', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_clec_access.external_order_capture_id', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_clec_access.lec_order_id', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_clec_access.lec_version_number', 'y', 'csr_tier1', 'edit');

-- primary_clec
exec am_add_privilege('samp.web.svcparm.primary_clec.confirmed_activation_date', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.primary_clec.external_order_capture_id', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.primary_clec.lec_order_id', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.primary_clec.lec_version_number', 'n', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svcparm.primary_clec.confirmed_activation_date', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.primary_clec.external_order_capture_id', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.primary_clec.lec_order_id', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.primary_clec.lec_version_number', 'y', 'csr_tier1', 'edit');

-- secondary_clec
exec am_add_privilege('samp.web.svcparm.secondary_clec.confirmed_activation_date', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.secondary_clec.external_order_capture_id', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.secondary_clec.lec_order_id', 'n', 'csr_tier1', 'view');
exec am_add_privilege('samp.web.svcparm.secondary_clec.lec_version_number', 'n', 'csr_tier1', 'view');

exec am_add_privilege('samp.web.svcparm.secondary_clec.confirmed_activation_date', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.secondary_clec.external_order_capture_id', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.secondary_clec.lec_order_id', 'y', 'csr_tier1', 'edit');
exec am_add_privilege('samp.web.svcparm.secondary_clec.lec_version_number', 'y', 'csr_tier1', 'edit');

-- primary_voicemail_box
exec am_add_privilege('samp.web.svcparm.primary_voicemail_box.preReqSvc', 'y', 'csr_tier1', 'edit');

exec am_add_privilege('samp.web.svcparm.primary_voicemail_box.preReqSvc', 'y', 'csr_tier1', 'view');

--hide sub and order searches
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : queries - csr_tier1   ---------')
exec am_add_privilege('smp.query.subbrief.LineItemQueryByLineItemId', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.SubExternalKey', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByExtOrdrCaptureId', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.SubLenSearchAll', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.LineItemQueryByOrdrId', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'csr_tier1', 'view');

-- hide sub_id from sub and order search results
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : sub_id in search results - csr_tier1  ---------')
exec am_add_privilege('smp.query.subbrief.SubNameSearch.sub_id', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.SubAcctSearch.sub_id', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.byServicePhone.sub_id', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.byMacAddress.sub_id', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.subbrief.byProvState.sub_id', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByAcct.samp_sub_key', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByOrdrId.samp_sub_key', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByCreateDate.samp_sub_key', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByStatus.samp_sub_key', 'y', 'csr_tier1', 'view');
exec am_add_privilege('smp.query.order.OrdrQueryDateRangeStatus.samp_sub_key', 'y', 'csr_tier1', 'view');

--============================================================================
-- Assign privileges to Tier2
--============================================================================
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load_cable_solutions.sql: assign priviledges: tier2  ---------')

exec am_add_privilege('samp.web.menu.subscriber.add', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.menu.subscriber.edit', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.menu.subscriber.actions', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.menu.services.add_edit', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.menu.services.change_status', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.menu.services.actions', 'n', 'csr_tier2', 'view');

exec am_add_privilege('samp.web.svc.smp_switch_dial_tone_access', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.primary_voicemail_box', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.primary_directory_listing', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.smp_switch_feature_pkg_std', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.smp_emta_inv_cm_hsd_access', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.smp_emta_inv_pc_voip_access', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.primary_clec', 'n', 'csr_tier2', 'view');
exec am_add_privilege('samp.web.svc.internet_access', 'n', 'csr_tier2', 'view');

exec am_add_privilege('samp.web.svc.smp_switch_dial_tone_access', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.primary_voicemail_box', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.primary_directory_listing', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.smp_switch_feature_pkg_std', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.smp_emta_inv_cm_hsd_access', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.smp_emta_inv_pc_voip_access', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.primary_clec', 'n', 'csr_tier2', 'edit');
exec am_add_privilege('samp.web.svc.internet_access', 'n', 'csr_tier2', 'edit');

-- primary_voicemail_box
exec am_add_privilege('samp.web.svcparm.primary_voicemail_box.preReqSvc', 'y', 'csr_tier2', 'edit');

exec am_add_privilege('samp.web.svcparm.primary_voicemail_box.preReqSvc', 'y', 'csr_tier2', 'view');

--hide sub and order searches
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : queries - csr_tier2   ---------')
exec am_add_privilege('smp.query.subbrief.LineItemQueryByLineItemId', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.SubExternalKey', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByExtOrdrCaptureId', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.SubLenSearchAll', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.LineItemQueryByOrdrId', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'csr_tier2', 'view');

-- hide sub_id from sub and order search results
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: am_spm_load.sql : sub_id in search results - csr_tier2  ---------')
exec am_add_privilege('smp.query.subbrief.SubNameSearch.sub_id', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.SubAcctSearch.sub_id', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.byServicePhone.sub_id', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.byMacAddress.sub_id', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.subbrief.byProvState.sub_id', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByAcct.samp_sub_key', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByOrdrId.samp_sub_key', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByCreateDate.samp_sub_key', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByStatus.samp_sub_key', 'y', 'csr_tier2', 'view');
exec am_add_privilege('smp.query.order.OrdrQueryDateRangeStatus.samp_sub_key', 'y', 'csr_tier2', 'view');

--============================================================================
-- New Entity Model privilege setup
--============================================================================
-- Allow access to all the entities
exec am_add_secure_obj('samp.web.entity', 'Base resource for generic entity control');
exec am_add_privilege('samp.web.entity', 'n', 'csr', 'view');
exec am_add_privilege('samp.web.entity', 'n', 'csr', 'edit');
exec am_add_privilege('samp.web.entity', 'n', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity', 'n', 'csr_supervisor', 'edit');

-- Hide the samp_sub svc
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'Service samp_sub in the tree');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'csr', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'sub', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'sub.secondary', 'view');

