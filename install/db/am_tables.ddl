CREATE TABLE REJECT_PWD (
       pwd              VARCHAR2(30) NOT NULL
);

CREATE TABLE ACCESS_CTRL_ENTRY (
       grp_nm               VARCHAR2(30) NOT NULL,
       secure_obj_nm        VARCHAR2(255) NOT NULL,
       permission_nm        VARCHAR2(30) NOT NULL,
       is_excluded          CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE AM_PARM (
       parm_nm              VARCHAR2(30) NOT NULL,
       parm_descr           VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE GRP (
       grp_nm               VARCHAR2(30) NOT NULL,
       grp_descr            VARCHAR2(255) NOT NULL,
       adm_secure_obj_nm    VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE GRP_HIERARCHY (
       parent_grp_nm        VARCHAR2(30) NOT NULL,
       member_grp_nm        VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE GRP_PARM (
       grp_nm               VARCHAR2(30) NOT NULL,
       parm_nm              VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE GRP_USR (
       usr_nm               VARCHAR2(30) NOT NULL,
       grp_nm               VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PERMISSION (
       permission_nm        VARCHAR2(30) NOT NULL,
       permission_descr     VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_USR_STATUS (
       usr_status           VARCHAR2(30) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SECURE_OBJ (
       secure_obj_nm        VARCHAR2(255) NOT NULL,
       secure_obj_descr     VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE USR (
       usr_nm               VARCHAR2(30) NOT NULL,
       usr_pswd             VARCHAR2(255) NOT NULL,
       usr_descr            VARCHAR2(255) NULL,
       last_pwd_chng_dtm    DATE NULL,
       usr_status           VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE USR_PARM (
       usr_nm               VARCHAR2(30) NOT NULL,
       parm_nm              VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);

