--============================================================================
--    $Id: am_video_load.sql,v 1.24 2012/02/07 19:22:53 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

exec am_add_grp('vd_csr_admin','Video Customer Sales Representatives Administration Group with ability to change service and service parameters');
exec am_add_grp_grp('vd_csr_admin', 'csr_supervisor');

exec am_add_grp('csr_video_read','Video Customer Sales Representatives Administration Group with no ability to change service and service parameters');
exec am_add_grp('csr_video_edit','Video Customer Sales Representatives Administration Group with ability to change service and service parameters');

exec am_add_grp('no_svcmap','Video Customer Sales Representatives Administration Group with no ability to view Svc_Rsrc_Cfg tab');
exec am_create_user('nsvcmap', 'pwnsvcmap', 'Canadian Customer Sales Representative Administrator');
exec am_add_user_parm('nsvcmap', 'country', 'CA');
exec am_add_user_parm('nsvcmap', 'language', 'en');
exec am_add_grp_user('nsvcmap', 'no_svcmap');
exec am_add_grp_parm('no_svcmap', 'new_sub_service_provider_id', 'Default');
exec am_add_grp_parm('no_svcmap', 'service_provider_id', 'Default');
exec am_add_secure_obj('samp.web.menu.video_map', 'Video Map Resource');
exec am_add_grp_grp('no_svcmap', 'vd_csr_admin');

exec am_add_grp('test','Video Customer Sales Representatives Administration Group');
exec am_add_grp_grp('test', 'csr_supervisor');
exec am_add_grp_grp('test', 'csr_video_edit');
exec am_create_user('test', 'pwtest', 'Canadian Customer Sales Representative Administrator');
exec am_add_user_parm('test', 'country', 'CA');
exec am_add_user_parm('test', 'language', 'en');
exec am_add_grp_user('test', 'test');

exec am_create_user('video1', 'pwvideo1', 'Canadian Customer Sales Representative Administrator');
exec am_add_user_parm('video1', 'country', 'CA');
exec am_add_user_parm('video1', 'language', 'en');
exec am_add_grp_user('video1', 'vd_csr_admin');
exec am_add_grp_parm('vd_csr_admin', 'new_sub_service_provider_id', 'Default');
exec am_add_grp_parm('vd_csr_admin', 'service_provider_id', 'Default');
exec am_add_secure_obj('samp.web.svcparm.subscriber.type.vd_csr_admin', 'Subscriber Type: csr_video_edit');

exec am_create_user('cvr', 'pwcvr', 'Canadian Customer Sales Representative Administrator');
exec am_add_user_parm('cvr', 'country', 'CA');
exec am_add_user_parm('cvr', 'language', 'en');
exec am_add_grp_user('cvr', 'csr_video_read');
exec am_add_grp_parm('csr_video_read', 'new_sub_service_provider_id', 'Default');
exec am_add_grp_parm('csr_video_read', 'service_provider_id', 'Default');

exec am_create_user('cve', 'pwcve', 'Canadian Customer Sales Representative Administrator');
exec am_add_user_parm('cve', 'country', 'CA');
exec am_add_user_parm('cve', 'language', 'en');
exec am_add_grp_user('cve', 'csr_video_edit');
exec am_add_grp_parm('csr_video_edit', 'new_sub_service_provider_id', 'Default');
exec am_add_grp_parm('csr_video_edit', 'service_provider_id', 'Default');
exec am_add_grp_grp('csr_video_edit', 'vd_csr_admin');

exec am_create_user('edsuser', 'pwedsuser', 'User for expiryDateScheduler');
exec am_add_user_parm('edsuser', 'country', 'CA');
exec am_add_user_parm('edsuser', 'language', 'en');
exec am_add_grp_user('edsuser', 'csr_supervisor');

exec am_create_user('tandbergbss', 'pwtandbergbss', 'User for tandberg BSS Adapter');
exec am_add_user_parm('tandbergbss', 'country', 'CA');
exec am_add_user_parm('tandbergbss', 'language', 'en');
exec am_add_grp_user('tandbergbss', 'csr_supervisor');


--secure objects and permissions for csr_video_read

exec am_add_privilege('samp.web.assoc', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.batch', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.chgsubstatus', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.chgsvcstatus', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.display_children_info', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.entity', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.exception', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.inactive_svc', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.menu', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders.actions', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders.actions.edit', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders.line_items.actions', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders.queued_order_detail', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.subparm', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.subparm.cust_group.modified', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.subparm.cust_group.not_modified', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.subparm.svc_provider', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svc', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcadvisory', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.testresult', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.view_cart', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.view_cart.analyze_order', 'n', 'csr_video_read', 'view');
exec am_add_privilege('smp.admin.user', 'n', 'csr_video_read', 'changeOwnPassword');
exec am_add_privilege('smp.dm.test', 'n', 'csr_video_read', 'view');
exec am_add_privilege('smp.dm.testResults', 'n', 'csr_video_read', 'viewDetails');
exec am_add_privilege('smp.dm.test_parm', 'n', 'csr_video_read', 'view');
exec am_add_privilege('smp.query', 'n', 'csr_video_read', 'execute');
exec am_add_privilege('smp.query', 'n', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order', 'n', 'csr_video_read', 'execute');
exec am_add_privilege('smp.query.order', 'n', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief', 'n', 'csr_video_read', 'execute');
exec am_add_privilege('smp.query.subbrief', 'n', 'csr_video_read', 'view');
exec am_add_privilege('smp.sbnotify', 'n', 'csr_video_read', 'execute');
exec am_add_privilege('smp.xmlcfg', 'n', 'csr_video_read', 'update');
exec am_add_privilege('smp.query.subbrief.SubNameSearchAll.sub_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubAcctSearchAll.sub_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubPhoneSearchAll.sub_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubMacAddrSearchAll.sub_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubProvStateSearchAll.sub_id', 'y', 'csr_video_read', 'view');

exec am_add_privilege('smp.query.order.OrderQueryByStatus.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByStatusSubId.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByOwner.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByAcct.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByOrdrId.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByCreateDate.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrdrQueryDateRangeStatus.samp_sub_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByStatusSubId.batch_ordr_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByStatusSubId.ordr_owner', 'y', 'csr_video_read', 'view');

exec am_add_privilege('smp.query.subbrief.SubNameSearch', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubNameSearch', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.SubAcctSearch', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubAcctSearch', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.byServicePhone', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.byServicePhone', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.byMacAddress', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.byMacAddress', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.byProvState', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.byProvState', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.subbrief.SearchByCompanyName', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByCompanyName', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialServicePhone', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialServicePhone', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialAccountNo', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialAccountNo', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialMACAddr', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialMACAddr', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialProvisionState', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByCommercialProvisionState', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.order.SearchByOrderStatusRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderStatusRes', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndSubIdRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndSubIdRes', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOwnerRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOwnerRes', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByAccountNoRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByAccountNoRes', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOrderIdRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderIdRes', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByCreatedDateRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByCreatedDateRes', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndCreatedDateRes', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndCreatedDateRes', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.order.SearchByOrderStatusCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderStatusCommercial', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndSubIdCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndSubIdCommercial', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOwnerCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOwnerCommercial', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByAccountNoCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByAccountNoCommercial', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOrderIdCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderIdCommercial', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByCreatedDateCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByCreatedDateCommercial', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndCreatedDateCommercial', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.SearchByOrderStatusAndCreatedDateCommercial', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.subbrief.byClecSvcTelephoneNumber', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.byClecSvcTelephoneNumber', 'y', 'csr_video_read', 'edit');

--secure objects and permissions for csr_video_edit

exec am_add_privilege('samp.web.assoc', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.batch', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.chgsubstatus', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.chgsvcstatus', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.detach', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.display_children_info', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.entity', 'n', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.entity', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.exception', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.inactive_svc', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.menu', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders.actions', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders.actions.edit', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders.line_items.actions', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders.queued_order_detail', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.orders.update_header_parm', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.subparm', 'n', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.subparm', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.subparm.cust_group.modified', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.subparm.cust_group.not_modified', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.subparm.svc_provider', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svc', 'n', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svc', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcadvisory', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm', 'n', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.testresult', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.view_cart', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.view_cart.analyze_order', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('smp.admin.user', 'n', 'csr_video_edit', 'changeOwnPassword');
exec am_add_privilege('smp.dm.test', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('smp.dm.testResults', 'n', 'csr_video_edit', 'viewDetails');
exec am_add_privilege('smp.dm.test_parm', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('smp.query', 'n', 'csr_video_edit', 'execute');
exec am_add_privilege('smp.query', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('smp.query.order', 'n', 'csr_video_edit', 'execute');
exec am_add_privilege('smp.query.order', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('smp.query.subbrief', 'n', 'csr_video_edit', 'execute');
exec am_add_privilege('smp.query.subbrief', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('smp.sbnotify', 'n', 'csr_video_edit', 'execute');
exec am_add_privilege('smp.xmlcfg', 'n', 'csr_video_edit', 'update');

--secure objects and permissions for no_svcmap

exec am_add_privilege('samp.web.menu.video_map', 'y', 'no_svcmap', 'view');

--secure objects

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_cm_hsd_access', 'Service: smp_emta_cm_hsd_access');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:internet_access', 'Service: internet_access');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_pc_voip_access', 'Service: smp_emta_pc_voip_access');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_composed', 'Service: smp_emta_composed');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_inv_composed', 'Service: smp_emta_inv_composed');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_intercept_disconnect', 'Service: smp_intercept_disconnect');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_intercept_port_out', 'Service: smp_intercept_port_out');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_voice_line', 'Service: smp_voice_line');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:isp_services_composed', 'Service: isp_services_composed');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:pc_voip_access', 'Service: pc_voip_access');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_cm_hsd_docsis_access', 'Service: smp_cm_hsd_docsis_access');

--permissions for group vd_csr_admin

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_cm_hsd_access', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_cm_hsd_access', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:internet_access', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:internet_access', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_pc_voip_access', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_pc_voip_access', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_composed', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_composed', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_inv_composed', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_emta_inv_composed', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_intercept_disconnect', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_intercept_disconnect', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_intercept_port_out', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_intercept_port_out', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_voice_line', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_voice_line', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:isp_services_composed', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:isp_services_composed', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:pc_voip_access', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:pc_voip_access', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_cm_hsd_docsis_access', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_cm_hsd_docsis_access', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.menu.app_cfg', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.menu.admin', 'n', 'vd_csr_admin', 'view');

--secure objects for video services

exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cpe_equipment', 'Service: cpe_equipment');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:video_cpe', 'Service: video_cpe');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:moto_prop_stb', 'Service: moto_prop_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cisco_prop_stb', 'Service: cisco_prop_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:moto_cablecard_video_cpe', 'Service: moto_cablecard_video_cpe');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:nagra_prop_smartcard_stb', 'Service: nagra_prop_smartcard_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:nds_prop_smartcard_stb', 'Service: nds_prop_smartcard_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_video_stb', 'Service: smp_video_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:digital_cable_stb', 'Service: digital_cable_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:nagra_prop_stb', 'Service: nagra_prop_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:nds_prop_stb', 'Service: nds_prop_stb');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_stb_topology_override', 'Service: smp_stb_topology_override');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_cpe_cas', 'Service: smp_cpe_cas');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:stb_cas', 'Service: stb_cas');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:moto_prop_stb_cas', 'Service: moto_prop_stb_cas');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:cisco_prop_stb_cas', 'Service: cisco_prop_stb_cas');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:moto_cablecard', 'Service: moto_cablecard');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:nagra_smartcard', 'Service: nagra_smartcard');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:nds_smartcard', 'Service: nds_smartcard');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:stb_cm', 'Service: stb_cm');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:stb_mta', 'Service: stb_mta');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:video_services_composed', 'Service: video_services_composed');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_video_billing', 'Service: smp_video_billing');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:prepaid_account', 'Service: prepaid_account');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:ppv_prepaid_account', 'Service: ppv_prepaid_account');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:vod_prepaid_account', 'Service: vod_prepaid_account');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:video_service_plan', 'Service: video_service_plan');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_video_defn', 'Service: smp_video_defn');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:video_access', 'Service: video_access');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_video_entitlement', 'Service: smp_video_entitlement');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:video_subscription', 'Service: video_subscription');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:video_event', 'Service: video_event');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_eb_ppv_entitlement', 'Service: smp_eb_ppv_entitlement');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_eb_vod_order', 'Service: smp_eb_vod_order');
exec am_add_secure_obj('samp.web.entity.SubSvc.SubSvcSpec:smp_eb_video_stb', 'Service: smp_eb_video_stb');

--permissions to hide named queries for tanbergbss adapter from spm ui

exec am_add_secure_obj('smp.query.subbrief.SearchByEquipmentId', 'Query: SearchByEquipmentId');
exec am_add_secure_obj('smp.query.subbrief.SearchByEquipmentId.sub_id', 'Query: SearchByEquipmentId.sub_id');

exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_residential', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_residential', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId.sub_id', 'y', 'csr_commercial', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_all', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_all', 'edit');

exec am_add_secure_obj('smp.query.subbrief.SearchByEquipmentId1', 'Query: SearchByEquipmentId1');
exec am_add_secure_obj('smp.query.subbrief.SearchByEquipmentId1.sub_id', 'Query: SearchByEquipmentId1.sub_id');

exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_residential', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_residential', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1.sub_id', 'y', 'csr_commercial', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_all', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_all', 'edit');

--secure objects and permissions for svcparm

exec am_add_secure_obj('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'Service Parameter: STB Id Name');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'Service Parameter: STB Id Validation Rule');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_prop_stb.stb_id_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_prop_stb.stb_id_name', 'Service Parameter: STB Id Name');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'Service Parameter: STB Id Validation Rule');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_prop_stb.stb_id_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_stb_topology_override.region_key', 'Service Parameter: Region Key');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_stb_topology_override.region_key', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_cpe_cas.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cas.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cas.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cas.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cas.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cas.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'Service Parameter: CAS Method');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_method', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'Service Parameter: Id1 Name');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'Service Parameter: Id1 Validation Rule');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id1_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'Service Parameter: Id2 Name');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'Service Parameter: Id2 Validation Rule');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.id2_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'Service Parameter: Cablecard Id');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'Service Parameter: Cablecard Host Id');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_host_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'Service Parameter: Cablecard Security Data');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_security_data', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'Service Parameter: Cablecard Override');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cablecard_override', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'Service Parameter: Key certificate');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.key_certificate', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'Service Parameter: CAS Equipment Version');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_equipment_version', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'Service Parameter: CPE Admin Status');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cpe_admin_status', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.location_x', 'Service Parameter: Location X');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_x', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.location_y', 'Service Parameter: Location Y');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.location_y', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'Service Parameter: CAS Method');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_method', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'Service Parameter: Id1 Name');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'Service Parameter: Id1 Validation Rule');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id1_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'Service Parameter: Id2 Name');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'Service Parameter: Id2 Validation Rule');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.id2_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'Service Parameter: Cablecard Id');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'Service Parameter: Cablecard Host Id');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_host_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'Service Parameter: Cablecard Security Data');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_security_data', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'Service Parameter: Cablecard Override');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cablecard_override', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'Service Parameter: Turnon VC');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnon_vc', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'Service Parameter: Turnoff VC');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.turnoff_vc', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'Service Parameter: Output Channel');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.output_channel', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'Service Parameter: On Plant');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.on_plant', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.cas_method', 'Service Parameter: CAS Method');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_method', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.id1_name', 'Service Parameter: Id1 Name');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'Service Parameter: Id1 Validation Rule');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id1_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.id2_name', 'Service Parameter: Id2 Name');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'Service Parameter: Id2 Validation Rule');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.id2_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.key_certificate', 'Service Parameter: Key certificate');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.key_certificate', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'Service Parameter: CAS Equipment Version');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_equipment_version', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'Service Parameter: CPE Admin Status');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cpe_admin_status', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.location_x', 'Service Parameter: Location X');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_x', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.location_y', 'Service Parameter: Location Y');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.location_y', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cas_method', 'Service Parameter: CAS Method');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_method', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.id1_name', 'Service Parameter: Id1 Name');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'Service Parameter: Id1 Validation Rule');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id1_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.id2_name', 'Service Parameter: Id2 Name');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'Service Parameter: Id2 Validation Rule');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.id2', 'Service Parameter: Id2');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.id2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cablecard_id', 'Service Parameter: Cablecard Id');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'Service Parameter: Cablecard Host Id');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_host_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'Service Parameter: Cablecard Security Data');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_security_data', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cablecard_override', 'Service Parameter: Cablecard Override');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cablecard_override', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.key_certificate', 'Service Parameter: Key certificate');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.key_certificate', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.turnon_vc', 'Service Parameter: Turnon VC');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnon_vc', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'Service Parameter: Turnoff VC');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.turnoff_vc', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.output_channel', 'Service Parameter: Output Channel');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.output_channel', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'Service Parameter: CAS Equipment Version');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_equipment_version', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.on_plant', 'Service Parameter: On Plant');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.on_plant', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'Service Parameter: CPE Admin Status');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cpe_admin_status', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.location_x', 'Service Parameter: Location X');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_x', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.location_y', 'Service Parameter: Location Y');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.location_y', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nagra_smartcard.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cas_method', 'Service Parameter: CAS Method');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_method', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.id1_name', 'Service Parameter: Id1 Name');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'Service Parameter: Id1 Validation Rule');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id1_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.id2_name', 'Service Parameter: Id2 Name');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_name', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'Service Parameter: Id2 Validation Rule');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2_validation_rule', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.id2', 'Service Parameter: Id2');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.id2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cablecard_id', 'Service Parameter: Cablecard Id');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'Service Parameter: Cablecard Host Id');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_host_id', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'Service Parameter: Cablecard Security Data');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_security_data', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cablecard_override', 'Service Parameter: Cablecard Override');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cablecard_override', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.parental_control_pin', 'Service Parameter: Parental Control PIN');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.parental_control_pin', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.key_certificate', 'Service Parameter: Key certificate');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.key_certificate', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.turnon_vc', 'Service Parameter: Turnon VC');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnon_vc', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.turnoff_vc', 'Service Parameter: Turnoff VC');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.turnoff_vc', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.output_channel', 'Service Parameter: Output Channel');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.output_channel', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'Service Parameter: CAS Equipment Version');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_equipment_version', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.on_plant', 'Service Parameter: On Plant');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.on_plant', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'Service Parameter: CPE Admin Status');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cpe_admin_status', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.location_x', 'Service Parameter: Location X');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_x', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.location_y', 'Service Parameter: Location Y');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.location_y', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'Service Parameter: CAS Reference ID 1');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_1', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'Service Parameter: CAS Reference ID 2');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.cas_ref_id_2', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.bc_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.ppv_video_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'n', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'n', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.nds_smartcard.vod_prov_tags', 'n', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'Service Parameter: Sub Svc Dependency Count');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_depy_count', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'Service Parameter: Sub Svc Prerequisite Count');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.sub_svc_prereq_count', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cm.va_svc_pkg', 'Service Parameter: Vital Access Service Pkg');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.va_svc_pkg', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_cm.hsi_capable', 'Service Parameter: HSI Capable');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_cm.hsi_capable', 'y', 'csr_supervisor', 'view');

exec am_add_secure_obj('samp.web.svcparm.stb_mta.num_of_voice_lines', 'Service Parameter: Number of Voice Lines');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.num_of_voice_lines', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'Service Parameter: Billing Account Id Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.account_id_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_billing.prepaid_balance', 'Service Parameter: Prepaid Balance');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_billing.prepaid_balance', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'Service Parameter: Prepaid Account Id Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.account_id_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.prepaid_account.prepaid_balance', 'Service Parameter: Prepaid Balance');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.prepaid_balance', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.prepaid_account.postpaid_limit', 'Service Parameter: PostPaid Limit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.prepaid_account.postpaid_limit', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'Service Parameter: Prepaid Account Id Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.account_id_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'Service Parameter: Prepaid Balance');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.prepaid_balance', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'Service Parameter: PostPaid Limit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.ppv_prepaid_account.postpaid_limit', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'Service Parameter: Prepaid Account Id Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.account_id_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'Service Parameter: Prepaid Balance');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.prepaid_balance', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'Service Parameter: PostPaid Limit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.vod_prepaid_account.postpaid_limit', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'Service Parameter: Broadcast Video Provisioning Tag List Change Date');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.bc_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'Service Parameter: PPV Provisioning Tag List Change Date');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.ppv_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'Service Parameter: VoD Provisioning Tag List Change Date');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.vod_prov_tags_chg_date', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'Service Parameter: Requested Purchases Reportback Day');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_requested_reportback', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'Service Parameter: Purchases Reportback Availability');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_defn.purchases_reportback_availability', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'Service Parameter: Broadcast Video Provisioning Tag List Change Date');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.bc_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'Service Parameter: PPV Provisioning Tag List Change Date');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'Service Parameter: VoD Provisioning Tag List Change Date');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.vod_prov_tags_chg_date', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_access.purchases_requested_reportback', 'Service Parameter: Requested Purchases Reportback Day');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_requested_reportback', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_access.purchases_reportback_availability', 'Service Parameter: Purchases Reportback Availability');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.purchases_reportback_availability', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'Service Parameter: Video Entitlement Type');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_type', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'Service Parameter: Video Entitlement Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_video_entitlement.video_entitlement_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_subscription.video_entitlement_type', 'Service Parameter: Video Entitlement Type');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_type', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'Service Parameter: Video Entitlement Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.video_entitlement_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'Service Parameter: Ppv video provisioning tags');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_prov_tags', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'Service Parameter: PPV Service Tags');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.ppv_video_service_tags', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_subscription.force_cas_assignment', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_subscription.force_cas_assignment', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_event.video_entitlement_type', 'Service Parameter: Video Entitlement Type');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_type', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'Service Parameter: Video Entitlement Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.video_entitlement_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_event.bc_video_prov_tags', 'Service Parameter: Broadcast video provisioning tags');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.bc_video_prov_tags', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_event.vod_prov_tags', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.vod_prov_tags', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_event.force_cas_assignment', 'Service Parameter: VoD provisioning tags');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_event.force_cas_assignment', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'Service Parameter: Key certificate');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.smp_eb_video_stb.key_certificate', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'Service Parameter: Parm Uniqueness Check');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.video_access.video_service_plan_uniqueness_check', 'y', 'csr_supervisor', 'view');
exec am_add_secure_obj('samp.web.svcparm.stb_mta.account_id_and_nickname', 'Service Parameter: nickname check');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.account_id_and_nickname', 'y', 'csr_supervisor', 'edit');
exec am_add_secure_obj('samp.web.svcparm.stb_mta.dev_prov_req', 'Service Parameter: Device Prov Required');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'vd_csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'vd_csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_admin', 'edit');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_supervisor', 'view');
exec am_add_privilege('samp.web.svcparm.stb_mta.dev_prov_req', 'y', 'csr_supervisor', 'edit');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_id', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_host_id', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_host_id', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_security_data', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_security_data', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_override', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_override', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_pin', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_pin', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.key_certificate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.key_certificate', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnon_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnon_vc', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnoff_vc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnoff_vc', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.output_channel', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.output_channel', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.regn_cfg', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.regn_cfg', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.downloadable', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.downloadable', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.rf_bypass', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.rf_bypass', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_rating', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_rating', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.volume_mute', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.volume_mute', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.vcr_tuning', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.vcr_tuning', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.last_channel', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.last_channel', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.fav_channel', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.fav_channel', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_time', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_time', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_cost', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_cost', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.interactive_ir', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.interactive_ir', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.purchase_cancellation', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.purchase_cancellation', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.remote_control', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.remote_control', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.time_controlled_prog', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.time_controlled_prog', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.user_specified_language', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.user_specified_language', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ab_output', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ab_output', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.applications_interface_port', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.applications_interface_port', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.force_standalone', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.force_standalone', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ignore_blackout', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ignore_blackout', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.high_speed_serial_output', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.high_speed_serial_output', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.on_plant', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.on_plant', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cpe_admin_status', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cpe_admin_status', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_x', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_x', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_y', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_y', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_1', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_2', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_2', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.svcAddress', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.svcAddress', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_method', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_method', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_name', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_validation_rule', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_name', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_name', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_validation_rule', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_validation_rule', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ippv_entitled', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ippv_entitled', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_entitled', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_entitled', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.interactive_service_entitled', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.interactive_service_entitled', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.dms_enabled', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.dms_enabled', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.analog_entitled', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.analog_entitled', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_prov_tags_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_prov_tags_chg_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_requested_reportback', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_requested_reportback', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_reportback_availability', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_reportback_availability', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.svcAddress', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.svcAddress', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.svcAddress', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.svcAddress', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_type', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_type', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_uniqueness_check', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_uniqueness_check', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.bc_video_prov_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.bc_video_prov_tags', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_prov_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_prov_tags', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_service_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_service_tags', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.vod_prov_tags', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.vod_prov_tags', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.begin_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.begin_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.end_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.end_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.taping_authorization', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.taping_authorization', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.force_cas_assignment', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.force_cas_assignment', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_list_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_list_chg_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.time_zone', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.time_zone', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.credit_limit', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.credit_limit', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.iptv_service_group', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.iptv_service_group', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.sd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.sd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.hd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.hd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.dvr_entitled', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.dvr_entitled', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_list_chg_date', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_list_chg_date', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.language', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.language', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.disable_icc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.disable_icc', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.language', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.language', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.disable_icc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.disable_icc', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_dtt_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_dtt_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_sd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_sd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_hd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_hd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.language', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.language', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.disable_icc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.disable_icc', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.ingress_dtt_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ingress_dtt_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.ingress_sd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ingress_sd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.ingress_hd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ingress_hd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.language', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.language', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.disable_icc', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.disable_icc', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.egress_max_bit_rate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.egress_max_bit_rate', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.egress_sd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.egress_sd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.egress_hd_streams', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.egress_hd_streams', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_id_list', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_id_list', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_id_list', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_id_list', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.time_zone', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.time_zone', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_id', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_host_id', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_host_id', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_security_data', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_security_data', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_override', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cablecard_override', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_pin', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_pin', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.key_certificate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.key_certificate', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnon_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnon_vc', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnoff_vc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.turnoff_vc', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.output_channel', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.output_channel', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.regn_cfg', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.regn_cfg', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.downloadable', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.downloadable', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.rf_bypass', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.rf_bypass', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_rating', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_rating', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.volume_mute', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.volume_mute', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.vcr_tuning', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.vcr_tuning', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.last_channel', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.last_channel', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.fav_channel', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.fav_channel', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_time', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_time', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_cost', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.parental_control_cost', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.interactive_ir', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.interactive_ir', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.purchase_cancellation', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.purchase_cancellation', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.remote_control', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.remote_control', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.time_controlled_prog', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.time_controlled_prog', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.user_specified_language', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.user_specified_language', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ab_output', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ab_output', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.applications_interface_port', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.applications_interface_port', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.force_standalone', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.force_standalone', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ignore_blackout', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.ignore_blackout', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.high_speed_serial_output', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.high_speed_serial_output', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.on_plant', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.on_plant', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cpe_admin_status', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cpe_admin_status', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_x', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_x', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_y', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.location_y', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_1', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_1', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_2', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_ref_id_2', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.svcAddress', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.svcAddress', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_method', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.cas_method', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_name', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id1_validation_rule', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_name', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_name', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_validation_rule', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.motorola_ip_stb.id2_validation_rule', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.video_service_plan_uniqueness_check', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ippv_entitled', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ippv_entitled', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_entitled', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_entitled', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.interactive_service_entitled', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.interactive_service_entitled', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.dms_enabled', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.dms_enabled', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.analog_entitled', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.analog_entitled', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.bc_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.ppv_video_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_prov_tags_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.vod_prov_tags_chg_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_requested_reportback', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_requested_reportback', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_reportback_availability', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.purchases_reportback_availability', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.svcAddress', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.svcAddress', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.svcAddress', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.svcAddress', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_type', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_type', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.video_entitlement_uniqueness_check', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.bc_video_prov_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.bc_video_prov_tags', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_prov_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_prov_tags', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_service_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.ppv_video_service_tags', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.vod_prov_tags', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.vod_prov_tags', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.begin_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.begin_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.end_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.end_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.taping_authorization', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.taping_authorization', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.force_cas_assignment', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_subscriber_grp.force_cas_assignment', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_list_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_list_chg_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.time_zone', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.time_zone', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.credit_limit', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.credit_limit', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.iptv_service_group', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.iptv_service_group', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.sd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.sd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.hd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.hd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.dvr_entitled', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.dvr_entitled', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_list_chg_date', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_list_chg_date', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.language', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.language', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.disable_icc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_prop_stb_cas.disable_icc', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_dtt_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_sd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.ingress_hd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.language', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.language', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.disable_icc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.cisco_prop_stb_cas.disable_icc', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_dtt_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_dtt_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_sd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_sd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_hd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.ingress_hd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.language', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.language', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.moto_cablecard.disable_icc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.moto_cablecard.disable_icc', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.ingress_dtt_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ingress_dtt_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.ingress_sd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ingress_sd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.ingress_hd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.ingress_hd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.language', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.language', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.disable_icc', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.disable_icc', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.egress_max_bit_rate', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.egress_max_bit_rate', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.egress_sd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.egress_sd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.egress_hd_streams', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.egress_hd_streams', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_id_list', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.video_access.sub_grp_id_list', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_id_list', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.sub_grp_id_list', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.time_zone', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.time_zone', 'y', 'csr_video_edit', 'edit');

exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId1', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SearchByEquipmentId', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_ip_stb', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:smp_ip_stb', 'y', 'csr_video_edit', 'view');

exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('samp.web.entity.SubSvc.SubSvcSpec:samp_sub', 'y', 'csr_video_read', 'view');

exec am_add_privilege('smp.query.subbrief.SubSvcSearchByExtOrdrCaptureId', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubSvcSearchByExtOrdrCaptureId', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.subbrief.LineItemQueryByLineItemId', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.LineItemQueryByLineItemId', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.subbrief.SubExternalKey', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubExternalKey', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.subbrief.byStatus', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.byStatus', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.subbrief.SubLenSearchAll', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.subbrief.SubLenSearchAll', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryByOwner', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByOwner', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryByCreateDate', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryByCreateDate', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('smp.query.order.LineItemQueryByOrdrId', 'y', 'csr_video_read', 'view');
exec am_add_privilege('smp.query.order.LineItemQueryByOrdrId', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.modified', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.modified', 'y', 'csr_video_read', 'edit');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.not_modified', 'n', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.not_modified', 'y', 'csr_video_read', 'edit');

exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.modified', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.modified', 'y', 'csr_video_edit', 'edit');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.not_modified', 'n', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.svcparm.ms_iptv_video_access.iptv_service_group.not_modified', 'y', 'csr_video_edit', 'edit');


exec am_add_privilege('samp.web.chgsubstatus.active.reprov', 'y', 'csr_video_read', 'view');
exec am_add_privilege('samp.web.chgsubstatus.active.reprov', 'y', 'csr_video_edit', 'view');
exec am_add_privilege('samp.web.chgsubstatus.active.reprov', 'y', 'no_svcmap', 'view');
exec am_add_privilege('samp.web.chgsubstatus.active.reprov', 'y', 'vd_csr_admin', 'view');
