--============================================================================
--    $Id: base_am_load.sql,v 1.35.6.1.2.1.2.1 2010/12/16 14:51:17 cristiand Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
exec am_add_secure_obj('smp.dm' , 'Application');
exec am_add_secure_obj('smp.dm.testResults' , 'Test results');
exec am_add_secure_obj('smp.dm.test' , 'View tests');
exec am_add_secure_obj('smp.dm.test_parm' , 'View test parms');
exec am_add_secure_obj('smp.dm.subscriber' , 'Subscriber');
exec am_add_secure_obj('smp.dm.troubleTicket' , 'Trouble Tickets in DM');
exec am_add_secure_obj('smp.dm.svcAdvisory' , 'Service Advisories in DM');
exec am_add_secure_obj('smp.dm.service' , 'service to reset');

-- SMP Web resources

-- create objects for checking permissions on entity status change
exec am_add_secure_obj('samp.web', 'Base Web Resource');
exec am_add_secure_obj('samp.web.chgstatus', 'Change entity status');


exec am_add_secure_obj('samp.web.exception', 'Runtime java exceptions');
exec am_add_secure_obj('samp.web.svc', 'Services in the tree');
exec am_add_secure_obj('samp.web.inactive_svc', 'Inactive services in the tree');
exec am_add_secure_obj('samp.web.svcparm', 'Services parameters');
exec am_add_secure_obj('samp.web.subparm', 'Subscriber parameters');
exec am_add_secure_obj('samp.web.chgsvcstatus', 'Target change service status');
exec am_add_secure_obj('samp.web.chgsubstatus', 'Target change subscriber status');
exec am_add_secure_obj('samp.web.detach', 'Detach item in menu frame');

exec am_add_secure_obj('samp.web.testresult', 'Rollup for test results');
exec am_add_secure_obj('samp.web.svcadvisory', 'Rollup for service advisories');

exec am_add_secure_obj('samp.web.entity', 'Base resource for generic entity control');
exec am_add_secure_obj('samp.web.batch', 'Base batch resource name');
exec am_add_secure_obj('samp.web.display_children_info', 'Controls display of children details in view composed service page');

exec am_add_secure_obj('samp.web.orders', 'Base order resources');
exec am_add_secure_obj('samp.web.orders.line_item_details_order_header', 'Order parms in line item details page');
exec am_add_secure_obj('samp.web.orders.line_item_details_header', 'Line item parms in line item details page');
exec am_add_secure_obj('samp.web.orders.order_details_header_parms', 'Order parms in order details page');
exec am_add_secure_obj('samp.web.orders.queued_order_detail', 'View Queued Order Details');
exec am_add_secure_obj('samp.web.orders.view_workflow', 'View workflow');
exec am_add_secure_obj('samp.web.orders.view_log_analyzer', 'View log analyzer');
exec am_add_secure_obj('samp.web.orders.actions', 'Actions on orders');
exec am_add_secure_obj('samp.web.orders.actions.edit', 'Edit order');
exec am_add_secure_obj('samp.web.orders.actions.replicate', 'Replicate order');
exec am_add_secure_obj('samp.web.orders.actions.repair', 'Repair order');
exec am_add_secure_obj('samp.web.orders.line_items.actions', 'Actions on line items');

exec am_add_secure_obj('samp.web.orders.capture_header_parm', 'Order header parameters captured in view cart list page');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.actualCompletionDate', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.apiClientId', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.created_by', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.created_dtm', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.err_code', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.err_reason', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.failedServices', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.managedEntityKey', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.modified_by', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.modified_dtm', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.need_partitioned_loading', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.orderDate', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.order_ext_key', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.purchaseOrder', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.queue_request_flag', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.requestedCompletionDate', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.samp_sub_key', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.services', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.src_nm', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.state', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.sub_ext_key', '');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.order_type' , 'just for batch orders');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.ordr_owner' , 'just for batch orders');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.external_batch_id' , 'just for batch orders');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.resolved_indicator', '');

-- resources for updating order header parms
exec am_add_secure_obj('samp.web.orders.update_header_parm' , 'Update order header parameters from batch order screen');
exec am_add_secure_obj('samp.web.orders.update_header_parm.order_type' , 'Update order header parameter order type from batch order screen');
exec am_add_secure_obj('samp.web.orders.update_header_parm.ordr_owner' , 'Update order header parameter order owner from batch order screen');
exec am_add_secure_obj('samp.web.orders.update_header_parm.external_batch_id' , 'Update order header parameter external batch id from batch order screen');
exec am_add_secure_obj('samp.web.orders.update_header_parm.debug_order', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.has_groups', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.pre_processor_info', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.pre_processor_jndiname', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.sub_version', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.user_id', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.description', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.priority', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm', 'Order header parameters captured in view cart list page');
exec am_add_secure_obj('samp.web.orders.update_header_parm.actualCompletionDate', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.apiClientId', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.created_by', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.created_dtm', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.err_code', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.err_reason', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.failedServices', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.managedEntityKey', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.modified_by', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.modified_dtm', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.need_partitioned_loading', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.orderDate', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.order_ext_key', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.purchaseOrder', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.queue_request_flag', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.requestedCompletionDate', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.samp_sub_key', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.services', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.src_nm', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.state', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.sub_ext_key', '');
exec am_add_secure_obj('samp.web.orders.update_header_parm.resolved_indicator' , '');

-- resources for batch order screen
exec am_add_secure_obj('samp.web.batch.take_ownership' , 'Permit taking ownership for batch without owner on batch order screen');
exec am_add_secure_obj('samp.web.admin_batch.release_ownership' , 'Permit to admin to release batch ownership on batch order screen');
exec am_add_secure_obj('samp.web.batch.add_order' , 'Permit adding order to batch');
exec am_add_secure_obj('samp.web.batch.submit_now' , 'Permit submitting orders on batch order screen');
exec am_add_secure_obj('samp.web.batch.cancel' , 'Permit canceling orders on batch order screen');
exec am_add_secure_obj('samp.web.batch.build_dependencies' , 'Permit setting order dependencies from on batch order screen');

exec am_add_secure_obj('smp.query', 'General SMP Query Resource');
exec am_add_secure_obj('smp.query.order', 'Order SMP Query Resource');
exec am_add_secure_obj('smp.query.order.OrderQueryByStatusSubId', 'OrderQueryByStatusSubId');
exec am_add_secure_obj('smp.query.order.OrderQueryByOwner', 'OrderQueryByOwner');
exec am_add_secure_obj('smp.query.order.LineItemQueryByOrdrId', 'LineItemQueryByOrdrId');
exec am_add_secure_obj('smp.query.subbrief', 'SubBrief SMP Query Resource');
exec am_add_secure_obj('smp.query.subbrief.SubTmpltSearch', 'Sub template search');

exec am_add_secure_obj('smp.sbnotify', 'SB notification through SBNotifyDriver');

exec am_add_secure_obj('smp.xmlcfg', 'Xml configuration resource');

exec am_add_secure_obj('smp.xmlcfg.smp_cfg', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.sb_integration', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.shelf', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.services', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.labels', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.named_query', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.template', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.sub_mgr', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.order_mgr', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.sb_wl_service', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.sdp_cfg', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.jmflib_cfg', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.meta_imp', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.smp_ref_data_imp', 'Xml configuration resource');
exec am_add_secure_obj('smp.xmlcfg.license', 'Xml configuration resource');

exec am_add_secure_obj('smp.management', 'management resource');
exec am_add_secure_obj('smp.manualqueue' , 'Default resource for manual queue');
exec am_add_secure_obj('smp.inventory' , 'SMP Inventory root');
exec am_add_secure_obj('smp.inventory.WfProcess' , 'Workflow process entity');


-- SORG resources

exec am_add_secure_obj('smp.svc_action', 'Service Actions');

-- AM resources

exec am_add_secure_obj('smp.admin.group' , 'admin group resource');
exec am_add_secure_obj('smp.admin.user' , 'admin resource');
exec am_add_secure_obj('smp.admin.resource' , 'admin resource');
exec am_add_secure_obj('smp.admin.permission' , 'admin resource');
exec am_add_secure_obj('smp.admin.privilege' , 'admin resource');
exec am_add_secure_obj('smp.admin.parm' , 'admin resource');

exec am_add_secure_obj('samp.web.menu' , 'Used for SPMWEB');
exec am_add_secure_obj('samp.web.menu.admin' , 'admin menu resource');
exec am_add_secure_obj('samp.web.menu.diagnostics' , 'diagnostics menu resource');

exec am_add_secure_obj('STM' , 'STM');
exec am_add_secure_obj('STM.create_subntwk' , 'STM.create_subntwk');
exec am_add_secure_obj('STM.modify_subntwk' , 'STM.modify_subntwk');
exec am_add_secure_obj('STM.delete_subntwk' , 'STM.delete_subntwk');
exec am_add_secure_obj('STM.congestion_relief.Congestion Relief' , 'STM.congestion_relief.congestion_relief');      
exec am_add_secure_obj('STM.congestion_relief.Congestion Relief.congestion_relief' , 'STM.congestion_relief.congestion_relief.congestion_relief'); 
exec am_add_secure_obj('STM.congestion_relief.Congestion Relief.verification' , 'STM.congestion_relief.congestion_relief.verification');           

-- Assoc resources

exec am_add_secure_obj('samp.web.assoc' , 'Default association security resource');

-- Paramaters

exec am_add_parm('country' , 'Parm for the Country');
exec am_add_parm('language' , 'Parm for the Language');
exec am_add_parm('adm_pkg_uri', 'ADM - Package URI');
exec am_add_parm('adm_audit_enable', 'ADM - Enable audit flag');
exec am_add_parm('adm_expert_enable', 'ADM - Expert status flag');
exec am_add_parm('max_pwd_age_days' , 'Maximum password age in days');

exec am_add_permission('viewDetails' , 'Allows users to view details for test results');
exec am_add_permission('execute' , 'Allows users to execute tests');
exec am_add_permission('create' , 'Allows user to create things');
exec am_add_permission('viewHistorical' , 'Allows users to view historical test results');
exec am_add_permission('search' , 'Allows users to view details for test results');
exec am_add_permission('logout' , 'logout permission');
exec am_add_permission('reset' , 'Allows the user to reset service in DM');
exec am_add_permission('view','view permission');
exec am_add_permission('delete','delete permission');
exec am_add_permission('update','update permission');
exec am_add_permission('assign','assign permission');
exec am_add_permission('exclude','exclude permission');
exec am_add_permission('changeOthersPassword','change others password permission');
exec am_add_permission('changeOwnPassword','change own password permission');

exec am_add_permission('v' , 'view');
exec am_add_permission('l' , 'lock');
exec am_add_permission('u' , 'update');
exec am_add_permission('e' , 'execute');
exec am_add_permission('ul' , 'unlock');
