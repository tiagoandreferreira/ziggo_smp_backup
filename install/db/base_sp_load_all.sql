--============================================================================
--    $Id: base_sp_load_all.sql,v 1.54.10.1 2010/01/27 21:22:15 mattl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Svc_Provider   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Svc_Provider   ---------')
------------------------------------------------------------------
---------   Data fill for table: State_Action   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: State_Action   ---------')
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (1,111,'activate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (2,111,'deactivate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (3,111,'delete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (4,111,'mso_block',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (5,111,'courtesy_block',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (6,111,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (7,111,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (8,111,'update',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (9,111,'add',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (11,100,'activate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (12,100,'deactivate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (13,100,'delete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (14,100,'mso_block',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (15,100,'courtesy_block',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (16,100,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (17,100,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (18,100,'update',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (19,100,'add',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (20,100,'reprov',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (21,114,'activate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (22,114,'deactivate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (23,114,'delete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (24,114,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (25,114,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (26,114,'update',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (27,114,'add',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (41,4,'activate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (42,4,'deactivate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (43,4,'delete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (44,4,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (45,4,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (46,4,'update',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (47,4,'add',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (51,324,'submit',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
--Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
--Values (57,324,'analyzed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (53,324,'completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (54,324,'failed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (55,324,'cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (56,324,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (61,507,'submit',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (62,507,'completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (63,507,'cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (64,507,'failed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (65,507,'order_failed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (67,507,'order_submitted',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (68,507,'order_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (69,507,'order_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (70,507,'enter_mt',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (71,507,'exit_mt',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (73,324,'commit',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (75,324,'cancel',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (76,324,'complete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (77,324,'reject',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (78,324,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (57,324,'item_warning',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (101,324,'suspend',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (102,324,'resume',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);


Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (79,507,'cancel',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (80,507,'complete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (81,507,'order_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (82,506,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (83,111,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (84,114,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (86,100,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (87,4,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (88,324,'item_failed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (89,324,'error',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);

---for SubUser
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (91,700,'activate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (92,700,'deactivate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (93,700,'delete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (94,700,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (95,700,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (96,700,'update',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (97,700,'add',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (98,700,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (50,324,'queued',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Action(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
Values (52,324,'pre_queued',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: State_Transition   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: State_Transition   ---------')
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (1,NULL,1,3,NULL,6, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (2,NULL,1,9,NULL,7, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (3,NULL,7,2,NULL,1, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (4,NULL,5,2,NULL,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (5,NULL,2,3,NULL,6, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (6,NULL,2,11,NULL,7, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (7,NULL,3,4,NULL,5, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (8,NULL,7,4,NULL,5, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (9,NULL,4,5,NULL,6, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (10,NULL,4,11,NULL,7, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (11,NULL,3,6,NULL,4, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (12,NULL,5,6,NULL,4, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (13,NULL,6,7,NULL,6, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (14,NULL,6,11,NULL,7, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (15,NULL,3,8,NULL,3, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (16,NULL,7,8,NULL,3, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (17,NULL,5,8,NULL,3, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (18,NULL,8,9,NULL,6, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (19,NULL,8,11,NULL,7, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (20,NULL,3,12,NULL,8, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (21,NULL,7,12,NULL,8, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (22,NULL,5,12,NULL,8, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (23,NULL,12,11,NULL,6, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (24,NULL,12,11,NULL,7, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (25,NULL,13,1,NULL,9, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (31,NULL,21,23,NULL,16, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (32,NULL,21,29,NULL,17, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (33,NULL,27,22,NULL,11, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (34,NULL,25,22,NULL,11, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (35,NULL,22,23,NULL,16, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (36,NULL,22,31,NULL,17, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (37,NULL,23,24,NULL,15, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (38,NULL,27,24,NULL,15, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (39,NULL,24,25,NULL,16, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (40,NULL,24,31,NULL,17, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (41,NULL,23,26,NULL,14, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (42,NULL,25,26,NULL,14, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (43,NULL,26,27,NULL,16, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (44,NULL,26,31,NULL,17, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (45,NULL,23,28,NULL,13, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (46,NULL,27,28,NULL,13, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (47,NULL,25,28,NULL,13, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (48,NULL,28,29,NULL,16, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (49,NULL,28,31,NULL,17, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (50,NULL,23,32,NULL,18, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (51,NULL,27,32,NULL,18, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (52,NULL,25,32,NULL,18, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (53,NULL,32,31,NULL,16, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (54,NULL,32,31,NULL,17, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (55,NULL,33,21,NULL,19, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (56,NULL,25,32,NULL,20, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (57,NULL,27,32,NULL,20, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (58,NULL,23,32,NULL,20, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (91,NULL,61,62,NULL,24, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (92,NULL,61,63,NULL,25, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (93,NULL,62,65,NULL,23, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (94,NULL,65,63,NULL,24, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (95,NULL,65,67,NULL,25, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (96,NULL,62,68,NULL,26, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (97,NULL,68,67,NULL,24, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (98,NULL,68,67,NULL,25, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (99,NULL,69,61,NULL,27, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (131,NULL,81,82,NULL,44, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (132,NULL,81,83,NULL,45, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (133,NULL,82,85,NULL,43, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (134,NULL,85,83,NULL,44, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (135,NULL,85,87,NULL,45, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (146,NULL,82,88,NULL,46, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (147,NULL,88,87,NULL,44, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (148,NULL,88,87,NULL,45, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (149,NULL,89,81,NULL,47, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (170,NULL,42,44,'HasItemInState.closed.aborted=="0"',76, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (171,NULL,42,45,'HasItemInState.closed.completed',75, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (172,NULL,42,46,'HasItemInState.closed.completed=="0"',75, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (173,NULL,48,45,'HasItemInState.closed.aborted',53, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (174,NULL,42,45,'HasItemInState.closed.aborted',76, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (175,NULL,48,44,'AllItemsInState.closed.completed',56, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (176,NULL,48,46,'HasItemInState.closed.completed=="0"',55, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (177,NULL,48,44,'HasItemInState.closed.aborted=="0"',53, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (178,NULL,48,45,'AllItemsInState.closed =="1" \&\& HasItemInState.closed.completed=="1"',78, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (180,NULL,91,92,'AllItemsInState.open.not_running',77, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (181,NULL,43,42,NULL,54, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (182,NULL,91,97,NULL,75, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (183,NULL,91,115,NULL,51, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (184,NULL,48,45,'AllItemsInState.closed =="1" \&\& HasItemInState.closed.aborted=="1"',56, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (185,NULL,48,46,'AllItemsInState.closed.aborted',78, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (186,NULL,48,45,'HasItemInState.closed.completed',55, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (187,NULL,58,55,NULL,63, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (188,NULL,58,57,NULL,69, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (189,NULL,58,93,NULL,68, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (190,NULL,58,54,NULL,62, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (191,NULL,58,59,NULL,81, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (192,NULL,53,93,NULL,80, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (193,NULL,53,57,NULL,79, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (194,NULL,52,53,NULL,64, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (196,NULL,51,59,NULL,81, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (197,NULL,51,210,NULL,67, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (198,NULL,13,9,NULL,83, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (199,NULL,69,63,NULL,84, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (201,NULL,33,29,NULL,86, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (202,NULL,89,83,NULL,87, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (203,NULL,51,98,NULL,69, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (204,NULL,43,43,NULL,75, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (205,NULL,48,96,NULL,89, SYSDATE, 'INIT', NULL, NULL);
--Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
--created_by, modified_dtm, modified_by)
--Values (206,NULL,91,99,NULL,57, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (207,NULL,52,211,NULL,70, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (208,NULL,211,210,NULL,71, SYSDATE, 'INIT', NULL, NULL);

---for SubUser
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (210,NULL,101,102,NULL,94, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (211,NULL,101,103,NULL,95, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (212,NULL,102,105,NULL,93, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (213,NULL,105,103,NULL,94, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (214,NULL,105,107,NULL,95, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (215,NULL,102,108,NULL,96, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (216,NULL,108,107,NULL,94, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (217,NULL,108,107,NULL,95, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (218,NULL,109,101,NULL,97, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (219,NULL,109,103,NULL,98, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (220,NULL,91,49,NULL,52, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (195,NULL,91,90,NULL,50, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (209,NULL,115,113,NULL,57, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (221,NULL,43,116,NULL,101, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (222,NULL,91,116,NULL,101, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, 
created_by, modified_dtm, modified_by)
Values (223,NULL,116,47,NULL,102, SYSDATE, 'INIT', NULL, NULL);

------------------------------------------------------------------
---------   Data fill for table: State_Transition_Event   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: State_Transition_Event   ---------')
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (170,'order_completed',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (171,'order_cancelled',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (172,'order_cancelled',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (173,'order_completed',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (174,'order_completed',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (176,'order_cancelled',507,1, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (177,'order_completed',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (180,'order_rejected',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (182,'order_cancelled',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (183,'order_submitted',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (186,'order_cancelled',507,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (187,'item_cancelled',324,2, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (187,'item_cancelled',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (188,'item_cancelled',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (189,'item_completed',506,1, SYSDATE, 'INIT', NULL, NULL);

Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (190,'item_completed',324,2, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (190,'item_completed',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (191,'item_cancelled',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (192,'item_completed',324,2, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (192,'item_completed',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (193,'item_cancelled',324,2, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (193,'item_cancelled',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (194,'item_warning',324,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (196,'item_rejected',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (203,'item_rejected',506,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (204,'cancel_svc_order',324,1, SYSDATE, 'INIT', NULL, NULL);
Insert into State_Transition_Event(state_transition_id, event_nm, target_class_id, event_seq, created_dtm, created_by, modified_dtm, 
modified_by)
Values (207,'item_warning',324,1, SYSDATE, 'INIT', NULL, NULL);

