--============================================================================
--    $Id: base_st_load_all.sql,v 1.40.2.1 2009/01/15 20:41:09 vivianw Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: AllowableProjTransitions   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: AllowableProjTransitions   ---------')

exec  AddAllowableProjTransitions('locked','open');
exec  AddAllowableProjTransitions('open','locked');
exec  AddAllowableProjTransitions('staged','locked');
exec  AddAllowableProjTransitions('locked','staged');
exec  AddAllowableProjTransitions('open','staged');
exec  AddAllowableProjTransitions('open', 'init');
exec  AddAllowableProjTransitions('locked', 'init');

exec  AddAllowableProjTransitions('staged', 'open');
exec  AddAllowableProjTransitions('aborted', 'open');
exec  AddAllowableProjTransitions('aborted', 'locked');
exec  AddAllowableProjTransitions('aborted', 'staged');

exec  AddAllowableProjTransitions('successful', 'staged');
exec  AddAllowableProjTransitions('provisioning', 'staged');
exec  AddAllowableProjTransitions('synchronizing', 'staged');
exec  AddAllowableProjTransitions('provisioning', 'synchronizing');

exec  AddAllowableProjTransitions('successful','synchronizing');
exec  AddAllowableProjTransitions('synch_failed', 'synchronizing');
exec  AddAllowableProjTransitions('synchronizing', 'synch_failed');

exec  AddAllowableProjTransitions('successful', 'provisioning');
exec  AddAllowableProjTransitions('failed', 'provisioning');
exec  AddAllowableProjTransitions('provisioning', 'failed');


------------------------------------------------------------------
---------   Data fill for table: AllowableTaskTransitions   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: AllowableTaskTransitions   ---------')
--exec  AddAllowableTaskTransitions(from_task,to_task);
---------------------------------------------------
exec  AddAllowableTaskTransitions('available','completed');
exec  AddAllowableTaskTransitions('init','available');


------------------------------------------------------------------
---------   Data fill for table: Action_Tmplt   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Action_Tmplt   ---------')
exec AddActionTmplt('lookup_subntwk','look up subnetwork', 'lookup', 'VLD');
exec AddActionTmplt('create_subntwk','create subnetwork', 'create', 'VLD');
exec AddActionTmplt('delete_subntwk','delete subnetwork', 'delete', 'VLD');
exec AddActionTmplt('assign_subntwk_parent','assign parent subnetwork', 'create', 'VLD');
exec AddActionTmplt('create_card','create card', 'create', 'VLD');
exec AddActionTmplt('set_card_to_subntwk','assign card to subnetwork', 'create', 'VLD');
exec AddActionTmplt('create_port','create port', 'create', 'VLD');
exec AddActionTmplt('create_up_port','create upstream port', 'create', 'VLD');
exec AddActionTmplt('create_down_port','create downstream port', 'create', 'VLD');
exec AddActionTmplt('lookup_port','look up port', 'lookup', 'VLD');
exec AddActionTmplt('lookup_card','look up card', 'lookup', 'VLD');
exec AddActionTmplt('set_port_to_card','assign port to card', 'create', 'VLD');
exec AddActionTmplt('lookup_freq','look up frequency', 'lookup', 'VLD');
exec AddActionTmplt('lookup_freq_grp','look up frequency group', 'lookup', 'VLD');
exec AddActionTmplt('assign_freq','assign frequency', 'create', 'VLD');
exec AddActionTmplt('assign_freq_grp','assign frequency group', 'create', 'VLD');
exec AddActionTmplt('create_pp','create port pair', 'create', 'VLD');
exec AddActionTmplt('assign_port','assign port', 'create', 'VLD');
exec AddActionTmplt('lookup_pp','look up port pair', 'lookup', 'VLD');
exec AddActionTmplt('assign_pp_to_link','assign port pair to link', 'create', 'VLD');
exec AddActionTmplt('lookup_block','look up CIDR block', 'lookup', 'VLD');
exec AddActionTmplt('lookup_subnet','look up subnet', 'lookup', 'VLD');
exec AddActionTmplt('lookup_ip','look up ip', 'lookup', 'VLD');
exec AddActionTmplt('assign_mgmt_ip','Assign management ip', 'create', 'VLD');
exec AddActionTmplt('assign_subnet','Assign Subnet', 'create', 'VLD');
exec AddActionTmplt('release_ip_subnet','Release IP Subnet', 'delete', 'VLD');
exec AddActionTmplt('update_ip_subnet_status','Update IP Subnet Status', 'update', 'VLD');
exec AddActionTmplt('update_subntwk_status','Update Subnetwork Status', 'update', 'VLD');
exec AddActionTmplt('update_parms','Update Parameters', 'update', 'VLD');
exec AddActionTmplt('delete_link','Delete Link', 'delete', 'VLD');
exec AddActionTmplt('create_link','Create Link', 'create', 'VLD');
exec AddActionTmplt('update_link','Update Link', 'update', 'VLD');
exec AddActionTmplt('update_subntwk_parent','Update subnetwork Parent', 'update', 'VLD');
exec AddActionTmplt('update_subnet_status','update subnet status', 'update', 'VLD');
exec AddActionTmplt('delete_subnet','Delete Subnet', 'delete', 'VLD');
exec AddActionTmplt('lookup_link','Lookup Link', 'lookup', 'VLD');
exec AddActionTmplt('create_subnet','create Subnet', 'create', 'VLD');
exec AddActionTmplt('update_subntwk_parms','Update subnetwork Parameters', 'update', 'VLD');
exec AddActionTmplt('lookup_tech_plat','look up technology platform', 'lookup', 'VLD');
exec AddActionTmplt('assign_tech_plat','assign subnetwork to technology platform', 'create', 'VLD');
exec AddActionTmplt('unassign_tech_plat','assign subnetwork to technology platform', 'delete', 'VLD');
exec AddActionTmplt('lookup_card_port','look up card and port', 'lookup', 'VLD');
exec AddActionTmplt('update_card_parms','Update card Parameters', 'update', 'VLD');
exec AddActionTmplt('update_up_port_parms','Update port Parameters', 'update', 'VLD');
exec AddActionTmplt('update_down_port_parms','Update port Parameters', 'update', 'VLD');
exec AddActionTmplt('update_port_freq','Update port frequency', 'update', 'VLD');
exec AddActionTmplt('update_port_freq_grp','Update port frequency group', 'update', 'VLD');
exec AddActionTmplt('create_subntwk_with_children','create subnetwork with children', 'create', 'VLD');
exec AddActionTmplt('delete_card','Delete RF Card', 'delete', 'VLD');
exec AddActionTmplt('create_ip_block','create IP Block', 'create', 'VLD');
exec AddActionTmplt('release_ip_block','Release IP Block', 'delete', 'VLD');
exec AddActionTmplt('update_ip_block_parms','Update subnetwork Parameters', 'update', 'VLD');
exec AddActionTmplt('assign_ip_block','Assign Block', 'create', 'VLD');
exec AddActionTmplt('delete_ip_block','Delete Block', 'delete', 'VLD');
exec AddActionTmplt('update_ip_block_status','update block status', 'update', 'VLD');
exec AddActionTmplt('create_subntwk_from_ext','create subnetwork from external system', 'create', 'VLD');
exec AddActionTmplt('update_subntwk_parms_from_ext','Update subnetwork Parameters from external system', 'update', 'VLD');
exec AddActionTmpltParm('lookup_freq','freq', STMParmId('port_id', Get_Class_Id('stm_project')), 'y');

-- action types for tech platform projects
exec AddActionTmplt('create_tech_plat','create technology platform', 'create', 'VLD');
exec AddActionTmplt('update_tech_plat_parms','modify technology platform', 'update', 'VLD');
exec AddActionTmplt('delete_tech_plat','create technology platform', 'delete', 'VLD');


-- action type for dummys is from technical point of view
exec AddActionTmplt('dummy_with_parm','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_lookup','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_subntwk_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_port_pair_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_return_segment_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_decommission_device','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_select_ip_sne','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_mgmt_mode','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_assigned_type','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_terayon_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_docsis_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_subntwk_children2','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_ip_block_assigned_type','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_multi_select_links','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_multi_select_pp','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_create_links','dummy action', 'update', 'VLD');

------------------------------------------------------------------
---------   Data fill for table: Action_Tmplt_Parm   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Action_Tmplt_Parm---------')
--==========================================================================
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'y');
exec AddActionTmpltParm('create_subntwk_with_children','subntwk', STMParmId('number_of_children', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_with_children', 'subntwk', STMParmId('auto_assign_platform', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('scope_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('scope_subntwk_nm', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'y');
exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('num_of_children', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('num_of_cards', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('is_read_only', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk', NULL, STMParmId('is_create_link', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('other_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('link_direction', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk', NULL,  STMParmID('include_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('child_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('is_visible', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subntwk', 'subntwk', STMParmId('can_create_link', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_subntwk', 'subntwk', STMParmId('can_assign_tech_plat', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('lookup_link','link', STMParmId('to_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('from_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_link','link', STMParmId('link_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_link','link', STMParmId('link_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('to_subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_link','link', STMParmId('from_subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_link','link', STMParmId('link_direction', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('subntwk_id1', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('subntwk_id2', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('to_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('from_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('scope_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',null, STMParmId('is_multi_select', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link', NULL,  STMParmID('include_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link', NULL,  STMParmID('is_con_relief', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('is_read_only', Get_Class_Id('stm_project')), 'y');

--==========================================================================

exec AddActionTmpltParm('lookup_freq','freq', STMParmId('freq_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_freq',null, STMParmId('communication_direction_nm', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_freq',null, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('lookup_freq_grp','freq_grp', STMParmId('freq_grp_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_freq_grp', null, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_freq_grp','freq_grp', STMParmId('port_id', Get_Class_Id('stm_project')) , 'y');

--==========================================================================
exec AddActionTmpltParm('lookup_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_port',NULL, STMParmId('port_direction', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_port',NULL, STMParmId('card_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_port',NULL, STMParmId('external_key', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('lookup_pp','port_pair', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_pp','port_pair', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_pp',NULL, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_pp',null, STMParmId('is_multi_select', Get_Class_Id('stm_project')), 'y');

--==========================================================================
--	lookup_subnet
--==========================================================================
exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('managing_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subnet',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_subnet',NULL, STMParmId('in_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_subnet',NULL, STMParmId('not_in_status', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('lookup_ip','consumable_rsrc', STMParmId('ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_ip',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_ip','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('lookup_card','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_card',null, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_card',NULL, STMParmId('num_of_ports', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_card',NULL, STMParmId('external_key', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'y');
exec AddActionTmpltParm('create_subntwk', 'subntwk', STMParmId('auto_assign_platform', Get_Class_Id('stm_project')), 'y');
--==========================================================================
exec AddActionTmpltParm('lookup_tech_plat','techn_platform', STMParmId('techn_platform_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_tech_plat','techn_platform', STMParmId('techn_platform_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_tech_plat', 'techn_platform', STMParmId('mgmt_mode_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_tech_plat', null, STMParmId('techn_platform_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_tech_plat', null, STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_tech_plat', null, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('assign_tech_plat','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_tech_plat','assoc', STMParmId('techn_platform_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_tech_plat','assoc', STMParmId('mgmt_mode_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_tech_plat','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('unassign_tech_plat','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('unassign_tech_plat','assoc', STMParmId('techn_platform_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('unassign_tech_plat','assoc', STMParmId('mgmt_mode_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('unassign_tech_plat','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('dummy_subntwk_children',NULL, STMParmId('number_of_mac_domains', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_port_pair_children',NULL, STMParmId('num_of_port_pairs', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_return_segment_children',NULL, STMParmId('num_of_return_segments', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_decommission_device',NULL, STMParmId('cascade_delete', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_select_ip_sne',NULL, STMParmId('type_of_ip_server', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_mgmt_mode',NULL, STMParmId('mgmt_mode_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_terayon_children',NULL, STMParmId('create_teralink_requiredCount', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('dummy_docsis_children',NULL, STMParmId('create_mac_domain_requiredCount', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('dummy_multi_select_links',null, STMParmId('number_of_links', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_multi_select_pp',null, STMParmId('number_of_port_pair', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_create_links',null, STMParmId('number_of_links', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_ip_block_assigned_type',NULL, STMParmId('ip_block_assignable_type', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_subntwk_children2',NULL, STMParmId('num_of_children', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_assigned_type',NULL, STMParmId('assigned_type', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('dummy_with_parm', NULL, STMParmId('dummy_parm', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('assign_subntwk_parent','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subntwk_parent','assoc', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subntwk_parent','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
-- action create_card
--==========================================================================
exec AddActionTmpltParm('create_card','card', STMParmId('card_description', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_card','card', STMParmId('srl_num', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_card','card', STMParmId('chassis_slot_number', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_card','card', STMParmId('card_typ_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_card','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_card','card', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_card','card', STMParmId('external_key', Get_Class_Id('stm_project')), 'y');
--==========================================================================
-- Should not exist
exec AddActionTmpltParm('set_card_to_subntwk','assoc', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('set_card_to_subntwk','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('set_card_to_subntwk','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('create_port','port', STMParmId('port_description', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_port','port', STMParmId('upstream_channel_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_port','port', STMParmId('port_direction', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_port','port', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_port','port', STMParmId('external_key', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('create_down_port','port', STMParmId('port_description', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_down_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_down_port','port', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_down_port','port', STMParmId('port_direction', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('create_up_port','port', STMParmId('port_description', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_up_port','port', STMParmId('upstream_channel_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_up_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_up_port','port', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_up_port','port', STMParmId('port_direction', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('assign_freq','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq','assoc', STMParmId('freq_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('assign_freq_grp','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq_grp','assoc', STMParmId('freq_grp_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq_grp','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('comments', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('port_pair_info', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('assign_port','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_port','assoc', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_port','assoc', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_port','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('create_link','link', STMParmId('from_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_link','link', STMParmId('to_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_link','link', STMParmId('link_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_link','link', STMParmId('subntwk_id1', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_link','link', STMParmId('subntwk_id2', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_link','link', STMParmId('link_direction', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('release_ip_subnet','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('release_ip_subnet','assoc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('release_ip_subnet',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('release_ip_subnet',NULL, STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('release_ip_subnet',NULL, STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_sbnt_msk', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_max_hosts', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('assigned_type', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('update_parms','', STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_subntwk_status','', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_status','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('child_subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('old_parent_subntwk_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('update_subnet_status','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subnet_status','consumable_rsrc', STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('update_subnet_status','consumable_rsrc', STMParmId('modify_ip_status_to', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subnet_status',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('delete_subnet','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_subnet','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_subnet',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_subnet',NULL, STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');

--==========================================================================

exec AddActionTmpltParm('delete_subntwk','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_subntwk','subntwk', STMParmId('cascade_delete', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_subntwk','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'y');
exec AddActionTmpltParm('delete_subntwk',NULL, STMParmId('num_of_children', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('delete_subntwk',NULL, STMParmId('num_of_cards', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('delete_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_link','link', STMParmId('is_con_relief', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('update_subntwk_parms','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parms','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parms','subntwk', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parms','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'y');
exec AddActionTmpltParm('update_subntwk_parms','subntwk', STMParmId('old_parms_map', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_card_parms','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_card_parms',NULL, STMParmId('num_of_ports', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('update_card_parms','card', STMParmId('srl_num', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_card_parms','card', STMParmId('chassis_slot_number', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_card_parms','card', STMParmId('card_description', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('update_up_port_parms','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_up_port_parms','port', STMParmId('port_description', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_up_port_parms','port', STMParmId('upstream_channel_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('update_down_port_parms','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_down_port_parms','port', STMParmId('port_description', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_port_freq','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_port_freq','assoc', STMParmId('freq_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_port_freq','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_port_freq_grp','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_port_freq_grp','assoc', STMParmId('freq_grp_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_port_freq_grp','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
-- Update Link
--==========================================================================
exec AddActionTmpltParm('update_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_link','link', STMParmId('status', Get_Class_Id('stm_project')), 'n');

--==========================================================================
-- lookup card and port 
--==========================================================================
exec AddActionTmpltParm('lookup_card_port',null, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_card_port',NULL, STMParmId('num_of_ports', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_card_port',NULL, STMParmId('port_direction', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_card_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_card_port','port', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');

--==========================================================================
-- These Action tmplt parameters will be used by external system to crete
-- subnetwork in STM database. In that case, external key will also need
-- to be updated
--==========================================================================
exec AddActionTmpltParm('create_subntwk_from_ext','subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_from_ext','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_from_ext','subntwk', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk_from_ext','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subntwk_from_ext','subntwk', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subntwk_from_ext','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_subntwk_parms_from_ext','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parms_from_ext','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parms_from_ext','subntwk', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parms_from_ext','subntwk', STMParmId('external_key', Get_Class_Id('subntwk_typ')), 'n');

--===================================create_ip_block=================================================================
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('ip_block_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('mask', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('assignable_hosts', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('start_ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('ip_block_assignable_type', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_ip_block','consumable_rsrc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--===================================lookup_block=================================================================
exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('mask', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('start_ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('managing_subntwk_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_block', NULL, STMParmId('in_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_block', NULL, STMParmId('not_in_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_block', 'consumable_rsrc', STMParmId('ip_block_nm', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_block', 'consumable_rsrc', STMParmId('assignable_hosts', Get_Class_Id('stm_project')), 'y');

--===================================release_ip_block=================================================================
exec AddActionTmpltParm('release_ip_block','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('release_ip_block','assoc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('release_ip_block',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('release_ip_block',NULL, STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('release_ip_block',NULL, STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--====================================update_ip_block_parms======================================
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('ip_block_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('start_ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('mask', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('assignable_hosts', Get_Class_Id('stm_project')),'n');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('modify_ip_status_to', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_parms','consumable_rsrc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--===================================assign_ip_block=================================================================
exec AddActionTmpltParm('assign_ip_block','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_ip_block','assoc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_ip_block','assoc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_ip_block','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--===================================delete_ip_block=================================================================
exec AddActionTmpltParm('delete_ip_block','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_ip_block',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_ip_block',NULL, STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');

--===================================update_ip_block_status=========================================================
exec AddActionTmpltParm('update_ip_block_status','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_status','consumable_rsrc', STMParmId('ip_status', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('update_ip_block_status','consumable_rsrc', STMParmId('modify_ip_status_to', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_ip_block_status',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--===================================delete_card=========================================================
exec AddActionTmpltParm('delete_card','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');

--===================================tech_platform============================
exec AddActionTmpltParm('create_tech_plat', 'techn_platform', STMParmId('techn_platform_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_tech_plat', 'techn_platform', STMParmId('techn_platform_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_tech_plat', 'techn_platform', STMParmId('techn_platform_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_tech_plat', 'techn_platform', STMParmId('external_key', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_tech_plat', 'techn_platform', STMParmId('mgmt_mode_nm', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_tech_plat_parms', 'techn_platform', STMParmId('techn_platform_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_tech_plat_parms', 'techn_platform', STMParmId('techn_platform_nm', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('delete_tech_plat', 'techn_platform', STMParmId('techn_platform_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('delete_tech_plat', 'techn_platform', STMParmId('techn_platform_nm', Get_Class_Id('stm_project')), 'n');

