--============================================================================
--    $Id: base_st_load_cm.sql,v 1.29 2008/08/13 18:57:11 vivianw Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
--set escape on
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Parm   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Parm   ---------')

exec Add_Parm(p_parm_nm => 'tech_platform_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('tech_platform_typ'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'external_key', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'mgmt_ip_address', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

--==========================================================================
--     Resource Selection Algorithm Parmeters
--==========================================================================
exec Add_Parm(p_parm_nm => 'resource_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_selection'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'resource_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_selection'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'resource_tag', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_selection'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'jmf_expr', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_selection'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

--==========================================================================
--     Project Parmeters
--==========================================================================
exec Add_Parm(p_parm_nm => 'cascade_delete', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'modify_mac_domain', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'associate_link_to_port_pair', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'create_and_associate_link_to_port_pair', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'external_key', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'parent_subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'scope_subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_typ2', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_typ3', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'scope_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'scope_subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'child_subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'child_subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'type_of_ip_server', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'link_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'link_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'from_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'from_subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'to_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'to_subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'freq_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'freq_grp_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
--==============================================================================
--	port specific parms
--==============================================================================
exec Add_Parm(p_parm_nm => 'port_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'port_direction', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'port_description', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'upstream_channel_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

--==============================================================================
--	port pair specific parms
--==============================================================================
exec Add_Parm(p_parm_nm => 'port_pair_info', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'comments', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

--==============================================================================
-- obsolete
exec Add_Parm(p_parm_nm => 'communication_direction', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'communication_direction_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_block_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'mask', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_sbnt_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_addr', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_sbnt_msk', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

-- obsolet parm
exec Add_Parm(p_parm_nm => 'subnet_status', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_status', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subnet_ip', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_max_hosts', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'managing_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'card_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_address', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'start_ip_addr', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_sbnt_addr', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'assigned_type', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'ip_address_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'status', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'parent_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'parent_subntwk_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
--=============================================================================
-- card command required parameters (not all)
--=============================================================================
exec Add_Parm(p_parm_nm => 'card_description', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'srl_num', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'chassis_slot_number', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'card_typ_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

--=============================================================================
-- tech_platform required parms
--=============================================================================
exec Add_Parm(p_parm_nm => 'techn_platform_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'techn_platform_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'techn_platform_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'mgmt_mode_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'mgmt_mode_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');
--=============================================================================

exec Add_Parm(p_parm_nm => 'rack_num', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'clmi_port', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'oob_port', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'catalystport', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'port_num', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'port_pair_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');


exec Add_Parm(p_parm_nm => 'num_of_cmtss', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_mac_domain', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'number_of_mac_domains', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_return_segments', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_assoc_pps', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_cards', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_children', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'number_of_children', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_ports', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

-- should change the name card to cards
exec Add_Parm(p_parm_nm => 'num_of_card', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_down_ports', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_up_ports', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'num_of_subnet', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'association_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'link_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'assigned_to', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'upstream_port_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'downstream_port_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'num_of_port_pairs', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'link_direction', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'assign_new_parent', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'assign_ip_subnet', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'consumable_resource_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

--================
-- Ip Config mac
--================
exec Add_Parm(p_parm_nm => 'is_private_ip', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'is_assign_ip_required', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'is_assign_ip_mgmt_required', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'is_assign_ip_subnet_req', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'associate_to_network_cluster', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'do_assign_management_ip_to_cmts', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'do_assign_management_ip_to_mac_layer', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'do_assign_subnet_to_mac_layer', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'do_assign_ip_scope_to_mac_layer', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

--==========================================================================
exec Add_Parm(p_parm_nm => 'create_card', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'create_down_port', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'create_port_pair', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'mac_subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'create_up_port', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

---==========================
-- Modify RF Card Data
---==========================
exec Add_Parm(p_parm_nm => 'modify_card', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'modify_downstream_port', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'modify_upstream_port', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

---==========================
exec Add_Parm(p_parm_nm => 'to_subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'from_subntwk_typ', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'other_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'list_of_IP_service_network_elements', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'subntwk_id1', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');
exec Add_Parm(p_parm_nm => 'subntwk_id2', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

--===================================================================
-- parms for action tmplt lookup_subnet
exec Add_Parm(p_parm_nm => 'in_status', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'not_in_status', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

--===================================================================
-- parms for action tmplt update_subnet_status
exec Add_Parm(p_parm_nm => 'modify_ip_status_to', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--===================================================================

exec Add_Parm(p_parm_nm => 'is_read_only', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'create_teralink_requiredCount', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'create_mac_domain_requiredCount', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'old_parent_subntwk_id', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'old_parms_map', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

--=========================Rogers 2013 Release new parms==============
exec Add_Parm(p_parm_nm => 'auto_assign_platform', p_parm_src_nm => 'stm', p_data_typ_nm => 'String', p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'include_status', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'is_con_relief', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'number_of_links', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'is_multi_select', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--==========================Roger fix for multi select port pair==========
exec Add_Parm(p_parm_nm => 'number_of_port_pair', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--=======================For IP Block Projects defect #2246============================
exec Add_Parm(p_parm_nm => 'ip_block_nm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'ip_block_assignable_type', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'assignable_hosts', p_parm_src_nm => 'stm', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by=> 'INIT');

--======================For create_link project defect #2571===========================
exec Add_Parm(p_parm_nm => 'is_create_link', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'),p_is_required => 'y', p_created_by => 'INIT');

--======================for Generic Projects===========================================
exec Add_Parm(p_parm_nm => 'is_visible', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'can_create_link', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'can_assign_tech_plat', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT', p_is_reserved => 'y');

exec Add_Parm(p_parm_nm => 'dummy_parm', p_parm_src_nm => 'stm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT', p_is_reserved => 'y');

------------------------------------------------------------------
---------   Data fill for table: Parm Permitted Values  ---------
------------------------------------------------------------------
exec PRC_ST_ADD_PARM_PERMITTED_VAL('ip_block_assignable_type', 'root_network');

exec PRC_ST_ADD_PARM_PERMITTED_VAL('status', 'in_service');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('status', 'out_of_service');
------==========================================================================
exec PRC_ST_ADD_PARM_PERMITTED_VAL('ip_status', 'free');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('ip_status', 'reserved');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('ip_status', 'allocated');
------==========================================================================
exec PRC_ST_ADD_PARM_PERMITTED_VAL('modify_ip_status_to', 'free');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('modify_ip_status_to', 'reserved');
------==========================================================================
exec PRC_ST_ADD_PARM_PERMITTED_VAL('mgmt_mode_nm', 'provision');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('mgmt_mode_nm', 'diagnostic');
------==========================================================================
exec PRC_ST_ADD_PARM_PERMITTED_VAL('is_read_only', '0');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('is_read_only', '1');
------==========================================================================
--exec PRC_ST_ADD_PARM_PERMITTED_VAL('create_mac_domain_requiredCount', '0');
--exec PRC_ST_ADD_PARM_PERMITTED_VAL('create_mac_domain_requiredCount', '1');
------==========================================================================
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '0');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '1');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '2');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '3');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '4');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '5');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '6');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '7');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '8');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '9');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '10');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '11');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '12');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '13');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '14');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '15');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '16');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '17');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '18');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '19');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '20');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '21');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '22');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '23');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '24');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '25');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '26');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '27');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '28');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '29');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '30');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '31');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '32');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '33');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '34');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '35');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '36');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '37');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '38');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '39');
exec PRC_ST_ADD_PARM_PERMITTED_VAL('number_of_children', '40');

