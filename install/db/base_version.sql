--============================================================================
--    $Id: base_version.sql,v 1.6 2005/10/11 14:15:11 release Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on

------------------------------------------------------------------
---------   Data fill for table: Version   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Version   ---------')

begin
 PKG_VERSION.PRC_SET_VER(
    P_MOD_NM => 'Core Refdata', 
    P_COMMENT => 'Core Reference Data', 
    P_SETTER => USER,     
    P_SPEC_TITLE => 'SMP',  
    P_SPEC_VENDOR => 'Sigma Systems',  
    P_SPEC_VER => '4', 
    P_IMPL_TITLE => 'base_version',  
    P_IMPL_VENDOR => 'Core Eng', 
    P_IMPL_VER  => '4.4.1.0 build 10 B10', 
    P_BUILT_BY  => 'smpbuild', 
    P_CREATED_BY => 'INIT',  
    P_BUILT_DTM => to_date('03/08/2012 21:25:20', 'dd/mm/yyyy hh24:mi:ss'));  
end;
/
