--============================================================================
--    $Id: bo_indexes.ddl,v 1.2 2005/09/26 16:04:14 dant Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================


CREATE UNIQUE INDEX Bo_alert_PK ON Bo_alert
(
       alert_id                       ASC
);

CREATE INDEX Bo_alert_IX1 ON Bo_alert
(
       trans_id                       ASC
);

CREATE INDEX Bo_alert_IX2 ON Bo_alert
(
       trans_grp_id                   ASC
);

CREATE INDEX Bo_alert_IX3 ON Bo_alert
(
       alert_typ_id                   ASC
);

CREATE UNIQUE INDEX Bo_trans_PK ON Bo_trans
(
       trans_id                       ASC
);

CREATE INDEX Bo_trans_IX1 ON Bo_trans
(
       trans_grp_id                   ASC
);

CREATE UNIQUE INDEX Bo_trans_grp_pk ON Bo_trans_grp
(
       trans_grp_id                   ASC
);

CREATE INDEX Bo_trans_grp_IX1 ON Bo_trans_grp
(
       trans_grp_typ_id               ASC
);

CREATE INDEX Bo_trans_grp_pk_Ix2 ON Bo_trans_grp
(
       external_id                    ASC,
       version                        ASC
);


CREATE INDEX bo_trans_monitor_IX1 ON Bo_trans_monitor
(
       external_id                    ASC,
       version                        ASC
);

CREATE UNIQUE INDEX Ref_trans_grp_type_PK ON Ref_trans_grp_type
(
       trans_grp_typ_id               ASC
);

CREATE UNIQUE INDEX Ref_trans_type_PK ON Ref_trans_type
(
       trans_typ_id                   ASC
);

CREATE UNIQUE INDEX Ref_alert_type_PK ON Ref_trans_type
(
       alert_typ_id                   ASC
);


