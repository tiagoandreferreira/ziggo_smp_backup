--============================================================================
--    $Id: bo_others.ddl,v 1.2 2005/09/26 16:04:14 dant Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

CREATE SEQUENCE bo_alert_seq 
INCREMENT BY 5 
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
CACHE 500 NOCYCLE
NOORDER
;
CREATE SEQUENCE bo_trans_grp_seq 
INCREMENT BY 5
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
CACHE 500 NOCYCLE
NOORDER
;
CREATE SEQUENCE bo_trans_seq 
INCREMENT BY 5
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
CACHE 500 NOCYCLE
NOORDER
;

ALTER TABLE Bo_alert
       ADD  ( CONSTRAINT Bo_alert_PK PRIMARY KEY (alert_id) ) ;


ALTER TABLE Bo_trans
       ADD  ( CONSTRAINT Bo_trans_PK PRIMARY KEY (trans_id) ) ;


ALTER TABLE Bo_trans_grp
       ADD  ( CONSTRAINT Bo_trans_grp_pk PRIMARY KEY (trans_grp_id) ) ;



ALTER TABLE Ref_trans_grp_type
       ADD  ( CONSTRAINT Ref_trans_grp_type_PK PRIMARY KEY (
              trans_grp_typ_id) ) ;


ALTER TABLE Ref_trans_type
       ADD  ( CONSTRAINT Ref_trans_type_PK PRIMARY KEY (trans_typ_id) ) ;

ALTER TABLE Ref_alert_type
       ADD  ( CONSTRAINT Ref_alert_type_PK PRIMARY KEY (alert_typ_id) ) ;


ALTER TABLE Bo_alert
       ADD  ( CONSTRAINT bo_alert_fk3
              FOREIGN KEY (alert_typ_id)
                             REFERENCES Ref_trans_type ) ;


ALTER TABLE Bo_alert
       ADD  ( CONSTRAINT bo_alert_fk2
              FOREIGN KEY (trans_grp_id)
                             REFERENCES Bo_trans_grp ) ;


ALTER TABLE Bo_alert
       ADD  ( CONSTRAINT bo_alert_fk1
              FOREIGN KEY (trans_id)
                             REFERENCES Bo_trans ) ;


ALTER TABLE Bo_trans
       ADD  ( CONSTRAINT bo_trans_fk1
              FOREIGN KEY (trans_grp_id)
                             REFERENCES Bo_trans_grp ) ;


ALTER TABLE Bo_trans
       ADD  ( CONSTRAINT bo_trans_fk3
              FOREIGN KEY (trans_typ_id)
                             REFERENCES Ref_trans_type ) ;


ALTER TABLE Bo_trans
       ADD  ( CONSTRAINT bo_trans_fk2
              FOREIGN KEY (trans_grp_typ_id)
                             REFERENCES Ref_trans_grp_type ) ;


ALTER TABLE Bo_trans_grp
       ADD  ( CONSTRAINT bo_trans_grp_fk1
              FOREIGN KEY (trans_grp_typ_id)
                             REFERENCES Ref_trans_grp_type ) ;


ALTER TABLE Bo_trans_monitor
       ADD  ( CONSTRAINT bo_trans_monitor_fk2
              FOREIGN KEY (alert_typ_id)
                             REFERENCES ref_alert_type ) ;


ALTER TABLE Bo_trans_monitor
       ADD  ( CONSTRAINT bo_trans_monitor_fk3
              FOREIGN KEY (exp_trans_typ_id)
                             REFERENCES Ref_trans_type ) ;


ALTER TABLE Bo_trans_monitor
       ADD  ( CONSTRAINT bo_trans_monitor_fk4
              FOREIGN KEY (trans_typ_id)
                             REFERENCES Ref_trans_type ) ;


ALTER TABLE Bo_trans_monitor
       ADD  ( CONSTRAINT bo_trans_monitor_fk1
              FOREIGN KEY (trans_id)
                             REFERENCES Bo_trans ) ;




