--============================================================================
--    $Id: bo_tables.ddl,v 1.2 2005/09/26 16:04:14 dant Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

CREATE TABLE Bo_alert (
       alert_id             NUMBER(12) NOT NULL,
       trans_grp_id         NUMBER(12) NOT NULL,
       trans_id             NUMBER(12) NOT NULL,
       alert_typ_id         NUMBER(2) NOT NULL,
       alert_status         VARCHAR2(30) NOT NULL,
       created_by           VARCHAR2(40) NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       ownership            VARCHAR2(30) NULL,
       priority_id          NUMBER(2) NULL,
       alert_txt            CLOB NOT NULL,
       comments             VARCHAR2(4000) NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
) LOB (alert_txt) STORE AS 
      ( TABLESPACE  USERS 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  0
        NOCACHE);



CREATE TABLE Bo_trans (
       trans_id             NUMBER(12) NOT NULL,
       trans_grp_id         NUMBER(12) NOT NULL,
       trans_grp_typ_id     NUMBER(2) NOT NULL,
       trans_typ_id         NUMBER(2) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       trans_txt            CLOB NOT NULL,
       search_key_1         VARCHAR2(100) NULL,
       search_key_2         VARCHAR2(100) NULL,
       search_key_3         VARCHAR2(100) NULL,
       search_key_4         VARCHAR2(100) NULL,
       search_key_5         VARCHAR2(100) NULL,
       search_key_6         VARCHAR2(100) NULL,
       search_key_7         VARCHAR2(100) NULL,
       search_key_8         VARCHAR2(100) NULL,
       search_key_9         VARCHAR2(100) NULL,
       search_key_10        VARCHAR2(100) NULL,
       search_date_1        DATE NULL,
       search_date_2        DATE NULL,
       search_date_3        DATE NULL,
       sb_trans_id          VARCHAR2(50) NULL,
       correlation_key    VARCHAR2(20) NULL,
       smp_trans_id        VARCHAR2(50) NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
) LOB (trans_txt) STORE AS 
      ( TABLESPACE  USERS 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  0
        NOCACHE);



CREATE TABLE Bo_trans_grp (
       trans_grp_id         NUMBER(12) NOT NULL,
       external_id          VARCHAR2(50) NOT NULL,
       version              VARCHAR2(32) NULL,
       sub_external_key     VARCHAR2(50) NULL,
       trans_grp_typ_id     NUMBER(2) NOT NULL,
       due_date             DATE NULL,
       status               VARCHAR2(30) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       search_key_1         VARCHAR2(30) NULL,
       search_key_2         VARCHAR2(30) NULL,
       search_key_3         VARCHAR2(30) NULL,
       search_key_4         VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       smp_sub_id           NUMBER(12) NULL
);


CREATE TABLE Bo_trans_monitor (
       trans_id             NUMBER(12) NOT NULL,
       trans_typ_id         NUMBER(2) NOT NULL,
       exp_trans_typ_id     NUMBER(2) NULL,
	alert_typ_id     NUMBER(2) NOT NULL,
       external_id          VARCHAR(50) NOT NULL,
       version              VARCHAR(15) NULL,
       matching_val         VARCHAR2(1000) NULL,
       monitor_flag         VARCHAR2(3) NOT NULL
                                   CHECK (monitor_flag IN ('YES','NO')),
       due_day              DATE NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       ref_alert_id         NUMBER(12) NULL,
       ref_trans_id         NUMBER(12) NULL,
       expression           VARCHAR(100) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
        priority_id          NUMBER(2) NULL
);


CREATE TABLE Ref_trans_grp_type (
       trans_grp_typ_id     NUMBER(2) NOT NULL,
       trans_grp_nm         VARCHAR2(50) NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE Ref_trans_type (
       trans_typ_id         NUMBER(2) NOT NULL,
       trans_typ_nm         VARCHAR2(50) NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE Ref_alert_type (
       alert_typ_id         NUMBER(2) NOT NULL,
       alert_typ_nm         VARCHAR2(50) NOT NULL,
       created_dtm          DATE DEFAULT SYSDATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);



