--============================================================================
--    $Id: cache_sequence.sql,v 1.1.2.1 2011/05/26 16:08:45 davidx Exp $
--
--  PURPOSE of this script to adjust index parameters for the sake of
--  performance
--    Use reverse indexes to spread head block contention
--    Use compression for better performance (less IO)
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set pages 0

spool keep_seq_memory.sql

select 'exec sys.DBMS_SHARED_POOL.KEEP('||''''||sequence_name||''''||','||''''||'Q'||''''||') ; ' from user_sequences;

spool off

spool keep_seq_memory.log

@keep_seq_memory.sql
spool off

