--=============================================================================
--    $Id: chk_stat.sql,v 1.1 2003/03/05 21:30:13 vadimg Exp $
--
--  DESCRIPTION
--
--  Script to report statistics status on a single schema
--  Reports object with missing and stale statistics (if monitoring is on)
--  and collected histograms
--
--  Usage: sqlplus /nolog @chk_stat.sql
--  
-- 
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

Prompt Report statistics status for single schema

accept p_user prompt "Username : "
accept p_pwd  prompt "Password : "  HIDE
accept db     prompt "Database alias: "  
accept spool DEF stat.txt prompt "Spool file name [stat.txt]:  "

connect &&p_user/&&p_pwd@&&db
set serveroutput on size 1000000
set feedback off
set termout off
spool &&spool



declare 

 objtab DBMS_STATS.ObjectTab;
 I number;
 p_username varchar2(30);

 function xtrim(s varchar2, length number) return char is
 begin
   return substr(rpad(s, length, ' '), 1, length);
 end;

 procedure prnhdr is
 begin
   dbms_output.put_line(     'OWNER           TYPE            NAME');
   dbms_output.put_line(     '--------------- --------------- ------------------------------');
 end;

begin
 p_username := user;
 dbms_output.enable(1000000);

 dbms_stats.gather_schema_stats(p_username, cascade=> true, options => 'LIST EMPTY',  objlist =>objtab);
 if objtab.count > 0 then 
   dbms_output.put_line('** OBJECTS WITH MISSING STATISTICS');
   prnhdr;
   for i in objtab.first .. objtab.last loop
     dbms_output.put_line(xtrim(objtab(i).ownname, 15)||' '||
	xtrim(objtab(i).objtype, 15)||' '||
	xtrim(objtab(i).objname, 30)
     );
   end loop;
 else 
   dbms_output.put_line('** NO OBJECTS WITH MISSING STATISTICS FOUND');
   
 end if;
 dbms_stats.gather_schema_stats(p_username, cascade=> true, options => 'LIST STALE',  objlist =>objtab);
 if objtab.count > 0 then 
   dbms_output.new_line;
   dbms_output.put_line(chr(10));
   dbms_output.put_line('** OBJECTS WITH STALE STATISTICS');
   prnhdr;
   for i in objtab.first .. objtab.last loop
     dbms_output.put_line(xtrim(objtab(i).ownname, 15)||' '||
	xtrim(objtab(i).objtype, 15)||' '||
	xtrim(objtab(i).objname, 30)
     );
   end loop;
 else 
   dbms_output.put_line('** NO OBJECTS WITH STALE STATISTICS FOUND');
   
 end if;

 dbms_output.put_line(chr(10));
 dbms_output.put_line('** OBJECT HISTOGRAMS');
 dbms_output.put_line(     'OWNER           NAME                           SIZE');
 dbms_output.put_line(     '--------------- ------------------------------ ----------');
 i := 0;
 for cr in (select OWNER, TABLE_NAME, max(ENDPOINT_NUMBER) ENDPOINT_NUMBER
    from all_TAB_HISTOGRAMS where owner = p_username
     group by OWNER, TABLE_NAME
    having  max(ENDPOINT_NUMBER) > 1) loop
     dbms_output.put_line(xtrim(cr.owner, 15)||' '||
     xtrim(cr.table_name, 30)||' '||cr.endpoint_number);
    i := 1;
 end loop;
 if i = 0 then 
     dbms_output.put_line('** NO HISTOGRAM FOUND');
   
 end if;
end;
/
 
exit;