--============================================================================
--    $Id: cisco_trans_num.sql,v 1.2 2009/02/03 11:59:59 pallavig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on

BEGIN
    EXECUTE IMMEDIATE 'DROP SEQUENCE CISCO_TRANS_NUM';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE SEQUENCE CISCO_TRANS_NUM MINVALUE 0 MAXVALUE 999999999 INCREMENT BY 1 START WITH 0 CACHE 10  ORDER CYCLE;
