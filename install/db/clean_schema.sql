--=============================================================================
--    $Id: clean_schema.sql,v 1.3 2003/09/12 15:38:49 vadimg Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
set termout off
set linesize 133
set pagesize 0
set feedback off
spool cleanup.sql
@get_objects.sql
spool off
@cleanup
exit