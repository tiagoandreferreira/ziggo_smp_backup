--=============================================================================
--    $Id: cleanup_adm_schema.sql,v 1.2 2003/09/04 18:55:48 vadimg Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
set termout off
set heading off
set linesize 133
set pagesize 0
set feedback off

begin
 for cr in (select table_name from user_tables where table_name like 'ADM%' ) loop
    execute immediate 'drop table '||cr.table_name||' cascade constraints';
 end loop;
 for cr in (select sequence_name from user_sequences where sequence_name like 'ADM%') loop
    execute immediate 'drop sequence '||cr.sequence_name;
 end loop;
end;
/
exit
