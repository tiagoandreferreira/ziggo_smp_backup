---------------------------------------------------------------------------
-- FILE INFO
--    $Id: cm_mig_plsql.sql,v 1.1 2003/11/13 22:02:52 vadimg Exp $
--
-- DESCRIPTION
--
-- Migration framework package
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------
create or replace package pkg_mig as

 -- Register new migration/update procedure. Name must be unique.
 procedure add_mig(p_nm varchar2);
 -- Start new migration/update  process. Many processes may be started in parallel, p_thread_id must be unique within mig_nm
 -- Thread remains locked until thread process dies or completes
 procedure start_thread(p_mig_nm varchar2, p_thread_id number);
 -- Log error , performed in a separate transaction context. Always saved to the DB unless critical error
 procedure log_error(p_mig_nm varchar2, p_thread_id number, p_obj_id number, p_sqlcode number, p_sqlerrm varchar2, p_appcode number, p_apperrm varchar2, p_err_parm t_cm_parmval) ;
 -- Thread Acquires a batch for processing. Batch remains locked until thread fails of batch is complete
 --   p_proc_nm  - external procedure name responsible for specific dynamic batch allocation.
 --                Accepts batch_id parameter and throws exception when no more object for the batch found.
 --   Returns either positive batch_id is successful or negative error code. -1 - no more batches to process.
 function acquire_batch(p_mig_nm varchar2, p_thread_id number, p_proc_nm varchar2 default null) return number;
 -- Update migration package state.
 --   Valid states - 'pending', 'ready', 'running', 'failed', 'complete'
 procedure set_mig_state(p_nm varchar2, p_status varchar2);
 -- Update migration process state information.
 --   Valid p_status states - 'running', 'failed', 'complete'
 --   p_batch_id - batch assigned to a thread
 procedure set_thread_state(p_mig_nm varchar2, p_thread_id number, p_batch_id number, p_status varchar2);
 -- Update migration process state information.
 --   Valid p_status states - 'pending', 'ready', 'running', 'failed', 'complete', 'warning'
 --   'warning' means complete with warnings - see error log
 procedure set_batch_state(p_batch_id number, p_status varchar2) ;

end pkg_mig;
/


create or replace package body pkg_mig as

 -- Register new migration/update procedure. Name must be unique.
 procedure add_mig(p_nm varchar2) as
 begin
     insert into mig(MIG_ID, MIG_NM, STATUS) values (MIG_SEQ.nextval, p_nm, 'pending');
 end;
 procedure prc_get_lock(p_lockname varchar2, p_autocommit boolean) as
  lockhandle varchar2(200);
  result   number;
 begin
   DBMS_LOCK.ALLOCATE_UNIQUE (p_lockname, lockhandle);
   result :=   DBMS_LOCK.REQUEST(
   		lockhandle => lockhandle,
   		lockmode   => 6,
        TIMEOUT    => 5,
        release_on_commit  => p_autocommit);
  if result <> 0 then
     raise_application_error(-20001, 'Error reason: dbms_lock result='||result);
  end if;
 end;

 -- Start new migration/update  process. Many processes may be started in parallel, p_thread_id must be unique within mig_nm
 -- Thread remains locked until thread process dies or completes

 procedure start_thread(p_mig_nm varchar2, p_thread_id number) as
   cursor c is select * from mig where mig_nm = p_mig_nm for update;
   cr c%rowtype;
 begin
   open c;
   fetch c into cr;
   close c;
   if cr.status in ('ready', 'running') then
       prc_get_lock('MIG/'||cr.mig_id||'/'||p_thread_id , false);
   else
     if cr.status is null then
        raise_application_error(-20001, 'Active Migration "'||p_mig_nm||'" not found' );
     else
        raise_application_error(-20001, 'Active Migration "'||p_mig_nm||'" not found' );
     end if;
   end if;

   begin
     insert into mig_run(MIG_ID, THREAD_ID, START_DTM, BATCH_CNT, status) values (cr.mig_id, p_thread_id, sysdate, 0, 'running');
   exception
     when others then
        update mig_run set status = 'running' where MIG_ID=cr.mig_id and  THREAD_ID=p_thread_id;
   end;
   commit;
 end;
 -- Log error , performed in a separate transaction context. Always saved to the DB unless critical error
 procedure log_error(p_mig_nm varchar2, p_thread_id number, p_obj_id number, p_sqlcode number, p_sqlerrm varchar2, p_appcode number, p_apperrm varchar2, p_err_parm t_cm_parmval) as
   pragma autonomous_transaction;
   v_mig_id number;
 begin

   select mig_id into v_mig_id from mig where mig_nm = p_mig_nm;

   insert into mig_err(MIG_ID,THREAD_ID,ERR_ID,ERR_DTM,OBJ_ID,ORA_ERR_CODE,ORA_ERR_MSG,APP_ERR_CODE,APP_ERR_MSG)
     values (v_mig_id, p_thread_id, mig_err_seq.nextval, sysdate, p_obj_id, p_sqlcode, p_sqlerrm, p_appcode, p_apperrm);

   insert into mig_err_parm (MIG_ID,THREAD_ID,ERR_ID,NM,VAL)
     select v_mig_id, p_thread_id, mig_err_seq.currval, parm_nm, val from table(cast(p_err_parm as t_cm_parmval));

   commit;
 end;

 -- Thread Acquires a batch for processing. Batch remains locked until thread fails of batch is complete
 function acquire_batch(p_mig_nm varchar2, p_thread_id number, p_proc_nm varchar2 default null) return number as
   cursor c is select * from mig_batch where status = 'ready' and mig_id = (select mig_id from mig where mig_nm = p_mig_nm)
     for update nowait skip locked;
   cr c%rowtype;
   v_batch_id   number;
   procedure add_batch(p_id number) is
     pragma autonomous_transaction;
   begin
     insert into mig_batch(mig_id, batch_id, status) select mig_id, p_id, 'pending' from mig where mig_nm = p_mig_nm;
     commit;
   end;
 begin
    if p_proc_nm is not null then
       select mig_batch_seq.nextval into v_batch_id from dual;
       add_batch(v_batch_id );
       begin
         execute immediate 'begin '||p_proc_nm ||'('||v_batch_id||'); end;';
       exception
         when others then
           rollback;
           delete from mig_batch where batch_id = v_batch_id;
           commit;
           return -1;
       end;
       update mig_batch set status = 'ready' where batch_id = v_batch_id;
    else
       open c;
       fetch c into cr;
       if c%notfound then
          close c;
          return -1;
       end if;
       close c;
       v_batch_id := cr.batch_id;
    end if;

    set_thread_state(p_mig_nm, p_thread_id, v_batch_id, 'running');
    commit;
    return v_batch_id;
 end;

 -- Update migration package state.
 --   Valid states - 'pending', 'ready', 'running', 'failed', 'complete'
 procedure set_mig_state(p_nm varchar2, p_status varchar2) as
   Cursor C is select * from MIG where mig_nm = p_nm for update;
   cr        c%rowtype;
   pragma autonomous_transaction;
   v_status varchar2(30);
 begin
    open c;
    fetch c into cr;
    if c%notfound then
       close c;
       raise_application_error(-20001, 'Migration "'||p_nm||'" not found');
    end if;
    close c;

    if (p_status = 'pending'    and cr.status not in ('pending' )) or
       (p_status = 'ready'    and cr.status not in ('ready', 'pending' )) or
       (p_status = 'running'  and cr.status not in ('ready', 'running' )) or
       (p_status = 'failed'   and cr.status not in ('running', 'failed' )) or
       (p_status = 'complete' and cr.status not in ('running', 'failed' ))
    then
       raise_application_error(-20001, 'MIG State Transition "'||cr.status||'" -> "'||p_status||'" not allowed');
    end if;

    v_status := p_status;

    if p_status =  'complete' and cr.status = 'failed' then
       v_status := 'failed';
    end if;

    update MIG set status = v_status where mig_nm = p_nm ;
    commit;
 end;

 -- Update migration process state information.
 --   Valid p_status states - 'running', 'failed', 'complete'
 --   p_batch_id - batch assigned to a thread
 procedure set_thread_state(p_mig_nm varchar2, p_thread_id number, p_batch_id number, p_status varchar2) as
   v_mig_id number;
   cursor T is select * from mig_run   where mig_id = v_mig_id and thread_id = p_thread_id;
   cursor B is select * from mig_batch where batch_id = p_batch_id for update;
   tr t%rowtype;
   br b%rowtype;
   procedure set_status(l_mig_id number, l_thread_id number, l_status varchar2) as
     pragma autonomous_transaction;
   begin
     update mig_run set status = l_status where mig_id = l_mig_id and thread_id = l_thread_id;
   end;
 begin
    select mig_id into v_mig_id from mig where mig_nm = p_mig_nm;
    open T;
    fetch t into  tr;
    if t%notfound then
       close t;
       raise_application_error(-20001, 'Thread '||p_mig_nm||'/'||p_thread_id||' not found');
    end if;
    close t;

    open b;
    fetch b into  br;
    if b%notfound then
       close b;
       raise_application_error(-20001, 'Batch '||p_batch_id||' not found');
    end if;
    close b;

    if p_status = 'running' then
       if br.status = 'ready' then
          update mig_batch set status = 'running' where batch_id = p_batch_id;
       else
          raise_application_error(-20001, 'Batch '||p_batch_id||'status can''t be set to "running"');
       end if;
       set_status(v_mig_id, p_thread_id, 'running');
    elsif p_status = 'failed' then
       if tr.batch_id = p_batch_id then
          if br.status = 'failed' then
             set_status(v_mig_id, p_thread_id, 'failed');
          else
             set_status(v_mig_id, p_thread_id, 'complete');
          end if;
       else
          raise_application_error(-20001, 'Batch '||p_batch_id||' is not currently allocated to thread '||p_thread_id);
       end if;
    elsif p_status = 'complete' then
       if tr.batch_id = p_batch_id then
          if br.status = 'failed' then
             set_status(v_mig_id, p_thread_id, 'failed');
          else
             set_status(v_mig_id, p_thread_id, 'complete');
          end if;
       else
          raise_application_error(-20001, 'Batch '||p_batch_id||' is not currently allocated to thread '||p_thread_id);
       end if;
    end if;
 end;

 -- Update migration process state information.
 --   Valid p_status states - 'pending', 'ready', 'running', 'failed', 'complete', 'warning'
 --   'warning' means complete with warnings - see error log
 procedure set_batch_state(p_batch_id number, p_status varchar2) as
   cursor B is select * from mig_batch where batch_id = p_batch_id for update;
   br b%rowtype;
 begin
    open b;
    fetch b into  br;
    if b%notfound then
       close b;
       raise_application_error(-20001, 'Batch '||p_batch_id||' not found');
    end if;
    close b;

    if (p_status = 'ready' and br.status not in ('pending')) or
       (p_status = 'pending' and br.status not in ('pending')) or
       (p_status = 'running' and br.status not in ('ready')) or
       (p_status = 'failed' and br.status not in ('running', 'ready')) or
       (p_status = 'complete' and br.status not in ('running')) or
       (p_status = 'warning' and br.status not in ('running')) then
       raise_application_error(-20001, 'Batch status Transition "'||br.status||'" -> "'||p_status||'" not allowed');
    end if;
    update mig_batch set status = p_status where batch_id = p_batch_id ;
 end;

end pkg_mig;
/



exec pkg_mig.add_mig('test');
exec pkg_mig.set_mig_state('test', 'ready');
exec pkg_mig.start_thread('test', 1);

exec pkg_mig.log_error('test', 1, 11, -1555, 'No msg', 0, '', t_cm_parmval(o_cm_parmval('name', 'value'))) ;
