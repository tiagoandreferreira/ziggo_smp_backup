---------------------------------------------------------------------------
-- FILE INFO
--    $Id: cm_mig_sql.sql,v 1.1 2003/11/13 22:02:52 vadimg Exp $
--
-- DESCRIPTION
--
-- Tables for migration framework
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------


begin
 execute immediate 'CREATE OR REPLACE '||
 'TYPE o_cm_parmval AS OBJECT (       '||
 '  parm_nm                       VARCHAR2 (255), '||
 '  val                           VARCHAR2 (255))';
exception
 when others then
  null;
end;
/

begin
  execute immadiate 'CREATE OR REPLACE TYPE t_cm_parmval AS TABLE OF o_cm_parmval';
exception
 when others then
  null;
end;
/


CREATE TABLE MIG (
       mig_id               NUMBER(12) NOT NULL,
       mig_nm               VARCHAR2(255) NOT NULL,
       status               VARCHAR2(30) NOT NULL
                                   CONSTRAINT mig_mig_status2
                                          CHECK (status IN ('pending', 'ready', 'running', 'failed', 'complete'))
);


CREATE TABLE MIG_BATCH (
       batch_id             NUMBER(12) NOT NULL,
       mig_id               NUMBER(12) NOT NULL,
       status               VARCHAR2(30) NOT NULL
                                   CONSTRAINT mig_batch_status2
                                          CHECK (status IN ('pending', 'ready', 'running', 'failed', 'complete', 'warning')),
       start_dtm            DATE NULL,
       end_dtm              DATE NULL
);


CREATE TABLE MIG_ERR (
       mig_id               NUMBER(12) NOT NULL,
       thread_id            NUMBER(12) NOT NULL,
       err_id               NUMBER(12) NOT NULL,
       err_dtm              DATE NOT NULL,
       obj_id               NUMBER(12) NULL,
       ora_err_code         NUMBER NULL,
       ora_err_msg          VARCHAR2(4000) NULL,
       app_err_code         NUMBER(12) NULL,
       app_err_msg          VARCHAR2(4000) NULL
);


CREATE TABLE MIG_ERR_PARM (
       mig_id               NUMBER(12) NOT NULL,
       thread_id            NUMBER(12) NOT NULL,
       nm                   VARCHAR2(255) NOT NULL,
       err_id               NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NULL
);


CREATE TABLE MIG_RUN (
       mig_id               NUMBER(12) NOT NULL,
       thread_id            NUMBER(12) NOT NULL,
       start_dtm            DATE NOT NULL,
       end_dtm              DATE NULL,
       batch_cnt            NUMBER(12) NOT NULL,
       batch_id             NUMBER(12) NULL,
       status               VARCHAR2(30) NOT NULL
);





CREATE SEQUENCE MIG_BATCH_SEQ
NOMAXVALUE
NOMINVALUE
NOCYCLE
NOORDER
;
CREATE SEQUENCE MIG_ERR_SEQ
NOMAXVALUE
NOMINVALUE
NOCYCLE
NOORDER
;
CREATE SEQUENCE MIG_RUN_SEQ
NOMAXVALUE
NOMINVALUE
NOCYCLE
NOORDER
;
CREATE SEQUENCE MIG_SEQ
NOMAXVALUE
NOMINVALUE
NOCYCLE
NOORDER
;

ALTER TABLE MIG
       ADD  ( CONSTRAINT MIG_PK PRIMARY KEY (mig_id) ) ;


ALTER TABLE MIG_BATCH
       ADD  ( CONSTRAINT MIG_BATCH_PK PRIMARY KEY (batch_id) ) ;


ALTER TABLE MIG_ERR
       ADD  ( CONSTRAINT MIG_ERR_PK PRIMARY KEY (mig_id, thread_id,
              err_id) ) ;


ALTER TABLE MIG_ERR_PARM
       ADD  ( CONSTRAINT MIG_ERR_PARM_PK PRIMARY KEY (mig_id,
              thread_id, nm, err_id) ) ;


ALTER TABLE MIG_RUN
       ADD  ( CONSTRAINT MIG_RUN_PK PRIMARY KEY (mig_id, thread_id) ) ;


ALTER TABLE MIG_BATCH
       ADD  ( CONSTRAINT MIG_BATCH_FK1
              FOREIGN KEY (mig_id)
                             REFERENCES MIG ) ;


ALTER TABLE MIG_ERR
       ADD  ( CONSTRAINT MIG_ERR_FK1
              FOREIGN KEY (mig_id, thread_id)
                             REFERENCES MIG_RUN ) ;


ALTER TABLE MIG_ERR_PARM
       ADD  ( CONSTRAINT MIG_ERR_PARM_FK1
              FOREIGN KEY (mig_id, thread_id, err_id)
                             REFERENCES MIG_ERR ) ;


ALTER TABLE MIG_RUN
       ADD  ( CONSTRAINT MIG_RUN_FK2
              FOREIGN KEY (batch_id)
                             REFERENCES MIG_BATCH ) ;


ALTER TABLE MIG_RUN
       ADD  ( CONSTRAINT MIG_RUN_FK1
              FOREIGN KEY (mig_id)
                             REFERENCES MIG ) ;

