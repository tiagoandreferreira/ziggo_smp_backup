#================================================================================================
#
#  FILE INFO
#
#    $Id: cm_plsprof.sql,v 1.3 2003/03/19 22:30:37 vadimg Exp $
#
#  DESCRIPTION
#
#    Create callback plugin to enable/disable PL/SQL profiling in Weblogic session
#
#    Any session can update PLSQL_PROFILER_STATE and set STATE to 0 or 1 (enable/disable)
#    Weblogic connection test must be enabled and using v_dummy view.
#    When Weblogic tests connection, select from v_dummy enables/disables plsql profile
#
#  PARAMETERS
#    Must be executed as SMP schema owner
#    SMP schema owner must be granted CREATE ANY CONTEXT privilege.
#    plsql profiling must be enabled before running this script (see related scripts)
#
#  RELATED SCRIPTS
#    Oracle version: 8i and higher
#    ?/rdbms/admin/dbmspbp.sql
#    ?/rdbms/admin/prvtpbp.sql
#    ?/rdbms/admin/proftab.sql
#
#  REVISION HISTORY
#  * Based on CVS log
#================================================================================================


CREATE TABLE PLSQL_PROFILER_STATE(
STATE  NUMBER
);

INSERT INTO PLSQL_PROFILER_STATE VALUES(0);

CREATE or replace CONTEXT CTX_PROFILER USING FN_DUMMY;
/

CREATE TYPE TB_VARCHAR AS TABLE OF VARCHAR2(10);
/

CREATE OR REPLACE FUNCTION FN_DUMMY RETURN TB_VARCHAR
IS
 TAB TB_VARCHAR := TB_VARCHAR('X');
 N NUMBER;
BEGIN
  BEGIN
    SELECT STATE INTO N FROM PLSQL_PROFILER_STATE;
    IF N <> NVL(SYS_CONTEXT('CTX_PROFILER', 'STATE'), 0) THEN
        DBMS_SESSION.SET_CONTEXT('CTX_PROFILER', 'STATE', N);
        IF N = 1 THEN
                DBMS_PROFILER.START_PROFILER(TO_CHAR(SYSDATE, 'DD.MM.YYYY HH24:MI')||
                   '/'||SYS_CONTEXT('USERENV', 'SESSIONID') , '', N);
        ELSE
                DBMS_PROFILER.STOP_PROFILER ;
                DBMS_PROFILER.FLUSH_DATA ;
        END IF;
    END IF;
  END;
 RETURN TAB;
END;
/

CREATE VIEW V_DUMMY AS SELECT * FROM TABLE(FN_DUMMY);

create or replace public synonym V_DUMMY for V_DUMMY;

grant select on V_DUMMY to public;


create or replace procedure toggle_profile(p_state number) as
begin
  update PLSQL_PROFILER_STATE set state = p_state;
  commit;
end;
/
grant execute on toggle_profile to public;


grant select, insert, update, delete on PLSQL_PROFILER_DATA to public;
grant select, insert, update, delete on PLSQL_PROFILER_RUNS to public;
grant select, insert, update, delete on PLSQL_PROFILER_UNITS to public;
create or replace public synonym PLSQL_PROFILER_DATA  for PLSQL_PROFILER_DATA;
create or replace  public synonym PLSQL_PROFILER_RUNS  for PLSQL_PROFILER_RUNS ;
create or replace  public synonym PLSQL_PROFILER_UNITS for PLSQL_PROFILER_UNITS;


