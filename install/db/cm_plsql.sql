--==============================================================================
--    $Id: cm_plsql.sql,v 1.79 2007/09/20 12:13:28 davidx Exp $
--
--  DESCRIPTION
--    SQL file to create the required stored procedures and functions in
--    the CM schema.
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--==============================================================================

SPOOL cm_plsql.log
-----------------------------------------------------------------------
-- Classes
-----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE syncRefClass (
   i_class_id           IN   NUMBER,
   i_class_nm           IN   VARCHAR2
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_class
    WHERE class_id = i_class_id;

   IF c = 1
   THEN
      UPDATE ref_class
         SET class_nm = i_class_nm
       WHERE class_id = i_class_id;
   ELSE
      INSERT INTO ref_class
                  (class_id, max_obj_id, class_nm, created_by,created_dtm)
           VALUES (i_class_id, 1, i_class_nm, 'sync',sysdate);
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE ADD_CLASS (
   p_class_nm    IN ref_class.class_nm%TYPE,
   p_created_by  IN ref_class.created_by%TYPE,
   p_db_tbl_nm   IN ref_class.db_tbl_nm%TYPE default null
   )
   IS
--
-- MODIFICATION HISTORY
-- Person         Date       Comments
-- ---------      ------     -------------------------------------------
-- Parmi Sahota   2001-09-21 Procedure creation

   l_class_id ref_class.class_id%TYPE;

BEGIN
   SELECT REF_CLASS_SEQ.nextval into l_class_id FROM DUAL;
   INSERT INTO ref_class (class_id, class_nm, db_tbl_nm, created_by)
   VALUES (l_class_id, p_class_nm, p_db_tbl_nm, p_created_by);

END;
/


CREATE OR REPLACE
function fn_cm_get_class_id(p_class_nm ref_class.class_nm%type)
return ref_class.class_id%type is
  cursor c_class is
  select class_id
  from   ref_class
  where  class_nm = p_class_nm;

  l_class_id  ref_class.class_id%type := 0;
begin
  for r_class in c_class loop
    l_class_id := r_class.class_id;
  end loop;
  return l_class_id;
end fn_cm_get_class_id;
/

CREATE OR REPLACE  FUNCTION GET_CLASS_ID  (p_class_nm
    IN ref_class.class_nm%TYPE)
  RETURN REF_CLASS.class_id%TYPE IS
  l_class_id ref_class.class_id%TYPE;
BEGIN

  if length(p_class_nm) <= 30 then

     select sys_context('CTX_CLASS', p_class_nm)  into l_class_id from v_dual;

     if l_class_id is not null then
        return l_class_id;
     end if;
  end if;

  SELECT class_id into l_class_id from ref_class WHERE class_nm = p_class_nm;
  if length(p_class_nm) <= 30 then
     prc_smp_ctx('CTX_CLASS', p_class_nm, l_class_id);
  end if;

  return l_class_id;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
END;
/

CREATE OR REPLACE FUNCTION fn_cm_get_class_nm(p_class_id IN ref_class.class_id%TYPE)
  RETURN REF_CLASS.class_nm%TYPE IS
  l_class_nm ref_class.class_nm%TYPE;
BEGIN
  SELECT class_nm into l_class_nm from ref_class WHERE class_id = p_class_id;
  RETURN l_class_nm;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
END;
/

-----------------------------------------------------------------------
-- Labels
-----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE Add_Lbl (
                              p_class_id     IN lbl.class_id%TYPE,
                              p_obj_nm       IN lbl.obj_nm%TYPE,
                              p_locale_cd       IN lbl.locale_cd%TYPE,
                              p_lbl_txt        IN lbl.lbl_txt%TYPE,
                              p_created_by   IN lbl.created_by%TYPE
                                   ) is
BEGIN
    INSERT INTO Lbl (
        CLASS_ID,
        OBJ_NM,
        LOCALE_CD,
        LBL_TXT,
        CREATED_DTM,
        CREATED_BY
        )
    VALUES (
        p_class_id,
        p_obj_nm,
        p_locale_cd,
        p_lbl_txt,
        SYSDATE,
        p_created_by);
END;
/

CREATE OR REPLACE FUNCTION Get_Lbl (
                              p_class_id     IN lbl.class_id%TYPE,
                              p_obj_nm       IN lbl.obj_nm%TYPE,
                              p_locale       IN lbl.locale_cd%TYPE,
                              p_class_nm     IN ref_class.class_nm%TYPE  DEFAULT NULL
                                   )
    RETURN lbl.lbl_txt%TYPE IS
    lbl_text    lbl.lbl_txt%TYPE;
    l_class_id  lbl.class_id%TYPE;
BEGIN
    IF p_class_id is NULL THEN
      l_class_id := get_class_id(p_class_nm => p_class_nm);
      IF p_class_id = 0 THEN RAISE NO_DATA_FOUND;
      END IF;
    ELSE
      l_class_id := p_class_id;
    END IF;

    SELECT lbl_txt INTO lbl_text FROM lbl WHERE class_id = l_class_id ;
    RETURN lbl_text;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
END;
/

-----------------------------------------------------------------------
-- PARMS
-----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE Add_Parm (
  p_parm_nm         IN PARM.parm_nm%TYPE,
  p_class_id        IN PARM.class_id%TYPE,
  p_data_typ_nm     IN PARM.data_typ_nm%TYPE,
  p_created_by      IN PARM.created_by%TYPE,
  p_conversion_typ  IN PARM.conversion_typ%TYPE DEFAULT NULL,
  p_parm_id         IN PARM.parm_id%TYPE  DEFAULT NULL,
  p_is_required     IN PARM.is_required%TYPE  DEFAULT NULL,
  p_dflt_val        IN PARM.dflt_val%TYPE  DEFAULT NULL,
  p_is_unique       IN PARM.is_unique%TYPE  DEFAULT NULL,
  p_parm_src_nm     IN varchar2  DEFAULT NULL,
  p_is_reserved     IN PARM.is_reserved%TYPE  DEFAULT NULL,
  p_is_primary_key  IN PARM.is_primary_key%TYPE  DEFAULT NULL,
  p_is_read_only    IN PARM.is_read_only%TYPE  DEFAULT NULL,
  p_capture_seq_num IN PARM.capture_seq_num%TYPE  DEFAULT NULL,
  p_base_parm_id    IN PARM.base_parm_id%TYPE  DEFAULT NULL,
  p_object_id       IN PARM.object_id%TYPE  DEFAULT NULL,
  p_db_col_nm       IN PARM.db_col_nm%TYPE  DEFAULT NULL,
  p_jmf_dflt_val    IN PARM.jmf_dflt_val%TYPE  DEFAULT NULL,
  p_jmf_src         IN PARM.jmf_src%TYPE  DEFAULT NULL,
  p_jmf_display_format IN PARM.jmf_display_format%TYPE  DEFAULT NULL
) IS

  l_parm_id  PARM.PARM_ID%TYPE;

BEGIN
  l_parm_id := p_parm_id;

  IF p_parm_id IS NULL THEN
    SELECT PARM_SEQ.nextval into l_parm_id FROM DUAL;
  END IF;

    INSERT INTO PARM(
      parm_nm         ,
      class_id        ,
      data_typ_nm     ,
      created_by      ,
      conversion_typ  ,
      parm_id         ,
      is_required     ,
      dflt_val        ,
      is_unique       ,
      is_reserved     ,
      is_primary_key  ,
      is_read_only    ,
      capture_seq_num ,
      base_parm_id    ,
      object_id       ,
      db_col_nm       ,
      jmf_dflt_val    ,
      jmf_src         ,
      jmf_display_format,
      created_dtm     )
    VALUES (
      p_parm_nm         ,
      p_class_id        ,
      p_data_typ_nm     ,
      p_created_by      ,
      p_conversion_typ  ,
      l_parm_id         ,
      p_is_required     ,
      p_dflt_val        ,
      p_is_unique       ,
      p_is_reserved     ,
      p_is_primary_key  ,
      p_is_read_only    ,
      p_capture_seq_num ,
      p_base_parm_id    ,
      p_object_id       ,
      p_db_col_nm       ,
      p_jmf_dflt_val    ,
      p_jmf_src         ,
      p_jmf_display_format,
      SYSDATE           );
EXCEPTION
 when others then 
  raise_application_error(-20001, 'Failed parm_nm/class/objid='
   ||p_parm_nm||'/'||p_class_id||'/'||p_object_id|| chr(10)||
   sqlerrm);
END;
/

CREATE OR REPLACE
function fn_cm_get_parm_id(
  p_class_id  parm.class_id%type,
  p_parm_nm   parm.parm_nm%type)
return parm.parm_id%type is
  cursor c_parm is
  select parm_id
  from   parm
  where  class_id = p_class_id
  and    parm_nm = p_parm_nm;

  l_parm_id  parm.parm_id%type := 0;
begin
  for r_parm in c_parm loop
    l_parm_id := r_parm.parm_id;
  end loop;
  return l_parm_id;
end fn_cm_get_parm_id;
/

CREATE OR REPLACE  FUNCTION GET_PARM_ID  (
    p_parm_nm IN  parm.parm_nm%TYPE,
    p_class_id IN parm.class_id%TYPE,
    p_object_id in parm.object_id%TYPE DEFAULT NULL)
  RETURN parm.parm_id%TYPE IS

  l_parm_id parm.parm_id%TYPE;
BEGIN

  IF p_object_id is null then
     SELECT parm_id into l_parm_id
        from parm
       WHERE parm_nm = p_parm_nm AND
             class_id = p_class_id AND
             object_id is null;
  ELSE
     SELECT parm_id into l_parm_id
        from parm
       WHERE parm_nm = p_parm_nm AND
             class_id = p_class_id AND
             object_id = p_object_id;
  END IF;

  RETURN l_parm_id;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
END;
/


-----------------------------------------------------------------------
-- PARM PERMITTED VALUES
-----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE PRC_CM_ADD_PARM_PERMITTED_VAL
   ( p_parm_id IN parm_permitted_val.parm_id%type,
     p_val IN parm_permitted_val.val%type,
     p_created_by IN parm_permitted_val.created_by%type )
   IS
BEGIN
    insert into parm_permitted_val(parm_id, val, created_by)
    values (p_parm_id, p_val, p_created_by);
END;
/




-----------------------------------------------------------------------
-- NextObjId
-----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE NextObjId (i_class_name IN varchar2,
                                       nextObjId OUT number)
is
    gg number;
begin
    select max_obj_id INTO nextObjId from Ref_Class
        where class_nm = i_class_name ;
    update ref_class set max_obj_id = max_obj_id + 1
        where class_nm = i_class_name;

--EXCEPTION
  --when no_data_found then return 0;
END;
/

-----------------------------------------------------------------------
-- Version Numbering
-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION get_next_version RETURN NUMBER IS
  l_next_ver_num number;
BEGIN
  SELECT SAMP_VER_SEQ.NEXTVAL INTO l_next_ver_num FROM DUAL;
  RETURN l_next_ver_num;
END;
/

-----------------------------------------------------------------------
-- Package to allow recompilation of invalid objects
-----------------------------------------------------------------------
CREATE OR REPLACE
PACKAGE pkg_recompile_all
  IS
   PROCEDURE recomp;
END PKG_RECOMPILE_ALL; -- Package spec
/
CREATE OR REPLACE
PACKAGE BODY pkg_recompile_all
IS
   FUNCTION get_invalid_count
      RETURN NUMBER
   IS
      v_count   NUMBER (12);
   BEGIN
      SELECT COUNT (user_objects.object_name)
        INTO v_count
        FROM user_objects
       WHERE user_objects.status = 'INVALID';

      RETURN v_count;
   END;

   FUNCTION get_obj_status (
      p_object   user_objects.object_name%TYPE,
      p_type     user_objects.object_type%TYPE
   )
      RETURN user_objects.status%TYPE
   IS
      result   user_objects.status%TYPE;
   BEGIN
      SELECT user_objects.status
        INTO result
        FROM user_objects
       WHERE user_objects.object_name = p_object
         AND user_objects.object_type = p_type;

      RETURN result;
   END;

   PROCEDURE show_err
   IS
      v_name   user_errors.NAME%TYPE;
      v_type   user_errors.TYPE%TYPE;

      CURSOR c_err
      IS
         SELECT DISTINCT user_errors.NAME, user_errors.TYPE
                    FROM user_errors;

      CURSOR c_err_info
      IS
         SELECT   line, position, text
             FROM user_errors
            WHERE NAME = v_name AND TYPE = v_type
         ORDER BY line, position;
   BEGIN
      FOR r_err IN c_err
      LOOP
         v_name := r_err.NAME;
         v_type := r_err.TYPE;
         DBMS_OUTPUT.put_line (   'ERROR found in '
                               || v_type
                               || ' '
                               || v_name);

         FOR r_err_text IN c_err_info
         LOOP
            DBMS_OUTPUT.put_line (
                  'Line '
               || r_err_text.line
               || 'Col '
               || r_err_text.position
               || ' '
               || r_err_text.text
            );
         END LOOP;
      END LOOP;
   END show_err;

   PROCEDURE recomp_obj (
      p_type          user_objects.object_type%TYPE,
      p_object_name   user_objects.object_name%TYPE
   )
   IS
      sqlstm   VARCHAR (1999);
   BEGIN
      if p_type = 'PACKAGE BODY' then 
       sqlstm :='ALTER PACKAGE '|| p_object_name||' COMPILE BODY';
      elsif p_type = 'TYPE BODY' then 
       sqlstm :='ALTER TYPE '|| p_object_name||' COMPILE BODY';
      else
       sqlstm :='ALTER '|| p_type ||' '|| p_object_name||' COMPILE';
      end if;

      DBMS_OUTPUT.put_line (   'SQL statement '
                            || sqlstm);
      EXECUTE IMMEDIATE sqlstm;
   ---DBMS_DDL.alter_compile (p_type, NULL, p_object_name);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END recomp_obj;

   PROCEDURE recompile_depy (
      p_object_name   user_objects.object_name%TYPE,
      p_type          user_objects.object_type%TYPE
   )
   IS
      v_object_name   user_objects.object_name%TYPE;
      v_object_type   user_objects.object_type%TYPE;

      CURSOR c_depy
      IS
         SELECT user_dependencies.NAME, user_dependencies.TYPE
           FROM user_dependencies
          WHERE user_dependencies.referenced_name = p_object_name
            AND user_dependencies.referenced_type = p_type;
   BEGIN
      FOR depy_rec IN c_depy
      LOOP
         v_object_name := depy_rec.NAME;
         DBMS_OUTPUT.put_line (   'Depy object name is '
                               || v_object_name);
         v_object_type := depy_rec.TYPE;
         DBMS_OUTPUT.put_line (   'Depy object type is '
                               || v_object_type);

         -- Is the object invalid
         IF get_obj_status (v_object_name, v_object_type) = 'INVALID'
         THEN
            DBMS_OUTPUT.put_line (
                  'Object '
               || v_object_name
               || ' is invalid'
            );
            recomp_obj (v_object_type, v_object_name);
         END IF;
      END LOOP;
   END recompile_depy;

   PROCEDURE recomp
   IS
      v_count         NUMBER (12);
      v_new_count     NUMBER (12);
      v_object_name   user_objects.object_name%TYPE;
      v_object_type   user_objects.object_type%TYPE;

      CURSOR c_invalid
      IS
         SELECT user_objects.object_name, user_objects.object_type,
                user_objects.status
           FROM user_objects
          WHERE user_objects.status = 'INVALID';
   BEGIN
      dbms_output.enable(1000000);
      LOOP
         v_count := get_invalid_count ();
         DBMS_OUTPUT.put_line (
               'Total invalid count is '
            || TO_CHAR (v_count)
         );

         IF v_count = 0
         THEN
            EXIT;
         END IF;

         IF v_count > 0
         THEN
            FOR rec IN c_invalid
            LOOP
               v_object_name := rec.object_name;
               DBMS_OUTPUT.put_line (
                     'Invalid Object name is '
                  || v_object_name
               );
               v_object_type := rec.object_type;
               DBMS_OUTPUT.put_line (
                     'Invalid Object name is '
                  || v_object_type
               );
               -- Recompile dependents
               recompile_depy (v_object_name, v_object_type);
               recomp_obj (v_object_type, v_object_name);
            END LOOP;

            IF v_count = get_invalid_count ()
            THEN
               DBMS_OUTPUT.put_line (
                     'Could not complile '
                  || get_invalid_count ()
                  || ' invalid object(s)'
               );
               dbms_output.put_line('ERRORS FOUND');
               ---dbms_output.put_line('see recomp_err.log');
               --show_err;
               EXIT;
            END IF;
         END IF;
      END LOOP;

/*   EXCEPTION
   WHEN exception_name THEN
          statements ;*/
   END recomp;
END pkg_recompile_all;
/

-----------------------------------------------------------------------
-- Common Objects and Collections
-----------------------------------------------------------------------
BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE t_cm_parmval FORCE';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE o_cm_parmval FORCE';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/


CREATE OR REPLACE
TYPE o_cm_parmval AS OBJECT (
   parm_nm                       VARCHAR2 (255),
   val                           VARCHAR2 (255));
/

CREATE OR REPLACE
TYPE t_cm_parmval AS TABLE OF o_cm_parmval
/

-- Add version repository support VG
@cm_version.sql
-----------------------------------------------------------------------
-- Data Model FIXES
-----------------------------------------------------------------------

--ECHO 'RUNNING RECOMPILE';

EXEC DBMS_OUTPUT.enable(1000000);

EXEC PKG_RECOMPILE_ALL.RECOMP;

SET LINE 100
SET FEEDBACK OFF
COLUMN TEXT FORMAT A40 WOR
SET RECSEP OFF
BREAK ON NAME SKIP 1 NODUP

SELECT NAME, LINE, POSITION, TEXT
    FROM USER_ERRORS
    ORDER BY NAME, LINE, POSITION;

begin
 PKG_VERSION.PRC_SET_VER('Core DB', 'Core tables and procedures', 'PS',
    'SMP', 'Sigma Systems', '4',
    'cm_plsql', 'Core Eng', '4.4.1.0 build 10 B10',
    'smpbuild', 'Apache Ant 1.5.1', to_date('03/08/2012 21:25:20', 'dd/mm/yyyy hh24:mi:ss'));
end;
/

SET FEEDBACK ON

CREATE OR REPLACE PROCEDURE SET_APPL_INFO(p_module varchar2,p_action varchar2 default NULL) IS
/******************************************************************************
   NAME:       SET_APPL_INFO
   PURPOSE: Set application info in V$SESSION table    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10/14/2005          1. Created this procedure.

   NOTES:
******************************************************************************/
BEGIN
   dbms_application_info.set_module(substr(p_module,1,42),substr(p_action,1,32));
   EXCEPTION
       WHEN OTHERS THEN
       NULL;
END SET_APPL_INFO;
/


spool off

