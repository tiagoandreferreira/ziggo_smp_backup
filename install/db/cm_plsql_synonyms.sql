--=============================================================================
--    $Id: cm_plsql_synonyms.sql,v 1.2 2001/08/29 17:38:00 germans Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
set termout off
set heading off
set linesize 133
set pagesize 0
set feedback off
spool cm_pl_synonyms.sql
@get_cm_pl_synonyms.sql
spool off
spool cm_pl_names.sql
@get_cm_pl_names.sql
spool off
