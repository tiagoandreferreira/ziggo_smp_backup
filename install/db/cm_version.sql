/*=========================================================================

  FILE INFO

    $Id: cm_version.sql,v 1.5 2003/09/24 19:01:11 vadimg Exp $

  DESCRIPTION

    PKG_VERSION is a set of procedures to maintain and query component
    version repository


  NOTES

  REVISION HISTORY
  * Based on CVS log
========================================================================*/


CREATE OR REPLACE PACKAGE PKG_VERSION AS
  PROCEDURE PRC_SET_VER(P_MOD_NM VARCHAR2, P_COMMENT VARCHAR2, P_SETTER VARCHAR2,
    P_SPEC_TITLE VARCHAR2, P_SPEC_VENDOR VARCHAR2, P_SPEC_VER VARCHAR2,
    P_IMPL_TITLE VARCHAR2, P_IMPL_VENDOR VARCHAR2, P_IMPL_VER VARCHAR2,
    P_BUILT_BY   VARCHAR2, P_CREATED_BY VARCHAR2 , P_BUILT_DTM DATE);
  -- PURPOSE
  --  To store specific module's version
  -- INPUT PARAMETERS
  --   P_MOD_NM  - module's name. it must uniquely identify tracked module, 100 char max
  --               Example DB.STM.PROJ_VERSIONING
  --   P_MOD_VER - module version, must be derived from CVS
  --   P_COMMENT - auxillary ver info
  --   P_SETTER  - "who" installed the module
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  PROCEDURE PRC_RM_MOD(P_MOD_NM VARCHAR2);
  -- PURPOSE
  --  To uninstall the module
  -- INPUT PARAMETERS
  --   P_MOD_NM  - module's name. it must uniquely identify tracked module, 100 char max
  --               Example DB.STM.PROJ_VERSIONING
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  FUNCTION FNC_GET_VER(P_MOD_NM VARCHAR2) RETURN VARCHAR2;
  -- PURPOSE
  --  Retrieve current version of the module
  -- INPUT PARAMETERS
  --   P_MOD_NM  - module's name. it must uniquely identify tracked module, 100 char max
  --               Example DB.STM.PROJ_VERSIONING
  -- OUTPUT PARAMETERS
  --   return XML of version, comment and last update date
  -- EXCEPTIONS
  --  NO_DATA_FOUND WHEN MODULE IS NOT INSTALLED
  FUNCTION FNC_SHOW_ALL RETURN CLOB;
  -- PURPOSE
  --  Retrieve current version of the module
  -- INPUT PARAMETERS
  -- OUTPUT PARAMETERS
  --   return XML formatted repository content
  -- EXCEPTIONS
END PKG_VERSION;
/

CREATE OR REPLACE PACKAGE BODY PKG_VERSION IS
  PROCEDURE PRC_SET_VER(P_MOD_NM VARCHAR2, P_COMMENT VARCHAR2, P_SETTER VARCHAR2,
    P_SPEC_TITLE VARCHAR2, P_SPEC_VENDOR VARCHAR2, P_SPEC_VER VARCHAR2,
    P_IMPL_TITLE VARCHAR2, P_IMPL_VENDOR VARCHAR2, P_IMPL_VER VARCHAR2,
    P_BUILT_BY   VARCHAR2, P_CREATED_BY VARCHAR2, P_BUILT_DTM DATE ) is
  BEGIN
     INSERT INTO VERSION (MODULE_NAME, COMMENTS, CREATED_DTM, installed_BY,
          SPEC_TITLE, SPEC_VENDOR, SPEC_VERSION,
          IMPL_TITLE, IMPL_VENDOR, IMPL_VERSION, CREATED_BY,BUILT_BY, BUILT_DTM)
       VALUES (P_MOD_NM, P_COMMENT, SYSDATE, P_SETTER,
          P_SPEC_TITLE, P_SPEC_VENDOR, P_SPEC_VER,
          P_IMPL_TITLE, P_IMPL_VENDOR, P_IMPL_VER, P_CREATED_BY, P_BUILT_BY, P_BUILT_DTM);
  EXCEPTION
   WHEN DUP_VAL_ON_INDEX THEN
     UPDATE VERSION SET COMMENTS=nvl(P_COMMENT, comments), MODIFIED_BY = P_SETTER, MODIFIED_DTM = SYSDATE,
          SPEC_TITLE = nvl(P_SPEC_TITLE ,SPEC_TITLE ),
          SPEC_VENDOR= nvl(P_SPEC_VENDOR,SPEC_VENDOR),
          SPEC_VERSION   = nvl(P_SPEC_VER   ,SPEC_VERSION   ),
          IMPL_TITLE = nvl(P_IMPL_TITLE ,IMPL_TITLE ),
          IMPL_VENDOR= nvl(P_IMPL_VENDOR,IMPL_VENDOR),
          IMPL_VERSION   = nvl(P_IMPL_VER   ,IMPL_VERSION   ),
          CREATED_BY = NVL(P_CREATED_BY, CREATED_BY), 
          BUILT_BY = NVL(P_BUILT_BY, BUILT_BY),
	  BUILT_DTM = NVL(P_BUILT_DTM, BUILT_DTM)
       WHERE MODULE_NAME = P_MOD_NM;
  END;
  PROCEDURE PRC_RM_MOD(P_MOD_NM VARCHAR2) IS
  BEGIN
    DELETE FROM VERSION WHERE MODULE_NAME = P_MOD_NM;
  END;
  FUNCTION FNC_SHOW(p_mod_nm varchar2 default null) RETURN CLOB IS
    l_buff CLOB;
    l_ctx  DBMS_XMLQuery.ctxHandle;
    l_condition varchar2(50);
  BEGIN
    if p_mod_nm is not null then
       l_condition := ' where module_name like '''||p_mod_nm||'''';
    end if;
    l_ctx := DBMS_XMLQuery.newContext('select MODULE_NAME, COMMENTS, '||
    'SPEC_TITLE, SPEC_VENDOR, SPEC_VERSION, IMPL_TITLE, IMPL_VENDOR, IMPL_VERSION,'||
    'CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM from version' || l_condition);
    DBMS_XMLQuery.setRowSetTag(l_ctx, 'DB_VERSION');
    DBMS_XMLQuery.setRowTag(l_ctx, 'MODULE');
    l_buff := DBMS_XMLQuery.getXML(l_ctx);
    DBMS_XMLQuery.closeContext(l_ctx);
    return l_buff;
  END;

  FUNCTION FNC_GET_VER(P_MOD_NM VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
     RETURN FNC_SHOW(p_mod_nm);
  END;
  FUNCTION FNC_SHOW_ALL RETURN CLOB IS
  BEGIN
    return FNC_SHOW;
  END;
END;
/

/*
exec pkg_version.PRC_SET_VER('DB/SMP', 'Database version', 'Vadim', 'SMP', 'Core', '2.3', 'SMP', 'Core', '2.2');


set long 8000
var c clob

exec :c := pkg_version.FNC_SHOW_ALL;
print c;
select  pkg_version.FNC_GET_VER('DB/SMP') from dual;

 */
