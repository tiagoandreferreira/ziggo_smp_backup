--=============================================================================
--    $Id: collect_stat.sql,v 1.2 2005/11/08 17:14:50 dant Exp $
--
--  DESCRIPTION
--
--  Script to automate statistics collection and collection undo
--
--  Usage: sqlplus /nolog @chk_stat.sql
--
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

exec DBMS_STATS.CREATE_STAT_TABLE ( user, 'SMP_STAT_HIST');

create or replace package pkg_smp_stat as
 /*
  collect_all
   - enables monitoring on the tables where it's not and collect stat for them,
   - collect statistics for the tables having none
   - repeats statistics collection on stale tables, incl histoigrams
   - p_statid - either default or user provided may be used to undo statistics collection
     when statid duplicate's found, it is appended with '/N', where N = 1...999
  */
 procedure collect_all(p_statid varchar2 default to_char(sysdate, 'yyyymmdd'));
 /*
  collect_table
   - collects statistics for a single table and indexes
   - repeats histoigram colllection if they have been collected before
   - p_statid - either default or user provided may be used to undo statistics collection
     when statid duplicate's found, it is appended with '/N', where N = 1...999
  */

 procedure collect_table(p_table_name varchar2, p_statid varchar2 default to_char(sysdate, 'yyyymmdd'));
 /*
  undo_table
  - undo statistics collection on a single table to a state existed before collection with specified statid
  */
 procedure undo_table(p_table_name varchar2, p_statid varchar2);
 /*
  undo_all
  - undo statistics collection on all tables collected with specified statid
  */
 procedure undo_all(p_statid varchar2);
end;
/

create or replace package body pkg_smp_stat as

procedure collect_all (p_statid varchar2 default to_char(sysdate, 'yyyymmdd'))  is
 objtab DBMS_STATS.ObjectTab;
begin
  for cr in (select table_name from  user_tables where monitoring = 'NO' and TEMPORARY = 'N') loop
    execute immediate 'alter table '||cr.table_name||' monitoring';
    collect_table(cr.table_name, p_statid);
  end loop;

  for cr in (select table_name from  user_tables where last_analyzed = null and TEMPORARY = 'N') loop
    collect_table(cr.table_name, p_statid);
  end loop;

  dbms_stats.gather_schema_stats(user, cascade=> true, options => 'LIST STALE',  objlist =>objtab);
  if objtab.count > 0 then
    for i in objtab.first .. objtab.last loop
      if objtab(i).objtype = 'TABLE' then
         collect_table(objtab(i).objname, p_statid);
      end if;
    end loop;
  end if;

end;


procedure collect_table(p_table_name varchar2, p_statid varchar2 default to_char(sysdate, 'yyyymmdd')) is
 n number;
 v_ver number;
 v_statid varchar2(30);

begin
    select count(*) into n from smp_stat_hist where c1 = p_table_name;
    if n > 0 then
       select nvl(max(to_number(substr(statid, 10, 3))), 0), count(*) into v_ver, n
         from  smp_stat_hist where c1 = p_table_name and statid like p_statid||'%';
       if n > 0 then
         v_statid := p_statid||'/'||to_char(v_ver + 1);
       else
         v_statid := p_statid;
       end if;

       DBMS_STATS.GATHER_TABLE_STATS (
         ownname          => user,
         tabname          => p_table_name,
         method_opt       => 'FOR ALL COLUMNS SIZE REPEAT',
         cascade          => TRUE,
         stattab          => 'SMP_STAT_HIST',
         statid           => v_statid);

    else
       DBMS_STATS.GATHER_TABLE_STATS (
         ownname          => user,
         tabname          => p_table_name,
         cascade          => TRUE,
         stattab          => 'SMP_STAT_HIST',
         statid           => p_statid);
    end if;
end;
procedure undo_table(p_table_name varchar2, p_statid varchar2) is
 n number;
begin
    select count(*) into n from smp_stat_hist where c1 = p_table_name and statid = p_statid and type = 'T';

    if n > 0 then
     DBMS_STATS.IMPORT_TABLE_STATS (
      ownname     =>  user,
      tabname     =>  p_table_name,
      stattab     =>  'SMP_STAT_HIST',
      statid      =>  p_statid,
      statown     =>  user);
    end if;
end;

procedure undo_all(p_statid varchar2) is
begin
    for cr in (select c1 from SMP_STAT_HIST where type = 'T' and statid = p_statid) loop
      undo_table(cr.c1, p_statid);
    end loop;
end;
end;
/



analyze table sub_obj_enq compute statistics for all columns for all indexes;




