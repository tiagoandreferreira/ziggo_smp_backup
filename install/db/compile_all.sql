---------------------------------------------------------------------------
-- FILE INFO
--    $Id: compile_all.sql,v 1.2 2001/08/27 14:11:43 parmis Exp $
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------
DECLARE
---------------------------------------------------------------------------
-- Author:  Parmi Sahota 24 Aug 01
-- DESCRIPTION
-- This script re-compiles all objects in the current schema.
-- It should be used after creating procedures/functions to ensure that 
-- the objects are valid even though the creation may not have been in the
-- correct sequence.
--
---------------------------------------------------------------------------
  l_user varchar2(30);
BEGIN
  select user into l_user from dual;
  dbms_utility.compile_schema(l_user);
END;
/
