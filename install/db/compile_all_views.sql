--==============================================================================
--    $Id: compile_all_views.sql,v 1.1 2005/09/27 17:25:53 gregg Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--==============================================================================
 
 DECLARE
---------------------------------------------------------------------------
-- Author:  Dan Tsipe 23 Sep 05
-- DESCRIPTION
-- This script compiles all vies.
--
---------------------------------------------------------------------------

  v_statement varchar2(100);

   cursor views_cur is
     select view_name from user_views;

   
BEGIN
   for r_views in views_cur loop
     v_statement := 'ALTER VIEW '||r_views.view_name|| ' compile';
     execute immediate v_statement;
   end loop;
END;
/
