#=============================================================================
#
#  FILE INFO
#
#    $Id: create_adm_db.sh,v 1.11 2007/04/17 16:47:27 siarheig Exp $
#
#  DESCRIPTION
#
#    Builds and executes SQl files for creating tables, primary key,
#    foreign keys, indexes, private synonyms,
#    and granting select and references privileges
#
#
#  PARAMETERS
#
#   ORACLE_LOGIN      - Oracle's root username
#   ORACLE_PASSWORD   - Oracle user's password
#   ORACLE_SID        - Oracle's instance name
#   Processing_Type   - Action type the script is going to perform,
#                       Possible values:
#                       1 : generate creation table syntax
#                       2 : generate primary key syntax and appropriate grants
#                       3 : generate both foreign key and index syntax
#
#   INPUT FILES
#
#    samp_tables      - contains the whole SMP's DB create table syntax
#    samp_primaries   - contains the whole SMP's DB create primary keys syntax
#    samp_constraints - contains the whole SMP's DB create foreign keys syntax
#                       as well as create index syntax
#
#  RELATED SCRIPTS
#
#   create_schema.awk - To generate creation table syntax, primary key syntax,
#                       foreign key syntax as well as create index syntax
#   create_grants.awk - To generate grant select to Oracle's roles and grant
#                       references to Oracle's users
#
#  REVISION HISTORY
#  * Based on CVS log
#=============================================================================

. ../../util/setEnv.sh

LOG_FILE=create_adm_db.log
echo "create_adm_db.sh Starting:"         > $LOG_FILE

#-------------------------------------------------------------------------
# Preparing all variables for later use
#-------------------------------------------------------------------------

NAME=$SAMP_DB_USER_ROOT"cm"
PASS=$SAMP_DB_PASSWORD"@"$SAMP_DB
AM_DB_USER=$SAMP_DB_USER_ROOT"am"

if [ "$ENV_TYPE" = "TEST" ]
 then
     TABLE_LIST_SMP=table_list.adm.test
     INDEX_LIST_SMP=index_list.adm.test
 else
     TABLE_LIST_SMP=table_list.adm.prod
     INDEX_LIST_SMP=index_list.adm.prod
fi

SMP_SCHEMA=SMP_SCHEMA_FILE.sql
export NAME PASS LOG_FILE SMP_SCHEMA

#-------------------------------------------------------------------------
# Building SQL file
#-------------------------------------------------------------------------


     echo Building file $SMP_SCHEMA for $NAME schema       |   tee -a $LOG_FILE


# cleanup unless called from smp install scripts
     echo Cleaning $NAME Schema

     sqlplus  ${NAME}/${PASS}  @cleanup_adm_schema.sql           >> $LOG_FILE

     echo creating tables ddl for $NAME schema

     echo set echo on > $SMP_SCHEMA

     nawk -v ListFile=$TABLE_LIST_SMP   -f create_adm_schema.awk adm_tables.ddl \
          >> $SMP_SCHEMA

     echo creating ddl for indexes for $NAME Schema

     nawk -v ListFile=$INDEX_LIST_SMP   -f create_adm_indexes.awk adm_indexes.ddl\
          >> $SMP_SCHEMA

     echo creating trigers etc for  $NAME Schema

     cat adm_pre_others.ddl   >> $SMP_SCHEMA

     cat adm_others.ddl   >> $SMP_SCHEMA

# integrate with smp  
if [ ! -z $SMP_CREATE ] 
then 

     cat  adm_smp_others.ddl   >> $SMP_SCHEMA
     
fi

     echo exit   >> $SMP_SCHEMA

     echo execute ADM DDL in $NAME schema

     sqlplus  ${NAME}/${PASS} @$SMP_SCHEMA    >> $LOG_FILE

     ./create_adm_plsql.sh


#-------------------------------------------------------------------------
# Granting GRP_USR rights for ADM Reports and create view in CM schema
#-------------------------------------------------------------------------

sqlplus -S $SAMP_DB_USER_ROOT"am"/${PASS} <<END >> $LOG_FILE
grant select on GRP_USR to $NAME;
END

sqlplus  ${NAME}/${PASS}  <<END >> $LOG_FILE
CREATE VIEW AM_GRP_USR_REP
AS SELECT * FROM $AM_DB_USER.GRP_USR;
END


#-------------------------------------------------------------------------
# Verifying the result
#-------------------------------------------------------------------------

if egrep -s 'ERROR' $LOG_FILE
then
    echo ....... Errors found. See \'$LOG_FILE\' for details
    exit 1
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 1
elif egrep -s "ORA-" $LOG_FILE
then
   echo SMP Database  created.
   echo ORA- messages found. Please review \'$LOG_FILE\'
   exit 0
else
   echo SMP Database successfully created. |        tee -a $LOG_FILE
   exit 0
fi


