#=============================================================================
#
#  FILE INFO
#
#    $Id: create_adm_plsql.sh,v 1.3 2003/09/04 18:55:48 vadimg Exp $
#
#  DESCRIPTION
#
#    Builds PL/SQL CODE
#
#    This script assumes following env vars set
#     NAME PASS LOG_FILE SMP_SCHEMA PKG_ARCH
#
#
#  REVISION HISTORY
#  * Based on CVS log
#=============================================================================

echo Creating PL/SQL code for $NAME schema | tee -a $LOG_FILE

if [ -z $NAME ] || [ -z $PASS ] || [ -z $LOG_FILE ] || [ -z $SMP_SCHEMA ]  
then
    echo
    echo NAME PASS LOG_FILE SMP_SCHEMA env. variables have not been set.
    exit 1
fi


echo set echo on       > $SMP_SCHEMA.adm.plsql

cat adm_plsql.sql     >> $SMP_SCHEMA.adm.plsql

echo exit  	      >> $SMP_SCHEMA.adm.plsql

echo installing ADM PL/SQL in $NAME schema  | tee -a $LOG_FILE

sqlplus ${NAME}/${PASS} @$SMP_SCHEMA.adm.plsql |  grep -v "Warning[A-Za-z :]*compilation"     >> $LOG_FILE

