#==============================================================================
#
#  FILE INFO
#    $Id: create_adm_schema.awk,v 1.1 2003/08/21 15:50:33 vadimg Exp $ 
#
#  DESCRIPTION
#    Splits input SQL file to 'list' tables. 
#
#  PARAMETERS
#     ListFile - name of the file with the table list for new schema or
#     File     - name of the new schema file or 
#                sql statement file for loading data
#
#  REVISION HISTORY
#  *  P Chpakovski  1997-09-09
#     Original version
#  *  P Chpakovski  1997-09-10
#     Added comment simbol # in the first position of table list file
#  *  P Chpakovski  1997-09-10
#     Added possibility to load data using list file
#  *  R Wydrowski  1997-09-10
#     Fixed deletes
#  *  P Chpakovski  1997-09-15
#     Added possibility to have table name in the end of the string
#  *  R Wydrowski  1999-01-29
#     Minor clean-up
#  *  P Chpakovski 1999-03-01
#     Added Tablespace and sizes attachment
#  *  P Chpakovski 1999-03-17
#     Added MINEXTENTS and PCTFREE
#  *  G Serrano    2001-07-24
#     Added Generation of Primary Key names, Foreign Key names, as
#      well as Indexes names
#  *  G Serrano    2001-08-20
#     Added possibility of generating comments on tables and columns
#      as well as dropping and creating indexes
#  *  G Serrano    2001-08-24
#     Added possibility of generating triggers on tables
#
#==============================================================================
BEGIN{
    selectInd                      = 0;
    fk_ind                         = 1;
    ix_ind                         = 1;
    maxLine                        = 0;
    n_a                            = "n/a";
    TableName                      = "";
    CreateTrigger                  = "";
    PreviousTableName              = "";
    NullLine                       = "";
    appendixActionNo               = 0;
    appendixActionCreateTable      = 1;
    appendixActionCreatePrimaryKey = 2;
    appendixActionCreateForeignKey = 3;
    appendixAction                 = appendixActionNo;

    while ( getline listBuf[maxLine] < ListFile > 0 )
    {
        if (!match(listBuf[maxLine],"^#"))
        {
            split ( listBuf[maxLine], col);
            listTableName[maxLine]       = col[1];
            listTableTablespace[maxLine] = col[2];
            listTableInitSize[maxLine]   = col[3];
            listTableNextSize[maxLine]   = col[4];
            listTableFree[maxLine]       = col[5];
            listTableExts[maxLine]       = col[6];
            listTableType[maxLine]       = col[7];
            listTableAudit[maxLine]      = col[8];
            
            maxLine++
        } 
    }
}
{
    
    if(match($0,"^ALTER")    ||
       match($0,"^INSERT")   ||
       match($0,"^DELETE")   ||
       match($0,"^DROP")     ||
       match($0,"^COMMENT ON")     ||
       match($0,"^CREATE  *SEQUENCE")     ||
       match($0,"^CREATE *TABLE"))
    {
        appendix = "";
        if (match($0,"^CREATE *SEQUENCE"))
        {
          $3 = substr($3,1,length($3)-4);
        }
        for(i=0; i < maxLine; i++)
        {
            if (match($0," " listTableName[i] "([^A-Za-z0-9_]|$)"))
            {
                selectInd=1;
                tabInd= i;
                if(match($0,"^CREATE  *TABLE"))
                {
                    print NullLine;
                    print NullLine;
                    appendixAction = appendixActionCreateTable;
                }

                break
            }
        }  
    }
    else if (match($0,"^COMMIT"))
    {
         selectInd=1;
    }

    if (selectInd)
    {
        if (match($0,"^CREATE *SEQUENCE"))
        {
            New = "table_SEQ";
            sub(/table/,$3,New);
            $3 =  New;
        } 
        if (match($0,";$"))
        {
            printWithAppendix(appendixAction,tabInd); 
            selectInd = 0
            appendixAction = appendixActionNo;
        }
        else
        {
            print $0
        }
    }

}

function printWithAppendix(appendixAction, tabInd)
{
    appendix = ""
    if(appendixAction == appendixActionCreateTable)
    {
        if(!match(listTableTablespace[tabInd],n_a))
            appendix = appendix  " TABLESPACE "  listTableTablespace[tabInd]
        if(!match(listTableFree[tabInd],n_a))
            appendix = appendix  " PCTFREE "  listTableFree[tabInd]
        if((!match(listTableInitSize[tabInd],n_a)) || 
           (!match(listTableNextSize[tabInd],n_a)) ||
           (!match(listTableExts[tabInd],n_a)))
        {
            appendix = appendix  " STORAGE ( "
            if( !match(listTableInitSize[tabInd],n_a))
                appendix = appendix  " INITIAL "  listTableInitSize[tabInd]
            if( !match(listTableNextSize[tabInd],n_a))
                appendix = appendix  " NEXT "  listTableNextSize[tabInd]
            if( !match(listTableExts[tabInd],n_a))
                appendix = appendix  " MINEXTENTS "  listTableExts[tabInd]
            appendix = appendix  " )"
        
        }
        print substr($0,1,index($0,";")-1) appendix " ;"
        print NullLine;
        if((listTableAudit[tabInd] == "y") || 
           (listTableAudit[tabInd] == "Y")) 
        {
         CreateTrigger = "CREATE OR REPLACE TRIGGER ABC123_TR1";
         sub(/ABC123/,listTableName[tabInd],CreateTrigger);
         print CreateTrigger;
         CreateTrigger = "BEFORE INSERT ON ABC123";
         sub(/ABC123/,listTableName[tabInd],CreateTrigger);
         print CreateTrigger;
         print "FOR EACH ROW";
         print "BEGIN";
         print " :NEW.CREATED_DTM  := SYSDATE;";
         print " :NEW.MODIFIED_BY  := NULL;";
         print " :NEW.MODIFIED_DTM := NULL;"
         print "END;";
         print "/";
         print NullLine;
         CreateTrigger = "CREATE OR REPLACE TRIGGER ABC123_TR2";
         sub(/ABC123/,listTableName[tabInd],CreateTrigger);
         print CreateTrigger;
         CreateTrigger = "BEFORE UPDATE ON ABC123";
         sub(/ABC123/,listTableName[tabInd],CreateTrigger);
         print CreateTrigger;
         print "FOR EACH ROW";
         print "BEGIN";
         print " :NEW.MODIFIED_DTM := SYSDATE;";
         print "END;";
         print "/";
         print NullLine;
        }
        if(( listTableType[tabInd] == "y" ) ||
           ( listTableType[tabInd] == "Y" ))
        {
         print "alter table " listTableName[tabInd] " monitoring;"
        }
    }
    else if (appendixAction == appendixActionCreatePrimaryKey)
    {
        if((!match(listIndexInitSize[tabInd],n_a)) || 
           (!match(listIndexNextSize[tabInd],n_a)) ||
           (!match(listIndexFree[tabInd],n_a)) ||
           (!match(listIndexExts[tabInd],n_a)) ||
           (!match(listIndexTablespace[tabInd],n_a)))
            appendix = appendix " USING INDEX "
        if(!match(listIndexTablespace[tabInd],n_a))
            appendix = appendix  " TABLESPACE "  listIndexTablespace[tabInd]
        if(!match(listIndexFree[tabInd],n_a))
            appendix = appendix  " PCTFREE "  listIndexFree[tabInd]

        if((!match(listIndexInitSize[tabInd],n_a)) || 
           (!match(listIndexNextSize[tabInd],n_a)))
        {
            appendix = appendix  " STORAGE ( "
            if( !match(listIndexInitSize[tabInd],n_a))
                appendix = appendix  " INITIAL "  listIndexInitSize[tabInd]
            if( !match(listIndexNextSize[tabInd],n_a))
                appendix = appendix  " NEXT "  listIndexNextSize[tabInd]
            if( !match(listIndexExts[tabInd],n_a))
                appendix = appendix  " MINEXTENTS "  listIndexExts[tabInd]
            appendix = appendix  " )"
        
        }
        print substr($0,1,match($0,"\\) *;")-1) appendix ") ;"
        print NullLine;
    }
    else
    {
        print $0
        print NullLine;
    }
}
