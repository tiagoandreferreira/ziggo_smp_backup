CREATE TABLE REJECT_PWD (
       pwd              VARCHAR2(30) PRIMARY KEY
);

CREATE TABLE ACCESS_CTRL_ENTRY (
       grp_nm               VARCHAR2(30) NOT NULL,
       secure_obj_nm        VARCHAR2(255) NOT NULL,
       permission_nm        VARCHAR2(30) NOT NULL,
       is_excluded          CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


ALTER TABLE ACCESS_CTRL_ENTRY
       ADD  ( PRIMARY KEY (secure_obj_nm, grp_nm, is_excluded, permission_nm) ) ;


CREATE TABLE AM_PARM (
       parm_nm              VARCHAR2(30) NOT NULL,
       parm_typ_nm          VARCHAR2(30) NOT NULL,
       parm_descr           VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);

CREATE INDEX AM_parm_FK1 ON AM_PARM
(
       parm_typ_nm                    ASC
);


ALTER TABLE AM_PARM
       ADD  ( PRIMARY KEY (parm_nm) ) ;


CREATE TABLE GRP (
       grp_nm               VARCHAR2(30) NOT NULL,
       grp_descr            VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


ALTER TABLE GRP
       ADD  ( PRIMARY KEY (grp_nm) ) ;


CREATE TABLE GRP_HIERARCHY (
       parent_grp_nm        VARCHAR2(30) NOT NULL,
       member_grp_nm        VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       UNIQUE (
              member_grp_nm,
              parent_grp_nm
       )
);


ALTER TABLE GRP_HIERARCHY
       ADD  ( PRIMARY KEY (parent_grp_nm, member_grp_nm) ) ;


CREATE TABLE GRP_PARM (
       grp_nm               VARCHAR2(30) NOT NULL,
       parm_nm              VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);

CREATE INDEX grp_parm_idx1 ON GRP_PARM
(
       parm_nm                        ASC,
       grp_nm                         ASC
);


ALTER TABLE GRP_PARM
       ADD  ( PRIMARY KEY (grp_nm, parm_nm) ) ;


CREATE TABLE GRP_USER (
       usr_nm               VARCHAR2(30) NOT NULL,
       grp_nm               VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       UNIQUE (
              grp_nm,
              usr_nm
       )
);


ALTER TABLE GRP_USER
       ADD  ( PRIMARY KEY (usr_nm, grp_nm) ) ;


CREATE TABLE PERMISSION (
       permission_nm        VARCHAR2(30) NOT NULL,
       permission_descr     VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


ALTER TABLE PERMISSION
       ADD  ( PRIMARY KEY (permission_nm) ) ;


CREATE TABLE REF_AM_PARAMETER_TYPE (
       parm_typ_nm          VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


ALTER TABLE REF_AM_PARAMETER_TYPE
       ADD  ( PRIMARY KEY (parm_typ_nm) ) ;


CREATE TABLE SECURE_OBJ (
       secure_obj_nm        VARCHAR2(255) NOT NULL,
       secure_obj_descr     VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


ALTER TABLE SECURE_OBJ
       ADD  ( PRIMARY KEY (secure_obj_nm) ) ;


CREATE TABLE USR (
       usr_nm               VARCHAR2(30) NOT NULL,
       usr_pswd             VARCHAR2(255) NOT NULL,
       usr_descr            VARCHAR2(255) NULL,
       last_pwd_chng_dtm    DATE NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


ALTER TABLE USR
       ADD  ( PRIMARY KEY (usr_nm) ) ;


CREATE TABLE USR_PARM (
       usr_nm               VARCHAR2(30) NOT NULL,
       parm_nm              VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       parm_val             VARCHAR2(255) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);

CREATE INDEX Usr_Parm_idx1 ON USR_PARM
(
       parm_nm                        ASC,
       usr_nm                         ASC
);


ALTER TABLE USR_PARM
       ADD  ( PRIMARY KEY (usr_nm, parm_nm) ) ;


ALTER TABLE ACCESS_CTRL_ENTRY
       ADD  ( FOREIGN KEY (secure_obj_nm)
                             REFERENCES SECURE_OBJ ) ;


ALTER TABLE ACCESS_CTRL_ENTRY
       ADD  ( FOREIGN KEY (grp_nm)
                             REFERENCES GRP ) ;


ALTER TABLE ACCESS_CTRL_ENTRY
       ADD  ( FOREIGN KEY (permission_nm)
                             REFERENCES PERMISSION ) ;


ALTER TABLE AM_PARM
       ADD  ( FOREIGN KEY (parm_typ_nm)
                             REFERENCES REF_AM_PARAMETER_TYPE ) ;


ALTER TABLE GRP_HIERARCHY
       ADD  ( FOREIGN KEY (member_grp_nm)
                             REFERENCES GRP ) ;


ALTER TABLE GRP_HIERARCHY
       ADD  ( FOREIGN KEY (parent_grp_nm)
                             REFERENCES GRP ) ;


ALTER TABLE GRP_PARM
       ADD  ( FOREIGN KEY (grp_nm)
                             REFERENCES GRP ) ;


ALTER TABLE GRP_PARM
       ADD  ( FOREIGN KEY (parm_nm)
                             REFERENCES AM_PARM ) ;


ALTER TABLE GRP_USER
       ADD  ( FOREIGN KEY (grp_nm)
                             REFERENCES GRP ) ;


ALTER TABLE GRP_USER
       ADD  ( FOREIGN KEY (usr_nm)
                             REFERENCES USR ) ;


ALTER TABLE USR_PARM
       ADD  ( FOREIGN KEY (usr_nm)
                             REFERENCES USR ) ;


ALTER TABLE USR_PARM
       ADD  ( FOREIGN KEY (parm_nm)
                             REFERENCES AM_PARM ) ;

