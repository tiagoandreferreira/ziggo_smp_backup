
--============================================================================
--    $Id: create_bulk.sql,v 1.10 2011/10/27 16:20:35 armenn Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'bulk_add_voice_line';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_add_voice_line exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE bulk_add_voice_line CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'bulk_add_parm';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_add_parm exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE bulk_add_parm CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'bulk_add_equipment';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_add_equipment exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE bulk_add_equipment CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'bulk_add_features';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_add_features exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE bulk_add_features CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'BULK_ADD_FEATURES_ATTR';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_add_features_attribute exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE bulk_add_features_attribute CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'BULK_VL_FEATURES_REL';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_voice_line_features_rel exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE bulk_voice_line_features_rel CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/

DECLARE
    v_is_existed NUMBER;
BEGIN
SELECT count(*) INTO v_is_existed FROM user_tables WHERE table_name = 'BULK_ERROR_REASON';
IF v_is_existed = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- bulk_error_reason exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE BULK_ERROR_REASON CASCADE CONSTRAINTS';
END IF;
        v_is_existed := NULL;
END;
/


CREATE TABLE bulk_add_voice_line (         
	--id NUMBER(12) PRIMARY KEY,         
	voiceLineId VARCHAR2(12),         
	batchId NUMBER(12),
	featuresId VARCHAR2(1000),
	equipmentId VARCHAR2(12),         
	port VARCHAR2(12),         
	subId NUMBER(12),
	CREATED_DTM date,
	status number(3),
	smp_ordr_id NUMBER(12)
);

CREATE TABLE bulk_add_parm (         
	--id NUMBER(12) PRIMARY KEY,         
	voiceLineId VARCHAR2(12),
	equipmentId VARCHAR2(12),
	batchId NUMBER(12),
	parm_nm VARCHAR2(250),
	parm_val VARCHAR2(2000)
);  

create table bulk_add_equipment (
	--id NUMBER(12) PRIMARY KEY,         
	equipmentId VARCHAR2(12),         
	batchId NUMBER(12)
);


create table bulk_add_features (
	--id NUMBER(12) PRIMARY KEY,         
	featureId VARCHAR2(12),
	batchId NUMBER(12),
	featureName VARCHAR2(120)	
);

create table BULK_ADD_FEATURES_ATTR (
	--id NUMBER(12) PRIMARY KEY,         
	featureId VARCHAR2(12),
	batchId NUMBER(12),
	--featureAttributeSequence Number(3),
	featureAttributeName VARCHAR2(120),
	featureAttributeValue VARCHAR2(2000)
);


create table BULK_VL_FEATURES_REL (
	voiceLineId VARCHAR2(12),
	featureId VARCHAR2(12),
	batchId NUMBER(12)
);


create table BULK_ERROR_REASON (
	batchId NUMBER(12),
	bulk_error_reason CLOB
);



