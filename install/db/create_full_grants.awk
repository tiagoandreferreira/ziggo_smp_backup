#
#  FILE INFO
#    $Id: create_full_grants.awk,v 1.1 2002/01/16 14:51:22 germans Exp $
#
#  DESCRIPTION
#   Generates reading permission to Oracle's roles and references permission to
#    create foreign keys in another schema 
#
#  PARAMETERS
#     UserSuffix - Oracle user that is granting the privileges 
#     User       - Oracle user receiving the privileges, this name is the suffix for the
#                   role name 
#                - The input file is a list of tables an Oracle User can reference and read 
#
#  REVISION HISTORY
#  * Based on CVS log
BEGIN{
    maxLine       = 0;
    Grant         = "";
    References    = "";
}
{
if (match($2,UserSuffix))
{
 Privilege = "grant all on abc123 to user;";
 sub(/abc123/,$1,Privilege);
 sub(/user/,User,Privilege);
 print Privilege;
}
else
{
}
}
