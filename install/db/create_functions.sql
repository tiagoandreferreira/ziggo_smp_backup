--============================================================================
--    $Id: create_functions.sql,v 1.31.2.1 2012/06/22 13:53:39 prajaktb Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

-- Start of DDL Script for Function get_svc_parm_value
CREATE OR REPLACE FUNCTION get_svc_parm_value(pSvcNm IN varchar2,subsvcid IN number,pParmName IN varchar2)
    return sub_svc_parm.val%TYPE is
    baseSvcId number;
    svcId number;
    parmValue sub_svc_parm.val%TYPE;
    pSvcId number;
begin
	select svc_id into pSvcId from svc where svc_nm=pSvcNm;
    svcId := pSvcId;
    loop
      select base_svc_id into baseSvcId from svc where svc_id=svcId;
      exit when baseSvcId is null;
      svcId :=baseSvcId;
    end loop;
    select s.val INTO parmValue from sub_svc_parm s,parm p where
		   p.class_id = 100 AND
    	   p.object_id = svcId AND
       	   p.parm_nm = pParmName AND 
		   p.parm_id=s.parm_id AND
		   s.sub_svc_id=subsvcid;
    return parmValue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/
-- Start of DDL Script for Function get_package

CREATE OR REPLACE FUNCTION get_package (subsvcid IN number)
    return sub_svc_parm.val%TYPE is
    packageNm sub_svc_parm.val%TYPE;
	psubsvcid number;
	rsubsvcid number;
begin
	 select sub_svc_id into psubsvcid from sub_svc where parent_sub_svc_id=(select parent_sub_svc_id from sub_svc where sub_svc_id=subsvcid) and svc_id= (select svc_id from svc where svc_nm='voice_service_class_of_service');
 	 select sub_svc_id into rsubsvcid from sub_svc where parent_sub_svc_id=psubsvcid and svc_id=(select svc_id from svc where svc_nm='smp_class_of_service_association');	 
	 packageNm := get_svc_parm_value('smp_class_of_service_association',rsubsvcid,'class_of_service_name');
	return packageNm;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/
-- Start DDL of Script for Function get_department
CREATE OR REPLACE FUNCTION get_department(subsvcid IN number)
    return sub_svc_parm.val%TYPE is
    assocsubsvcid number;
    assocparmvalue sub_svc_parm.val%TYPE;
	svcid number;
begin
	 select s.sub_svc_id into assocsubsvcid from sub_svc sc,sub_svc_assoc s,ref_assoc_typ t,ref_assoc_rule r where 
     		r.assoc_typ_id=t.assoc_typ_id AND
			s.assoc_rule_id=r.assoc_rule_id AND
			t.assoc_typ_nm='user_has_voice_service' AND s.assoc_sub_svc_id=subsvcid
			AND s.sub_svc_id = sc.SUB_SVC_ID
			AND sc.SUB_SVC_STATUS_ID!=29;	 
	 select svc_id into svcid from svc where svc_nm='smp_user';
	 assocparmvalue := get_svc_parm_value('smp_user',assocsubsvcid,'department');
    return assocparmvalue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/


-- Start DDL of Script for Function get_pilot_tn_and_ext

CREATE OR REPLACE FUNCTION get_pilot_tn_and_ext(parentSubSvcId IN NUMBER ,svcId IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
    IS
      getExt SUB_SVC_PARM.val%TYPE;
	  getTn SUB_SVC_PARM.val%TYPE;
	  subsvcid NUMBER;
	  assocsubsvcid NUMBER;
BEGIN
        SELECT sub_svc_id INTO subsvcid FROM SUB_SVC WHERE parent_sub_svc_id=parentSubSvcId  AND svc_id = svcId and SUB_SVC.SUB_SVC_STATUS_ID!=29;
		SELECT assoc_sub_svc_id INTO assocsubsvcid FROM SUB_SVC_ASSOC WHERE sub_svc_id = subsvcid;
                getTn := get_svc_parm_value('primary_voice_line',assocsubsvcid,'telephone_number'); 
	        getExt:= get_svc_parm_value('primary_voice_line',assocsubsvcid,'extension'); 
		IF 
		  getTn IS NOT NULL AND getExt IS NOT NULL  
		THEN
           RETURN getTn ||'x'||getExt;
		ELSIF
		  getExt IS NULL 
		THEN
		   RETURN getTn ;
		ELSIF  
		 getTn IS NULL
		THEN 
		 RETURN  getExt;
		END IF;

EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/


-- Start DDL of Script for Function get_user_name

CREATE OR REPLACE FUNCTION get_user_name (SvcId IN NUMBER,parmNm VARCHAR)
    RETURN VARCHAR2
    IS
       Name VARCHAR2(2000);
	   parmId NUMBER ;
BEGIN
        SELECT parm_id INTO parmId FROM PARM 
        WHERE parm_id IN (SELECT parm_id FROM PARM WHERE parm_nm = parmNm ) AND object_id = (SELECT svc_id FROM SVC WHERE svc_nm = 'smp_commercial_voice_line') AND class_id = get_class_id ('SubSvcSpec');
        SELECT val INTO Name FROM SUB_SVC_PARM WHERE sub_svc_id=SvcId AND parm_id=parmId;

        RETURN  Name;

EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;

/
CREATE OR REPLACE FUNCTION get_sequence(SubSvcId IN NUMBER,parmNm VARCHAR)
    RETURN VARCHAR2
    IS
       seq VARCHAR2(2000);
       parmId NUMBER ;
BEGIN
         SELECT parm_id INTO parmId FROM PARM 
         WHERE parm_id IN (SELECT parm_id FROM PARM WHERE parm_nm = parmNm ) AND object_id = (SELECT svc_id FROM SVC WHERE svc_nm = 'call_group_member') AND class_id = get_class_id ('SubSvcSpec');
         SELECT val INTO seq FROM  SUB_SVC_PARM WHERE sub_svc_id=SubSvcId AND parm_id=parmId;
         RETURN  seq;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/


-- Start DDL of Script for Function get_parm_tn_ext

CREATE OR REPLACE FUNCTION get_parm_tn_ext(assocSvc IN NUMBER,parmNm IN VARCHAR2)
    RETURN SUB_SVC_PARM.val%TYPE
    IS
	  valReturn VARCHAR2(20000);
	  parmId NUMBER;
	  
BEGIN
     SELECT parm_id INTO parmId FROM PARM 
     WHERE parm_id IN (SELECT parm_id FROM PARM WHERE parm_nm = parmNm ) AND object_id = (SELECT svc_id FROM SVC WHERE svc_nm = 'smp_commercial_voice_line') AND class_id = get_class_id ('SubSvcSpec');
	 SELECT val INTO valReturn FROM SUB_SVC_PARM WHERE SUB_SVC_ID=assocSvc AND parm_id=parmId;

    RETURN valReturn;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

-- Start DDL of Script for Function get_group_name

CREATE OR REPLACE FUNCTION get_group_name(parentSubSvcId IN NUMBER,SvcNm IN VARCHAR)
    RETURN SUB_SVC_PARM.val%TYPE
    IS
      getGroupname SUB_SVC_PARM.val%TYPE;
      subsvcid NUMBER;
	  svcId NUMBER;
BEGIN
     SELECT svc_id INTO svcId FROM SVC WHERE svc_nm=SvcNm;
	 SELECT sub_svc_id INTO subsvcid FROM SUB_SVC WHERE parent_sub_svc_id=parentSubSvcId AND svc_id=svcId;
	 getGroupname := get_svc_parm_value(SvcNm,subsvcid,'group_name');
    RETURN getGroupname;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

-- Start DDL of Script for Function get_equipment_info
CREATE OR REPLACE FUNCTION get_equipment_info(PSubSvcId IN number,PparmNm IN varchar2)
 return sub_svc_parm.val%TYPE is    
 deviceNameValue sub_svc_parm.val%TYPE;
 svcNm svc.SVC_NM%TYPE;
 pSvcNm svc.SVC_NM%TYPE;
 childSvcId number;
 svcId number;
begin
	 select s.svc_nm into pSvcNm from svc s,sub_svc b where s.SVC_ID=b.SVC_ID and b.SUB_SVC_ID=PSubSvcId; 
	 IF PparmNm = 'device_nickname' THEN
	 	IF pSvcNm = 'commercial_emta' THEN
   			svcNm := 'emta_voice_device';
		ELSIF  pSvcNm = 'commercial_iad' THEN
   			svcNm := 'iad_device_control';
		ELSIF pSvcNm = 'commercial_ata' THEN
   			svcNm := 'ata_device_control';
		ELSIF pSvcNm = 'commercial_hosted_ivr' THEN
   			svcNm := 'ivr_definition_device_control';
		ELSIF pSvcNm = 'soft_phone' THEN
   			svcNm := 'comm_soft_phone_device_control';
		ELSIF pSvcNm = 'ip_phone' THEN
   			svcNm := 'ip_phone_device_control';
		ELSIF pSvcNm = 'ip_pbx' THEN
   			svcNm := 'ip_pbx_device_control';
		ELSIF pSvcNm = 'trunk_iad' THEN
   			svcNm := 'trunk_iad_device_control';
		ELSE
   			svcNm := NULL;
		END IF; 
	ELSE
	 	 IF pSvcNm = 'commercial_emta' THEN
   			svcNm := 'emta_voice_device';
		ELSIF  pSvcNm = 'commercial_iad' THEN
   			svcNm := 'iad_device_control';
		ELSIF pSvcNm = 'commercial_ata' THEN
   			svcNm := 'ata_device_control';
		ELSIF pSvcNm = 'commercial_hosted_ivr' THEN
   			svcNm := 'ivr_definition_device_control';
		ELSIF pSvcNm = 'soft_phone' THEN
   			svcNm := NULL;
		ELSIF pSvcNm = 'ip_phone' THEN
   			svcNm := 'ip_phone_device_control';
		ELSIF pSvcNm = 'ip_pbx' THEN
   			svcNm := 'ip_pbx_device_control';
		ELSIF pSvcNm = 'trunk_iad' THEN
   			svcNm := 'trunk_iad_device_control';
		ELSE
   			svcNm := NULL;
		END IF;
	END IF;	
	 select svc_id into svcId from svc where svc_nm=svcNm;
	 select sub_svc_id into childSvcId from sub_svc where  parent_sub_svc_id=PSubSvcId and svc_id=svcId;
  	 deviceNameValue:=get_svc_parm_value (svcNm, childSvcId, PparmNm);
	 return deviceNameValue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/


-- Start DDL of Script for Function get_equipment_defualt_value
CREATE OR REPLACE FUNCTION get_equipment_defualt_value(PSubSvcId IN number)
 return sub_svc_parm.val%TYPE is    
 defaultValue sub_svc_parm.val%TYPE;
 svcNm svc.SVC_NM%TYPE;
 pSvcNm svc.SVC_NM%TYPE;
 childSvcId number;
 svcId number;
begin
	 select s.svc_nm into pSvcNm from svc s,sub_svc b where s.SVC_ID=b.SVC_ID and b.SUB_SVC_ID=PSubSvcId; 
     IF pSvcNm = 'commercial_emta' THEN
   svcNm := 'emta_voice_device';
ELSIF  pSvcNm = 'commercial_iad' THEN
   svcNm := 'iad_device_control';
ELSIF pSvcNm = 'commercial_ata' THEN
   svcNm := 'ata_device_control';
ELSIF pSvcNm = 'commercial_hosted_ivr' THEN
   svcNm := 'ivr_definition_device_control';
ELSIF pSvcNm = 'soft_phone' THEN
   svcNm := 'comm_soft_phone_device_control';
ELSIF pSvcNm = 'ip_phone' THEN
   svcNm := 'ip_phone_device_control';
ELSIF pSvcNm = 'ip_pbx' THEN
   svcNm := 'ip_pbx_device_control';
ELSIF pSvcNm = 'trunk_iad' THEN
   svcNm := 'trunk_iad_device_control';
ELSE
   svcNm := NULL;
END IF;
  	 select svc_id into svcId from svc where svc_nm=svcNm;
  	 select dflt_val into defaultValue from parm where object_id=svcID and parm_nm='equipment_type';
	 return defaultValue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_zend_assoc_svc_parm_value(pSvcNm IN varchar2,subsvcid IN number,pParmName IN varchar2,assocRuleNm IN varchar2)
    return sub_svc_parm.val%TYPE is
    baseSvcId number;
	sSvcId number;
    svcId number;
    parmValue sub_svc_parm.val%TYPE;
    pSvcId number;
    assocRule number;
begin
	select svc_id into pSvcId from svc where svc_nm=pSvcNm;
	select l.assoc_rule_id into assocRule from ref_assoc_rule l,ref_assoc_typ t where l.assoc_typ_id=t.assoc_typ_id and assoc_typ_nm=assocRuleNm;
    svcId := pSvcId;
	select p.sub_svc_id into sSvcId from sub_svc_assoc p ,sub_svc s where s.sub_svc_id=p.sub_svc_id and s.sub_svc_status_id!=29 and assoc_sub_svc_id=subsvcid and assoc_rule_id=assocRule;
    loop
      select base_svc_id into baseSvcId from svc where svc_id=svcId;
      exit when baseSvcId is null;
      svcId :=baseSvcId;
    end loop;
    select s.val INTO parmValue from sub_svc_parm s,parm p where
		   p.class_id = 100 AND
    	   p.object_id = svcId AND
       	   p.parm_nm = pParmName AND 
		   p.parm_id=s.parm_id AND
		   s.sub_svc_id=sSvcId;
    return parmValue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

create or replace
FUNCTION fn_get_parm_val (
   p_sub_svc_id   IN   sub_svc.sub_svc_id%TYPE,
   p_sub_svc_nm   IN   svc.svc_nm%TYPE
)
   RETURN  sub_svc_parm.val%TYPE
IS
   result  sub_svc_parm.val%TYPE;
BEGIN
   select sub_svc_parm.val into result
   from sub_svc_parm, parm where
   sub_svc_parm.sub_svc_id = p_sub_svc_id AND
   sub_svc_parm.parm_id = parm.parm_id AND
   parm.parm_nm = p_sub_svc_nm;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      -- get the default value
      select parm.dflt_val into result from parm, ref_object_typ,
      sub_svc, svc
      where sub_svc.sub_svc_id = p_sub_svc_id AND
      sub_svc.sub_svc_status_id != 29 AND
      svc.svc_id = sub_svc.svc_id AND
      ref_object_typ.object_nm = svc.svc_nm AND
      parm.object_id = ref_object_typ.object_id AND
      parm.parm_nm = p_sub_svc_nm;
      RETURN result;
END fn_get_parm_val;
/

CREATE OR REPLACE FUNCTION get_user_assoc_tn_and_ext(assocSubSvcId IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
    IS
      getExt SUB_SVC_PARM.val%TYPE;
	  getTn SUB_SVC_PARM.val%TYPE;
	  
  BEGIN
        	 getTn := fn_get_parm_val(assocSubSvcId, 'telephone_number');
	        getExt:= fn_get_parm_val(assocSubSvcId, 'extension'); 
		IF 
		  getTn IS NOT NULL AND getExt IS NOT NULL  
		THEN
           RETURN getTn ||'x'||getExt;
		ELSIF
		  getExt IS NULL 
		THEN
		   RETURN getTn ;
		ELSIF  
		 getTn IS NULL
		THEN 
		 RETURN  'x'||getExt;
		END IF;

EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_adminstatus(subsvcid IN number)
    return sub_svc_parm.val%TYPE is
    assocsubsvcid number;
    assocparmvalue sub_svc_parm.val%TYPE;
        svcid number;
begin
         select s.sub_svc_id into assocsubsvcid from sub_svc sc,sub_svc_assoc s,ref_assoc_typ t,ref_assoc_rule r where
                r.assoc_typ_id=t.assoc_typ_id AND
                        s.assoc_rule_id=r.assoc_rule_id AND
                        t.assoc_typ_nm='user_has_voice_service' AND s.assoc_sub_svc_id=subsvcid
                        AND s.sub_svc_id = sc.SUB_SVC_ID
                        AND sc.SUB_SVC_STATUS_ID!=29;
         select svc_id into svcid from svc where svc_nm='smp_user';
         assocparmvalue := get_svc_parm_value('smp_user',assocsubsvcid,'company_admin');
         if assocparmvalue is null then
             assocparmvalue := 'N';
         end if;
    return assocparmvalue;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/
CREATE OR REPLACE FUNCTION get_associated_sub_svc_id(subsvcid IN NUMBER,assocNm IN VARCHAR)
    RETURN SUB_SVC_PARM.val%TYPE
    IS
      assocsubsvcid NUMBER;
BEGIN

      select assoc_sub_svc_id into assocsubsvcid from sub_svc_assoc a,sub_svc s
     where
     assoc_rule_id =(select assoc_rule_id from ref_assoc_rule
                          where assoc_typ_id=(select assoc_typ_id from ref_assoc_typ
                               where assoc_typ_nm = assocNm)) and s.sub_svc_id=assoc_sub_svc_id and s.sub_svc_status_id!=29
      and a.sub_svc_id=subsvcid;
    RETURN assocsubsvcid;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/
CREATE OR REPLACE FUNCTION get_secondary_package_info (subsvcid IN number,subid IN number)
    return sub_svc_parm.val%TYPE is	
	childsubsvcid number;
	packageNm sub_svc_parm.val%TYPE;
    packageNm2 sub_svc_parm.val%TYPE;
	st sub_svc_parm.val%TYPE;
	psubsvcid number;
	rsubsvcid number;		
	CURSOR c_chk_pkg
   IS
         select sub_svc_id sbsvcid from sub_svc where parent_sub_svc_id=(
select sub_svc_id from sub_svc where sub_id=subid and svc_id=(
     select svc_id from svc where svc_nm='voice_profiles')) and 
       svc_id not in (select svc_id from svc where svc_nm='company_profile');    
begin	
	 select sub_svc_id into psubsvcid from sub_svc where parent_sub_svc_id=(select parent_sub_svc_id from sub_svc where sub_svc_id=subsvcid) and svc_id= (select svc_id from svc where svc_nm='voice_service_class_of_service');
 	 select sub_svc_id into rsubsvcid from sub_svc where parent_sub_svc_id=psubsvcid and svc_id=(select svc_id from svc where svc_nm='smp_class_of_service_association');	 
	 packageNm := get_svc_parm_value('smp_class_of_service_association',rsubsvcid,'class_of_service_name');
	 FOR r_chk_pkg IN c_chk_pkg LOOP
      select sub_svc_id into childsubsvcid from sub_svc s ,svc v where parent_sub_svc_id=r_chk_pkg.sbsvcid and v.base_svc_id=(select svc_id from svc where svc_nm='class_of_service_definition') and s.svc_id=v.svc_id;
	  select dflt_val into packageNm2 from parm where object_id=(select svc_id from sub_svc where sub_svc_id=childsubsvcid) and parm_nm='class_of_service_name';
	  if packageNm2 = packageNm
      	  then 	  
	  	  		  select dflt_val into st from parm where object_id=(select svc_id from sub_svc where sub_svc_id=childsubsvcid) and parm_nm='secondary_line_package'; 	  			  
	      end if;	 	
  	 END LOOP;
	return st;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/


CREATE OR REPLACE FUNCTION get_parm_tn_and_ext(assocSvc IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
    IS
      valReturn VARCHAR2(20000);
      parmId NUMBER;

BEGIN
     SELECT parm_id INTO parmId FROM PARM
     WHERE parm_id IN (SELECT parm_id FROM PARM WHERE parm_nm in ('telephone_number','extension')) AND object_id = (SELECT svc_id FROM SVC WHERE svc_nm = 'smp_commercial_voice_line') AND class_id = get_class_id ('SubSvcSpec');
     SELECT val INTO valReturn FROM SUB_SVC_PARM WHERE SUB_SVC_ID=assocSvc AND parm_id=parmId;

    RETURN valReturn;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_pilot_tn(parentSubSvcId IN NUMBER ,svcId IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
IS
    getTn SUB_SVC_PARM.val%TYPE;
    subsvcid NUMBER;
    assocsubsvcid NUMBER;
BEGIN
    SELECT sub_svc_id INTO subsvcid FROM SUB_SVC WHERE parent_sub_svc_id=parentSubSvcId  AND svc_id = svcId and SUB_SVC.SUB_SVC_STATUS_ID!=29;
    SELECT assoc_sub_svc_id INTO assocsubsvcid FROM SUB_SVC_ASSOC WHERE sub_svc_id = subsvcid;
    getTn := get_svc_parm_value('primary_voice_line',assocsubsvcid,'telephone_number');
    RETURN getTn;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_pilot_ext(parentSubSvcId IN NUMBER ,svcId IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
IS
    getExt SUB_SVC_PARM.val%TYPE;
    subsvcid NUMBER;
    assocsubsvcid NUMBER;
BEGIN
    SELECT sub_svc_id INTO subsvcid FROM SUB_SVC WHERE parent_sub_svc_id=parentSubSvcId  AND svc_id = svcId and SUB_SVC.SUB_SVC_STATUS_ID!=29;
    SELECT assoc_sub_svc_id INTO assocsubsvcid FROM SUB_SVC_ASSOC WHERE sub_svc_id = subsvcid;
    getExt := get_svc_parm_value('primary_voice_line',assocsubsvcid,'extension');
    RETURN getExt;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_trunk_member_tn(assocSubSvcId IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
IS
    getTn SUB_SVC_PARM.val%TYPE;
BEGIN
    getTn := fn_get_parm_val(assocSubSvcId, 'telephone_number');
    RETURN getTn;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_trunk_member_ext(assocSubSvcId IN NUMBER)
    RETURN SUB_SVC_PARM.val%TYPE
IS
    getExt SUB_SVC_PARM.val%TYPE;
BEGIN
    getExt := fn_get_parm_val(assocSubSvcId, 'extension');
    RETURN getExt ;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_assoc_svc_parm_value(subsvcid IN number,pParmName IN varchar2,assocRuleNm IN varchar2)
    return sub_svc_parm.val%TYPE
is
    parmValue sub_svc_parm.val%TYPE;
    voicelineId sub_svc.sub_svc_id%TYPE;
begin
    select assoc.assoc_sub_svc_id into voicelineId
    from SUB_SVC_ASSOC assoc, REF_ASSOC_RULE r, REF_ASSOC_TYP t
    where assoc.sub_svc_id = subsvcid
    AND assoc.assoc_rule_id = r.assoc_rule_id
    AND r.assoc_typ_id = t.assoc_typ_id
    AND t.assoc_typ_nm = assocRuleNm;
    IF voicelineId IS NOT NULL
    THEN
        select s.val INTO parmValue from sub_svc_parm s,parm p
        where p.parm_nm = pParmName AND
              p.parm_id=s.parm_id AND
              s.sub_svc_id=voicelineId;
        return parmValue;
    ELSE
        return NULL;
    END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_aside_assoc_svc_parm_value(subsvcid IN number,pParmName IN varchar2,assocRuleNm IN varchar2)
    return sub_svc_parm.val%TYPE
is
    parmValue sub_svc_parm.val%TYPE;
    voicelineId sub_svc.sub_svc_id%TYPE;
begin
    select assoc.sub_svc_id into voicelineId
    from SUB_SVC_ASSOC assoc, REF_ASSOC_RULE r, REF_ASSOC_TYP t
    where assoc.assoc_sub_svc_id = subsvcid
    AND assoc.assoc_rule_id = r.assoc_rule_id
    AND r.assoc_typ_id = t.assoc_typ_id
    AND t.assoc_typ_nm = assocRuleNm;
    IF voicelineId IS NOT NULL
    THEN
        select s.val INTO parmValue from sub_svc_parm s,parm p
        where p.parm_nm = pParmName AND
              p.parm_id=s.parm_id AND
              s.sub_svc_id=voicelineId;
        return parmValue;
    ELSE
        return NULL;
    END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_voice_line_info_from_port(subsvcid IN number,pParmName IN varchar2)
    return sub_svc_parm.val%TYPE
is
    parmValue sub_svc_parm.val%TYPE;
begin
    parmValue := get_aside_assoc_svc_parm_value(subsvcid, pParmName, 'tn_uses_equipment');
    IF parmValue IS NOT NULL
    THEN
        return parmValue;
    END IF;

    parmValue := get_aside_assoc_svc_parm_value(subsvcid, pParmName, 'shared_tn_uses_equipment');
    IF parmValue IS NOT NULL
    THEN
        return parmValue;
    ELSE
        return NULL;
    END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_device_mac_addr(parent_subsvc_id IN number, subsvcid IN number,pParmName IN varchar2)
    return sub_svc_parm.val%TYPE
is
    parmValue sub_svc_parm.val%TYPE;
    pSvcNm svc.SVC_NM%TYPE;
begin
    select s.svc_nm into pSvcNm from svc s, sub_svc b where s.svc_id = b.svc_id and b.sub_svc_id = parent_subsvc_id; 
    IF pSvcNm = 'soft_phone' THEN
        return NULL;
    ELSE
        parmValue := fn_get_parm_val(subsvcid, pParmName);
        return parmValue;
    END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_group_tn_and_ext(subsvcid IN number, assocRuleNm IN varchar2)
    RETURN SUB_SVC_PARM.val%TYPE
IS
    getExt SUB_SVC_PARM.val%TYPE;
    getTn SUB_SVC_PARM.val%TYPE;
BEGIN
    getTn := get_assoc_svc_parm_value(subsvcid, 'telephone_number', assocRuleNm);
    getExt:= get_assoc_svc_parm_value(subsvcid, 'extension', assocRuleNm);
    IF getTn IS NOT NULL AND getExt IS NOT NULL
    THEN
        RETURN getTn ||'x'||getExt;
    ELSIF getExt IS NULL
    THEN
        RETURN getTn ;
    ELSIF getTn IS NULL
    THEN
        RETURN  getExt;
    END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
       RETURN NULL;
END;
/

CREATE OR REPLACE FUNCTION get_blf_user_name (pSubSvcId  IN number,userName IN varchar2)
RETURN 	sub_svc_parm.val%TYPE 
IS 
	blfUserName sub_svc_parm.val%TYPE; 
	tmpSubSvcId number;
	tmpAssocSubSvcId number;

BEGIN 
	select assoc_rule_id into tmpAssocSubSvcId
	from ref_assoc_rule l,ref_assoc_typ t 
	where l.assoc_typ_id=t.assoc_typ_id 
	and assoc_typ_nm='blf_tn_uses_equipment';
	
	select sub_svc_id into tmpSubSvcId
	from sub_svc_assoc 
	where assoc_sub_svc_id= pSubSvcId
	and assoc_rule_id= tmpAssocSubSvcId ;
	
	blfUserName :=  get_zend_assoc_svc_parm_value('smp_user',tmpSubSvcId,userName,'user_has_voice_service') ;

   IF blfUserName IS NULL
   THEN
	    RETURN NULL; 
   END IF;

RETURN blfUserName; 

EXCEPTION 
WHEN NO_DATA_FOUND 
THEN 
     RETURN NULL;

END get_blf_user_name;
/

CREATE OR REPLACE FUNCTION get_blf_port_user (pSubSvcId  IN number)
 	RETURN sub_svc_parm.val%TYPE 
 IS 
	blfPortUser sub_svc_parm.val%TYPE; 
	tmpSubSvcId number;
	tmpAssocSubSvcId number;
BEGIN 
	select assoc_rule_id into tmpAssocSubSvcId
	from ref_assoc_rule l,ref_assoc_typ t 
	where l.assoc_typ_id=t.assoc_typ_id 
	and assoc_typ_nm='blf_tn_uses_equipment';

	select sub_svc_id into tmpSubSvcId
	from sub_svc_assoc 
	where assoc_sub_svc_id= pSubSvcId
	and assoc_rule_id= tmpAssocSubSvcId ;

	select p.sub_svc_id into blfPortUser
	from sub_svc_assoc p,sub_svc s
	where s.sub_svc_id=p.sub_svc_id 
	and s.sub_svc_status_id!=29
	and assoc_sub_svc_id=tmpSubSvcId
	and assoc_rule_id=(select assoc_rule_id 
					   from ref_assoc_rule l,ref_assoc_typ t
    				   where l.assoc_typ_id=t.assoc_typ_id
    				   and assoc_typ_nm='user_has_voice_service');

   IF blfPortUser IS NULL
   THEN
   	 RETURN NULL; 
   END IF;
RETURN blfPortUser; 

EXCEPTION 
	WHEN NO_DATA_FOUND 
	THEN 
      RETURN NULL;
 
END get_blf_port_user;
/
