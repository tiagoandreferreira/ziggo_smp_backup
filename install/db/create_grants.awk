#
#  FILE INFO
#    $Id: create_grants.awk,v 1.2 2001/08/29 17:37:59 germans Exp $
#
#  DESCRIPTION
#   Generates reading permission to Oracle's roles and references permission to
#    create foreign keys in another schema 
#
#  PARAMETERS
#     UserSuffix - Oracle user that is granting the privileges 
#     User       - Oracle user receiving the privileges, this name is the suffix for the
#                   role name 
#                - The input file is a list of tables an Oracle User can reference and read 
#
#  REVISION HISTORY
#  * Based on CVS log
BEGIN{
    maxLine       = 0;
    Grant         = "";
    References    = "";
}
{
if (match($2,UserSuffix))
{
 Privilege = "grant select, references on abc123 to user;";
 sub(/abc123/,$1,Privilege);
 sub(/user/,User,Privilege);
 print Privilege;
}
else
{
}
}
