#  FILE INFO
#    $Id: create_grants_cm.awk,v 1.7 2001/11/01 15:38:08 germans Exp $
#
#  DESCRIPTION
#    Generates SQL file with granting select and references privileges on CM tables 
#     and plsql objects to users dm, sp, sd, and st
#
#  PARAMETERS
#     User       - Oracle root user name
#                - The input file is a list of own tables per Oracle User
#
#  REVISION HISTORY
#  * Based on CVS log
#
#================================================================================================
BEGIN{
 Privilege = "";
 Suffix1 = "dm";
 Suffix2 = "sd";
 Suffix3 = "sp";
 Suffix4 = "st";
 Seq     = "";
}
/^[^#-]/{
  if (match(PT,"1"))
  {
    Privilege = "grant select, references on abc123 to userRR;";
  }
  else
  {
    Privilege = "grant execute on abc123 to userRR;";
    Seq = substr($1,length($1)-3); 
    if (Seq == "_SEQ")
    {
      Privilege = "grant select on abc123 to userRR;";
    }
  }
  if (match($1,"SAMP_VER") &&
      match(PT,"1"))
  {
    Privilege = "";
  }
  sub(/abc123/,$1,Privilege);
  sub(/user/,User,Privilege);
  sub(/RR/,Suffix1,Privilege);
  print Privilege;
  sub(/dm/,Suffix3,Privilege);
  print Privilege;
  sub(/sp/,Suffix4,Privilege);
  print Privilege;
  if (match(US,"1"))
  {
   sub(/st/,Suffix2,Privilege);
  }
  else
  {
   Privilege = " ";
  }
  print Privilege;
}
