#==============================================================================
#
#  FILE INFO
#    $Id: create_indexes.awk,v 1.3 2001/10/19 16:35:47 germans Exp $ 
#
#  DESCRIPTION
#    Splits input SQL file to 'list' tables. 
#
#  PARAMETERS
#     ListFile - name of the file with the table list for new schema or
#     File     - name of the new schema file or 
#                sql statement file for loading data
#
#  REVISION HISTORY
#  *  P Chpakovski  1997-09-09
#  *  G Serrano    2001-08-24
#     Added possibility of generating triggers on tables
#
#==============================================================================
BEGIN{
    selectInd                      = 0;
    fk_ind                         = 1;
    ix_ind                         = 1;
    maxLine                        = 0;
    n_a                            = "n/a";
    TableName                      = "";
    PreviousLine                   = "";
    PreviousTableName              = "";
    NullLine                       = "";
    appendixActionNo               = 0;
    appendixActionCreateIndex      = 1;
    appendixActionCreatePrimaryKey = 2;
    appendixAction                 = appendixActionNo;

    while ( getline listBuf[maxLine] < ListFile > 0 )
    {
        if (!match(listBuf[maxLine],"^#"))
        {
            split ( listBuf[maxLine], col);
            listTableName[maxLine]       = col[1];
            listIndexName[maxLine]       = col[2];
            listTableTablespace[maxLine] = col[3];
            listTableInitSize[maxLine]   = col[4];
            listTableNextSize[maxLine]   = col[5];
            listTableFree[maxLine]       = col[6];
            listTableExts[maxLine]       = col[7];
            
            maxLine++
        } 
    }
}
{
    
     if(match($0,"^CREATE") ||
        match($0,"^ALTER")  ||
        match($0," *PRIMARY  *KEY"))
    {
        appendix = "";
        for(i=0; i < maxLine; i++)
        {
         if(match($0,"^CREATE"))
         {
            if (match($0," " listTableName[i] "([^A-Za-z0-9_]|$)") && 
                match($0," " listIndexName[i] "([^A-Za-z0-9_]|$)"))
            {
                appendixAction = appendixActionCreateIndex;
                selectInd=1;
                tabInd= i;
                break
            }
         }
         if(match($0,"^ALTER") &&
            match($0," " listTableName[i] "([^A-Za-z0-9_]|$)"))
         {
           PreviousLine = $0;
           break
         }
         if(match($0," *PRIMARY  *KEY"))
         {
            if (match($0," " listIndexName[i] "([^A-Za-z0-9_]|$)"))
            {
                print PreviousLine;
                appendixAction = appendixActionCreatePrimaryKey;
                selectInd=2;
                tabInd= i;
            }
         }
        }  
    }
    else if (match($0,"^COMMIT"))
    {
         selectInd=1;
    }

    if (selectInd)
    {
        if (match($0,";$"))
        {
            printWithAppendix(appendixAction,tabInd); 
            selectInd = 0
            appendixAction = appendixActionNo;
        }
        else
        {
            print $0
        }
    }

}

function printWithAppendix(appendixAction, tabInd)
{
    appendix = ""
    if(appendixAction == appendixActionCreateIndex)
    {
        if(!match(listTableTablespace[tabInd],n_a))
            appendix = appendix  " TABLESPACE "  listTableTablespace[tabInd]
        if(!match(listTableFree[tabInd],n_a))
            appendix = appendix  " PCTFREE "  listTableFree[tabInd]
        if((!match(listTableInitSize[tabInd],n_a)) || 
           (!match(listTableNextSize[tabInd],n_a)) ||
           (!match(listTableExts[tabInd],n_a)))
        {
            appendix = appendix  " STORAGE ( "
            if( !match(listTableInitSize[tabInd],n_a))
                appendix = appendix  " INITIAL "  listTableInitSize[tabInd]
            if( !match(listTableNextSize[tabInd],n_a))
                appendix = appendix  " NEXT "  listTableNextSize[tabInd]
            if( !match(listTableExts[tabInd],n_a))
                appendix = appendix  " MINEXTENTS "  listTableExts[tabInd]
            appendix = appendix  " )"
        
        }
        print substr($0,1,index($0,";")-1) appendix " ;"
        print NullLine;
    }
    else if (appendixAction == appendixActionCreatePrimaryKey)
    {
        if((!match(listTableInitSize[tabInd],n_a)) ||
           (!match(listTableNextSize[tabInd],n_a)) ||
           (!match(listTableFree[tabInd],n_a)) ||
           (!match(listTableExts[tabInd],n_a)) ||
           (!match(listTableTablespace[tabInd],n_a)))
            appendix = appendix " USING INDEX "
        if(!match(listTableTablespace[tabInd],n_a))
            appendix = appendix  " TABLESPACE "  listTableTablespace[tabInd]
        if(!match(listTableFree[tabInd],n_a))
            appendix = appendix  " PCTFREE "  listTableFree[tabInd]

        if((!match(listTableInitSize[tabInd],n_a)) ||
           (!match(listTableNextSize[tabInd],n_a)))
        {
            appendix = appendix  " STORAGE ( "
            if( !match(listTableInitSize[tabInd],n_a))
                appendix = appendix  " INITIAL "  listTableInitSize[tabInd]
            if( !match(listTableNextSize[tabInd],n_a))
                appendix = appendix  " NEXT "  listTableNextSize[tabInd]
            if( !match(listTableExts[tabInd],n_a))
                appendix = appendix  " MINEXTENTS "  listTableExts[tabInd]
                appendix = appendix  " )"
    
        }
        print substr($0,1,match($0,"\\) *;")-1) appendix ") ;"
        print NullLine;
    }
    else
    {
        print $0
        print NullLine;
    }
}
