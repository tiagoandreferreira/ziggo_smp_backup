#!/usr/bin/ksh
#=============================================================================
#
#  FILE INFO
#
#    $Id: create_jasper_db.sh,v 1.1.2.13 2011/11/14 21:44:00 davidx Exp $
#
#  DESCRIPTION
#
#    Builds and executes SQl files for creating tables, primary key,
#    foreign keys, indexes, private synonyms,
#    and granting select and references privileges
#
#    This script assumes env was loaded by caller with the proper
#    environment settings.
#
#  PARAMETERS
#
#   ORACLE_LOGIN      - Oracle's root username
#   ORACLE_PASSWORD   - Oracle user's password
#   ORACLE_SID        - Oracle's instance name
#   Processing_Type   - Action type the script is going to perform,
#                       Possible values:
#                       1 : generate creation table syntax
#                       2 : generate primary key syntax and appropriate grants
#                       3 : generate both foreign key and index syntax
#
#   INPUT FILES
#
#    js-pro-create    - contains the whole JASPER's DB create table syntax,primary keys syntax,
#                       foreign keys syntax as well as create index syntax
#    quartz           - contains the whole QUARTZ's DB create table syntax,primary keys syntax,
#                       foreign keys syntax as well as create index syntax
#
#  RELATED SCRIPTS
#
#
#  REVISION HISTORY
#  * Based on CVS log
#=============================================================================

LOG_FILE=create_jasper_db.log
echo "create_jasper_db.sh Starting:"         > $LOG_FILE

#-------------------------------------------------------------------------
# Preparing all variables for later use
#-------------------------------------------------------------------------

flag=0

for val in ${JS_DB_USER_ROOT} ${JS_DB_PASSWORD} ${JS_DB} ${JS_ORACLE_THIN_URL}
do
    if [ -n "$(echo $val | egrep '^[\@].*[\@]$')" ]
    then
        flag=1
        break
    fi
done

if [ ${flag} -eq 1 ] && [ ${ENV_TYPE} = 'PROD' ]
then
    echo "Error: Please set following properties in env_user.properties file." | tee -a $LOG_FILE 
    echo "js.ora.env=" | tee -a $LOG_FILE
    echo "js.ora.password=" | tee -a $LOG_FILE
    echo "js.ora.sid=" | tee -a $LOG_FILE
    echo "js.ora.thin.url=" | tee -a $LOG_FILE
    exit 1            
fi

if [ ${flag} -eq 1 ]
then
    if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
    then
        echo Cannot find oracle connection information login/pass/sid
    else
        NAME=$SAMP_DB_USER_ROOT"CM"
        PASS=$SAMP_DB_PASSWORD"@"$SAMP_DB
    fi
else
    NAME=$JS_DB_USER_ROOT
    PASS=$JS_DB_PASSWORD"@"$JS_DB
fi

JASPER_SCHEMA=JASPER_SCHEMA_FILE.SQL
JASPER_CREATE=Y
JASPER_TOOL=$DOMAIN_HOME/$WL_DOMAIN/tools/com.sigma.samp.reporting/scripts
CURR_DIR=`pwd`
export NAME PASS LOG_FILE JASPER_SCHEMA JASPER_CREATE

#-------------------------------------------------------------------------
# Building SQL file
#-------------------------------------------------------------------------

     echo Check script prerequisites       |   tee -a $LOG_FILE
     
     if [ ${flag} -eq 0 ] && [ `echo "${NAME}" | tr '[:lower:]' '[:upper:]' ` != `echo ${SAMP_DB_USER_ROOT}"CM" | tr '[:lower:]' '[:upper:]' ` ]
     then
        #JASPER_CLEANUP=JASPER_CLEANUP.SQL
        #export JASPER_CLEANUP
        echo Cleaning $NAME Schema         |   tee -a $LOG_FILE
        
        #echo set echo on > $JASPER_CLEANUP

        #cat js-pro-drop.ddl     >> $JASPER_CLEANUP

        #cat quartz-drop.ddl     >> $JASPER_CLEANUP

        #echo exit               >> $JASPER_CLEANUP

        echo execute JASPER CLEAN-UP in $NAME schema          |   tee -a $LOG_FILE

        sqlplus  ${NAME}/${PASS} @cleanup_schema.sql           >> $LOG_FILE
        #sqlplus ${NAME}/${PASS} @$JASPER_CLEANUP              >> $LOG_FILE    
     fi
     
     echo Building file $JASPER_SCHEMA for $NAME schema       |   tee -a $LOG_FILE

     echo creating tables ddl for $NAME schema                |   tee -a $LOG_FILE

     echo set echo on > $JASPER_SCHEMA

     cat js-pro-create.ddl   >> $JASPER_SCHEMA

     cat quartz.ddl          >> $JASPER_SCHEMA

     echo exit               >> $JASPER_SCHEMA

     echo execute JASPER DDL in $NAME schema                  |   tee -a $LOG_FILE

     sqlplus ${NAME}/${PASS} @$JASPER_SCHEMA                  >> $LOG_FILE


     cd $JASPER_TOOL
     
     mv config/js.jdbc.properties config/js.jdbc.properties.backup
     
     if [ ${flag} -eq 1 ]
     then
        cat config/js.jdbc.properties.backup | \
        sed -e "s/@js.ora.thin.url@/${ORACLE_THIN_URL}/g" \
            -e "s/@js.ora.env@/${NAME}/g" \
            -e "s/@js.ora.password@/${SAMP_DB_PASSWORD}/g" \
        > config/js.jdbc.properties   
     else
        cat config/js.jdbc.properties.backup | \
        sed -e "s/@js.ora.thin.url@/${JS_ORACLE_THIN_URL}/g" \
            -e "s/@js.ora.env@/${JS_DB_USER_ROOT}/g" \
            -e "s/@js.ora.password@/${JS_DB_PASSWORD}/g" \
        > config/js.jdbc.properties
     fi
     
#    echo import the default users and organization           | tee -a $CURR_DIR/$LOG_FILE
#    echo ./js-import.sh --input-zip ./export/js-catalog-oracle-minimal-pro.zip 2>&1 >> $CURR_DIR/$LOG_FILE

#    ./js-import.sh --input-zip ./export/js-catalog-oracle-minimal-pro.zip 2>&1 >> $CURR_DIR/$LOG_FILE

     echo import the default users, organization and reports  | tee -a $CURR_DIR/$LOG_FILE   

     for file in ./export/*
     do
        if [ -d "$file" ]
        then
            ./js-import.sh --input-dir "$file"  2>&1          >> $CURR_DIR/$LOG_FILE
        fi
     done     
       
#    ./js-import.sh --input-dir export  2>&1                  >> $CURR_DIR/$LOG_FILE
     
     cd $CURR_DIR
#-------------------------------------------------------------------------
# Verifying the result
#-------------------------------------------------------------------------

if egrep -s 'ORA-' $LOG_FILE
then
    echo ....... Errors found. See \'$CURR_DIR\/$LOG_FILE\' for details
    exit 1
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$CURR_DIR\/$LOG_FILE\' for details
    exit 1
elif egrep -s "ORA-" $LOG_FILE
then
   echo Reports Database  created.
   echo ORA- messages found. Please review \'$CURR_DIR\/$LOG_FILE\'
   exit 0
else
   echo Reports Database successfully created. | tee -a $LOG_FILE
   exit 0
fi
