#!/usr/bin/ksh
#==============================================================================
#
#  FILE INFO
#    $Id: create_lens.sh,v 1.4 2011/07/12 20:36:15 zhenl Exp $
#
#  DESCRIPTION
#
#    Create SQL file CREATE_LENS.SQL to load len in Nortel GWC
#    The value of RSRC_NM should be unique within SMP
#
#  REVISION HISTORY
#  * Based on CVS
#==============================================================================

echo "--   Load len scripts" > CREATE_LENS.SQL
let numOfLens=100
# the purpose of bacth number is keeping the value of LEN unique
# the vlaue of batch number should be less than 10
let batchNum=0
for g in GWC-NR3-CL1-1 GWC-NC1-CL3-1 GWC-IN1-CLI2-1 GWC-IN1-CLI4-1; do
  let i=0
  while [ $i -lt $numOfLens ]
  do
    echo "INSERT INTO NTWK_RESOURCE (RSRC_ID,RSRC_NM,SUBNTWK_ID,CONSUMABLE_RSRC_TYP_NM,RSRC_STATE,CREATED_DTM,CREATED_BY) VALUES" >>CREATE_LENS.SQL
    if [ $i -lt 10 ]
    then
      echo "(NTWK_RESOURCE_SEQ.nextval,'LG 00 ${batchNum} 00 0${i}',SubntwkId('${g}'),'LEN','Available',sysdate,'Sigma');" >>CREATE_LENS.SQL
    else
      echo "(NTWK_RESOURCE_SEQ.nextval,'LG 00 ${batchNum} 00 ${i}',SubntwkId('${g}'),'LEN','Available',sysdate,'Sigma');" >>CREATE_LENS.SQL
    fi
    (( i = i + 1 ))
    if [ $i -eq $numOfLens ]
    then
      (( batchNum = batchNum + 1 ))
    fi
  done
done

echo "commit;" >> CREATE_LENS.SQL
echo "/" >> CREATE_LENS.SQL
