#!/usr/bin/ksh
#==============================================================================
#
#  FILE INFO
#    $Id: create_lens_bulk.sh,v 1.1 2011/12/06 19:10:09 jerryc Exp $
#
#  DESCRIPTION
#
#    Create SQL file CREATE_LENS.SQL to load len in Nortel GWC
#    The value of RSRC_NM should be unique within SMP
#
#  REVISION HISTORY
#  * Based on CVS
#==============================================================================

echo "--   Load len scripts" > CREATE_BULK_LENS.SQL
let numOfLens=9999
# the purpose of bacth number is keeping the value of LEN unique
# the vlaue of batch number should be less than 10
let batchNum=0
for g in GWC-NR3-CL1-1 GWC-NC1-CL3-1 GWC-IN1-CLI2-1 GWC-IN1-CLI4-1; do
  let i=0
  let j=0
  let k=0
  while [ $k -lt $numOfLens ]
  do
    echo "INSERT INTO NTWK_RESOURCE (RSRC_ID,RSRC_NM,SUBNTWK_ID,CONSUMABLE_RSRC_TYP_NM,RSRC_STATE,CREATED_DTM,CREATED_BY) VALUES" >>CREATE_BULK_LENS.SQL
    if [ $j -lt 10 ]
    then
      jj="0${j}"
    else
      jj="${j}"
    fi
    if [ $i -lt 10 ]
    then
      ii="0${i}"
    else
      ii="${i}"
    fi
    echo "(NTWK_RESOURCE_SEQ.nextval,'LG 00 ${batchNum} ${jj} ${ii}',SubntwkId('${g}'),'LEN','Available',sysdate,'Sigma');" >>CREATE_BULK_LENS.SQL

    (( i = i + 1 ))
    if [ $i -gt 99 ]
    then
      i=0
      (( j = j + 1 ))
    fi
    (( k = k + 1 ))
    if [ $k -eq $numOfLens ]
    then
      (( batchNum = batchNum + 1 ))
    fi
  done
done

echo "commit;" >> CREATE_BULK_LENS.SQL
echo "/" >> CREATE_BULK_LENS.SQL
