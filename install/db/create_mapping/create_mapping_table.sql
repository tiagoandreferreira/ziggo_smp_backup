--============================================================================
--    $Id: create_mapping_table.sql,v 1.1 2013/02/20 07:24:12 poonamm Exp $
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

CREATE TABLE MSGID_JOBID_MAPPING 
( 
  MSG_ID       VARCHAR2(255), 
  JOB_ID       VARCHAR2(255), 
  created_dtm  DATE 
);
commit;
/
