--============================================================================
--    $Id: create_resource_sel_db.sql,v 1.1 2007/11/29 21:15:28 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
--delete from table resource_assign;

--delete from table ref_resource;

--drop table resource_assign;

--drop table ref_resource;

create table ref_resource
(
 resource_id number(12) not null,
 resource_typ varchar(256) not null,
 resource_tag varchar(256) null,
 created_dtm          DATE NOT NULL,
 created_by           VARCHAR2(30) NOT NULL,
 modified_dtm         DATE NULL,
 modified_by          VARCHAR2(30) NULL
);

alter table ref_resource
  add constraint ref_resource_pk primary key (resource_id);

create table resource_assign
(
 resource_id number(12) not null,
 subntwk_id  number(12) not null,
 created_dtm          DATE NOT NULL,
 created_by           VARCHAR2(30) NOT NULL,
 modified_dtm         DATE NULL,
 modified_by          VARCHAR2(30) NULL
);

alter table resource_assign
  add constraint resource_assign_pk primary key (resource_id,subntwk_id);

alter table resource_assign
  add constraint resource_assign_fk1 foreign key (resource_id)
    references ref_resource;

alter table resource_assign
  add constraint resource_assign_fk2 foreign key (subntwk_id)
    references subntwk;

--============================================================================
--  ResourceId
--============================================================================
CREATE OR REPLACE FUNCTION ResourceId (i_resource_typ IN varchar2,
                                       i_resource_tag IN varchar2)
   return number is
    gg number;
begin

    select resource_id INTO gg from ref_resource
      where resource_typ = i_resource_typ and
        (resource_tag = i_resource_tag or i_resource_tag is null and resource_tag is null);
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddResourceAssign
--============================================================================
CREATE OR REPLACE PROCEDURE AddResourceAssign (
                                       i_resource_id IN NUMBER,
                                       i_subntwk_id IN number) is
BEGIN
    INSERT INTO resource_assign(
         RESOURCE_ID, SUBNTWK_ID, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_resource_id, i_subntwk_id, SYSDATE, 'andy', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating resource_assign : ' || i_resource_id || ' ' || i_subntwk_id || ' - ' || SQLERRM);
END;
/

------------------------------------------------------------------
---------   Insert into table: Ref_Resource   ---------
------------------------------------------------------------------

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_resource---------')
--insert into ref_resource ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, RESOURCE_ID, RESOURCE_TYP, RESOURCE_TAG)
--VALUES ( SYSDATE,'Andy', SYSDATE,'INIT', 1, 'Phone Area Code', '416');

--insert into ref_resource ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, RESOURCE_ID, RESOURCE_TYP, RESOURCE_TAG)
--VALUES ( SYSDATE,'Andy', SYSDATE,'INIT', 2, 'Phone Area Code', '905');

--insert into ref_resource ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, RESOURCE_ID, RESOURCE_TYP, RESOURCE_TAG)
--VALUES ( SYSDATE,'Andy', SYSDATE,'INIT', 3, 'Phone Area Code', null);

------------------------------------------------------------------
---------   Insert into table: Resource_Assign   ---------
------------------------------------------------------------------
--exec AddResourceAssign(ResourceId('Phone Area Code', '416'),SubntwkId('MAG4'));
--exec AddResourceAssign(ResourceId('Phone Area Code', '905'),SubntwkId('MAG6'));
--exec AddResourceAssign(ResourceId('Phone Area Code', null),SubntwkId('MAG4'));
--exec AddResourceAssign(ResourceId('Phone Area Code', '416'),SubntwkId('DHCP_SERVER_NEWYORK'));
--exec AddResourceAssign(ResourceId('Phone Area Code', '905'),SubntwkId('DHCP_SERVER_NEWYORK2'));
--exec AddResourceAssign(ResourceId('Phone Area Code', null),SubntwkId('DHCP_SERVER_NEWYORK2'));

