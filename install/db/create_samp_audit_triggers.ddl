declare
  cursor table_cur is
    select table_name from user_tables;
begin
  for r_table IN table_cur LOOP
    execute immediate ('create or replace trigger '||r_table.table_name||'_tr1 before insert on '
      ||r_table.table_name||' for each row begin  :NEW.created_dtm  := SYSDATE; :NEW.modified_dtm:=NULL; '||
      ':NEW.modified_by:=NULL; end;');
    execute immediate ('create or replace trigger '||r_table.table_name||'_tr2 before update on '
      ||r_table.table_name||' for each row begin  :NEW.modified_dtm:=SYSDATE; end;');
  END LOOP;
end;
/
