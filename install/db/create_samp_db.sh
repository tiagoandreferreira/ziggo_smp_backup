#=============================================================================
#
#  FILE INFO
#    $Id: create_samp_db.sh,v 1.2 2001/08/19 05:46:32 caling Exp $
#
#    SAMP 1.0
#    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
#    All rights reserved.
#
#  DESCRIPTION
#
#    -
#
#  REVISION HISTORY
#  *  Based on CVS log
#=============================================================================

#set -x
sql_dir=$SRC_ROOT/com/sigma/samp/util/sqls
log_dir=$sql_dir

# Create the drop script
grep "^CREATE TABLE" $sql_dir/create_samp_schema.ddl |
  sort | awk {'print "DROP TABLE " $3 " CASCADE CONSTRAINTS;"}' >\
  $sql_dir/drop_samp_tables.tmp

# Drop the schema and reload the schema
sqlplus $ORACLE_LOGIN/$ORACLE_PASSWORD@$ORACLE_SID  <<EOF \
     > $log_dir/create_samp_db.log
@$sql_dir/drop_samp_tables.tmp
@$sql_dir/create_samp_schema.ddl
EOF

grep "^ORA-" $log_dir/create_samp_db.log
