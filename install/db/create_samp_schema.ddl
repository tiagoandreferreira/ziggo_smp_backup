-- creating samp tables
@dba/samp_tables.ddl
@dba/samp_primaries.ddl
@dba/samp_constraints.ddl
@dba/samp_sequences.ddl
@dba/samp_triggers.ddl
set sqlblanklines on
@dba/samp_comments.ddl
