#  FILE INFO
#    $Id: create_sequences.awk,v 1.1 2002/01/28 16:12:36 germans Exp $
#
#  DESCRIPTION
#    Generates DDL file to create sequences on the database
#
#  PARAMETERS
#   - The input file is a list of sequences and the starting value for each of them
#
BEGIN{
 Create = "";
}
/^[^#]/{
 Create = "CREATE SEQUENCE abc123 START WITH 999 CACHE 20;";
 sub(/abc123/,$1,Create);
 sub(/999/,$2,Create);
 print Create;
}
