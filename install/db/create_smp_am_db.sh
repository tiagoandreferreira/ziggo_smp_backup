#================================================================================================
#
#  FILE INFO
#
#    $Id: create_smp_am_db.sh,v 1.9 2005/11/09 19:53:03 dant Exp $
#
#  DESCRIPTION
#
#    Builds and executes SQl files for creating tables, primary key, foreign keys,
#     indexes, private synonyms, and granting select and references privileges
#
#  PARAMETERS
#
#   ORACLE_LOGIN        - Oracle's root username
#   ORACLE_PASSWORD     - Oracle user's password
#   ORACLE_SID          - Oracle's instance name
#   Processing_Type     - Action type the script is going to perform, Possible values:
#                         1 : To generate creation table syntax
#                         2 : To generate both primary key syntax and appropriate grants
#                         3 : To generate both foreign key and index syntax
#
#   INPUT FILES
#
#    samp_tables        - contains the whole SAMP's database create table syntax
#    samp_primaries     - contains the whole SAMP's database create primary keys syntax
#    samp_constraints   - contains the whole SAMP's database create foreign keys syntax
#                          as well as create index syntax
#
#  RELATED SCRIPTS
#
#   create_schema.awk   - To generate creation table syntax, primary key syntax, foreign key
#                        sintax as well as create index syntax
#   create_grants.awk   - To generate grant select to Oracle's roles and grant references to
#                          Oracle's users
#   create_synonyms.awk - To generate both drop private synonym and create private synonym syntax
#
#  REVISION HISTORY
#  * Based on CVS log
#================================================================================================

LOG_FILE=create_smp_am_db.log
echo "create_smp_am_db.sh Starting:"         > $LOG_FILE

#-------------------------------------------------------------------------
# Preparing all variables for later use
#-------------------------------------------------------------------------

NAME=$SAMP_DB_USER_ROOT"am"
PASS_AM=$SAMP_DB_PASSWORD
PASS=$SAMP_DB_PASSWORD"@"$SAMP_DB
THIN_DB=$ORACLE_THIN_URL

if [ "$ENV_TYPE" = "TEST" ]
 then
     TABLE_LIST_AM=table_list.AM.test
     INDEX_LIST_AM=index_list.AM.test
 else
     TABLE_LIST_AM=table_list.AM.prod
     INDEX_LIST_AM=index_list.AM.prod
fi

SCHEMA_AM=AM_SCHEMA_FILE

#-------------------------------------------------------------------------
# Building SQL file
#-------------------------------------------------------------------------


     echo Building file $SCHEMA_AM for $NAME schema  |  tee -a $LOG_FILE

     echo Cleaning AM $NAME schema     |  tee -a $LOG_FILE

     cat cleanup_schema.sql | sqlplus -s ${NAME}/${PASS}     >> $LOG_FILE

     nawk -v ListFile=$TABLE_LIST_AM  -f create_schema.awk am_tables.ddl         > $SCHEMA_AM

     echo Creating tables in ${NAME}/${PASS}                                |  tee -a $LOG_FILE

     nawk -v ListFile=$INDEX_LIST_AM  -f create_indexes.awk am_indexes.ddl    >> $SCHEMA_AM

     echo Creating indexes in ${NAME}/${PASS}     |  tee -a $LOG_FILE

     echo Creating PLSQL CODE and creating reference data for AM  schema  |  tee -a $LOG_FILE

     cat am_others.ddl  >> $SCHEMA_AM

     cat am_features.sql >> $SCHEMA_AM

     cat am_plsql.sql  >> $SCHEMA_AM

     cat ref_data_am.sql >> $SCHEMA_AM

     cat base_am_load.sql >> $SCHEMA_AM

     echo creating AM database in $NAME schema

     cat $SCHEMA_AM | sqlplus -s ${NAME}/${PASS}      >>  $LOG_FILE
     loadjava -resolve -thin -user ${NAME}/${PASS_AM}@${THIN_DB} -verbose Authentication.java  >> $LOG_FILE

. ./smp_db_stats.sh  >> $LOG_FILE

#-------------------------------------------------------------------------
# Verifying the result
#-------------------------------------------------------------------------

if egrep -s 'ERROR' $LOG_FILE
then
    echo ....... Errors found. See \'$LOG_FILE\' for details
    exit 1
elif
 egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 1
elif
   egrep -s ORA_* $LOG_FILE
 then
     echo oracle error found. See \'$LOG_FILE\' |      tee -a $LOG_FILE
     exit 1
 else
     echo AM schema is successfully created |        tee -a $LOG_FILE
     exit 0
fi
