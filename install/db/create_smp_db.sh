#=============================================================================
#
#  FILE INFO
#
#    $Id: create_smp_db.sh,v 1.54.12.1 2011/02/07 16:12:44 rameshk Exp $
#
#  DESCRIPTION
#
#    Builds and executes SQl files for creating tables, primary key,
#    foreign keys, indexes, private synonyms,
#    and granting select and references privileges
#
#    This script assumes env was loaded by caller with the proper
#    environment settings.
#
#  PARAMETERS
#
#   ORACLE_LOGIN      - Oracle's root username
#   ORACLE_PASSWORD   - Oracle user's password
#   ORACLE_SID        - Oracle's instance name
#   Processing_Type   - Action type the script is going to perform,
#                       Possible values:
#                       1 : generate creation table syntax
#                       2 : generate primary key syntax and appropriate grants
#                       3 : generate both foreign key and index syntax
#
#   INPUT FILES
#
#    samp_tables      - contains the whole SMP's DB create table syntax
#    samp_primaries   - contains the whole SMP's DB create primary keys syntax
#    samp_constraints - contains the whole SMP's DB create foreign keys syntax
#                       as well as create index syntax
#
#  RELATED SCRIPTS
#
#   create_schema.awk - To generate creation table syntax, primary key syntax,
#                       foreign key syntax as well as create index syntax
#   create_grants.awk - To generate grant select to Oracle's roles and grant
#                       references to Oracle's users
#
#  REVISION HISTORY
#  * Based on CVS log
#=============================================================================

LOG_FILE=create_smp_db.log
echo "create_smp_db.sh Starting:"         > $LOG_FILE

#-------------------------------------------------------------------------
# Preparing all variables for later use
#-------------------------------------------------------------------------

NAME=$SAMP_DB_USER_ROOT"cm"
PASS=$SAMP_DB_PASSWORD"@"$SAMP_DB

if [ "$ENV_TYPE" = "TEST" ]
 then
#     TABLE_LIST_SMP=table_list.smp.test
#     INDEX_LIST_SMP=index_list.smp.test
     echo "Error:  \"ENV_TYPE\" \"TEST\" is not supported, please set it to \"PROD\"..."  |   tee -a $LOG_FILE
     exit 1
 else
     TABLE_LIST_SMP=table_list.smp.prod
     INDEX_LIST_SMP=index_list.smp.prod
fi

SMP_SCHEMA=SMP_SCHEMA_FILE.SQL
SMP_CREATE=Y    
export NAME PASS LOG_FILE SMP_SCHEMA SMP_CREATE

#LBL=./labels

#-------------------------------------------------------------------------
# Building SQL file
#-------------------------------------------------------------------------

     echo Check script prerequisites       |   tee -a $LOG_FILE

sqlplus -s ${NAME}/${PASS}     <<eof
 WHENEVER SQLERROR exit 1
exec prc_smp_ctx('ctx_ordr_action', 'v1', 'v1');
eof

     if [ "$?" = "1" ]
     then
        echo "******************************************************************************" |   tee -a $LOG_FILE
        echo "ERROR: You need to execute ctx_system.sql as SYSTEM before running this script" |   tee -a $LOG_FILE
        echo "******************************************************************************" |   tee -a $LOG_FILE
        exit 1
     fi

#--- Get database version, two digits

     DBVER=`./get_DBVersion.sh ${NAME}/${PASS} 2`

     if [ "9.2" = "$DBVER" ]
     then
       PKG_ARCH=pkg_arch_92.sql
     else
       PKG_ARCH=pkg_arch.sql
     fi

     export PKG_ARCH

     echo Building file $SMP_SCHEMA for $NAME schema       |   tee -a $LOG_FILE

     echo Cleaning $NAME Schema

      sqlplus  ${NAME}/${PASS} @cleanup_schema.sql           >> $LOG_FILE

     echo creating tables ddl for $NAME schema

     echo set echo on > $SMP_SCHEMA
   
     if [ "$ENV_TYPE" = "TEST" ]
     then 
       nawk -v ListFile=$TABLE_LIST_SMP   -f create_schema.awk smp_tables.ddl \
          >> $SMP_SCHEMA
    
       echo creating ddl for indexes for $NAME Schema

       nawk -v ListFile=$INDEX_LIST_SMP   -f create_indexes.awk smp_indexes.ddl\
          >> $SMP_SCHEMA

      # cat smp_indexes.ddl  >> $SMP_SCHEMA
     else
       nawk -v ListFile=$TABLE_LIST_SMP   -f create_schema.awk smp_tables.ddl \
          >> $SMP_SCHEMA

       echo creating ddl for indexes for $NAME Schema

       nawk -v ListFile=$INDEX_LIST_SMP   -f create_indexes.awk smp_indexes.ddl\
          >> $SMP_SCHEMA
     fi

     echo creating trigers etc for  $NAME Schema

     cat smp_pre_others.ddl   >> $SMP_SCHEMA

     cat smp_others.ddl   >> $SMP_SCHEMA

     cat smp_tune_indexes.sql  >> $SMP_SCHEMA

     cat smp_tune_segments.sql  >> $SMP_SCHEMA

     cat smp_tune_constraints.sql  >> $SMP_SCHEMA

     cat smp_tune_seq.sql  >> $SMP_SCHEMA

     cat smp_tune_dual.sql  >> $SMP_SCHEMA

     echo exit   >> $SMP_SCHEMA

     echo execute SMP DDL in $NAME schema

     sqlplus ${NAME}/${PASS} @$SMP_SCHEMA     >> $LOG_FILE

     #./create_adm_db.sh 

     ./create_smp_plsql.sh

     echo Creating reference data for $NAME schema  \
          | tee -a $LOG_FILE

     echo set echo on > $SMP_SCHEMA.dat.sql

     cat ref_data_cm.sql >> $SMP_SCHEMA.dat.sql

     #cat  $LBL/Lbl_Ref_*.sql $LBL/base_Lbl_*.sql $LBL/err_Lbl_*.sql \
          >> $SMP_SCHEMA.dat.sql

     cat ref_data_sd.sql >> $SMP_SCHEMA.dat.sql

     cat ref_data_sp.sql base_sp_load_all.sql >> $SMP_SCHEMA.dat.sql

     cat ref_data_st.sql >> $SMP_SCHEMA.dat.sql

     #cat ref_data_dm.sql >> $SMP_SCHEMA.dat.sql

     cat base_version.sql >> $SMP_SCHEMA.dat.sql

     echo exit            >> $SMP_SCHEMA.dat.sql


     #load ref_class from xml first

     echo Running: >sync_ref_class.log
     echo java -classpath $CLASSPATH:sync_ref_class.jar com.sigma.vframe.joss.MetaInventory $SMP_CFG_PATH ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${ORACLE_THIN_URL} thin 2>&1 >>sync_ref_class.log
     
     java -classpath $CLASSPATH:$DOMAIN_HOME/$WL_DOMAIN/libs/distribution.jar:$DOMAIN_HOME/$WL_DOMAIN/libs/installation.jar:sync_ref_class.jar com.sigma.vframe.joss.MetaInventory $SMP_CFG_PATH ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${ORACLE_THIN_URL} thin 2>&1 >>sync_ref_class.log

     echo deploy SMP reference data to $NAME schema

     sqlplus  ${NAME}/${PASS} @$SMP_SCHEMA.dat.sql >> $LOG_FILE

#-------------------------------------------------------------------------
# Verifying the result
#-------------------------------------------------------------------------

if egrep -s 'ORA-' $LOG_FILE
then
    echo ....... Errors found. See \'$LOG_FILE\' for details
    exit 1
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 1
elif egrep -s "ORA-" $LOG_FILE
then
   echo SMP Database  created.
   echo ORA- messages found. Please review \'$LOG_FILE\'
   exit 0
else
   echo SMP Database successfully created. |        tee -a $LOG_FILE
   exit 0
fi
