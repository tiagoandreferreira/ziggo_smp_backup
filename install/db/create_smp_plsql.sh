#=============================================================================
#
#  FILE INFO
#
#    $Id: create_smp_plsql.sh,v 1.5 2005/08/16 15:49:19 dant Exp $
#
#  DESCRIPTION
#
#    Builds PL/SQL CODE
#
#    This script assumes following env vars set
#     NAME PASS LOG_FILE SMP_SCHEMA PKG_ARCH
#
#
#  REVISION HISTORY
#  * Based on CVS log
#=============================================================================

echo Creating PL/SQL code for $NAME schema | tee -a $LOG_FILE

if [ -z $NAME ] || [ -z $PASS ] || [ -z $LOG_FILE ] || [ -z $SMP_SCHEMA ] || [ -z $PKG_ARCH ] 
then
    echo
    echo NAME PASS LOG_FILE SMP_SCHEMA PKG_ARCH env. variables have not been set.
    exit 1
fi

echo set echo on      >  $SMP_SCHEMA.plsql

cat am_features.sql   >> $SMP_SCHEMA.plsql

cat cm_features.sql   >> $SMP_SCHEMA.plsql

cat ctx_smp.sql       >> $SMP_SCHEMA.plsql

cat cm_plsql.sql      >> $SMP_SCHEMA.plsql

cat pkg_arch_obj.sql  >> $SMP_SCHEMA.plsql

cat $PKG_ARCH         >> $SMP_SCHEMA.plsql

cat sd_plsql.sql      >> $SMP_SCHEMA.plsql

cat sp_plsql.sql      >> $SMP_SCHEMA.plsql

cat st_plsql.sql      >> $SMP_SCHEMA.plsql

# cat dm_plsql.sql      >> $SMP_SCHEMA.plsql

# cat adm_plsql.sql     >> $SMP_SCHEMA.plsql

echo exit >>             $SMP_SCHEMA.plsql

echo installing SMP PL/SQL in $NAME schema  | tee -a $LOG_FILE

sqlplus  ${NAME}/${PASS} @$SMP_SCHEMA.plsql   |  grep -v "Warning[A-Za-z :]*compilation"     >> $LOG_FILE

