/*=========================================================================

  FILE INFO

    $Id: create_sub_ordr_rm.sql,v 1.2 2003/11/13 17:14:43 vadimg Exp $

  DESCRIPTION

    PKG_ARCH implementation to purge selective SUB_ORDR


  NOTES
  Customize parameters
	'RETAIN'
	'ARCH_SIZE'
	'TGT_TABLESPACE'

  to fit your needs below


  REVISION HISTORY
  * Based on CVS log
========================================================================*/

exec pkg_arch.Prc_adm_auth(user);

exec pkg_arch.Prc_add_group('SUB_ORDR');
exec pkg_arch.Prc_add_tbl('SUB_ORDR', 'SUB_ORDR');
exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'OWNER', USER);
exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'ROOT TABLE', 'SUB');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'ROOT ARCH_MODE', 'purge');

exec pkg_arch.Prc_gen_depy('SUB_ORDR', 'SUB_ORDR', -1);

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'RETAIN', 'SUB_id in (select sub_id from sub_ordr2rm)');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'ARCH_SIZE', 'REC=1000000');
exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'BATCH', '1000');
exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'TGT_TABLESPACE', 'USERS');
exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'NEXT_RUN', to_char(sysdate, 'DD.MM.YYYY HH24:MI:SS'));

update arch_tbl_net set arch_mode = 'purge' where grp_id = (select grp_id from arch_grp where GRP_NM = 'SUB_ORDR');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'STATUS', 'active');

create table sub_ordr2rm(
sub_id  number);


/* Runtime usage example

<... Populate sub_ordr2rm here ...>

exec pkg_arch.Prc_adm_auth(user);

exec pkg_arch.Prc_run_arch('SUB_ORDR');
*/