/*=========================================================================

  FILE INFO

    $Id: create_svc_rm.sql,v 1.2 2003/11/13 14:00:34 vadimg Exp $

  DESCRIPTION

    PKG_ARCH implementation to remove SVC definition from the database


  NOTES
  Use helper procedures

  procedure  rm_svc(p_svc_id number); - to delete a single SVC

  procedure  rm_complex_svc; - to delete subtyped and composed SVC


  REVISION HISTORY
  * Based on CVS log
========================================================================*/

exec pkg_arch.Prc_adm_auth(user);

exec pkg_arch.Prc_add_group('SVC');
exec pkg_arch.Prc_add_tbl('SVC', 'SVC');
exec pkg_arch.Prc_set_grp_parm('SVC', 'OWNER', USER);
exec pkg_arch.Prc_set_grp_parm('SVC', 'ROOT TABLE', 'SVC');

exec pkg_arch.Prc_set_grp_parm('SVC', 'ROOT ARCH_MODE', 'purge');

exec pkg_arch.Prc_gen_depy('SVC', 'SVC', -1);

exec pkg_arch.Prc_set_grp_parm('SVC', 'RETAIN', 'SVC_id in (select SVC_id from svc2rm)');

exec pkg_arch.Prc_set_grp_parm('SVC', 'ARCH_SIZE', 'REC=1000000');
exec pkg_arch.Prc_set_grp_parm('SVC', 'BATCH', '10');
exec pkg_arch.Prc_set_grp_parm('SVC', 'TGT_TABLESPACE', 'USERS');
exec pkg_arch.Prc_set_grp_parm('SVC', 'NEXT_RUN', to_char(sysdate, 'DD.MM.YYYY HH24:MI:SS'));

update arch_tbl_net set arch_mode = 'purge' where grp_id = (select grp_id from arch_grp where GRP_NM = 'SVC');

exec pkg_arch.Prc_set_grp_parm('SVC', 'STATUS', 'active');

create table svc2rm(
svc_id  number);


create or replace procedure rm_svc(p_svc_id number) as
begin
 delete from svc2rm;
 insert into svc2rm values (p_svc_id);
 update svc set base_svc_id = null where svc_id in (select svc_id from svc2rm);
 pkg_arch.Prc_adm_auth(user);
 pkg_arch.Prc_run_arch('SVC');

end;
/

create or replace procedure rm_complex_svc as
begin
 delete from svc2rm;
 insert into svc2rm 
    select distinct r.composed_svc_id from svc_composition_rule r
    union
    select svc_id from svc s where s.base_svc_id is not null;
 update svc set base_svc_id = null where svc_id in (select svc_id from svc2rm);
 pkg_arch.Prc_adm_auth(user);
 pkg_arch.Prc_run_arch('SVC');

end;
/

