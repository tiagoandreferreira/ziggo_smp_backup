#  FILE INFO
#    $Id: create_synonyms.awk,v 1.2 2001/08/29 17:27:08 germans Exp $
#
#  DESCRIPTION
#    Generates SQL file with both drop private synonyms and create private synonyms for other schema objects 
#
#  PARAMETERS
#     User       - Oracle user name owner of the tables for which the synonyms are beign generated for
#                - The input file is a list of own tables per Oracle User
#
BEGIN{
 Drop    = "";
 Create = "";
}
/^[^#]/{
 Create = "Create synonym abc123 for user.abc123;";
 gsub(/abc123/,$1,Create);
 sub(/user/,User,Create);
 print Create;
}
