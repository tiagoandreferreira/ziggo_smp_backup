#==============================================================================
#
#  FILE INFO
#    $Id: create_triggers.awk,v 1.1 2001/09/21 20:09:58 germans Exp $
#
#  DESCRIPTION
#    Splits input SQL file to 'list' tables.
#
#  PARAMETERS
#     ListFile - name of the file with the table list for new schema or
#     File     - name of the new schema file or
#                sql statement file with triggers definition
#
#  REVISION HISTORY
#  *  P Chpakovski  1997-09-09
#     Original version
#  *  G Serrano    2001-09-21
#     Added possibility of generating triggers on tables
#
#==============================================================================
BEGIN{
    selectInd                      = 0;
    maxLine                        = 0;
    triggerNameSuffix              = "";

    while ( getline listBuf[maxLine] < ListFile > 0 )
    {
        if (!match(listBuf[maxLine],"^#"))
        {
            split ( listBuf[maxLine], col);
            listTableName[maxLine]       = col[1];
            listTableTablespace[maxLine] = col[2];
            listTableInitSize[maxLine]   = col[3];
            listTableNextSize[maxLine]   = col[4];
            listTableFree[maxLine]       = col[5];
            listTableExts[maxLine]       = col[6];
            listIndexTablespace[maxLine] = col[7];
            listIndexInitSize[maxLine]   = col[8];
            listIndexNextSize[maxLine]   = col[9];
            listIndexFree[maxLine]       = col[10];
            listIndexExts[maxLine]       = col[11];
            
            maxLine++
        } 
    }
}
{
    if (match($0,"^create"))
    {
        appendix = "";
        if (match($0,"^create *trigger"))
        {
          triggerNameSuffix = substr($3,length($3)-3);
          $3 = substr($3,1,length($3)-4);
        }
        for(i=0; i < maxLine; i++)
        {
            if (match($0," " listTableName[i] "([^A-Za-z0-9_]|$)"))
            {
                selectInd=1;
                tabInd= i;
                break
            }
        }  
    }

    if (selectInd)
    {
        if (match($0,"^create *trigger"))
        {
            New = "tableSEQ";
            sub(/table/,$3,New);
            sub(/SEQ/,triggerNameSuffix,New);
            $3 =  New;
        } 
        if (match($0,"/$"))
        {
            print $0
        }
        else
        {
            print $0
        }
    }

}

