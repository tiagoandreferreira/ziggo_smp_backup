--============================================================================
--    $Id: create_views.sql,v 1.4 2011/01/17 13:20:00 bharath Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================



CREATE OR REPLACE VIEW V_EQUIPMENT_SEARCH
(SUB_ID, EQUIPMENT_ID)
AS 
SELECT sb.SUB_ID, ssp.VAL FROM PARM p, SUB_SVC SS,SUB_SVC_PARM ssp,SUB sb
WHERE  sb.SUB_ID = ss.SUB_ID
AND ss.SUB_SVC_ID = ssp.SUB_SVC_ID
AND p.PARM_ID = ssp.PARM_ID
AND p.PARM_NM = 'id2';
/

CREATE OR REPLACE VIEW V_EQUIPMENT_SEARCH_BY_ID1
(SUB_ID, EQUIPMENT_ID)
AS 
SELECT sb.SUB_ID, ssp.VAL FROM PARM p, SUB_SVC SS,SUB_SVC_PARM ssp,SUB sb
WHERE  sb.SUB_ID = ss.SUB_ID
AND ss.SUB_SVC_ID = ssp.SUB_SVC_ID
AND p.PARM_ID = ssp.PARM_ID
AND p.PARM_NM = 'id1';
/

CREATE OR REPLACE VIEW V_STB_SEARCH_NQ ( SUB_ID, SUB_STATUS, SVC_PROVIDER, 
SALUTATION, FIRST_NAME, MIDDLE_NAME, LAST_NAME, SUFFIX, ACCOUNT_NO, STB_ID
) AS SELECT 
        a.sub_id,
        b.status sub_status,
        fn_get_svc_provider (a.svc_provider_id) svc_provider,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'salutation') salutation,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'first_name') first_name,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'middle_name') middle_name,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'last_name') last_name,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
        pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
        ssp.val stb_id
   FROM sub a,
        ref_status b,
        sub_svc ss, 
	    sub_svc_parm ssp 
  WHERE a.sub_status_id = b.status_id 
    AND b.class_id = 111 
    AND a.sub_typ_id = 1 
    AND a.sub_id=ss.sub_id 
    AND ss.sub_svc_id=ssp.sub_svc_id 
    AND ssp.parm_id in (get_parm_id('id1',100,get_svcid('smp_cpe_cas')),get_parm_id('id2',100,get_svcid('smp_cpe_cas')),get_parm_id('stb_id',100,get_svcid('smp_video_stb')))
/

CREATE OR REPLACE VIEW V_STATUS ( SUB_ID, STATUS, STATUS_ID)
AS SELECT DISTINCT s.sub_id, status,status_id
           FROM sub s, ref_status rf, sub_svc sc, svc sv, sub_svc_parm ssp
          WHERE rf.status_id = s.sub_status_id
            AND s.sub_id = sc.sub_id
            AND sc.svc_id = sv.svc_id
            AND ssp.sub_svc_id = sc.sub_svc_id
            AND rf.status IN ('active', 'mso_block', 'courtesy_block')
            AND sc.sub_svc_status_id != 29
            AND sv.svc_nm IN ('video_event', 'video_subscription')
            AND (    parm_id =
                        get_parm_id ('end_date',
                                     100,
                                     get_svcid ('smp_video_entitlement')
                                    )
                 AND sc.svc_id IN
                        (get_svcid ('video_event'),
                         get_svcid ('video_subscription')
                        )
                 AND ssp.val < (SELECT TO_CHAR (SYSDATE, 'yyyyMMddhhmmss')
                                  FROM DUAL)
                );
/

-- Added for CSC

CREATE OR REPLACE VIEW V_SUB_SERVICE_ID_PHONE_SEARCH
(SUB_ID, SUB_STATUS, SVC_PROVIDER, SALUTATION, FIRST_NAME, 
 MIDDLE_NAME, LAST_NAME, SUFFIX, ACCOUNT_NO, SERVICE_PHONE, 
 SUB_SVC_ID)
AS 
SELECT
          a.sub_id,
          f.status,
          fn_get_svc_provider (d.svc_provider_id),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'first_name') first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'last_name') last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          c.val service_phone,
		  a.SUB_SVC_ID
    FROM  SUB_SVC a, SUB_SVC_PARM c, SUB d, REF_STATUS f, SVC s, PARM p
    WHERE d.sub_id = a.sub_id
      AND d.sub_status_id = f.status_id
      AND a.sub_svc_id = c.sub_svc_id
      AND s.svc_id = a.svc_id
      AND c.parm_id = p.parm_id
      AND p.parm_nm = 'telephone_number'
      AND (s.svc_nm = 'smp_switch_dial_tone_access')
      AND a.sub_svc_status_id != 29;
/
