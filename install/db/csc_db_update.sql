============================================================================
--    $Id: csc_db_update.sql,v 1.1 2007/11/29 21:15:26 siarheig Exp $
--
============================================================================

CREATE TABLE CONTACT_LST
(
  user_phone VARCHAR(10) NOT NULL,
  phone_no VARCHAR(10) NOT NULL,
  name VARCHAR(30) NOT NULL,
  CONSTRAINT PK_CONTACT_LST PRIMARY KEY (user_phone, phone_no )
);

