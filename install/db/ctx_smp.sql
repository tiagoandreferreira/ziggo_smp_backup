/*================================================================================================
#
#  FILE INFO
#
#    $Id: ctx_smp.sql,v 1.12 2007/09/20 12:25:44 davidx Exp $
#
#  DESCRIPTION
#
#    Create context to speedup most popular small-sized lookups.
#    Create procedures to manage these contexts
#
#  PARAMETERS
#    Must be executed as SMP schema owner
#
#  RELATED SCRIPTS
#    Oracle version: 8i and higher
#
#  REVISION HISTORY
#  * Based on CVS log
#================================================================================================*/


create or replace trigger load_class after logon on schema
begin
    for cr in (select class_nm, class_id from ref_class where length(class_nm) <= 30) loop
        prc_smp_ctx('CTX_CLASS', cr.class_nm, cr.class_id);
    end loop;

    for cr in (select svc_nm, svc_id from svc where length(svc_nm) <= 30) loop
        prc_smp_ctx('CTX_SVC', cr.svc_nm, cr.svc_id);
    end loop;

    for cr in (select status, status_id, class_id from ref_status) loop
        if length(cr.class_id||'/'||cr.status) <= 30 then
           prc_smp_ctx('CTX_STATUS', cr.class_id||'/'||cr.status, cr.status_id);
        end if;
        prc_smp_ctx('CTX_STATUS_NM', cr.class_id||'/'||cr.status_id, cr.status);
    end loop;


    for cr in (select REASON_ID, class_id, REASON_NM, upper(IS_DFLT) IS_DFLT from REF_STATUS_CHG_REASON) loop
        if cr.is_dflt = 'Y' then
           prc_smp_ctx('CTX_CHG_REASON_DFLT', cr.class_id, cr.reason_id);
        else
          if length(cr.class_id||'/'||cr.reason_nm) <= 30 then 
             prc_smp_ctx('CTX_CHG_REASON', cr.class_id||'/'||cr.reason_nm, cr.reason_id);
          end if;
        end if;
    end loop;

    
    for cr in (select ORDR_ACTION_ID, ORDR_ACTION_NM from ref_ordr_action) loop
        prc_smp_ctx('ctx_ordr_action', cr.ORDR_ACTION_NM, cr.ORDR_ACTION_ID);
    end loop;
end;
/

