/*================================================================================================
#
#  FILE INFO
#
#    $Id: ctx_system.sql,v 1.3 2003/03/25 16:05:38 vadimg Exp $
#
#  DESCRIPTION
#
#    Create context to speedup most popular small-sized lookups.
#    Create procedures to manage these contexts
#
#  PARAMETERS
#    Must be executed as SYSTEM user  only once per database, regardless of number of deployed SMP
#    SYSTEM must be granted these privileges.
#        -  CREATE ANY CONTEXT
#        -  CREATE PUBLIC SYNONYM
#        -  select on dba_context
#
#  RELATED SCRIPTS
#    Oracle version: 8i and higher
#
#  REVISION HISTORY
#  * Based on CVS log
#================================================================================================*/

declare
 n number;
 l_schema varchar2(30);

 obj_exists exception;
 many_rows  exception;
 pragma  exception_init(obj_exists , -955);
 pragma  exception_init(many_rows,  -1422);

 procedure make_context (p_name varchar2) is
 begin
   execute immediate 'create or replace context '||p_name||' using system.prc_smp_ctx';
 end;

begin
 if user <> 'SYSTEM' then
    raise_application_error(-20001, 'Must be executed as SYSTEM');
 end if;

 for cr in (
   select owner from all_objects
     where object_type like 'PROC%' and OBJECT_NAME like 'PRC_SMP_CTX' and owner <> 'SYSTEM'
     ) loop
    execute immediate 'drop procedure '||cr.owner||'.PRC_SMP_CTX';
 end loop;

 make_context ('ctx_class');
 make_context ('ctx_status');
 make_context ('ctx_status_nm');
 make_context ('ctx_svc');
 make_context ('ctx_parm');
 make_context ('ctx_arch');
 make_context ('ctx_var');
 make_context ('ctx_chg_reason');
 make_context ('ctx_chg_reason_dflt');
 make_context ('ctx_addr_typ');
 make_context ('ctx_ordr_action');

 execute immediate 'create or replace public synonym PRC_SMP_CTX for system.PRC_SMP_CTX';

 execute immediate 'create or replace procedure prc_smp_ctx(p_ctx varchar2, p_key varchar2, p_val varchar2) is '||
 ' begin'||
 '     DBMS_SESSION.SET_CONTEXT(p_ctx, p_key, p_val);'||
 ' end;';

 execute immediate 'grant execute on prc_smp_ctx to public';

end;
/


