--==========================================================================
-- FILE INFO
--   $Id: Action_InTask_Tmplt.dat.sql,v 1.1 2007/11/29 21:13:49 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
--  sample insert for actions in a task
--exec AddActionsInTaskTmplt('tasktemplate','action ', exec_seq,'JMR_EXPR', 'screen name',repeat_parm_id,parent_exec_seq);
----------------------------------------------------------------------------
--------------------------------------------------------
--  subntwk_hw_cmts_mac 
--------------------------------------------------------
exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_subntwk', 2, NULL,'create network element',NULL,NULL);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','assign_subntwk_parent', 3, NULL,NULL,NULL,NULL);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','dummy_subntwk_children', 4, NULL,'Specify number of Mac_domain',NULL,NULL);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_subntwk', 5, '-1','create network element',STMParmId('num_of_mac_domain',Get_Class_Id('stm_project')),4);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','assign_subntwk_parent', 6, NULL,'Specify number of Card',NULL,5);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_card', 7, '-1','create card',STMParmId('num_of_card',Get_Class_Id('stm_project')), 6);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_port', 8, '-1','create port',STMParmId('num_of_down_ports',Get_Class_Id('stm_project')), 7);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_freq', 9, NULL,'Please Select A Frequency For Downstream Port',NULL, 8);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','assign_freq', 10, NULL, NULL,NULL, 9);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_port', 11, '-1','create port',STMParmId('num_of_up_ports',Get_Class_Id('stm_project')), 7);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_freq_grp', 12, NULL,'Please Select A Frequency Group For Upstream Port',NULL, 11);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','assign_freq_grp', 13, NULL,NULL,NULL, 12);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','dummy_port_pair_children', 14, NULL, 'Specify number of Port_pair', NULL, 5);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_card', 15, '-1','Please Select The Card Used For Downstream Port',STMParmId('num_of_port_pairs',Get_Class_Id('stm_project')), 14);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_port', 16, NULL,'Please Select The Downstream Port',NULL, 15);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_card', 17, NULL,'Please Select The Card Used for Upstream Port',NULL, 16);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_port', 18, NULL,'Please Select The Upstream Port',NULL, 17);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_pp', 19, NULL, NULL,NULL, 18);

-- The next action should be assign pp to subntwk
exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','dummy_return_segment_children', 20, NULL, 'Specify number of return segments', NULL, 5);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_subntwk', 21, '-1','lookup network element',STMParmId('num_of_return_segments',Get_Class_Id('stm_project')), 20);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','create_link', 22, NULL, 'Specify number of associated port pairs',NULL, 21);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','lookup_pp', 23, '-1','Please Select A Port Pair Serving This Link',STMParmId('num_of_assoc_pps',Get_Class_Id('stm_project')), 22);

exec AddActionsInTaskTmplt('subntwk_hw_cmts_mac','assign_pp_to_link', 24, NULL,NULL,NULL, 23);

--------------------------------------------------------
--  modify_hw_cmts_mac 
--------------------------------------------------------
exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);
exec AddActionsInTaskTmplt('modify_hw_cmts_mac','update_subntwk_parms', 2, NULL,'update network element properties',NULL,NULL);
exec AddActionsInTaskTmplt('modify_hw_cmts_mac','dummy', 3, NULL,'branch',NULL,NULL);
exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_subntwk', 4, '-2', 'lookup network element', STMParmId('modify_mac_domain',Get_Class_Id('stm_project')),3); 
exec AddActionsInTaskTmplt('modify_hw_cmts_mac','update_subntwk_parms', 5, NULL,'update network element properties',NULL,4);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','dummy', 6, NULL,'branch_2',NULL,NULL);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_subntwk', 7, '-2', 'lookup network element', STMParmId('create_and_associate_link_to_port_pair',Get_Class_Id('stm_project')),6); 

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_subntwk', 8, NULL, 'lookup network element', NULL,7); 

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','create_link', 9, NULL,'Specify number of associated port pairs',NULL, 8);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_pp', 10, '-1','lookup port pair',STMParmId('num_of_port_pairs',Get_Class_Id('stm_project')), 9);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','assign_pp_to_link', 11, NULL,NULL,NULL, 10);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','dummy', 12, NULL,'branch_3',NULL,NULL);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_subntwk', 13, '-2', 'lookup network element', STMParmId('associate_link_to_port_pair',Get_Class_Id('stm_project')),12); 

--exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_subntwk', 14, NULL, 'lookup network element', NULL,13); 

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_link', 14, NULL,'lookup link',NULL, 13);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','dummy', 15, NULL,'Specify number of associated port pairs',NULL,14);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','lookup_pp', 16, '-1','lookup port pair',STMParmId('num_of_port_pairs',Get_Class_Id('stm_project')), 15);

exec AddActionsInTaskTmplt('modify_hw_cmts_mac','assign_pp_to_link', 17, NULL,NULL,NULL, 16);

--------------------------------------------------------
--  modify_ipconfig_cmts_mac
--------------------------------------------------------
exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','dummy', 1, NULL,'branch',NULL,NULL);

-- There is no action 2 in this task
--exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_subntwk',2, '-1', NULL,STMParmId('do_assign_management_ip_to_cmts',Get_Class_Id('stm_project')),1);

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_subnet', 3, '-1', 'lookup ip_subnet', STMParmId('do_assign_management_ip_to_cmts',Get_Class_Id('stm_project')),1); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_ip', 4, NULL, 'lookup ip', NULL,3); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','assign_mgmt_ip', 5, NULL, NULL, NULL,3); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','dummy', 6, NULL,'branch_2',NULL,NULL);

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_subntwk',7, '-2', 'lookup network element',STMParmId('do_assign_ip_scope_to_mac_layer',Get_Class_Id('stm_project')),6);

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','dummy', 8, NULL,'branch',NULL,7);

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_subnet', 9, '-1', 'lookup ip_subnet', STMParmId('do_assign_management_ip_to_mac_layer',Get_Class_Id('stm_project')),8); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_ip', 10, NULL, 'lookup ip', NULL,9); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','assign_mgmt_ip', 11, NULL, NULL, NULL,9); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','dummy', 12, NULL,'branch_2',NULL,7);

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','lookup_subnet', 13, '-2', 'lookup ip_subnet', STMParmId('do_assign_subnet_to_mac_layer',Get_Class_Id('stm_project')),12); 

exec AddActionsInTaskTmplt('modify_ipconfig_cmts_mac','assign_subnet', 14, NULL, NULL, NULL,13); 
--------------------------------------------------------------------
--             decommission_device 
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('decommission_device','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);
exec AddActionsInTaskTmplt('decommission_device','dummy', 2, NULL, 'cascade delete', NULL, NULL);
exec AddActionsInTaskTmplt('decommission_device','delete_subntwk', 3, '-1', 'delete network element', STMParmId('cascade_delete',Get_Class_Id('stm_project')),2); 
--------------------------------------------------------------------
--              subntwk_hw_cmts_no_mac
--------------------------------------------------------------------
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','create_subntwk', 2, NULL,'create network element',NULL,NULL);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','assign_subntwk_parent', 3, NULL,'specify number of mac_domain',NULL,NULL);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','create_card', 4, NULL,'create card',STMParmId('num_of_card',Get_Class_Id('stm_project')),2);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','set_card_to_subntwk', 5, NULL,'assign card to network element',NULL,2);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','create_port', 6, NULL,'create port',STMParmId('num_of_port',Get_Class_Id('stm_project')), 4);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','set_port_to_card', 7, NULL,'assign port to card',NULL, 6);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_freq_grp', 8, 'port_direction="up"','lookup frequency group',NULL, 6);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_freq', 9, 'port_direction="down"','lookup frequency',NULL, 6);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','assign_freq', 10, 'port_direction=="up"','assign frequency group to port',NULL, 6);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','assign_freq_grp', 11, 'port_direction=="down"','assign frequency group to port',NULL, 6);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','create_pp', 12, NULL,'create port pair',NULL, 2);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_port', 13, 'port_direction=="down"','lookup port',NULL, 12);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','assign_port', 14, 'port_direction=="down"',NULL,NULL, 12);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_port', 15, 'port_direction=="up"','lookup port',NULL, 12);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','assign_port', 16, 'port_direction=="down"',NULL,NULL, 12);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_subntwk', 17, NULL,'lookup network element',NULL, 2);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','create_link', 18, NULL,'create network link',NULL, 2);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','lookup_pp', 19, NULL,'lookup port pair',NULL, 18);
--exec AddActionsInTaskTmplt('subntwk_hw_cmts_no_mac','assign_pp_to_link', 20, NULL,'assign port pair to link',NULL, 18);
--
--------------------------------------------------------------------
--   ip_config_mac
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('ip_config_mac','lookup_subnet', 1, NULL, 'Please Select Subnet of the Managment IP for CMTS Layer', NULL,NULL); 

exec AddActionsInTaskTmplt('ip_config_mac','lookup_ip', 2, NULL, 'Please Select the Management Ip', NULL,NULL); 

exec AddActionsInTaskTmplt('ip_config_mac','assign_mgmt_ip', 3, NULL, NULL, NULL,NULL); 

exec AddActionsInTaskTmplt('ip_config_mac','dummy_subntwk_children', 4, NULL, NULL, NULL,NULL); 

exec AddActionsInTaskTmplt('ip_config_mac','lookup_subntwk', 5, 'if exist num_of_mac_domain then num_of_mac_domain else "-1"', 'Lookup MAC Domain Layer', STMParmId('num_of_mac_domain',Get_Class_Id('stm_project')),4); 

exec AddActionsInTaskTmplt('ip_config_mac','dummy', 6, NULL, 'Assign Managment Ip to MAC Domain Layer', NULL,5); 

exec AddActionsInTaskTmplt('ip_config_mac','lookup_subnet', 7, -1, 'Please Select Subnet of Management IP for MAC Domain Layer', STMParmId('is_assign_ip_mgmt_required',Get_Class_Id('stm_project')),6); 

exec AddActionsInTaskTmplt('ip_config_mac','lookup_ip', 8, NULL, 'Please Select Management IP for MAC Domain Layer', NULL,7); 

exec AddActionsInTaskTmplt('ip_config_mac','assign_mgmt_ip', 9, NULL, NULL, NULL,8); 

exec AddActionsInTaskTmplt('ip_config_mac','dummy', 10, NULL, 'Assign Ip Subnet to MAC Domain Layer', NULL,5); 

exec AddActionsInTaskTmplt('ip_config_mac','lookup_subnet', 11, -2, 'Please Select The IP Subnet To Be Assigned', STMParmId('is_assign_ip_subnet_req',Get_Class_Id('stm_project')),10); 

exec AddActionsInTaskTmplt('ip_config_mac','assign_subnet', 12, NULL, NULL, NULL,11); 

exec AddActionsInTaskTmplt('ip_config_mac','dummy', 13, NULL, 'Link MAC domain to a network cluster', NULL,5); 

exec AddActionsInTaskTmplt('ip_config_mac','lookup_subntwk', 14, '-1', 'Lookup Network Cluster', STMParmId('associate_to_network_cluster',Get_Class_Id('stm_project')),13); 

exec AddActionsInTaskTmplt('ip_config_mac','create_link', 15, NULL, NULL,NULL, 14);
---------------------------------------------------------------------
-- create_subntwk
-------------------------------------------------------------------

exec AddActionsInTaskTmplt('create_subntwk','lookup_subntwk', 1, NULL, 'lookup scope network element', NULL,NULL); 
exec AddActionsInTaskTmplt('create_subntwk','create_subntwk', 2, NULL, 'create network element', NULL,NULL); 
exec AddActionsInTaskTmplt('create_subntwk','assign_subntwk_parent', 3, NULL, NULL, NULL,NULL); 

---------------------------------------------------------------------
-- modify_subntwk
-------------------------------------------------------------------

exec AddActionsInTaskTmplt('modify_subntwk','lookup_subntwk', 1, NULL, 'lookup network element', NULL,NULL); 
exec AddActionsInTaskTmplt('modify_subntwk','update_subntwk_parms', 2, NULL, 'update network element properties', NULL,NULL); 
exec AddActionsInTaskTmplt('modify_subntwk','lookup_subntwk', 3, '-1', 'lookup network element',STMParmId('assign_new_parent',Get_Class_Id('stm_project')),2);
exec AddActionsInTaskTmplt('modify_subntwk','update_subntwk_parent', 4, NULL, NULL, NULL,3); 

---------------------------------------------------------------------
-- delete_subntwk
-------------------------------------------------------------------

exec AddActionsInTaskTmplt('delete_subntwk','lookup_subntwk', 1, NULL, 'lookup network element', NULL,NULL); 
exec AddActionsInTaskTmplt('delete_subntwk','delete_subntwk', 2, NULL, 'delete network element', NULL,NULL); 
--------------------------------------------------------------------
--   Congestion_relief
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('congestion_relief','lookup_link', 31, NULL, 'lookup link', NULL,NULL); 
exec AddActionsInTaskTmplt('congestion_relief','delete_link', 32, NULL, 'delete link',NULL, NULL);
exec AddActionsInTaskTmplt('congestion_relief','lookup_subntwk', 33, NULL, 'lookup network element', NULL, NULL);
exec AddActionsInTaskTmplt('congestion_relief','create_link', 34, NULL, 'create link', NULL, NULL);
exec AddActionsInTaskTmplt('congestion_relief','lookup_pp', 35, '-1', 'lookup port pair', STMParmId('num_of_port_pairs',Get_Class_Id('stm_project')), 34);
exec AddActionsInTaskTmplt('congestion_relief','assign_pp_to_link', 36, NULL, '', NULL, 35);

--------------------------------------------------------------------
--  create_subnet 
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('create_subnet','lookup_block', 1, NULL, 'lookup ip_block', NULL, NULL);
exec AddActionsInTaskTmplt('create_subnet','create_subnet', 2, NULL, 'create ip_subnet', NULL, NULL);
exec AddActionsInTaskTmplt('create_subnet','lookup_subntwk', 3, '-1', 'lookup network element', STMParmId('assign_ip_subnet',Get_Class_Id('stm_project')), 2);
exec AddActionsInTaskTmplt('create_subnet','assign_subnet', 4, NULL, NULL, NULL, 3);
--------------------------------------------------------------------
--  decommission_ip_subnet 
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('decommission_subnet','lookup_subnet', 1, NULL, 'lookup ip_subnet', NULL, NULL);
exec AddActionsInTaskTmplt('decommission_subnet','release_ip_subnet', 2, NULL, NULL, NULL, NULL);
--------------------------------------------------------------------
--  modify_ip_subnet 
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('modify_subnet','lookup_subnet', 1, NULL, 'lookup ip_subnet', NULL, NULL);
exec AddActionsInTaskTmplt('modify_subnet','update_subnet_status', 2, NULL, 'update ip_subnet status', NULL, NULL);
--------------------------------------------------------------------
--  delete_ip_subnet 
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('delete_subnet','lookup_subnet', 1, NULL, 'lookup ip_subnet', NULL, NULL);
exec AddActionsInTaskTmplt('delete_subnet','delete_subnet', 2, NULL, NULL, NULL, NULL);
--------------------------------------------------------------------
--  assign_subnet 
--------------------------------------------------------------------
exec AddActionsInTaskTmplt('assign_subnet','dummy', 1, NULL, 'assign an IP subnet to a network device', NULL, NULL);
exec AddActionsInTaskTmplt('assign_subnet','lookup_subnet', 2, '-2', 'lookup ip_subnet', STMParmId('assign_ip_subnet',Get_Class_Id('stm_project')), 1);
exec AddActionsInTaskTmplt('assign_subnet','lookup_subntwk', 3, NULL, 'lookup network element', NULL, 2);
exec AddActionsInTaskTmplt('assign_subnet','assign_subnet', 4, NULL, NULL, NULL, 2);

--------------------------------------------------------
-- create_rf_card_port_pps 
--------------------------------------------------------
exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','dummy', 2, NULL, 'branch', NULL, NULL);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_subntwk', 3, '-2', 'lookup network element', STMParmId('create_card',Get_Class_Id('stm_project')), 2);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','create_card', 4, NULL,'create card',NULL, 3);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','dummy', 5, NULL, 'branch', NULL, 4);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','create_port', 6, '-2','Create Downstream Port',STMParmId('create_down_port',Get_Class_Id('stm_project')), 5);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_freq', 7, NULL,'lookup frequency',NULL, 6);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','assign_freq', 8, NULL,'assign frequency to port',NULL, 7);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','dummy', 9, NULL, 'branch_2', NULL, 4);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','create_port', 10, '-2','Create Upstream Port',STMParmId('create_up_port',Get_Class_Id('stm_project')), 9);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_freq_grp', 11, NULL,'lookup frequency group',NULL, 10);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','assign_freq_grp', 12, NULL,NULL,NULL, 11);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','dummy', 13, NULL, 'branch_2', NULL, NULL);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_subntwk', 14, '-2', 'lookup network element', STMParmId('create_port_pair',Get_Class_Id('stm_project')),13);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','dummy', 15, NULL, 'branch', NULL, 14);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_card', 16, '-1','Lookup Card From Where DownStream Port Will be Selected',STMParmId('num_of_port_pairs',Get_Class_Id('stm_project')), 15);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_port', 17, NULL,'Lookup DownStream Port',NULL, 16);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_card', 18, NULL,'Lookup Card From Where UpStream Port Will be Selected',NULL, 17);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','lookup_port', 19, NULL,'lookup port',NULL, 18);

exec AddActionsInTaskTmplt('create_rf_card_port_pps','create_pp', 20, NULL, NULL,NULL, 19);

--=================================================
-- Modify RF Card with Ports
--=================================================
exec AddActionsInTaskTmplt('modify_rf_card_port_pps','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','dummy', 2, NULL, 'branch', NULL, NULL);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','lookup_card', 3, '-2', 'lookup network element', STMParmId('modify_card',Get_Class_Id('stm_project')), 2);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','update_card_parms', 4, NULL,'Update Card Properties',NULL, 3);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','dummy', 5, NULL, 'branch', NULL, 3);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','lookup_port', 6, '-2','Modify Downstream Port',STMParmId('modify_downstream_port',Get_Class_Id('stm_project')), 5);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','lookup_freq', 7, NULL,'lookup frequency',NULL, 6);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','update_port_freq', 8, NULL, NULL, NULL, 7);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','dummy', 9, NULL, 'branch_2', NULL, 3);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','lookup_port', 10, '-2','Modify Upstream Port',STMParmId('modify_upstream_port',Get_Class_Id('stm_project')), 9);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','lookup_freq_grp', 11, NULL,'lookup frequency group',NULL, 10);

exec AddActionsInTaskTmplt('modify_rf_card_port_pps','update_port_freq_grp', 12, NULL,NULL,NULL, 11);

--==========================================================================
-- create_mac_domain_hw 
--==========================================================================
exec AddActionsInTaskTmplt('create_mac_domain_hw','lookup_subntwk', 1, NULL,'lookup network element',NULL,NULL);

exec AddActionsInTaskTmplt('create_mac_domain_hw','dummy_subntwk_children', 2, NULL, 'Specify number of Mac_domain', NULL, NULL);

exec AddActionsInTaskTmplt('create_mac_domain_hw','create_subntwk', 3, '-1', 'create network element', STMParmId('num_of_mac_domain',Get_Class_Id('stm_project')), 2);

exec AddActionsInTaskTmplt('create_mac_domain_hw','assign_subntwk_parent', 4, NULL,'Specify number of Card',NULL,3);

exec AddActionsInTaskTmplt('create_mac_domain_hw','create_card', 5, '-1','create card',STMParmId('num_of_card',Get_Class_Id('stm_project')), 4);

exec AddActionsInTaskTmplt('create_mac_domain_hw','dummy', 12, NULL, 'branch', NULL, 5);

exec AddActionsInTaskTmplt('create_mac_domain_hw','create_port', 6, '-1','Create Downstream Port',STMParmId('num_of_down_ports',Get_Class_Id('stm_project')), 12);

exec AddActionsInTaskTmplt('create_mac_domain_hw','lookup_freq', 7, NULL,'lookup frequency',NULL, 6);

exec AddActionsInTaskTmplt('create_mac_domain_hw','assign_freq', 8, NULL,NULL,NULL, 7);

exec AddActionsInTaskTmplt('create_mac_domain_hw','dummy', 13, NULL, 'branch_2', NULL, 5);

exec AddActionsInTaskTmplt('create_mac_domain_hw','create_port', 9, '-1','Create Upstream Port',STMParmId('num_of_up_ports',Get_Class_Id('stm_project')), 13);

exec AddActionsInTaskTmplt('create_mac_domain_hw','lookup_freq_grp', 10, NULL,'lookup frequency group',NULL, 9);

exec AddActionsInTaskTmplt('create_mac_domain_hw','assign_freq_grp', 11, NULL,NULL,NULL, 10);


--==========================================================================
-- create_mac_domain_ipconfig
--==========================================================================
exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','dummy_subntwk_children', 1, NULL, NULL, NULL, NULL);

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','lookup_subntwk', 2, '-1', 'lookup network element',STMParmId('num_of_mac_domain',Get_Class_Id('stm_project')),1);

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','dummy', 3, NULL,'branch',NULL,2);

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','lookup_subnet', 4, '-1', 'lookup ip_subnet', STMParmId('do_assign_management_ip_to_mac_layer',Get_Class_Id('stm_project')),3); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','lookup_ip', 5, NULL, 'lookup ip', NULL,4); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','assign_mgmt_ip', 6, NULL, NULL, NULL,5); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','dummy', 7, NULL,'branch_2',NULL,2);

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','lookup_subnet', 8, '-2', 'lookup ip_subnet', STMParmId('do_assign_subnet_to_mac_layer',Get_Class_Id('stm_project')),7); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','assign_subnet', 9, NULL, NULL, NULL,8); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','dummy', 10, NULL, 'Link MAC domain to a network cluster', NULL,2); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','lookup_subntwk', 11, '-1', 'Lookup Network Cluster', STMParmId('associate_to_network_cluster',Get_Class_Id('stm_project')),10); 

exec AddActionsInTaskTmplt('create_mac_domain_ipconfig','create_link', 12, NULL, NULL,NULL, 11);
