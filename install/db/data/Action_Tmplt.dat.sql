--==========================================================================
-- FILE INFO
--   $Id: Action_Tmplt.dat.sql,v 1.1 2007/11/29 21:13:53 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddActionTmplt('lookup_subntwk','look up subnetwork', 'lookup', 'VLD');
exec AddActionTmplt('create_subntwk','create subnetwork', 'create', 'VLD');
exec AddActionTmplt('delete_subntwk','delete subnetwork', 'delete', 'VLD');
exec AddActionTmplt('assign_subntwk_parent','assign parent subnetwork', 'create', 'VLD');
exec AddActionTmplt('create_card','create card', 'create', 'VLD');
exec AddActionTmplt('set_card_to_subntwk','assign card to subnetwork', 'create', 'VLD');
exec AddActionTmplt('create_port','create port', 'create', 'VLD');
exec AddActionTmplt('lookup_port','look up port', 'lookup', 'VLD');
exec AddActionTmplt('lookup_card','look up card', 'lookup', 'VLD');
exec AddActionTmplt('set_port_to_card','assign port to card', 'create', 'VLD');
exec AddActionTmplt('lookup_freq','look up frequency', 'lookup', 'VLD');
exec AddActionTmplt('lookup_freq_grp','look up frequency group', 'lookup', 'VLD');
exec AddActionTmplt('assign_freq','assign frequency', 'create', 'VLD');
exec AddActionTmplt('assign_freq_grp','assign frequency group', 'create', 'VLD');
exec AddActionTmplt('create_pp','create port pair', 'create', 'VLD');
exec AddActionTmplt('assign_port','assign port', 'create', 'VLD');
exec AddActionTmplt('lookup_pp','look up port pair', 'lookup', 'VLD');
exec AddActionTmplt('assign_pp_to_link','assign port pair to link', 'create', 'VLD');
exec AddActionTmplt('lookup_block','look up CIDR block', 'lookup', 'VLD');
exec AddActionTmplt('lookup_subnet','look up subnet', 'lookup', 'VLD');
exec AddActionTmplt('lookup_ip','look up ip', 'lookup', 'VLD');
exec AddActionTmplt('assign_mgmt_ip','Assign management ip', 'create', 'VLD');
exec AddActionTmplt('assign_subnet','Assign Subnet', 'create', 'VLD');
exec AddActionTmplt('release_ip_subnet','Release IP Subnet', 'delete', 'VLD');
exec AddActionTmplt('update_ip_subnet_status','Update IP Subnet Status', 'update', 'VLD');
exec AddActionTmplt('update_subntwk_status','Update Subnetwork Status', 'update', 'VLD');
exec AddActionTmplt('update_parms','Update Parameters', 'update', 'VLD');
exec AddActionTmplt('delete_link','Delete Link', 'delete', 'VLD');
exec AddActionTmplt('create_link','Create Link', 'create', 'VLD');
exec AddActionTmplt('update_subntwk_parent','Update subnetwork Parent', 'update', 'VLD');
exec AddActionTmplt('update_subnet_status','update subnet status', 'update', 'VLD');
exec AddActionTmplt('delete_subnet','Delete Subnet', 'delete', 'VLD');
exec AddActionTmplt('lookup_link','Lookup Link', 'lookup', 'VLD');
exec AddActionTmplt('create_subnet','create Subnet', 'create', 'VLD');
exec AddActionTmplt('update_subntwk_parms','Update subnetwork Parameters', 'update', 'VLD');
-- action type for dummys is from technical point of view
exec AddActionTmplt('dummy_lookup','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_subntwk_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_port_pair_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_return_segment_children','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy_decommission_device','dummy action', 'update', 'VLD');
exec AddActionTmplt('dummy','dummy action', 'update', 'VLD');

--==========================================================================
exec AddActionTmplt('update_card_parms','Update card Parameters', 'update', 'VLD');

exec AddActionTmplt('update_port_freq','Update port frequency', 'update', 'VLD');

exec AddActionTmplt('update_port_freq_grp','Update port frequency group', 'update', 'VLD');
