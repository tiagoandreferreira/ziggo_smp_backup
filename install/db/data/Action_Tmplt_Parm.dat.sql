--==========================================================================
-- FILE INFO
--   $Id: Action_Tmplt_Parm.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('lookup_subntwk','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('scope_subntwk_id', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('scope_subntwk_nm', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('lookup_subntwk',NULL, STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('lookup_link',NULL, STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('to_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_link','link', STMParmId('from_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');

--exec AddActionTmpltParm('lookup_link',NULL, STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('to_subntwk_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link',NULL, STMParmId('from_subntwk_typ', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('lookup_link','link', STMParmId('link_typ', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('to_subntwk_nm', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_link','link', STMParmId('from_subntwk_nm', Get_Class_Id('stm_project')), 'n');
--exec AddActionTmpltParm('lookup_link','link', STMParmId('link_direction', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('lookup_freq','freq', STMParmId('freq_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('lookup_freq_grp','freq_grp', STMParmId('freq_grp_id', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('lookup_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_port',NULL, STMParmId('port_direction', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('lookup_port',NULL, STMParmId('card_id', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('lookup_pp','port_pair', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_pp','port_pair', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_pp',NULL, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('mask', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block','consumable_rsrc', STMParmId('start_ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_block',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
--	lookup_subnet
--==========================================================================
exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');

--exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('ip_sbnt_msk', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('lookup_subnet','consumable_rsrc', STMParmId('managing_subntwk_id', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('lookup_subnet',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--exec AddActionTmpltParm('lookup_subnet',NULL, STMParmId('is_private_ip', Get_Class_Id('stm_project')), 'y');
--==========================================================================

exec AddActionTmpltParm('lookup_ip','consumable_rsrc', STMParmId('ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_ip','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_ip',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
--exec AddActionTmpltParm('lookup_ip',NULL, STMParmId('is_private_ip', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_ip','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('lookup_card','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('lookup_card',null, STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');

--==========================================================================

exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('subntwk_nm', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subntwk','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'y');

--==========================================================================
exec AddActionTmpltParm('dummy_subntwk_children',NULL, STMParmId('num_of_mac_domain', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('dummy_port_pair_children',NULL, STMParmId('num_of_port_pairs', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('dummy_return_segment_children',NULL, STMParmId('num_of_return_segments', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('dummy_decommission_device',NULL, STMParmId('cascade_delete', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('assign_subntwk_parent','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subntwk_parent','assoc', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_subntwk_parent','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
-- action create_card
--==========================================================================
-- obsolete, replaced by srl_num
--exec AddActionTmpltParm('create_card','card', STMParmId('rf_serial_num', Get_Class_Id('stm_project')), 'n');

-- obsolete, replaced by chassis_slot_number
--exec AddActionTmpltParm('create_card','card', STMParmId('slot_num', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('create_card','card', STMParmId('srl_num', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('create_card','card', STMParmId('chassis_slot_number', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('create_card','card', STMParmId('card_typ_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('create_card','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'y');

exec AddActionTmpltParm('create_card','card', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================
-- Should not exist
exec AddActionTmpltParm('set_card_to_subntwk','assoc', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('set_card_to_subntwk','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('set_card_to_subntwk','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

--exec AddActionTmpltParm('create_port','port', STMParmId('port_num', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_port','port', STMParmId('port_direction', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_port','port', STMParmId('port_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_port','port', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================
-- Should not exist
exec AddActionTmpltParm('set_port_to_card','assoc', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('set_port_to_card','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('set_port_to_card','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('assign_freq','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq','assoc', STMParmId('freq_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('assign_freq_grp','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq_grp','assoc', STMParmId('freq_grp_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_freq_grp','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_pp','port_pair', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('assign_port','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_port','assoc', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_port','assoc', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_port','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('create_link','link', STMParmId('from_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_link','link', STMParmId('to_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_link','link', STMParmId('link_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('upstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('downstream_port_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_pp_to_link','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('ip_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('assign_mgmt_ip','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');

--exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('assign_subnet','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================
exec AddActionTmpltParm('release_ip_subnet','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('release_ip_subnet','assoc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');

--exec AddActionTmpltParm('release_ip_subnet','assoc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('release_ip_subnet',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('release_ip_subnet',NULL, STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'y');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_block_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_sbnt_msk', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('ip_max_hosts', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('create_subnet','consumable_rsrc', STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('update_parms','', STMParmId('subntwk_typ', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_subntwk_status','', STMParmId('status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_status','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('parent_subntwk_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subntwk_parent','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_subnet_status','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subnet_status','consumable_rsrc', STMParmId('ip_status', Get_Class_Id('stm_project')), 'n');
exec AddActionTmpltParm('update_subnet_status',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
--	
--==========================================================================
exec AddActionTmpltParm('delete_subnet','consumable_rsrc', STMParmId('ip_sbnt_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('delete_subnet','consumable_rsrc', STMParmId('ip_sbnt_addr', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('delete_subnet',NULL, STMParmId('consumable_resource_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================

exec AddActionTmpltParm('delete_subntwk','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('delete_subntwk','subntwk', STMParmId('cascade_delete', Get_Class_Id('stm_project')), 'n');
--==========================================================================

exec AddActionTmpltParm('delete_link','link', STMParmId('link_id', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_subntwk_parms','subntwk', STMParmId('subntwk_id', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_card_parms','card', STMParmId('card_id', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_port_freq','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_port_freq','assoc', STMParmId('freq_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_port_freq','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');

--==========================================================================
exec AddActionTmpltParm('update_port_freq_grp','assoc', STMParmId('port_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_port_freq_grp','assoc', STMParmId('freq_grp_id', Get_Class_Id('stm_project')), 'n');

exec AddActionTmpltParm('update_port_freq_grp','assoc', STMParmId('association_typ', Get_Class_Id('stm_project')), 'n');
