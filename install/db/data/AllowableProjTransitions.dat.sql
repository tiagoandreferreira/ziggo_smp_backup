--==========================================================================
-- FILE INFO
--   $Id: AllowableProjTransitions.dat.sql,v 1.1 2007/11/29 21:13:51 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
--exec  AddAllowableProjTransitions(to_project,from_project);
----------------------------------------------------------------
exec  AddAllowableProjTransitions('locked','open');
exec  AddAllowableProjTransitions('open','locked');
exec  AddAllowableProjTransitions('staged','locked');
exec  AddAllowableProjTransitions('locked','staged');
exec  AddAllowableProjTransitions('open','staged');
exec  AddAllowableProjTransitions('open', 'init');
exec  AddAllowableProjTransitions('locked', 'init');

exec  AddAllowableProjTransitions('staged', 'open');
exec  AddAllowableProjTransitions('aborted', 'open');
exec  AddAllowableProjTransitions('aborted', 'locked');
exec  AddAllowableProjTransitions('aborted', 'staged');


exec  AddAllowableProjTransitions('provisioning', 'staged');
exec  AddAllowableProjTransitions('failed', 'provisioning');

exec  AddAllowableProjTransitions('synchronizing', 'staged');
exec  AddAllowableProjTransitions('synchronizing', 'provisioning');
exec  AddAllowableProjTransitions('successful', 'synchronizing');
exec  AddAllowableProjTransitions('synch_failed', 'synchronizing');
exec  AddAllowableProjTransitions('open', 'failed');
exec  AddAllowableProjTransitions('staged', 'failed');

exec  AddAllowableProjTransitions('synchronizing', 'synch_failed');
exec  AddAllowableProjTransitions('aborted', 'provisioning');
