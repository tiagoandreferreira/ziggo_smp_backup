--==========================================================================
-- FILE INFO
--   $Id: AllowableTaskTransitions.dat.sql,v 1.1 2007/11/29 21:13:55 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
--exec  AddAllowableTaskTransitions(from_task,to_task);
---------------------------------------------------
exec  AddAllowableTaskTransitions('available','completed');
exec  AddAllowableTaskTransitions('init','available');
