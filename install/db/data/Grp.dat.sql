--==========================================================================
-- FILE INFO
--   $Id: Grp.dat.sql,v 1.3 2011/12/28 11:33:28 prithvim Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.SuperUser', 'STM Super User',  sysdate,  'INIT' , NULL, NULL); 
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.OutsidePlantData', 'STM Outside Plant Data',  sysdate,  'INIT' , NULL, NULL); 
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.InsidePlantData', 'STM Inside Plant Data',  sysdate,  'INIT' , NULL, NULL); 
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.FtfRebuildConRelief', 'STM FTF Rebuild Cong Relief',  sysdate,  'INIT' , NULL, NULL); 
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.QA', 'STM QA ',  sysdate,  'INIT' , NULL, NULL); 
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.IPDevComm', 'STM IP Dev Comm',  sysdate,  'INIT' , NULL, NULL); 
INSERT INTO GRP ( GRP_NM, GRP_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.IPMgmt', 'STM IP Mgmt',  sysdate,  'INIT' , NULL, NULL); 
