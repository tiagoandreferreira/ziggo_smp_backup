--==========================================================================
-- FILE INFO
--   $Id: Grp_User.dat.sql,v 1.3 2011/12/28 11:33:28 prithvim Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the sigma implementation spec
--==========================================================================

INSERT INTO GRP_USR ( GRP_NM, USR_NM, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.SuperUser', 'superuser',  SYSDATE, 'INIT', NULL, NULL);

INSERT INTO GRP_USR ( GRP_NM, USR_NM, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.OutsidePlantData', 'mario',  SYSDATE, 'INIT', NULL, NULL);

INSERT INTO GRP_USR ( GRP_NM, USR_NM, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.FtfRebuildConRelief', 'leah',  SYSDATE, 'INIT', NULL, NULL);

INSERT INTO GRP_USR ( GRP_NM, USR_NM, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ('stm.SuperUser', 'stm_system',  SYSDATE, 'INIT', NULL, NULL);
