--==========================================================================
-- FILE INFO
--   $Id: Link.dat.sql,v 1.1 2007/11/29 21:13:53 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec AddLink('RETURN_SEGMENT_TO_MTA',LinkTyp('logical'),SubntwkId('Default_return_segment'),SubntwkId('Default_mta'));
exec AddLink('RETURN_SEGMENT_TO_CM',LinkTyp('logical'),SubntwkId('Default_return_segment'),SubntwkId('Default_cm'));
exec AddLink('NETWORK_SERVICE_CLUSTER_TO_MAC_DOMAIN',LinkTyp('logical'),SubntwkId('Default_network_service_cluster'),SubntwkId('Default_docsis_cmts_mac_domain'));
exec AddLink('MAC_DOMAIN_TO_RETURN_SEGMENT',LinkTyp('logical'),SubntwkId('Default_docsis_cmts_mac_domain'),SubntwkId('Default_return_segment'));
exec AddLink('DHCP_SERVER_TO_NETWORK_SERVICE_CLUSTER',LinkTyp('logical'),SubntwkId('Default_dhcp_server'),SubntwkId('Default_network_service_cluster'));
exec AddLink('DNS_SERVER_TO_NETWORK_SERVICE_CLUSTER',LinkTyp('logical'),SubntwkId('Default_dns_server'),SubntwkId('Default_network_service_cluster'));
exec AddLink('TFTP_SERVER_TO_NETWORK_SERVICE_CLUSTER',LinkTyp('logical'),SubntwkId('Default_tftp_server'),SubntwkId('Default_network_service_cluster'));
exec AddLink('TOD_SERVER_TO_NETWORK_SERVICE_CLUSTER',LinkTyp('logical'),SubntwkId('Default_tod_server'),SubntwkId('Default_network_service_cluster'));
exec AddLink('VOICE_MAIL_TO_CMS',LinkTyp('logical'),SubntwkId('Default_vmail'),SubntwkId('Default_class_five_switch'));
exec AddLink('GATEWAY_CONTROLLER_TO_GATEWAY_CONTROLLER_CLUSTER',LinkTyp('logical'),SubntwkId('Default_gateway_controller'),SubntwkId('Default_gateway_controller_cluster'));
exec AddLink('GATEWAY_CONTROLLER_CLUSTER_TO_CMTS',LinkTyp('logical'),SubntwkId('Default_gateway_controller_cluster'),SubntwkId('Default_cmts_docsis'));

