-- FILE INFO
--   $Id: Nep_Info.dat.sql,v 1.1 2007/11/29 21:13:52 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec AddNepInfo(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_pc_voip_access')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'), 'subsvc.svcaddr.ntd_return_segment_nm');

exec AddNepInfo(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_dial_tone_access')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'), 'subsvc.svcaddr.ntd_return_segment_nm');

exec AddNepInfo(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_switch_feature')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'), 'subsvc.svcaddr.ntd_return_segment_nm');

exec AddNepInfo(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_secondary_switch_feature')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'), 'subsvc.svcaddr.ntd_return_segment_nm');

exec AddNepInfo(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_intercept')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'), 'subsvc.svcaddr.ntd_return_segment_nm');