--==========================================================================
-- FILE INFO
--   $Id: Ntwk_Action.dat.sql,v 1.1 2007/11/29 21:13:50 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddNtwkAction('dial.tone.addline', null,NtwkActionTyp('add'), 'n', 21);
exec AddNtwkAction('dial.tone.bind', null,NtwkActionTyp('bind'), 'n', 26);
exec AddNtwkAction('dial.tone.chg_tn', null,NtwkActionTyp('change'), 'n', 30);
exec AddNtwkAction('dial.tone.deleteline', null,NtwkActionTyp('delete'), 'n', 23);
exec AddNtwkAction('dial.tone.swap.add', null,NtwkActionTyp('swap'), 'n', 25);
exec AddNtwkAction('dial.tone.swap.delete', null,NtwkActionTyp('swap'), 'n', 25);
exec AddNtwkAction('dial.tone.unbind', null,NtwkActionTyp('unbind'), 'n', 26);
exec AddNtwkAction('dial.tone.update', null,NtwkActionTyp('change'), 'n', 25);

exec AddNtwkAction('intercept.add', null,NtwkActionTyp('add'), 'n', 40);
exec AddNtwkAction('intercept.delete', null,NtwkActionTyp('delete'), 'n', 45);

exec AddNtwkAction('secondary.features.add', null,NtwkActionTyp('add'), 'n', 50);
exec AddNtwkAction('secondary.features.delete', null,NtwkActionTyp('delete'), 'n', 51);
exec AddNtwkAction('secondary.features.update', null,NtwkActionTyp('change'), 'n', 53);

exec AddNtwkAction('voice.features.add', null,NtwkActionTyp('add'), 'n', 30);
exec AddNtwkAction('voice.features.delete', null,NtwkActionTyp('delete'), 'n', 31);
exec AddNtwkAction('voice.features.update', null,NtwkActionTyp('change'), 'n', 33);

exec AddNtwkAction('voip.mta.add', SubntwkTyp('mta'),NtwkActionTyp('add'), 'y', 18);
exec AddNtwkAction('voip.mta.bind', SubntwkTyp('mta'),NtwkActionTyp('bind'), 'n', 18);
exec AddNtwkAction('voip.mta.chg_cmts', SubntwkTyp('mta'),NtwkActionTyp('change'), 'n', 18);
exec AddNtwkAction('voip.mta.swap.add', SubntwkTyp('mta'),NtwkActionTyp('swap'), 'n', 18);
exec AddNtwkAction('voip.mta.swap.delete', SubntwkTyp('mta'),NtwkActionTyp('swap'), 'n', 18);
exec AddNtwkAction('voip.mta.unbind', SubntwkTyp('mta'),NtwkActionTyp('unbind'), 'n', 18);
exec AddNtwkAction('voip.mta.delete', SubntwkTyp('mta'),NtwkActionTyp('delete'), 'n', 18);
