--==========================================================================
-- FILE INFO
--   $Id: Parm.dat.sql,v 1.1 2007/11/29 21:13:50 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec Add_Parm(p_parm_nm => 'tech_platform_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('tech_platform_typ'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subntwk_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'mgmt_ip_address', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('subntwk_typ'), p_is_required => 'n', p_created_by => 'INIT');

--==========================================================================
--     Project Parmeters
--==========================================================================
exec Add_Parm(p_parm_nm => 'cascade_delete', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'modify_mac_domain', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'associate_link_to_port_pair', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'create_and_associate_link_to_port_pair', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subntwk_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subntwk_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'parent_subntwk_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'scope_subntwk_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subntwk_typ2', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'subntwk_typ3', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'scope_subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'scope_subntwk_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'link_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'from_subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'from_subntwk_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'to_subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'to_subntwk_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'freq_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'freq_grp_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'port_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');


exec Add_Parm(p_parm_nm => 'port_direction', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

---exec Add_Parm(p_parm_nm => 'port_pair_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

-- Obsolete, replaced by ip_block_id
--exec Add_Parm(p_parm_nm => 'block_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_block_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'mask', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

-- Obsolete replaced by ip_sbnt_id 
exec Add_Parm(p_parm_nm => 'subnet_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_sbnt_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_addr', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_sbnt_msk', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

-- obsolet parm
exec Add_Parm(p_parm_nm => 'subnet_status', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_status', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'subnet_ip', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--exec Add_Parm(p_parm_nm => 'subnet_mask', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_max_hosts', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'managing_subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'card_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_address', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'start_ip_addr', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_sbnt_addr', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'ip_address_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'status', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'parent_subntwk_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'parent_subntwk_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
--=============================================================================
-- card command required parameters (not all)
--=============================================================================
-- obsolete, relpaced by 'srl_num'
--exec Add_Parm(p_parm_nm => 'rf_serial_num', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

-- obsolete, relpaced by 'chassis_slot_number'
--exec Add_Parm(p_parm_nm => 'slot_num', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'srl_num', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'chassis_slot_number', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'n', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'card_typ_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--=============================================================================

exec Add_Parm(p_parm_nm => 'rack_num', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'clmi_port', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'oob_port', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'catalystport', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'port_num', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'port_pair_nm', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');


exec Add_Parm(p_parm_nm => 'num_of_cmtss', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'num_of_mac_domain', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'num_of_return_segments', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'num_of_assoc_pps', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

-- should change the name card to cards
exec Add_Parm(p_parm_nm => 'num_of_card', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'num_of_down_ports', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'num_of_up_ports', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'num_of_subnet', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'association_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'link_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'assigned_to', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'upstream_port_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'downstream_port_id', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'num_of_port_pairs', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'link_direction', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'assign_new_parent', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'assign_ip_subnet', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'consumable_resource_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--================
-- Ip Config mac
--================
exec Add_Parm(p_parm_nm => 'is_private_ip', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'is_assign_ip_required', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'is_assign_ip_mgmt_required', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'is_assign_ip_subnet_req', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'associate_to_network_cluster', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'do_assign_management_ip_to_cmts', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'do_assign_management_ip_to_mac_layer', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'do_assign_subnet_to_mac_layer', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'do_assign_ip_scope_to_mac_layer', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

--==========================================================================
exec Add_Parm(p_parm_nm => 'create_card', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'create_down_port', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'create_port_pair', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'mac_subntwk_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'create_up_port', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

---==========================
-- Modify RF Card Data
---==========================
exec Add_Parm(p_parm_nm => 'modify_card', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'modify_downstream_port', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

exec Add_Parm(p_parm_nm => 'modify_upstream_port', p_data_typ_nm => 'Integer',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');

---==========================
exec Add_Parm(p_parm_nm => 'to_subntwk_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
exec Add_Parm(p_parm_nm => 'from_subntwk_typ', p_data_typ_nm => 'String',  p_class_id => Get_Class_Id('stm_project'), p_is_required => 'y', p_created_by => 'INIT');
