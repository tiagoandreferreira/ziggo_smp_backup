--==========================================================================
-- FILE INFO
--   $Id: Parm_Chg_Ntwk_Impact.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
set define off;

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1055,NtwkAction('voip.mta.add'),SvcActionId('smp_pc_voip_access','add'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1057,NtwkAction('voip.mta.swap.add'),SvcActionId('smp_pc_voip_access','swap_mta_add'),NULL,NULL,1,'nzlen subsvc.mta_fqdn',SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1058,NtwkAction('voip.mta.swap.delete'),SvcActionId('smp_pc_voip_access','swap_mta_delete'),NULL,NULL,1,'nzlen subsvc.mta_fqdn',SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1101,NtwkAction('voip.mta.chg_cmts'),SvcActionId('smp_pc_voip_access','chg_cmts'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1060,NtwkAction('voip.mta.bind'),SvcActionId('smp_pc_voip_access','bind_mta'),NULL,NULL,1,'nzlen subsvc.mta_fqdn',SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1061,NtwkAction('voip.mta.unbind'),SvcActionId('smp_pc_voip_access','unbind_mta'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1105,NtwkAction('voip.mta.delete'),SvcActionId('smp_pc_voip_access','delete'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1065,NtwkAction('dial.tone.addline'),SvcActionId('smp_dial_tone_access','add'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1066,NtwkAction('dial.tone.deleteline'),SvcActionId('smp_dial_tone_access','delete'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1069,NtwkAction('dial.tone.update'),SvcActionId('smp_dial_tone_access','change'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1070,NtwkAction('dial.tone.swap.add'),SvcActionId('smp_dial_tone_access','swap_dt_add'),NULL,NULL,1,'nzlen subsvc.mta_fqdn',SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1071,NtwkAction('dial.tone.swap.delete'),SvcActionId('smp_dial_tone_access','swap_dt_delete'),NULL,NULL,1,'nzlen subsvc.mta_fqdn',SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1072,NtwkAction('dial.tone.bind'),SvcActionId('smp_dial_tone_access','bind_dt'),NULL,NULL,1,'nzlen subsvc.mta_fqdn',SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1073,NtwkAction('dial.tone.unbind'),SvcActionId('smp_dial_tone_access','unbind_dt'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1103,NtwkAction('dial.tone.chg_tn'),SvcActionId('smp_dial_tone_access','chg_telephone_number'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1078,NtwkAction('voice.features.add'),SvcActionId('smp_switch_feature','add'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1079,NtwkAction('voice.features.delete'),SvcActionId('smp_switch_feature','delete'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1080,NtwkAction('voice.features.update'),SvcActionId('smp_switch_feature','change'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1150,NtwkAction('intercept.add'),SvcActionId('smp_intercept','add'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1155,NtwkAction('intercept.delete'),SvcActionId('smp_intercept','delete'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1160,NtwkAction('secondary.features.add'),SvcActionId('smp_secondary_switch_feature','add'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1161,NtwkAction('secondary.features.delete'),SvcActionId('smp_secondary_switch_feature','delete'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);

insert into parm_chg_ntwk_impact(PARM_CHG_NTWK_IMPACT_ID,NTWK_ACTION_ID,SVC_ACTION_ID,PARM_ID,SVC_PARM_CHG_TYP,EXECUTION_SEQ,JMF_COND,CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY) values(1162,NtwkAction('secondary.features.update'),SvcActionId('smp_secondary_switch_feature','change'),NULL,NULL,1,NULL,SYSDATE,'INIT',NULL,NULL);


set define on;

