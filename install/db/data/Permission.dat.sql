--==========================================================================
-- FILE INFO
--   $Id: Permission.dat.sql,v 1.1 2007/11/29 21:13:57 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================



--INSERT INTO PERMISSION ( PERMISSION_NM, PERMISSION_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'v', 'view',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO PERMISSION ( PERMISSION_NM, PERMISSION_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'l', 'lock',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO PERMISSION ( PERMISSION_NM, PERMISSION_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'u', 'update',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO PERMISSION ( PERMISSION_NM, PERMISSION_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'e', 'execute',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO PERMISSION ( PERMISSION_NM, PERMISSION_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'ul', 'unlock',  sysdate , 'INIT', NULL, NULL);


