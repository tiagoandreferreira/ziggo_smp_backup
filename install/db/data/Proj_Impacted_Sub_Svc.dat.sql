--==========================================================================
-- FILE INFO
--   $Id: Proj_Impacted_Sub_Svc.dat.sql,v 1.1 2007/11/29 21:13:56 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
alter table PROJ_IMPACTED_SUB_SVC disable constraint R_420;
alter table PROJ_IMPACTED_SUB_SVC disable constraint R_418;

insert into PROJ_IMPACTED_SUB_SVC (PROJ_ID, SUB_SVC_ID, REPROV_STATUS, ERR_MSG, SUBNTWK_INFO, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1001, 100, 'in_progress', NULL, 'reprovisionong of cm', 'albeda', SYSDATE, 'albeda', SYSDATE);
insert into PROJ_IMPACTED_SUB_SVC (PROJ_ID, SUB_SVC_ID, REPROV_STATUS, ERR_MSG, SUBNTWK_INFO, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1001, 102, 'in_progress', NULL, 'reprovisionong of pc', 'albeda', SYSDATE, 'albeda', SYSDATE);

