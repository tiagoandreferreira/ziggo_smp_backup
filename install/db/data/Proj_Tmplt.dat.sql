--==========================================================================
-- FILE INFO
--   $Id: Proj_Tmplt.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddProjTmplt('create_cmts_mac','adding docsis cmts with mac domain');
exec AddProjTmplt('create_cmts_no_mac','adding docsis cmts without mac domain');
exec AddProjTmplt('create_poprouter','create pop router');
exec AddProjTmplt('modify_cmts_mac','modify docsis cmts with mac domain ');
exec AddProjTmplt('modify_cmts_no_mac','modify docsis cmts without mac domain ');
--==========================================================================
-- Mac Layer Project
--==========================================================================
exec AddProjTmplt('create_mac_domain','adding mac_domains to an existing cmts');

--==========================================================================
exec AddProjTmplt('decommission_device','decommission network element');
exec AddProjTmplt('delete_cmts','delete cmts');
exec AddProjTmplt('congestion_relief','congestion relief');

---============================================
-- HFC projects
---============================================
exec AddProjTmplt('create_subntwk','create subntework');
exec AddProjTmplt('modify_subntwk','modify subntework');
exec AddProjTmplt('delete_subntwk','delete subntework');

---============================================
-- IP Managemment
---============================================
exec AddProjTmplt('create_ip_subnet','create ip subnet');
exec AddProjTmplt('decommission_ip_subnet','ip subnet decommision');
exec AddProjTmplt('modify_ip_subnet','ip subnet modification');
exec AddProjTmplt('delete_ip_subnet','ip subnet deletion');
exec AddProjTmplt('assign_ip_subnet','ip subnet assignment');


---============================================
-- IP Network Elements
---============================================
exec AddProjTmplt('create_ip_sub_network_element','create service network element');
exec AddProjTmplt('create_network_cluster','create network cluster');
exec AddProjTmplt('modify_ip_sub_network_element','Modify IP Service Network Element');
exec AddProjTmplt('modify_network_cluster','Modify Network Cluster');
exec AddProjTmplt('delete_ip_service_ne','Delete IP Service Network Element');
exec AddProjTmplt('delete_network_cluster','Delete Network Cluster');


---============================================
-- RelationShip Management
---============================================
exec AddProjTmplt('add_link','Add Link');
exec AddProjTmplt('remove_link','Remove Link');
exec AddProjTmplt('swap_link','Swap Link');

---============================================
-- Component projects
---============================================
exec AddProjTmplt('lookup_card','Lookup Card');
exec AddProjTmplt('lookup_port','Lookup Port');
exec AddProjTmplt('create_rf_card','Create RF Card With Port Pair ');
exec AddProjTmplt('modify_rf_card','Modify RF Card');
