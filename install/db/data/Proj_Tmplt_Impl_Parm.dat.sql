--==========================================================================
-- FILE INFO
--   $Id: Proj_Tmplt_Impl_Parm.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
--==========================================================================
--	decommission_device project(implementation) is to be deleted
--==========================================================================
--exec AddProjTmpltImplParm('decommission_device', ProjTmpltParmId('decommission_device', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

--exec AddProjTmpltImplParm('decommission_device', ProjTmpltParmId('decommission_device', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');

--==========================================================================
--	decommission_docsis_cmts
--==========================================================================
exec AddProjTmpltImplParm('decommission_docsis_cmts', ProjTmpltParmId('decommission_device', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('decommission_docsis_cmts', ProjTmpltParmId('decommission_device', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');

--==========================================================================
--	decommission_terayon_gateway
--==========================================================================
exec AddProjTmpltImplParm('decommission_terayon_gateway', ProjTmpltParmId('decommission_device', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('decommission_terayon_gateway', ProjTmpltParmId('decommission_device', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_terayon_gateway');

--==========================================================================
-- create_docsis_cmts
--==========================================================================
exec AddProjTmpltImplParm('create_docsis_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
exec AddProjTmpltImplParm('create_docsis_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');
exec AddProjTmpltImplParm('create_docsis_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('create_docsis_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');
exec AddProjTmpltImplParm('create_docsis_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ2', Get_Class_Id('stm_project'))), 'docsis_cmts_mac_domain');
exec AddProjTmpltImplParm('create_docsis_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ3', Get_Class_Id('stm_project'))), 'return_segment');
--==========================================================================
-- modify_docsis_cmts
--==========================================================================
exec AddProjTmpltImplParm('modify_docsis_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

exec AddProjTmpltImplParm('modify_docsis_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('modify_docsis_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');

exec AddProjTmpltImplParm('modify_docsis_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ2', Get_Class_Id('stm_project'))), 'docsis_cmts_mac_domain');

exec AddProjTmpltImplParm('modify_docsis_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ3', Get_Class_Id('stm_project'))), 'return_segment');

--==========================================================================
-- modify_terayon_cmts
--==========================================================================
exec AddProjTmpltImplParm('modify_terayon_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
exec AddProjTmpltImplParm('modify_terayon_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('modify_terayon_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_terayon_gateway');
exec AddProjTmpltImplParm('modify_terayon_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ2', Get_Class_Id('stm_project'))), 'teralink');
exec AddProjTmpltImplParm('modify_terayon_cmts', ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ3', Get_Class_Id('stm_project'))), 'return_segment');
--==========================================================================
-- create_terayon_cmts
--==========================================================================

exec AddProjTmpltImplParm('create_terayon_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
exec AddProjTmpltImplParm('create_terayon_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');
exec AddProjTmpltImplParm('create_terayon_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('create_terayon_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_terayon_gateway');
exec AddProjTmpltImplParm('create_terayon_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ2', Get_Class_Id('stm_project'))), 'teralink');
exec AddProjTmpltImplParm('create_terayon_cmts', ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ3', Get_Class_Id('stm_project'))), 'return_segment');

--==========================================================================
-- create_docsis_mac_domain
--==========================================================================
exec AddProjTmpltImplParm('create_docsis_mac_domain', ProjTmpltParmId('create_mac_domain', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

exec AddProjTmpltImplParm('create_docsis_mac_domain', ProjTmpltParmId('create_mac_domain', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('create_docsis_mac_domain', ProjTmpltParmId('create_mac_domain', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project'))), 'docsis_cmts_mac_domain');

exec AddProjTmpltImplParm('create_docsis_mac_domain', ProjTmpltParmId('create_mac_domain', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');

--==========================================================================
-- create_terayon_teralink
--==========================================================================
exec AddProjTmpltImplParm('create_terayon_teralink', ProjTmpltParmId('create_mac_domain', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

exec AddProjTmpltImplParm('create_terayon_teralink', ProjTmpltParmId('create_mac_domain', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('create_terayon_teralink', ProjTmpltParmId('create_mac_domain', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project'))), 'teralink');

exec AddProjTmpltImplParm('create_terayon_teralink', ProjTmpltParmId('create_mac_domain', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_terayon_gateway');

--==========================================================================
exec AddProjTmpltImplParm('create return segment', ProjTmpltParmId('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'forward_segment');
exec AddProjTmpltImplParm('create return segment', ProjTmpltParmId('create_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'return_segment');

exec AddProjTmpltImplParm('create return segment', ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('create return segment', ProjTmpltParmId('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================

exec AddProjTmpltImplParm('modify return segment', ProjTmpltParmId('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'forward_segment');
exec AddProjTmpltImplParm('modify return segment', ProjTmpltParmId('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'return_segment');

exec AddProjTmpltImplParm('modify return segment', ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('modify return segment', ProjTmpltParmId('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================

exec AddProjTmpltImplParm('delete return segment', ProjTmpltParmId('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'return_segment');

exec AddProjTmpltImplParm('delete return segment', ProjTmpltParmId('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

exec AddProjTmpltImplParm('delete return segment', ProjTmpltParmId('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

--==========================================================================
exec AddProjTmpltImplParm('Congestion Relief', ProjTmpltParmId('congestion_relief', STMParmId('to_subntwk_typ', Get_Class_Id('stm_project'))), 'return_segment');
exec AddProjTmpltImplParm('Congestion Relief', ProjTmpltParmId('congestion_relief', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('Congestion Relief', ProjTmpltParmId('congestion_relief', STMParmId('link_direction', Get_Class_Id('stm_project'))), 'served_by');
exec AddProjTmpltImplParm('Congestion Relief', ProjTmpltParmId('congestion_relief', STMParmId('link_typ', Get_Class_Id('stm_project'))), 'hfc_link');
exec AddProjTmpltImplParm('Congestion Relief', ProjTmpltParmId('congestion_relief', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'FTFRebuildCongestionRelief');
--==========================================================================

exec AddProjTmpltImplParm('create forward segment', ProjTmpltParmId('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'phub');

exec AddProjTmpltImplParm('create forward segment', ProjTmpltParmId('create_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'forward_segment');

exec AddProjTmpltImplParm('create forward segment', ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('create forward segment', ProjTmpltParmId('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
--==========================================================================


exec AddProjTmpltImplParm('modify forward segment', ProjTmpltParmId('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'phub');

exec AddProjTmpltImplParm('modify forward segment', ProjTmpltParmId('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'forward_segment');

exec AddProjTmpltImplParm('modify forward segment', ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('modify forward segment', ProjTmpltParmId('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
--==========================================================================

exec AddProjTmpltImplParm('delete forward segment', ProjTmpltParmId('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'forward_segment');

exec AddProjTmpltImplParm('delete forward segment', ProjTmpltParmId('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('delete forward segment', ProjTmpltParmId('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
--==========================================================================

exec AddProjTmpltImplParm('create headend', ProjTmpltParmId('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('create headend', ProjTmpltParmId('create_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('create headend', ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('create headend', ProjTmpltParmId('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================

exec AddProjTmpltImplParm('modify headend', ProjTmpltParmId('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');
exec AddProjTmpltImplParm('modify headend', ProjTmpltParmId('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('modify headend', ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('modify headend', ProjTmpltParmId('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
--==========================================================================

exec AddProjTmpltImplParm('delete headend', ProjTmpltParmId('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('delete headend', ProjTmpltParmId('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('delete headend', ProjTmpltParmId('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================

exec AddProjTmpltImplParm('create service area', ProjTmpltParmId('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'return_segment');
exec AddProjTmpltImplParm('create service area', ProjTmpltParmId('create_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'serviced_area');

exec AddProjTmpltImplParm('create service area', ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('create service area', ProjTmpltParmId('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================

exec AddProjTmpltImplParm('modify service area', ProjTmpltParmId('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'return_segment');
exec AddProjTmpltImplParm('modify service area', ProjTmpltParmId('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'serviced_area');

exec AddProjTmpltImplParm('modify service area', ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('modify service area', ProjTmpltParmId('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================
exec AddProjTmpltImplParm('delete service area', ProjTmpltParmId('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'serviced_area');

exec AddProjTmpltImplParm('delete service area', ProjTmpltParmId('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('delete service area', ProjTmpltParmId('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================
exec AddProjTmpltImplParm('create phub', ProjTmpltParmId('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('create phub', ProjTmpltParmId('create_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'phub');

exec AddProjTmpltImplParm('create phub', ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('create phub', ProjTmpltParmId('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================
exec AddProjTmpltImplParm('modify phub', ProjTmpltParmId('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('modify phub', ProjTmpltParmId('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'phub');

exec AddProjTmpltImplParm('modify phub', ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('modify phub', ProjTmpltParmId('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');
--==========================================================================

exec AddProjTmpltImplParm('delete phub', ProjTmpltParmId('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'phub');

exec AddProjTmpltImplParm('delete phub', ProjTmpltParmId('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'root_network');

exec AddProjTmpltImplParm('delete phub', ProjTmpltParmId('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================
exec AddProjTmpltImplParm('Add DOCSIS RF Cards with Ports', ProjTmpltParmId('create_rf_card', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');
exec AddProjTmpltImplParm('Add DOCSIS RF Cards with Ports', ProjTmpltParmId('create_rf_card', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project'))), 'docsis_cmts_mac_domain');

exec AddProjTmpltImplParm('Add DOCSIS RF Cards with Ports', ProjTmpltParmId('create_rf_card', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');

exec AddProjTmpltImplParm('Add DOCSIS RF Cards with Ports', ProjTmpltParmId('create_rf_card', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================
exec AddProjTmpltImplParm('Add Terayon RF Cards with Port', ProjTmpltParmId('create_rf_card', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'headend');
exec AddProjTmpltImplParm('Add Terayon RF Cards with Port', ProjTmpltParmId('create_rf_card', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project'))), 'teralink');

exec AddProjTmpltImplParm('Add Terayon RF Cards with Port', ProjTmpltParmId('create_rf_card', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_terayon_gateway');

exec AddProjTmpltImplParm('Add Terayon RF Cards with Port', ProjTmpltParmId('create_rf_card', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

--==========================================================================
exec AddProjTmpltImplParm('Modify DOCSIS RF Cards', ProjTmpltParmId('modify_rf_card', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project'))), 'docsis_cmts_mac_domain');

exec AddProjTmpltImplParm('Modify DOCSIS RF Cards', ProjTmpltParmId('modify_rf_card', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_docsis');

exec AddProjTmpltImplParm('Modify DOCSIS RF Cards', ProjTmpltParmId('modify_rf_card', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');


exec AddProjTmpltImplParm('Modify Terayon RF Cards', ProjTmpltParmId('modify_rf_card', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project'))), 'teralink');

exec AddProjTmpltImplParm('Modify Terayon RF Cards', ProjTmpltParmId('modify_rf_card', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), 'cmts_terayon_gateway');

exec AddProjTmpltImplParm('Modify Terayon RF Cards', ProjTmpltParmId('modify_rf_card', STMParmId('assigned_to', Get_Class_Id('stm_project'))), 'Outside Plant Data');

