--==========================================================================
-- FILE INFO
--   $Id: Proj_Tmplt_Parm.dat.sql,v 1.1 2007/11/29 21:13:50 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
--==========================================================================
-- create_subntwk 
--==========================================================================
exec AddProjTmpltParm('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- modify_subntwk 
--==========================================================================
exec AddProjTmpltParm('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- delete_subntwk 
--==========================================================================
exec AddProjTmpltParm('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- congestion_relief 
--==========================================================================
exec AddProjTmpltParm('congestion_relief', STMParmId('to_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('congestion_relief', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('congestion_relief', STMParmId('link_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('congestion_relief', STMParmId('link_direction', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('congestion_relief', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- create_cmts_mac
--==========================================================================
exec AddProjTmpltParm('create_cmts_mac', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_cmts_mac', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_cmts_mac', STMParmId('subntwk_typ2', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_cmts_mac', STMParmId('subntwk_typ3', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_cmts_mac', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- modify_cmts_mac
--==========================================================================
exec AddProjTmpltParm('modify_cmts_mac', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_cmts_mac', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_cmts_mac', STMParmId('subntwk_typ2', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_cmts_mac', STMParmId('subntwk_typ3', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- create_mac_domain
--==========================================================================
exec AddProjTmpltParm('create_mac_domain', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_mac_domain', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_mac_domain', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_mac_domain', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
-- decommission_device 
--==========================================================================
exec AddProjTmpltParm('decommission_device', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('decommission_device', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));

--==========================================================================
-- create_rf_card
--==========================================================================
exec AddProjTmpltParm('create_rf_card', STMParmId('subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_rf_card', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_rf_card', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('create_rf_card', STMParmId('assigned_to', Get_Class_Id('stm_project')));

--==========================================================================
exec AddProjTmpltParm('modify_rf_card', STMParmId('mac_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_rf_card', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project')));
exec AddProjTmpltParm('modify_rf_card', STMParmId('assigned_to', Get_Class_Id('stm_project')));
