--==========================================================================
-- FILE INFO
--   $Id: Proj_Tmplt_impl.dat.sql,v 1.1 2007/11/29 21:13:59 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddProjTmpltImpl('create_docsis_cmts','adding docsis cmts ','create_cmts_mac');

exec AddProjTmpltImpl('create_terayon_cmts','adding terayon cmts ','create_cmts_mac');
--==========================================================================
exec AddProjTmpltImpl('create_docsis_mac_domain','adding docsis mac_domain','create_mac_domain');
exec AddProjTmpltImpl('create_terayon_teralink','adding terayon teralink','create_mac_domain');
--==========================================================================

--exec AddProjTmpltImpl('create_motorola_cable_router','adding motorola cmts ','create_cmts_no_mac');
--exec AddProjTmpltImpl('create_lancity_lce','adding motorola cmts ','create_cmts_no_mac');
--exec AddProjTmpltImpl('create_poprouter','create pop router','create_poprouter');
--==========================================================================

exec AddProjTmpltImpl('modify_docsis_cmts','modify docsis cmts ', 'modify_cmts_mac');

exec AddProjTmpltImpl('modify_terayon_cmts','modify docsis cmts ', 'modify_cmts_mac');
--==========================================================================
--exec AddProjTmpltImpl('modify_lancity_lce','modify  cmts  ','modify_cmts_no_mac');
--exec AddProjTmpltImpl('modify_motorola_cable_router','modify  cmts  ','modify_cmts_no_mac');

--==========================================================================
-- To Be Deleted
--exec AddProjTmpltImpl('decommission_device','decommission network element','decommission_device');

--==========================================================================
exec AddProjTmpltImpl('decommission_docsis_cmts','decommission network element','decommission_device');

exec AddProjTmpltImpl('decommission_terayon_gateway','decommission network element','decommission_device');

--==========================================================================
exec AddProjTmpltImpl('Congestion Relief','congestion relief','congestion_relief');


---============================================
-- HFC projects
---============================================
exec AddProjTmpltImpl('create return segment','create subnetwork','create_subntwk');
exec AddProjTmpltImpl('modify return segment','modify subnetwork','modify_subntwk');
exec AddProjTmpltImpl('delete return segment','delete subnetwork','delete_subntwk');

exec AddProjTmpltImpl('create forward segment','create subnetwork','create_subntwk');
exec AddProjTmpltImpl('modify forward segment','modify subnetwork','modify_subntwk');
exec AddProjTmpltImpl('delete forward segment','delete subnetwork','delete_subntwk');

exec AddProjTmpltImpl('create service area','create subnetwork','create_subntwk');
exec AddProjTmpltImpl('modify service area','modify subnetwork','modify_subntwk');
exec AddProjTmpltImpl('delete service area','delete subnetwork','delete_subntwk');

exec AddProjTmpltImpl('create phub','create subnetwork','create_subntwk');
exec AddProjTmpltImpl('modify phub','modify subnetwork','modify_subntwk');
exec AddProjTmpltImpl('delete phub','delete subnetwork','delete_subntwk');


exec AddProjTmpltImpl('create headend','create subnetwork','create_subntwk');
exec AddProjTmpltImpl('modify headend','modify subnetwork','modify_subntwk');
exec AddProjTmpltImpl('delete headend','delete subnetwork','delete_subntwk');

---============================================
-- IP Managemment
---============================================
exec AddProjTmpltImpl('create_ip_subnet','create ip subnet','create_ip_subnet');
exec AddProjTmpltImpl('decommission_ip_subnet','ip subnet decommision','decommission_ip_subnet');
exec AddProjTmpltImpl('modify_ip_subnet','ip subnet modification','modify_ip_subnet');
exec AddProjTmpltImpl('delete_ip_subnet','ip subnet deletion','delete_ip_subnet');
exec AddProjTmpltImpl('assign_ip_subnet','ip subnet assignment','assign_ip_subnet');


---============================================
-- IP Network Elements
---============================================
---exec AddProjTmpltImpl('create_ip_sub_network_element','create service network element','create_ip_sub_network_element');
---exec AddProjTmpltImpl('create_network_cluster','create network cluster','create_network_cluster');
---exec AddProjTmpltImpl('modify_ip_sub_network_element','Modify IP Service Network Element','modify_ip_sub_network_element');
---exec AddProjTmpltImpl('modify_network_cluster','Modify Network Cluster','modify_network_cluster');
---exec AddProjTmpltImpl('delete_ip_service_ne','Delete IP Service Network Element','delete_ip_service_ne');
exec AddProjTmpltImpl('delete_network_cluster','Delete Network Cluster','delete_network_cluster');


---============================================
-- RelationShip Management
---============================================
---exec AddProjTmpltImpl('add_link','Add Link','add_link');
---exec AddProjTmpltImpl('remove_link','Remove Link','remove_link');
---exec AddProjTmpltImpl('swap_link','Swap Link','swap_link');

---============================================
-- Card/Port projects
---============================================
exec AddProjTmpltImpl('Add DOCSIS RF Cards with Ports','Add Docsis RF cards','create_rf_card');

exec AddProjTmpltImpl('Add Terayon RF Cards with Port','Add Terayon RF cards','create_rf_card');

---============================================
-- Card/Port projects
---============================================
exec AddProjTmpltImpl('Modify DOCSIS RF Cards','Modify Docsis RF cards','modify_rf_card');
exec AddProjTmpltImpl('Modify Terayon RF Cards','Modify Terayon RF cards','modify_rf_card');
