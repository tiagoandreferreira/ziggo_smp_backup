--==========================================================================
-- FILE INFO
--   $Id: Ref_Card_Typ.dat.sql,v 1.1 2007/11/29 21:13:53 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
insert into ref_card_typ(CARD_TYP_ID, CARD_TYP_NM, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values(1, 'rf_card', SYSDATE, 'Initial', NULL, NULL);
