--==========================================================================
-- FILE INFO
--   $Id: Ref_Communication_Directn.dat.sql,v 1.1 2007/11/29 21:13:58 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
insert into ref_communication_directn (COMM_DIRECTION_ID, COMM_DIRECTION_NM, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values(1, 'upstream', SYSDATE, 'Initial', NULL, NULL);
insert into ref_communication_directn (COMM_DIRECTION_ID, COMM_DIRECTION_NM, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values(2, 'downstream', SYSDATE, 'Initial', NULL, NULL);
