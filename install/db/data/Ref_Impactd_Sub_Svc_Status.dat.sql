--==========================================================================
-- FILE INFO
--   $Id: Ref_Impactd_Sub_Svc_Status.dat.sql,v 1.1 2007/11/29 21:13:56 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
insert into ref_reprov_status (REPROV_STATUS, IMPACTED_SUB_SVC_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('initial', 'Intial Status. Has not been sent to SPM', SYSDATE, 'Initial', NULL, NULL);

insert into ref_reprov_status (REPROV_STATUS, IMPACTED_SUB_SVC_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('failed', 'Provisioning failed', SYSDATE, 'Initial', NULL, NULL);

insert into ref_reprov_status (REPROV_STATUS, IMPACTED_SUB_SVC_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('completed', 'Provisioning succesful', SYSDATE, 'Initial', NULL, NULL);

insert into ref_reprov_status (REPROV_STATUS, IMPACTED_SUB_SVC_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('in_progress', 'Provisioning in progress', SYSDATE, 'Initial', NULL, NULL);
