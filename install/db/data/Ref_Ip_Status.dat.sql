--==========================================================================
-- FILE INFO
--   $Id: Ref_Ip_Status.dat.sql,v 1.1 2007/11/29 21:13:51 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'free');

insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'reserved');

insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'allocated');

-- Be careful with this status, it is not the business view. Temperary
insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'pending_delete');
