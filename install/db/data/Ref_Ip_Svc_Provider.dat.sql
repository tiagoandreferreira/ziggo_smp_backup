--==========================================================================
-- FILE INFO
--   $Id: Ref_Ip_Svc_Provider.dat.sql,v 1.1 2007/11/29 21:13:57 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
insert into ref_ip_svc_provider( IP_SVC_PROVIDER_ID, IP_SVC_PROVIDER_NM, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY) VALUES ( 1, 'Default', SYSDATE,'INIT', NULL,NULL);
