--==========================================================================
-- FILE INFO
--   $Id: Ref_Ntwk_Action_Typ.dat.sql,v 1.1 2007/11/29 21:13:56 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddRefNtwkActionTyp('add');
exec AddRefNtwkActionTyp('delete');

exec AddRefNtwkActionTyp('change');
exec AddRefNtwkActionTyp('bind');
exec AddRefNtwkActionTyp('unbind');
exec AddRefNtwkActionTyp('swap');
exec AddRefNtwkActionTyp('reprovision');

exec AddRefNtwkActionTyp('suspend');
exec AddRefNtwkActionTyp('resume');
