--==========================================================================
-- FILE INFO
--   $Id: Ref_Ntwk_Cmpnt_typ.dat.sql,v 1.1 2007/11/29 21:13:55 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddRefNtwkCmpntTyp('subntwk','subnetwork');
exec AddRefNtwkCmpntTyp('assoc','association');
exec AddRefNtwkCmpntTyp('card','Card');
exec AddRefNtwkCmpntTyp('port','port');
exec AddRefNtwkCmpntTyp('port_pair','port pair');
exec AddRefNtwkCmpntTyp('link','link');
exec AddRefNtwkCmpntTyp('freq','frequency ');
exec AddRefNtwkCmpntTyp('freq_grp','frequency group ');
exec AddRefNtwkCmpntTyp('tech_platform','technology platform ');
exec AddRefNtwkCmpntTyp('consumable_rsrc','consumbable resource');
