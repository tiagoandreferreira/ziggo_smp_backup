--==========================================================================
-- FILE INFO
--   $Id: Ref_Ntwk_Role.dat.sql,v 1.1 2007/11/29 21:13:56 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddRefNtwkRole('service_distribution_point');
exec AddRefNtwkRole('network_entry_point');
exec AddRefNtwkRole('gateway_controller');
exec AddRefNtwkRole('docsis_cmts_mac_domain');
exec AddRefNtwkRole('ip_network_cluster_mgr');
exec AddRefNtwkRole('cmts_docsis');
exec AddRefNtwkRole('call_switch_platform');
exec AddRefNtwkRole('cpe');
exec AddRefNtwkRole('primary_dns_server');
