--==========================================================================
-- FILE INFO
--   $Id: Ref_Ntwk_Typ.dat.sql,v 1.1 2007/11/29 21:13:51 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddRefNtwkTyp('hfc');
exec AddRefNtwkTyp('ip');
exec AddRefNtwkTyp('logical');
exec AddRefNtwkTyp('cpe');
