--==========================================================================
-- FILE INFO
--   $Id: Ref_Parm_Direction.dat.sql,v 1.1 2007/11/29 21:13:58 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
Insert into Ref_Parm_Direction(parm_direction_id, descr, created_dtm, created_by, modified_dtm, modified_by) Values ('i','in', SYSDATE, 'INIT', SYSDATE, 'INIT');
Insert into Ref_Parm_Direction(parm_direction_id, descr, created_dtm, created_by, modified_dtm, modified_by) Values ('o','out', SYSDATE, 'INIT', SYSDATE, 'INIT');
Insert into Ref_Parm_Direction(parm_direction_id, descr, created_dtm, created_by, modified_dtm, modified_by) Values ('b','both in and out', SYSDATE, 'INIT', SYSDATE, 'INIT');
