--==========================================================================
-- FILE INFO
--   $Id: Ref_Screen.dat.sql,v 1.1 2007/11/29 21:13:52 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddRefScreen('lookup network element');
exec AddRefScreen('lookup scope network element');

exec AddRefScreen('create network element');
exec AddRefScreen('assign parent to network element');
exec AddRefScreen('create card');
exec AddRefScreen('assign card to network element');

exec AddRefScreen('create port');
exec AddRefScreen('assign port to card');

exec AddRefScreen('lookup frequency group');
exec AddRefScreen('lookup frequency');
exec AddRefScreen('assign frequency group to port');
exec AddRefScreen('assign frequency to port');

exec AddRefScreen('create port pair');

exec AddRefScreen('lookup port');
exec AddRefScreen('lookup card');

exec AddRefScreen('create network link');
exec AddRefScreen('lookup port pair');
exec AddRefScreen('assign port pair to link');

exec AddRefScreen('lookup link');
exec AddRefScreen('delete link');
exec AddRefScreen('create link');
--======================================================
exec AddRefScreen('create ip_subnet');
exec AddRefScreen('lookup ip_block');
exec AddRefScreen('lookup ip_subnet');
exec AddRefScreen('release ip_subnet');
exec AddRefScreen('update ip_subnet status');
exec AddRefScreen('lookup ip');
exec AddRefScreen('assign mgmt ip');
exec AddRefScreen('assign subnet');
exec AddRefScreen('assign an IP subnet to a network device');
--======================================================
exec AddRefScreen('update network element properties');
exec AddRefScreen('delete network element');
--======================================================
exec AddRefScreen('Specify number of associated port pairs');
exec AddRefScreen('Specify number of return segments');
exec AddRefScreen('Specify number of Mac_domain');
exec AddRefScreen('Specify number of Port_pair');
exec AddRefScreen('Specify number of Card');
--======================================================
exec AddRefScreen('cascade delete');
--======================================================
exec AddRefScreen('branch');

-- we need to reconsider it
exec AddRefScreen('branch_2');
exec AddRefScreen('branch_3');

--======================================================
-- Added By Albeda
--======================================================
exec AddRefScreen('Lookup MAC Domain Layer');
exec AddRefScreen('Assign Managment Ip to MAC Domain Layer');
exec AddRefScreen('Assign Ip Subnet to MAC Domain Layer');

exec AddRefScreen('Link MAC domain to a network cluster');
exec AddRefScreen('Lookup Network Cluster');

--==========================================================================
exec AddRefScreen('Create Downstream Port');
exec AddRefScreen('Lookup DownStream Port');
exec AddRefScreen('Lookup Card From Where DownStream Port Will be Selected');
exec AddRefScreen('Lookup Card From Where UpStream Port Will be Selected');
exec AddRefScreen('Create Upstream Port');

--===========================
-- Modify RF Card Data
--===========================
exec AddRefScreen('Update Card Properties');
exec AddRefScreen('Modify Downstream Port');
exec AddRefScreen('Modify Upstream Port');

--===========================
-- Create CMTS Project
--===========================
exec AddRefScreen('Please Select A Frequency For Downstream Port');
exec AddRefScreen('Please Select A Frequency Group For Upstream Port');
exec AddRefScreen('Please Select The Card Used For Downstream Port');
exec AddRefScreen('Please Select The Downstream Port');
exec AddRefScreen('Please Select The Card Used for Upstream Port');
exec AddRefScreen('Please Select The Upstream Port');
exec AddRefScreen('Please Select A Port Pair Serving This Link');

--===========================
-- IP Config for CMTS with Mac Domain
--===========================
exec AddRefScreen('Please Select Subnet of the Managment IP for CMTS Layer');
exec AddRefScreen('Please Select the Management Ip');
exec AddRefScreen('Please Select Subnet of Management IP for MAC Domain Layer');
exec AddRefScreen('Please Select Management IP for MAC Domain Layer');
exec AddRefScreen('Please Select The IP Subnet To Be Assigned');
