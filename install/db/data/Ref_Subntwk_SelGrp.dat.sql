--==========================================================================
-- FILE INFO
--   $Id: Ref_Subntwk_SelGrp.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddRefSubntwkSelGrp('mac interface', SelectionAlgorithm('exclusive'));
exec AddRefSubntwkSelGrp('cmts level', SelectionAlgorithm('exclusive'));

