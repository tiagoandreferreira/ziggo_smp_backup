--==========================================================================
-- FILE INFO
--   $Id: Ref_Topology_Action_Typ.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddRefTopologyActionTyp('lookup','lookup');
exec AddRefTopologyActionTyp('create','create');
exec AddRefTopologyActionTyp('delete','delete');
exec AddRefTopologyActionTyp('update','update');
exec AddRefTopologyActionTyp('assign','assign');
exec AddRefTopologyActionTyp('unassign','unassign');
--exec AddRefTopologyActionTyp('dummy','position action, non-business requirements');
