--==========================================================================
-- FILE INFO
--   $Id: Ref_proj_status.dat.sql,v 1.1 2007/11/29 21:13:51 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the sigma implementation spec
--==========================================================================
exec  AddRefProjStatus('open','open');
exec  AddRefProjStatus('locked','locked ');
exec  AddRefProjStatus('staged','staged ');
exec  AddRefProjStatus('successful','successful ');
exec  AddRefProjStatus('aborted','aborted ');
exec  AddRefProjStatus('failed','failed ');
exec  AddRefProjStatus('init','intial ');
exec  AddRefProjStatus('provisioning','provisioning in progress ');
exec  AddRefProjStatus('synchronizing','external synchronization');
exec  AddRefProjStatus('synch_failed','external synchronization failed');
