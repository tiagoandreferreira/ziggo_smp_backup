--==========================================================================
-- FILE INFO
--   $Id: Ref_task_status.dat.sql,v 1.1 2007/11/29 21:13:52 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec  AddRefTaskStatus('available','available');
exec  AddRefTaskStatus('completed','completed ');
exec  AddRefTaskStatus('init','initial');
