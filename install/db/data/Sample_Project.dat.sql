--==========================================================================
-- FILE INFO
--   $Id: Sample_Project.dat.sql,v 1.1 2007/11/29 21:13:56 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
insert into proj (PROJ_ID, PROJ_TMPLT_NM, COMPLETION_DT, EFFECTIVE_DT, PROJ_STATUS_NM, PROJ_NM, PROJ_DESCR, START_DT, EXT_PROJ_KEY, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1001, 'congestion_relief', SYSDATE + 45 , SYSDATE + 90 , 'open', 'Congestion relief for SN0001', 'Sample congestion relief project 1', SYSDATE, 'ext_key_1001', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into task (PROJ_ID, TASK_ID, TASK_NM, COMPLETION_DT, ASSIGNED_TO, TASK_STATUS_NM, TASK_TMPLT_NM, TASK_SEQ, START_DT, TASK_DESCR, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values(1001, 10010, 'congestion relief', SYSDATE + 45, 'nela', 'available', 'congestion_relief', 0, SYSDATE, 'This is sample congestion relief', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into proj (PROJ_ID, PROJ_TMPLT_NM, COMPLETION_DT, EFFECTIVE_DT, PROJ_STATUS_NM, PROJ_NM, PROJ_DESCR, START_DT, EXT_PROJ_KEY, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1002, 'congestion_relief', SYSDATE + 45 , SYSDATE + 90 , 'locked', 'Congestion relief for SN1002', 'Sample congestion relief project 1', SYSDATE, 'ext_key_1002', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into task (PROJ_ID, TASK_ID, TASK_NM, COMPLETION_DT, ASSIGNED_TO, TASK_STATUS_NM, TASK_TMPLT_NM, TASK_SEQ, START_DT, TASK_DESCR, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values(1002, 10020, 'congestion relief - 10020', SYSDATE + 45, 'nela', 'available', 'congestion_relief', 0, SYSDATE, 'This is sample congestion relief', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);


insert into proj (PROJ_ID, PROJ_TMPLT_NM, COMPLETION_DT, EFFECTIVE_DT, PROJ_STATUS_NM, PROJ_NM, PROJ_DESCR, START_DT, EXT_PROJ_KEY, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1003, 'congestion_relief', SYSDATE + 45 , SYSDATE + 90 , 'successful', 'Congestion relief for SN1003', 'Sample congestion relief project 1', SYSDATE, 'ext_key_1003', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into task (PROJ_ID, TASK_ID, TASK_NM, COMPLETION_DT, ASSIGNED_TO, TASK_STATUS_NM, TASK_TMPLT_NM, TASK_SEQ, START_DT, TASK_DESCR, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values(1003, 10030, 'congestion relief-1003', SYSDATE + 45, 'nela', 'available', 'congestion_relief', 0, SYSDATE, 'This is sample congestion relief', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into proj (PROJ_ID, PROJ_TMPLT_NM, COMPLETION_DT, EFFECTIVE_DT, PROJ_STATUS_NM, PROJ_NM, PROJ_DESCR, START_DT, EXT_PROJ_KEY, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1004, 'congestion_relief', SYSDATE + 45 , SYSDATE + 90 , 'aborted', 'Congestion relief for SN1004', 'Sample congestion relief project 1', SYSDATE, 'ext_key_1004', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into task (PROJ_ID, TASK_ID, TASK_NM, COMPLETION_DT, ASSIGNED_TO, TASK_STATUS_NM, TASK_TMPLT_NM, TASK_SEQ, START_DT, TASK_DESCR, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values(1004, 10040, 'congestion relief-1004', SYSDATE + 45, 'nela', 'available', 'congestion_relief', 0, SYSDATE, 'This is sample congestion relief', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into proj (PROJ_ID, PROJ_TMPLT_NM, COMPLETION_DT, EFFECTIVE_DT, PROJ_STATUS_NM, PROJ_NM, PROJ_DESCR, START_DT, EXT_PROJ_KEY, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values (1005, 'congestion_relief', SYSDATE + 45 , SYSDATE + 90 , 'failed', 'Congestion relief for SN1005', 'Sample congestion relief project 1', SYSDATE, 'ext_key_1005', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

insert into task (PROJ_ID, TASK_ID, TASK_NM, COMPLETION_DT, ASSIGNED_TO, TASK_STATUS_NM, TASK_TMPLT_NM, TASK_SEQ, START_DT, TASK_DESCR, LOCKED_BY, CREATED_BY, CREATED_DTM, MODIFIED_BY, MODIFIED_DTM) values(1005, 10050, 'congestion relief - 1005', SYSDATE + 45, 'nela', 'available', 'congestion_relief', 0, SYSDATE, 'This is sample congestion relief', NULL, 'albeda', SYSDATE, 'albeda', SYSDATE);

