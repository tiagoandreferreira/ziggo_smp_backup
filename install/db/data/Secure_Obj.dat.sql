--==========================================================================
-- FILE INFO
--   $Id: Secure_Obj.dat.sql,v 1.1 2007/11/29 21:13:53 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================

--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM', 'STM',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.create_subntwk', 'STM.create_subntwk',  sysdate , 'INIT', NULL, NULL);

--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.modify_subntwk', 'STM.modify_subntwk',  sysdate , 'INIT', NULL, NULL);

--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.delete_subntwk', 'STM.delete_subntwk',  sysdate , 'INIT', NULL, NULL);

--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.congestion_relief.Congestion Relief', 'STM.congestion_relief.congestion_relief',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.congestion_relief.Congestion Relief.congestion_relief', 'STM.congestion_relief.congestion_relief.congestion_relief',  sysdate , 'INIT', NULL, NULL);
--INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.congestion_relief.Congestion Relief.verification', 'STM.congestion_relief.congestion_relief.verification',  sysdate , 'INIT', NULL, NULL);

INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.subntwk.root_network', 'GENERIC_PROJECT.subntwk.root_network',  sysdate , 'INIT', NULL, NULL);
INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.subntwk.headend', 'GENERIC_PROJECT.subntwk.headend',  sysdate , 'INIT', NULL, NULL);
INSERT INTO SECURE_OBJ ( SECURE_OBJ_NM, SECURE_OBJ_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY ) VALUES ( 'STM.subntwk.rnc', 'GENERIC_PROJECT.subntwk.rnc',  sysdate , 'INIT', NULL, NULL);
