--==========================================================================
-- FILE INFO
--   $Id: Sorb_Parm.dat.sql,v 1.1 2007/11/29 21:13:59 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================


--============================================================================
-- AddRefSorbParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddSorbParm (i_name IN varchar2,
                                         i_data_typ in varchar2,
                                         i_sorb_parm_id in number default 0) is
    next_parm_id number(9);
    this_parm_id number(9);
BEGIN
    select count(*) into this_parm_id from sorb_parm
       where sorb_parm_nm = i_name and
             data_typ_nm = i_data_typ;

    if (this_parm_id =  0 )
    then
        if (i_sorb_parm_id = 0)
        then
            select nvl(max(sorb_parm_id), 100) INTO next_parm_id
                 FROM sorb_parm ;
            next_parm_id := next_parm_id + 1;
        else
            next_parm_id := i_sorb_parm_id;
        end if;

        INSERT INTO sorb_parm(
            sorb_parm_ID, sorb_parm_nm, CREATED_DTM, CREATED_BY,
             MODIFIED_DTM, MODIFIED_BY, DATA_TYP_NM)
        VALUES(next_parm_id, i_name,SYSDATE,'albeda',
             SYSDATE,'INIT', i_data_typ);
    end if;
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating parm : ' || i_name || SQLERRM);
END;
/

--============================================================================
--  SorbParm
--============================================================================
CREATE OR REPLACE FUNCTION SorbParm (i_parm_name IN varchar2)
   return number is
    gg number;
begin

    select sorb_parm_id INTO gg from sorb_parm
      where sorb_parm_nm = i_parm_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/


