--==========================================================================
-- FILE INFO
--   $Id: Src_Action_Parm.dat.sql,v 1.1 2007/11/29 21:13:57 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
-- SAMPLE 
--exec AddSrcActionParm(scope_proj_tmplt_nm, scope_task_tmplt_nm, 
--                       scope_action_exec_seq,
--                    ActionTmpltParmId('lookup_subntwk', 'subntwk_type'), -- 
--    ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_type', Get_Class_Id('stm_project'))), 
--    taskTmplParmId(.......),
--   taskTmpltnm,
--        src_action_exec_seq,
--       src_actionTmpltparmId(.....) ,
--       default_value); 
--
--
--=========================================================================
exec AddSrcActionParm('create_subntwk', 'create_subntwk', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'),  ProjTmpltParmId('create_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_subntwk', 'create_subntwk', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'),  ProjTmpltParmId('create_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_subntwk', 'create_subntwk', 2, ActionTmpltParmId('create_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_subntwk', STMParmId('subntwk_typ',Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_subntwk', 'create_subntwk', 3, ActionTmpltParmId('assign_subntwk_parent', 'subntwk_id'), NULL,NULL,'create_subntwk' ,2,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_subntwk', 'create_subntwk', 3, ActionTmpltParmId('assign_subntwk_parent', 'parent_subntwk_id'), NULL,NULL,'create_subntwk' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_subntwk', 'create_subntwk', 3, ActionTmpltParmId('assign_subntwk_parent', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_subntwk');

---------------------------------------------------------------------
--  modify_subntwk
---------------------------------------------------------------------
exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'),  ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'),  ProjTmpltParmId('modify_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 2, ActionTmpltParmId('update_subntwk_parms', 'subntwk_id'), NULL,NULL,'modify_subntwk' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 3, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'),  ProjTmpltParmId('modify_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 3, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'),  ProjTmpltParmId('modify_subntwk', STMParmId('parent_subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 4, ActionTmpltParmId('update_subntwk_parent', 'subntwk_id'), NULL,NULL,'modify_subntwk' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 4, ActionTmpltParmId('update_subntwk_parent', 'parent_subntwk_id'), NULL,NULL,'modify_subntwk' ,3,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);
exec AddSrcActionParm('modify_subntwk', 'modify_subntwk', 4, ActionTmpltParmId('update_subntwk_parent', 'association_typ'), NULL,NULL,NULL ,NULL,NULL,'subntwk_to_subntwk');


exec AddSrcActionParm('delete_subntwk', 'delete_subntwk', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'),  ProjTmpltParmId('delete_subntwk', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))),  NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('delete_subntwk', 'delete_subntwk', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'),  ProjTmpltParmId('delete_subntwk', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('delete_subntwk', 'delete_subntwk', 2, ActionTmpltParmId('delete_subntwk', 'subntwk_id'), NULL,NULL,'delete_subntwk' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

--====================
-- Congestion Relief
--====================
exec AddSrcActionParm('congestion_relief', 'congestion_relief', 31, ActionTmpltParmId('lookup_link', 'to_subntwk_typ'), ProjTmpltParmId('congestion_relief', STMParmId('to_subntwk_typ',Get_Class_Id('stm_project'))),  NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 31, ActionTmpltParmId('lookup_link', 'scope_subntwk_typ'), ProjTmpltParmId('congestion_relief', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))),  NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 31, ActionTmpltParmId('lookup_link', 'link_typ'), ProjTmpltParmId('congestion_relief', STMParmId('link_typ',Get_Class_Id('stm_project'))),  NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 32, ActionTmpltParmId('delete_link', 'link_id'),NULL,NULL,'congestion_relief',31,ActionTmpltParmId('lookup_link', 'link_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 33, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('congestion_relief', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 33, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'),NULL,NULL,'congestion_relief',31,ActionTmpltParmId('lookup_link', 'from_subntwk_typ'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 34, ActionTmpltParmId('create_link', 'to_subntwk_id'),NULL,NULL,'congestion_relief',31,ActionTmpltParmId('lookup_link', 'to_subntwk_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 34, ActionTmpltParmId('create_link', 'from_subntwk_id'),NULL,NULL,'congestion_relief',33,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 34, ActionTmpltParmId('create_link', 'link_typ'), ProjTmpltParmId('congestion_relief', STMParmId('link_typ',Get_Class_Id('stm_project'))),  NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 35, ActionTmpltParmId('lookup_pp', 'subntwk_id'),NULL,NULL,'congestion_relief',33,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 36, ActionTmpltParmId('assign_pp_to_link', 'link_id'),NULL,NULL,'congestion_relief',34,ActionTmpltParmId('create_link', 'link_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 36, ActionTmpltParmId('assign_pp_to_link', 'upstream_port_id'),NULL,NULL,'congestion_relief',35,ActionTmpltParmId('lookup_pp', 'upstream_port_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 36, ActionTmpltParmId('assign_pp_to_link', 'downstream_port_id'),NULL,NULL,'congestion_relief',35,ActionTmpltParmId('lookup_pp', 'downstream_port_id'),NULL);

exec AddSrcActionParm('congestion_relief', 'congestion_relief', 36, ActionTmpltParmId('assign_pp_to_link', 'association_typ'), NULL,NULL,NULL ,NULL,NULL,'port_pair_to_link');
--===========================================================================
-- decommission device
--===========================================================================
exec AddSrcActionParm('decommission_device', 'decommission_device', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'),  ProjTmpltParmId('decommission_device', STMParmId('scope_subntwk_typ', Get_Class_Id('stm_project'))),  NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('decommission_device', 'decommission_device', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'),  ProjTmpltParmId('decommission_device', STMParmId('subntwk_typ', Get_Class_Id('stm_project'))), NULL, NULL,NULL,NULL,NULL);

exec AddSrcActionParm('decommission_device', 'decommission_device', 3, ActionTmpltParmId('delete_subntwk', 'subntwk_id'), NULL,NULL,'decommission_device' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('decommission_device', 'decommission_device', 3, ActionTmpltParmId('delete_subntwk', 'cascade_delete'), NULL,NULL,'decommission_device' ,2,ActionTmpltParmId('dummy_decommission_device', 'cascade_delete'),NULL);
--===========================================================================
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_cmts_mac', STMParmId('parent_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 2, ActionTmpltParmId('create_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 3, ActionTmpltParmId('assign_subntwk_parent', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,2,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 3, ActionTmpltParmId('assign_subntwk_parent', 'parent_subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 3, ActionTmpltParmId('assign_subntwk_parent', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_subntwk');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 5, ActionTmpltParmId('create_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ2',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 6, ActionTmpltParmId('assign_subntwk_parent', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 6, ActionTmpltParmId('assign_subntwk_parent', 'parent_subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,2,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 6, ActionTmpltParmId('assign_subntwk_parent', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_subntwk');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 7, ActionTmpltParmId('create_card', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

-- CAUTION:For the card_typ_id, we should have a CardTypeId() function.
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 7, ActionTmpltParmId('create_card', 'card_typ_id'), NULL,NULL,NULL,NULL,NULL,'1');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 8, ActionTmpltParmId('create_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'downstream');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 8, ActionTmpltParmId('create_port', 'card_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,7,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 10, ActionTmpltParmId('assign_freq', 'freq_id'), NULL,NULL,'subntwk_hw_cmts_mac',9,ActionTmpltParmId('lookup_freq', 'freq_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 10, ActionTmpltParmId('assign_freq', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_to_port');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 10, ActionTmpltParmId('assign_freq', 'port_id'), NULL,NULL,'subntwk_hw_cmts_mac',8,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 11, ActionTmpltParmId('create_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'upstream');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 11, ActionTmpltParmId('create_port', 'card_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,7,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 13, ActionTmpltParmId('assign_freq_grp', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_grp_to_port');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 13, ActionTmpltParmId('assign_freq_grp', 'freq_grp_id'), NULL,NULL,'subntwk_hw_cmts_mac',12,ActionTmpltParmId('lookup_freq_grp', 'freq_grp_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 13, ActionTmpltParmId('assign_freq_grp', 'port_id'), NULL,NULL,'subntwk_hw_cmts_mac',11,ActionTmpltParmId('create_port', 'port_id'),NULL);


exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 15, ActionTmpltParmId('lookup_card', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac', 5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 16, ActionTmpltParmId('lookup_port', 'port_direction'), NULL,NULL,NULL, NULL,NULL,'downstream');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 16, ActionTmpltParmId('lookup_port', 'card_id'), NULL,NULL,'subntwk_hw_cmts_mac', 15,ActionTmpltParmId('lookup_card', 'card_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 17, ActionTmpltParmId('lookup_card', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac', 5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 18, ActionTmpltParmId('lookup_port', 'port_direction'), NULL,NULL,NULL, NULL,NULL,'upstream');

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 18, ActionTmpltParmId('lookup_port', 'card_id'), NULL,NULL,'subntwk_hw_cmts_mac', 17,ActionTmpltParmId('lookup_card', 'card_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 19, ActionTmpltParmId('create_pp', 'downstream_port_id'), NULL,NULL,'subntwk_hw_cmts_mac', 16,ActionTmpltParmId('lookup_port', 'port_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 19, ActionTmpltParmId('create_pp', 'upstream_port_id'), NULL,NULL,'subntwk_hw_cmts_mac', 18,ActionTmpltParmId('lookup_port', 'port_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 19, ActionTmpltParmId('create_pp', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac', 5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);
--
--
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 21, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL, NULL,'subntwk_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 21, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_cmts_mac', STMParmId('subntwk_typ3',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);
--
--
--
--exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 21, ActionTmpltParmId('lookup_pp', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,4,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);
--
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 22, ActionTmpltParmId('create_link', 'to_subntwk_id'),NULL,NULL,'subntwk_hw_cmts_mac',21,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 22, ActionTmpltParmId('create_link', 'from_subntwk_id'),NULL,NULL,'subntwk_hw_cmts_mac',5,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 22, ActionTmpltParmId('create_link', 'link_typ'), NULL, NULL,NULL,NULL,NULL,'hfc_link');

--
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 23, ActionTmpltParmId('lookup_pp', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac', 5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);
--
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 24, ActionTmpltParmId('assign_pp_to_link', 'upstream_port_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,23,ActionTmpltParmId('lookup_pp', 'upstream_port_id'),NULL);
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 24, ActionTmpltParmId('assign_pp_to_link', 'downstream_port_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,23,ActionTmpltParmId('lookup_pp', 'downstream_port_id'),NULL);
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 24, ActionTmpltParmId('assign_pp_to_link', 'link_id'), NULL,NULL,'subntwk_hw_cmts_mac' ,22,ActionTmpltParmId('create_link', 'link_id'),NULL);
exec AddSrcActionParm('create_cmts_mac', 'subntwk_hw_cmts_mac', 24, ActionTmpltParmId('assign_pp_to_link', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'port_pair_to_link');

--==========================================================================
-- ip config mac test
--==========================================================================
exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 1, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

--exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 1, ActionTmpltParmId('lookup_subnet', 'is_private_ip'), NULL,NULL,NULL,NULL,NULL,'y');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 2, ActionTmpltParmId('lookup_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 2, ActionTmpltParmId('lookup_ip', 'ip_sbnt_id'), NULL,NULL,'ip_config_mac',1,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

--exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 2, ActionTmpltParmId('lookup_ip', 'is_private_ip'), NULL,NULL,'ip_config_mac',1,ActionTmpltParmId('lookup_subnet', 'is_private_ip'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 3, ActionTmpltParmId('assign_mgmt_ip', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 3, ActionTmpltParmId('assign_mgmt_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 3, ActionTmpltParmId('assign_mgmt_ip', 'ip_addr'), NULL,NULL,'ip_config_mac',2,ActionTmpltParmId('lookup_ip', 'ip_addr'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 3, ActionTmpltParmId('assign_mgmt_ip', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac',2,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

-- This action name is should be changed to dummy_mac_domain_children later
exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 4, ActionTmpltParmId('dummy_subntwk_children', 'num_of_mac_domain'), NULL,NULL,'subntwk_hw_cmts_mac',4,ActionTmpltParmId('dummy_subntwk_children', 'num_of_mac_domain'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 5, ActionTmpltParmId('lookup_subntwk', 'subntwk_id'), NULL,NULL,'subntwk_hw_cmts_mac',5,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 7, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 8, ActionTmpltParmId('lookup_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 8, ActionTmpltParmId('lookup_ip', 'ip_sbnt_id'), NULL,NULL,'ip_config_mac',7,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

--exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 8, ActionTmpltParmId('lookup_ip', 'is_private_ip'), NULL,NULL,'ip_config_mac',7,ActionTmpltParmId('lookup_subnet', 'is_private_ip'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 9, ActionTmpltParmId('assign_mgmt_ip', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 9, ActionTmpltParmId('assign_mgmt_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 9, ActionTmpltParmId('assign_mgmt_ip', 'subntwk_id'), NULL,NULL,'ip_config_mac',5,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 9, ActionTmpltParmId('assign_mgmt_ip', 'ip_addr'), NULL,NULL,'ip_config_mac',8,ActionTmpltParmId('lookup_ip', 'ip_addr'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 11, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 12, ActionTmpltParmId('assign_subnet', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 12, ActionTmpltParmId('assign_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 12, ActionTmpltParmId('assign_subnet', 'ip_sbnt_id'), NULL,NULL,'ip_config_mac',11,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 12, ActionTmpltParmId('assign_subnet', 'subntwk_id'), NULL,NULL,'ip_config_mac',5,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

-- This subntwk_typ should be in the project level, but 'subntwk_typX' may not
-- be a good solution
exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 14, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), NULL, NULL,NULL,NULL,NULL,'network_service_cluster');

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 15, ActionTmpltParmId('create_link', 'from_subntwk_id'),NULL,NULL,'ip_config_mac',14,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 15, ActionTmpltParmId('create_link', 'to_subntwk_id'),NULL,NULL,'ip_config_mac',5,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_cmts_mac', 'ip_config_mac', 15, ActionTmpltParmId('create_link', 'link_typ'), NULL, NULL,NULL,NULL,NULL,'hfc_link');

--===========================================================================
--	modify_cmts_mac:modify_hw_cmts_mac
--===========================================================================
exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 2, ActionTmpltParmId('update_subntwk_parms', 'subntwk_id'), NULL,NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 4, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ2',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 4, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL,NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 5, ActionTmpltParmId('update_subntwk_parms', 'subntwk_id'), NULL,NULL,'modify_hw_cmts_mac',4,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 7, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ2',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

-- obsolete, it is not as acurate as the scope_subntwk_id
--exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 7, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))),NULL,NULL,NULL, NULL, NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 7, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL, NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 8, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ3',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 8, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 9, ActionTmpltParmId('create_link', 'to_subntwk_id'),NULL,NULL,'modify_hw_cmts_mac',8,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 9, ActionTmpltParmId('create_link', 'from_subntwk_id'),NULL,NULL,'modify_hw_cmts_mac',7,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 9, ActionTmpltParmId('create_link', 'link_typ'), NULL, NULL,NULL,NULL,NULL,'hfc_link');

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 10, ActionTmpltParmId('lookup_pp', 'subntwk_id'),NULL,NULL,'modify_hw_cmts_mac',7,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 11, ActionTmpltParmId('assign_pp_to_link', 'upstream_port_id'), NULL,NULL,'modify_hw_cmts_mac' ,10,ActionTmpltParmId('lookup_pp', 'upstream_port_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 11, ActionTmpltParmId('assign_pp_to_link', 'downstream_port_id'), NULL,NULL,'modify_hw_cmts_mac' ,10,ActionTmpltParmId('lookup_pp', 'downstream_port_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 11, ActionTmpltParmId('assign_pp_to_link', 'link_id'), NULL,NULL,'modify_hw_cmts_mac' ,9,ActionTmpltParmId('create_link', 'link_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 11, ActionTmpltParmId('assign_pp_to_link', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'port_pair_to_link');

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 13, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ2',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

--exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 13, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))),NULL,NULL,NULL, NULL, NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 13, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL, NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 14, ActionTmpltParmId('lookup_link', 'from_subntwk_id'),NULL,NULL,'modify_hw_cmts_mac',13,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 14, ActionTmpltParmId('lookup_link', 'link_typ'), NULL, NULL,NULL,NULL,NULL,'hfc_link');

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 16, ActionTmpltParmId('lookup_pp', 'subntwk_id'),NULL,NULL,'modify_hw_cmts_mac',13,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 17, ActionTmpltParmId('assign_pp_to_link', 'upstream_port_id'), NULL,NULL,'modify_hw_cmts_mac' ,16,ActionTmpltParmId('lookup_pp', 'upstream_port_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 17, ActionTmpltParmId('assign_pp_to_link', 'downstream_port_id'), NULL,NULL,'modify_hw_cmts_mac' ,16,ActionTmpltParmId('lookup_pp', 'downstream_port_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 17, ActionTmpltParmId('assign_pp_to_link', 'link_id'), NULL,NULL,'modify_hw_cmts_mac' ,14,ActionTmpltParmId('lookup_link', 'link_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_hw_cmts_mac', 17, ActionTmpltParmId('assign_pp_to_link', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'port_pair_to_link');

--===========================================================================
--	modify_cmts_mac:modify_ipconfig_cmts_mac
--==========================================================================
--exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 2, ActionTmpltParmId('lookup_subntwk', 'subntwk_id'), NULL,NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 3, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 4, ActionTmpltParmId('lookup_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 4, ActionTmpltParmId('lookup_ip', 'ip_sbnt_id'), NULL,NULL,'modify_ipconfig_cmts_mac',3,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 5, ActionTmpltParmId('assign_mgmt_ip', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 5, ActionTmpltParmId('assign_mgmt_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 5, ActionTmpltParmId('assign_mgmt_ip', 'ip_addr'), NULL,NULL,'modify_ipconfig_cmts_mac',4,ActionTmpltParmId('lookup_ip', 'ip_addr'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 5, ActionTmpltParmId('assign_mgmt_ip', 'subntwk_id'), NULL,NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 7, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_cmts_mac', STMParmId('subntwk_typ2',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 7, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL,NULL,'modify_hw_cmts_mac',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 9, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 10, ActionTmpltParmId('lookup_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 10, ActionTmpltParmId('lookup_ip', 'ip_sbnt_id'), NULL,NULL,'modify_ipconfig_cmts_mac',9,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 11, ActionTmpltParmId('assign_mgmt_ip', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 11, ActionTmpltParmId('assign_mgmt_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 11, ActionTmpltParmId('assign_mgmt_ip', 'ip_addr'), NULL,NULL,'modify_ipconfig_cmts_mac',10,ActionTmpltParmId('lookup_ip', 'ip_addr'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 11, ActionTmpltParmId('assign_mgmt_ip', 'subntwk_id'), NULL,NULL,'modify_ipconfig_cmts_mac',7,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 13, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 14, ActionTmpltParmId('assign_subnet', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 14, ActionTmpltParmId('assign_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 14, ActionTmpltParmId('assign_subnet', 'ip_sbnt_id'), NULL,NULL,'modify_ipconfig_cmts_mac',13,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('modify_cmts_mac', 'modify_ipconfig_cmts_mac', 14, ActionTmpltParmId('assign_subnet', 'subntwk_id'), NULL,NULL,'modify_ipconfig_cmts_mac',7,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

--==========================================================================
exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 1, ActionTmpltParmId('lookup_block', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_block');

exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 2, ActionTmpltParmId('create_subnet', 'ip_block_id'), NULL,NULL,'create_subnet',1,ActionTmpltParmId('lookup_block', 'ip_block_id'),NULL);

exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 2, ActionTmpltParmId('create_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 4, ActionTmpltParmId('assign_subnet', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 4, ActionTmpltParmId('assign_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 4, ActionTmpltParmId('assign_subnet', 'ip_sbnt_id'), NULL,NULL,'create_subnet',2,ActionTmpltParmId('create_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('create_ip_subnet', 'create_subnet', 4, ActionTmpltParmId('assign_subnet', 'subntwk_id'), NULL,NULL,'create_subnet',3,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);
--==========================================================================
exec AddSrcActionParm('decommission_ip_subnet', 'decommission_subnet', 1, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('decommission_ip_subnet', 'decommission_subnet', 2, ActionTmpltParmId('release_ip_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('decommission_ip_subnet', 'decommission_subnet', 2, ActionTmpltParmId('release_ip_subnet', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('decommission_ip_subnet', 'decommission_subnet', 2, ActionTmpltParmId('release_ip_subnet', 'ip_sbnt_id'), NULL,NULL,'decommission_subnet',1,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

--exec AddSrcActionParm('decommission_ip_subnet', 'decommission_subnet', 2, ActionTmpltParmId('release_ip_subnet', 'ip_sbnt_addr'), NULL,NULL,'decommission_subnet',1,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_addr'),NULL);

exec AddSrcActionParm('decommission_ip_subnet', 'decommission_subnet', 2, ActionTmpltParmId('release_ip_subnet', 'subntwk_id'), NULL,NULL,'decommission_subnet',1,ActionTmpltParmId('lookup_subnet', 'managing_subntwk_id'),NULL);
--==========================================================================
exec AddSrcActionParm('modify_ip_subnet', 'modify_subnet', 1, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('modify_ip_subnet', 'modify_subnet', 2, ActionTmpltParmId('update_subnet_status', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('modify_ip_subnet', 'modify_subnet', 2, ActionTmpltParmId('update_subnet_status', 'ip_sbnt_id'), NULL,NULL,'modify_subnet',1,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

--exec AddSrcActionParm('modify_ip_subnet', 'modify_subnet', 2, ActionTmpltParmId('update_subnet_status', 'ip_sbnt_addr'), NULL,NULL,'modify_subnet',1,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_addr'),NULL);
--==========================================================================
exec AddSrcActionParm('delete_ip_subnet', 'delete_subnet', 1, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('delete_ip_subnet', 'delete_subnet', 2, ActionTmpltParmId('delete_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('delete_ip_subnet', 'delete_subnet', 2, ActionTmpltParmId('delete_subnet', 'ip_sbnt_id'), NULL,NULL,'delete_subnet',1,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);
--==========================================================================
exec AddSrcActionParm('assign_ip_subnet', 'assign_subnet', 2, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('assign_ip_subnet', 'assign_subnet', 4, ActionTmpltParmId('assign_subnet', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('assign_ip_subnet', 'assign_subnet', 4, ActionTmpltParmId('assign_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('assign_ip_subnet', 'assign_subnet', 4, ActionTmpltParmId('assign_subnet', 'ip_sbnt_id'), NULL,NULL,'assign_subnet',2,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('assign_ip_subnet', 'assign_subnet', 4, ActionTmpltParmId('assign_subnet', 'subntwk_id'), NULL,NULL,'assign_subnet',3,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

--==========================================================================
exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_rf_card', STMParmId('subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('create_rf_card', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 3, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL,NULL,'create_rf_card_port_pps' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 3, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_rf_card', STMParmId('mac_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 4, ActionTmpltParmId('create_card', 'subntwk_id'), NULL,NULL,'create_rf_card_port_pps' ,3,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

-- CAUTION:For the card_typ_id, we should have a CardTypeId() function.
exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 4, ActionTmpltParmId('create_card', 'card_typ_id'), NULL,NULL,NULL,NULL,NULL,'1');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 6, ActionTmpltParmId('create_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'downstream');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 6, ActionTmpltParmId('create_port', 'card_id'), NULL,NULL,'create_rf_card_port_pps' ,4,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 8, ActionTmpltParmId('assign_freq', 'freq_id'), NULL,NULL,'create_rf_card_port_pps',7,ActionTmpltParmId('lookup_freq', 'freq_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 8, ActionTmpltParmId('assign_freq', 'port_id'), NULL,NULL,'create_rf_card_port_pps',6,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 8, ActionTmpltParmId('assign_freq', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_to_port');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 10, ActionTmpltParmId('create_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'upstream');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 10, ActionTmpltParmId('create_port', 'card_id'), NULL,NULL,'create_rf_card_port_pps' ,4,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 12, ActionTmpltParmId('assign_freq_grp', 'freq_grp_id'), NULL,NULL,'create_rf_card_port_pps',11,ActionTmpltParmId('lookup_freq_grp', 'freq_grp_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 12, ActionTmpltParmId('assign_freq_grp', 'port_id'), NULL,NULL,'create_rf_card_port_pps',10,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 12, ActionTmpltParmId('assign_freq_grp', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_grp_to_port');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 14, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_rf_card', STMParmId('mac_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);
exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 14, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL,NULL,'create_rf_card_port_pps' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 16, ActionTmpltParmId('lookup_card', 'subntwk_id'), NULL,NULL,'create_rf_card_port_pps', 14,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 17, ActionTmpltParmId('lookup_port', 'port_direction'), NULL,NULL,NULL, NULL,NULL,'downstream');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 17, ActionTmpltParmId('lookup_port', 'card_id'), NULL,NULL,'create_rf_card_port_pps', 16,ActionTmpltParmId('lookup_card', 'card_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 18, ActionTmpltParmId('lookup_card', 'subntwk_id'), NULL,NULL,'create_rf_card_port_pps', 14,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 19, ActionTmpltParmId('lookup_port', 'port_direction'), NULL,NULL,NULL, NULL,NULL,'upstream');

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 19, ActionTmpltParmId('lookup_port', 'card_id'), NULL,NULL,'create_rf_card_port_pps', 18,ActionTmpltParmId('lookup_card', 'card_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 20, ActionTmpltParmId('create_pp', 'downstream_port_id'), NULL,NULL,'create_rf_card_port_pps', 17,ActionTmpltParmId('lookup_port', 'port_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 20, ActionTmpltParmId('create_pp', 'upstream_port_id'), NULL,NULL,'create_rf_card_port_pps', 19,ActionTmpltParmId('lookup_port', 'port_id'),NULL);

exec AddSrcActionParm('create_rf_card', 'create_rf_card_port_pps', 20, ActionTmpltParmId('create_pp', 'subntwk_id'), NULL,NULL,'create_rf_card_port_pps', 14,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

--==========================================================================
exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('modify_rf_card', STMParmId('mac_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('modify_rf_card', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 3, ActionTmpltParmId('lookup_card', 'subntwk_id'), NULL,NULL,'modify_rf_card_port_pps', 1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 4, ActionTmpltParmId('update_card_parms', 'card_id'), NULL,NULL,'modify_rf_card_port_pps' ,3,ActionTmpltParmId('lookup_card', 'card_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 6, ActionTmpltParmId('lookup_port', 'port_direction'), NULL,NULL,NULL, NULL,NULL,'downstream');

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 6, ActionTmpltParmId('lookup_port', 'card_id'), NULL,NULL,'modify_rf_card_port_pps', 3,ActionTmpltParmId('lookup_card', 'card_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 8, ActionTmpltParmId('update_port_freq', 'freq_id'), NULL,NULL,'modify_rf_card_port_pps',7,ActionTmpltParmId('lookup_freq', 'freq_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 8, ActionTmpltParmId('update_port_freq', 'port_id'), NULL,NULL,'modify_rf_card_port_pps',6,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 8, ActionTmpltParmId('update_port_freq', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_to_port');

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 10, ActionTmpltParmId('lookup_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'upstream');

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 10, ActionTmpltParmId('lookup_port', 'card_id'), NULL,NULL,'modify_rf_card_port_pps' ,3,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 12, ActionTmpltParmId('update_port_freq_grp', 'freq_grp_id'), NULL,NULL,'modify_rf_card_port_pps',11,ActionTmpltParmId('lookup_freq_grp', 'freq_grp_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 12, ActionTmpltParmId('update_port_freq_grp', 'port_id'), NULL,NULL,'modify_rf_card_port_pps',10,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('modify_rf_card', 'modify_rf_card_port_pps', 12, ActionTmpltParmId('update_port_freq_grp', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_grp_to_port');

--===========================================================================
exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 1, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_mac_domain', STMParmId('subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 1, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_typ'), ProjTmpltParmId('create_mac_domain', STMParmId('scope_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 3, ActionTmpltParmId('create_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_mac_domain', STMParmId('mac_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 4, ActionTmpltParmId('assign_subntwk_parent', 'subntwk_id'), NULL,NULL,'create_mac_domain_hw' ,3,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 4, ActionTmpltParmId('assign_subntwk_parent', 'parent_subntwk_id'), NULL,NULL,'create_mac_domain_hw' ,1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 4, ActionTmpltParmId('assign_subntwk_parent', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_subntwk');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 5, ActionTmpltParmId('create_card', 'subntwk_id'), NULL,NULL,'create_mac_domain_hw' ,3,ActionTmpltParmId('create_subntwk', 'subntwk_id'),NULL);

-- CAUTION:For the card_typ_id, we should have a CardTypeId() function.
exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 5, ActionTmpltParmId('create_card', 'card_typ_id'), NULL,NULL,NULL,NULL,NULL,'1');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 6, ActionTmpltParmId('create_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'downstream');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 6, ActionTmpltParmId('create_port', 'card_id'), NULL,NULL,'create_mac_domain_hw' ,5,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 8, ActionTmpltParmId('assign_freq', 'freq_id'), NULL,NULL,'create_mac_domain_hw',7,ActionTmpltParmId('lookup_freq', 'freq_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 8, ActionTmpltParmId('assign_freq', 'port_id'), NULL,NULL,'create_mac_domain_hw',6,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 8, ActionTmpltParmId('assign_freq', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_to_port');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 9, ActionTmpltParmId('create_port', 'port_direction'), NULL,NULL,NULL,NULL, NULL, 'upstream');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 9, ActionTmpltParmId('create_port', 'card_id'), NULL,NULL,'create_mac_domain_hw' ,5,ActionTmpltParmId('create_card', 'card_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 11, ActionTmpltParmId('assign_freq_grp', 'freq_grp_id'), NULL,NULL,'create_mac_domain_hw',10,ActionTmpltParmId('lookup_freq_grp', 'freq_grp_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 11, ActionTmpltParmId('assign_freq_grp', 'port_id'), NULL,NULL,'create_mac_domain_hw',9,ActionTmpltParmId('create_port', 'port_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_hw', 11, ActionTmpltParmId('assign_freq_grp', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'freq_grp_to_port');

--===========================================================================
exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 1, ActionTmpltParmId('dummy_subntwk_children', 'num_of_mac_domain'), NULL,NULL,'create_mac_domain_hw',2,ActionTmpltParmId('dummy_subntwk_children', 'num_of_mac_domain'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 2, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), ProjTmpltParmId('create_mac_domain', STMParmId('mac_subntwk_typ',Get_Class_Id('stm_project'))), NULL,NULL,NULL,NULL,NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 2, ActionTmpltParmId('lookup_subntwk', 'scope_subntwk_id'), NULL,NULL,'create_mac_domain_hw',1,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 4, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 5, ActionTmpltParmId('lookup_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 5, ActionTmpltParmId('lookup_ip', 'ip_sbnt_id'), NULL,NULL,'create_mac_domain_ipconfig',4,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 6, ActionTmpltParmId('assign_mgmt_ip', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 6, ActionTmpltParmId('assign_mgmt_ip', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_address');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 6, ActionTmpltParmId('assign_mgmt_ip', 'ip_addr'), NULL,NULL,'create_mac_domain_ipconfig',5,ActionTmpltParmId('lookup_ip', 'ip_addr'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 6, ActionTmpltParmId('assign_mgmt_ip', 'subntwk_id'), NULL,NULL,'create_mac_domain_ipconfig',2,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 8, ActionTmpltParmId('lookup_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 9, ActionTmpltParmId('assign_subnet', 'association_typ'), NULL,NULL,NULL,NULL,NULL,'subntwk_to_consumable_rsrc');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 9, ActionTmpltParmId('assign_subnet', 'consumable_resource_typ'), NULL,NULL,NULL,NULL,NULL,'ip_subnet');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 9, ActionTmpltParmId('assign_subnet', 'ip_sbnt_id'), NULL,NULL,'create_mac_domain_ipconfig',8,ActionTmpltParmId('lookup_subnet', 'ip_sbnt_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 9, ActionTmpltParmId('assign_subnet', 'subntwk_id'), NULL,NULL,'create_mac_domain_ipconfig',2,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 11, ActionTmpltParmId('lookup_subntwk', 'subntwk_typ'), NULL, NULL,NULL,NULL,NULL,'network_service_cluster');

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 12, ActionTmpltParmId('create_link', 'from_subntwk_id'),NULL,NULL,'create_mac_domain_ipconfig',11,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 12, ActionTmpltParmId('create_link', 'to_subntwk_id'),NULL,NULL,'create_mac_domain_ipconfig',2,ActionTmpltParmId('lookup_subntwk', 'subntwk_id'),NULL);

exec AddSrcActionParm('create_mac_domain', 'create_mac_domain_ipconfig', 12, ActionTmpltParmId('create_link', 'link_typ'), NULL, NULL,NULL,NULL,NULL,'hfc_link');
