--==========================================================================
-- FILE INFO
--   $Id: Subntwk.dat.sql,v 1.1 2007/11/29 21:13:50 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddSubntwk('VoIP_Lite_Network',SubntwkTyp('root_network'), null, TopologyStatus('in_service'));

exec AddSubntwk('Default_vmail',SubntwkTyp('vmail'), SubntwkId('VoIP_Lite_Network'), TopologyStatus('in_service'));

exec AddSubntwk('Default_class_five_switch',SubntwkTyp('class_five_switch'), SubntwkId('VoIP_Lite_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Default_gateway_controller',SubntwkTyp('gateway_controller'), SubntwkId('Default_class_five_switch'), TopologyStatus('in_service'));

exec AddSubntwk('Default_gateway_controller_cluster',SubntwkTyp('gateway_controller_cluster'), SubntwkId('VoIP_Lite_Network'), TopologyStatus('in_service'));

exec AddSubntwk('Default_network_service_cluster_mgr',SubntwkTyp('network_service_cluster_mgr'), SubntwkId('VoIP_Lite_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Default_network_service_cluster',SubntwkTyp('network_service_cluster'), SubntwkId('Default_network_service_cluster_mgr'), TopologyStatus('in_service'));

exec AddSubntwk('Default_rnc',SubntwkTyp('rnc'), SubntwkId('VoIP_Lite_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Default_tod_server',SubntwkTyp('tod_server'), SubntwkId('Default_rnc'), TopologyStatus('in_service'));
exec AddSubntwk('Default_tftp_server',SubntwkTyp('tftp_server'), SubntwkId('Default_rnc'), TopologyStatus('in_service'));
exec AddSubntwk('Default_dns_server',SubntwkTyp('dns_server'), SubntwkId('Default_rnc'), TopologyStatus('in_service'));
exec AddSubntwk('Default_dhcp_server',SubntwkTyp('dhcp_server'), SubntwkId('Default_rnc'), TopologyStatus('in_service'));

exec AddSubntwk('Default_headend',SubntwkTyp('headend'), SubntwkId('VoIP_Lite_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Default_phub',SubntwkTyp('phub'), SubntwkId('Default_headend'), TopologyStatus('in_service'));
exec AddSubntwk('Default_cmts_docsis',SubntwkTyp('cmts_docsis'), SubntwkId('Default_phub'), TopologyStatus('in_service'));
exec AddSubntwk('Default_docsis_cmts_mac_domain',SubntwkTyp('docsis_cmts_mac_domain'), SubntwkId('Default_cmts_docsis'), TopologyStatus('in_service'));
exec AddSubntwk('Default_mta',SubntwkTyp('mta'), SubntwkId('Default_docsis_cmts_mac_domain'), TopologyStatus('in_service'));
exec AddSubntwk('Default_cm',SubntwkTyp('cm'), SubntwkId('Default_docsis_cmts_mac_domain'), TopologyStatus('in_service'));
exec AddSubntwk('Default_cpe',SubntwkTyp('cpe'), SubntwkId('Default_cm'), TopologyStatus('in_service'));
exec AddSubntwk('Default_forward_segment',SubntwkTyp('forward_segment'), SubntwkId('Default_phub'), TopologyStatus('in_service'));
exec AddSubntwk('Default_return_segment',SubntwkTyp('return_segment'), SubntwkId('Default_forward_segment'), TopologyStatus('in_service'));
