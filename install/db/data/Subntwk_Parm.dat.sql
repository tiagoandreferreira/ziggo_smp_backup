--==========================================================================
-- FILE INFO
--   $Id: Subntwk_Parm.dat.sql,v 1.1 2007/11/29 21:13:57 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec AddSubntwkParm(SubntwkId('Default_docsis_cmts_mac_domain'), 'supported_svc', 'voice');
exec AddSubntwkParm(SubntwkId('Default_mta'), 'ip_prov_mode', 'dynamic');
