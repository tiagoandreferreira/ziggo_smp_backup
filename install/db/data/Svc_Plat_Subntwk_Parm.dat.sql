-- FILE INFO
--   $Id: Svc_Plat_Subntwk_Parm.dat.sql,v 1.1 2007/11/29 21:13:58 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec AddSvcPlatSubntwkParm(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_pc_voip_access')), SubntwkTyp('docsis_cmts_mac_domain'), SelectionAlgorithm('jmf'), 'jmf_expr', 'if ((nzlen subsvc.smp_pc_voip_access.sub_svc_id) \|\| (subsvc.svc_id == "10102")) then (subnetwork.supported_svc == "voice") else (subnetwork.supported_svc == "data")', null);