-- FILE INFO
--   $Id: Svc_Plat_Subntwk_Typ.dat.sql,v 1.1 2007/11/29 21:13:58 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('class_five_switch'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('gateway_controller'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('docsis_cmts_mac_domain'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('network_service_cluster'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('cmts_docsis'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('return_segment'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('dns_server'), SelectionAlgorithm('walk'), null,'n');

exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('class_five_switch'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('gateway_controller'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'), SvcId('smp_pc_voip_access')), SubntwkTyp('docsis_cmts_mac_domain'), SelectionAlgorithm('walk'), SelectionAlgorithm('jmf'), 'y');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('network_service_cluster'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('cmts_docsis'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('return_segment'), SelectionAlgorithm('walk'), null,'n');

exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_switch_feature')), SubntwkTyp('class_five_switch'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_switch_feature')), SubntwkTyp('return_segment'), SelectionAlgorithm('walk'), null,'n');

exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_secondary_switch_feature')), SubntwkTyp('class_five_switch'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_secondary_switch_feature')), SubntwkTyp('return_segment'), SelectionAlgorithm('walk'), null,'n');

exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_intercept')), SubntwkTyp('class_five_switch'), SelectionAlgorithm('walk'), null,'n');
exec AddSvcPlatSubntwkTyp(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_intercept')), SubntwkTyp('return_segment'), SelectionAlgorithm('walk'), null,'n');
