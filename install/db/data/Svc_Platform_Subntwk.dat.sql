-- FILE INFO
--   $Id: Svc_Platform_Subntwk.dat.sql,v 1.1 2007/11/29 21:13:50 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================

exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('class_five_switch'), MgmtMode('provision'), NtwkRole('call_switch_platform'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('gateway_controller'), MgmtMode('provision'), NtwkRole('gateway_controller'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('docsis_cmts_mac_domain'), MgmtMode('provision'), NtwkRole('docsis_cmts_mac_domain'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('network_service_cluster'), MgmtMode('provision'), NtwkRole('ip_network_cluster_mgr'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('cmts_docsis'), MgmtMode('provision'), NtwkRole('cmts_docsis'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access')), SubntwkTyp('dns_server'), MgmtMode('provision'), NtwkRole('primary_dns_server'));

exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('class_five_switch'), MgmtMode('provision'), NtwkRole('call_switch_platform'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('gateway_controller'), MgmtMode('provision'), NtwkRole('gateway_controller'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('docsis_cmts_mac_domain'), MgmtMode('provision'), NtwkRole('docsis_cmts_mac_domain'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('network_service_cluster'), MgmtMode('provision'), NtwkRole('ip_network_cluster_mgr'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('cmts_docsis'), MgmtMode('provision'), NtwkRole('cmts_docsis'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'));

exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_switch_feature')), SubntwkTyp('class_five_switch'), MgmtMode('provision'), NtwkRole('call_switch_platform'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_switch_feature')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'));

exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_secondary_switch_feature')), SubntwkTyp('class_five_switch'), MgmtMode('provision'), NtwkRole('call_switch_platform'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_secondary_switch_feature')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'));

exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_intercept')), SubntwkTyp('class_five_switch'), MgmtMode('provision'), NtwkRole('call_switch_platform'));
exec AddSvcPlatformSubntwk(SvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_intercept')), SubntwkTyp('return_segment'), MgmtMode('provision'), NtwkRole('network_entry_point'));
