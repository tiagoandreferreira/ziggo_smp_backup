--==========================================================================
-- FILE INFO
--   $Id: Svc_Supported_Platform.dat.sql,v 1.1 2007/11/29 21:13:52 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddSvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_pc_voip_access'),1,NULL,'dynamic');
exec AddSvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_dial_tone_access'),1,NULL,'dynamic');
exec AddSvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_switch_feature'),1,NULL,'dynamic');
exec AddSvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_secondary_switch_feature'),1,NULL,'dynamic');
exec AddSvcSupportedPlatform(SvcDeliveryPlatform('voip'),SvcId('smp_intercept'),1,NULL,'dynamic');
