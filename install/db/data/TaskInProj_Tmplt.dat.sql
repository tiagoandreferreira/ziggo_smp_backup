--==========================================================================
-- FILE INFO
--   $Id: TaskInProj_Tmplt.dat.sql,v 1.1 2007/11/29 21:13:51 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddTaskInProjTmplt('create_cmts_mac','subntwk_hw_cmts_mac',1);
exec AddTaskInProjTmplt('create_cmts_mac','ip_config_mac',2);
exec AddTaskInProjTmplt('create_cmts_mac','verification',3);

--==========================================================================
exec AddTaskInProjTmplt('create_mac_domain','create_mac_domain_hw',1);
exec AddTaskInProjTmplt('create_mac_domain','create_mac_domain_ipconfig',2);
exec AddTaskInProjTmplt('create_mac_domain','verification',3);

--==========================================================================
-- not implemented yet
exec AddTaskInProjTmplt('create_cmts_no_mac','subntwk_hw_cmts_no_mac',1);
exec AddTaskInProjTmplt('create_cmts_no_mac','ip_config_no_mac',2);

exec AddTaskInProjTmplt('create_poprouter','subntwk_poprouter',1);

exec AddTaskInProjTmplt('modify_cmts_no_mac','modify_hw_cmts_no_mac',1);
exec AddTaskInProjTmplt('modify_cmts_no_mac','modify_ipconfig_cmts_no_mac',2);

--==========================================================================
exec AddTaskInProjTmplt('modify_cmts_mac','modify_hw_cmts_mac',1);
exec AddTaskInProjTmplt('modify_cmts_mac','modify_ipconfig_cmts_mac',2);
exec AddTaskInProjTmplt('modify_cmts_mac','verification',3);

--==========================================================================
exec AddTaskInProjTmplt('decommission_device','decommission_device',1);
exec AddTaskInProjTmplt('decommission_device','verification',2);

--==========================================================================
exec AddTaskInProjTmplt('congestion_relief','congestion_relief',1);
exec AddTaskInProjTmplt('congestion_relief','verification',2);

--==========================================================================
exec AddTaskInProjTmplt('create_subntwk','create_subntwk', 1);
exec AddTaskInProjTmplt('create_subntwk','verification', 2);

--==========================================================================
exec AddTaskInProjTmplt('modify_subntwk','modify_subntwk', 1);
exec AddTaskInProjTmplt('modify_subntwk','verification', 2);

--==========================================================================
exec AddTaskInProjTmplt('delete_subntwk','delete_subntwk', 1);
exec AddTaskInProjTmplt('delete_subntwk','verification', 2);

--==========================================================================
exec AddTaskInProjTmplt('create_ip_subnet','create_subnet',1);
exec AddTaskInProjTmplt('create_ip_subnet','verification', 2);

--==========================================================================
exec AddTaskInProjTmplt('decommission_ip_subnet','decommission_subnet',1);
exec AddTaskInProjTmplt('decommission_ip_subnet','verification',2);

--==========================================================================
exec AddTaskInProjTmplt('delete_ip_subnet','delete_subnet',1);
exec AddTaskInProjTmplt('delete_ip_subnet','verification',2);

--==========================================================================
exec AddTaskInProjTmplt('modify_ip_subnet','modify_subnet',1);
exec AddTaskInProjTmplt('modify_ip_subnet','verification',2);

--==========================================================================
exec AddTaskInProjTmplt('assign_ip_subnet','assign_subnet',1);
exec AddTaskInProjTmplt('assign_ip_subnet','verification',2);

--==========================================================================
-- not implemented yet
exec AddTaskInProjTmplt('create_ip_sub_network_element','create_ipsne',1);
exec AddTaskInProjTmplt('create_network_cluster','create_cluster',1);
exec AddTaskInProjTmplt('modify_network_cluster','modify_cluster',1);
--exec AddTaskInProjTmplt('delete_ip_service_network','delete_ipsne', 1);
--exec AddTaskInProjTmplt('delete_ip_service_network','delete_ipsne', 1);
exec AddTaskInProjTmplt('add_link','add_link', 1);
exec AddTaskInProjTmplt('swap_link','add_link', 2);
exec AddTaskInProjTmplt('swap_link','delete_link',1);
exec AddTaskInProjTmplt('remove_link','delete_link',1);

exec AddTaskInProjTmplt('modify_ip_sub_network_element','modify_ipsne',1);
exec AddTaskInProjTmplt('delete_network_cluster','delete_ipsne',1);

--==========================================================================
exec AddTaskInProjTmplt('create_rf_card','create_rf_card_port_pps', 1);
exec AddTaskInProjTmplt('create_rf_card','verification', 2);

--==========================================================================
exec AddTaskInProjTmplt('modify_rf_card','modify_rf_card_port_pps', 1);
exec AddTaskInProjTmplt('modify_rf_card','verification', 2);
