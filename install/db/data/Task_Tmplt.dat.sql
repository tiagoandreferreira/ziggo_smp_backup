--==========================================================================
-- FILE INFO
--   $Id: Task_Tmplt.dat.sql,v 1.1 2007/11/29 21:13:55 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddTaskTmplt('subntwk_hw_cmts_mac','hardware configuration for cmts with mac domain', '10');

exec AddTaskTmplt('subntwk_hw_cmts_no_mac','hardware configuration for cmts without mac domain', '10');

exec AddTaskTmplt('create_mac_domain_hw','hardware configuration for mac domain layer', '10');

--==========================================================================
exec AddTaskTmplt('ip_config_mac','ip configuration for cmts with mac domain', '10');

exec AddTaskTmplt('ip_config_no_mac','ip configuration for cmts without mac domain', '10');

exec AddTaskTmplt('create_mac_domain_ipconfig','ip configuration for mac domain layer', '10');

--==========================================================================
exec AddTaskTmplt('modify_hw_cmts_no_mac','modify hardware configuration for cmts without mac domain', '10');
exec AddTaskTmplt('modify_hw_cmts_mac','modify hardware configuration for cmts with mac domain', '10');
--==========================================================================
exec AddTaskTmplt('modify_ipconfig_cmts_no_mac','modify ip configuration for cmts without mac domain', '10');
exec AddTaskTmplt('modify_ipconfig_cmts_mac','modify ip configuration for cmts with mac domain', '10');
--==========================================================================
exec AddTaskTmplt('subntwk_poprouter','create pop router', '10');
exec AddTaskTmplt('decommission_device','decommision device', '10');
--==========================================================================
exec AddTaskTmplt('congestion_relief','congestion relief', '10');
exec AddTaskTmplt('create_subntwk','create hfc subnetwork', '10');
exec AddTaskTmplt('modify_subntwk','modify hfc subnetwork', '10');
exec AddTaskTmplt('delete_subntwk','delete hfc subnetwork', '10');
--==========================================================================
exec AddTaskTmplt('create_subnet','create ip subnet', '10');
exec AddTaskTmplt('decommission_subnet','decommission ip subnet', '10');
exec AddTaskTmplt('modify_subnet','modify ip subnet', '10');
exec AddTaskTmplt('delete_subnet','delete ip subnet', '10');
exec AddTaskTmplt('assign_subnet','assign ip subnet', '10');
--==========================================================================
exec AddTaskTmplt('create_ipsne','create ip service network', '10');
exec AddTaskTmplt('modify_ipsne','modify ip service network', '10');
exec AddTaskTmplt('create_cluster','create network service cluster', '10');
exec AddTaskTmplt('modify_cluster','modify network service cluster', '10');
exec AddTaskTmplt('delete_ipsne','delete ip service network', '10');
--==========================================================================
exec AddTaskTmplt('add_link','add link', '10');
exec AddTaskTmplt('delete_link','delete link', '10');
exec AddTaskTmplt('verification','complete verification of the project', '10');
--==========================================================================
exec AddTaskTmplt('create_rf_card_port_pps','Create RF cards with port pair', '10');

--==========================================================================
exec AddTaskTmplt('modify_rf_card_port_pps','Modify RF cards with port pair', '10');
