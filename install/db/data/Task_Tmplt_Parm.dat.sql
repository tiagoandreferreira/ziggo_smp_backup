--==========================================================================
-- FILE INFO
--   $Id: Task_Tmplt_Parm.dat.sql,v 1.1 2007/11/29 21:13:56 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddTaskTmpltParm('create_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project')));
exec AddTaskTmpltParm('modify_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project')));
exec AddTaskTmpltParm('delete_subntwk', STMParmId('assigned_to', Get_Class_Id('stm_project')));
exec AddTaskTmpltParm('congestion_relief', STMParmId('assigned_to', Get_Class_Id('stm_project')));
exec AddTaskTmpltParm('subntwk_hw_cmts_mac', STMParmId('assigned_to', Get_Class_Id('stm_project')));
