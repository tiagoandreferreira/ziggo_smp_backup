--==========================================================================
-- FILE INFO
--   $Id: Task_tmplt_depy.dat.sql,v 1.1 2007/11/29 21:13:54 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
exec AddTaskTmpltDepy('create_cmts_mac','subntwk_hw_cmts_mac','ip_config_mac');
exec AddTaskTmpltDepy('create_cmts_mac','ip_config_mac','verification');

--==========================================================================
exec AddTaskTmpltDepy('create_mac_domain','create_mac_domain_hw','create_mac_domain_ipconfig');
exec AddTaskTmpltDepy('create_mac_domain','create_mac_domain_ipconfig','verification');

--exec AddTaskTmpltDepy('create_mac_domain','create_mac_domain_hw','modify_ipconfig_cmts_mac');
--exec AddTaskTmpltDepy('create_mac_domain','modify_ipconfig_cmts_mac','verification');

--==========================================================================
exec AddTaskTmpltDepy('decommission_device','decommission_device','verification');

--==========================================================================
--exec AddTaskTmpltDepy(('create_cmts_no_mac','subntwk_hw_cmts_no_mac','ip_config_no_mac');

exec AddTaskTmpltDepy('modify_cmts_no_mac','modify_hw_cmts_no_mac','modify_ipconfig_cmts_no_mac');

--==========================================================================
exec AddTaskTmpltDepy('modify_cmts_mac','modify_hw_cmts_mac','modify_ipconfig_cmts_mac');
exec AddTaskTmpltDepy('modify_cmts_mac','modify_ipconfig_cmts_mac','verification');

--==========================================================================
exec AddTaskTmpltDepy('swap_link','delete_link','add_link' );

--==========================================================================
exec AddTaskTmpltDepy('congestion_relief','congestion_relief','verification');
exec AddTaskTmpltDepy('create_subntwk','create_subntwk','verification');
exec AddTaskTmpltDepy('modify_subntwk','modify_subntwk','verification');
exec AddTaskTmpltDepy('delete_subntwk','delete_subntwk','verification');

--==========================================================================
exec AddTaskTmpltDepy('create_ip_subnet','create_subnet','verification');
exec AddTaskTmpltDepy('decommission_ip_subnet','decommission_subnet','verification');
exec AddTaskTmpltDepy('modify_ip_subnet','modify_subnet','verification');
exec AddTaskTmpltDepy('delete_ip_subnet','delete_subnet','verification');
exec AddTaskTmpltDepy('assign_ip_subnet','assign_subnet','verification');

--==========================================================================
exec AddTaskTmpltDepy('create_rf_card','create_rf_card_port_pps','verification');

--==========================================================================
exec AddTaskTmpltDepy('modify_rf_card','modify_rf_card_port_pps','verification');
