--==========================================================================
-- FILE INFO
--   $Id: Topology_Impact.dat.sql,v 1.1 2007/11/29 21:13:51 siarheig Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--==========================================================================
exec AddTopologyImpact('dial_tone_addline', NtwkAction('dial.tone.addline'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_bind', NtwkAction('dial.tone.bind'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_deleteline', NtwkAction('dial.tone.deleteline'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_swap_update_cms.add', NtwkAction('dial.tone.swap.add'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_swap_update_cms.delete', NtwkAction('dial.tone.swap.delete'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_unbind', NtwkAction('dial.tone.unbind'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_update', NtwkAction('dial.tone.update'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('dial_tone_chg_tn', NtwkAction('dial.tone.chg_tn'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));

exec AddTopologyImpact('intercept_add', NtwkAction('intercept.add'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('intercept_delete', NtwkAction('intercept.delete'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));

exec AddTopologyImpact('mta_voip_addto_cms', NtwkAction('voip.mta.add'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('mta_voip_bind_cms', NtwkAction('voip.mta.bind'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('mta_voip_swap_cms.add', NtwkAction('voip.mta.swap.add'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('mta_voip_swap_cms.delete', NtwkAction('voip.mta.swap.delete'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('mta_voip_unbind_cms', NtwkAction('voip.mta.unbind'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('mta_voip_chg_cmts', NtwkAction('voip.mta.chg_cmts'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('mta_voip_deletefrom_cms', NtwkAction('voip.mta.delete'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));

exec AddTopologyImpact('switch_voice_features_add', NtwkAction('voice.features.add'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('switch_voice_features_delete', NtwkAction('voice.features.delete'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('switch_voice_features_update', NtwkAction('voice.features.update'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));

exec AddTopologyImpact('secondary_features_add', NtwkAction('secondary.features.add'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('secondary_features_delete', NtwkAction('secondary.features.delete'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));
exec AddTopologyImpact('secondary_features_update', NtwkAction('secondary.features.update'),SubntwkTyp('class_five_switch'), SvcDeliveryPlatform('voip'));