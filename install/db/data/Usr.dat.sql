--==========================================================================
-- FILE INFO
--   $Id: Usr.dat.sql,v 1.3 2011/12/28 11:33:28 prithvim Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the sigma implementation spec
--==========================================================================
INSERT INTO USR ( USR_NM, USR_PSWD, USR_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, USR_STATUS ) VALUES ( 'superuser', 'superuser', 'admin user',  SYSDATE, 'INIT', NULL, NULL, 'a');

INSERT INTO USR ( USR_NM, USR_PSWD, USR_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, USR_STATUS ) VALUES ( 'mario', 'mario', 'Out Side Plant User',  SYSDATE , 'INIT', NULL, NULL, 'a');

INSERT INTO USR ( USR_NM, USR_PSWD, USR_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, USR_STATUS ) VALUES ( 'leah', 'leah', 'Out Side Plant User',  SYSDATE , 'INIT', NULL, NULL, 'a');

INSERT INTO USR ( USR_NM, USR_PSWD, USR_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, USR_STATUS ) VALUES ( 'sigma', 'sigma', 'Sigma Default User',  SYSDATE , 'INIT', NULL, NULL, 'a');

INSERT INTO USR ( USR_NM, USR_PSWD, USR_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, USR_STATUS ) VALUES ( 'stm_system', 'stm_system', 'STM Backend User',  SYSDATE , 'INIT', NULL, NULL, 'a');
