--============================================================================
--    $Id: antfsm.dat.sql,v 1.1 2007/11/29 21:13:57 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
Delete from trouble_ticket_parm;
Delete from trouble_ticket;

------------------------------------------------------------------
---------   Data fill for table: Trouble_Ticket          ---------
------------------------------------------------------------------
insert into trouble_ticket(ticket_id,ticket_nm,ticket_status_nm,title,descr,closed_dtm,closed_by,created_dtm,created_by,modified_dtm,modified_by)
values (1, 'INTERNAL', 'Open', 'WSR24567823', 'Service advisory for serviced area BROOKLYN_AMP_1001_12', null, null, SYSDATE, 'veliborm', SYSDATE, 'veliborm');
insert into trouble_ticket(ticket_id,ticket_nm,ticket_status_nm,title,descr,closed_dtm,closed_by,created_dtm,created_by,modified_dtm,modified_by)
values (2, 'INTERNAL', 'Open', 'WSR24567543', 'Trouble Ticket #2 for Tom Jones', null, null, SYSDATE, 'veliborm', SYSDATE, 'veliborm');
insert into trouble_ticket(ticket_id,ticket_nm,ticket_status_nm,title,descr,closed_dtm,closed_by,created_dtm,created_by,modified_dtm,modified_by)
values (3, 'INTERNAL', 'Open', 'WSR29276511', 'Trouble Ticket #1 for Bill Jones', null, null, SYSDATE, 'veliborm', SYSDATE, 'veliborm');
insert into trouble_ticket(ticket_id,ticket_nm,ticket_status_nm,title,descr,closed_dtm,closed_by,created_dtm,created_by,modified_dtm,modified_by)
values (4, 'INTERNAL', 'Open', 'WFR4567348299', 'Service Advisory #1 for svc id 304246', null, null, SYSDATE, 'veliborm', SYSDATE, 'veliborm');
insert into trouble_ticket(ticket_id,ticket_nm,ticket_status_nm,title,descr,closed_dtm,closed_by,created_dtm,created_by,modified_dtm,modified_by)
values (5, 'INTERNAL', 'Open', 'WFR456782334', 'Service Advisory #1 for svc id 304250', null, null, SYSDATE, 'veliborm', SYSDATE, 'veliborm');
insert into trouble_ticket(ticket_id,ticket_nm,ticket_status_nm,title,descr,closed_dtm,closed_by,created_dtm,created_by,modified_dtm,modified_by)
values (6, 'INTERNAL', 'Open', 'WFR4675467532', 'Service Advisory #2 for svc id 304250', null, null, SYSDATE, 'veliborm', SYSDATE, 'veliborm');

------------------------------------------------------------------
---------   Data fill for table: Trouble_Ticket_Parm          ---------
------------------------------------------------------------------
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('trouble_ticket_id', 501), 'WSR24567823', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('status', 501), 'Open', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('desc', 501), 'E-mail Problem', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('created_by', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('create_dtm', 501), '20010521120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('closed_dtm', 501), '20010621120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('last_modified_by', 501), 'davidc', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('modified_dtm', 501), '20010616120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('cim_ticket', 501), '0', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('related_wsr', 501), 'WSR00345595', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('failure_ticket', 501), 'WFR00056789', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('assigned_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('category', 501), 'e-mail', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('type', 501), 'Local WSR', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('resolution_category', 501), 'Urgent', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('resolution_type', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('chronic', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('referred_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('acct_number', 501), '829372637', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('company_number', 501), '238', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('province', 501), 'Ontario', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('shub', 501), 'so060', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('smt', 501), '10.24.248.130', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('street', 501), '35 Aldwych Street', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('city', 501), 'Toronto', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('pop', 501), '10.24.47.142', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('work_log', 501), 'Tried restoring connection at 10:47:43 am.', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('topology_type', 500), 'serviced_area', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (1, GET_PARM_ID('topology_name', 500), 'BROOKLYN_AMP_1001_12', null, SYSDATE, 'veliborm', null, null);

insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('trouble_ticket_id', 501), 'WSR24567543', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('status', 501), 'Open', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('desc', 501), 'hsd problem', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('created_by', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('create_dtm', 501), '20010521120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('closed_dtm', 501), '20010621120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('last_modified_by', 501), 'davidc', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('modified_dtm', 501), '20010616120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('cim_ticket', 501), '0', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('related_wsr', 501), 'WSR00345595', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('failure_ticket', 501), 'WFR00056789', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('assigned_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('category', 501), 'e-mail', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('type', 501), 'Local WSR', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('resolution_category', 501), 'Not Urgent', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('resolution_type', 501), 'Action', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('chronic', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('referred_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('acct_number', 501), '983477873', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('company_number', 501), '230', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('province', 501), 'Ontario', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('shub', 501), 'so060', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('smt', 501), '10.24.248.130', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('street', 501), '35 Aldwych Street', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('city', 501), 'Toronto', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('pop', 501), '10.24.47.142', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('work_log', 501), 'Tried restoring connection at 10:47:43 am.', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('topology_type', 500), 'cmts_ip_address', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (2, GET_PARM_ID('topology_name', 500), '124.48.34.10', null, SYSDATE, 'veliborm', null, null);

insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('trouble_ticket_id', 501), 'WSR29276511', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('status', 501), 'Open', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('desc', 501), 'hsd', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('created_by', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('create_dtm', 501), '20010521120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('closed_dtm', 501), '20010621120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('last_modified_by', 501), 'davidc', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('modified_dtm', 501), '20010616120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('cim_ticket', 501), '0', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('related_wsr', 501), 'WSR00345595', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('failure_ticket', 501), 'WFR00056789', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('assigned_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('category', 501), 'e-mail', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('type', 501), 'Local WSR', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('resolution_category', 501), 'Not Urgent', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('resolution_type', 501), 'Action', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('chronic', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('referred_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('acct_number', 501), '274422301', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('company_number', 501), '170', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('province', 501), 'Ontario', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('shub', 501), 'so060', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('smt', 501), '10.24.248.130', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('street', 501), '19 Silver Lake Drive', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('city', 501), 'Toronto', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('pop', 501), '10.24.47.142', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('work_log', 501), 'Tried restoring connection at 10:47:43 am.', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('topology_type', 500), 'cmts_ip_address', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (3, GET_PARM_ID('topology_name', 500), '124.48.34.10', null, SYSDATE, 'veliborm', null, null);

insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('trouble_ticket_id', 501), 'WFR4567348299', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('status', 501), 'Open', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('desc', 501), 'hsd problem', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('created_by', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('create_dtm', 501), '20010521120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('closed_dtm', 501), '20010621120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('last_modified_by', 501), 'davidc', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('modified_dtm', 501), '20010616120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('cim_ticket', 501), '0', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('related_wsr', 501), 'WSR00345595', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('failure_ticket', 501), 'WFR00056789', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('assigned_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('category', 501), 'e-mail', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('type', 501), 'Local WSR', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('resolution_category', 501), 'Not Urgent', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('resolution_type', 501), 'Action', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('chronic', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('referred_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('acct_number', 501), '98347787', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('company_number', 501), '230', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('province', 501), 'Ontario', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('shub', 501), 'so060', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('smt', 501), '10.24.248.130', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('street', 501), '19 Silver Lake Drive', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('city', 501), 'Toronto', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('pop', 501), '10.24.47.142', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('work_log', 501), 'Tried restoring connection at 10:47:43 am.', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('topology_type', 500), 'cm_mac_address', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (4, GET_PARM_ID('topology_name', 500), '0000CF6523AB', null, SYSDATE, 'veliborm', null, null);

insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('trouble_ticket_id', 501), 'WFR456782334', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('status', 501), 'Open', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('desc', 501), 'E-mail Problem', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('created_by', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('create_dtm', 501), '20010521120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('closed_dtm', 501), '20010621120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('last_modified_by', 501), 'davidc', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('modified_dtm', 501), '20010616120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('cim_ticket', 501), '0', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('related_wsr', 501), 'WSR00345595', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('failure_ticket', 501), 'WFR00056789', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('assigned_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('category', 501), 'e-mail', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('type', 501), 'Local WSR', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('resolution_category', 501), 'Not Urgent', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('resolution_type', 501), 'Action', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('chronic', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('referred_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('acct_number', 501), '98347787', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('company_number', 501), '230', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('province', 501), 'Ontario', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('shub', 501), 'so060', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('smt', 501), '10.24.248.130', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('street', 501), '19 Silver Lake Drive', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('city', 501), 'Toronto', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('pop', 501), '10.24.47.142', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('work_log', 501), 'Tried restoring connection at 10:47:43 am.', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('topology_type', 500), 'cm_mac_address', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (5, GET_PARM_ID('topology_name', 500), '0000CF6523AC', null, SYSDATE, 'veliborm', null, null);

insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('trouble_ticket_id', 501), 'WFR4675467532', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('status', 501), 'Open', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('desc', 501), 'hsd problem', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('created_by', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('create_dtm', 501), '20010521120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('closed_dtm', 501), '20010621120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('last_modified_by', 501), 'davidc', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('modified_dtm', 501), '20010616120000', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('cim_ticket', 501), '0', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('related_wsr', 501), 'WSR00345595', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('failure_ticket', 501), 'WFR00056789', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('assigned_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('category', 501), 'e-mail', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('type', 501), 'Local WSR', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('resolution_category', 501), 'Not Urgent', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('resolution_type', 501), 'Action', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('chronic', 501), '1', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('referred_to', 501), 'veliborm', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('acct_number', 501), '98347787', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('company_number', 501), '230', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('province', 501), 'Ontario', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('shub', 501), 'so060', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('smt', 501), '10.24.248.130', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('street', 501), '19 Silver Lake Drive', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('city', 501), 'Toronto', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('pop', 501), '10.24.47.142', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('work_log', 501), 'Tried restoring connection at 10:47:43 am.', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('topology_type', 500), 'docsis_cmts', null, SYSDATE, 'veliborm', null, null);
insert into trouble_ticket_parm(ticket_id,parm_id,val,extra_val,created_dtm,created_by,modified_dtm,modified_by) 
values (6, GET_PARM_ID('topology_name', 500), 'FS_CMTS2016_LINK', null, SYSDATE, 'veliborm', null, null);
