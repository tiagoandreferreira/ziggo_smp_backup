---------------------------------------------------------------------------
-- FILE INFO
--    $Id: drop_all_tables.sql,v 1.1 2001/08/21 13:31:52 parmis Exp $
--
-- DESCRIPTION
-- This script a stored procedure DROP_ALL_TABLES. When executed with eg 
-- EXEC DROP_ALL_TABLES it drops ALL the tables in the schema, including all 
-- associated indexes and contraints.
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------
CREATE OR REPLACE
PROCEDURE drop_all_tables is
-- Author:  Parmi Sahota 12 Jul 01

  v_statement varchar2(100);

   cursor tables_cur is
     select table_name from user_tables;

BEGIN
   for r_tables in tables_cur loop
     v_statement := 'DROP TABLE '||r_tables.table_name||' CASCADE CONSTRAINTS';
     execute immediate v_statement;

   end loop;
END;
/





