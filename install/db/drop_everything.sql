---------------------------------------------------------------------------
-- FILE INFO
--    $Id: drop_everything.sql,v 1.2 2001/08/28 14:59:04 parmis Exp $
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------
DECLARE
---------------------------------------------------------------------------
-- Author:  Parmi Sahota 23 Aug 01
-- DESCRIPTION
-- This script drops ALL the tables, synonyms and sequences in the
-- schema, including all associated indexes and constraints.
--
---------------------------------------------------------------------------

  v_statement varchar2(100);

   cursor views_cur is
     select view_name from user_views;

   cursor tables_cur is
     select table_name from user_tables;

   cursor synonyms_cur is
     select synonym_name from user_synonyms;

   cursor sequences_cur is
     select sequence_name from user_sequences;

   cursor functions_cur is
     select distinct(name) function_name from user_source
     where  type = 'FUNCTION';

   cursor procedures_cur is
     select distinct(name) procedure_name from user_source
     where  type = 'PROCEDURE';

BEGIN
   for r_views in views_cur loop
     v_statement := 'DROP VIEW '||r_views.view_name;
     execute immediate v_statement;
   end loop;

   for r_tables in tables_cur loop
     v_statement := 'DROP TABLE '||r_tables.table_name||' CASCADE CONSTRAINTS';
     execute immediate v_statement;
   end loop;

   for r_synonyms in synonyms_cur loop
     v_statement := 'DROP SYNONYM '||r_synonyms.synonym_name;
     execute immediate v_statement;
   end loop;

   for r_sequence in sequences_cur loop
     v_statement := 'DROP SEQUENCE '||r_sequence.sequence_name;
     execute immediate v_statement;
   end loop;

   for r_function in functions_cur loop
     v_statement := 'DROP FUNCTION '||r_function.function_name;
     execute immediate v_statement;
   end loop;

   for r_procedure in procedures_cur loop
     v_statement := 'DROP PROCEDURE '||r_procedure.procedure_name;
     execute immediate v_statement;
   end loop;
END;
/





