---------------------------------------------------------------------------
-- FILE INFO
--    $Id: drop_objects.sql,v 1.3 2001/08/23 13:43:21 parmis Exp $
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------
DECLARE
---------------------------------------------------------------------------
-- Author:  Parmi Sahota 23 Aug 01
-- DESCRIPTION
-- This script drops ALL the tables, synonyms and sequences in the
-- schema, including all associated indexes and constraints.
--
---------------------------------------------------------------------------

  v_statement varchar2(100);

   cursor tables_cur is
     select table_name from user_tables;
   cursor synonyms_cur is
     select synonym_name from user_synonyms;
   cursor sequences_cur is
     select sequence_name from user_sequences;

BEGIN
   for r_tables in tables_cur loop
     v_statement := 'DROP TABLE '||r_tables.table_name||' CASCADE CONSTRAINTS';
     execute immediate v_statement;

   end loop;

   for r_synonyms in synonyms_cur loop
     v_statement := 'DROP SYNONYM '||r_synonyms.synonym_name;
     execute immediate v_statement;

   end loop;

   for r_sequences in sequences_cur loop
     v_statement := 'DROP SEQUENCE '||r_sequences.sequence_name;
     execute immediate v_statement;

   end loop;
END;
/





