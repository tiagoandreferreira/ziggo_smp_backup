---------------------------------------------------------------------------
-- FILE INFO
--    $Id: drop_tablespaces.sql,v 1.1.2.1 2009/07/17 11:36:22 atulg Exp $
--
-- DESCRIPTION
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------
SPOOL drop_tablespaces.log

DROP TABLESPACE "SMP_IDX_SMALL"  including contents and datafiles;
DROP TABLESPACE "SMP_IDX_MED"  including contents and datafiles;
DROP TABLESPACE "SMP_IDX_LARGE01"  including contents and datafiles;
DROP TABLESPACE "SMP_IDX_LARGE04"  including contents and datafiles;
DROP TABLESPACE "SMP_IDX_LARGE03"  including contents and datafiles;
DROP TABLESPACE "SMP_IDX_LARGE05"  including contents and datafiles;
DROP TABLESPACE "SMP_IDX_LARGE02"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_CONTROL"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_CATTR"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_CORR_SET"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_CORR_SET_PROP"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_DLINK"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_IPARMS"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_INST"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_JEA_TASK"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_MT"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_MT_LOG"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_MT_NOTE"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_MT_PARM"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_OPARMS"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_PDEF"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_SMALL"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_RINSTANCE"  including contents and datafiles;
DROP TABLESPACE "JWF_IDX_VAR"  including contents and datafiles;
DROP TABLESPACE "SMP_MEDIUM"  including contents and datafiles;
DROP TABLESPACE "SMP_ARCH"  including contents and datafiles;
DROP TABLESPACE "SMP_LI_DEPY"  including contents and datafiles;
DROP TABLESPACE "SMP_SMALL"  including contents and datafiles;
DROP TABLESPACE "SMP_LOCATION"  including contents and datafiles;
DROP TABLESPACE "SMP_LOCATION_DTL"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_ADDR"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_CONTACT"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_CONT_PARM"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_ORDR"  including contents and datafiles;
DROP TABLESPACE "SMP_SO_ITEM"  including contents and datafiles;
DROP TABLESPACE "SMP_SO_ITEM_PARM"  including contents and datafiles;
DROP TABLESPACE "SMP_SO_PARM"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_PARM"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_SVC"  including contents and datafiles;
DROP TABLESPACE "SMP_SUB_SVC_ADDR"  including contents and datafiles;
DROP TABLESPACE "SMP_SS_ASSOC"  including contents and datafiles;
DROP TABLESPACE "SMP_SS_DPLAT"  including contents and datafiles;
DROP TABLESPACE "SMP_SSDEPY"  including contents and datafiles;
DROP TABLESPACE "SMP_SSN"  including contents and datafiles;
DROP TABLESPACE "SMP_SSP"  including contents and datafiles;
DROP TABLESPACE "SMP_SSP_DEPY"  including contents and datafiles;
DROP TABLESPACE "SMP_WRK_LOCK"  including contents and datafiles;
DROP TABLESPACE "JWF_CONTROL"  including contents and datafiles;
DROP TABLESPACE "JWF_CATTR"  including contents and datafiles;
DROP TABLESPACE "JWF_CORR_SET"  including contents and datafiles;
DROP TABLESPACE "JWF_CORR_SET_PROP"  including contents and datafiles;
DROP TABLESPACE "JWF_DEP_LINK"  including contents and datafiles;
DROP TABLESPACE "JWF_INPUT_PARMS"  including contents and datafiles;
DROP TABLESPACE "JWF_INST"  including contents and datafiles;
DROP TABLESPACE "JWF_JEA_TASK"  including contents and datafiles;
DROP TABLESPACE "JWF_MTASK"  including contents and datafiles;
DROP TABLESPACE "JWF_MT_LOG"  including contents and datafiles;
DROP TABLESPACE "JWF_MTASK_NOTE"  including contents and datafiles;
DROP TABLESPACE "JWF_MT_PARM"  including contents and datafiles;
DROP TABLESPACE "JWF_OUTPUT_PARMS"  including contents and datafiles;
DROP TABLESPACE "JWF_PDEF"  including contents and datafiles;
DROP TABLESPACE "JWF_SMALL"  including contents and datafiles;
DROP TABLESPACE "JWF_RINSTANCE"  including contents and datafiles;
DROP TABLESPACE "JWF_VAR"  including contents and datafiles;

spool off; 

exit;

