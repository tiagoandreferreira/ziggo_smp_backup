--============================================================================
--    $Id: dtv_views.sql,v 1.0 2014/03/26 17:18:09 Rujuta Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

CREATE OR REPLACE FORCE VIEW "V_CUST_SMART_CARD_SEARCH_QUERY" ("SUB_ID", "SUB_ORDR_ID", "CREATED_BY", "ORDR_STATUS", "SMART_CARD", "CREATED_DTM", "MODIFIED_DTM", "EXTERNAL_KEY") AS 
  SELECT
       distinct(o.sub_id),o.sub_ordr_id, o.created_by,
       s.status,
       a.smart_card,
       o.created_dtm, o.modified_dtm,o.external_key
  FROM sub_ordr o, ref_status s,
       (SELECT distinct SB.SUB_ID, SSP.VAL smart_card FROM PARM P, SUB_SVC SS, SUB_SVC_PARM SSP, SUB SB, SVC S
WHERE SB.SUB_ID = SS.SUB_ID
AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
AND P.PARM_ID = SSP.PARM_ID
AND S.SVC_ID = SS.SVC_ID
AND P.PARM_NM = 'id1'
AND S.SVC_NM = 'stb_cas'
AND ss.sub_svc_status_id not in (29)) a
where o.sub_id = a.sub_id and o.ORDR_STATUS_ID=s.status_id;