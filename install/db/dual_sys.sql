/*================================================================================================
#
#  FILE INFO
#
#    $Id: dual_sys.sql,v 1.2 2003/04/11 21:04:37 vadimg Exp $
#
#  DESCRIPTION
#
#    SMP makes heavy use of dual table. Replacing calls to sys.dual by sys.x$dual
#    improves database performance (CPU)
#
#  PARAMETERS
#    Must be executed as SYS database user.
#
#  RELATED SCRIPTS
#    Oracle version: 8i and higher
#    smp_tune_dual.sql - must be run after this script to pick up changes
#
#  REVISION HISTORY
#  * Based on CVS log
#================================================================================================*/

create view x_$dual as select dummy from x$dual;

grant select on x_$dual to public;
