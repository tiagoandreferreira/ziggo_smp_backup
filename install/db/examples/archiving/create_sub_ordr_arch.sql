/*=========================================================================

  FILE INFO

    $Id: create_sub_ordr_arch.sql,v 1.1 2003/11/13 17:13:52 vadimg Exp $

  DESCRIPTION

    PKG_ARCH implementation to archive SUB_ORDR older than 80 days


  NOTES
  Customize parameters
	'RETAIN'
	'ARCH_SIZE'
	'TGT_TABLESPACE'

  to fit your needs below


  REVISION HISTORY
  * Based on CVS log
========================================================================*/


exec pkg_arch.Prc_adm_auth(user);

exec pkg_arch.Prc_add_group('SUB_ORDR');

exec pkg_arch.Prc_add_tbl('SUB_ORDR', 'SUB_ORDR');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'OWNER', USER);

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'ROOT TABLE', 'SUB_ORDR');

exec pkg_arch.Prc_gen_depy('SUB_ORDR', 'SUB_ORDR', -1);

-- Set archiving criteria below
exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'RETAIN', 'created_dtm < sysdate - 80');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'ARCH_SIZE', 'REC=1000000');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'BATCH', '2000');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'TGT_TABLESPACE', 'USERS');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'ROOT ARCH_MODE', 'move');

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'NEXT_RUN', to_char(sysdate, 'DD.MM.YYYY HH24:MI:SS'));

exec pkg_arch.Prc_set_grp_parm('SUB_ORDR', 'STATUS', 'active');


/* Runtime usage example
exec pkg_arch.Prc_adm_auth(user);

exec pkg_arch.Prc_run_arch('SUB_ORDR');
*/