/*=========================================================================

  FILE INFO

    $Id: create_sub_rm_grp.sql,v 1.1 2003/11/13 17:19:58 vadimg Exp $

  DESCRIPTION

    PKG_ARCH implementation to purge selective SUB


  NOTES
  Customize parameters
	'RETAIN'

  to fit your needs below


  REVISION HISTORY
  * Based on CVS log
========================================================================*/

exec pkg_arch.Prc_adm_auth(user);

exec pkg_arch.Prc_add_group('SUB_RM');
exec pkg_arch.Prc_add_tbl('SUB_RM', 'SUB');
exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'OWNER', USER);
exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'ROOT TABLE', 'SUB');

exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'ROOT ARCH_MODE', 'purge');

exec pkg_arch.Prc_gen_depy('SUB_RM', 'SUB', -1);

exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'RETAIN', 'SUB_id in (select sub_id from sub2remove)');

exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'ARCH_SIZE', 'REC=1000000');
exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'BATCH', '1000');
exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'TGT_TABLESPACE', 'USERS');
exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'NEXT_RUN', to_char(sysdate, 'DD.MM.YYYY HH24:MI:SS'));

update arch_tbl_net set arch_mode = 'purge' where grp_id = (select grp_id from arch_grp where GRP_NM = 'SUB_RM');

exec pkg_arch.Prc_set_grp_parm('SUB_RM', 'STATUS', 'active');

create table sub2remove(
sub_id  number);


/* Runtime use example

< ... Populate sub2remove here ... >

create index sub_svc_addr_ix_tmp on sub_svc_addr(sub_addr_id);

exec pkg_arch.Prc_adm_auth(user);

--  actually remove SUBs
exec pkg_arch.Prc_run_arch('SUB_RM');

drop index sub_svc_addr_ix_tmp ;


*/