#=============================================================================
#
#  FILE INFO
#
#    $Id: get_DBVersion.sh,v 1.1 2003/04/08 21:35:51 vadimg Exp $
#
#  DESCRIPTION
#  Helper script to get db version
#
#=============================================================================
usage="Usage: $0 conn_str number_of_digits "

conn_str=${1:?$usage}
ver=${2:?$usage}

sqlplus -s $conn_str << eof
set heading off
set pagesize 0
select substr(banner, instr(banner, '.') - 1, instr(banner, '.', 1, $ver)  - instr(banner, '.') + 1) from v\$version where banner like 'Oracle%';
exit;
eof
