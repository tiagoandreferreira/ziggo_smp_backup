--=============================================================================
--    $Id: get_cm_pl_names.sql,v 1.3 2001/11/01 15:37:16 germans Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
select object_name 
  from user_objects
 where object_type in ('PROCEDURE','FUNCTION','VIEW','SEQUENCE');
