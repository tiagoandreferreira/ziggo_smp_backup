--=============================================================================
--    $Id: get_cm_pl_synonyms.sql,v 1.3 2001/11/01 15:37:16 germans Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
select 'create synonym '||object_name||' for '||owner||'.'||object_name||';' 
from all_objects where object_type in ('PROCEDURE','FUNCTION','VIEW','SEQUENCE')
and owner like '%CM';
