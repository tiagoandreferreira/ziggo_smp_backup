--=============================================================================
--    $Id: get_objects.sql,v 1.5 2008/02/14 19:43:03 cristiand Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
select 'drop '||object_type||' '||object_name||';' from user_objects
 where object_name not like 'BIN$%' and object_type not in ('TABLE','TYPE BODY','TYPE');
select 'drop '||object_type||' '||object_name||' FORCE ;' from user_objects
 where object_type in ('TYPE BODY','TYPE') and  object_name not like 'BIN$%';
select 'drop table '||object_name||' cascade constraints;' from user_objects
 where object_type = 'TABLE' and  object_name not like 'BIN$%';
