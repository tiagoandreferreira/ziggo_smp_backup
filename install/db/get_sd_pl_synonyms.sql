--=============================================================================
--    $Id: get_sd_pl_synonyms.sql,v 1.1 2001/09/05 01:58:38 germans Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
select 'create synonym '||object_name||' for '||owner||'.'||object_name||';' 
from all_objects where object_type = 'FUNCTION'
and owner like '%SD';
