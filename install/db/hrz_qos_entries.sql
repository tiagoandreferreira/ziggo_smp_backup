--============================================================================
-- $Id: hrz_qos_entries.sql,v 1.1 2014/11/17 divyap Exp $ 
--									 
--  REVISION HISTORY

--============================================================================
spool hrz_qos_entries.log

execute insert_bac_modem_mfg(81, 'SMT-G7400', 'samsung', 3.0, NULL, 0, 'SMT-G7400', 'Samsung Electronics Co., Ltd', NULL, NULL, NULL);

execute insert_DIN_QOS(301, 'HRZ_DTVONLY', 301);

execute INSERT_DIN_QOS_FLAVOR_FILTER(301,'diamond','FilterH');

execute INSERT_ALLOWED_MODEM_PROFILE('928', '81', '301');
 commit;
 

commit;
spool off;
exit;