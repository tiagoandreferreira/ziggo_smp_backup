#!/usr/bin/ksh
#=============================================================================i
#  FILE INFO
#    $Id: install_smp_db.sh,v 1.1 2011/12/06 15:31:07 prithvim Exp $
#
#  DESCRIPTION
#    Install the entire implementation.
#    1. create database tables, constraints and indexes for each user
#
#    This assumes the env_user.cfg is in the current directory.
#
#  RELATED SCRIPTS
#
#  REVISION HISTORY
#   * Based on CVS log
#==============================================================================

LOG_FILE=install_smp_db.log

echo "install_smp_db.sh Starting:"                                 > $LOG_FILE

#------------------------------------------------------------------------
# Load environment variables from env_user.cfg file. This will
# load up userids, SID, passwords etc.
#------------------------------------------------------------------------

#if [ -r ./env_user.cfg ]
#then
#    . ./env_user.cfg
#else
#    echo install_smp_db.sh: cannot find env_user.cfg file | tee -a $LOG_FILE
#    exit 1
#fi
. ../../util/setEnv.sh

#------------------------------------------------------------------------
# create database schemas
#------------------------------------------------------------------------

echo "install_smp_db.sh: Creating SMP database"         | tee -a $LOG_FILE
create_smp_db.sh  
#cat create_cm_db1.log >> $LOG_FILE

echo "install_smp_am_db.sh: Creating SMP/AM database"         | tee -a $LOG_FILE
create_smp_am_db.sh 
#cat create_am_db1.log >> $LOG_FILE

#echo "install_jasper_db.sh: Creating JASPER database"         | tee -a $LOG_FILE
#create_jasper_db.sh 

#-------------------------------------------------------------------------
# Verifying the result
#-------------------------------------------------------------------------

if egrep -s 'ERROR' $LOG_FILE
then
    echo Errors found. See \'$LOG_FILE\' for details
    exit 1
fi

if egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 1
fi

echo Done ........... |           tee -a $LOG_FILE
