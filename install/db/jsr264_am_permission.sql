--============================================================================
--    $Id: jsr264_am_permission.sql,v 1.1 2012/05/03 07:49:28 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

exec am_add_grp('jsr264_ws' , 'JSR 264 group');

exec am_add_secure_obj('com.sigma.samp.ordermgr.jsr264', 'JSR264 operation');

exec am_add_privilege('com.sigma.samp.ordermgr.jsr264', 'n', 'jsr264_ws', 'execute');

exec am_add_grp_grp('csr_admin','jsr264_ws');
exec am_add_grp_grp('csr_supervisor','jsr264_ws');

exec am_add_privilege('com.sigma.samp.ordermgr.jsr264', 'y', 'ziggo_tsr', 'execute');
exec am_add_privilege('com.sigma.samp.ordermgr.jsr264', 'y', 'ziggo_tsr_ro', 'execute');

commit;
