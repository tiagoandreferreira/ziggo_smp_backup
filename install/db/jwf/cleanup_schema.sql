--=============================================================================
--    $Id: cleanup_schema.sql,v 1.1 2006/10/05 17:49:48 dant Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
set termout off
set heading off
set linesize 133
set pagesize 0
set feedback off
spool cleanup.sql
@get_objects.sql
spool off
@cleanup
exit
