#=============================================================================
#
#  FILE INFO
#
#    $Id: create_jwf_db.sh,v 1.6.8.1 2009/08/31 19:32:57 atulg Exp $
#
#  DESCRIPTION
#
#    Builds and executes WorkFlow SQl files for creating tables, primary key,
#    foreign keys, indexes, private synonyms,
#    and granting select and references privileges
#
#    This script assumes env was loaded by caller with the proper
#    environment settings.
#
#  PARAMETERS
#
#
#  RELATED SCRIPTS
#
#   create_schema.awk - To generate creation table syntax, primary key syntax,
#                       foreign key syntax as well as create index syntax
#   create_grants.awk - To generate grant select to Oracle's roles and grant
#                       references to Oracle's users
#
#  REVISION HISTORY
#  * Based on CVS log
#=============================================================================



LOG_FILE=create_jwf_db.log
echo "create_jwf_db.sh Starting:"         > $LOG_FILE

#-------------------------------------------------------------------------
# Preparing all variables for later use
#-------------------------------------------------------------------------

#NAME=$JWF_DB_USER_ROOT"sb"
#PASS=$JWF_DB_PASSWORD"@"$JWF_DB
NAME=$SAMP_DB_USER_ROOT"cm"
PASS=$SAMP_DB_PASSWORD"@"$SAMP_DB


if [ "$ENV_TYPE" = "TEST" ]
 then
     TABLE_LIST_JWF=table_list.jwf.test
     INDEX_LIST_JWF=index_list.jwf.test
     echo ""
 else
     TABLE_LIST_JWF=table_list.jwf.prod
     INDEX_LIST_JWF=index_list.jwf.prod
fi

JWF_SCHEMA=JWF_SCHEMA_FILE.SQL
JWF_CREATE=Y    
export NAME PASS LOG_FILE JWF_SCHEMA JWF_CREATE


#-------------------------------------------------------------------------
# Building SQL file
#-------------------------------------------------------------------------



     echo Building file $JWF_SCHEMA for $NAME schema       |   tee -a $LOG_FILE

     echo Cleaning $NAME Schema

#      sqlplus  ${NAME}/${PASS} @cleanup_schema.sql           >> $LOG_FILE

     echo creating tables ddl for $NAME schema

     echo set echo on > $JWF_SCHEMA
   
     if [ "$ENV_TYPE" = "TEST" ]
     then 
       nawk -v ListFile=$TABLE_LIST_JWF   -f create_schema.awk jwf_tables.ddl \
          >> $JWF_SCHEMA
    
       echo creating ddl for indexes for $NAME Schema

       nawk -v ListFile=$INDEX_LIST_JWF   -f create_indexes.awk jwf_indexes.ddl\
          >> $JWF_SCHEMA

      # cat jwf_indexes.ddl  >> $JWF_SCHEMA
     else
       nawk -v ListFile=$TABLE_LIST_JWF   -f create_schema.awk jwf_tables.ddl \
          >> $JWF_SCHEMA

       echo creating ddl for indexes for $NAME Schema

       nawk -v ListFile=$INDEX_LIST_JWF   -f create_indexes.awk jwf_indexes.ddl\
          >> $JWF_SCHEMA
     fi


     cat jwf_others.ddl   >> $JWF_SCHEMA

     cat jwf_tune.sql   >> $JWF_SCHEMA

     echo exit   >> $JWF_SCHEMA

     echo execute JWF DDL in $NAME schema

     sqlplus ${NAME}/${PASS} @$JWF_SCHEMA     >> $LOG_FILE

     echo Creating stored procedures for $NAME schema  \
          | tee -a $LOG_FILE

     echo set echo on > $JWF_SCHEMA.plsql.sql

     cat nextkey.sql >> $JWF_SCHEMA.plsql.sql

     echo exit            >> $JWF_SCHEMA.plsql.sql
     sqlplus ${NAME}/${PASS} @$JWF_SCHEMA.plsql.sql     >> $LOG_FILE

     echo Creating reference data for $NAME schema  \
          | tee -a $LOG_FILE

     echo set echo on > $JWF_SCHEMA.dat.sql

     cat jwf_views.sql >> $JWF_SCHEMA.dat.sql

     cat jwf_ref_data.sql >> $JWF_SCHEMA.dat.sql
     cat update_server_nm.sql >> $JWF_SCHEMA.dat.sql

     echo exit            >> $JWF_SCHEMA.dat.sql
     sqlplus ${NAME}/${PASS} @$JWF_SCHEMA.dat.sql     >> $LOG_FILE

#-------------------------------------------------------------------------
# Verifying the result
#-------------------------------------------------------------------------

if egrep -s 'ORA-' $LOG_FILE
then
    echo ....... Errors found. See \'$LOG_FILE\' for details
    exit 1
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 1
elif egrep -s "ORA-" $LOG_FILE
then
   echo JWF Database  created.
   echo ORA- messages found. Please review \'$LOG_FILE\'
   exit 0
else
   echo JWF Database successfully created. |        tee -a $LOG_FILE
   exit 0
fi
