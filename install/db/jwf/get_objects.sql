--=============================================================================
--    $Id: get_objects.sql,v 1.1 2006/10/05 17:50:07 dant Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
select 'drop '||object_type||' '||object_name||';' from user_objects
 where object_type not in ('TABLE','TYPE BODY','TYPE');
select 'drop '||object_type||' '||object_name||' FORCE ;' from user_objects
 where object_type in ('TYPE BODY','TYPE');
select 'drop table '||object_name||' cascade constraints;' from user_objects
 where object_type = 'TABLE';
