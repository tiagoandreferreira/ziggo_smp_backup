--=============================================================================
--    $Id: jwf_ref_data.sql,v 1.25 2008/05/30 13:37:19 vasyly Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
INSERT INTO REF_CONTROL_STATE(
       CONTROL_STATE_ID 
       , CONTROL_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       1
       ,'new'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
       
INSERT INTO REF_CONTROL_STATE(
       CONTROL_STATE_ID 
       , CONTROL_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       2
       ,'ready'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
       
INSERT INTO REF_CONTROL_STATE(
       CONTROL_STATE_ID 
       , CONTROL_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       3
       ,'waiting'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATE(
       CONTROL_STATE_ID 
       , CONTROL_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       4
       ,'completed'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATE(
       CONTROL_STATE_ID 
       , CONTROL_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       5
       ,'skipped'
       ,SYSDATE
       ,SYSDATE
       ,'init');       


INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       1
       ,'success'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       2
       ,'failure'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
       
INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       3
       ,'terminated'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
      
INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       4
       ,'compensated'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       5
       ,'incomplete'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       6
       ,'suspended'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       7
       ,'cancelled'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       8
       ,'timeout'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       9
       ,'compensating'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       10
       ,'cancelling'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_CONTROL_STATUS(
       CONTROL_STATUS_ID 
       , CONTROL_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       11
       ,'not_created'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATE(
       INSTANCE_STATE_ID 
       , INSTANCE_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       1
       ,'new'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
       
INSERT INTO REF_INSTANCE_STATE(
       INSTANCE_STATE_ID 
       , INSTANCE_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       2
       ,'ready'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
       
INSERT INTO REF_INSTANCE_STATE(
       INSTANCE_STATE_ID 
       , INSTANCE_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       3
       ,'waiting'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATE(
       INSTANCE_STATE_ID 
       , INSTANCE_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       4
       ,'completed'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATE(
       INSTANCE_STATE_ID 
       , INSTANCE_STATE_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       5
       ,'skipped'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       1
       ,'success'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       2
       ,'failure'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
       
INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       3
       ,'terminated'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
      
INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       4
       ,'compensated'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       5
       ,'incomplete'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       6
       ,'suspended'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
    
INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       7
       ,'cancelled'
       ,SYSDATE
       ,SYSDATE
       ,'init');       
    
INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       8
       ,'timeout'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       9
       ,'compensating'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

INSERT INTO REF_INSTANCE_STATUS(
       INSTANCE_STATUS_ID 
       , INSTANCE_STATUS_NM
       , CREATED_DTM
       , MODIFIED_DTM
       , LAST_MODIFIED_BY
       ) VALUES (
       10
       ,'cancelling'
       ,SYSDATE
       ,SYSDATE
       ,'init');       

    
INSERT INTO REF_CONTROL_TYPE (
      CONTROL_TYPE_ID
    , CONTROL_TYPE_NM
    , CREATED_DTM
    , MODIFIED_DTM
    , LAST_MODIFIED_BY
    ) VALUES (
    2,
    'composite control',
    SYSDATE,
    SYSDATE,
    'init' ) ;
      
INSERT INTO REF_CONTROL_TYPE (
      CONTROL_TYPE_ID
    , CONTROL_TYPE_NM
    , CREATED_DTM
    , MODIFIED_DTM
    , LAST_MODIFIED_BY
    ) VALUES (
    1,
    'control',
    SYSDATE,
    SYSDATE,
    'init' ) ;
    
INSERT INTO REF_CONTROL_TYPE (
      CONTROL_TYPE_ID
    , CONTROL_TYPE_NM
    , CREATED_DTM
    , MODIFIED_DTM
    , LAST_MODIFIED_BY
    ) VALUES (
    3,
    'process',
    SYSDATE,
    SYSDATE,
    'init' ) ;


INSERT INTO REF_ATTR (ATTR_ID,ATTR_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY ) VALUES (1, 'groupID', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 2, 'fault', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 3, 'lateStatus', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 4, 'activeManualTaskCount', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 5, 'compensationContext', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 6, 'noCancel', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 7, 'err_code', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 8, 'err_reason', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 9, 'is_transactional', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 10, 'priority', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 11, 'clientId', SYSDATE, SYSDATE, 'INIT' );
INSERT INTO REF_ATTR (ATTR_ID, ATTR_NM, CREATED_DTM, MODIFIED_DTM, LAST_MODIFIED_BY ) VALUES ( 12, 'hasFailedTask', SYSDATE, SYSDATE, 'INIT' );

INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'process_def_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'security_resource_nm',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'tcName',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'boxNm', SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'srcCmpntName',  SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'destCmpntName',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'smpOrderId',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'statusName',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'samp_orig_sub_svc_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'samp_orig_svc_action_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'taskTypeNm',SYSDATE,SYSDATE,'Init');

INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'task_format',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'jea_type_name',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'box_nm',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'is_transactional',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'is_oneway',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'src_cmpnt_nm',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'dest_cmpnt_nm',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'smp_order_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'priority',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'tsk_type_nm',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'owner_cmpnt_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'return_code',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'return_code_message',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'root_instance_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'instance_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'control_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'syntax_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'version_id',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'task_key',SYSDATE,SYSDATE,'Init');
INSERT INTO REF_PARM(PARM_ID,PARM_NM,CREATED_DTM,MODIFIED_DTM,LAST_MODIFIED_BY) VALUES (nextKey('PARM_ID'),'status',SYSDATE,SYSDATE,'Init');


insert into ref_late_status(late_status_id,late_status_nm,created_dtm, modified_dtm, last_modified_by) 
     values(0, 'normal', sysdate, sysdate, 'installer');
insert into ref_late_status(late_status_id,late_status_nm,created_dtm, modified_dtm, last_modified_by) 
     values(1, 'jeopardy', sysdate, sysdate, 'installer');
insert into ref_late_status(late_status_id,late_status_nm,created_dtm, modified_dtm, last_modified_by) 
     values(2, 'overdue', sysdate, sysdate, 'installer');

-- populate JEA task status IDs

insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(0, 'success', sysdate, sysdate, 'installer');
insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(1, 'failed', sysdate, sysdate, 'installer');
insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(2, 'incomplete', sysdate, sysdate, 'installer');
insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(3, 'cancelled', sysdate, sysdate, 'installer');
insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(4, 'unavailable', sysdate, sysdate, 'installer');
insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(5, 'invalid', sysdate, sysdate, 'installer');
insert into ref_jea_task_status(task_status_id,task_status_nm,created_dtm, modified_dtm, last_modified_by) values(6, 'duplicate', sysdate, sysdate, 'installer');

-- popluate JEA task state IDs

insert into ref_jea_task_state(task_state_id,task_state_nm,created_dtm, modified_dtm, last_modified_by) values(4, 'created', sysdate, sysdate, 'installer');
insert into ref_jea_task_state(task_state_id,task_state_nm,created_dtm, modified_dtm, last_modified_by) values(3, 'waitForReply', sysdate, sysdate, 'installer');
insert into ref_jea_task_state(task_state_id,task_state_nm,created_dtm, modified_dtm, last_modified_by) values(7, 'readyForReply', sysdate, sysdate, 'installer');
insert into ref_jea_task_state(task_state_id,task_state_nm,created_dtm, modified_dtm, last_modified_by) values(5, 'done', sysdate, sysdate, 'installer');

insert into process_def(
        process_def_id,
        DEFINITION ,
        tc_name,
        tc_version,
        process_name,
        process_version,
        description,
        SMP_VERSION,
        SOURCE,
        CREATED_DTM)
VALUES (
        PROCESS_DEF_ID_SEQ.nextval,
        empty_blob(),
        'Non-Persistent',
        1,
        'Non-Persistent',
        1,
        'Dummy',
        '4.0',
        'DB',
        SYSDATE);

--===============================================================
-- Test data that maybe required by engine
--===============================================================

-- INSERT INTO REF_CMPNT ( CMPNT_ID , CMPNT_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY) VALUES (
--         nextKey('cmpnt_id'), 'test_component', SYSDATE, SYSDATE, 'init');       

-- INSERT INTO REF_CMPNT ( CMPNT_ID , CMPNT_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY) VALUES (
--  nextKey('cmpnt_id'), 'sbengine1', SYSDATE, SYSDATE, 'init');       

--INSERT INTO REF_CMPNT_INSTANCE ( CMPNT_ID , CMPNT_INSTANCE_ID , SERVER_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY
--     ) VALUES( (select cmpnt_id from ref_cmpnt where cmpnt_nm='test_component') ,nextKey('CMPNT_INSTANCE_ID') ,'test_server' ,SYSDATE ,SYSDATE ,'init');

--INSERT INTO REF_CMPNT_INSTANCE ( CMPNT_ID , CMPNT_INSTANCE_ID , SERVER_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY) VALUES(
--     (select cmpnt_id from ref_cmpnt where cmpnt_nm='sbengine1') ,nextKey('CMPNT_INSTANCE_ID') ,'node1' ,SYSDATE ,SYSDATE ,'init');

--INSERT INTO REF_CMPNT_INSTANCE ( CMPNT_ID , CMPNT_INSTANCE_ID , SERVER_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY) VALUES(
--     (select cmpnt_id from ref_cmpnt where cmpnt_nm='sbengine1') ,nextKey('CMPNT_INSTANCE_ID') ,'node2' ,SYSDATE ,SYSDATE ,'init');

--INSERT INTO REF_CMPNT_INSTANCE ( CMPNT_ID , CMPNT_INSTANCE_ID , SERVER_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY) VALUES(
--     (select cmpnt_id from ref_cmpnt where cmpnt_nm='sbengine1') ,nextKey('CMPNT_INSTANCE_ID') ,'admin_svr' ,SYSDATE ,SYSDATE ,'init');

--insert into ref_cmpnt(cmpnt_id,cmpnt_nm,created_dtm,modified_dtm,last_modified_by) 
--values(nextKey('CMPNT_ID'), 'genericJEA', sysdate, sysdate, 'init');
