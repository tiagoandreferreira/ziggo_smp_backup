--#==============================================================================
--#  $Id: jwf_tune.sql,v 1.1.6.2.2.2.2.2 2011/04/26 08:44:48 pankajgu Exp $
--#==============================================================================


ALTER TABLE CONTROL INITRANS 20 ;
ALTER TABLE VARIABLE INITRANS 20 ;
ALTER TABLE DEPENDENCY_LINK INITRANS 20 ;
ALTER TABLE INPUT_PARMS INITRANS 20 ;
ALTER TABLE INSTANCE INITRANS 20 ;
ALTER TABLE JEA_TASK INITRANS 20 ;
ALTER TABLE JEA_TASK_PARM INITRANS 20 ;
ALTER TABLE JEA_TASK_ALT_KEY INITRANS 20 ;
ALTER TABLE OUTPUT_PARMS INITRANS 20 ;
ALTER TABLE PROCESS_DEF INITRANS 20 ;
ALTER TABLE ROOT_INSTANCE INITRANS 20 ;

ALTER INDEX INDEX1_INSTANCE INITRANS 40 ;
ALTER INDEX DEPENDENCY_LINK_PK INITRANS 40 ;
ALTER INDEX ROOT_INSTANCE_PK INITRANS 40 ;
ALTER INDEX PRCSDEF_UNIQ_NAME_VER INITRANS 40 ;
ALTER INDEX PROCESS_DEF_PK INITRANS 40 ;
ALTER INDEX VARIABLE_PK INITRANS 40 ;
ALTER INDEX OUTPUT_PARMS_PK INITRANS 40 ;
ALTER INDEX CONTROL_PK INITRANS 40 ;
ALTER INDEX INPUT_PARMS_PK INITRANS 40 ;
ALTER INDEX XIF9INSTANCE INITRANS 40 ;
ALTER INDEX INSTANCE_PK INITRANS 40 ;
ALTER INDEX XIF270JEA_TASK INITRANS 40 ;
ALTER INDEX JEA_TASK_ALT_KEY_PK INITRANS 40 ;
ALTER INDEX JEA_TASK_ALT_KEY_IX1 INITRANS 40 ;
ALTER INDEX XIF92JEA_TASK INITRANS 40 ;
ALTER INDEX XIF93JEA_TASK INITRANS 40 ;
ALTER INDEX JEA_TASK_PK INITRANS 40 ;
ALTER INDEX JEA_TASK_PARM_PK INITRANS 40 ;

