--=============================================================================
--    $Id: jwf_views.sql,v 1.14.12.1 2010/11/04 16:12:54 vasyly Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

-- View to select all root instances
CREATE OR REPLACE VIEW V_ROOT_INSTANCES AS 
SELECT * from instance where parent_instance_id is null;
/

-- Select all root instanes with a status string so that query can use a pattern like.
CREATE OR REPLACE VIEW V_ROOT_INSTANCE_STR_STATUS AS
        SELECT 
                RI.INSTANCE_ID, 
                RI.SMP_ORDR_ID,
                RI.INSTANCE_STATE_ID,
                RIS.INSTANCE_STATE_NM STATE_NM,
                RI.INSTANCE_STATUS_ID,
                RST.INSTANCE_STATUS_NM STATUS_NM,
                RI.PROCESS_DEF_ID, 
                RI.PARENT_INSTANCE_ID,
                RI.CREATED_DTM, 
                RI.MODIFIED_DTM,
                RI.LAST_MODIFIED_BY,
                RI.MODIFIED_MSG
        FROM INSTANCE RI, REF_INSTANCE_STATE RIS, REF_INSTANCE_STATUS RST
        WHERE RI.PARENT_INSTANCE_ID IS NULL AND 
                RST.INSTANCE_STATUS_ID = RI.INSTANCE_STATUS_ID AND 
                RIS.INSTANCE_STATE_ID = RI.INSTANCE_STATE_ID;
/

-- Select all instances with process definition information.
-- Used in named queries.
CREATE OR REPLACE VIEW V_ROOT_INSTANCE_DEF AS
        SELECT 
                RI.INSTANCE_ID, 
                RI.SMP_ORDR_ID,
                RI.INSTANCE_STATE_ID,
                RIS.INSTANCE_STATE_NM STATE_NM,
                RI.INSTANCE_STATUS_ID,
                RST.INSTANCE_STATUS_NM STATUS_NM,
                RI.PROCESS_DEF_ID,
                D.PROCESS_NAME,
                D.PROCESS_VERSION,
                D.TC_NAME,
                D.TC_VERSION,
                RI.PARENT_INSTANCE_ID,
                RI.CREATED_DTM, 
                RI.MODIFIED_DTM,
                RI.LAST_MODIFIED_BY,
                RI.MODIFIED_MSG
        FROM INSTANCE RI, REF_INSTANCE_STATE RIS, REF_INSTANCE_STATUS RST, PROCESS_DEF D, INSTANCE I
        WHERE RI.PARENT_INSTANCE_ID IS NULL AND 
                RST.INSTANCE_STATUS_ID = RI.INSTANCE_STATUS_ID AND 
                RIS.INSTANCE_STATE_ID = RI.INSTANCE_STATE_ID AND
                RI.INSTANCE_ID = I.ROOT_INSTANCE_ID AND
                I.PROCESS_DEF_ID = D.PROCESS_DEF_ID;
/

-- Select all instances with process definition information.
-- Used in named queries to select .
CREATE OR REPLACE VIEW V_ROOT_INSTANCE_FT_DEF AS
        SELECT 
                RI.INSTANCE_ID, 
                RI.SMP_ORDR_ID,
                RI.INSTANCE_STATE_ID,
                RIS.INSTANCE_STATE_NM STATE_NM,
                RI.INSTANCE_STATUS_ID,
                RST.INSTANCE_STATUS_NM STATUS_NM,
                RI.PROCESS_DEF_ID,
                D.PROCESS_NAME,
                D.PROCESS_VERSION,
                D.TC_NAME,
                D.TC_VERSION,
                RI.PARENT_INSTANCE_ID,
                RI.CREATED_DTM, 
                RI.MODIFIED_DTM,
                RI.LAST_MODIFIED_BY,
                RI.MODIFIED_MSG,
                IIS.INSTANCE_STATE_NM INS_STATE_NM,
                IST.INSTANCE_STATUS_NM INS_STATUS_NM
        FROM 
                INSTANCE RI, REF_INSTANCE_STATE RIS, REF_INSTANCE_STATUS RST, PROCESS_DEF D, INSTANCE I, 
                CONTROL_ATTR CA, REF_ATTR RA,
                REF_INSTANCE_STATE IIS, REF_INSTANCE_STATUS IST
        WHERE RI.PARENT_INSTANCE_ID IS NULL AND 
                RST.INSTANCE_STATUS_ID = RI.INSTANCE_STATUS_ID AND 
                RIS.INSTANCE_STATE_ID = RI.INSTANCE_STATE_ID AND
                RI.INSTANCE_ID = I.ROOT_INSTANCE_ID AND
                I.PROCESS_DEF_ID = D.PROCESS_DEF_ID AND
                IST.INSTANCE_STATUS_ID = I.INSTANCE_STATUS_ID AND 
                IIS.INSTANCE_STATE_ID = I.INSTANCE_STATE_ID AND
                I.INSTANCE_ID = CA.CONTROL_ID AND
                RA.ATTR_ID = CA.ATTR_ID AND
                RA.ATTR_NM = 'hasFailedTask';

/


-- Helper view to view processes with simbolic status names linked 
-- to process defiition
CREATE OR REPLACE VIEW V_PROCESS_2_DEF AS
        SELECT I.INSTANCE_ID
        ,I.PARENT_INSTANCE_ID
        ,I.SMP_ORDR_ID
        ,I.PROCESS_DEF_ID
        ,D.PROCESS_NAME
        ,D.PROCESS_VERSION
        ,D.TC_NAME
        ,D.TC_VERSION   
        FROM INSTANCE I, PROCESS_DEF D
        WHERE I.PROCESS_DEF_ID = D.PROCESS_DEF_ID;
/

CREATE OR REPLACE VIEW V_ROOT_INSTANCE_IN_PARM AS
        SELECT 
                RI.INSTANCE_ID, 
                RI.SMP_ORDR_ID,
                RI.INSTANCE_STATE_ID,
                RIS.INSTANCE_STATE_NM STATE_NM,
                RI.INSTANCE_STATUS_ID,
                RST.INSTANCE_STATUS_NM STATUS_NM,
                RI.PROCESS_DEF_ID, 
                RI.PARENT_INSTANCE_ID,
                RI.CREATED_DTM, 
                RI.MODIFIED_DTM,
                RI.LAST_MODIFIED_BY,
                RI.MODIFIED_MSG,
                RPI.PARM_NM PARM_NM,
                IP.PARM_VALUE,
                IPT.PARM_VALUE TASK_TYPE,
                CTRL.CREATED_DTM TASK_CREATED_DTM,
                CTRL.MODIFIED_DTM TASK_MODIFIED_DTM,
                CIS.CONTROL_STATE_NM TASK_STATE_NM,
                CST.CONTROL_STATUS_NM TASK_STATUS_NM
        FROM CONTROL CTRL,REF_CONTROL_STATUS CST,REF_CONTROL_STATE CIS,INSTANCE RI, REF_INSTANCE_STATE RIS, REF_INSTANCE_STATUS RST, INPUT_PARMS IP, REF_PARM RPI,INPUT_PARMS IPT, REF_PARM RFPT
        WHERE RI.PARENT_INSTANCE_ID IS NULL AND 
                RST.INSTANCE_STATUS_ID = RI.INSTANCE_STATUS_ID AND 
                RIS.INSTANCE_STATE_ID = RI.INSTANCE_STATE_ID AND 
                IP.ROOT_INSTANCE_ID = RI.INSTANCE_ID AND RPI.PARM_ID = IP.PARM_ID AND
                IPT.CONTROL_ID = IP.CONTROL_ID AND IPT.PARM_ID=RFPT.PARM_ID AND RFPT.PARM_NM='taskTypeNm'
                AND RI.INSTANCE_ID = IPT.ROOT_INSTANCE_ID
                AND CTRL.CONTROL_ID=IPT.CONTROL_ID AND
                CST.CONTROL_STATUS_ID = CTRL.CONTROL_STATUS_ID AND 
                CIS.CONTROL_STATE_ID = CTRL.CONTROL_STATE_ID; 
/

CREATE OR REPLACE VIEW V_ROOT_INSTANCE_OUT_PARM AS
        SELECT 
                RI.INSTANCE_ID, 
                RI.SMP_ORDR_ID,
                RI.INSTANCE_STATE_ID,
                RIS.INSTANCE_STATE_NM STATE_NM,
                RI.INSTANCE_STATUS_ID,
                RST.INSTANCE_STATUS_NM STATUS_NM,
                RI.PROCESS_DEF_ID, 
                RI.PARENT_INSTANCE_ID,
                RI.CREATED_DTM, 
                RI.MODIFIED_DTM,
                RI.LAST_MODIFIED_BY,
                RI.MODIFIED_MSG,
                RPI.PARM_NM PARM_NM,
                IP.PARM_VALUE,
                IPT.PARM_VALUE TASK_TYPE,
                CTRL.CREATED_DTM TASK_CREATED_DTM,
                CTRL.MODIFIED_DTM TASK_MODIFIED_DTM,
                CIS.CONTROL_STATE_NM TASK_STATE_NM,
                CST.CONTROL_STATUS_NM TASK_STATUS_NM
        FROM CONTROL CTRL,REF_CONTROL_STATUS CST,REF_CONTROL_STATE CIS,INSTANCE RI, REF_INSTANCE_STATE RIS, REF_INSTANCE_STATUS RST, OUTPUT_PARMS IP, REF_PARM RPI,INPUT_PARMS IPT, REF_PARM RFPT
        WHERE RI.PARENT_INSTANCE_ID IS NULL AND 
                RST.INSTANCE_STATUS_ID = RI.INSTANCE_STATUS_ID AND 
                RIS.INSTANCE_STATE_ID = RI.INSTANCE_STATE_ID AND 
                IP.ROOT_INSTANCE_ID = RI.INSTANCE_ID AND RPI.PARM_ID = IP.PARM_ID AND
                IPT.CONTROL_ID = IP.CONTROL_ID AND IPT.PARM_ID=RFPT.PARM_ID AND RFPT.PARM_NM='taskTypeNm'
                AND IP.ROOT_INSTANCE_ID = RI.INSTANCE_ID 
                AND CTRL.CONTROL_ID=IPT.CONTROL_ID AND
                CST.CONTROL_STATUS_ID = CTRL.CONTROL_STATUS_ID AND 
                CIS.CONTROL_STATE_ID = CTRL.CONTROL_STATE_ID; 
/

-- Start of DDL Script for View V_MANUAL_TASK_SEARCH

CREATE OR REPLACE VIEW V_MANUAL_TASK_SEARCH 
(
        CONTROL_ID
        ,SMP_ORDR_ID
        ,OWNER_NM
        ,CREATED_DTM
        ,ROOT_INSTANCE_ID
        ,TASK_TYPE_ID
        ,LATE_STATUS_ID
        ,LATE_STATUS_NM
        ,TASK_TYPE_NM
        ,PROCESS_NM
        ,TC_NM,DESCRIPTION
        ,SRC_CMPNT_NM
        ,SYNTAX_ID
        ,INSTANCE_ID
        ,SECURITY_RESOURCE_NM
        ,PARM_ID
        ,PARM_VALUE
)
AS SELECT 
        DISTINCT MT.CONTROL_ID
        ,MT.SMP_ORDR_ID
        ,MT.OWNER_NM
        ,MT.CREATED_DTM
        ,MT.ROOT_INSTANCE_ID
        ,MT.TASK_TYPE_ID
        ,MT.LATE_STATUS_ID
        ,RS.LATE_STATUS_NM
        ,RT.TASK_TYPE_NM
        ,PD.PROCESS_NAME
        ,MT.TC_NAME
        ,PD.DESCRIPTION 
        ,MT.SRC_CMPNT_NM
        ,MT.SYNTAX_ID
        ,MT.INSTANCE_ID
        ,MT.SECURITY_RESOURCE_NM
        ,IP.PARM_ID,IP.PARM_VALUE
FROM
         MANUAL_TASK MT
        ,REF_TASK_TYPE RT
        ,REF_LATE_STATUS RS
        ,INPUT_PARMS IP
        ,PROCESS_DEF PD
        ,CONTROL CTRL
WHERE
        MT.CONTROL_ID=IP.CONTROL_ID
        and MT.ROOT_INSTANCE_ID=IP.ROOT_INSTANCE_ID
        and MT.TASK_TYPE_ID=RT.TASK_TYPE_ID
        and MT.PROCESS_DEF_ID=PD.PROCESS_DEF_ID
        and MT.LATE_STATUS_ID=RS.LATE_STATUS_ID
        and MT.CONTROL_ID=CTRL.CONTROL_ID
        and CTRL.CONTROL_STATE_ID != 4;
/
-- End of DDL Script for View V_MANUAL_TASK_SEARCH

