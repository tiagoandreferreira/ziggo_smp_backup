--=============================================================================
--    $Id: nextkey.sql,v 1.23 2007/10/05 13:46:30 handongw Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
CREATE OR REPLACE FUNCTION nextKey (keyName varchar2)
RETURN INTEGER IS
cur       INTEGER;
     rows      INTEGER;
     val       INTEGER;
BEGIN
     cur := dbms_sql.open_cursor;
     dbms_sql.parse (cur, 'SELECT to_char(' || keyName ||
                     '_SEQ.NEXTVAL) FROM DUAL', dbms_sql.native);
     dbms_sql.define_column (cur, 1, val);
     rows := dbms_sql.execute (cur);

     IF (dbms_sql.fetch_rows (cur) > 0) THEN
         dbms_sql.column_value (cur, 1, val);
     ELSE
         RAISE NO_DATA_FOUND;
     END IF;

     dbms_sql.close_cursor (cur);
     RETURN val;
END nextKey;
/
