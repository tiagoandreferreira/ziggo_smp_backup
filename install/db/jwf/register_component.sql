--=============================================================================
--    $Id: register_component.sql,v 1.1 2006/10/23 21:09:11 vasyly Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

-- Add information about component installation to database.
--  	Parameters required: ${component_name}, ${server_name}
-- Script first register component in REF_CMPNT table and than in REF_CMPNT_INSTANCE

INSERT INTO REF_CMPNT ( CMPNT_ID , CMPNT_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY) VALUES (
 	   nextKey('cmpnt_id'), '${component_name}', SYSDATE, SYSDATE, 'init');	   

INSERT INTO REF_CMPNT_INSTANCE ( CMPNT_ID , CMPNT_INSTANCE_ID , SERVER_NM , CREATED_DTM , MODIFIED_DTM , LAST_MODIFIED_BY
	   ) VALUES( (select cmpnt_id from ref_cmpnt where cmpnt_nm='${component_name}') ,nextKey('CMPNT_INSTANCE_ID') ,'${server_name}' ,SYSDATE ,SYSDATE ,'init');
	   
	   