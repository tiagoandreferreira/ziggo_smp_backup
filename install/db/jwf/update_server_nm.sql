--===============================================================
-- FILE INFO
-- $Id: update_server_nm.sql,v 1.1 2006/10/20 04:46:53 vasyly Exp $
--
-- REVISION HISTORY
-- * Based on CVS log
--===============================================================

-- For now just set the admin server name
update REF_CMPNT_INSTANCE set SERVER_NM = '@smp.server@';
commit;
