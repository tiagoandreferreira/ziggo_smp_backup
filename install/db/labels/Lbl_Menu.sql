--============================================================================
--    $Id: Lbl_Menu.sql,v 1.3 2002/06/13 17:15:02 davidc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Delete for table: Lbl Class: 5   ---------
------------------------------------------------------------------
Delete from Lbl where class_id = '5' ;
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 5   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 5   ---------')
BEGIN
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'no_results_found',
p_locale_cd => 'en_CA',
p_lbl_txt => 'No Results Found',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'no_results_found',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - No Results Found',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'search_subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Search Subscriber',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'subscriber_details',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Details',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'diagnostic_history',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Diagnostic History',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_results',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Results',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_phase',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Phase',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => 5,
p_obj_nm => 'create_ticket',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Create Ticket',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'diagnostic_manager',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Diagnostic Manager',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'logout',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Logout',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'logged_in',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Logged In',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'enter_search_criteria',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Enter Search Criteria',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'select_subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Select Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'subscriber_information',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Information',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_run',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Run',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'start_date',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Start Date',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'end_date',
p_locale_cd => 'en_CA',
p_lbl_txt => 'End Date',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Status',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'user_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'User ID',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Progress',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'display_format',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Display Format',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'brief_list',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Brief List',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'detailed_list',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Detailed List',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'view_results',
p_locale_cd => 'en_CA',
p_lbl_txt => 'View Results',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'close_window',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Close Window',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'anchor_window',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Anchor Window',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'service_advisories',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Advisories',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'trouble_tickets',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Trouble Tickets',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'open_trouble_ticket',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open Trouble Ticket',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'next',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Next',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'open_ticket',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open Ticket',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'error_message',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error Message',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'error_get_sub',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error Getting Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'close_window_msg',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Click here to close this window',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'loading',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Loading, please wait.',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_result_msg',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Result Message',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'no_tests_found',
p_locale_cd => 'en_CA',
p_lbl_txt => 'No Tests Found',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'ticket_number',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Ticket Number',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'trouble_ticket_created',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Trouble Ticket Created',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'find_subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Find Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'clear',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Clear',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'call_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Call ID',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'call_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Call ID',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'clear',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Clear',
p_created_by => 'INIT');


Add_Lbl(
p_class_id => 5,
p_obj_nm => 'find_subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Find Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'trouble_ticket_created',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Trouble Ticket Created',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'ticket_number',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Ticket Number',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'no_tests_found',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - No Tests Found',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_result_msg',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Test Result Message',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'loading',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Loading, please wait.',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'close_window_msg',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Click here to close this window',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'error_get_sub',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Error Getting Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'error_message',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Error Message',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'open_ticket',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open Ticket',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'next',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Next',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'open_trouble_ticket',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open Trouble Ticket',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'trouble_tickets',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Trouble Tickets',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'service_advisories',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Advisories',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'anchor_window',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Anchor Window',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'close_window',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Close Window',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'view_results',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - View Results',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'detailed_list',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Detailed List',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'brief_list',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Brief List',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'display_format',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Display Format',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Progress',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'user_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - User ID',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Status',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'end_date',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - End Date',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'start_date',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Start Date',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_run',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Test Run',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'subscriber_information',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Information',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'select_subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Select Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'enter_search_criteria',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Enter Search Criteria',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'logged_in',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Logged In',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'logout',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Logout',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'diagnostic_manager',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Diagnostic Manager',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'create_ticket',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Create Ticket',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_phase',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Test Phase',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'test_results',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Test Results',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'diagnostic_history',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Diagnostic History',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'subscriber_details',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Details',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'search_subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Search Subscriber',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'reset_svc',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reset Services',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'no_reset_svc',
p_locale_cd => 'en_CA',
p_lbl_txt => 'No services found to reset',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'list_reset_svc',
p_locale_cd => 'en_CA',
p_lbl_txt => 'List of services to reset',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'proceed_reset',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Proceed With Reset',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'svc_reset_sent',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Request has been made to reset services',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'reset_svc',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reset Services',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'no_reset_svc',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - No services found to reset',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'list_reset_svc',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - List of services to reset',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'proceed_reset',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Proceed With Reset',
p_created_by => 'INIT');

Add_Lbl(
p_class_id => 5,
p_obj_nm => 'svc_reset_sent',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Request has been made to reset services',
p_created_by => 'INIT');

END;
/
