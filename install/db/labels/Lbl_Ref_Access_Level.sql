--============================================================================
--    $Id: Lbl_Ref_Access_Level.sql,v 1.3 2002/08/29 12:48:48 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 141   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 141   ---------')

--- Ref_Access_Lvl table was made obsolete.  Functionality being completed using AM
--- Removed labels