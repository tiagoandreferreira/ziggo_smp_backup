--============================================================================
--    $Id: Lbl_Ref_Addr_Typ.sql,v 1.3 2003/04/03 18:58:39 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 340   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 340   ---------')

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'primary',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Primary',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'primary',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Primary',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'primary',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Primary',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'billing',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Billing',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'billing',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Billing',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'billing',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Billing',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'contact',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Contact',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'contact',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Contact',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'contact',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Contact',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'service',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'service',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'service',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 340,
p_obj_nm => 'subscriber',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber',
p_created_by => 'INIT');
END;
/
