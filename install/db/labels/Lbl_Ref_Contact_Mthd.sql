--============================================================================
--    $Id: Lbl_Ref_Contact_Mthd.sql,v 1.4 2002/07/08 22:06:24 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 330   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 330   ---------')

BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'business',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Business Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'business',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Business Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'business',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Business Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'cell',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Cellular Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'cell',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Cellular Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'cell',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Cellular Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'home',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Home Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'home',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Home Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 330,
p_obj_nm => 'home',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP -Home Phone',
p_created_by => 'INIT');
END;
/
