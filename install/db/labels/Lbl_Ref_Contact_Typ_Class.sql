--============================================================================
--    $Id: Lbl_Ref_Contact_Typ_Class.sql,v 1.2 2002/03/04 16:07:07 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 333   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 333   ---------')

BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'account',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Account',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'billing',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Billing',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'primary',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Primary',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'account',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Account',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'billing',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Billing',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'primary',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Primary',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'account',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Account',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'billing',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Billing',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 333,
p_obj_nm => 'primary',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Primary',
p_created_by => 'INIT');
END;
/
