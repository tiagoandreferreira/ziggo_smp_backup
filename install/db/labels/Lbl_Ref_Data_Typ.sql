--============================================================================
--    $Id: Lbl_Ref_Data_Typ.sql,v 1.3 2002/06/26 15:05:44 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 136   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 136   ---------')

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Password',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Password',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Boolean',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Boolean',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Boolean',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Boolean',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Boolean',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Boolean',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'DateTime',
p_locale_cd => 'en_CA',
p_lbl_txt => 'DateTime',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'DateTime',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - DateTime',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'DateTime',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - DateTime',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Float',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Float',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Float',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Float',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Float',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Float',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Integer',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Integer',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Integer',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Integer',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Integer',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Integer',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'String',
p_locale_cd => 'en_CA',
p_lbl_txt => 'String',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'String',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - String',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'String',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - String',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Password',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Password',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 136,
p_obj_nm => 'Password',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Password',
p_created_by => 'INIT');
END;
/
