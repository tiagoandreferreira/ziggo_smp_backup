--============================================================================
--    $Id: Lbl_Ref_Exec_Cost.sql,v 1.2 2002/03/04 16:07:07 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 102   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 102   ---------')

BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'high',
p_locale_cd => 'en_CA',
p_lbl_txt => 'High',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'low',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Low',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'medium',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Medium',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'high',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - High',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'low',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Low',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'medium',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Medium',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'high',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - High',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'low',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Low',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 102,
p_obj_nm => 'medium',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Medium',
p_created_by => 'INIT');
END;
/
