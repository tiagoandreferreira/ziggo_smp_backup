--============================================================================
--    $Id: Lbl_Ref_Ordr_Status.sql,v 1.17 2004/02/09 14:04:56 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 322   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 322   ---------')

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'pre_processor_info',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Pre Processor Information',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'pre_processor_info',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Pre Processor Information',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'pre_processor_info',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Pre Processor Information',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'pre_processor_jndiname',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Pre Processor JNDI Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'pre_processor_jndiname',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Pre Processor JNDI Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'pre_processor_jndiname',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Pre Processor JNDI Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'action',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Action',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'action',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Action',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'action',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Action',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'activate',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Activate',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'activate',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Activate',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'activate',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Activate',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'activation_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Activation in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'activation_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Activation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'activation_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Activation in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'active',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Active',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'active',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Active',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'active',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Active',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'actualCompletionDate',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Actual Completion Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'actualCompletionDate',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Actual Completion Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'actualCompletionDate',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Actual Completion Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'add',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Add',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'add',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Add',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'add',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Add',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'add_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Add in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'add_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Add in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'add_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Add in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'apiClientId',
p_locale_cd => 'en_CA',
p_lbl_txt => 'API Client ID',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'apiClientId',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - API Client ID',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'apiClientId',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - API Client ID',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'cancel',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Cancel',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'cancel',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Cancel',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'cancel',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Cancel',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'cancelled',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Cancelled',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'cancelled',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Cancelled',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'cancelled',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Cancelled',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'change_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Change in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'change_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Change in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'change_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Change in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.aborted_by_client',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Aborted, Aborted By Client',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.aborted_by_client',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Aborted, Aborted By Client',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.aborted_by_client',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Aborted, Aborted By Client',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.aborted_byserver',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Aborted, Aborted by Server',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.aborted_byserver',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Aborted, Aborted by Server',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.aborted_byserver',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Aborted, Aborted by Server',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.enforced',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Aborted, Enforced',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.enforced',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Aborted, Enforced',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.enforced',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Aborted, Enforced',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.invalid',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Aborted, Invalid',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.invalid',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Aborted, Invalid',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.aborted.invalid',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Aborted, Invalid',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.all',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Completed, All',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.all',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Completed, All',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.all',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Completed, All',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.enforced',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Completed, Enforced',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.enforced',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Completed, Enforced',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.enforced',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Completed, Enforced',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.partially',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed, Completed, Partially',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.partially',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Closed, Completed, Partially',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'closed.completed.partially',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Closed, Completed, Partially',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'commit',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Commit',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'commit',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Commit',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'commit',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Commit',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'complete',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Complete',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'complete',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Complete',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'complete',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Complete',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'completed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Completed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'completed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Completed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'completed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Completed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'courtesy_block',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Courtesy Block',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'courtesy_block',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Courtesy Block',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'courtesy_block',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Courtesy Block',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'courtesy_block_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Courtesy Block in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'courtesy_block_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Courtesy Block in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'courtesy_block_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Courtesy Block in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'created_by',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created By',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'created_by',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Created By',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'created_by',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Created By',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'created_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'created_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Created Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'created_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Created Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deactivate',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Deactivate',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deactivate',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Deactivate',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deactivate',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Deactivate',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deactivation_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Deactivation in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deactivation_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Deactivation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deactivation_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Deactivation in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'delete',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Delete',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'delete',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Delete',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'delete',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Delete',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'delete_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Delete in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'delete_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Delete in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'delete_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Delete in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deleted',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Deleted',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deleted',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Deleted',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'deleted',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Deleted',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'err_code',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error Code',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'err_code',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Error Code',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'err_code',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Error Code',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'err_reason',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error Reason',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'err_reason',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Error Reason',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'err_reason',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Error Reason',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'failed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'failed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Failed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'failed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'failedServices',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Failed Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'failedServices',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Failed Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'failedServices',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Failed Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'has_groups',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Has Groups',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'has_groups',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Has Groups',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'has_groups',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Has Groups',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'inactive',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Inactive',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'inactive',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Inactive',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'inactive',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Inactive',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_cancelled',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Item Cancelled',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_cancelled',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Item Cancelled',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_cancelled',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Item Cancelled',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_completed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Item Completed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_completed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Item Completed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_completed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Item Completed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_failed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Item Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_failed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Item Failed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_failed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Item Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_name',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_name',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_object_reason_code',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reason Code',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_object_reason_code',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reason Code',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_object_reason_code',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Reason Code',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_object_reason_note',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reason Note',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_object_reason_note',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reason Note',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_object_reason_note',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Reason Note',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_rejected',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Item Rejected',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_rejected',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Item Rejected',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'item_rejected',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Item Rejected',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'managedEntityKey',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Managed Entity Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'managedEntityKey',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Managed Entity Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'managedEntityKey',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Managed Entity Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'modified_by',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified By',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'modified_by',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified By',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'modified_by',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified By',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'modified_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'modified_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'modified_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'mso_block',
p_locale_cd => 'en_CA',
p_lbl_txt => 'MSO Block',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'mso_block',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - MSO Block',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'mso_block',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - MSO Block',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'mso_block_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'MSO Block in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'mso_block_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - MSO Block in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'mso_block_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - MSO Block in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'object_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Object Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'object_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Object Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'object_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Object Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.not_running.not_started',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open, Not Running, Not Started',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.not_running.not_started',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open, Not Running, Not Started',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.not_running.not_started',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Open, Not Running, Not Started',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.not_running.queued',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open, Not Running, Queued',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.not_running.queued',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open, Not Running, Queued',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.not_running.queued',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Open, Not Running, Queued',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open, Running',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open, Running',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Open, Running',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.failed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open, Running, Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.failed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open, Running, Failed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.failed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Open, Running, Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.workflow',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open, Running, Workflow',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.workflow',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open, Running, Workflow',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.workflow',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Open, Running, Workflow',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.zombie',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Open, Running, Zombie',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.zombie',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Open, Running, Zombie',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'open.running.zombie',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Open, Running, Zombie',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_cancelled',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Cancelled',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_cancelled',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Cancelled',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_cancelled',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Cancelled',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_completed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Completed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_completed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Completed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_completed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Completed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_ext_key',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_ext_key',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order External Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_ext_key',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_failed',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_failed',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Failed',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_failed',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Failed',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_rejected',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Rejected',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_rejected',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Rejected',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_rejected',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Rejected',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_submitted',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Submitted',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_submitted',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Submitted',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'order_submitted',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Submitted',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'orderDate',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Order Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'orderDate',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Order Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'orderDate',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Order Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'previous_state',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Previous State',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'previous_state',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Previous State',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'previous_state',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Previous State',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'priority',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Priority',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'priority',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Priority',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'priority',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Priority',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'purchaseOrder',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Purchase Order',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'purchaseOrder',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Purchase Order',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'purchaseOrder',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Purchase Order',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'queue_request_flag',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Queue Request Flag',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'queue_request_flag',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Queue Request Flag',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'queue_request_flag',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Queue Request Flag',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'reject',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reject',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'reject',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reject',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'reject',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Reject',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'reprov',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reprovision',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'reprov',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reprovision',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'reprov',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Reprovision',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'requestedCompletionDate',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Requested Completion Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'requestedCompletionDate',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Requested Completion Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'requestedCompletionDate',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Requested Completion Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'samp_sub_key',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Samp Sub Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'samp_sub_key',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Samp Sub Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'samp_sub_key',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Samp Sub Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'services',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'services',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'services',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'src_nm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Source Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'src_nm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Source Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'src_nm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Source Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'state',
p_locale_cd => 'en_CA',
p_lbl_txt => 'State',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'state',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - State',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'state',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - State',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'sub_ext_key',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Sub External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'sub_ext_key',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Sub External Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'sub_ext_key',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Sub External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'sub_version',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Sub Version',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'sub_version',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Sub Version',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'sub_version',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Sub Version',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'submit',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Submit',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'submit',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Submit',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'submit',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Submit',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'update',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Update',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'update',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Update',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 324,
p_obj_nm => 'update',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Update',
p_created_by => 'INIT');
END;
/
