--============================================================================
--    $Id: Lbl_Ref_Sub_Modification_Typ.sql,v 1.3 2004/01/21 14:31:38 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 304   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 304   ---------')

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'abuse',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Abuse',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'default',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Default',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'new_service',
p_locale_cd => 'en_CA',
p_lbl_txt => 'New Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'unhappy_with_service',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Unhappy With Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'abuse',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Abuse',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'default',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Default',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'new_service',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - New Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'unhappy_with_service',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Unhappy With Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'abuse',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Abuse',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'default',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Default',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'new_service',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - New Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'unhappy_with_service',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Unhappy With Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'changing_provider',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Changing Provider',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'changing_provider',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Changing Provider',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'changing_provider',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Changing Provider',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'disgruntled_subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Disgruntled Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'disgruntled_subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Disgruntled Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'disgruntled_subscriber',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Disgruntled Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'moving',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Moving',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'moving',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Moving',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'moving',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Moving',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'new_subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'New Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'new_subscriber',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - New Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'new_subscriber',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - New Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'other',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Other',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'other',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Other',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 304,
p_obj_nm => 'other',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Other',
p_created_by => 'INIT');
END;
/
