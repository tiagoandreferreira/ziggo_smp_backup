--============================================================================
--    $Id: Lbl_Ref_Sub_Svc_Status.sql,v 1.5 2004/01/21 14:31:38 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 306   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 306   ---------')

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'active',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Active',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'active',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Active',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'active',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Active',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'add_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Add in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'add_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Add in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'add_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Add in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'courtesy_block',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Courtesy Block',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'courtesy_block',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Courtesy Block',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'courtesy_block',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Courtesy Block',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'courtesy_block_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Courtesy Block in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'courtesy_block_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Courtesy Block in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'courtesy_block_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Courtesy Block in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'delete_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Delete in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'delete_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Delete in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'delete_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Delete in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'deleted',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Deleted',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'deleted',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Deleted',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'deleted',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Deleted',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'inactive',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Inactive',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'inactive',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Inactive',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'inactive',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Inactive',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'mso_block',
p_locale_cd => 'en_CA',
p_lbl_txt => 'MSO Block',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'mso_block',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - MSO Block',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'mso_block',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - MSO Block',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'mso_block_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'MSO Block in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'mso_block_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - MSO Block in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'mso_block_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - MSO Block in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'activation_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Activation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'change_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Change in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'deactivation_in_progress',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Deactivation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'previous_state',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Previous State',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'activation_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Activation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'change_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Change in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'deactivation_in_progress',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Deactivation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'previous_state',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Previous State',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'activation_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Activation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'change_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Change in Progress',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'deactivation_in_progress',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Deactivation in Progress',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'previous_state',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Previous State',
p_created_by => 'INIT');
END;
/
