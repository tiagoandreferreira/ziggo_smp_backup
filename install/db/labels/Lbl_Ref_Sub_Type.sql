--============================================================================
--    $Id: Lbl_Ref_Sub_Type.sql,v 1.2 2002/03/04 16:07:06 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 110   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 110   ---------')

BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'business',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Business',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'residential',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Residential',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'soho',
p_locale_cd => 'en_CA',
p_lbl_txt => 'SOHO',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'business',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Business',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'residential',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Residential',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'soho',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - SOHO',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'business',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Business',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'residential',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Residential',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 110,
p_obj_nm => 'soho',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - SOHO',
p_created_by => 'INIT');
END;
/
