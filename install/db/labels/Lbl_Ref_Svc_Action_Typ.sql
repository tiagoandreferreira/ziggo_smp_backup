--============================================================================
--    $Id: Lbl_Ref_Svc_Action_Typ.sql,v 1.2 2002/08/21 15:27:16 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 142   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 142   ---------')

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'a',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Add',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'c',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Change',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'd',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Delete',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'g',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Diagnose',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'p',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Provisionable',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'a',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Add',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'c',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Change',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'd',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Delete',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'g',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Diagnose',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'p',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Provisionable',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'a',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Add',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'c',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Change',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'd',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Delete',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'g',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Diagnose',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 142,
p_obj_nm => 'p',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Provisionable',
p_created_by => 'INIT');
END;
/
