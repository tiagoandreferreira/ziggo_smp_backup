--============================================================================
--    $Id: Lbl_Ref_Svc_Addr_Typ.sql,v 1.2 2002/03/04 16:07:06 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 341   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 341   ---------')

BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'from_address',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - From Address',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'service_address',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Address',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'to_address',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - To Address',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'from_address',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - From Address',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'service_address',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Address',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'to_address',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - To Address',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'from_address',
p_locale_cd => 'en_CA',
p_lbl_txt => 'From Address',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'service_address',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Address',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 341,
p_obj_nm => 'to_address',
p_locale_cd => 'en_CA',
p_lbl_txt => 'To Address',
p_created_by => 'INIT');
END;
/
