--============================================================================
--    $Id: Lbl_Ref_Svc_Parm_Chg_Typ.sql,v 1.2 2002/03/04 16:07:06 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 134   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 134   ---------')

BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'a',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Any Change',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'd',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Value to Null (Delete)',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'i',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Null to Value (Insert)',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'u',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Value to Value (Update)',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'a',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Any Change',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'd',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Value to Null (Delete)',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'i',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Null to Value (Insert)',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'u',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Value to Value (Update)',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'a',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Any Change',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'd',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Value to Null (Delete)',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'i',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Null to Value (Insert)',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 134,
p_obj_nm => 'u',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Value to Value (Update)',
p_created_by => 'INIT');
END;
/
