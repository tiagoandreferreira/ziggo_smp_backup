--============================================================================
--    $Id: Lbl_Ref_Svc_State.sql,v 1.2 2002/03/04 16:07:06 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 101   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 101   ---------')

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'e',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Expired',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'o',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Offered',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'p',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Prepared',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 's',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Suspended',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'c',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Created',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'd',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Deleted',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'e',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Expired',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'o',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Offered',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'p',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Prepared',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 's',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Suspended',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'c',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'c',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Created',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'd',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Deleted',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'e',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Expired',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'o',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Offered',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'p',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Prepared',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 's',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Suspended',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 101,
p_obj_nm => 'd',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Deleted',
p_created_by => 'INIT');
END;
/
