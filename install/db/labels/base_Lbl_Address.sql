--============================================================================
--    $Id: base_Lbl_Address.sql,v 1.1 2002/11/27 14:29:04 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 4   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 4   ---------')

BEGIN
Add_Lbl(
p_class_id => 4,
p_obj_nm => 'pref_nm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Preferred Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 4,
p_obj_nm => 'pref_nm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Preferred Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 4,
p_obj_nm => 'pref_nm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Preferred Name',
p_created_by => 'INIT');
END;
/
