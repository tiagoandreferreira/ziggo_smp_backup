--============================================================================
--    $Id: base_Lbl_Contact_Mthd_Typ.sql,v 1.2 2003/05/01 13:20:31 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 116   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 116   ---------')

BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.email.home',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Home Email',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.business',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Business Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.cell',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Cellular Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.home',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Home Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.email.home',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Home Email',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.business',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Business Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.cell',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Cellular Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.home',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Home Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.email.home',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Home Email',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.business',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Business Phone',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.cell',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Cellular Phone',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 116,
p_obj_nm => 'Sub_Contact_Mthd.phone.home',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Home Phone',
p_created_by => 'INIT');
END;
/
