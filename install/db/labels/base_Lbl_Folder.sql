--============================================================================
--    $Id: base_Lbl_Folder.sql,v 1.1 2002/03/05 18:00:10 razvanc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 140   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 140   ---------')

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'all_services',
p_locale_cd => 'en_CA',
p_lbl_txt => 'All Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'composed_services',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Composed Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'orderable_services',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Orderable Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'provisionable_services',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Provisionable Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'subtyped_services',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subtyped Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'all_services',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - All Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'composed_services',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Composed Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'orderable_services',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Orderable Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'provisionable_services',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Provisionable Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'subtyped_services',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subtyped Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'all_services',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - All Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'composed_services',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Composed Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'orderable_services',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Orderable Services',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'provisionable_services',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Provisionable Services',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 140,
p_obj_nm => 'subtyped_services',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subtyped Services',
p_created_by => 'INIT');
END;
/
