--============================================================================
--    $Id: base_Lbl_Item_Status.sql,v 1.1 2003/03/18 21:18:58 razvanc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 601   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 601   ---------')

BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'new_modified',
p_locale_cd => 'en_CA',
p_lbl_txt => 'New Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'new',
p_locale_cd => 'en_CA',
p_lbl_txt => 'New',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'not_modified',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Not Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'modified',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'new_modified',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - New Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'new',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - New',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'not_modified',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Not Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'modified',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'new_modified',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - New Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'new',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - New',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'not_modified',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Not Modified',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 601,
p_obj_nm => 'modified',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified',
p_created_by => 'INIT');
END;
/
