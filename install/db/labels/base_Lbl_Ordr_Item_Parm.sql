--============================================================================
--    $Id: base_Lbl_Ordr_Item_Parm.sql,v 1.1 2003/10/08 20:18:14 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 507   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 507   ---------')

BEGIN
Add_Lbl(
p_class_id => 507,
p_obj_nm => 'err_code',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Error Code',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 507,
p_obj_nm => 'err_code',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Error Code',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 507,
p_obj_nm => 'err_code',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error Code',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 507,
p_obj_nm => 'err_reason',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Error Reason',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 507,
p_obj_nm => 'err_reason',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Error Reason',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 507,
p_obj_nm => 'err_reason',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error Reason',
p_created_by => 'INIT');
END;
/
