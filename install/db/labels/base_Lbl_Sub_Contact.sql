--============================================================================
--    $Id: base_Lbl_Sub_Contact.sql,v 1.1 2003/05/01 13:20:53 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 114   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 114   ---------')

BEGIN
Add_Lbl(
p_class_id => 114,
p_obj_nm => 'Sub_Contact',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Contact',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 114,
p_obj_nm => 'Sub_Contact',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Contact',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 114,
p_obj_nm => 'Sub_Contact',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Contact',
p_created_by => 'INIT');
END;
/
