--============================================================================
--    $Id: base_Lbl_Svc.sql,v 1.5 2002/09/23 15:01:08 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 100   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 100   ---------')

BEGIN
Add_Lbl(
p_class_id => 100,
p_obj_nm => 'samp_access',
p_locale_cd => 'en_CA',
p_lbl_txt => 'SMP Access Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 100,
p_obj_nm => 'samp_access',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - SMP Access Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 100,
p_obj_nm => 'samp_access',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - SMP Access Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 100,
p_obj_nm => 'samp_sub',
p_locale_cd => 'en_CA',
p_lbl_txt => 'SMP Subscriber Service',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 100,
p_obj_nm => 'samp_sub',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - SMP Subscriber Service',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 100,
p_obj_nm => 'samp_sub',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - SMP Subscriber Service',
p_created_by => 'INIT');
END;
/
