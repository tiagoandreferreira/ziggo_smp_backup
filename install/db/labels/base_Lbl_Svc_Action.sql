--============================================================================
--    $Id: base_Lbl_Svc_Action.sql,v 1.2 2002/08/20 20:53:41 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 130   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 130   ---------')

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.add',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Add SMP Access',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.add',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Add SMP Access',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.add',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Add SMP Access',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.delete',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Delete SMP Access',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.delete',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Delete SMP Access',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.delete',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Delete SMP Access',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.reprov',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reprovision SMP Access',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.reprov',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reprovision SMP Access',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_access.reprov',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Reprovision SMP Access',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.add',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Add SMP Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.add',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Add SMP Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.add',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Add SMP Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.delete',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Delete SMP Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.delete',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Delete SMP Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.delete',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Delete SMP Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.reprov',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Reprovision SMP Subscriber',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.reprov',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Reprovision SMP Subscriber',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 130,
p_obj_nm => 'samp_sub.reprov',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Reprovision SMP Subscriber',
p_created_by => 'INIT');
END;
/
