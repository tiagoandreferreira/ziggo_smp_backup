--============================================================================
--    $Id: base_Lbl_Svc_Addr.sql,v 1.2 2003/05/01 19:48:59 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 508   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 508   ---------')

BEGIN
Add_Lbl(
p_class_id => 508,
p_obj_nm => 'Svc_Addr',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Address',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 508,
p_obj_nm => 'Svc_Addr',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Address',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 508,
p_obj_nm => 'Svc_Addr',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Address',
p_created_by => 'INIT');
END;
/
