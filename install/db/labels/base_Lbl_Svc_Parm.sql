--============================================================================
--    $Id: base_Lbl_Svc_Parm.sql,v 1.4 2003/11/18 17:56:15 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 138   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 138   ---------')

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'end_svc_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Expiry Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Home Location',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Home Location',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Home Location',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.default',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Default?',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.default',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Default?',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.default',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Default?',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.pref_nm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Preferred Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.pref_nm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Preferred Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.pref_nm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Preferred Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.status',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Type',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.type',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Type',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_location.type',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Type',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'modified_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'modified_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'modified_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'nep',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Network Entry Point',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'nep',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Network Entry Point',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'nep',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Network Entry Point',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'parent_sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'parent_sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'parent_sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'provision_status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Provision Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'provision_status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Provision Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'provision_status',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Provision Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'purchase_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Purchase Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'purchase_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Purchase Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'purchase_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Purchase Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'start_svc_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Effective Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'start_svc_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Effective Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'start_svc_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Effective Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'sub_svc_ext_key',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Service External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'sub_svc_ext_key',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Service External Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'sub_svc_ext_key',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber Service External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.password-confirm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Confirm Password',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.password-confirm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Confirm Password',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.password-confirm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Confirm Password',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.country',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Country',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.language',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Language',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.country',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Country',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.language',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Language',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.country',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Country',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.language',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Language',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.base_sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.base_sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.base_sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.created_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.created_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Created Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.created_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Created Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.email_login',
p_locale_cd => 'en_CA',
p_lbl_txt => 'User Name / Email ID',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.email_login',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - User Name / Email ID',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.end_svc_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Expiry Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.end_svc_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Expiry Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.end_svc_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Expiry Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.force_pswd_chg',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Force Password Change',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.force_pswd_chg',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Force Password Change',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.force_pswd_chg',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP -Force Password Change',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.groups',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Groups',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.groups',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Groups',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.last_locked',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Last Locked Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.last_locked',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Last Locked Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.last_locked',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Last Locked Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.last_unsuccessful_attempt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Date of Last Unsuccessful Attempt',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.last_unsuccessful_attempt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Date of Last Unsuccessful Attempt',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.last_unsuccessful_attempt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Date of Last Unsuccessful Attempt',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Home Location',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Home Location',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Home Location',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.default',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Default?',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.default',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Default?',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.default',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Default?',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.pref_nm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Preferred Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.pref_nm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Preferred Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.pref_nm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Preferred Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.status',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Type',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.type',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Type',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_location.type',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Type',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.modified_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.modified_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.modified_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.num_of_attempts',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Number of Attempts',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.num_of_attempts',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Number of Attempts',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.num_of_attempts',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Number of Attempts',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.parent_sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.parent_sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.parent_sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.password',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Password',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.password',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Password',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.provision_status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Provision Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.provision_status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Provision Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.provision_status',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Provision Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.purchase_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Purchase Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.purchase_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Purchase Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.purchase_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Purchase Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.self_svc_access_scope',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Self Service Access Scope',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.self_svc_access_scope',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Self Service Access Scope',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.self_svc_access_scope',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Self Service Access Scope',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.start_svc_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Effective Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.start_svc_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Effective Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.start_svc_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Effective Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.sub_svc_ext_key',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Service External Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.sub_svc_ext_key',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Service External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.sub_svc_ext_key',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber Service External Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.base_sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.base_sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.base_sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.created_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.created_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Created Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.created_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Created Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.end_svc_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Expiry Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.end_svc_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Expiry Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.end_svc_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Expiry Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Home Location',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Home Location',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Home Location',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.default',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Default?',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.default',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Default?',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.default',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Default?',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.pref_nm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Preferred Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.pref_nm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Preferred Name',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.pref_nm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Preferred Name',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.status',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Location Type',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.type',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Location Type',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_location.type',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Location Type',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.modified_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.modified_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Modified Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.modified_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Modified Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.parent_sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.parent_sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.parent_sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Parent Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.provision_status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Provision Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.provision_status',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Provision Status',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.provision_status',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Provision Status',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.purchase_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Purchase Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.purchase_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Purchase Date',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.purchase_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Purchase Date',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.start_svc_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Effective Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.start_svc_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Effective Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.start_svc_dt',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Effective Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.sub_svc_ext_key',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Service External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.sub_svc_ext_key',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Service External Key',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.sub_svc_ext_key',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber Service External Key',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_sub.svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.email_login',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - User Name / Email ID',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.groups',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Groups',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'samp_access.password',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Password',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'base_sub_svc_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'base_sub_svc_id',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'base_sub_svc_id',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Base Subscriber Service Id',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'created_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'created_dtm',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Created Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'created_dtm',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Created Date and Time',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'end_svc_dt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Expiry Date and Time',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 138,
p_obj_nm => 'end_svc_dt',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Expiry Date and Time',
p_created_by => 'INIT');
END;
/
