--============================================================================
--    $Id: base_Lbl_Tmplt.sql,v 1.3 2002/08/20 16:22:06 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Lbl Class: 132   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl Class: 132   ---------')

BEGIN
Add_Lbl(
p_class_id => 132,
p_obj_nm => 'spm_status_upd',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Status Update Template',
p_created_by => 'INIT');
END;
/
BEGIN
Add_Lbl(
p_class_id => 132,
p_obj_nm => 'spm_status_upd',
p_locale_cd => 'fr_CA',
p_lbl_txt => 'FR - Status Update Template',
p_created_by => 'INIT');
END;
/

BEGIN
Add_Lbl(
p_class_id => 132,
p_obj_nm => 'spm_status_upd',
p_locale_cd => 'ja_JP',
p_lbl_txt => 'JP - Status Update Template',
p_created_by => 'INIT');
END;
/
