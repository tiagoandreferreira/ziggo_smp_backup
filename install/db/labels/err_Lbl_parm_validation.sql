--============================================================================
--    $Id: err_Lbl_parm_validation.sql,v 1.2 2003/04/02 22:31:20 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on

---- Labels for parameter validations
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Labels for Parameter Validations   ---------')

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_SUCCESS', 'en_CA','Value is valid',sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_SUCCESS', 'fr_CA','FR - Value is valid',sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_SUCCESS', 'ja_JP','JP - Value is valid',sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_VALUE_REQ', 'en_CA','Value required for parmNm = "{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_VALUE_REQ', 'fr_CA','FR - Value required for parmNm = "{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_VALUE_REQ', 'ja_JP','JP - Value required for parmNm = "{0}"',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_JMF_FAIL', 'en_CA', 'JMF expression "{2}" failed for parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_JMF_FAIL', 'fr_CA', 'FR - JMF expression "{2}" failed for parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7, 'VLDERR_JMF_FAIL', 'ja_JP', 'JP - JMF expression "{2}" failed for parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);
-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_VAL_INVALID','en_CA', 'Min inclusion check failed for rule = "{2}" parmNm = "{0} value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_VAL_INVALID','fr_CA', 'FR - Min inclusion check failed for rule = "{2}" parmNm = "{0} value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_VAL_INVALID','ja_JP', 'JP - Min inclusion check failed for rule = "{2}" parmNm = "{0} value = "{1}"',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_VAL_INVALID','en_CA', 'Max inclusion check failed for rule = "{2}" parmNm = "{0} value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_VAL_INVALID','fr_CA', 'FR - Max inclusion check failed for rule = "{2}" parmNm = "{0} value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_VAL_INVALID','ja_JP', 'JP - Max inclusion check failed for rule = "{2}" parmNm = "{0} value = "{1}"',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_EX_INVALID','en_CA',
'Min exclusion check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_EX_INVALID','fr_CA',
'FR - Min exclusion check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_EX_INVALID','ja_JP',
'JP - Min exclusion check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);
-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_EX_INVALID','en_CA', 'Max exclusion check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_EX_INVALID','fr_CA', 'FR - Max exclusion check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_EX_INVALID','ja_JP', 'JP - Max exclusion check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_IN_INVALID','en_CA', 'Min value check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_IN_INVALID','fr_CA', 'FR - Min value check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_IN_INVALID','ja_JP', 'JP - Min value check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);
-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_IN_INVALID','en_CA', 'Max value check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_IN_INVALID','fr_CA', 'FR - Max value check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_IN_INVALID','ja_JP', 'JP - Max value check failed for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);
-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_CHAR_INVALID','en_CA', 'Max char number failed  for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_CHAR_INVALID','fr_CA', 'FR - Max char number failed  for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MAX_CHAR_INVALID','ja_JP', 'JP - Max char number failed  for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);
-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_CHAR_INVALID','en_CA', 'Min char number failed  for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_CHAR_INVALID','fr_CA', 'FR - Min char number failed  for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_MIN_CHAR_INVALID','ja_JP', 'JP - Min char number failed  for rule = "{2}" parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_DATATYPE_INVALID','en_CA', 'Invalid data type parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_DATATYPE_INVALID','fr_CA', 'FR - Invalid data type parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_DATATYPE_INVALID','ja_JP', 'JP - Invalid data type parmNm = "{0}" value = "{1}"',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_VALUE_NOT_PERMIT','en_CA', 'Value "{1}" not permitted for parmNm = "{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_VALUE_NOT_PERMIT','fr_CA', 'FR - Value "{1}" not permitted for parmNm = "{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_VALUE_NOT_PERMIT','ja_JP', 'JP - Value "{1}" not permitted for parmNm = "{0}"',
sysdate,'INIT',null,null);
-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_FORMAT_INVALID','en_CA', 'Invalid number format "{1}" for parmNm = "{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_FORMAT_INVALID','fr_CA', 'FR - Invalid number format "{1}" for parmNm = "{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_FORMAT_INVALID','ja_JP', 'JP - Invalid number format "{1}" for parmNm = "{0}"',
sysdate,'INIT',null,null);
