--============================================================================
--    $Id: err_Lbl_spm_process.sql,v 1.6 2004/01/27 12:06:35 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on

---- Labels for general errors
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Labels for General Errors   ---------')

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_UNKNOWN_REASON','en_CA','Unexpected Error. Please contact Information Services',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_UNKNOWN_REASON','fr_CA','FR - Unexpected Error. Please contact Information Services',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_UNKNOWN_REASON','ja_JP','JP - Unexpected Error. Please contact Information Services',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_SVC_LOCATION_INVALID','en_CA','Invalid Service Location',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_SVC_LOCATION_INVALID','fr_CA','FR - Invalid Service Location',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_SVC_LOCATION_INVALID','ja_JP','JP - Invalid Service Location',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_STATE_PROC_FAIL','en_CA','Illegal Status Change',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_STATE_PROC_FAIL','fr_CA','FR - Illegal Status Change',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_STATE_PROC_FAIL','ja_JP','JP - Illegal Status Change',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_ORDER_ID_INVALID','en_CA','Order cannot be located in the system',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_ORDER_ID_INVALID','fr_CA','FR - Order cannot be located in the system',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_ORDER_ID_INVALID','ja_JP','JP - Order cannot be located in the system',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_REQUEST_DATAOUTOFSYNC','en_CA','Data has changed in the database prior to your changes. Please retry',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_REQUEST_DATAOUTOFSYNC','fr_CA','FR - Data has changed in the database prior to your changes. Please retry',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'PRCERR_REQUEST_DATAOUTOFSYNC','ja_JP','JP - Data has changed in the database prior to your changes. Please retry',
sysdate,'INIT',null,null);

-----

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_VALUE_NOTUNIQUE','en_CA','Parameter value is not unique for parmNm="{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_VALUE_NOTUNIQUE','fr_CA','FR  - Parameter value is not unique for parmNm="{0}"',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'VLDERR_VALUE_NOTUNIQUE','ja_JP','JP  - Parameter value is not unique for parmNm="{0}"',
sysdate,'INIT',null,null);

---- Labels for Invalid Password Processing
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Labels for Invalid Password Processing   ---------')

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_PASSWORD_INVALID', 'en_CA',
 'Login password is invalid', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_PASSWORD_INVALID', 'fr_CA',
 'FR - Login password is invalid', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_PASSWORD_INVALID', 'ja_JP',
 'JP - Login password is invalid', sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_INVALID', 'en_CA',
 'Login user name is invalid', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_INVALID', 'fr_CA',
 'FR - Login user name is invalid', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_INVALID', 'ja_JP',
 'JP - Login user name is invalid', sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_PASSWORD_MISSING', 'en_CA',
 'Missing user/password', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_PASSWORD_MISSING', 'fr_CA',
 'FR - Missing user/password', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_PASSWORD_MISSING', 'ja_JP',
 'JP - Missing user/password', sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_MSOBLOCKED', 'en_CA',
 'The User is blocked by MSO', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_MSOBLOCKED', 'fr_CA',
 'FR - The User is blocked by MSO', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_MSOBLOCKED', 'ja_JP',
 'JP - The User is blocked by MSO', sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_BLOCKED', 'en_CA',
 'The user is blocked in system for some reason, most likely the user is not in an active status', 
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_BLOCKED', 'fr_CA',
 'FR - The user is blocked in system for some reason, most likely the user is not in an active status', 
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_BLOCKED', 'ja_JP',
 'JP - The user is blocked in system for some reason, most likely the user is not in an active status', 
  sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_ERRATTEMPTS_LOCKED', 'en_CA',
 'The User is locked because of too many unsuccessful authentication attempts', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_ERRATTEMPTS_LOCKED', 'fr_CA',
 'FR - The User is locked because of too many unsuccessful authentication attempts', sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'LOGIN_USER_ERRATTEMPTS_LOCKED', 'ja_JP',
 'JP - The User is locked because of too many unsuccessful authentication attempts', sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 VALUES (7, 'ORDER_ANALYSIS_SVC_CHANGE_ERR', 'en_CA', 'The order relates to a service "{0}", which is inactive',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 VALUES ( 7, 'ORDER_ANALYSIS_SVC_CHANGE_ERR', 'fr_CA', 'FR - The order relates to a service "{0}", which is inactive',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) 
 VALUES ( 7, 'ORDER_ANALYSIS_SVC_CHANGE_ERR', 'ja_JP', 'JP - The order relates to a service "{0}", which is inactive',
 sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 VALUES (7, 'SUB_ANALYSIS_SVC_CHANGE_ERR', 'en_CA', 'The subscriber profile has a service "{0}", which is inactive',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) 
 VALUES ( 7, 'SUB_ANALYSIS_SVC_CHANGE_ERR', 'fr_CA', 'FR - The subscriber profile has a service "{0}", which is inactive',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 VALUES ( 7, 'SUB_ANALYSIS_SVC_CHANGE_ERR', 'ja_JP', 'JP - The subscriber profile has a service "{0}", which is inactive',
 sysdate,'INIT',null,null);

---------

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SPM_DUPLICATE_ORDER','en_CA','Duplicate Order Received',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SPM_DUPLICATE_ORDER','fr_CA','FR - Duplicate Order Received',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SPM_DUPLICATE_ORDER','ja_JP','JP - Duplicate Order Received',
sysdate,'INIT',null,null);

---------

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SVC_PROVIDER_UNAUTHORIZED_ERR','en_CA','Unauthorized access, Service Provider of the object is different from yours!',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SVC_PROVIDER_UNAUTHORIZED_ERR','fr_CA','FR - Unauthorized access, Service Provider of the object is different from yours!',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SVC_PROVIDER_UNAUTHORIZED_ERR','ja_JP','JP - Unauthorized access, Service Provider of the object is different from yours!',
sysdate,'INIT',null,null);

---------

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SPM_DUPLICATE_SUB','en_CA','Duplicate SubExtKey Received',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SPM_DUPLICATE_SUB','fr_CA','FR - Duplicate SubExtKey Received',
sysdate,'INIT',null,null);

insert into LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM) values
(7,'SPM_DUPLICATE_SUB','ja_JP','JP - Duplicate SubExtKey Received',
sysdate,'INIT',null,null);