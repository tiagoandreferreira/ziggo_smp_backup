--============================================================================
--    $Id: err_Lbl_sub_validation.sql,v 1.4 2003/06/06 22:18:05 ceciliar Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set serveroutput on

---- Labels for sub profile validations
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Labels for Sub Profile Validations   ---------')

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_alias_preq_svc_missing',  'en_CA', 
'svc:{0} is required by svc:{1} for parm alias reason', 
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_alias_preq_svc_missing',  'fr_CA', 
'FR - svc:{0} is required by svc:{1} for parm alias reason', 
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_alias_preq_svc_missing',  'ja_JP', 
'JP - svc:{0} is required by svc:{1} for parm alias reason', 
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_invalid_parm_alias',  'en_CA', 
'parm:{0} in svc:{1} has a undefined alias', 
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_invalid_parm_alias',  'fr_CA', 
'FR - parm:{0} in svc:{1} has a undefined alias', 
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_invalid_parm_alias',  'ja_JP', 
'JP - parm:{0} in svc:{1} has a undefined alias', 
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_invalid_svc_jmf',  'en_CA', 
'Svc : "{0}" has invalid JMF condition', 
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_invalid_svc_jmf',  'fr_CA', 
'FR - Svc : "{0}" has invalid JMF condition', 
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_invalid_svc_jmf',  'ja_JP', 
'JP - Svc : "{0}" has invalid JMF condition', 
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_orderable',  'en_CA',
'Svc : "{0}" is not orderable and it has no parent, it cannot be added',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_orderable',  'fr_CA',
'FR - Svc : "{0}" is not orderable and it has no parent, it cannot be added',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_orderable',  'ja_JP',
'JP - Svc : "{0}" is not orderable and it has no parent, it cannot be added',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_effective',  'en_CA',
'Svc : "{0}" is not effective yet',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_effective',  'fr_CA',
'FR - Svc : "{0}" is not effective yet',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_effective',  'ja_JP',
'JP - Svc : "{0}" is not effective yet',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_has_null_eff_date',  'en_CA',
'SvcNm : "{0}" has null Effective Date (is not effective)',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_has_null_eff_date',  'fr_CA',
'FR - SvcNm : "{0}" has null Effective Date (is not effective)',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_has_null_eff_date',  'ja_JP',
'JP - SvcNm : "{0}" has null Effective Date (is not effective)',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_eff_date_outside_base_svc_eff_date',  'en_CA',
'Svc : "{0}" has effective date extending base svc effective date : not allowed',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_eff_date_outside_base_svc_eff_date',  'fr_CA',
'FR - Svc : "{0}" has effective date extending base svc effective date : not allowed',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_eff_date_outside_base_svc_eff_date',  'ja_JP',
'JP - Svc : "{0}" has effective date extending base svc effective date : not allowed',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_exp_date_extends_base_svc_exp_date',  'en_CA',
'Svc : "{0}" has expiry date extending base svc expiry date : not allowed',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_exp_date_extends_base_svc_exp_date',  'fr_CA',
'FR - Svc : "{0}" has expiry date extending base svc expiry date : not allowed',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_exp_date_extends_base_svc_exp_date',  'ja_JP',
'JP - Svc : "{0}" has expiry date extending base svc expiry date : not allowed',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_is_expired',  'en_CA',
'Svc : "{0}" svc is expired',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_is_expired',  'fr_CA',
'FR - Svc : "{0}" svc is expired',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_is_expired',  'ja_JP',
'JP - Svc : "{0}" svc is expired',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_empty_svc_model',  'en_CA',
'Empty subscriber profile (svc model)',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_empty_svc_model',  'fr_CA',
'FR - Empty subscriber profile (svc model)',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_empty_svc_model',  'ja_JP',
'JP - Empty subscriber profile (svc model)',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_min_instance_num',  'en_CA',
 'Svc : "{0}"  having svccat minProfInstNum : {1,number,integer} has current instance num :{2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_min_instance_num',  'fr_CA',
 'FR - Svc : "{0}"  having svccat minProfInstNum : {1,number,integer} has current instance num :{2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_min_instance_num',  'ja_JP',
 'JP - Svc : "{0}"  having svccat minProfInstNum : {1,number,integer} has current instance num :{2,number,integer}',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_max_instance_num',  'en_CA',
'Svc : "{0}"  having svccat maxProfInstNum : {1,number,integer} has current instance num :{2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_max_instance_num',  'fr_CA',
'FR - Svc : "{0}"  having svccat maxProfInstNum : {1,number,integer} has current instance num :{2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_max_instance_num',  'ja_JP',
'JP - Svc : "{0}"  having svccat maxProfInstNum : {1,number,integer} has current instance num :{2,number,integer}',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_address',  'en_CA',
'Svc : "{0}" needs svc address',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_address',  'fr_CA',
'FR - Svc : "{0}" needs svc address',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_address',  'ja_JP',
'JP - Svc : "{0}" needs svc address',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_composed_with_child',  'en_CA',
'Svc : "{0}" is not composed but it has children in sub profile',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_composed_with_child',  'fr_CA',
'FR - Svc : "{0}" is not composed but it has children in sub profile',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_not_composed_with_child',  'ja_JP',
'JP - Svc : "{0}" is not composed but it has children in sub profile',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_composed_with_invalid_child',  'en_CA',
'Svc : "{0}"  has invalid child with svcNm : "{1}"',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_composed_with_invalid_child',  'fr_CA',
'FR - Svc : "{0}"  has invalid child with svcNm : "{1}"',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_composed_with_invalid_child',  'ja_JP',
'JP - Svc : "{0}"  has invalid child with svcNm : "{1}"',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_min_child_svc_inst_num',  'en_CA',
'SvcCompositionRule: component Svc : "{0}" has  invalid min instance number : {1,number,integer}' ||
' Required : {2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_min_child_svc_inst_num',  'fr_CA',
'FR - SvcCompositionRule: component Svc : "{0}" has  invalid min instance number : {1,number,integer}' ||
' Required : {2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_min_child_svc_inst_num',  'ja_JP',
'JP - SvcCompositionRule: component Svc : "{0}" has  invalid min instance number : {1,number,integer}' ||
' Required : {2,number,integer}',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_max_child_svc_inst_num',  'en_CA',
'SvcCompositionRule coponent svcNm : "{0}"  has invalid max instance number : {1,number,integer} ' ||
' Required : {2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_max_child_svc_inst_num',  'fr_CA',
'FR - SvcCompositionRule coponent svcNm : "{0}"  has invalid max instance number : {1,number,integer} ' ||
' Required : {2,number,integer}',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_max_child_svc_inst_num',  'ja_JP',
'JP - SvcCompositionRule coponent svcNm : "{0}"  has invalid max instance number : {1,number,integer} ' ||
' Required : {2,number,integer}',
sysdate,'INIT',null,null);
-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_exclusion_rule',  'en_CA',
'Svc : "{0}" is  mutually exclusive with svc : "{1}"',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_exclusion_rule',  'fr_CA',
'FR - Svc : "{0}" is  mutually exclusive with svc : "{1}"',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_exclusion_rule',  'ja_JP',
'JP - Svc : "{0}" is  mutually exclusive with svc : "{1}"',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_instance_depy_missing_prereq_svc',  'en_CA',
'Instance dependency : missing prerequisite svc : "{0}" for svc : "{1}" ',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_instance_depy_missing_prereq_svc',  'fr_CA',
'FR - Instance dependency : missing prerequisite svc : "{0}" for svc : "{1}" ',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_instance_depy_missing_prereq_svc',  'ja_JP',
'JP - Instance dependency : missing prerequisite svc : "{0}" for svc : "{1}" ',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_depy_or_group_svcs_not_found',  'en_CA',
'Class depy for dependent svc "{0}" :   none of the {1,number,integer} ' ||
' prerequisite services were found for OrGrpId : {2,number,integer} ',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_depy_or_group_svcs_not_found',  'fr_CA',
'FR - Class depy for dependent svc "{0}" :   none of the {1,number,integer} ' || 
' prerequisite services were found for OrGrpId : {2,number,integer} ',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_svc_depy_or_group_svcs_not_found',  'ja_JP',
'JP - Class depy for dependent svc "{0}" :   none of the {1,number,integer} ' || 
' prerequisite services were found for OrGrpId : {2,number,integer} ',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_class_depy_missing_prereq_svc',  'en_CA',
'Class dependency : missing prerequisite  svc : "{0}" for svc : "{1}"',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_class_depy_missing_prereq_svc',  'fr_CA',
'FR - Class dependency : missing prerequisite  svc : "{0}" for svc : "{1}"',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_vld_err_class_depy_missing_prereq_svc',  'ja_JP',
'JP - Class dependency : missing prerequisite  svc : "{0}" for svc : "{1}"',
sysdate,'INIT',null,null);

-----

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_invalid_sub_profile',  'en_CA',
'Null or invalid catalog svc id ',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_invalid_sub_profile',  'fr_CA',
'FR - Null or invalid catalog svc id ',
sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'sub_prof_invalid_sub_profile',  'ja_JP',
'JP - Null or invalid catalog svc id ',
sysdate,'INIT',null,null);

---- Labels for migration errors
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Labels for Migration Errors   ---------')

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'migr_err_internal_system_err', 'en_CA',
'Migration System Error',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'migr_err_internal_system_err', 'fr_CA',
'FR - Migration System Error',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'migr_err_internal_system_err', 'ja_JP',
'JP - Migration System Error',
 sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 values(7,'migr_err_internal_system_err_cannot_find_svc_by_key', 'en_CA',
 'Migration System Error : cannot find svc by Key(Id) in the profile',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 values(7,'migr_err_internal_system_err_cannot_find_svc_by_key', 'fr_CA',
 'FR - Migration System Error : cannot find svc by Key(Id) in the profile',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 values(7,'migr_err_internal_system_err_cannot_find_svc_by_key', 'ja_JP',
 'JP - Migration System Error : cannot find svc by Key(Id) in the profile',
  sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_system_err_cannot_add_new_svc_proto', 'en_CA',
 'Migration System Error : cannot create new svc prototype ',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_system_err_cannot_add_new_svc_proto', 'fr_CA',
 'FR - Migration System Error : cannot create new svc prototype ',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_system_err_cannot_add_new_svc_proto', 'ja_JP',
 'JP - Migration System Error : cannot create new svc prototype ',
  sysdate,'INIT',null,null);
---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
   values(7,'migr_err_cannot_find_svc_migr_by_name', 'en_CA',
   'Migration Config Err: Cannot find svc migration with name : "{0}"',
   sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
   values(7,'migr_err_cannot_find_svc_migr_by_name', 'fr_CA',
   'FR - Migration Config Err: Cannot find svc migration with name : "{0}"',
   sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
   values(7,'migr_err_cannot_find_svc_migr_by_name', 'ja_JP',
   'JP - Migration Config Err: Cannot find svc migration with name : "{0}"',
   sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
    values(7,'migr_err_cannot_apply_migration_to_svc_wrong_from_svc', 'en_CA',
    'Migration Config Err : wrong from Svc : "{0}" in migration name : "{1}"',
    sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
    values(7,'migr_err_cannot_apply_migration_to_svc_wrong_from_svc', 'fr_CA',
    'FR - Migration Config Err : wrong from Svc : "{0}" in migration name : "{1}"',
    sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
    values(7,'migr_err_cannot_apply_migration_to_svc_wrong_from_svc', 'ja_JP',
    'JP - Migration Config Err : wrong from Svc : "{0}" in migration name : "{1}"',
    sysdate,'INIT',null,null);
---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_(unique)_destination_for_relocation_of_svc', 'en_CA',
  'Migration Config Err : Cannot uniquely find destination scope svc name : "{0}" , migr name : "{1}"',
   sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_(unique)_destination_for_relocation_of_svc', 'fr_CA',
  'FR - Migration Config Err : Cannot uniquely find destination scope svc name : "{0}" , migr name : "{1}"',
   sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_(unique)_destination_for_relocation_of_svc', 'ja_JP',
  'JP - Migration Config Err : Cannot uniquely find destination scope svc name : "{0}" , migr name : "{1}"',
   sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'migr_err_multiple_source_scope_svc_found', 'en_CA',
'Migration Config Err : multiple source scope services found with name : "{0}"',
 sysdate,'INIT',null,null);


INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'migr_err_multiple_source_scope_svc_found', 'fr_CA',
'FR - Migration Config Err : multiple source scope services found with name : "{0}"',
 sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
values(7,'migr_err_multiple_source_scope_svc_found', 'ja_JP',
'JP - Migration Config Err : multiple source scope services found with name : "{0}"',
 sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 values(7,'migr_err_multiple_dest_scope_svc_found', 'en_CA',
 'Migration Config Err : multiple destination scope services found with name : "{0}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 values(7,'migr_err_multiple_dest_scope_svc_found', 'fr_CA',
 'FR - Migration Config Err : multiple destination scope services found with name : "{0}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
 values(7,'migr_err_multiple_dest_scope_svc_found', 'ja_JP',
 'JP - Migration Config Err : multiple destination scope services found with name : "{0}"',
  sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_source_scope_svc', 'en_CA',
 'Migration Config Err : cannot find source scope svc name : "{0}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_source_scope_svc', 'fr_CA',
 'FR - Migration Config Err : cannot find source scope svc name : "{0}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_source_scope_svc', 'ja_JP',
 'JP - Migration Config Err : cannot find source scope svc name : "{0}"',
  sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_dest_scope_svc', 'en_CA',
 'Migration Config Err : cannot find destination scope svc name : "{0}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_dest_scope_svc', 'fr_CA',
 'FR - Migration Config Err : cannot find destination scope svc name : "{0}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_find_dest_scope_svc', 'ja_JP',
 'JP - Migration Config Err : cannot find destination scope svc name : "{0}"',
  sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_downgrade', 'en_CA',
 'Cannot migrate from svc : "{0}"  to svc : "{1}" because it has  {2,number,integer} instances \
  of child svc : "{3}" while after migration  it would be allowed to have only {4,number,integer}\
  instances of child svc : "{5}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_downgrade', 'fr_CA',
 'FR - Cannot migrate from svc : "{0}"  to svc : "{1}" because it has  {2,number,integer} instances \
  of child svc : "{3}" while after migration  it would be allowed to have only {4,number,integer}\
  instances of child svc : "{5}"',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'migr_err_cannot_downgrade', 'ja_JP',
 'JP - Cannot migrate from svc : "{0}"  to svc : "{1}" because it has  {2,number,integer} instances \
  of child svc : "{3}" while after migration  it would be allowed to have only {4,number,integer}\
  instances of child svc : "{5}"',
  sysdate,'INIT',null,null);

---------

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'PRCERR_REQUEST_DATAOUTOFSYNC_SUB_LOCKED', 'en_CA',
 'Sub is locked. Not able to process now! Please try again later.',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'PRCERR_REQUEST_DATAOUTOFSYNC_SUB_LOCKED', 'fr_CA',
 'FR - Sub is locked. Not able to process now! Please try again later.',
  sysdate,'INIT',null,null);

INSERT INTO LBL(CLASS_ID,OBJ_NM,LOCALE_CD,LBL_TXT,CREATED_DTM,CREATED_BY,MODIFIED_BY,MODIFIED_DTM)
  values(7,'PRCERR_REQUEST_DATAOUTOFSYNC_SUB_LOCKED', 'ja_JP',
 'JP - Sub is locked. Not able to process now! Please try again later.',
  sysdate,'INIT',null,null);
