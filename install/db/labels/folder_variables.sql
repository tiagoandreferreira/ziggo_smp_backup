--=============================================================================
--
--  FILE INFO
--    $Id: folder_variables.sql,v 1.2 2001/10/03 18:22:41 davidc Exp $
--
--    SAMP 1.0
--    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
--    All rights reserved.
--
--  DESCRIPTION
--
--    -
--
--  REVISION HISTORY
--  *  Based on CVS log
--=============================================================================
------------------------------------------------------------------
---------   Data fill for table: Lbl   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl   ---------')
BEGIN
Add_Lbl(
p_class_id => Get_Class_Id('Folder'),
p_obj_nm => 'trouble_tickets',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Trouble Ticket(s)',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Folder'),
p_obj_nm => 'selected_service',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Selected Service',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Folder'),
p_obj_nm => 'service_advisory',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Advisory',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Folder'),
p_obj_nm => 'subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber',
p_created_by => 'INIT');
END;
/
