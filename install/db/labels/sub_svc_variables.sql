--=============================================================================
--
--  FILE INFO
--    $Id: sub_svc_variables.sql,v 1.3 2001/10/03 18:22:41 davidc Exp $
--
--    SAMP 1.0
--    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
--    All rights reserved.
--
--  DESCRIPTION
--
--    -
--
--  REVISION HISTORY
--  *  Based on CVS log
--=============================================================================
------------------------------------------------------------------
---------   Data fill for table: Lbl   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl   ---------')
BEGIN
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'service_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'diagnostic_action_group',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Diagnostic Action Group',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'diagnostic_svc_action',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Diagnostic Svc Action',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'test_run',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Run',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Status',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'test_result',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Result',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'test_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'test_status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Status',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'start_date',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Start Date',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'end_date',
p_locale_cd => 'en_CA',
p_lbl_txt => 'End Date',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'test_action_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Test Action Id',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'parameter_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Parameter Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'parameter_value',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Parameter Value',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'parameter_status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Parameter Status',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'hsd',
p_locale_cd => 'en_CA',
p_lbl_txt => 'High Speed Data',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'sec_pc',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Additional IP',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Sub_Svc'),
p_obj_nm => 'email',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Email',
p_created_by => 'INIT');
END;
/
