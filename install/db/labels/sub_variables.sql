--=============================================================================
--
--  FILE INFO
--    $Id: sub_variables.sql,v 1.2 2001/10/03 18:22:41 davidc Exp $
--
--    SAMP 1.0
--    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
--    All rights reserved.
--
--  DESCRIPTION
--
--    -
--
--  REVISION HISTORY
--  *  Based on CVS log
--=============================================================================
------------------------------------------------------------------
---------   Data fill for table: Lbl   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl   ---------')
BEGIN
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'subscriber_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'telephone_number',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Telephone Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'company-account_number',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Company - Account Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'co',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Company Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'acct',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Account Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contact_email',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Contact Email',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Address',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'postal_code',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Postal Code',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'subscriber',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Subscriber',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.name.salutation',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Title',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.name.first',
p_locale_cd => 'en_CA',
p_lbl_txt => 'First Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.name.middle',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Middle Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.name.last',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Last Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.name.suffix',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Suffix',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.emails.name.address',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Primary ID',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.phones.name.number',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Telephone Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'contacts.primary.phones.name.ext',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Telephone Ext',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.street_no',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Street Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.city',
p_locale_cd => 'en_CA',
p_lbl_txt => 'City',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.province',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Province',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.country',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Country',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.street_suffix',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Street Suffix',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.street_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Street Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.street_type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Street Type',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Parm'),
p_obj_nm => 'address.home.apt_no',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Apt',
p_created_by => 'INIT');
END;
/

