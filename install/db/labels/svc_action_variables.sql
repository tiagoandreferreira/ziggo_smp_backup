--=============================================================================
--
--  FILE INFO
--    $Id: svc_action_variables.sql,v 1.2 2001/10/03 18:22:41 davidc Exp $
--
--    SAMP 1.0
--    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
--    All rights reserved.
--
--  DESCRIPTION
--
--    -
--
--  REVISION HISTORY
--  *  Based on CVS log
--=============================================================================
------------------------------------------------------------------
---------   Data fill for table: Lbl   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl   ---------')
BEGIN
Add_Lbl(
p_class_id => Get_Class_Id('Svc_Action'),
p_obj_nm => 'hsd_low_intensity_test',
p_locale_cd => 'en_CA',
p_lbl_txt => 'HSD Low Intensity Test',
p_created_by => 'INIT');
END;
/



