--=============================================================================
--
--  FILE INFO
--    $Id: test_result_status_variables.sql,v 1.2 2001/10/03 18:22:42 davidc Exp $
--
--    SAMP 1.0
--    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
--    All rights reserved.
--
--  DESCRIPTION
--
--    -
--
--  REVISION HISTORY
--  *  Based on CVS log
--=============================================================================
------------------------------------------------------------------
---------   Data fill for table: Lbl   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl   ---------')
BEGIN
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 's',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Pass',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 'i',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Impaired',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 'f',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Fail',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 't',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Time Out',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 'p',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Pass',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 'u',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Unclassified',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Ref_Test_Result_Status'),
p_obj_nm => 'x',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Error',
p_created_by => 'INIT');
END;
/
