--=============================================================================
--
--  FILE INFO
--    $Id: trouble_ticket_variables.sql,v 1.2 2001/10/02 20:48:43 veliborm Exp $
--
--    SAMP 1.0
--    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
--    All rights reserved.
--
--  DESCRIPTION
--
--    -
--
--  REVISION HISTORY
--  *  Based on CVS log
--=============================================================================
------------------------------------------------------------------
---------   Data fill for table: Lbl   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Lbl   ---------')
BEGIN
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'trouble_ticket_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Trouble Ticket ID',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'svc_advisory_id',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Service Advisory ID',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'status',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Status',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'desc',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Description',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'created_by',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created By',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'create_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Created Date',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'closed_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Closed Date',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'last_modified_by',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Last Modified By',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'modified_dtm',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Modified Date',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'acct',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Account Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'co',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Company Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'topology_type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Topology Type',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'topology_name',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Topology Name',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'cim_ticket',
p_locale_cd => 'en_CA',
p_lbl_txt => 'CIM Ticket',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'related_wsr',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Related WSR',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'failure_ticket',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Failure Ticket',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'assigned_to',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Assigned To',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'category',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Category',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Type',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'resolution_category',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Resolution Category',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'resolution_type',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Resolution Type',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'chronic',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Chronic',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'referred_to',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Referred To',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'province',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Province',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'shub',
p_locale_cd => 'en_CA',
p_lbl_txt => 'SHUB',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'smt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'SMT',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'street',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Street',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'street_number',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Street Number',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'apt',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Apt#',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'city',
p_locale_cd => 'en_CA',
p_lbl_txt => 'City',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'pop',
p_locale_cd => 'en_CA',
p_lbl_txt => 'POP',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'work_log',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Work Log',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'outage_start',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Outage Start',
p_created_by => 'INIT');
Add_Lbl(
p_class_id => Get_Class_Id('Trouble_Ticket'),
p_obj_nm => 'outage_end',
p_locale_cd => 'en_CA',
p_lbl_txt => 'Outage End',
p_created_by => 'INIT');
END;
/
