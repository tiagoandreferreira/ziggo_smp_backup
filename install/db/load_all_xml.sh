#!/usr/bin/ksh
#==============================================================================
#
#  FILE INFO
#    $Id: load_all_xml.sh,v 1.1 2007/11/29 21:15:28 siarheig Exp $
#
#  DESCRIPTION
#  
#    Make sure that any script you call/invoke from here has it's output
#    also copied into the $LOG_FILE. Otherwise, the ERROR detection at the 
#    end won't work and you get confusing results.
#
#  REVISION HISTORY
#  * Based on CVS
#==============================================================================
LOG_FILE=load_all_xml.log
. ../../util/setEnv.sh

BASE_DB_DIR=.

if [ -r $BASE_DB_DIR/../../util/setEnv.sh ]
then
    . $BASE_DB_DIR/../../util/setEnv.sh
else
    echo Cannot find $SRC_ROOT/util/setEnv.sh
    exit 1
fi

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    echo $USAGE
    exit 1
fi

LOGIN=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}

#-------------------------------------------------------------------------
# Load the xml configuration into CM
#-------------------------------------------------------------------------
echo .
echo Loading DM xml configuration...
CUR_DIR=`pwd`
cd $BASE_DB_DIR/../../cfg
for f in *.xml *.xsd; do 
   ../tools/xmlload.sh $LOGIN $f          | tee -a $LOG_FILE
done
