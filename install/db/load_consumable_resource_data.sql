--==============================================================================
--  $Id: load_consumable_resource_data.sql,v 1.2 2008/03/27 15:13:45 michaell Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--      sql commands to setup consumable resources
--  REVISION HISTORY
--  * Based on CVS log
--==============================================================================

SPOOL load_consumable_resource_data.log

set escape on
set serveroutput on

DECLARE
    CR_SPEC_SEQ_EXIST NUMBER;
BEGIN
SELECT count(*) INTO CR_SPEC_SEQ_EXIST FROM user_sequences WHERE SEQUENCE_NAME = 'CONSUMABLERESOURCESPEC_SEQ';
IF CR_SPEC_SEQ_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- CONSUMABLERESOURCESPEC_SEQ exists before, it will be dropped ---');
	EXECUTE IMMEDIATE 'DROP SEQUENCE CONSUMABLERESOURCESPEC_SEQ';
END IF;
	CR_SPEC_SEQ_EXIST := NULL;
END;
/


EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Creating consumable resource sequence   ---------')

CREATE SEQUENCE CONSUMABLERESOURCESPEC_SEQ START WITH 20000 INCREMENT BY 50 MINVALUE 1 CACHE 50 NOCYCLE  NOORDER ;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Creating consumable resource on REF_CLASS has started   ---------')

DECLARE
    max_pr_key NUMBER(12);
    v_count_ref_class NUMBER;
BEGIN
    SELECT COUNT(*) 
        INTO v_count_ref_class 
        FROM REF_CLASS
        WHERE CLASS_NM = 'ConsumableResourceSpec';
    IF (v_count_ref_class < 1) THEN
        --SELECT nvl(max(CLASS_ID), 1) INTO max_pr_key FROM REF_CLASS;
        
        INSERT INTO REF_CLASS(CLASS_ID, MAX_OBJ_ID, CLASS_NM, CREATED_DTM, CREATED_BY)
            VALUES (2000, 1, 'ConsumableResourceSpec', SYSDATE,'INIT' );
        max_pr_key := NULL;
        v_count_ref_class := NULL;
    END IF;
    SELECT COUNT(*) 
        INTO v_count_ref_class 
        FROM REF_CLASS
        WHERE CLASS_NM = 'ConsumableResource';
    IF (v_count_ref_class < 1) THEN
        -- SELECT nvl(max(CLASS_ID), 1) INTO max_pr_key FROM REF_CLASS;

        INSERT INTO REF_CLASS(CLASS_ID, MAX_OBJ_ID, CLASS_NM, CREATED_DTM, CREATED_BY)
            VALUES (2001, 1, 'ConsumableResource', SYSDATE,'INIT' );
        max_pr_key := NULL;
        v_count_ref_class := NULL;
    END IF;
END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Creating consumable resource on REF_CLASS has finished   ---------')

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Creating REF_STATUS_TAI has started   ---------')
CREATE OR REPLACE TRIGGER REF_STATUS_TAI
	AFTER INSERT
	ON REF_STATUS

FOR EACH ROW
    DECLARE

    v_class_nm ref_class.CLASS_NM%type;

    BEGIN

    SELECT ref_class.CLASS_NM INTO v_class_nm FROM ref_class

        WHERE ref_class.CLASS_ID=:NEW.class_id;

    IF (v_class_nm='SubSpec') THEN

        INSERT INTO REF_SUB_STATUS(SUB_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='SubSvcSpec') THEN

        INSERT INTO REF_SUB_SVC_STATUS(SUB_SVC_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='Order') THEN

        INSERT INTO REF_ORDR_STATUS(ORDR_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='OrderItem') THEN

        INSERT INTO REF_ORDR_ITEM_STATUS(ORDR_ITEM_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='SubContactSpec') THEN

        INSERT INTO REF_CONTACT_STATUS(CONTACT_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='SubAddressSpec') THEN

        INSERT INTO REF_ADDR_STATUS(ADDR_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='SubUserSpec') THEN

        INSERT INTO REF_USER_STATUS(USER_STATUS_ID) VALUES(:NEW.status_id);

    ELSIF (v_class_nm='ConsumableResourceSpec') THEN

        INSERT INTO REF_CONSUMABLE_RESOURCE_STATUS(consumable_rsrc_status_id) VALUES(:NEW.status_id);

    END IF;

END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Creating REF_STATUS_TAI has finished   ---------')
    

--REF_STATUS
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  status has started  ---------')
DECLARE 
    v_class_id NUMBER(12);
    v_count_ref_class NUMBER;
    v_count_ref_status NUMBER;
    v_max_status_id NUMBER(12);
BEGIN
    SELECT count(*) 
        INTO v_count_ref_class 
        FROM ref_class 
        WHERE class_nm = 'ConsumableResourceSpec';
    IF (v_count_ref_class = 1) THEN
        SELECT class_id 
            INTO v_class_id 
            FROM ref_class 
            WHERE class_nm = 'ConsumableResourceSpec';
        -- add_in_progress       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'add_in_progress';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
         
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'add_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- active       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'active';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'active',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);         
        END IF;
        
        -- deleted       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'deleted';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'deleted',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
        END IF;
        
        -- activation_in_progress       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'activation_in_progress';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'activation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- delete_in_progress       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'delete_in_progress';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'delete_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
        END IF;
        
        -- deactivation_in_progress       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'deactivation_in_progress';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'deactivation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
        END IF;
        
        -- previous_state       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'previous_state';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- change_in_progress       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'change_in_progress';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'change_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- inactive       
        SELECT COUNT(*) 
            INTO v_count_ref_status
            FROM Ref_Status
            WHERE class_id = v_class_id
                AND status = 'inactive';
        
        IF (v_count_ref_status < 1) THEN
            SELECT NVL(MAX(status_id), 1) 
                INTO v_max_status_id             
                FROM REF_STATUS;
            Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
                Values ((v_max_status_id + 1),v_class_id,'inactive',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);
        END IF;
        INSERT INTO REF_CONSUMABLE_RESOURCE_STATUS(SELECT status_id  FROM ref_status WHERE class_id = v_class_id);            
    END IF;
    
END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  status has finished   ---------')


--actions
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  actions has started  ---------')
DECLARE 
    v_class_id NUMBER(12);
    v_max_state_action_id NUMBER(12);
    v_count_ref_class NUMBER;
    v_count_state_action NUMBER;
BEGIN
    SELECT count(*) 
        INTO v_count_ref_class 
        FROM ref_class 
        WHERE class_nm = 'ConsumableResourceSpec';
    IF (v_count_ref_class = 1) THEN
        SELECT class_id 
            INTO v_class_id 
            FROM ref_class 
            WHERE class_nm = 'ConsumableResourceSpec';
        
        -- activate
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'activate';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
                VALUES ((v_max_state_action_id + 1),v_class_id,'activate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;
    
        -- deactivate
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'deactivate';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
                VALUES ((v_max_state_action_id + 1),v_class_id,'deactivate',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- delete
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'delete';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
            VALUES ((v_max_state_action_id + 1),v_class_id,'delete',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- item_completed
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'item_completed';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
            VALUES ((v_max_state_action_id + 1),v_class_id,'item_completed',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- item_cancelled
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'item_cancelled';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
            VALUES ((v_max_state_action_id + 1),v_class_id,'item_cancelled',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- update
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'update';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
            VALUES ((v_max_state_action_id + 1),v_class_id,'update',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- add
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'add';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
            VALUES ((v_max_state_action_id + 1),v_class_id,'add',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;

        -- item_rejected
        SELECT COUNT(*) 
            INTO v_count_state_action
            FROM STATE_ACTION
            WHERE class_id = v_class_id
                AND action = 'item_rejected';
        
        IF (v_count_state_action < 1) THEN
            SELECT NVL(MAX(state_action_id), 1) 
                INTO v_max_state_action_id             
                FROM STATE_ACTION;
            INSERT INTO STATE_ACTION(state_action_id, class_id, action, svc_id, descr, created_dtm, created_by, modified_dtm, modified_by)
            VALUES ((v_max_state_action_id + 1),v_class_id,'item_rejected',NULL,NULL, SYSDATE, 'INIT', NULL, NULL);
        END IF;
                                                            
    END IF;
END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  actions has finished  ---------')


--- state transitions
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  state transitions  has started ---------')
DECLARE
    v_max_state_trans_id NUMBER(12);
    v_from_status_id NUMBER(12);
    v_to_status_id NUMBER(12);
    v_state_action_id NUMBEr(12);
    v_count_ref_class NUMBER;
    v_count_state_transition NUMBER;
    v_class_id NUMBER(12);
BEGIN
    SELECT count(*) 
        INTO v_count_ref_class 
        FROM ref_class 
        WHERE class_nm = 'ConsumableResourceSpec';
    IF (v_count_ref_class = 1) THEN
        SELECT class_id 
            INTO v_class_id 
            FROM ref_class 
            WHERE class_nm = 'ConsumableResourceSpec';

        -- add_in_progress -> active    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'add_in_progress';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'active';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_completed';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN        
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        

        -- add_in_progress -> deleted    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'add_in_progress';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'deleted';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_cancelled';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        
            
        -- active -> delete_in_progress    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'active';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'delete_in_progress';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'delete';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        

        -- delete_in_progress -> deleted    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'delete_in_progress';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'deleted';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_completed';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                        
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        

        -- delete_in_progress -> previous_state    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'delete_in_progress';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'previous_state';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_cancelled';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        
        
        -- active -> change_in_progress    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'active';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'change_in_progress';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'update';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
                        SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        
        
        -- change_in_progress -> previous_state    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'change_in_progress';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'previous_state';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_completed';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        

        -- change_in_progress -> previous_state    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'change_in_progress';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'previous_state';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_cancelled';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        
                
        -- inactive -> add_in_progress    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'inactive';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'add_in_progress';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'add';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;        

        -- inactive -> deleted    
        SELECT status_id
            INTO v_from_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'inactive';
        
        SELECT status_id
            INTO v_to_status_id
            FROM REF_STATUS 
            WHERE class_id = v_class_id
                AND status = 'add_in_progress';
            
        SELECT state_action_id
            INTO v_state_action_id
            FROM STATE_ACTION 
            WHERE class_id = v_class_id
                AND action = 'item_rejected';
        IF ( v_from_status_id > 0 AND v_to_status_id > 0 AND v_state_action_id > 0  ) THEN
            SELECT COUNT(*) 
                INTO v_count_state_transition
                FROM STATE_TRANSITION
                WHERE from_status_id = v_from_status_id
                    AND to_status_id = v_to_status_id
                    AND state_action_id = v_state_action_id;
            IF (v_count_state_transition < 1) THEN                
                SELECT nvl(max(state_transition_id), 1)
                    INTO v_max_state_trans_id
                    FROM State_Transition;
                    
                    INSERT INTO STATE_TRANSITION(state_transition_id, scope_svc_id, from_status_id, to_status_id, cond, state_action_id, created_dtm, created_by, modified_dtm, modified_by)
                    VALUES ((v_max_state_trans_id +1),NULL,v_from_status_id,v_to_status_id,NULL,v_state_action_id, SYSDATE, 'INIT', NULL, NULL);
            END IF;
            v_from_status_id := NULL;
            v_to_status_id := NULL;
            v_state_action_id := NULL;
        END IF;  
   END IF;
END;
/      
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  state transitions  has finished ---------')
        

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  reason code/desc  has started ---------')
DECLARE 
    v_class_id NUMBER(12);
    v_max_reason_id NUMBER(12);
    v_count_ref_class NUMBER;
    v_count_ref_sts_chg_reason NUMBER;
BEGIN
    SELECT count(*) 
        INTO v_count_ref_class 
        FROM ref_class 
        WHERE class_nm = 'ConsumableResourceSpec';
    IF (v_count_ref_class = 1) THEN
        SELECT class_id 
            INTO v_class_id 
            FROM ref_class 
            WHERE class_nm = 'ConsumableResourceSpec';
        
        SELECT COUNT(*) 
            INTO v_count_ref_sts_chg_reason
            FROM REF_STATUS_CHG_REASON
            WHERE class_id = v_class_id
                AND reason_nm = 'default';
        
        IF (v_count_ref_sts_chg_reason < 1) THEN
            SELECT NVL(MAX(reason_id), 1) 
                INTO v_max_reason_id             
                FROM REF_STATUS_CHG_REASON;
                
            INSERT INTO REF_STATUS_CHG_REASON(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
                VALUES ((v_max_reason_id + 1),'default',NULL,'y',v_class_id, SYSDATE, 'INIT', NULL, NULL);
        END IF;
    END IF;
END;    
/                
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading  consumable resource  reason code/desc  has finished ---------')


-- REF_CLASS 
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading REF_CLASS for Consumable Resource  has started ---------')
DECLARE 
    v_count_ref_class NUMBER;
    v_max_class_id NUMBER(12);
BEGIN    
    SELECT COUNT(*) 
        INTO v_count_ref_class
        FROM ref_class
        WHERE class_nm = 'stm_resource';
    
    IF (v_count_ref_class < 1) THEN
        SELECT NVL(MAX(class_id), 1) 
            INTO v_max_class_id             
            FROM REF_CLASS;
                        
        INSERT INTO ref_class(class_id, max_obj_id, class_nm, created_by) VALUES ((v_max_class_id + 1), 1, 'stm_resource', 'INIT');        
    END IF;
END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading REF_CLASS for Consumable Resource  has finished ---------')
       
    

-- PARM
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Adding needed parameter to PARM for Consumable Resource has started  ---------')
DECLARE 
    v_ref_class_type NUMBER(12);
    v_count_ref_class NUMBER;
    v_count_porting_status NUMBER;
    v_max_parm_id NUMBER(12);
BEGIN
    SELECT count(*)
        INTO v_count_ref_class
        FROM REF_CLASS
        WHERE class_nm = 'stm_resource';
    IF (v_count_ref_class =  1) THEN
        SELECT class_id
            INTO v_ref_class_type
            FROM REF_CLASS
            WHERE class_nm = 'stm_resource';
            
        SELECT COUNT(*)
            INTO v_count_porting_status
            FROM PARM 
            WHERE parm_nm = 'portingStatus'
                AND class_id = v_ref_class_type;
        IF (v_count_porting_status < 1 ) THEN
            SELECT nvl(max(parm_id), 1)
                INTO v_max_parm_id
                FROM PARM;
                
            INSERT INTO PARM(parm_id, parm_nm, class_id, data_typ_nm, created_by ) VALUES((v_max_parm_id + 1),'portingStatus', v_ref_class_type, 'String', 'INIT' );
        END IF;
    END IF;
END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Adding needed parameter to PARM for Consumable Resource has finished  ---------')

-- REF_CONSUMABLE_RSRC_TYP
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading REF_CONSUMABLE_RSRC_TYP for Consumable Resource has started   ---------')
DECLARE 
    v_count_cons_res_type NUMBER;
BEGIN
    SELECT COUNT(*) 
        INTO v_count_cons_res_type
        FROM REF_CONSUMABLE_RSRC_TYP
        WHERE CONSUMABLE_RSRC_TYP_NM = 'Telephone Number';
    IF (v_count_cons_res_type < 1) THEN
        INSERT INTO REF_CONSUMABLE_RSRC_TYP (CONSUMABLE_RSRC_TYP_NM, MODIFIED_BY, MODIFIED_DTM, CREATED_BY, CREATED_DTM) VALUES( 'Telephone Number', NULL, NULL, 'INIT', SYSDATE);
        v_count_cons_res_type := NULL;
    END IF;    

    SELECT COUNT(*) 
        INTO v_count_cons_res_type
        FROM REF_CONSUMABLE_RSRC_TYP
        WHERE CONSUMABLE_RSRC_TYP_NM = 'Extension';
    IF (v_count_cons_res_type < 1) THEN
        INSERT INTO REF_CONSUMABLE_RSRC_TYP (CONSUMABLE_RSRC_TYP_NM, MODIFIED_BY, MODIFIED_DTM, CREATED_BY, CREATED_DTM) VALUES( 'Extension', NULL, NULL, 'INIT', SYSDATE);
        v_count_cons_res_type := NULL;
    END IF;    

    SELECT COUNT(*)
        INTO v_count_cons_res_type
        FROM REF_CONSUMABLE_RSRC_TYP
        WHERE CONSUMABLE_RSRC_TYP_NM = 'LEN';
    IF (v_count_cons_res_type < 1) THEN
        INSERT INTO REF_CONSUMABLE_RSRC_TYP (CONSUMABLE_RSRC_TYP_NM, MODIFIED_BY, MODIFIED_DTM, CREATED_BY, CREATED_DTM) VALUES( 'LEN', NULL, NULL, 'INIT', SYSDATE);
        v_count_cons_res_type := NULL;
    END IF;

END;
/
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Loading REF_CONSUMABLE_RSRC_TYP for Consumable Resource has finished  ---------')
spool off


