--==============================================================================
--  $Id: load_consumable_resource_scripts.sql,v 1.7 2012/03/28 16:01:12 bharath Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--      sql commands to setup consumable resources
--  REVISION HISTORY
--  * Based on CVS log
--==============================================================================
SPOOL load_consumable_resource_scripts.log
set serveroutput on


DECLARE
    TBL_REF_CR_STS_EXIST NUMBER;
BEGIN
SELECT count(*) INTO TBL_REF_CR_STS_EXIST FROM user_tables WHERE table_name = 'REF_CONSUMABLE_RESOURCE_STATUS';
IF TBL_REF_CR_STS_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- REF_CONSUMABLE_RESOURCE_STATUS exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE REF_CONSUMABLE_RESOURCE_STATUS CASCADE CONSTRAINTS';
END IF;
        TBL_REF_CR_STS_EXIST := NULL;
END;
/

DECLARE
    TBL_CR_PARM_EXIST NUMBER;
BEGIN
SELECT count(*) INTO TBL_CR_PARM_EXIST FROM user_tables WHERE table_name = 'CONSUMABLE_RESOURCE_PARM';
IF TBL_CR_PARM_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- CONSUMABLE_RESOURCE_PARM exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE CONSUMABLE_RESOURCE_PARM CASCADE CONSTRAINTS';
END IF;
        TBL_CR_PARM_EXIST := NULL;
END;
/


DECLARE
    TBL_CR_EXIST  NUMBER;
BEGIN
SELECT count(*) INTO TBL_CR_EXIST FROM user_tables WHERE table_name = 'CONSUMABLE_RESOURCE';
if TBL_CR_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- CONSUMABLE_RESOURCE_PARM exists before, it will be deleted ---');
    EXECUTE IMMEDIATE 'DROP TABLE CONSUMABLE_RESOURCE CASCADE CONSTRAINTS';
end if;
TBL_CR_EXIST := NULL;
END;
/


EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Table, Index and PR Key creation for Consumable Resource---------');

CREATE TABLE CONSUMABLE_RESOURCE (
    consumable_rsrc_id                  NUMBER(12) NOT NULL,
    samp_ver                            NUMBER(9) NOT NULL,
    external_key                        VARCHAR2(100) NOT NULL,
    consumable_rsrc_status_id   NUMBER(3) NOT NULL,
    object_typ_id               NUMBER(12) NOT NULL,
    created_dtm                 DATE NOT NULL,
    created_by                                  VARCHAR2(30) NOT NULL,
    pre_status_id               NUMBER(12) NULL,
    sub_id                      NUMBER(12) NULL,
    modified_dtm                DATE NULL,
    modified_by                 VARCHAR2(30) NULL
);


CREATE TABLE CONSUMABLE_RESOURCE_PARM (
        consumable_rsrc_id      NUMBER(12) NOT NULL,
        parm_id                         NUMBER(12) NOT NULL,
        val                             VARCHAR2(2000) NOT NULL
);


CREATE TABLE REF_CONSUMABLE_RESOURCE_STATUS (
    consumable_rsrc_status_id NUMBER(3) NOT NULL
);


CREATE INDEX CONSUMABLE_RESOURCE_IX1 ON CONSUMABLE_RESOURCE (
    sub_id ASC,
    consumable_rsrc_status_id ASC
    );

ALTER TABLE CONSUMABLE_RESOURCE
    ADD (CONSTRAINT CONSUMABLE_RESOURCE_PK PRIMARY KEY (consumable_rsrc_id));

CREATE UNIQUE INDEX  CONSUMABLE_RESOURCE_PARM_IX ON  CONSUMABLE_RESOURCE_PARM (
        consumable_rsrc_id,
        parm_id
);


DECLARE
    TBL_NTWK_RES_PARM_EXIST NUMBER;
BEGIN
SELECT count(*) INTO TBL_NTWK_RES_PARM_EXIST FROM user_tables WHERE table_name = 'NTWK_RESOURCE_PARM';
IF TBL_NTWK_RES_PARM_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- NTWK_RESOURCE_PARM exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE NTWK_RESOURCE_PARM CASCADE CONSTRAINTS';
END IF;
        TBL_NTWK_RES_PARM_EXIST := NULL;
END;
/


DECLARE
    TBL_NTWK_RES_EXIST NUMBER;
BEGIN
SELECT count(*) INTO TBL_NTWK_RES_EXIST FROM user_tables WHERE table_name = 'NTWK_RESOURCE';
IF TBL_NTWK_RES_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- NTWK_RESOURCE exists before, it will be deleted ---');
        EXECUTE IMMEDIATE 'DROP TABLE NTWK_RESOURCE CASCADE CONSTRAINTS';
END IF;
        TBL_NTWK_RES_EXIST := NULL;
END;
/

DECLARE
    NTWK_RES_SEQ_EXIST NUMBER;
BEGIN
SELECT count(*) INTO NTWK_RES_SEQ_EXIST FROM user_sequences WHERE SEQUENCE_NAME = 'NTWK_RESOURCE_SEQ';
IF NTWK_RES_SEQ_EXIST = 1 THEN
    DBMS_OUTPUT.PUT_LINE('--- NTWK_RESOURCE_SEQ exists before, it will be dropped ---');
        EXECUTE IMMEDIATE 'DROP SEQUENCE NTWK_RESOURCE_SEQ';
END IF;
        NTWK_RES_SEQ_EXIST := NULL;
END;
/

CREATE TABLE NTWK_RESOURCE (
       rsrc_id              number(12) NOT NULL,
       rsrc_nm              varchar2(100) NOT NULL,
       sub_svc_id           number(12) NULL,
       subntwk_id           number(12) NULL,
       consumable_rsrc_typ_nm VARCHAR2(30) NOT NULL,
       rsrc_state           varchar2(50) NOT NULL,
       rsrc_category        varchar2(50) NULL,
       created_dtm          date DEFAULT sysdate NULL,
       created_by           varchar2(30) NOT NULL,
       prev_sub_svc_id      NUMBER(12) NULL,
       prev_rsrc_state      varchar2(50) NULL,
       modified_dtm         date NULL,
       modified_by          VARCHAR2(30) NULL
);

CREATE TABLE NTWK_RESOURCE_PARM (
       rsrc_id              number(12) NOT NULL,
       parm_id              number(12) NOT NULL,
       val                  varchar2(300) NOT NULL,
       created_dtm          CHAR(18) DEFAULT sysdate NULL,
       created_by           CHAR(18) NULL,
       modified_dtm         CHAR(18) NULL,
       modified_by          CHAR(18) NULL
);

CREATE INDEX NTWK_RESOURCE_IX1 ON NTWK_RESOURCE
(
       rsrc_state                     ASC
);

CREATE INDEX NTWK_RESOURCE_IX2 ON NTWK_RESOURCE
(
       sub_svc_id                     ASC
);

CREATE INDEX NTWK_RESOURCE_IX3 ON NTWK_RESOURCE
(
       subntwk_id                     ASC
);


CREATE UNIQUE INDEX NTWK_RESOURCE_UNQ1 ON NTWK_RESOURCE
(
       rsrc_nm                        ASC,
       rsrc_category                  ASC
);

CREATE UNIQUE INDEX NTWK_RESOURCE_UNQ2 ON NTWK_RESOURCE
(
       rsrc_id                        ASC
);

CREATE UNIQUE INDEX NTWK_RESOURCE_PARM_UNQ1 ON NTWK_RESOURCE_PARM
(
       rsrc_id                        ASC,
       parm_id                        ASC
);

CREATE INDEX NTWK_RESOURCE_IX4 ON NTWK_RESOURCE
(CONSUMABLE_RSRC_TYP_NM, RSRC_CATEGORY)
LOGGING
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE NTWK_RESOURCE
       ADD  ( CONSTRAINT NTWK_RESOURCE_PK PRIMARY KEY (rsrc_id) ) ;


ALTER TABLE NTWK_RESOURCE
       ADD  ( CONSTRAINT NTWK_RESOURCE_FK1 FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE NTWK_RESOURCE
       ADD  ( CONSTRAINT NTWK_RESOURCE_FK2 FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE NTWK_RESOURCE
       ADD  ( CONSTRAINT NTWK_RESOURCE_FK3 FOREIGN KEY (consumable_rsrc_typ_nm)
                             REFERENCES REF_CONSUMABLE_RSRC_TYP ) ;


ALTER TABLE NTWK_RESOURCE_PARM
       ADD  ( CONSTRAINT NTWK_RESOURCE_PARM_PK PRIMARY KEY (rsrc_id, parm_id) ) ;


ALTER TABLE NTWK_RESOURCE_PARM
       ADD  ( CONSTRAINT NTWK_RESOURCE_PARM_FK1 FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE NTWK_RESOURCE_PARM
       ADD  ( CONSTRAINT NTWK_RESOURCE_PARM_FK2 FOREIGN KEY (rsrc_id)
                             REFERENCES NTWK_RESOURCE ) ;



CREATE SEQUENCE NTWK_RESOURCE_SEQ
START WITH 1
NOMAXVALUE
NOMINVALUE
NOCYCLE
NOORDER;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Table, Index and PR Key creation for Consumable Resource---------');
spool off
