#!/usr/bin/ksh
#==============================================================================
#
#  FILE INFO
#    $Id: load_table.sh,v 1.1 2007/11/29 21:15:28 siarheig Exp $
#
#  DESCRIPTION
#
#  REVISION HISTORY
#  * Based on CVS
#==============================================================================

_AWK_SCRIPT='
BEGIN { 
  mystart=0 
}

/^Insert into SORB_Parm[^_a-zA-Z]/ {
  mystart=1 ;
}

/^Delete from SORB_Parm[^_a-zA-Z]/ {
  print $0
}

{
  if (mystart == 1){
    print $0 ;
    if (match($0,"^Values")) {
      mystart = 0 ;
    }
  }
}
'

_AWK_SCRIPT=`echo $_AWK_SCRIPT | sed "s/SORB_Parm/${1}/g"`
#echo $_AWK_SCRIPT
cat sd_load_all.sql | nawk "$_AWK_SCRIPT"

