--============================================================================
--    $Id: load_topology.sql,v 1.10 2011/09/28 18:24:35 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

spool load_topology.log

set echo off
set serveroutput on

----------------------------------
---- Solution Sample Topology ----
----------------------------------

EXECUTE DBMS_OUTPUT.PUT_LINE('--------- Solution Sample Topology ---------');

@topology/root_network.sql
@topology/video_root_network.sql
@topology/area_code_network.sql
@topology/residential_lec_network.sql
@topology/residential_ncs_dqos_super_region.sql
@topology/residential_ncs_dqos_voice_network.sql
@topology/residential_ncs_dqos_lec_network.sql
@topology/residential_ncs_dqos_video_network.sql
@topology/residential_sip_video_root_network.sql
-- Broadsoft SIP for residential is out of scope in Solution 3.6, so that it's commented out
--@topology/residential_sip_super_region.sql
--@topology/residential_sip_voice_network.sql
--@topology/residential_sip_lec_network.sql
--@topology/residential_sip_video_network.sql 
-- Siemens SIP for residential
@topology/residential_siemens_sip_super_region.sql
@topology/residential_siemens_sip_voice_network.sql
@topology/residential_siemens_sip_video_network.sql
@topology/residential_nortel_sip_super_region.sql
@topology/residential_nortel_sip_voice_network.sql
@topology/residential_nortel_sip_lec_network.sql
@topology/residential_nortel_sip_video_network.sql
@topology/isp_layer.sql
@topology/commercial_lec_network.sql
@topology/commercial_ncs_dqos_super_region.sql
@topology/commercial_ncs_dqos_voice_network.sql
@topology/commercial_ncs_dqos_lec_network.sql
@topology/commercial_ncs_dqos_video_network.sql
@topology/commercial_sip_video_root_network.sql
@topology/commercial_sip_super_region.sql
@topology/commercial_sip_voice_network.sql
@topology/commercial_sip_lec_network.sql
@topology/commercial_sip_video_network.sql
@topology/commercial_nortel_sip_super_region.sql
@topology/commercial_nortel_sip_voice_network.sql
@topology/commercial_nortel_sip_lec_network.sql
@topology/commercial_nortel_sip_video_network.sql
@topology/nortel_international_switch.sql
@topology/commercial_nortel_dms_super_region.sql
@topology/commercial_nortel_dms_voice_network.sql
@topology/residential_nortel_dms_super_region.sql
@topology/residential_nortel_dms_voice_network.sql
