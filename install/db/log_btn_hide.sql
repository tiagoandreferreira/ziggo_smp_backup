--============================================================================
--    $Id: log_btn_hide.sql,v 1.1 2011/12/14 15:28:06 prithvim Exp $
--  For adding User Entity
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
exec am_add_secure_obj('samp.web.orders.view_log_analyzer', 'Log  Button');
exec am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.view_log_analyzer', 'y', 'csr_admin', 'edit');

commit;
