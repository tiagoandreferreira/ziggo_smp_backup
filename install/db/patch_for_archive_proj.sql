--=============================================================================
--    $Id: patch_for_archive_proj.sql,v 1.2 2002/09/05 22:22:05 scotta Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

alter table proj_tmplt modify proj_tmplt_nm varchar2(50);
alter table proj_tmplt_parm modify proj_tmplt_nm varchar2(50);
alter table tasks_in_proj_tmplt modify proj_tmplt_nm varchar2(50);
alter table task_tmplt_depy modify proj_tmplt_nm varchar2(50);
alter table proj_tmplt_impl modify proj_tmplt_nm varchar2(50);

alter table proj_tmplt_impl modify proj_tmplt_impl_nm varchar2(50);
alter table proj_tmplt_impl_parm modify proj_tmplt_impl_nm varchar2(50);
alter table proj modify proj_tmplt_impl_nm varchar2(50);

alter table src_action_parm modify scope_proj_tmplt_nm varchar2(50);

