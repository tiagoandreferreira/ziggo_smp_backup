/*=========================================================================

  FILE INFO

    $Id: pkg_arch_92.sql,v 1.14 2005/10/28 15:51:12 dant Exp $

  DESCRIPTION

    PKG_ARCH is a set of procedure to setup and execute SMP data
    archiving and purging


  NOTES
  Prc_run_arch procedure is included in this package, but it is recommended
  to execute routine archiving activities through standalon procedure
  Prc_run_arch , this allows to restrict access to admin functions

  RELATED SCRIPTS
  cxt_syste.sql is required for this script
  pkg_arch_obj.sql is required for this script


  REVISION HISTORY
  * Based on CVS log
========================================================================*/

create or replace package PKG_ARCH as

  TYPE t_parm_typ IS TABLE OF VARCHAR2(255) INDEX BY VARCHAR2(64);
  v_parm t_parm_typ;

  type rc is ref cursor;
  type t_var is table of varchar2(30);
  l_tabs     t_var;
  g_newseq     number;

  type rec_constraint is record (
    TABLE_NAME         VARCHAR2(30),
    DELETE_RULE        VARCHAR2(9),
    CONSTRAINT_NAME    VARCHAR2(30),
    OWNER              VARCHAR2(30),
    R_CONSTRAINT_NAME  VARCHAR2(30),
    R_OWNER            VARCHAR2(30));

ind number:=1;

  procedure Prc_run_arch(p_grp_nm varchar2 default null);
  -- PURPOSE
  --  To perform data regular archiving
  -- INPUT PARAMETERS
  --  p_grm_nm - optional archiving group name. Procedure archives all groups
  --    if none is specified
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_create_grp(p_xml_cfg CLOB, p_new_owner varchar2 default null);
  -- PURPOSE
  --   create archiving group from XML
  -- INPUT PARAMETERS
  --   p_xml_cfg - group profile
  --   p_new_owner - deploy configuration to a new owner
  -- OUTPUT PARAMETERS
  -- EXCEPTION
  -- ORA 00001 when the group already exists
  procedure Prc_add_group(p_grp_nm varchar2);
  -- PURPOSE
  --   create a new archiving group
  -- INPUT PARAMETERS
  --   p_grp_nm - new group name
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  -- ORA 00001 when the group exists

  procedure Prc_add_tbl(p_grp_nm varchar2, p_tbl_nm varchar2);
  -- PURPOSE
  --  add table to the archiving group
  --   p_grp_nm - group name
  --   p_tbl_nm - new table name
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  -- ORA 00001 when the table already in the group

  procedure Prc_add_tbl_depy(p_grp_nm varchar2,
                             p_src_tbl_nm varchar2,
                             p_tgt_tbl_nm varchar2,
                             p_arch_mode varchar2,
                             p_src_ref varchar2,
                             p_tgt_ref varchar2,
                             p_role varchar2,
                             p_join_clause varchar2,
                             p_tgt_typ   varchar2) ;
  -- PURPOSE
  --   add table dependency. dependency navigates from source to target table
  --   to build consistent dataset
  -- INPUT PARAMETERS
  --   p_grp_nm      - archive group name
  --   p_src_tbl_nm  - source table name, all dependency are navigated from
  --                   source  to target, starting with a root table
  --   p_tgt_tbl_nm  - target name
  --   p_arch_mode   - archiving mode,
  --              "move" - indicates delete from source and add to target
  --              "copy"  - indicates add to target
  --              "purge" - indicates delete from source
  --   p_src_ref     - indicates how to apply referential restriction on the source side
  --              none    - means source side is not affected
  --              restrict  - means source record must be taken out of set
  --                        if child record exists
  --   p_tgt_ref     - indicates how to apply referential restriction on the target side
  --              none    - means do nothing
  --              restrict  - N/A
  --              cascade - include all relevant records on target sid
  --   p_role   - Role of replation. To differ between many possible connections of two tables
  --   p_join_clause - how source and target must be joined, It assunes,
  --              that source and target databases will have S and T aliases,
  --              this clause will be ANDed with filtering condition when
  --              selecting records from the tagret
  --   p_tgt_typ - type of dependency, 'C' - from parent to Child, 'P' - child to Parent
  --
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_upd_tbl_depy(p_grp_nm varchar2,
                             p_src_tbl_nm varchar2,
                             p_tgt_tbl_nm varchar2,
                             p_arch_mode varchar2,
                             p_src_ref varchar2,
                             p_tgt_ref varchar2,
                             p_role varchar2,
                             p_join_clause varchar2,
                             p_tgt_typ   varchar2) ;
  -- PURPOSE
  --   update dependency settings
  -- INPUT PARAMETERS
  --    see Prc_add_tbl_depy
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_rm_tbl_depy(p_grp_nm varchar2, p_src_tbl_nm varchar2, p_tgt_tbl_nm varchar2, p_role varchar2) ;
  -- PURPOSE
  -- remove table dependency
  -- INPUT PARAMETERS
  --   p_grp_nm      - archive group name
  --   p_src_tbl_nm  - source table name
  --   p_tgt_tbl_nm  - target name
  --   p_role   - Role of replation. To differ between many possible connections of two tables
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_Add_parm(p_parm_nm varchar2) ;
  -- PURPOSE
  --    define new optional property
  -- INPUT PARAMETERS
  --  p_parm_nm - new parameter name,
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_set_grp_parm(p_grp_nm varchar2, p_parm_nm varchar2, p_parm_val varchar2) ;
  -- PURPOSE
  --    set archive group attributes
  -- INPUT PARAMETERS
  --  p_grp_nm  - archive group name
  --  p_parm_nm - parameter name, valid names:
  --
  --  p_parm_val - value to set, null means unset parameter
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_set_tbl_parm(p_grp_nm varchar2,
                             p_tbl_nm varchar2,
                             p_parm_nm varchar2,
                             p_parm_val varchar2) ;
  -- PURPOSE
  --   set table attributes, e.g., filter value
  -- INPUT PARAMETERS
  --  p_grp_nm  - archive group name
  --  p_tbl_nm  - table name
  --  p_parm_nm - parameter name, valid names:
  --
  --  p_parm_val - value to set, null means unset parameter
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_chk_grp(p_grp_nm varchar2);
  -- PURPOSE
  --   validate arch policy. it must not break existing referential integrity
  --   of the working result set
  -- INPUT PARAMETERS
  --  p_grp_nm  - archive group name
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  function Fnc_get_grp_cfg(p_grp_nm varchar2 default null) return clob;
  -- PURPOSE
  --   show group config (xml or table)
  -- INPUT PARAMETERS
  --   p_grp_nm  - archive group name
  -- OUTPUT PARAMETERS
  --   config formatted as XML
  -- EXCEPTIONS
  function Fnc_show_log(p_grp_nm varchar2, dtl_lvl number, dtm_from date default null) return clob;
  -- PURPOSE
  --   show archive logs, for audit purpose
  -- INPUT PARAMETERS
  --   dtl_lvl  - detail level,
  --              0 - show partition-level stat
  --              1 - show partition- and table-level stat
  --              +2 - show run diced instead of partition diced logs
  --   p_grp_nm  - archive group name
  --   dtm_from  - day to select from
  -- OUTPUT PARAMETERS
  --   list of logs, xml format
  -- EXCEPTIONS
  function Fnc_audit(p_grp_nm varchar2, p_tbl_nm varchar2, dtm_from date default null) return clob;
  -- PURPOSE
  --   show parameter modification logs, for audit purpose
  -- INPUT PARAMETERS
  --   p_grp_nm  - archive group name
  --   p_tbl_nm  - table name
  --   dtm_from  - day to select from
  -- OUTPUT PARAMETERS
  --   ref cursor of audit records
  -- EXCEPTIONS
  procedure Prc_drop_part(p_grp_nm varchar2, p_part_id varchar2) ;
  -- PURPOSE
  --   drop archive group partition. data will be permanently removed from
  --   the database
  -- INPUT PARAMETERS
  --   p_grp_nm  - archive group name
  --   p_tbl_nm  - table name
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_gen_depy(p_grp_nm varchar2,
                         p_src_tbl_nm varchar2,
                         p_dir number,
                         p_depth number default null);
  -- PURPOSE
  --   generate default dependencies (parent or child) for the given root table,
  --   based on existing constraints (from db dictionary)
  -- INPUT PARAMETERS
  --   p_grp_nm  - archive group name
  --   p_tbl_nm  - starting table name
  --   p_dir     - navigate direction,
  --             +1 - go up, to parent tables
  --             -1 - go down, tho child tables
  --   p_depth   - how far to go. Number of levels to cross
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_adm_add(p_adm_nm varchar2 default user, p_adm_pswd varchar2 default null);
  -- PURPOSE
  --   register person having access to admin functions
  -- INPUT PARAMETERS
  --   p_adm_nm   - admin username. It must be valid oracle username when password is empty
  --   p_adm_pwd  - password may be empty, Oracle performs authentication then
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_adm_pwd(p_adm_nm varchar2 default user, p_adm_pswd varchar2 default null);
  -- PURPOSE
  --   reset admin password
  -- INPUT PARAMETERS
  --   p_adm_nm   - admin username. It must be valid oracle username when password is empty
  --   p_adm_pwd  - password may be empty, Oracle performs authentication then
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_adm_rm(p_adm_nm varchar2 );
  -- PURPOSE
  --   remove admin
  -- INPUT PARAMETERS
  --   p_adm_nm   - admin username.
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_adm_auth(p_adm_nm varchar2 default user, p_adm_pswd varchar2 default null);
  -- PURPOSE
  --   athenticate person having access to admin functions
  -- INPUT PARAMETERS
  --   p_adm_nm   - admin username. It must be valid oracle username when password is empty
  --   p_adm_pwd  - password may be empty, Oracle performs authentication then, but admin password must be set null
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  procedure Prc_clean_audit_log(p_date_limit date, p_grp_nm varchar2 default null);
  -- PURPOSE
  --   clean audit log
  -- INPUT PARAMETERS
  --   p_date_limit   - date to clean to
  --   p_grp_nm - group name to clean, optional. all groups will be cleaned if none provided
  -- OUTPUT PARAMETERS
  -- EXCEPTIONS
  function get_parm(p_key varchar2) return varchar2;

    no_parent_record EXCEPTION;
    PRAGMA EXCEPTION_INIT(no_parent_record, -2291);
end PKG_ARCH;
/


begin
 begin
  execute immediate 'CREATE GLOBAL TEMPORARY TABLE WRK_ARCH_SESSION(tbl_nm varchar2(30), rid rowid) ON COMMIT PRESERVE ROWS';
 exception
  when others then
    null;
 end;
 begin
  execute immediate 'create index WRK_ARCH_SESSION_IX1 on WRK_ARCH_SESSION(tbl_nm, rid)';
 exception
  when others then
    null;
 end;
end;
/


create or replace package body PKG_ARCH as

  audit_rowid rowid;
  procedure set_parm(p_key varchar2, p_val varchar2) as
  begin
    v_parm(UPPER(p_key)) := p_val;
  end;

  function get_parm(p_key varchar2) return varchar2 as
  begin
   return v_parm(UPPER(p_key));
  exception
    when no_data_found then
      return null;

  end;

  procedure chk_tblspace(t_name varchar2) is
    l_COUNT number;
  begin
    select count(*) into l_count from user_tablespaces where tablespace_name like upper(t_name);
    if l_count = 0 then
       raise_application_error(-20001, 'Tablespace '||t_name||' is not available');
    end if;
  end;
  function expand_filter(p_flt varchar2) return varchar2 is
    i number;
    j number;
    l_buff varchar2(300);
  begin
     i := instr(p_flt , '%[');
     if i = 0 then
        return p_flt;
     else
        j := instr(p_flt , ']', i);
        if j = 0 then
           raise_application_error(-20001, 'Syntax error');
        end if;
        l_buff :=  get_parm(substr(p_flt, i+2, j - i - 2)) ;
        if l_buff is null then
           raise_application_error(-20001, 'Environment variable %['|| substr(p_flt, i+2, j - i - 2) ||'] is not defined');
        end if;

        return expand_filter(substr(p_flt, 1, i - 1) || l_buff || substr(p_flt, j+1));
     end if;
  end;

  /* PREPARE RECORD SET FROM CONNECTED TABLE */

  function prep_record_set(p_config arch_tbl_net%rowtype,
         p_level number, p_seq number, p_newseq number) return number is
         l_flt arch_tbl.filter%type;
         v_count number;
    begin
        select filter into l_flt from arch_tbl where tbl_nm = p_config.tgt_tbl_nm and grp_id = get_parm('GRP_ID');
        if l_flt is not null then
            l_flt := ' and '|| expand_filter(l_flt);
        end if;

        if p_config.src_tbl_nm is null then -- root
            execute immediate 'insert into wrk_arch_rec select '''||p_config.tgt_tbl_nm||
                ''', '||p_level||', '''||p_config.arch_mode||''',rowid, '||p_newseq||
                ' from '||p_config.tgt_tbl_nm||
                ' T where not exists (select null from wrk_arch_session w '||
                ' where w.rid=t.rowid and w.tbl_nm='''||p_config.tgt_tbl_nm ||''') and '||
                expand_filter(get_parm('RETAIN')) || l_flt ||
                ' and rownum <= '|| get_parm('BATCH');
        else
          if p_config.arch_mode = 'move' and p_config.src_ref = 'restrict' and p_config.tgt_ref = 'none' then
             -- remove parent records for existing child
             execute immediate 'delete from wrk_arch_rec where (action = ''move'' or action = ''purge'') and '||
               ' rid in (select distinct s.rowid '||
                    ' from '||p_config.tgt_tbl_nm||' T, '||p_config.src_tbl_nm || ' S, wrk_arch_rec r '||
                    ' where '|| p_config.join_clause || l_flt||
                    ' and r.seq = '||p_seq||' and r.tbl_nm = '''||p_config.src_tbl_nm||''' and r.rid = s.rowid'||
                    ')';

          else  -- cascade action to dependent table
            execute immediate 'insert into wrk_arch_rec select /*+ ORDERED USE_NL(S) USE_NL(T)*/'''||p_config.tgt_tbl_nm||
                ''', '||p_level||', '''||p_config.arch_mode||''', t.rowid, '||p_newseq||
                    ' from wrk_arch_rec r ,'||p_config.src_tbl_nm || ' S, '||p_config.tgt_tbl_nm||' T ' ||
                    '  where not exists (select null from wrk_arch_session w '||
                    ' where w.rid=t.rowid and w.tbl_nm='''||p_config.tgt_tbl_nm ||''') and '||
                    p_config.join_clause || l_flt||
                    ' and r.seq = '||p_seq||' and r.tbl_nm = '''||p_config.src_tbl_nm||''' and r.rid = s.rowid';
          end if;
        end if;
        v_count := SQL%ROWCOUNT;
        execute immediate 'analyze table wrk_arch_rec estimate statistics';
        return v_count;
    end;


    procedure set_audit(p_sql varchar2, p_tbl_nm varchar2, p_act varchar2, p_status varchar2 default 'F') is
      l_clob clob;
      qryCtx DBMS_XMLGEN.ctxHandle;
      pragma autonomous_transaction;
    begin
      if upper(p_act) <> 'I' and p_sql is not null then
         qryCtx := dbms_xmlgen.newContext(p_sql);
         l_clob := DBMS_XMLGEN.getXML(qryCtx);
      end if;
      insert into arch_parm_hist(MODIFIED_DTM, MOD_TBL_NM, parm_hist_id, ACT, OLD_VAL, MODIFIED_BY, STATUS)
         values (sysdate, p_tbl_nm, arch_parm_hist_seq.nextval, p_act,
              sys.XMLType.createXML(l_clob), get_parm('USERNAME'), p_status)
           returning rowid into audit_rowid;
      commit;
    end;

    procedure audit(p_sql varchar2, p_tbl_nm varchar2, p_act varchar2) is
       rid rowid;
    begin
       set_audit(p_sql, p_tbl_nm, p_act, 'S');
    end;

    procedure commit_audit is
      pragma autonomous_transaction;
    begin
      update arch_parm_hist set status = 'S' where rowid = audit_rowid;
      audit_rowid := null;
      commit;
    end;

    function add_stack(p_role varchar2) return varchar2 is
      v_level number;
      v_var varchar2(30);
    begin
      begin
        insert into arch_stack values (p_role);
        return p_role;
      exception
       when dup_val_on_index then
         for cr in (select * from arch_stack where ref_role like p_role||'%' ) loop
            v_var := cr.ref_role;
         end loop;

         select max(to_number(substr(ref_role, length(p_role)+2, 5)))  into v_level
            from arch_stack where ref_role like p_role||'/%';

         v_level := nvl(v_level,0)+1;

         if v_level > get_parm('MAX_HIERARCHY_LEVEL') then
           raise;
         end if;

         insert into arch_stack values (p_role||'/'||v_level);
         return p_role||'/'||v_level;
      end;
    end;


    function build_batch(p_grp_id number,
       p_tbl_nm varchar2 default null,
       p_level number,
       p_upperseq number) return number is
      l_level number;
      l_count number;
      l_cntx  number;
      l_cnt   number;
      l_seq   number;
      l_stack_ptr varchar2(30);

      cursor c(l_tbl varchar2) is
        select * from arch_tbl_net n
         where grp_id = p_grp_id and src_tbl_nm = l_tbl
          order by tgt_typ;
      cr c%rowtype;
    begin

       if p_tbl_nm is null then
          g_newseq := 0;
          cr.src_tbl_nm := null;
          l_level := 1;
          cr.tgt_tbl_nm := get_parm( 'ROOT TABLE');
          execute immediate 'truncate table wrk_arch_rec';
          l_cnt := prep_record_set(cr, 0, 0, 0);

          update wrk_arch_rec set action = get_parm('ROOT ARCH_MODE');
          open c(cr.tgt_tbl_nm);
       else
          open c(p_tbl_nm);
          l_level := p_level;
       end if;

       l_seq := g_newseq;
       l_count := 0;

       loop
         fetch c into cr;
         exit when c%notfound;
         begin
          -- same table cannot be accessed more than n-times in the same thread n=max_hierarchy_depth
          l_stack_ptr := add_stack(cr.ref_role);
         exception
           when dup_val_on_index then
              raise_application_error(-20001, 'Unresolved cyclic reference role name "'||cr.ref_role||'"');
         end;

         g_newseq := g_newseq +1;

         l_cnt := prep_record_set(cr, l_level, l_seq, g_newseq);
         if l_cnt > 0 then -- move on to connected records if some records found
            l_cntx := build_batch(p_grp_id, cr.tgt_tbl_nm, l_level+1, l_seq);
            l_count := l_count + l_cntx;
         end if;
         delete from arch_stack where ref_role = l_stack_ptr;
       end loop;

       close c;

       if p_tbl_nm is null then
          select count(*) into l_count from wrk_arch_rec where prio = 0;
       end if;

       return l_count;

    end;

  procedure exec_current_batch is
    l_buff t_arch_cmd;
    l_dest_tbl varchar2(30);
    type t_rowid is table of varchar2(20);

    type t_cursr is ref cursor;
    c    t_cursr;
    tmp_tbl  t_rowid;
    undo_tbl t_rowid;
    undo_sql varchar2(500);
    i     number;
    j     number;
    l_count0 number;
    l_count number;
    err_flag boolean;
    dml_errors EXCEPTION;
    PRAGMA exception_init(dml_errors, -24381);

  begin
    delete from wrk_arch_rec r where exists (select null from wrk_arch_rec w
       where w.TBL_NM = r.TBL_NM and w.RID = r.RID and w.PRIO > r.PRIO);

    delete from wrk_arch_rec r where exists (select null from wrk_arch_rec w
       where w.TBL_NM = r.TBL_NM and w.RID = r.RID and w.PRIO = r.PRIO and r.seq > w.seq);

    select o_arch_cmd(prio,  action, tbl_nm) bulk collect into l_buff
        from (select distinct prio, action, tbl_nm  from wrk_arch_rec);

    for cr in (select * from table(cast( l_buff  as t_arch_cmd)) order by prio desc) loop
        select arch_tbl_nm into l_dest_tbl
        from arch_tbl_part p, arch_tbl t, arch_grp g, arch_grp_part gp
        where t.grp_id = g.grp_id and g.grp_nm = get_parm('GRP_NM')
            and t.tbl_nm = cr.tbl_nm and gp.grp_id= g.grp_id and gp.status = 'current'
            and p.part_id = gp.part_id  and t.tbl_nm = p.tbl_nm;

        begin
           insert into arch_tbl_log (GRP_ID,RUN_ID ,TBL_NM ,PART_ID,STARTED_DTM, rec_cnt)
             values (get_parm('GRP_ID'), get_parm('RUN_ID'),
              cr.tbl_nm, get_parm('PART_ID'), sysdate, 0);
        exception
         when dup_val_on_index then
            null;
        end;


        if cr.action = 'copy'  then
            execute immediate 'insert into '||l_dest_tbl||
            ' select s.* from '||cr.tbl_nm||
            ' s, wrk_arch_rec t where s.rowid = t.rid and t.tbl_nm = '''||cr.tbl_nm||
            ''' and t.prio= '||cr.prio||' and t.action ='''||cr.action||'''';

            l_count := sql%rowcount;

            execute immediate 'insert into wrk_arch_session '||
            ' select ''' || cr.tbl_nm ||''', s.rowid from '||cr.tbl_nm||
            ' s, wrk_arch_rec t where s.rowid = t.rid and t.tbl_nm = '''||cr.tbl_nm||
            ''' and t.prio= '||cr.prio||' and t.action ='''||cr.action||'''';

            update arch_tbl_log set rec_cnt = rec_cnt + nvl(l_count, 0), ended_dtm = sysdate
                where grp_id = to_number(get_parm('GRP_ID')) and
                    run_id = to_number(get_parm('RUN_ID')) and
                    part_id = to_number(get_parm('PART_ID')) and
                    tbl_nm = cr.tbl_nm;
        end if;

        if cr.action = 'move' or cr.action = 'purge' then
            -- prepare rowid list in mem
            open c for ' select /*+ ORDERED USE_NL(S)*/ s.rowid from wrk_arch_rec t, '||cr.tbl_nm||
            ' s where s.rowid = t.rid and t.tbl_nm = '''||cr.tbl_nm||
            ''' and t.prio= '||cr.prio||' and t.action ='''||cr.action||'''';

            loop 
                fetch c bulk collect into tmp_tbl limit 10000;
                exit when tmp_tbl.count = 0;

                <<START_MOVE>>
                savepoint s1;

                if cr.action = 'move' then
                    forall i in 1..tmp_tbl.last
                        execute immediate 'insert into '||l_dest_tbl||' select s.* from '||cr.tbl_nm||' s  where s.rowid = :1 ' using tmp_tbl(i);
                    l_count0 := sql%rowcount;
                end if;

                err_flag := false;
                begin
                    forall i in 1..tmp_tbl.last save exceptions
                        execute immediate 'delete from '||cr.tbl_nm||' s  where s.rowid = :1'  using tmp_tbl(i);
                exception
                    when dml_errors then
                        l_count :=  tmp_tbl.last -  sql%bulk_exceptions.count;
                        err_flag := true;
                        rollback to savepoint s1;
                        l_count0 := 0;

                        if l_count > 0 then
                            for I in 1..sql%bulk_exceptions.count loop
                                j := sql%bulk_exceptions(I).error_index;
                                insert into wrk_arch_session values(cr.tbl_nm, tmp_tbl(j));
                                tmp_tbl.delete(j);
                            end loop;

                            undo_tbl := t_rowid();
                            undo_tbl.extend(l_count);

                            j := 1;
                            for i in 1..tmp_tbl.last loop
                                begin
                                    undo_tbl(j) := tmp_tbl(i);
                                    j := j + 1;
                                exception
                                    when no_data_found then null;
                                end;
                            end loop;

                            tmp_tbl := undo_tbl;
                            goto START_MOVE;
                        else
                            forall i in 1..tmp_tbl.last
                                insert into wrk_arch_session values(cr.tbl_nm, tmp_tbl(i));
                        end if;
                end;

                if not err_flag then
                    l_count := sql%rowcount;
                end if;

                if cr.action = 'move' and l_count <> l_count0 then
                    raise_application_error(-20009, 'Inconsistent data move - '||l_count0||' of '||l_count);
                end if;

                update arch_tbl_log set rec_cnt = rec_cnt + nvl(l_count, 0), ended_dtm = sysdate
                    where grp_id = to_number(get_parm('GRP_ID')) and
                    run_id = to_number(get_parm('RUN_ID')) and
                    part_id = to_number(get_parm('PART_ID')) and
                    tbl_nm = cr.tbl_nm;
            end loop; 

            close c;
        end if;

    end loop;

  end;

  procedure exe_ddl(p_sql in varchar2 ) is
      pragma autonomous_transaction;
  begin
     execute immediate p_sql;
     commit;
  end;

  procedure chk_grp_stat(p_grp_nm varchar2, p_stat varchar2) is
    l_COUNT number;
  begin
    select count(*) into l_count from arch_grp where grp_nm = p_grp_nm and status = p_stat;
    if l_count = 0 then
       raise_application_error(-20001, 'Archive group '|| p_grp_nm ||' status is not '||p_stat);
    end if;
  end;

  procedure validate_parm_complete is
  begin
     for cr in (select parm_nm from ref_arch_parm where is_required = 'Y')loop
       if get_parm(cr.parm_nm) is null then
          raise_application_error(-20010, 'Parameter "'||cr.parm_nm||'" is not set');
       end if;
     end loop;

     if get_parm('ROOT TABLE') is null then
        raise_application_error(-20010, 'Parameter "ROOT TABLE" is not set');
     end if;

     if get_parm('TGT_TABLESPACE') is null then
        raise_application_error(-20010, 'Parameter "TGT_TABLESPACE" is not set');
     end if;
     if get_parm('ROOT ARCH_MODE') is null then
        raise_application_error(-20010, 'Parameter "ROOT ARCH_MODE" is not set');
     end if;
     if get_parm('NEXT_RUN') is null then
        raise_application_error(-20010, 'Parameter "NEXT_RUN" is not set');
     end if;

  end;

  --  extract original object definition fromm the database and transform into archive
  --  object definition, i.e., rename, change tablespace, remove FK

  function get_ddl(table_nm varchar2, p_suffix varchar2, p_new_tbs varchar2) return varchar2 is
   text0 varchar2(30000);
   pos1 number;
   pos2 number;
   cursor c is select constraint_name from user_constraints where table_name = table_nm and CONSTRAINT_TYPE <> 'R';

    function replace_name(p_string varchar2, p_tbs varchar2, p_pattern varchar2, p_pattern1 varchar2 default null) return varchar2 is
      buff  varchar2(30000);
      buff1 varchar2(30000);
      n number;
      pos number;
    begin
     buff := p_string ;
     N := instr(buff, p_pattern);
     pos := n  + length(p_pattern);

     if p_pattern1 is not null then
       N := instr(buff, p_pattern1);
       pos := n  + length(p_pattern1);
     end if;

     if n = 0 then
       return p_string;
     end if;

     buff1 := substr(buff, 1, pos - 1);
     buff := substr(buff, pos);
     n := instr(buff, '"');
     buff1 := buff1 ||p_tbs ;
     buff  := substr(buff, n);
     if instr(buff, p_pattern) = 0 then
       return buff1||buff;
     else
       return buff1||replace_name(buff, p_tbs, p_pattern, p_pattern1);
     end if;
    end;

    function remove_FK(p_string varchar2) return varchar2 is
     i number;
     j number;
     k number;
     k1 number;
    begin
     i := instr(p_string, 'FOREIGN KEY');
     if i = 0 then
       return p_string;
     end if;
     J := instr(p_string, ',', i - length(p_string)); -- find first comma before FK
     i := instr(p_string, ')', i, 2); -- find second next ) after FK, assume it closes list of referenced fields
     k := instr(p_string, ',', i+1); -- find next , assume next fk start
     k1 := instr(p_string, ')', i+1); -- find next ) assume end of table structure definition
     if k = 0 then
       K := k1;
     end if;
     k := least(k,k1);
     return remove_FK(substr(p_string, 1, j - 1) || substr(p_string, k));

    end;

  begin
    select tmplt into  text0  from arch_tbl
      where grp_id = get_parm('GRP_ID') and tbl_nm = table_nm;
    if text0 is null then
       text0 := DBMS_METADATA.get_ddl('TABLE', table_nm);
       while instr(text0,'STORAGE(',1) > 0 loop
	     pos1:=instr(text0,'STORAGE(',1)-1;
		 pos2:=instr(text0,')',pos1)+1;
         text0:=substr(text0,1,pos1)||' '||substr(text0,pos2)||' ';
      end loop;
      Prc_set_tbl_parm(get_parm('GRP_NM'), table_nm, 'TMPLT', text0);
    end if;

    text0 := replace_name(text0, p_suffix, 'TABLE "', '"."'||table_nm);
    text0 := replace_name(text0, p_new_tbs, 'TABLESPACE "');
    text0 := remove_fk(text0);
    for cr in c loop
     text0 := replace_name(text0, p_suffix, 'CONSTRAINT "', cr.constraint_name);
    end loop;
    return text0;

  end;

  procedure init_partition is
    i number;
    j number;
    l_statement varchar2(30000);
  begin

    select grp_id, arch_grp_part_seq.nextval into i,j
         from arch_grp where grp_nm like get_parm('GRP_NM');

    insert into arch_grp_part(GRP_ID, PART_ID, CREATED_DTM, LAST_RUN, STATUS)
        values(i,j, sysdate, null, 'current');

    update arch_grp_part set status = 'closed'
      where grp_id = i and status <> 'closed' and part_id <> j;

    for cr in (select tbl_nm from arch_tbl where grp_id = i and status = 'active') loop
        if get_parm('ROOT ARCH_MODE') <> 'purge' then
           exe_ddl(get_ddl(cr.tbl_nm, j, get_parm( 'TGT_TABLESPACE')));
        end if;
        insert into arch_tbl_part (grp_id, tbl_nm, part_id, arch_tbl_nm)
           values ( i, cr.tbl_nm, j, cr.tbl_nm||j);
    end loop;

  end;

  -- check if the group is active
  -- identify destination partition
  -- analyze root table destination, switch partition if necessary.

  procedure validate_to_run is
    l_part_id number;
    l_root_tbl varchar2(30);
    n_rec number;
  begin

     if get_parm('status') <> 'active'  then
        raise_application_error(-20001, 'Group is not active');
     end if;

     select p.part_id, t.arch_tbl_nm into l_part_id, l_root_tbl
       from arch_grp g, arch_grp_part p, arch_tbl_part t
       where g.grp_id = p.grp_id and t.tbl_nm = g. root_tbl_nm and
             p.status = 'current' and p.part_id=t.part_id and
             grp_nm = get_parm('GRP_NM');

     -- check if target archive is full;
     if get_parm('ROOT ARCH_MODE') = 'purge' then
       n_rec  := 0;
     else
      if substr(get_parm('ARCH_SIZE'), 1,4) = 'REC=' then
        execute immediate 'select count(*) from '||l_root_tbl into n_rec ;
        if n_rec > 0 then  -- never create new archive if current is empty, otherwise:
           n_rec := n_rec - to_number(substr(get_parm('ARCH_SIZE'), 5)) + 1;
        end if;
      else
        execute immediate 'select count(*) from '|| l_root_tbl
          ||' '||get_parm('ARCH_SIZE')  into n_rec ;
      end if;
     end if;

     if n_rec > 0 then
        init_partition;
        select p.part_id, t.arch_tbl_nm  into l_part_id, l_root_tbl
            from arch_grp g, arch_grp_part p, arch_tbl_part t
            where g.grp_id = p.grp_id and t.tbl_nm = g. root_tbl_nm and
                  p.status = 'current' and p.part_id=t.part_id and
                  grp_nm = get_parm('GRP_NM');
     end if;

     set_parm('PART_ID', l_part_id);

  end;


  procedure load_params(p_grp_nm varchar2)  is
    cursor C is select * from arch_grp where grp_nm like p_grp_nm ;
    cursor d is
    select parm_nm, val
       from arch_grp_parm p, arch_grp g
          where grp_nm like p_grp_nm and g.grp_id = p.grp_id ;
    cr c%rowtype;
    dr d%rowtype;
    l_grp_id number;
  begin

    open c;
    fetch c into cr;
    if c%notfound then
       close c;
       raise_application_error(-20005, 'Archive group '||p_grp_nm||' not found');
    end if;
    close c;

    open d;
    loop
      fetch d into dr;
      exit when  d%notfound;
      set_parm(dr.parm_nm, dr.val);
    end loop;
    close d;

    set_parm('STATUS', CR.STATUS);
    set_parm('ROOT TABLE', cr.root_tbl_nm);
    set_parm('TGT_TABLESPACE', cr.TGT_TABLESPACE);
    set_parm('LAST_RUN',  TO_CHAR(CR.LAST_RUN,  'DD.MM.YYYY HH24:MI:SS'));
    set_parm('NEXT_RUN',  TO_CHAR(CR.NEXT_RUN,  'DD.MM.YYYY HH24:MI:SS'));
    set_parm('GRP_NM',  p_grp_nm);
    select grp_id into l_grp_id from arch_grp where grp_nm = p_grp_nm;
    set_parm('GRP_ID',  l_grp_id);
    set_parm('ROOT ARCH_MODE',  cr.root_arch_mode);

  end;

  procedure prc_chk_branch(p_tbl_nm varchar2, p_parent_move varchar2 default null) as
    l_schema_owner varchar2(30);
    l_parent_move  varchar2(10);
    procedure seek_missing_children is
    begin
      for cr in (SELECT other.table_name
                  FROM ALL_CONSTRAINTS this, ALL_CONSTRAINTS other
                       WHERE this.constraint_name = other.r_constraint_name AND this.owner = l_schema_owner AND
                             this.table_name =  p_tbl_nm AND other.constraint_type = 'R' AND other.r_owner = this.owner
               minus
               SELECT tgt_tbl_nm FROM arch_tbl_net WHERE src_tbl_nm = p_tbl_nm and grp_id = get_parm('GRP_ID')) loop
        raise_application_error(-20008, 'Relation missing: "'||p_tbl_nm||'"->"'||cr.table_name||'" (child)');
      end loop;
    end;

  begin
    -- 1. Make sure that all children are present in the net

    select val into l_schema_owner from arch_grp_parm
        where grp_id = get_parm('GRP_ID') and PARM_NM = 'OWNER';
    l_parent_move := p_parent_move;
    if p_tbl_nm = get_parm('ROOT TABLE') then
       l_parent_move := get_parm('ROOT ARCH_MODE');
       if get_parm('ROOT ARCH_MODE') in ('move', 'purge') then
          seek_missing_children;
       end if;
    end if;

    -- 2. Validate all connections allowed
    for cr in (select * from arch_tbl_net WHERE src_tbl_nm = p_tbl_nm and grp_id = get_parm('GRP_ID')) loop
        if (l_parent_move = 'move' and cr.tgt_typ='C' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'move') or
           (l_parent_move = 'move' and cr.tgt_typ='C' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'purge') or
           (l_parent_move = 'move' and cr.tgt_typ='C' and cr.src_ref = 'restrict' and cr.tgt_ref='none' and cr.arch_mode = 'move') or
           (l_parent_move = 'purge' and cr.tgt_typ='C' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'purge')or
           (l_parent_move = 'purge' and cr.tgt_typ='C' and cr.src_ref = 'restrict' and cr.tgt_ref='none' and cr.arch_mode = 'purge')or
           (l_parent_move = 'copy' and cr.tgt_typ='C' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'copy') or
           (l_parent_move = 'copy' and cr.tgt_typ='C' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'move') or
           (l_parent_move = 'move' and cr.tgt_typ='P' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'copy') or
           (l_parent_move = 'copy' and cr.tgt_typ='P' and cr.src_ref = 'none' and cr.tgt_ref='cascade' and cr.arch_mode = 'copy') then
           null;
         else
          null;
            raise_application_error(-20008, 'Invalid connection properties for: '||chr(10)||
              'Groupname: '||get_parm('GRP_NM' )||chr(10)||
              'Source table name :'||cr.src_tbl_nm||chr(10)||
              'Target table name :'||cr.tgt_tbl_nm||chr(10)||
              'Role name :'||cr.ref_role);
         end if;
         -- do same for children
         begin
           insert into arch_stack values(cr.ref_role);
           prc_chk_branch(cr.tgt_tbl_nm, cr.arch_mode);

         exception
           when dup_val_on_index then

             dbms_output.put_line('Warn1ng! Cyclic reference: "'||cr.ref_role||'"');
         end;

         delete from arch_stack where ref_role = cr.ref_role;
    end loop;
  end;


  procedure Prc_int_run_arch(p_grp_nm varchar2)  is
    l_size    number;
    l_run_id   number;
    l_grp_id number;
    i number := 0;

  begin
    select grp_id into l_grp_id from arch_grp where grp_nm = p_grp_nm;

    load_params(p_grp_nm );

    prc_chk_branch(get_parm('ROOT TABLE')); -- validate group doesn't braek DB ref integrity

    if get_parm('PERIOD') is not null then
       exe_ddl('update arch_grp set last_run = sysdate, next_run = '||get_parm('PERIOD')||
         ' where grp_nm = '''||p_grp_nm||'''');
    else
       update arch_grp set last_run = sysdate, next_run = null where grp_nm = p_grp_nm;
    end if;


    insert into arch_run(GRP_ID,  RUN_ID,STARTED_DTM, STATUS, SES_ID, TOTAL_CNT, done_cnt)
      values( get_parm('GRP_ID'),  arch_run_seq.nextval, sysdate, 'r', sys_context('USERENV', 'SESSIONID'), 0, 0)
         returning run_id into l_run_id;
    commit;
    set_parm('RUN_ID', l_run_id);

    begin
      loop

         validate_to_run;

         update arch_grp_part set
           first_run = nvl(first_run, sysdate),
           last_run = nvl(last_run, sysdate)
           where part_id = get_parm('PART_ID') and
                grp_id = to_number(get_parm('GRP_ID'));

         l_size := build_batch(l_grp_id, null, 0, 0);
         exit when nvl(l_size, 0) = 0;

         exec_current_batch;
         update arch_run set done_cnt = done_cnt + nvl(l_size,0) where run_id = l_run_id and
           grp_id = to_number(get_parm('GRP_ID'));
         commit;
         execute immediate 'truncate table wrk_arch_rec';
      end loop;
      update arch_run set status = 's', ended_dtm = sysdate
         where run_id = l_run_id and grp_id = l_grp_id;
    exception
      when others then
        rollback;
        update arch_run set status = 'f', ended_dtm = sysdate
           where run_id = l_run_id and grp_id = to_number(get_parm('GRP_ID'));
        commit;
        raise;
    end;

  end;

  procedure Prc_run_arch(p_grp_nm varchar2 default null)  is
    cursor c is SELECT GRP_NM FROM ARCH_GRP WHERE STATUS = 'active' and next_run <= sysdate;
    cr c%rowtype;
    n number := 0;
  begin

    IF P_GRP_NM IS NOT NULL THEN
       Prc_int_run_arch(p_grp_nm );
       RETURN;
    END IF;

    loop
      open c;
      n := 0;
      loop
        fetch c into cr;
        exit when c%notfound;
        begin
            Prc_int_run_arch(cr.grp_nm );
            n := n + 1;
        exception
            when others then
            null;
        end;
      end loop;
      close c;
      exit when n = 0;
    end loop;

  end;
  procedure chk_auth is
  begin
    if get_parm('USERNAME') is null then
       raise_application_error(-20010, 'You are not authorized');
    end if;
  end;

  procedure Prc_add_group(p_grp_nm varchar2) is
  begin
    chk_auth;

    insert into arch_grp(GRP_ID, GRP_NM, TGT_TABLESPACE, STATUS,
           LAST_RUN, NEXT_RUN, ROOT_TBL_NM)
           values (arch_grp_seq.nextval, p_grp_nm, null, 'stop',
           null, null, null);
    audit(' select * from arch_grp where GRP_ID = arch_grp_seq.currval', 'arch_grp', 'I');

  end;

  procedure Prc_add_tbl(p_grp_nm varchar2, p_tbl_nm varchar2) is
    n number;
  begin
    chk_auth;

    insert into arch_tbl(GRP_ID, TBL_NM, FILTER, STATUS)
      select grp_id, upper(p_tbl_nm), null, 'active' from arch_grp
         where grp_nm = p_grp_nm ;
    n := SQL%ROWCOUNT;
    audit('select * from arch_tbl where tbl_nm = upper('''||p_tbl_nm||
      ''' and grp_id = (select grp_id from arch_grp where grp_nm='''||p_grp_nm||''')',
       'arch_tbl', 'I');
    if n = 0 then
         raise NO_DATA_FOUND;
    end if;

  end;
  procedure Prc_add_tbl_depy(p_grp_nm varchar2,
                             p_src_tbl_nm varchar2,
                             p_tgt_tbl_nm varchar2,
                             p_arch_mode varchar2,
                             p_src_ref varchar2,
                             p_tgt_ref varchar2,
                             p_role varchar2,
                             p_join_clause varchar2,
                             p_tgt_typ   varchar2) is
    l_grp_id number;
    l_rid  rowid;
    l_n number;
  begin
    chk_auth;

    if p_arch_mode not in ('move', 'copy', 'purge') then
       raise_application_error(-20001, 'p_arch_mode must be one of (''move'', ''copy'', ''purge'')');
    end if;
    if p_src_ref not in ('none', 'restrict') then
       raise_application_error(-20001, 'p_src_ref must be one of (''none'', ''restrict'')');
    end if;
    if p_tgt_ref not in ('none', 'cascade') then
       raise_application_error(-20001, 'p_tgt_ref must be one of (''none'', ''cascade'')');
    end if;
    begin
       select grp_id into l_grp_id from arch_grp  where grp_nm = p_grp_nm ;

       insert into arch_tbl_net(GRP_ID, SRC_TBL_NM, TGT_TBL_NM, REF_ROLE, ARCH_MODE,
         SRC_REF, TGT_REF, JOIN_CLAUSE, tgt_typ) values (
            l_grp_id, upper(p_src_tbl_nm), upper(p_tgt_tbl_nm), upper(p_role), p_arch_mode, p_src_ref,
                p_tgt_ref, p_join_clause, p_tgt_typ)
                   returning rowid into l_rid;
       l_n := SQL%ROWCOUNT;
       audit('select * from arch_tbl_net where rowid = '''||l_rid||'''', 'arch_tbl_net', 'I');
    exception
      when dup_val_on_index then
         return; -- dependency already here
    end;

    if l_n = 0 then
         raise NO_DATA_FOUND;
    end if;

  end;
  procedure Prc_upd_tbl_depy(p_grp_nm varchar2,
                             p_src_tbl_nm varchar2,
                             p_tgt_tbl_nm varchar2,
                             p_arch_mode varchar2,
                             p_src_ref varchar2,
                             p_tgt_ref varchar2,
                             p_role varchar2,
                             p_join_clause varchar2,
                             p_tgt_typ   varchar2) is
  begin
    chk_auth;
    if p_arch_mode not in ('move', 'copy', 'purge') then
       raise_application_error(-20001, 'p_arch_mode must be one of (''move'', ''copy'', ''purge'')');
    end if;
    if p_src_ref not in ('none', 'restrict') then
       raise_application_error(-20001, 'p_src_ref must be one of (''none'', ''restrict'')');
    end if;
    if p_tgt_ref not in ('none', 'cascade') then
       raise_application_error(-20001, 'p_tgt_ref must be one of (''none'', ''cascade'')');
    end if;
    set_audit('select * from arch_tbl_net where src_tbl_nm = upper('''||p_src_tbl_nm||
     ''') and tgt_tbl_nm = upper('''||p_tgt_tbl_nm||''') and ref_role = upper('''||p_role||
     ''') and grp_id = (select grp_id from arch_grp where grp_nm = '''||p_grp_nm||''')', 'arch_tbl_net', 'U');

    update arch_tbl_net set arch_mode = nvl(p_arch_mode, arch_mode),
       src_ref = nvl(p_src_ref, src_ref),
       tgt_ref = nvl(p_tgt_ref, tgt_ref),
       join_clause = nvl(p_join_clause, join_clause),
       tgt_typ   = nvl(p_tgt_typ, tgt_typ)
       where src_tbl_nm = upper(p_src_tbl_nm) and tgt_tbl_nm = upper(p_tgt_tbl_nm) and ref_role = upper(p_role) and
         grp_id = (select grp_id from arch_grp where grp_nm = p_grp_nm);
    commit_audit;
  end;

  procedure Prc_rm_tbl_depy(p_grp_nm varchar2, p_src_tbl_nm varchar2, p_tgt_tbl_nm varchar2, p_role varchar2) is
  begin
    chk_auth;
    set_audit('select * from arch_tbl_net where src_tbl_nm = upper('''||p_src_tbl_nm||
     ''') and tgt_tbl_nm = upper('''||p_tgt_tbl_nm||''') and ref_role = upper('''||p_role||
     ''') and grp_id = (select grp_id from arch_grp where grp_nm = '''||p_grp_nm||''')', 'arch_tbl_net', 'U');
    delete from arch_tbl_net
       where src_tbl_nm = upper(p_src_tbl_nm) and tgt_tbl_nm = upper(p_tgt_tbl_nm) and  ref_role = upper(p_role) and
         grp_id = (select grp_id from arch_grp where grp_nm = p_grp_nm);
    commit_audit;

  end;

  procedure Prc_set_grp_parm(p_grp_nm varchar2, p_parm_nm varchar2, p_parm_val varchar2) is
    l_grp_id number;
    l_count  number;
  begin
    chk_auth;

    select grp_id into l_grp_id from arch_grp where grp_nm = p_grp_nm;

    if p_parm_nm <> 'STATUS' then -- group must be stoped to change parameters
       chk_grp_stat(p_grp_nm, 'stop');
    end if;

    if upper(p_parm_nm) = 'ROOT TABLE' then
       select count(*) into l_count from arch_tbl_net where grp_id = l_grp_id;
       if l_count > 0 then
          raise_application_error(-20002, 'Table dependencies must be removed before resetting the ROOT TABLE');
       end if;
       begin
          audit('select * from arch_grp where grp_id = '||l_grp_id, 'arch_grp', 'U');
          update arch_grp set root_tbl_nm = upper(p_parm_val) where grp_id = l_grp_id;
          commit_audit;
       exception
         when no_parent_record then
           raise_application_error(-20003, 'Table '||p_parm_val||' is not a group member');
       end;
       return;

    elsif upper(p_parm_nm) = 'STATUS' then
       if p_parm_val not in ('active', 'stop') then
          raise_application_error(-20001, 'STATUS must be one of (''active'', ''stop'')');
       end if;
       if p_parm_val = 'active' then
          load_params(p_grp_nm);  -- refresh parameters from the DB
          validate_parm_complete; -- put validate here
          select count(*) into l_count from arch_grp_part where grp_id = l_grp_id;

          if l_count = 0 then  -- first time go active, create first partition then
             init_partition;
          end if;

       end if;
       audit('select * from arch_grp where grp_id = '||l_grp_id, 'arch_grp', 'U');
       update arch_grp set status = p_parm_val where grp_id = l_grp_id;
       commit_audit;
       return;
    elsif upper(p_parm_nm) = 'TGT_TABLESPACE' then
       chk_tblspace(p_parm_val);
       audit('select * from arch_grp where grp_id = '||l_grp_id, 'arch_grp', 'U');
       update arch_grp set tgt_tablespace = upper(p_parm_val) where grp_id = l_grp_id;
       commit_audit;
       return;
    elsif upper(p_parm_nm) = 'NEXT_RUN' then

       audit('select * from arch_grp where grp_id = '||l_grp_id, 'arch_grp', 'U');
       begin
        if length(p_parm_val) < 15 then
           raise_application_error(-20001, '');
        end if;
        update arch_grp set next_run = to_date(p_parm_val, 'DD.MM.YYYY HH24:MI:SS') where grp_id = l_grp_id;
       exception
        when others then
          raise_application_error(-20001, 'Datetime format expected DD.MM.YYYY HH24:MI:SS, given '||p_parm_val);
       end;
       commit_audit;
       return;
    elsif upper(p_parm_nm) = 'ROOT ARCH_MODE' then
       if (p_parm_val in ('move', 'copy', 'purge')) then
         null;
       else
         raise_application_error(-20001, 'Archive mode must be one of "move, copy, purge');
       end if;
       audit('select * from arch_grp where grp_id = '||l_grp_id, 'arch_grp', 'U');
       update arch_grp set root_arch_mode = p_parm_val;
       commit_audit;
       return;
    end if;

    if p_parm_val is null then
       set_audit('select * from arch_grp_parm where grp_id = '||l_grp_id||' and parm_nm = upper('''||p_parm_nm||''')', 'arch_grp_parm', 'D');
       delete from arch_grp_parm where grp_id = l_grp_id and parm_nm = upper(p_parm_nm);
       commit_audit;
    else
       begin
         insert into arch_grp_parm (grp_id, parm_nm, val) values (l_grp_id, upper(p_parm_nm), p_parm_val);
         audit('select * from arch_grp_parm where grp_id = '||l_grp_id||' and parm_nm = upper('''||p_parm_nm||''')', 'arch_grp_parm', 'I');

       exception
         when dup_val_on_index then
             set_audit('select * from arch_grp_parm where grp_id = '||l_grp_id||' and parm_nm = upper('''||p_parm_nm||''')', 'arch_grp_parm', 'U');
             update arch_grp_parm set val = p_parm_val
               where grp_id = l_grp_id and parm_nm = upper(p_parm_nm);
             commit_audit;
       end;
    end if;
  end;

  procedure Prc_set_tbl_parm(p_grp_nm varchar2,
                             p_tbl_nm varchar2,
                             p_parm_nm varchar2,
                             p_parm_val varchar2)  is
  begin
    chk_auth;
     if upper(p_parm_nm) = 'FILTER' then
        set_audit('select * from arch_tbl where tbl_nm = upper('''||p_tbl_nm||
           ''') and grp_id = (select grp_id from arch_grp where grp_nm = '''||p_grp_nm||''')',
           'arch_tbl', 'U');
        update arch_tbl set filter = p_parm_val
          where tbl_nm = upper(p_tbl_nm) and grp_id = (select grp_id from arch_grp where grp_nm = p_grp_nm);
        commit_audit;

     elsif upper(p_parm_nm) = 'STATUS' then
        if p_parm_val not in ('active', 'stop') then
           raise_application_error(-20001, 'STATUS must be one of (''active'', ''stop'')');
        end if ;
        set_audit('select * from arch_tbl where tbl_nm = upper('''||p_tbl_nm||
           ''') and grp_id = (select grp_id from arch_grp where grp_nm = '''||p_grp_nm||''')',
           'arch_tbl', 'U');
        update arch_tbl set status = p_parm_val
          where tbl_nm = upper(p_tbl_nm) and grp_id = (select grp_id from arch_grp where grp_nm = p_grp_nm);
        commit_audit;
     elsif upper(p_parm_nm) = 'TMPLT' then
        set_audit('select * from arch_tbl where tbl_nm = upper('''||p_tbl_nm||
           ''') and grp_id = (select grp_id from arch_grp where grp_nm = '''||p_grp_nm||''')',
           'arch_tbl', 'U');
        update arch_tbl set tmplt = p_parm_val
          where tbl_nm = upper(p_tbl_nm) and grp_id = (select grp_id from arch_grp where grp_nm = p_grp_nm);
        commit_audit;

     end if;
  end;

  function Fnc_get_grp_cfg(p_grp_nm varchar2 default null) return clob is
    l_clob clob;
    qryCtx DBMS_XMLGEN.ctxHandle;
    l_condition   varchar2(100);
  begin
    chk_auth;

    if p_grp_nm is not null then
       l_condition := ' where grp_nm like '''||p_grp_nm||'''';
    end if;

    qryCtx := dbms_xmlgen.newContext(
       'select GRP_ID, GRP_NM, TGT_TABLESPACE, STATUS, LAST_RUN, NEXT_RUN, ROOT_TBL_NM, ROOT_ARCH_MODE'||
            ' ,cast(multiset(select parm_nm, val'||
                    ' from arch_grp_parm p'||
                    ' where P.grp_id = G.grp_id'||
                           ' ) as t_arch_grp_parm'||
                  ' ) arch_grp_parm'||
            ' ,cast(multiset('||
               ' select tbl_nm, filter, status'||
                 ' ,cast(multiset(select SRC_TBL_NM, TGT_TBL_NM, REF_ROLE, ARCH_MODE, SRC_REF, TGT_REF, JOIN_CLAUSE, TGT_TYP'||
                         ' from arch_tbl_net n'||
                         ' where N.grp_id = T.grp_id and N.src_tbl_nm = T.tbl_nm'||
                                ' ) as t_arch_tbl_net'||
                      '  ) net'||
                 ' from arch_tbl T where T.grp_id = G.grp_id'||
                           ' ) as t_arch_tbl'||
                    ' ) arch_tbl'||
            ' from arch_grp G '||l_condition);

    dbms_xmlgen.setRowSetTag(qryCtx, 'GROUPS');
    dbms_xmlgen.setRowTag(qryCtx, 'ARCH_GROUP');
    l_clob := DBMS_XMLGEN.getXML(qryCtx);
    return l_clob;
  end;

  function Fnc_show_log(p_grp_nm varchar2, dtl_lvl number, dtm_from date default null) return clob is
    l_clob clob;
    qryCtx DBMS_XMLGEN.ctxHandle;
    l_sql_detail varchar2(300);
    l_sql_date   varchar2(100);
  begin
    chk_auth;
    if dtm_from is not null then
       If dtl_lvl < 2 then
            l_sql_date := ' and last_run >= to_date('''|| to_char(dtm_from, 'dd.mm.yyyy hh24:mi:ss') ||''', ''dd.mm.yyyy hh24:mi:ss'') ';
       else
            l_sql_date := ' and started_dtm >= to_date('''|| to_char(dtm_from, 'dd.mm.yyyy hh24:mi:ss') ||''', ''dd.mm.yyyy hh24:mi:ss'') ';
       end if;
    end if;

    if mod(dtl_lvl, 2) = 0  then
      l_sql_detail := ' null ';
    else
      If dtl_lvl < 2 then
         l_sql_detail := 'cast(multiset(select RUN_ID, STARTED_DTM, ENDED_DTM, REC_CNT '||
                             ' from arch_tbl_log L '||
                             'where L.grp_id = T.grp_id and L.tbl_nm = T.tbl_nm and L.part_id = T.part_id'||
                                    ') as t_arch_tbl_log)';
      else
         l_sql_detail := 'cast(multiset(select RUN_ID, STARTED_DTM, ENDED_DTM, REC_CNT, L.tbl_nm, arch_tbl_nm '||
                          ' from arch_tbl_log L, arch_tbl_part T'||
                          ' where L.grp_id = T.grp_id and L.tbl_nm = T.tbl_nm and L.part_id = T.part_id'||
                               ' and r.grp_id = L.grp_id and r.run_id = L.run_id '||
                                 ') as t_arch_tbl_log_ext) ';
      end if;
    end if;

    If dtl_lvl < 2 then

       qryCtx := dbms_xmlgen.newContext(
      'select GRP_NM, TGT_TABLESPACE, STATUS, LAST_RUN, NEXT_RUN, ROOT_TBL_NM, ROOT_ARCH_MODE'||
       ',cast'||
         '(multiset'||
           '(select PART_ID, CREATED_DTM, LAST_RUN,  STATUS, FIRST_RUN'||
            ',cast (multiset(select tbl_nm, arch_tbl_nm'||
                        ', '|| l_sql_detail ||' log '||
                     ' from arch_tbl_part T where P.grp_id = T.grp_id and P.part_id = T.part_id) as t_arch_tbl_part '||
                ') TBL_part from arch_grp_part P where P.grp_id = G.grp_id'|| l_sql_date ||
            ') as t_arch_part'||
          ') partition from arch_grp g');
    else
        qryCtx := dbms_xmlgen.newContext(
        'select GRP_NM, TGT_TABLESPACE, STATUS, LAST_RUN, NEXT_RUN, ROOT_TBL_NM, ROOT_ARCH_MODE '||
         ',cast (multiset( '||
                     'select RUN_ID, STARTED_DTM , ENDED_DTM, STATUS, SES_ID, TOTAL_CNT, DONE_CNT, err_msg '||
                     ','||l_sql_detail ||' log  from arch_run R where R.grp_id = G.grp_id '|| l_sql_date ||
                        ' ) as t_arch_run )  run  from arch_grp g');
    end if;

    dbms_xmlgen.setRowSetTag(qryCtx, 'GROUPS');
    dbms_xmlgen.setRowTag(qryCtx, 'ARCH_GROUP');
    l_clob := DBMS_XMLGEN.getXML(qryCtx);
    return l_clob;
  end;

  function Fnc_audit(p_grp_nm varchar2, p_tbl_nm varchar2, dtm_from date default null) return clob is
    qryCtx DBMS_XMLGEN.ctxHandle;
    l_date varchar2(100);
    l_grp_id number;
  begin
     chk_auth;
     select grp_id into l_grp_id from arch_grp where grp_nm like p_grp_nm;
     if dtm_from is not null then
        l_date := ' and modified_dtm >= to_date('''|| to_char(dtm_from, 'dd.mm.yyyy hh24:mi:ss')||''', ''dd.mm.yyyy hh24:mi:ss'')';
     end if;

     qryCtx := dbms_xmlgen.newContext('select * from arch_parm_hist where upper(mod_tbl_nm)='''||
        upper(p_tbl_nm)||''' and instr(old_val, ''<GRP_ID>'|| l_grp_id ||'</GRP_ID>'') > 0 '||l_date);

     dbms_xmlgen.setRowSetTag(qryCtx, 'AUDIT_LOG');
     dbms_xmlgen.setRowTag(qryCtx, 'AUDIT_REC');

     return DBMS_XMLGEN.getXML(qryCtx);
  end;

  procedure Prc_drop_part(p_grp_nm varchar2, p_part_id varchar2) is
  begin
    chk_auth;
    set_audit('select * from arch_grp_part where part_id = '||p_part_id, 'arch_grp_part', 'd');
    for cr in (select arch_tbl_nm, p.status
       from arch_grp a, arch_grp_part p, arch_tbl_part t
       where a.grp_nm = p_grp_nm and a.grp_id = p.grp_id and p.part_id = p_part_id
          and t.grp_id = p.grp_id and t.part_id = p.part_id) loop

       if cr.status like 'current' then
          raise_application_error(-20001, 'Current partition #'||p_part_id||' cannot be dropped');
       end if;
       exe_ddl('drop table '||cr.arch_tbl_nm);
    end loop;
    delete from arch_tbl_log  where grp_id in (select grp_id from arch_grp where grp_nm = p_grp_nm) and part_id = p_part_id;
    delete from arch_tbl_part where grp_id in (select grp_id from arch_grp where grp_nm = p_grp_nm) and part_id = p_part_id;
    delete from arch_grp_part where grp_id in (select grp_id from arch_grp where grp_nm = p_grp_nm) and part_id = p_part_id;
    commit;
    commit_audit;
  end;

  function get_depy(t_name varchar2, p_dir number, p_schema_owner varchar2) return pkg_arch.rc is
   c pkg_arch.rc;
  begin
    if p_dir = -1 then
      open c for
      SELECT other.table_name, other.delete_rule, other.CONSTRAINT_NAME,
         other.OWNER ,other.R_CONSTRAINT_NAME, other.R_OWNER
         FROM ALL_CONSTRAINTS this, ALL_CONSTRAINTS other
            WHERE this.constraint_name = other.r_constraint_name AND this.owner = p_schema_owner AND
               this.table_name =  t_name AND other.constraint_type = 'R' AND other.r_owner = this.owner;
    else
      open c for
      SELECT other.table_name, this.delete_rule, this.CONSTRAINT_NAME,
         other.OWNER ,this.R_CONSTRAINT_NAME, this.OWNER
         FROM ALL_CONSTRAINTS this, ALL_CONSTRAINTS other
            WHERE other.constraint_name = this.r_constraint_name AND this.owner = p_schema_owner AND
               this.table_name = t_name AND this.constraint_type = 'R' AND this.r_owner = other.owner;
    end if;

    return c;

  end;

  function get_join_condition(
      t_src varchar2,
      c_src varchar2,
      o_src varchar2,
      t_tgt varchar2,
      c_tgt varchar2,
      o_tgt varchar2) return varchar2 is

    cursor c is
     select s.COLUMN_NAME src_COLUMN_NAME, t.COLUMN_NAME tgt_COLUMN_NAME
      from all_cons_columns s, all_cons_columns t
      where s.owner = o_src and s.table_name = t_src and s.CONSTRAINT_NAME = c_src and
            t.owner = o_tgt and t.table_name = t_tgt and t.CONSTRAINT_NAME = c_tgt and
            t.POSITION = s.POSITION and s.owner = get_parm('OWNER')
            and t.owner = get_parm('OWNER');
    l_clause varchar(2000) ;
  begin
    for cr in c loop
        if l_clause is not null then
           l_clause := l_clause ||' and ';
        end if;
        l_clause := l_clause || 'T.'||cr.tgt_column_name||' = S.'||cr.src_column_name;
    end loop;
    return l_clause;
  end;


  procedure Prc_gen_depy_int(p_grp_nm varchar2,
                         p_src_tbl_nm varchar2,
                         p_dir number,
                         p_depth number default null,
                         p_level number default 0) is
     l_grp_id number;
     l_ref pkg_arch.rc;
     l_rec pkg_arch.rec_constraint;
     l_var1 varchar2(10);
     l_var2 varchar2(10);
     l_join varchar(500);
     l_tgt_typ varchar2(1);
     l_schema_owner varchar2(30);
  begin
    chk_auth;
    if (p_level > p_depth) then
        return;
    end if;

    if p_level = 0 then
       load_params(p_grp_nm);
    end if;

    if l_tabs is null then
       l_tabs := t_var(p_src_tbl_nm);
    else
       for i in l_tabs.first .. l_tabs.last loop
         if l_tabs(i) = p_src_tbl_nm then
            return;
         end if;
       end loop;
       l_tabs.extend(1);
       l_tabs(l_tabs.count) := p_src_tbl_nm;
    end if;

    select grp_id into l_grp_id from arch_grp where grp_nm = p_grp_nm;
    begin
      select val into l_schema_owner from arch_grp_parm where grp_id = l_grp_id and PARM_NM = 'OWNER';
    exception
     when no_data_found then
        raise_application_error(-20011, 'Data OWNER must be defined for the group "'||p_grp_nm||'"');
    end;

    l_ref := get_depy(p_src_tbl_nm, p_dir, l_schema_owner);
    loop
       fetch l_ref into l_rec;
       exit when l_ref%notfound;

       if p_dir = -1 then
          l_join := get_join_condition(p_src_tbl_nm, l_rec.R_CONSTRAINT_NAME, l_rec.R_owner,
                                l_rec.table_name, l_rec.CONSTRAINT_NAME, l_rec.owner);
          l_var1 := get_parm('ROOT ARCH_MODE');
          l_var2 := 'cascade';
          l_tgt_typ := 'C';
       else
          l_join := get_join_condition(p_src_tbl_nm, l_rec.CONSTRAINT_NAME, l_rec.owner,
                                l_rec.table_name, l_rec.R_CONSTRAINT_NAME, l_rec.r_owner);
          l_var1 := 'copy';
          l_var2 := 'none';
          l_tgt_typ := 'P';
       end if;

       begin
         Prc_add_tbl(p_grp_nm, l_rec.table_name);
       exception
         when dup_val_on_index then
           null;
       end;

       Prc_add_tbl_depy(p_grp_nm,
                        p_src_tbl_nm,
                        l_rec.table_name,
                        l_var1,
                        'none',
                        l_var2,
                        l_rec.CONSTRAINT_NAME,
                        l_join,
                        l_tgt_typ
                         ) ;

       Prc_gen_depy_int(p_grp_nm,
                         l_rec.table_name,
                         p_dir,
                         p_depth,
                         p_level+1);
    end loop;


  end;

  procedure Prc_gen_depy(p_grp_nm varchar2,
                         p_src_tbl_nm varchar2,
                         p_dir number,
                         p_depth number default null) is
  begin
     Prc_gen_depy_int( p_grp_nm, p_src_tbl_nm, p_dir, p_depth , 0);
  end;


  procedure Prc_adm_add(p_adm_nm varchar2 default user, p_adm_pswd varchar2 default null) is
    l_var varchar2(30) := null;
  begin
    chk_auth;
    if p_adm_pswd is not null then
       l_var := dbms_utility.get_hash_value(p_adm_pswd, 1000000, 1073741824);
    end if;
    insert into arch_adm (adm_nm, pswd, created_dtm, created_by, modified_dtm, modified_by) values
     (p_adm_nm, l_var, sysdate, get_parm('USERNAME'), null, null);
    audit('select * from arch_adm where adm_nm = '''||p_adm_nm||'''', 'arch_adm', 'I');
  end;

  procedure Prc_adm_pwd(p_adm_nm varchar2 default user, p_adm_pswd varchar2 default null) is
    l_var varchar2(30) := null;
  begin
    chk_auth;
    if p_adm_pswd is not null then
       l_var := dbms_utility.get_hash_value(p_adm_pswd, 1000000, 1073741824);
    end if;
    set_audit('select * from arch_adm where adm_nm = '''||p_adm_nm||'''', 'arch_adm', 'U');
    update arch_adm set pswd = l_var, modified_dtm = sysdate,
           modified_by = get_parm('USERNAME') where adm_nm = p_adm_nm;
    commit_audit;
  end;

  procedure Prc_adm_rm(p_adm_nm varchar2 ) is
  begin
    chk_auth;
    set_audit('select * from arch_adm where adm_nm = '''||p_adm_nm||'''', 'arch_adm', 'D');
    delete from  arch_adm where adm_nm = p_adm_nm;
    commit_audit;
  end;

  procedure Prc_adm_auth(p_adm_nm varchar2 default user, p_adm_pswd varchar2  default null) is
    l_count number;
  begin
    if p_adm_pswd is null then
       if p_adm_nm = user then
          set_parm('USERNAME', p_adm_nm);
       else
          raise_application_error(-20010, 'Authentication failed');
       end if;
    else
       select count(*) into l_count from arch_adm where adm_nm = p_adm_nm and
          pswd =  dbms_utility.get_hash_value(p_adm_pswd, 1000000, 1073741824);
    end if;
  end;
  procedure Prc_clean_audit_log(p_date_limit date, p_grp_nm varchar2 default null) is
     l_grp_id varchar2(10);
     n number;
     l_date date := sysdate;
  begin
    chk_auth;


    set_audit(null, 'arch_parm_hist', 'd');
    if p_grp_nm is null then
       delete from arch_parm_hist where modified_dtm > p_date_limit and modified_dtm < l_date;
    else
       select grp_id into l_grp_id from arch_grp where grp_nm = p_grp_nm;
       delete from arch_parm_hist a where modified_dtm > p_date_limit and
           a.old_val.extract('/ROWSET/ROW/GRP_ID/text()').getStringVal() = l_grp_id;
    end if;
    n := sql%rowcount;
    update arch_parm_hist set old_val = sys.XMLType.createXML('<PARM grp_nm = "'||nvl(p_grp_nm, 'NULL')
      ||'" limit_date="'|| to_char(p_date_limit, 'dd-mon-yyyy hh24:mi:ss')||'" rows="'||n||'"/>')
         where rowid = audit_rowid;
    commit;
    commit_audit;
  end;

  procedure Prc_chk_grp(p_grp_nm varchar2) is
  begin
    chk_auth;

    load_params(p_grp_nm );

    prc_chk_branch(get_parm('ROOT TABLE'));

  end;

  procedure Prc_create_grp(p_xml_cfg CLOB, p_new_owner varchar2 default null) is
    Parser      dbms_xmlparser.Parser;
    doc         dbms_xmldom.domdocument;
    docelem     dbms_xmldom.DOMElement;
    grplist     dbms_xmldom.DOMNodelist;
    tab_list    dbms_xmldom.DOMNodelist;
    grp_node    dbms_xmldom.domnode;
    v_grp_nm          varchar2(30);
    v_tbl_nm          varchar2(30);
    v_TGT_TABLESPACE  varchar2(30);
    v_STATUS          varchar2(30);
    v_NEXT_RUN        varchar2(30);
    v_ROOT_TBL_NM     varchar2(30);
    v_ROOT_ARCH_MODE  varchar2(30);

    function getNodeValue(p_node dbms_xmldom.domnode, p_nm varchar2) return varchar2 is
        nodelist1    dbms_xmldom.DOMNodelist;
        node1       dbms_xmldom.domnode;
    begin
        nodelist1 := dbms_xmldom.getChildNodes(p_node);
        for j in 0..dbms_xmldom.getLength(nodelist1)-1 loop
            node1 := dbms_xmldom.item(nodelist1, j);
            -- dbms_output.put_line(dbms_xmldom.getNodeName(node1));
            if dbms_xmldom.getNodeName(node1) = p_nm then
                return dbms_xmldom.getNodeValue( dbms_xmldom.getFirstChild(node1));
            end if;
        end loop;
        return null;
    end;
  begin
   chk_auth;

   Parser := dbms_xmlparser.newParser;
   dbms_xmlparser.parseCLOB(Parser, p_xml_cfg);

   doc   := dbms_xmlparser.getDocument(Parser);
   docelem := dbms_xmldom.getDocumentElement( doc );

   grplist := dbms_xmldom.getElementsByTagName(docelem, 'ARCH_GROUP');
   for i in 0..dbms_xmldom.getLength(grplist)-1 loop
       grp_node := dbms_xmldom.item(grplist, i); -- iterate i-th group
       -- find group name/attr
       v_grp_nm := getNodeValue(grp_node, 'GRP_NM');
       v_TGT_TABLESPACE := getNodeValue(grp_node, 'TGT_TABLESPACE');
       v_STATUS := getNodeValue(grp_node, 'STATUS');
       v_NEXT_RUN := getNodeValue(grp_node, 'NEXT_RUN');
       v_ROOT_TBL_NM := getNodeValue(grp_node, 'ROOT_TBL_NM');
       v_ROOT_ARCH_MODE := getNodeValue(grp_node, 'ROOT_ARCH_MODE');

       -- create group here
       Prc_add_group(v_grp_nm);
       -- set group parameters

       Prc_set_grp_parm(v_grp_nm, 'TGT_TABLESPACE', v_TGT_TABLESPACE);
       Prc_set_grp_parm(v_grp_nm, 'NEXT_RUN'      , v_NEXT_RUN);
       Prc_set_grp_parm(v_grp_nm, 'ROOT ARCH_MODE', v_ROOT_ARCH_MODE);

       tab_list := dbms_xmldom.getElementsByTagName(dbms_xmldom.makeElement(grp_node), 'O_ARCH_GRP_PARM');
       for j in 0..dbms_xmldom.getLength(tab_list)-1 loop
          if p_new_owner is not null and getNodeValue(dbms_xmldom.item(tab_list, j), 'PARM_NM') = 'OWNER' then
             Prc_set_grp_parm(v_grp_nm,
                'OWNER',
                UPPER(p_new_owner)
             );
          else
             Prc_set_grp_parm(v_grp_nm,
                getNodeValue(dbms_xmldom.item(tab_list, j), 'PARM_NM'),
                getNodeValue(dbms_xmldom.item(tab_list, j), 'VAL')
             );
          end if;

       end loop;
       -- reload parameters
       load_params(v_grp_nm);

       tab_list := dbms_xmldom.getElementsByTagName(dbms_xmldom.makeElement(grp_node), 'O_ARCH_TBL');
       -- add tables
       for j in 0..dbms_xmldom.getLength(tab_list)-1 loop
          v_tbl_nm := getNodeValue(dbms_xmldom.item(tab_list, j), 'TBL_NM');
          Prc_add_tbl(v_grp_nm, v_tbl_nm);
          Prc_set_tbl_parm(v_grp_nm, v_tbl_nm, 'STATUS', getNodeValue(dbms_xmldom.item(tab_list, j), 'STATUS'));
          Prc_set_tbl_parm(v_grp_nm, v_tbl_nm, 'FILTER', getNodeValue(dbms_xmldom.item(tab_list, j), 'FILTER'));
       end loop;

       Prc_set_grp_parm(v_grp_nm, 'ROOT TABLE'   , v_ROOT_TBL_NM);

       -- add table net (dependencies)
       tab_list := dbms_xmldom.getElementsByTagName(dbms_xmldom.makeElement(grp_node), 'O_ARCH_TBL_NET');
       for j in 0..dbms_xmldom.getLength(tab_list)-1 loop

           Prc_add_tbl_depy(v_grp_nm,
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'SRC_TBL_NM'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'TGT_TBL_NM'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'ARCH_MODE'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'SRC_REF'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'TGT_REF'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'REF_ROLE'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'JOIN_CLAUSE'),
                            getNodeValue(dbms_xmldom.item(tab_list, j), 'TGT_TYP')
                            ) ;
       end loop;

       -- set group status
       Prc_set_grp_parm(v_grp_nm, 'STATUS', v_STATUS);

    end loop;
  end;
  procedure Prc_Add_parm(p_parm_nm varchar2) is
  begin
    chk_auth;
    insert into ref_arch_parm (PARM_NM, IS_REQUIRED, CREATED_DTM, CREATED_BY)
        values (p_parm_nm, 'N', sysdate, get_parm('USERNAME'));
  end;

end PKG_ARCH;
/

                   /*      REF DATA     */
/*

declare
 --  upload archive required parameter list
 procedure add_ref_arch(p_nm varchar2, p_is_default varchar2) as
 begin
    insert into ref_arch_parm (PARM_NM, VAL, IS_REQUIRED, CREATED_BY, CREATED_DTM) values (p_nm, null, p_is_default, 'init', sysdate);
 exception
   when dup_val_on_index then
    null;
 end;
begin
  add_ref_arch ('RETAIN',   'Y');
  add_ref_arch ('ARCH_SIZE','Y');
  add_ref_arch ('PERIOD',   'N');
  add_ref_arch ('BATCH',    'Y');
  add_ref_arch ('OWNER',    'Y');
end;
/

begin
 -- bootstrap archive admin (on password set)
 insert into  arch_adm values (user, null, sysdate, user, null, null);
exception
 when dup_val_on_index then
   null;
end;
/

*/


