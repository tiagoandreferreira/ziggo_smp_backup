/*=========================================================================

  FILE INFO

    $Id: pkg_arch_obj.sql,v 1.1 2003/03/25 16:18:18 vadimg Exp $

  DESCRIPTION

    Create object types for PKG_ARCH - a set of procedure to setup and execute SMP data
    archiving and purging


  NOTES



  REVISION HISTORY
  * Based on CVS log
========================================================================*/

create or replace type o_arch_cmd as object (
    prio           number,
    action         VARCHAR2(5),
    TBL_NM         VARCHAR2(30));
/
create or replace type t_arch_cmd as table of o_arch_cmd;
/


create or replace type o_arch_tbl_log as object (
RUN_ID         number(12),
STARTED_DTM    DATE,
ENDED_DTM      DATE,
REC_CNT        NUMBER
) not final;
/

create or replace type t_arch_tbl_log as table of o_arch_tbl_log;
/



create or replace type o_arch_tbl_part as object (
tbl_nm       varchar2(30),
arch_tbl_nm  varchar2(30),
tbl_log      t_arch_tbl_log
);
/

create or replace type t_arch_tbl_part as table of o_arch_tbl_part;
/

create or replace type o_arch_part as object (
PART_ID       number(12),
CREATED_DTM   date,
LAST_RUN      date,
STATUS        varchar2(20),
FIRST_RUN     date,
tbl_part      t_arch_tbl_part);
/

create or replace type t_arch_part as table of o_arch_part;
/

create or replace type o_arch_tbl_log_ext under  o_arch_tbl_log (
tbl_nm varchar2(30),
arch_tbl_nm varchar2(30)
);
/

create or replace type t_arch_tbl_log_ext as table of o_arch_tbl_log_ext;
/

create or replace type o_arch_run as object (
RUN_ID         NUMBER(12),
STARTED_DTM    DATE,
ENDED_DTM      DATE,
STATUS         VARCHAR2(1),
SES_ID         VARCHAR2(30),
TOTAL_CNT      NUMBER,
DONE_CNT       NUMBER,
err_msg        VARCHAR2(1000),
tbl_part       t_arch_tbl_log_ext);
/

create or replace type t_arch_run as table of o_arch_run;
/

create or replace type o_arch_tbl_net as object (
SRC_TBL_NM    VARCHAR2(30),
TGT_TBL_NM    VARCHAR2(30),
REF_ROLE      VARCHAR2(30),
ARCH_MODE     VARCHAR2(5) ,
SRC_REF       VARCHAR2(10),
TGT_REF       VARCHAR2(10),
JOIN_CLAUSE   VARCHAR2(255),
TGT_TYP       VARCHAR2(1) )
/

create or replace type t_arch_tbl_net as table of o_arch_tbl_net
/

create or replace type o_arch_tbl as object (
TBL_NM        VARCHAR2(30),
FILTER        VARCHAR2(500),
STATUS        VARCHAR2(10),
dependency    t_arch_tbl_net)
/
create or replace type t_arch_tbl as table of o_arch_tbl
/
create or replace type o_arch_grp_parm as object (
PARM_NM       VARCHAR2(30),
VAL           VARCHAR2(255))
/
create or replace type t_arch_grp_parm as table of o_arch_grp_parm
/

create or replace type o_arch_grp as object (
GRP_ID          NUMBER(12),
GRP_NM          VARCHAR2(30),
TGT_TABLESPACE  VARCHAR2(30),
STATUS          VARCHAR2(10),
LAST_RUN        DATE        ,
NEXT_RUN        DATE        ,
ROOT_TBL_NM     VARCHAR2(20),
ROOT_ARCH_MODE  VARCHAR2(5) ,
arch_grp_parm   t_arch_grp_parm,
arch_tbl        t_arch_tbl)
/

