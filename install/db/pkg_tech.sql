---------------------------------------------------------------------------
-- FILE INFO
--    $Id: pkg_tech.sql,v 1.1 2003/10/08 18:46:13 vadimg Exp $
--
-- DESCRIPTION
--
-- Direct tech platform and template manipulation
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------

create or replace package pkg_adm_tech_platform as
  /* Remove tech platform including all dependent impacts.
   p_platform_nm  - case sensitive platf name
   */
  procedure rm_tech_platform(p_platform_nm varchar2);
  /* Remove template(s) including all dependent impacts.
    p_template_pattern - Oracle format regular expression for tmplt_nm, case sensitive
   */
  procedure rm_template(p_template_pattern varchar2);
  /* Remove template's dependent impacts. Template itself is not removed
    p_template_pattern - Oracle format regular expression for tmplt_nm, case sensitive
   */
  procedure rm_impact(p_template_pattern varchar2);
end;
/

create or replace package body pkg_adm_tech_platform as
  procedure rm_tech_platform(p_platform_nm varchar2) is
    l_tech_platform_typ_id  number;
  begin
    select tech_platform_typ_id into l_tech_platform_typ_id
      from tech_platform_typ where tech_platform_typ_nm = p_platform_nm;

           delete from sbnt_tech_platform where (tech_platform_id , mgmt_mode_id) in
           ( select tech_platform_id , mgmt_mode_id from tech_platform_mgmt_mode  where tech_platform_id  in (
               select tech_platform_id from tech_platform
                 where tech_platform_typ_id  = l_tech_platform_typ_id
             )
           );
        delete from tech_platform_mgmt_mode  where tech_platform_id  in (
          select tech_platform_id from tech_platform
            where tech_platform_typ_id  = l_tech_platform_typ_id
        );
        delete from tech_platform_parm  where tech_platform_id  in (
          select tech_platform_id from tech_platform
            where tech_platform_typ_id  = l_tech_platform_typ_id
        );

      delete from tech_platform  where tech_platform_typ_id  = l_tech_platform_typ_id ;
        delete from impact_tmplt_list where tech_impact_id in (
          select tech_impact_id from tech_impact  where tech_platform_typ_id  = l_tech_platform_typ_id
        );

        delete from impact_tmplt_list where tech_impact_id in (
          select tech_impact_id from tech_impact  where tech_platform_typ_id  = l_tech_platform_typ_id
        );
      delete from tech_impact  where tech_platform_typ_id  = l_tech_platform_typ_id;

      delete from tech_platform_typ_parm  where tech_platform_typ_id  = l_tech_platform_typ_id ;

      delete from subntwk_supp_plat where tech_platform_typ_id  = l_tech_platform_typ_id ;

    delete from tech_platform_typ  where tech_platform_typ_id  = l_tech_platform_typ_id ;

  end;

  procedure rm_template(p_template_pattern varchar2) is
  begin
    for cr in (select tmplt_id from tmplt where tmplt_nm like p_template_pattern) loop

         delete from ntwk_impact_parm_src where tmplt_id = cr.tmplt_id and tmplt_parm_id in (
           select sorb_parm_id from tmplt_parm where tmplt_id = cr.tmplt_id
         );
         delete from matching_parm_list where tmplt_id = cr.tmplt_id and tmplt_parm_id in (
           select tmplt_parm_id from tmplt_parm where tmplt_id = cr.tmplt_id
         );
         delete from matching_parm_list where nullified_tmplt_id = cr.tmplt_id and nullified_tmplt_parm_id in (
           select tmplt_parm_id from tmplt_parm where tmplt_id = cr.tmplt_id
         );
       delete from tmplt_parm where tmplt_id = cr.tmplt_id;

       delete from tmplt_depy where prereq_tmplt_id = cr.tmplt_id;

       delete from tmplt_depy where dependent_tmplt_id = cr.tmplt_id;

         delete from svc_impact_hdr_parm where (svc_action_id, seq_num) in (
           select svc_action_id, seq_num from svc_impact where tmplt_id = cr.tmplt_id
         );
         delete from svc_impact_parm where (svc_action_id, seq_num) in (
           select svc_action_id, seq_num from svc_impact where tmplt_id = cr.tmplt_id
         );
       delete from svc_impact where tmplt_id = cr.tmplt_id;

         delete from matching_parm_list where impact_tmplt_nullify_id in (
           select impact_tmplt_nullify_id from impact_tmplt_nullify where tmplt_id = cr.tmplt_id
         );
       delete from impact_tmplt_nullify where tmplt_id = cr.tmplt_id;

         delete from matching_parm_list where impact_tmplt_nullify_id in (
           select impact_tmplt_nullify_id from impact_tmplt_nullify where nullified_tmplt_id = cr.tmplt_id
         );
       delete from impact_tmplt_nullify where nullified_tmplt_id = cr.tmplt_id;

       delete from impact_tmplt_list where tmplt_id = cr.tmplt_id;

       delete from tmplt where tmplt_id = cr.tmplt_id;
    end loop;

  end;
  procedure rm_impact(p_template_pattern varchar2) is
  begin
    for cr in (select tmplt_id from tmplt where tmplt_nm like p_template_pattern) loop

         delete from ntwk_impact_parm_src where tmplt_id = cr.tmplt_id and tmplt_parm_id in (
           select sorb_parm_id from tmplt_parm where tmplt_id = cr.tmplt_id
         );

         delete from svc_impact_hdr_parm where (svc_action_id, seq_num) in (
           select svc_action_id, seq_num from svc_impact where tmplt_id = cr.tmplt_id
         );
         delete from svc_impact_parm where (svc_action_id, seq_num) in (
           select svc_action_id, seq_num from svc_impact where tmplt_id = cr.tmplt_id
         );
       delete from svc_impact where tmplt_id = cr.tmplt_id;

         delete from matching_parm_list where impact_tmplt_nullify_id in (
           select impact_tmplt_nullify_id from impact_tmplt_nullify where tmplt_id = cr.tmplt_id
         );
       delete from impact_tmplt_nullify where tmplt_id = cr.tmplt_id;

         delete from matching_parm_list where impact_tmplt_nullify_id in (
           select impact_tmplt_nullify_id from impact_tmplt_nullify where nullified_tmplt_id = cr.tmplt_id
         );
       delete from impact_tmplt_nullify where nullified_tmplt_id = cr.tmplt_id;

       delete from impact_tmplt_list where tmplt_id = cr.tmplt_id;

    end loop;
  end;
end;
/

/*  Test

exec pkg_adm_tech_platform.rm_tech_platform('bmcpatrol_x.x');
exec pkg_adm_tech_platform.rm_tech_platform('cip');
exec pkg_adm_tech_platform.rm_tech_platform('motorola_ems_x.x');
exec pkg_adm_tech_platform.rm_tech_platform('opensrs');
exec pkg_adm_tech_platform.rm_tech_platform('qip_x.x');
exec pkg_adm_tech_platform.rm_tech_platform('spectrum_x.x');
exec pkg_adm_tech_platform.rm_tech_platform('terayon_ems_x.x');
exec pkg_adm_tech_platform.rm_tech_platform('vital_access_x.x');

exec pkg_adm_tech_platform.rm_template('%');

exec pkg_adm_tech_platform.rm_impact('%');

 */

