#!/usr/bin/ksh
#--============================================================================
#--    $Id: portaone_update.sh,v 1.3 2013/03/08 07:49:28 poonamm Exp $
#--
#--  DESCRIPTION
#--
#--  RELATED SCRIPTS
#--
#--  REVISION HISTORY
#--  * Based on CVS log
#--============================================================================

CUR_DIR=`pwd`

echo ${CUR_DIR}

LOG_DATE=`date '+%Y%m%d.%H%M%S'`
LOG_FILE=${CUR_DIR}/portaone_update.$LOG_DATE.log

. ../../util/setEnv.sh

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    exit 1
fi

LOGIN=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}

echo
echo Updating logical_sim_ring_dn_prefix | tee -a $LOG_FILE


echo Executing portaone_update.sql | tee -a $LOG_FILE
cat  portaone_update/portaone_update.sql | sqlplus $LOGIN >> $LOG_FILE

#-------------------------------------------------------------------------
# Verify the result
#-------------------------------------------------------------------------

if egrep -s '^ERROR' $LOG_FILE || egrep -s '^Error' $LOG_FILE
then
    echo Errors found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s  ORA_* $LOG_FILE
then
    echo Oracle errors found. See \'$LOG_FILE\' for details
    exit 1
else
echo  Script execution completed successfully. | tee -a $LOG_FILE
	  echo See \'$LOG_FILE\' for details
      exit 0
fi


