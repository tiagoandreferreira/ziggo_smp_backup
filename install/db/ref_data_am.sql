--============================================================================
--    $Id: ref_data_am.sql,v 1.4 2007/06/26 00:37:13 alberte Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on
------------------------------------------------------------------
---------   Delete for table: Ref_Usr_status   ---------
------------------------------------------------------------------
Delete from Ref_Usr_Status;
------------------------------------------------------------------
---------   Data fill for table: Ref_Usr_Status   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_User_Status   ---------')
Insert into ref_usr_status(usr_status, descr, created_by, created_dtm)
Values ('a','active', 'INIT', SYSDATE);

Insert into ref_usr_status(usr_status, descr, created_by, created_dtm)
Values ('l','locked', 'INIT', SYSDATE);

Insert into ref_usr_status(usr_status, descr, created_by, created_dtm)
Values ('s','suspended', 'INIT', SYSDATE);

Insert into ref_usr_status(usr_status, descr, created_by, created_dtm)
Values ('e','pwd_expired', 'INIT', SYSDATE);
