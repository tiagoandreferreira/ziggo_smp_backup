--============================================================================
--    $Id: ref_data_cm.sql,v 1.56.14.3 2010/02/04 19:46:03 davidx Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Ref_Arch_Parm   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Arch_Parm   ---------')
Insert into Ref_Arch_Parm(parm_nm, val, is_required, created_dtm, created_by, modified_dtm, modified_by)
Values ('RETAIN',NULL,'Y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Arch_Parm(parm_nm, val, is_required, created_dtm, created_by, modified_dtm, modified_by)
Values ('ARCH_SIZE',NULL,'Y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Arch_Parm(parm_nm, val, is_required, created_dtm, created_by, modified_dtm, modified_by)
Values ('PERIOD',NULL,'N', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Arch_Parm(parm_nm, val, is_required, created_dtm, created_by, modified_dtm, modified_by)
Values ('BATCH',NULL,'Y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Arch_Parm(parm_nm, val, is_required, created_dtm, created_by, modified_dtm, modified_by)
Values ('OWNER',NULL,'Y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Arch_Parm(parm_nm, val, is_required, created_dtm, created_by, modified_dtm, modified_by)
Values ('MAX_HIERARCHY_LEVEL',NULL,'N', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Data_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Data_Typ   ---------')

Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('Boolean','Boolean', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('DateTime','DateTime', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('Float','Float', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('Integer','Integer', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('String','String', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('Password','Represents a password parameter.  Translates to string for SB.', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('State','StateMachine parm type', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Data_Typ(data_typ_nm, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('LongText','Long text parm type', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Ordr_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Ordr_Typ   ---------')
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('com.sigma.samp.cmn.order.BatchOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('SnapshotOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('SvcActOrderValue','INIT',sysdate,null,null); 
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('ReprovisionSubOrderValue','INIT',sysdate,null,null); 
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('ReprovOrderValue','INIT',sysdate,null,null); 
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('javax.oss.order.CreateOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('javax.oss.order.CancelOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('javax.oss.order.ModifyOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('com.sigma.samp.cmn.order.OssjReprovisionOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('com.sigma.samp.cmn.order.OssjSuspendOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('com.sigma.samp.cmn.order.OssjResumeOrderValue','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('EntityOrderSpec:ActionOrder','INIT',sysdate,null,null);
insert into REF_ORDR_TYP (ordr_typ,  created_by, created_dtm,modified_dtm,modified_by)  values
('ServiceOrderSpec:ActionOrder','INIT',sysdate,null,null);

------------------------------------------------------------------
---------   Data fill for table: Ref_Conversion_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Conversion_Typ   ---------')
Insert into Ref_Conversion_Typ(conversion_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('upper','Convert to Upper Case', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Conversion_Typ(conversion_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('lower','Convert to Lower Case', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Conversion_Typ(conversion_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('whitespace','Removes whitespaces from the parameter value', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Lang   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Lang   ---------')

Insert into Ref_Lang(lang_cd, is_dflt, created_dtm, created_by, modified_dtm, modified_by)
Values ('en','y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Lang(lang_cd, is_dflt, created_dtm, created_by, modified_dtm, modified_by)
Values ('fr','n', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Lang(lang_cd, is_dflt, created_dtm, created_by, modified_dtm, modified_by)
Values ('ja','n', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Locale_Country   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Locale_Country   ---------')
Insert into Ref_Locale_Country(locale_country_cd, is_default, created_dtm, created_by, modified_dtm, modified_by)
Values ('CA','y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Locale_Country(locale_country_cd, is_default, created_dtm, created_by, modified_dtm, modified_by)
Values ('JP','n', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Locale_Country(locale_country_cd, is_default, created_dtm, created_by, modified_dtm, modified_by)
Values ('US','n', SYSDATE, 'INIT', NULL, NULL);

------------------------------------------------------------------
---------   Data fill for table: Ref_Locale   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Locale   ---------')
Insert into Ref_Locale(locale_cd, lang_cd, locale_country_cd, created_dtm, created_by, modified_dtm, modified_by)
Values ('en_CA','en','CA', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Locale(locale_cd, lang_cd, locale_country_cd, created_dtm, created_by, modified_dtm, modified_by)
Values ('fr_CA','fr','CA', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Locale(locale_cd, lang_cd, locale_country_cd, created_dtm, created_by, modified_dtm, modified_by)
Values ('ja_JP','ja','JP', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Locale(locale_cd, lang_cd, locale_country_cd, created_dtm, created_by, modified_dtm, modified_by)
Values ('en_US','en','US', SYSDATE, 'INIT', NULL, NULL);
