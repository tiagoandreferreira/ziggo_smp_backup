--============================================================================
--    $Id: ref_data_dm.sql,v 1.3 2003/02/28 16:27:25 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo on
set serveroutput on
------------------------------------------------------------------
---------   Delete for table: ref_ticket_typ   ---------
------------------------------------------------------------------
Delete from ref_ticket_typ;
------------------------------------------------------------------
---------   Delete for table: ref_ticket_status   ---------
------------------------------------------------------------------
Delete from ref_ticket_status;
------------------------------------------------------------------
---------   Delete for table: ref_svc_advisory_status   ---------
------------------------------------------------------------------
Delete from ref_svc_advisory_status;
------------------------------------------------------------------
---------   Delete for table: ref_result_status   ---------
------------------------------------------------------------------
Delete from ref_result_status;
------------------------------------------------------------------
---------   Data fill for table: ref_ticket_typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_ticket_typ   ---------')
Insert into ref_ticket_typ(TICKET_NM, descr, created_dtm, created_by, modified_dtm, modified_by)
values ('INTERNAL', 'Internal Trouble Ticketing System - ITTS', SYSDATE, 'INIT', SYSDATE, 'INIT');
Insert into ref_ticket_typ(TICKET_NM, descr, created_dtm, created_by, modified_dtm, modified_by)
values ('EXTERNAL', 'External Trouble Ticketing System - ETTS(Remedy)', SYSDATE, 'INIT', SYSDATE, 'INIT');
------------------------------------------------------------------
---------   Data fill for table: ref_ticket_status   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_ticket_status   ---------')
Insert into ref_ticket_status(TICKET_STATUS_NM, descr, created_dtm, created_by, modified_dtm, modified_by)
values ('Open', 'Open trouble Ticket', SYSDATE, 'INIT', SYSDATE, 'INIT');
Insert into ref_ticket_status(TICKET_STATUS_NM, descr, created_dtm, created_by, modified_dtm, modified_by)
values ('Closed', 'Closed trouble ticket', SYSDATE, 'INIT', SYSDATE, 'INIT');
------------------------------------------------------------------
---------   Data fill for table: ref_svc_advisory_status   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_svc_advisory_status   ---------')
Insert into ref_svc_advisory_status(SVC_ADVISORY_STATUS_NM, created_dtm, created_by, modified_dtm, modified_by)
values ('Open', SYSDATE, 'INIT', SYSDATE, 'INIT');
Insert into ref_svc_advisory_status(SVC_ADVISORY_STATUS_NM, created_dtm, created_by, modified_dtm, modified_by)
values ('Closed', SYSDATE, 'INIT', SYSDATE, 'INIT');
------------------------------------------------------------------
---------   Data fill for table: ref_result_status   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_result_status   ---------')
Insert into ref_result_status(created_dtm, created_by, modified_dtm, modified_by, result_status_nm)
values (SYSDATE, 'INIT', SYSDATE, 'INIT', 'u');
Insert into ref_result_status(created_dtm, created_by, modified_dtm, modified_by, result_status_nm)
values (SYSDATE, 'INIT', SYSDATE, 'INIT', 'x');
Insert into ref_result_status(created_dtm, created_by, modified_dtm, modified_by, result_status_nm)
values (SYSDATE, 'INIT', SYSDATE, 'INIT', 's');
Insert into ref_result_status(created_dtm, created_by, modified_dtm, modified_by, result_status_nm)
values (SYSDATE, 'INIT', SYSDATE, 'INIT', 't');
Insert into ref_result_status(created_dtm, created_by, modified_dtm, modified_by, result_status_nm)
values (SYSDATE, 'INIT', SYSDATE, 'INIT', 'i');
Insert into ref_result_status(created_dtm, created_by, modified_dtm, modified_by, result_status_nm)
values (SYSDATE, 'INIT', SYSDATE, 'INIT', 'f');
