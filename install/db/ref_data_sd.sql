--============================================================================
--    $Id: ref_data_sd.sql,v 1.20 2006/04/10 15:18:30 dant Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on


------------------------------------------------------------------
---------   Data fill for table: Ref_Svc_Action_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Svc_Action_Typ   ---------')
Insert into Ref_Svc_Action_Typ(svc_action_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('a','add', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Svc_Action_Typ(svc_action_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('c','change', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Svc_Action_Typ(svc_action_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('d','delete', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Svc_Action_Typ(svc_action_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('g','diagnose', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Svc_Action_Typ(svc_action_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('p','provisionable', SYSDATE, 'INIT', NULL, NULL);

------------------------------------------------------------------
---------   Data fill for table: Ref_Sub_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Sub_Typ   ---------')
Insert into Ref_Sub_Typ(sub_typ_id, sub_typ_nm, descr, is_active, created_dtm, created_by, modified_dtm, modified_by)
Values (1,'residential','residential','y', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Sub_Typ(sub_typ_id, sub_typ_nm, descr, is_active, created_dtm, created_by, modified_dtm, modified_by)
Values (2,'soho','soho','n', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Sub_Typ(sub_typ_id, sub_typ_nm, descr, is_active, created_dtm, created_by, modified_dtm, modified_by)
Values (3,'business','business','n', SYSDATE, 'INIT', NULL, NULL);

