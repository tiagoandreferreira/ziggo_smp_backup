--============================================================================
--    $Id: ref_data_sp.sql,v 1.81.4.1.2.1.2.1 2012/01/05 03:24:52 mattl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Ref_Event   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Event   ---------')
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('order_submitted', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('order_completed', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('item_completed', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('item_cancelled', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('order_failed', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('failed', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('order_cancelled', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('completed', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('cancelled', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('item_failed', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('item_rejected', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('order_rejected', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('cancel_svc_order', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('error', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('pre_queued', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('queued', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('item_warning', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('enter_mt', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Event(event_nm, created_dtm, created_by, modified_dtm, modified_by)
Values ('exit_mt', SYSDATE, 'INIT', NULL, NULL);

------------------------------------------------------------------
---------   Data fill for table: Ref_Status_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Status_Typ   ---------')
Insert into Ref_Status_Typ(status_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('initial','Represents the initial state of the service or order', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Typ(status_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('transient','Represents the transient state when the order is in progress', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Typ(status_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('ultimate','Represents the final status of the service or order based on the action performed in the order or order item', SYSDATE, 
'INIT', NULL, NULL);
Insert into Ref_Status_Typ(status_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('final','No other transition of state is possible.', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Typ(status_typ, descr, created_dtm, created_by, modified_dtm, modified_by)
Values ('node','Represents a node', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Status_Chg_Reason   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Status_Chg_Reason   ---------')

Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (1,'default',NULL,'y',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (2,'default',NULL,'y',111, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (3,'default',NULL,'y',114, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (5,'default',NULL,'y',324, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (6,'default',NULL,'y',507, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (7,'default',NULL,'y',4, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (8,'other',NULL,'n',324, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (9,'moving',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (10,'unhappy_with_service',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (11,'changing_provider',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (12,'new_service',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (13,'abuse',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (14,'other',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (15,'new_subscriber',NULL,'n',111, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (16,'moving',NULL,'n',111, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (17,'disgruntled_subscriber',NULL,'n',111, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (18,'changing_provider',NULL,'n',111, SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (19,'other',NULL,'n',111, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (20,'abuse',NULL,'n',111, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (21,'disgruntled_subscriber',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (22,'new_subscriber',NULL,'n',100, SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status_Chg_Reason(reason_id, reason_nm, descr, is_dflt, class_id, created_dtm, created_by, modified_dtm, modified_by)
Values (23,'default',NULL,'y',700, SYSDATE, 'INIT', NULL, NULL);

------------------------------------------------------------------
---------   Data fill for table: Ref_Status   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Status   ---------')
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (68,114,'change_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (69,114,'inactive',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (7,111,'mso_block',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (8,111,'delete_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (81,4,'add_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (82,4,'active',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (83,4,'deleted',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (84,4,'activation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (85,4,'delete_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (86,4,'deactivation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (87,4,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (88,4,'change_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (89,4,'inactive',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (9,111,'deleted',NULL,'final', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (91,324,'open.not_running',NULL,'node', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (90,324,'open.not_running.queued',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (49,324,'open.not_running.pre_queued',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (110,324,'open.not_running.pre_queued.held',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (111,324,'open.not_running.pre_queued.scheduled',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (112,324,'open.not_running.pre_queued.prereq',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (92,324,'closed.aborted.invalid',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (93,507,'closed.completed.enforced',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (96,324,'open.running.zombie',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (48,324,'open.running',NULL,'node', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (59,507,'closed.aborted.invalid',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (57,507,'closed.aborted.enforced',NULL,'final', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (58,507,'open.running',NULL,'node', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (97,324,'closed.aborted.aborted_byclient',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (98,507,'closed.aborted.aborted_byclient',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (1,111,'add_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (10,111,'deactivation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (11,111,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (12,111,'change_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (13,111,'inactive',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (2,111,'activation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (3,111,'active',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (5,111,'courtesy_block',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (4,111,'courtesy_block_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (21,100,'add_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (22,100,'activation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (23,100,'active',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (24,100,'courtesy_block_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (25,100,'courtesy_block',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (26,100,'mso_block_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (27,100,'mso_block',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (28,100,'delete_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (29,100,'deleted',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (30,100,'deactivation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (31,100,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (32,100,'change_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (33,100,'inactive',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (41,324,'open.not_running.not_started',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (42,324,'open.running.workflow.failed',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (43,324,'open.running.workflow',NULL,'node', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (113,324,'open.running.workflow.warning',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (115,324,'open.running.workflow.processing',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (44,324,'closed.completed.all',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (45,324,'closed.completed.partially',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (46,324,'closed.aborted.aborted_byserver',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (47,324,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (116,324,'open.not_running.suspended',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);


Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (51,507,'open.not_running.not_started',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (52,507,'open.running.workflow',NULL,'node', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (53,507,'open.running.workflow.failed',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (54,507,'closed.completed.all',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (210,507,'open.running.workflow.processing',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (211,507,'open.running.workflow.manualtask',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (55,507,'closed.aborted.aborted_byserver',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (56,507,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (6,111,'mso_block_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (61,114,'add_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (62,114,'active',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (63,114,'deleted',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (64,114,'activation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (65,114,'delete_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (66,114,'deactivation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (67,114,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (101,700,'add_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (102,700,'active',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (103,700,'deleted',NULL,'final', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (104,700,'activation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (105,700,'delete_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (106,700,'deactivation_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (107,700,'previous_state',NULL,'ultimate', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (108,700,'change_in_progress',NULL,'transient', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Status(status_id, class_id, status, descr, status_typ, created_dtm, created_by, modified_dtm, modified_by)
Values (109,700,'inactive',NULL,'initial', SYSDATE, 'INIT', NULL, NULL);


------------------------------------------------------------------
---------   Data fill for table: Ref_Addr_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Addr_Typ   ---------')
Insert into Ref_Addr_Typ(addr_typ_id, addr_typ_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (1,'service', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Addr_Typ(addr_typ_id, addr_typ_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (2,'billing', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Addr_Typ(addr_typ_id, addr_typ_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (3,'contact', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Addr_Typ(addr_typ_id, addr_typ_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (4,'subscriber', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Contact_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Contact_Typ   ---------')
Insert into Ref_Contact_Typ(contact_typ_id, contact_typ_nm, created_dtm, created_by, modified_dtm, 
modified_by)
Values (1,'primary', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Contact_Typ(contact_typ_id, contact_typ_nm, created_dtm, created_by, modified_dtm, 
modified_by)
Values (2,'account', SYSDATE, 'INIT', NULL, NULL);

Insert into Ref_Contact_Typ(contact_typ_id, contact_typ_nm, created_dtm, created_by, modified_dtm, 
modified_by)
Values (3,'billing', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: Ref_Ordr_Action   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Ordr_Action   ---------')
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (1,'add', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (2,'update', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (3,'delete', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (4,'reprov', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (5,'suspend', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (6,'resume', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (7,'add_or_update', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (8,'add_or_ignore', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (9,'delete_if_exists', SYSDATE, 'INIT', NULL, NULL);
Insert into Ref_Ordr_Action(ordr_action_id, ordr_action_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (10,'mso_block', SYSDATE, 'INIT', NULL, NULL);

