--============================================================================
--    $Id: ref_data_st.sql,v 1.22 2007/10/05 14:08:27 andyl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo on
set serveroutput on

------------------------------------------------------------------
---------   Delete for table: Ref_Topology_Status   ---------
------------------------------------------------------------------
Delete from Ref_Topology_Status;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Topology_Status  ---------')
exec AddRefTopologyStatus('in_service');
exec AddRefTopologyStatus('out_of_service');
exec AddRefTopologyStatus('pending_activation');
exec AddRefTopologyStatus('pending_change');
exec AddRefTopologyStatus('pending_delete');

------------------------------------------------------------------
---------   Delete for table: Ref_Mgmt_Mode   ---------
------------------------------------------------------------------
Delete from Ref_Mgmt_Mode;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Mgmt_Mode  ---------')

--exec AddRefMgmtMode('provision');
--exec AddRefMgmtMode('diagnostic');


------------------------------------------------------------------
---------   Delete for table: ref_communication_directn   ---------
------------------------------------------------------------------
Delete from ref_comm_direction;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Ref_Communication_Directn   ---------')

insert into ref_comm_direction (COMM_DIRECTION_ID, COMM_DIRECTION_NM, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values(1, 'upstream', SYSDATE, 'Initial', NULL, NULL);
insert into ref_comm_direction (COMM_DIRECTION_ID, COMM_DIRECTION_NM, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values(2, 'downstream', SYSDATE, 'Initial', NULL, NULL);

------------------------------------------------------------------
---------   Delete for table: Ref_Impactd_Sub_Svc_Status   ---------
------------------------------------------------------------------
delete from ref_reprov_status;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_impactd_sub_svc_status---------')
insert into ref_reprov_status (REPROV_STATUS, REPROV_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('initial', 'Intial Status. Has not been sent to SPM', SYSDATE, 'Initial', NULL, NULL);

insert into ref_reprov_status (REPROV_STATUS, REPROV_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('failed', 'Provisioning failed', SYSDATE, 'Initial', NULL, NULL);

insert into ref_reprov_status (REPROV_STATUS, REPROV_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('completed', 'Provisioning succesful', SYSDATE, 'Initial', NULL, NULL);

insert into ref_reprov_status (REPROV_STATUS, REPROV_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_BY, MODIFIED_DTM) values('in_progress', 'Provisioning in progress', SYSDATE, 'Initial', NULL, NULL);

------------------------------------------------------------------
---------   Delete for table: Ref_Ip_Status   ---------
------------------------------------------------------------------
delete from Ref_Ip_Status;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_ip_status---------')
insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'free');

insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'reserved');

insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'allocated');

-- Be careful with this status, it is not the business view. Temperary
insert into ref_ip_status ( CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, IP_STATUS_NM) VALUES ( SYSDATE,'Harry', SYSDATE,'INIT', 'pending_delete');



------------------------------------------------------------------
---------   Delete for table: Ref_proj_status   ---------
------------------------------------------------------------------
delete from Ref_proj_status;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_proj_status---------')
exec  AddRefProjStatus('open','open');
exec  AddRefProjStatus('locked','locked ');
exec  AddRefProjStatus('staged','staged ');
exec  AddRefProjStatus('successful','successful ');
exec  AddRefProjStatus('aborted','aborted ');
exec  AddRefProjStatus('failed','failed ');
exec  AddRefProjStatus('init','intial ');
exec  AddRefProjStatus('provisioning','provisioning in progress ');
exec  AddRefProjStatus('synchronizing','external synchronization');
exec  AddRefProjStatus('synch_failed','external synchronization failed');

------------------------------------------------------------------
---------   Delete for table: Ref_task_status   ---------
------------------------------------------------------------------
delete from Ref_task_status;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_task_status---------')
exec  AddRefTaskStatus('available','available');
exec  AddRefTaskStatus('completed','completed ');
exec  AddRefTaskStatus('init','initial');

------------------------------------------------------------------
---------   Delete for table: Ref_task_status   ---------
------------------------------------------------------------------
delete from Ref_topology_action_typ;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_topology_action_typ---------')
exec AddRefTopologyActionTyp('lookup','lookup');
exec AddRefTopologyActionTyp('create','create');
exec AddRefTopologyActionTyp('delete','delete');
exec AddRefTopologyActionTyp('update','update');
exec AddRefTopologyActionTyp('assign','assign');
exec AddRefTopologyActionTyp('unassign','unassign');

------------------------------------------------------------------
---------   Delete for table: Ref_ntwk_cmpnt_typ---------
------------------------------------------------------------------
delete from Ref_ntwk_cmpnt_typ;

EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: ref_ntwk_cmpnt_typ---------')
exec AddRefNtwkCmpntTyp('subntwk','subnetwork');
exec AddRefNtwkCmpntTyp('assoc','association');
exec AddRefNtwkCmpntTyp('card','Card');
exec AddRefNtwkCmpntTyp('port','port');
exec AddRefNtwkCmpntTyp('port_pair','port pair');
exec AddRefNtwkCmpntTyp('link','link');
exec AddRefNtwkCmpntTyp('freq','frequency ');
exec AddRefNtwkCmpntTyp('freq_grp','frequency group ');
exec AddRefNtwkCmpntTyp('techn_platform','technology platform ');
exec AddRefNtwkCmpntTyp('consumable_rsrc','consumbable resource');

-- the name is too long, still don't know the reason why we have this command
--exec AddRefNtwkCmpntTyp('ChangeTechnologyPlatformCommand','change technology platform');

------------------------------------------------------------------
---------   Delete for table: Ref_Lock_Typ---------
------------------------------------------------------------------
insert into REF_LOCK_TYP(lock_typ, lock_typ_descr, created_dtm, created_by,modified_dtm,modified_by) values('exclusive', 'write lock',SYSDATE, 'INIT', SYSDATE, 'INIT');
insert into REF_LOCK_TYP(lock_typ, lock_typ_descr, created_dtm, created_by,modified_dtm,modified_by) values('shared', 'read lock',SYSDATE, 'INIT', SYSDATE, 'INIT');
