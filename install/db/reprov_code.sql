--==========================================================================
-- FILE INFO
--    $Id: reprov_code.sql,v 1.13 2007/09/21 15:16:43 davidx Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
CREATE OR REPLACE PACKAGE Pkg_St_Reprov
  IS
--
-- Purpose: Procedures to help with the performance of reprovisioning
--
-- MODIFICATION HISTORY
-- Person        Date        Comments
-- ---------     ------      ------------------------------------------
-- Parmi Sahota  2002-08-12  Module creation
-- Dan Tsipe     2005-03-12  Modified
-- Dan Tsipe     2005-07-18  Modified
-- Dan Tsipe     2005-07-28  Modified to include cpe logic
-- Dan Tsipe     2005-08-12  Modified to support multiple svc_delivery platforms
  l_debug_level NUMBER := 0;
  TYPE rec_sub_svc IS RECORD
       (sub_id NUMBER,
	    sub_svc_id NUMBER,
		ssn_group_id NUMBER);

  TYPE rec_sub_ssn_list_merged IS RECORD
       (subntwk_id NUMBER(12),
	    role_id VARCHAR2(20),
		role_nm VARCHAR2(1000));


  TYPE rec_sub_ssn_list IS RECORD
       (subntwk_id NUMBER(12),
	    role_id NUMBER(5));
		
  TYPE rec_svc_attr IS RECORD
       (cpe_subntwk_id NUMBER(12),
	    ver NUMBER(12));

  TYPE tb_svc_cpe_subntwk IS TABLE OF rec_svc_attr INDEX BY BINARY_INTEGER;
  TYPE tb_sub_svc IS TABLE OF rec_sub_svc INDEX BY BINARY_INTEGER;
  TYPE tb_sub_id IS TABLE OF SUB_SVC.sub_id%TYPE INDEX BY BINARY_INTEGER;
  TYPE tb_number IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE tb_sub_ssn_list_merged IS TABLE OF rec_sub_ssn_list_merged INDEX BY BINARY_INTEGER;
  TYPE tb_sub_ssn_list IS TABLE OF rec_sub_ssn_list INDEX BY BINARY_INTEGER;

  FUNCTION get_base_svc_id (p_subsvc_svcid svc.svc_id%TYPE)
  RETURN svc.svc_id%TYPE;
  FUNCTION is_cpe_subntwk (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
  RETURN CHAR;
  FUNCTION fnc_find_sub_id (p_sub_svc_id SUB_SVC.sub_svc_id%TYPE)
    RETURN SUB_SVC.sub_id%TYPE;
  PROCEDURE prc_find_ssn_list (p_sub_svc_id           IN  SUB_SVC_NTWK.sub_svc_id%TYPE,
                               p_cpe_role_subntwk_id  OUT NUMBER,
                               p_nep_role_subntwk_id  OUT NUMBER,
                               p_ssn_list             OUT o_cm_csv_list,
                               p_ver                  OUT NUMBER);
  FUNCTION fnc_get_subntwk_typ_nm (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    RETURN REF_SUBNTWK_TYP.subntwk_typ_nm%TYPE;
  FUNCTION fnc_get_ntwk_typ_nm (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    RETURN REF_NTWK_TYP.ntwk_typ_nm%TYPE;
  FUNCTION fnc_get_parent_subntwk_id (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    RETURN SUBNTWK.subntwk_id%TYPE;
  FUNCTION fnc_get_impacted_sub_count
    ( p_subntwk_list IN t_st_subntwk_list)
    RETURN NUMBER;
  FUNCTION fnc_get_cpe_subntwk_id (p_sub_svc_id SUB_SVC.sub_svc_id%TYPE)
  RETURN SUB_SVC_NTWK.subntwk_id%TYPE;
  FUNCTION fnc_combine_subntwks(
    p_copy_from_list   IN VARCHAR2)
  RETURN VARCHAR2;
  PROCEDURE prc_get_impacted_ssn_list
    ( p_subntwk_list IN  t_st_subntwk_list,
      p_ssn_list     OUT t_st_reprov_ssn,
      p_subsvc_list  OUT t_st_reprov_subsvc);
  PROCEDURE prc_update_ssn
    ( p_subntwk_list IN t_st_reprov_ssn,
      p_unlocked_sub_svc_list IN t_st_reprov_subsvc);
  PROCEDURE prc_debug_print_ssn_tmp(p_impacted_ssn_tmp t_st_reprov_ssn_tmp);
END; -- Package spec
/


CREATE OR REPLACE PACKAGE BODY Pkg_St_Reprov
 IS
--
   FUNCTION get_base_svc_id (p_subsvc_svcid SVC.svc_id%TYPE)
    RETURN SVC.svc_id%TYPE IS
    l_svc_id SVC.svc_id%TYPE;
  BEGIN
    SELECT base_svc_id INTO l_svc_id FROM SVC WHERE svc_id = p_subsvc_svcid;
    if (l_svc_id is null) then
      l_svc_id := p_subsvc_svcid;
    end if;
    RETURN l_svc_id;
  END;
--
  FUNCTION fnc_find_sub_id (p_sub_svc_id SUB_SVC.sub_svc_id%TYPE)
    RETURN SUB_SVC.sub_id%TYPE IS
    l_sub_id SUB_SVC.sub_id%TYPE;
  BEGIN
    SELECT sub_id INTO l_sub_id FROM SUB_SVC WHERE sub_svc_id = p_sub_svc_id;
    RETURN l_sub_id;
  END;
--
  FUNCTION fnc_get_subntwk_typ_nm (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    RETURN REF_SUBNTWK_TYP.subntwk_typ_nm%TYPE IS
    CURSOR c_subnetwork IS
      SELECT st.subntwk_typ_nm FROM SUBNTWK s, REF_SUBNTWK_TYP st
      WHERE  s.subntwk_id = p_subntwk_id
      AND    s.subntwk_typ_id = st.subntwk_typ_id;
    r_subnetwork c_subnetwork%ROWTYPE;
  BEGIN
    OPEN c_subnetwork;
    FETCH c_subnetwork INTO r_subnetwork;
    CLOSE c_subnetwork;
    RETURN r_subnetwork.subntwk_typ_nm;
  END;
--
  FUNCTION fnc_get_parent_subntwk_id (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    RETURN SUBNTWK.subntwk_id%TYPE IS
    CURSOR c_parent IS
      SELECT parent_subntwk_id FROM SUBNTWK s
      WHERE  s.subntwk_id = p_subntwk_id;
    r_parent c_parent%ROWTYPE;
  BEGIN
    OPEN c_parent;
    FETCH c_parent INTO r_parent;
    CLOSE c_parent;
    RETURN r_parent.parent_subntwk_id;
  END;
--
  FUNCTION fnc_get_ntwk_typ_nm (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    RETURN REF_NTWK_TYP.ntwk_typ_nm%TYPE IS
    CURSOR c_network IS
      SELECT nt.ntwk_typ_nm
      FROM   SUBNTWK s, REF_SUBNTWK_TYP st, REF_NTWK_TYP nt
      WHERE  s.subntwk_id = p_subntwk_id
      AND    s.subntwk_typ_id = st.subntwk_typ_id
      AND    st.ntwk_typ_id = nt.ntwk_typ_id;
    r_network c_network%ROWTYPE;
  BEGIN
    OPEN c_network;
    FETCH c_network INTO r_network;
    CLOSE c_network;
    RETURN r_network.ntwk_typ_nm;
  END;
--
  FUNCTION fnc_find_distinct_sub_svc_id (p_subntwk_list t_st_subntwk_list)
    RETURN tb_sub_svc IS
    l_subntwk_id1  SUBNTWK.subntwk_id%TYPE;
    l_subntwk_id2  SUBNTWK.subntwk_id%TYPE;

    sub_svc_id_tab  tb_sub_svc;
    l_subntwk_list o_cm_csv_list;
    i NUMBER;
	i_itr NUMBER;

    CURSOR c_single_subntwk (p_subntwk_id SUBNTWK.subntwk_id%TYPE) IS
	  SELECT c.sub_id,A.sub_svc_id,ssn_group_id
      FROM SUB_SVC_NTWK A,SUB_SVC c,SVC_SUPPORTED_PLATFORM B,SUB_SVC_DELIVERY_PLAT d
	  WHERE  A.sub_svc_id=c.sub_svc_id AND
      B.svc_delivery_plat_id=d.svc_delivery_plat_id AND get_base_svc_id(c.svc_id)=b.svc_id
      AND subntwk_id = p_subntwk_id AND A.sub_svc_id=d.sub_svc_id
	  --AND EXISTS (SELECT 0 FROM SUB_SVC B WHERE A.sub_svc_id=B.sub_svc_id AND sub_id IN (17529470,17529462))
	  ORDER BY c.sub_id,ssn_group_id,A.sub_svc_id;


    CURSOR c_dual_subntwk (p_subntwk_id1 SUBNTWK.subntwk_id%TYPE,
                           p_subntwk_id2 SUBNTWK.subntwk_id%TYPE) IS
      SELECT c.sub_id,A.sub_svc_id,ssn_group_id
      FROM SUB_SVC_NTWK A,SUB_SVC c,SVC_SUPPORTED_PLATFORM B,SUB_SVC_DELIVERY_PLAT d
	  WHERE  A.sub_svc_id=c.sub_svc_id AND
      B.svc_delivery_plat_id=d.svc_delivery_plat_id AND get_base_svc_id(c.svc_id)=B.svc_id
      AND subntwk_id = p_subntwk_id1 AND A.sub_svc_id=d.sub_svc_id
	  --AND EXISTS (SELECT 0 FROM SUB_SVC B WHERE A.sub_svc_id=B.sub_svc_id AND sub_id IN (17529470,17529462))
	  INTERSECT
	  SELECT c.sub_id,A.sub_svc_id,ssn_group_id
      FROM SUB_SVC_NTWK A,SUB_SVC c,SVC_SUPPORTED_PLATFORM B,SUB_SVC_DELIVERY_PLAT d
	  WHERE  A.sub_svc_id=c.sub_svc_id AND
      B.svc_delivery_plat_id=d.svc_delivery_plat_id AND get_base_svc_id(c.svc_id)=B.svc_id
      AND subntwk_id = p_subntwk_id2 AND A.sub_svc_id=d.sub_svc_id
	  --AND EXISTS (SELECT 0 FROM SUB_SVC B WHERE A.sub_svc_id=B.sub_svc_id AND sub_id IN (17529470,17529462))
	  ORDER BY 1,3,2;


  BEGIN
    i_itr:=1;
    FOR i IN p_subntwk_list.FIRST .. p_subntwk_list.LAST
    LOOP
        l_subntwk_list := p_subntwk_list(i);
        -- for now we will assume that there are only either 1 or 2 subnetworks in each list
        l_subntwk_id1 := l_subntwk_list.fnc_get_next_element;
        l_subntwk_id2 := l_subntwk_list.fnc_get_next_element;
        IF l_subntwk_id2 IS NULL
        THEN -- single subntwk
          FOR r_single_subntwk IN c_single_subntwk(p_subntwk_id => l_subntwk_id1) LOOP
              sub_svc_id_tab(i_itr).sub_svc_id := r_single_subntwk.sub_svc_id;
			  sub_svc_id_tab(i_itr).sub_id := r_single_subntwk.sub_id;
			  sub_svc_id_tab(i_itr).ssn_group_id := r_single_subntwk.ssn_group_id;
			  i_itr:=i_itr+1;
          END LOOP;
        ELSE -- dual subnetwork
          FOR r_dual_subntwk IN c_dual_subntwk(p_subntwk_id1 => l_subntwk_id1,
                                               p_subntwk_id2 => l_subntwk_id2) LOOP
	          sub_svc_id_tab(i_itr).sub_svc_id := r_dual_subntwk.sub_svc_id;
			  sub_svc_id_tab(i_itr).sub_id := r_dual_subntwk.sub_id;
			  sub_svc_id_tab(i_itr).ssn_group_id := r_dual_subntwk.ssn_group_id;
			  i_itr:=i_itr+1;
			--  dbms_output.put_line(r_dual_subntwk.sub_svc_id||' '||r_dual_subntwk.sub_id||' '||r_dual_subntwk.ssn_group_id);
          END LOOP;
        END IF;
    END LOOP;
	RETURN sub_svc_id_tab;
  END;
--
  FUNCTION fnc_get_impacted_sub_count
    ( p_subntwk_list IN t_st_subntwk_list)
    RETURN NUMBER IS
    -- MODIFICATION HISTORY
    -- Person        Date        Comments
    -- ---------     ------      ------------------------------------------
    -- Parmi Sahota  2002-08-12  Module creation
    l_sql_statement  VARCHAR2(30000);
    l_sql_single_subntwks VARCHAR2(30000);
    l_sql_multiple_subntwks VARCHAR2(30000);
    l_sql_multiple_snippet VARCHAR2(30000);
    l_elements_in_grp NUMBER;
    l_subntwk_list o_cm_csv_list;
    l_impacted_sub_count NUMBER;
  BEGIN
      FOR i IN p_subntwk_list.FIRST .. p_subntwk_list.LAST
      LOOP -- for all subnetwork lists
        l_subntwk_list := p_subntwk_list(i);
        l_elements_in_grp := p_subntwk_list(i).fnc_get_num_elements;
        IF l_elements_in_grp <= 1
        THEN -- it's a single group
          IF l_sql_single_subntwks IS NOT NULL THEN
            l_sql_single_subntwks := l_sql_single_subntwks || ',';
          END IF;
          l_sql_single_subntwks := l_sql_single_subntwks || p_subntwk_list(i).csv_list;
        ELSE -- it's a multiple group
        IF l_sql_multiple_subntwks IS NOT NULL THEN
          l_sql_multiple_subntwks := l_sql_multiple_subntwks || ' union ';
        END IF;
        l_sql_multiple_snippet := '';
        FOR j IN 1 .. l_elements_in_grp LOOP
          IF l_sql_multiple_snippet IS NOT NULL THEN
            l_sql_multiple_snippet := l_sql_multiple_snippet || ' intersect ';
          END IF;
          l_sql_multiple_snippet := l_sql_multiple_snippet ||
                                    'select sub_svc_id from sub_svc_ntwk where subntwk_id='||
                                    l_subntwk_list.fnc_get_next_element;
        END LOOP;
        l_sql_multiple_subntwks := l_sql_multiple_subntwks ||'('||l_sql_multiple_snippet||')';
        END IF;
      END LOOP;
      -- combine single and multiple sql_subntwks into one query.
      l_sql_statement := 'select count (distinct pkg_st_reprov.fnc_find_sub_id(DISTINCT_SUBSVCS.sub_svc_id)) into :p1 ' ||
                         'from (select distinct SUBSVCS.sub_svc_id from (';
        IF l_sql_single_subntwks IS NOT NULL
        THEN -- combine single
          l_sql_statement := l_sql_statement ||
                             'select sub_svc_id from sub_svc_ntwk where subntwk_id in ('||
                             l_sql_single_subntwks || ')';
        END IF;
        IF l_sql_multiple_subntwks IS NOT NULL
        THEN -- combine multiple
          IF l_sql_single_subntwks IS NOT NULL THEN
            l_sql_statement := l_sql_statement || '
 ';
          END IF;
          l_sql_statement := l_sql_statement ||l_sql_multiple_subntwks;
        END IF;
        l_sql_statement := l_sql_statement ||') SUBSVCS ) DISTINCT_SUBSVCS' ;
 /*     dbms_output.put_line(substr('l_sql_statement: '||l_sql_statement,1,150));
      dbms_output.put_line(substr('l_sql_statement: '||l_sql_statement,151,150));
      dbms_output.put_line(substr('l_sql_statement: '||l_sql_statement,301,150));
      dbms_output.put_line(substr('l_sql_statement: '||l_sql_statement,451,150)); */
      EXECUTE IMMEDIATE l_sql_statement INTO l_impacted_sub_count;
      RETURN l_impacted_sub_count;
  EXCEPTION
    WHEN OTHERS THEN
       RAISE ;
  END;
  --
  FUNCTION is_cpe_subntwk (p_subntwk_id SUBNTWK.subntwk_id%TYPE)
    -- Purpose: Checks whether or not the passed in subnetwork is CPE subnetwork.
    -- MODIFICATION HISTORY
    -- Person        Date        Comments
    -- ---------     ------      ------------------------------------------
    -- Parmi Sahota  2002-08-18  Module creation
  RETURN CHAR
  IS
  CURSOR c_ntwk_typ IS
      SELECT ntwk_typ_nm FROM SUBNTWK s, REF_SUBNTWK_TYP st, REF_NTWK_TYP nt
      WHERE s.subntwk_id = p_subntwk_id
      AND   s.subntwk_typ_id = st.subntwk_typ_id
      AND   st.ntwk_typ_id = nt.ntwk_typ_id;
      r_ntwk_typ c_ntwk_typ%ROWTYPE;
      l_cpe CHAR(1);
  BEGIN
    OPEN c_ntwk_typ;
    FETCH c_ntwk_typ INTO r_ntwk_typ;
    CLOSE c_ntwk_typ;
    IF r_ntwk_typ.ntwk_typ_nm = 'cpe'
    THEN l_cpe := 'y';
    ELSE l_cpe := 'n';
    END IF;
    RETURN l_cpe;
  END;
  --
  PROCEDURE prc_find_ssn_list (p_sub_svc_id           IN  SUB_SVC_NTWK.sub_svc_id%TYPE,
                               p_cpe_role_subntwk_id  OUT NUMBER,
                               p_nep_role_subntwk_id  OUT NUMBER,
                               p_ssn_list             OUT o_cm_csv_list,
                               p_ver                  OUT NUMBER)
    -- Purpose: finds the subnetworks in the ssn for the given subsvc
    --          excluding subnetworks of type CPE.
    -- MODIFICATION HISTORY
    -- Person        Date        Comments
    -- ---------     ------      ------------------------------------------
    -- Parmi Sahota  2002-08-19  Module creation
   IS
  CURSOR c_ssn_list IS
      SELECT Ssn.subntwk_id,Ssn.ntwk_role_id , ssp.ver
      FROM SUB_SVC_NTWK Ssn, SUB_SVC_DELIVERY_PLAT ssp
      WHERE Ssn.sub_svc_id = p_sub_svc_id AND ssp.sub_svc_id = Ssn.sub_svc_id
      ORDER BY Ssn.subntwk_id;
    l_nep_ntwk_role_id  NUMBER;
    l_cpe_ntwk_role_id  NUMBER;
  BEGIN
    p_ssn_list := o_cm_csv_list('');
    SELECT ntwk_role_id INTO l_nep_ntwk_role_id
    FROM REF_NTWK_ROLE WHERE ntwk_role_nm = 'network_entry_point';
    SELECT ntwk_role_id INTO l_cpe_ntwk_role_id
    FROM REF_NTWK_ROLE WHERE ntwk_role_nm = 'cpe';
    FOR r_ssn_list IN c_ssn_list LOOP
      p_ver := r_ssn_list.ver;
      IF r_ssn_list.ntwk_role_id = l_nep_ntwk_role_id
      THEN
        p_nep_role_subntwk_id := r_ssn_list.subntwk_id;
      END IF;
      IF r_ssn_list.ntwk_role_id = l_cpe_ntwk_role_id
      THEN
        p_cpe_role_subntwk_id := r_ssn_list.subntwk_id;
      END IF;
        -- save only the non-cpe subnetworks and keep nep anyway
     IF (is_cpe_subntwk(p_subntwk_id => r_ssn_list.subntwk_id)) = 'n' THEN
         -- OR r_ssn_list.ntwk_role_id = l_nep_ntwk_role_id
         --      if is_cpe_subntwk(p_subntwk_id => r_ssn_list.subntwk_id) = 'n' then
          p_ssn_list.add_element(TO_CHAR(r_ssn_list.subntwk_id)||','||TO_CHAR(r_ssn_list.ntwk_role_id));
          IF r_ssn_list.ntwk_role_id IS  NULL
          THEN
             p_cpe_role_subntwk_id := r_ssn_list.subntwk_id;
          END IF;
     END IF;
    END LOOP;
  END;

  FUNCTION fnc_combine_subntwks(
    p_copy_from_list   IN VARCHAR2)
  RETURN VARCHAR2 IS

    l_sub_ssn_list_merged  tb_sub_ssn_list_merged;
    l_ssn_iterator NUMBER;
    l_ssn_list o_cm_csv_list;
    i NUMBER;
    l_subntwk_id NUMBER(12);
    l_ntwk_role_id NUMBER(5);
    l_ntwk_role_nm VARCHAR2(50);
	l_copy_to_list VARCHAR2(30000):=NULL;
	l_copy_from_list_iterator NUMBER;
	l_sub_ssn_list_merged_iterator NUMBER;
    l_copy_from_list  o_cm_csv_list;



  BEGIN
    l_copy_from_list := o_cm_csv_list(p_copy_from_list);
	l_subntwk_id := l_copy_from_list.fnc_get_next_element;
    l_ntwk_role_id := l_copy_from_list.fnc_get_next_element;
    WHILE l_subntwk_id IS NOT NULL LOOP
      l_copy_from_list_iterator:=l_subntwk_id;
	  --dbms_output.put_line(l_subntwk_id||' '||l_ntwk_role_id);
	   --IF is_cpe_subntwk(l_subntwk_id)='n' THEN
		SELECT ntwk_role_nm INTO l_ntwk_role_nm FROM REF_NTWK_ROLE WHERE ntwk_role_id=l_ntwk_role_id;
	    IF l_sub_ssn_list_merged.EXISTS(l_subntwk_id) THEN
		   IF INSTR(l_sub_ssn_list_merged(l_copy_from_list_iterator).role_nm,l_ntwk_role_nm) = 0 THEN
		      IF INSTR(l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id,'.') = 0 THEN
			     IF TO_NUMBER(l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id) < l_ntwk_role_id THEN
				    l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id:=TO_CHAR(l_ntwk_role_id)||'.'||l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id;
			     ELSE
				    l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id:=l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id||'.'||TO_CHAR(l_ntwk_role_id);
			     END IF;
			   END IF;
			  l_sub_ssn_list_merged(l_copy_from_list_iterator).role_nm:=l_sub_ssn_list_merged(l_copy_from_list_iterator).role_nm||','||l_ntwk_role_nm;
		   END IF;
		ELSE
		   --dbms_output.put_line(l_subntwk_id||' '||l_ntwk_role_id);
		   l_sub_ssn_list_merged(l_copy_from_list_iterator).subntwk_id:=l_subntwk_id;
		   l_sub_ssn_list_merged(l_copy_from_list_iterator).role_id:=TO_CHAR(l_ntwk_role_id);
		   l_sub_ssn_list_merged(l_copy_from_list_iterator).role_nm:=l_ntwk_role_nm;
		END IF;
	 -- END IF;
	    l_subntwk_id := l_copy_from_list.fnc_get_next_element;
        l_ntwk_role_id := l_copy_from_list.fnc_get_next_element;
	END LOOP;
    l_sub_ssn_list_merged_iterator:=l_sub_ssn_list_merged.FIRST;
	WHILE l_sub_ssn_list_merged_iterator  IS NOT NULL LOOP
	   l_copy_to_list:=l_copy_to_list||l_sub_ssn_list_merged(l_sub_ssn_list_merged_iterator).subntwk_id||','||l_sub_ssn_list_merged(l_sub_ssn_list_merged_iterator).role_id||',';
       l_sub_ssn_list_merged_iterator:=l_sub_ssn_list_merged.NEXT(l_sub_ssn_list_merged_iterator);
	END LOOP;
	l_copy_to_list:=SUBSTR(l_copy_to_list,1,LENGTH(l_copy_to_list)-1);
	RETURN l_copy_to_list;
 END;



  PROCEDURE prc_get_impacted_ssn_list
    ( p_subntwk_list IN  t_st_subntwk_list,
      p_ssn_list     OUT t_st_reprov_ssn,
      p_subsvc_list  OUT t_st_reprov_subsvc)
    IS
    -- Purpose: gets the distinct list of SSNs that are impacted for network changes
    -- specified by the input list of subnetworks p_subntwk_list.
    -- We first get the unique list of sub_svcs, and then for each sub_svc
    -- determine what network elements they need excluding
    -- CPE subnetworks.
    -- then we should combine the SSNs of all subnetworks using ssn_group_id of svc_supported_platform table.
    -- MODIFICATION HISTORY
    -- Person        Date        Comments
    -- ---------     ------      ------------------------------------------
    -- Parmi Sahota  2002-08-12  Module creation
    -- Dan Tsipe     2005-03-12  Modified to support 4119b requirements


    sub_svc_id_tab_bin tb_sub_svc;
    l_sub_svc_id NUMBER;
    l_ssn_list  o_cm_csv_list;
	l_tb_svc_cpe_subntwk tb_svc_cpe_subntwk;

    l_impacted_ssn_tmp t_st_reprov_ssn_tmp := t_st_reprov_ssn_tmp();
    l_impacted_sub_ssn_tmp t_ST_REPROV_SUB_SVC_SSN_TMP:=t_ST_REPROV_SUB_SVC_SSN_TMP();
    l_list_seq NUMBER := 0;
    l_csv_list o_cm_csv_list;
	l_csv_list1 o_cm_csv_list;
    l_old_subntwk_id SUB_SVC_NTWK.subntwk_id%TYPE;
    l_subntwk_typ_nm REF_SUBNTWK_TYP.subntwk_typ_nm%TYPE;
    l_ntwk_role_id_list   VARCHAR2(255);

    l_ssn_list_iterator NUMBER := 0;
    l_sub_svc_list_iterator NUMBER := 0;
    l_impacted_ssn_tmp_iterator NUMBER:=0;
    l_impacted_sub_ssn_tmp_iter NUMBER:=0;
    l_previous_ssn_list VARCHAR2(30000);

    l_nep_subntwk_id  NUMBER;
    l_cpe_subntwk_id  NUMBER;
    l_svc_ssn_list1 VARCHAR2(30000);
    l_svc_ssn_list2 VARCHAR2(30000);
    l_sub_ssn_list1  VARCHAR2(30000);
    l_sub_svc_list VARCHAR2(30000);
    l_svc_list VARCHAR2(30000);

    l_ver           NUMBER(12);
    l_nep_ntwk_role_id  VARCHAR2(5);
	l_sub_id   NUMBER(12);
	l_ssn_group_id NUMBER(2);
	l_sub_id_prev   NUMBER(12);
	l_ssn_group_id_prev NUMBER(2);
    l_sub_svc_id_tab_bin_iterator NUMBER:=0;
	l_sub_ssn_list tb_sub_ssn_list;
	l_sub_svc_iterator NUMBER:=0;


    FUNCTION is_nep(p_list VARCHAR2) RETURN VARCHAR2 AS
      idx NUMBER :=0 ;
    BEGIN
        LOOP
          idx := INSTR(p_list, l_nep_ntwk_role_id, idx + 1);
          IF idx = 0 THEN
             RETURN 'n';
          END IF;
          IF idx > 1 THEN
             IF INSTR('1234567890', SUBSTR(p_list, idx -1, 1)) > 0 THEN
                NULL;
             ELSE
                IF INSTR('1234567890', SUBSTR(p_list, idx + LENGTH(l_nep_ntwk_role_id), 1)) > 0 THEN
                   NULL;
                ELSE
                   RETURN 'y';
                END IF;
             END IF;
          ELSE
                IF INSTR('1234567890', SUBSTR(p_list, idx + LENGTH(l_nep_ntwk_role_id), 1)) > 0 THEN
                   NULL;
                ELSE
                   RETURN 'y';
                END IF;
          END IF;
        END LOOP;
        RETURN 'n';

    END;

  BEGIN
    p_subsvc_list := t_st_reprov_subsvc();
    p_ssn_list    := t_st_reprov_ssn();
	

    --find the list of impacted sub_svc_ids
    sub_svc_id_tab_bin := fnc_find_distinct_sub_svc_id (p_subntwk_list => p_subntwk_list);
    l_impacted_ssn_tmp_iterator := 0;  -- subscript of impacted_ssn_list_tmp
    l_sub_svc_id_tab_bin_iterator:=sub_svc_id_tab_bin.FIRST;

	-- find ssn list and other attributes for every impacted service
	WHILE l_sub_svc_id_tab_bin_iterator IS NOT NULL LOOP
	  -----------------Debug Message--------------------------------
	  IF l_debug_level > 0 THEN
	   dbms_output.put_line(sub_svc_id_tab_bin.COUNT);
	   dbms_output.put_line(sub_svc_id_tab_bin(l_sub_svc_id_tab_bin_iterator).sub_svc_id||' '||sub_svc_id_tab_bin(l_sub_svc_id_tab_bin_iterator).sub_id||' '||sub_svc_id_tab_bin(l_sub_svc_id_tab_bin_iterator).ssn_group_id);
	  END IF;
	  -------------------------------------------------------------
	    --- reading sub_id,sub_svc_id snd ssn_group of the impacted service
        l_sub_id:=sub_svc_id_tab_bin(l_sub_svc_id_tab_bin_iterator).sub_id;
	    l_sub_svc_id:=sub_svc_id_tab_bin(l_sub_svc_id_tab_bin_iterator).sub_svc_id;
		l_ssn_group_id:=sub_svc_id_tab_bin(l_sub_svc_id_tab_bin_iterator).ssn_group_id;
        l_impacted_ssn_tmp.EXTEND;
        l_impacted_ssn_tmp_iterator:=l_impacted_ssn_tmp_iterator+1;
		--- find ssn list for every impacted service
		prc_find_ssn_list(p_sub_svc_id           => l_sub_svc_id,
		                  p_cpe_role_subntwk_id  => l_cpe_subntwk_id,
                          p_nep_role_subntwk_id  => l_nep_subntwk_id,
                          p_ssn_list             => l_ssn_list,
                          p_ver                  => l_ver);
		l_tb_svc_cpe_subntwk(l_sub_svc_id).cpe_subntwk_id:=l_cpe_subntwk_id;
		l_tb_svc_cpe_subntwk(l_sub_svc_id).ver:=l_ver;
		 
		  -- filling out the object with the info obtained above
        l_impacted_ssn_tmp(l_impacted_ssn_tmp_iterator) := o_st_reprov_ssn_tmp(l_sub_id,
		                                                                       l_sub_svc_id,
                                                                               l_ssn_list.csv_list,
                                                                               l_cpe_subntwk_id,
                                                                               l_nep_subntwk_id,
                                                                               'n',
                                                                               l_ver,
																			   l_ssn_group_id);
	    -- read next impacted service
       l_sub_svc_id_tab_bin_iterator:=sub_svc_id_tab_bin.NEXT(l_sub_svc_id_tab_bin_iterator);
     END LOOP;
	 -------------------------------DEBUG--------------------------------
	 IF l_debug_level > 0 THEN
		l_sub_svc_id:=l_tb_svc_cpe_subntwk.FIRST;
	    WHILE l_sub_svc_id  IS NOT NULL LOOP
		  dbms_output.put_line(l_sub_svc_id||' '||l_tb_svc_cpe_subntwk(l_sub_svc_id).cpe_subntwk_id);
		  l_sub_svc_id:=l_tb_svc_cpe_subntwk.next(l_sub_svc_id);
		END LOOP; 
	 END IF;	
	 -------------------------Debug Message ----------------------------------------
	 IF l_debug_level > 0 THEN
	  FOR i IN l_impacted_ssn_tmp.FIRST .. l_impacted_ssn_tmp.last LOOP
	    dbms_output.put_line('sub_id:'||l_impacted_ssn_tmp(i).sub_id||' sub_svc_id:'||
		l_impacted_ssn_tmp(i).sub_svc_id||' ssn_list:'||
        l_impacted_ssn_tmp(i).ssn_list);
        dbms_output.put_line('cpe:'||l_impacted_ssn_tmp(i).cpe_role_subntwk_id||' nep:'||
        l_impacted_ssn_tmp(i).nep_role_subntwk_id||' DELETE:'||
        l_impacted_ssn_tmp(i).delete_flag||' ver:'||
        l_impacted_ssn_tmp(i).ver||' ssn_group_id:'||
        l_impacted_ssn_tmp(i).ssn_group_id);
		 dbms_output.put_line('----------------------------------------------');
	  END LOOP;
	 END IF;
	 --------------------------------------------------------------------------------
   	l_sub_id_prev:=-1;
	l_ssn_group_id_prev:=-1;
	-- merge subscriber ssn's based on sub_id and ssn_group, merge only ssn's in the context of single subscriber
	-- the data in l_impacted_ssn_tmp is ordered by sub_id,ssn_group and service_id
	FOR i IN 1 .. l_impacted_ssn_tmp_iterator LOOP
	    l_sub_id:=l_impacted_ssn_tmp(i).sub_id;
	    l_ssn_group_id:=l_impacted_ssn_tmp(i).ssn_group_id;
		l_svc_ssn_list1 := l_impacted_ssn_tmp(i).ssn_list;
		l_sub_svc_id:=l_impacted_ssn_tmp(i).sub_svc_id;
		-- a subscriber id has changed
	    IF l_sub_id_prev <> l_sub_id THEN
		   -- check that it is not the first subscriber in the list
		   IF l_sub_id_prev <> -1 THEN
		     ------------------------Debug Message -------------------------------
		     IF l_debug_level > 0 THEN
		      dbms_output.put_line('Send list to merge:');
			  dbms_output.put_line(SUBSTR(l_svc_ssn_list2,1,255));
			  dbms_output.put_line(SUBSTR(l_svc_ssn_list2,256,255));
			 END IF;
			 ----------------------------------------------------------------------
			  -- send ssn list for merge  
			  l_sub_ssn_list1:= fnc_combine_subntwks(p_copy_from_list => l_svc_ssn_list2);
			  l_impacted_sub_ssn_tmp_iter:=l_impacted_sub_ssn_tmp_iter+1;
			  l_impacted_sub_ssn_tmp.EXTEND;
			  -- store merged ssn list and its assosiated service list
			  l_impacted_sub_ssn_tmp(l_impacted_sub_ssn_tmp_iter) := O_ST_REPROV_SUB_SVC_SSN_TMP(l_sub_ssn_list1,
                                                                                                     l_sub_svc_list);
			 -----------------------Debug Message -------------------------------
			 IF l_debug_level > 0 THEN
			  dbms_output.put_line('out: '||SUBSTR(l_sub_ssn_list1,1,255));
			  dbms_output.put_line('out: '||SUBSTR(l_sub_ssn_list1,256,255));
			  dbms_output.put_line('out svc: '||l_sub_svc_list);
			 END IF;
			 ---------------------------------------------------------------------
			  ---  nulify SSN list before next loop iteration
			  l_svc_ssn_list2:=NULL;
		   END IF;
		  --- Prepare all variables for new subs iteration
		  l_svc_ssn_list2 := l_svc_ssn_list1;
		  l_sub_svc_list:=l_sub_svc_id;
		  l_sub_id_prev:=l_sub_id;
		  l_ssn_group_id_prev:=l_ssn_group_id;
		  
		 -- Subs has not changed then check that ssn_group has not changed yet
		 -- Change of SSN group will send SSN list for the merge
         ELSIF l_ssn_group_id_prev <> l_ssn_group_id THEN
		   -- it's fisrt iteration for the ssn_group within subsciber 
		   IF l_ssn_group_id_prev <> -1 THEN
		      ------------------------------------Debug ----------------------------
		      IF l_debug_level > 0 THEN
		       dbms_output.put_line('Send list TO merge:');
		       dbms_output.put_line(SUBSTR(l_svc_ssn_list2,1,255));
			   dbms_output.put_line(SUBSTR(l_svc_ssn_list2,256,255));
			   ---------------------------------------------------------------------
			  END IF;
			   ---- combine and  store result
			   l_sub_ssn_list1:= fnc_combine_subntwks(p_copy_from_list => l_svc_ssn_list2);
			   l_impacted_sub_ssn_tmp_iter:=l_impacted_sub_ssn_tmp_iter+1;
			   l_impacted_sub_ssn_tmp.EXTEND;
			   l_impacted_sub_ssn_tmp(l_impacted_sub_ssn_tmp_iter) := O_ST_REPROV_SUB_SVC_SSN_TMP(l_sub_ssn_list1, l_sub_svc_list);
                --------------------------- Debug Message --------------------------                                                                                    
			    IF l_debug_level > 0 THEN
			     dbms_output.put_line('OUT: '||SUBSTR(l_sub_ssn_list1,1,255));
			     dbms_output.put_line('OUT: '||SUBSTR(l_sub_ssn_list1,256,255));
			     dbms_output.put_line('out svc: '||l_sub_svc_list);
			    END IF;
				---------------------------------------------------------------------
				-- nulify SSN list before next loop iteration
			    l_svc_ssn_list2:=NULL;
		   END IF;
		  ---  Prepare all variables for new SSN_Group iteration
		  l_svc_ssn_list2 := l_svc_ssn_list1;
		  l_sub_svc_list:=l_sub_svc_id;
		  l_ssn_group_id_prev :=l_ssn_group_id;
	    ELSE
		  -- the ssn list of the current service  can be combined with the previos ssn list
		  -- all this in the context of the same subscriber and ssn_group 
		  l_svc_ssn_list2 := l_svc_ssn_list2||','||l_svc_ssn_list1;
		  l_sub_svc_list:=l_sub_svc_list||','||l_sub_svc_id;
	    END IF;
    END LOOP;
    --- Special processing for the last record in the list	
	IF l_svc_ssn_list2 IS NOT NULL THEN
	  --------------------------  Debug Message--------------------------------------
	  IF l_debug_level > 0 THEN
	           dbms_output.put_line('Send list TO merge:');
		       dbms_output.put_line(SUBSTR(l_svc_ssn_list2,1,255));
			   dbms_output.put_line(SUBSTR(l_svc_ssn_list2,256,255));
	  END IF;
	  -------------------------------------------------------------------------------
			   l_sub_ssn_list1:= fnc_combine_subntwks(p_copy_from_list => l_svc_ssn_list2);
			   l_impacted_sub_ssn_tmp_iter:=l_impacted_sub_ssn_tmp_iter+1;
			   l_impacted_sub_ssn_tmp.EXTEND;
			   l_impacted_sub_ssn_tmp(l_impacted_sub_ssn_tmp_iter) := O_ST_REPROV_SUB_SVC_SSN_TMP(l_sub_ssn_list1, l_sub_svc_list);
       ---------------------------- Debug Message ----------------------------------                                                                                                    
	   IF l_debug_level > 0 THEN
			   dbms_output.put_line('OUT: '||SUBSTR(l_sub_ssn_list1,1,255));
			   dbms_output.put_line('OUT: '||SUBSTR(l_sub_ssn_list1,256,255));
			   dbms_output.put_line('out svc: '||l_sub_svc_list);
	   END IF;
	   -----------------------------------------------------------------------------
    END IF;
	---------------------------- Debug Message -------------------------------------
	IF l_debug_level > 0 THEN
	 FOR i IN  l_impacted_sub_ssn_tmp.FIRST ..  l_impacted_sub_ssn_tmp.last LOOP
	    dbms_output.put_line('ssn_list:'||l_impacted_sub_ssn_tmp(i).ssn_list||' sub_svc_list:'||
		l_impacted_sub_ssn_tmp(i).sub_svc_list);
        dbms_output.put_line('----------------------------------------------');
      END LOOP;
	END IF;
    ---------------------------------------------------------------------------------

	-- OUTER LOOP - LOOP FOR EACH OF the SSN collections and merge/load only distinct SSN collection into the target SSN return list
    -- INNER LOOP - merge sub_svc of the the merged SSN and load into the target SVC return list
    l_list_seq :=0;
    l_previous_ssn_list := '-1';
	FOR ssn_tmp IN (SELECT * FROM TABLE(CAST(l_impacted_sub_ssn_tmp AS t_ST_REPROV_SUB_SVC_SSN_TMP))
                    ORDER BY ssn_list) LOOP
	IF l_debug_level > 0 THEN
     dbms_output.put_line('merge ssn lists '||ssn_tmp.ssn_list);
	END IF;
     IF l_previous_ssn_list!=ssn_tmp.ssn_list THEN
	    IF l_previous_ssn_list <> '-1' THEN
		  --dbms_output.put_line('DAN '||l_list_seq);
		  --dbms_output.put_line('DAN '||SUBSTR(l_sub_svc_list,1,250));
		  --dbms_output.put_line('DAN '||SUBSTR(l_sub_svc_list,251,250));
	      -- load the target subsvc collection
		  l_csv_list1:=o_cm_csv_list(l_sub_svc_list);
		  l_sub_svc_id:=l_csv_list1.fnc_get_next_element;
		  WHILE l_sub_svc_id IS NOT NULL LOOP
		         l_sub_svc_iterator:=l_sub_svc_iterator+1;
		         p_subsvc_list.EXTEND;
				 -- select ver into l_ver from sub_svc_delivery_plat where sub_svc_id=l_sub_svc_id and rownum=1;
				 l_cpe_subntwk_id:=l_tb_svc_cpe_subntwk(l_sub_svc_id).cpe_subntwk_id;
				 l_ver:=l_tb_svc_cpe_subntwk(l_sub_svc_id).ver;
                 p_subsvc_list(l_sub_svc_iterator):= o_st_reprov_subsvc(l_list_seq, l_sub_svc_id,l_cpe_subntwk_id,l_ver);
                 l_sub_svc_id:=l_csv_list1.fnc_get_next_element;
		  END LOOP;
	    ELSE
		  l_sub_svc_list:=ssn_tmp.sub_svc_list;
		END IF;
		  l_sub_svc_list:=ssn_tmp.sub_svc_list;
	     -- load the target ssn collection
   	      l_list_seq := l_list_seq+1;
          l_previous_ssn_list := ssn_tmp.ssn_list;
		  l_csv_list          := o_cm_csv_list(ssn_tmp.ssn_list);
          l_old_subntwk_id    := l_csv_list.fnc_get_next_element;
          l_ntwk_role_id_list := l_csv_list.fnc_get_next_element;
          WHILE l_old_subntwk_id IS NOT NULL LOOP
              l_subntwk_typ_nm    := fnc_get_subntwk_typ_nm(p_subntwk_id => l_old_subntwk_id);
              l_ssn_list_iterator := l_ssn_list_iterator+1;
              p_ssn_list.EXTEND;
              p_ssn_list(l_ssn_list_iterator) :=
              o_st_reprov_ssn(l_list_seq, l_old_subntwk_id, REPLACE(l_ntwk_role_id_list,'.',','), l_subntwk_typ_nm, NULL);
              l_old_subntwk_id    := l_csv_list.fnc_get_next_element;
              l_ntwk_role_id_list := l_csv_list.fnc_get_next_element;
          END LOOP;
	  ELSE
		   l_sub_svc_list:=l_sub_svc_list||','||ssn_tmp.sub_svc_list;
      END IF;

  END LOOP;
      -- load the target subsvc collection for the last iteration of the OUTER LOOP above
      IF l_sub_svc_list IS NOT NULL THEN
	     l_csv_list:= o_cm_csv_list(l_sub_svc_list);
		 l_sub_svc_id:=l_csv_list.fnc_get_next_element;
		 WHILE l_sub_svc_id IS NOT NULL LOOP
		         l_sub_svc_iterator:=l_sub_svc_iterator+1;
		         p_subsvc_list.EXTEND;
				 -- select ver into l_ver from sub_svc_delivery_plat where sub_svc_id=l_sub_svc_id and rownum=1;
				 l_cpe_subntwk_id:=l_tb_svc_cpe_subntwk(l_sub_svc_id).cpe_subntwk_id;
				 l_ver:=l_tb_svc_cpe_subntwk(l_sub_svc_id).ver;
				 p_subsvc_list(l_sub_svc_iterator):= o_st_reprov_subsvc(l_list_seq, l_sub_svc_id,l_cpe_subntwk_id,l_ver);
                 l_sub_svc_id:=l_csv_list.fnc_get_next_element;
		  END LOOP;
	  END IF;
END;





  FUNCTION fnc_get_cpe_subntwk_id (p_sub_svc_id SUB_SVC.sub_svc_id%TYPE)
  RETURN SUB_SVC_NTWK.subntwk_id%TYPE IS
    l_cpe_subntwk_id SUB_SVC_NTWK.subntwk_id%TYPE;
    CURSOR c_cpe_subntwk IS
      SELECT subntwk_id cpe_subntwk_id
      FROM   SUB_SVC_NTWK Ssn, REF_NTWK_ROLE r
      WHERE  sub_svc_id = p_sub_svc_id
      AND    Ssn.ntwk_role_id = r.ntwk_role_id
      AND    r.ntwk_role_nm = 'cpe';
    r_cpe_subntwk c_cpe_subntwk%ROWTYPE;
  BEGIN
    OPEN c_cpe_subntwk;
    FETCH c_cpe_subntwk INTO r_cpe_subntwk;
    CLOSE c_cpe_subntwk;
    RETURN r_cpe_subntwk.cpe_subntwk_id;
  END;
  PROCEDURE prc_update_ssn
    ( p_subntwk_list IN t_st_reprov_ssn,
      p_unlocked_sub_svc_list IN t_st_reprov_subsvc)
    IS
    -- Purpose: Update the SSN for the given subnetworks (old_subnetwork_ids)
    -- and the given subsvc and the given role to the specified new subnetwork
    -- (new_subntwk_id).
    -- In addition when if any of the CPE subnetworks have links to the old_subnetwork
    -- we need to unattach that link and re-link it to the new_subnetwork_id
    --
    -- MODIFICATION HISTORY
    -- Person        Date        Comments
    -- ---------     ------      ------------------------------------------
    -- Parmi Sahota  2002-08-12  Module creation
    --
    l_subsvc_id NUMBER;
    l_subsvc_iterator      NUMBER;
    l_ssn_list_seq  NUMBER;
    l_cpe_ntwk_role_id   NUMBER;
    l_cpe_parent         SUBNTWK.subntwk_id%TYPE;
    new_subntwk_lookup_tab  tb_number;
    l_list_seq  NUMBER;
    l_ver       NUMBER(12);
    l_sysdate DATE;
    l_sql   varchar2(2000);
  BEGIN
/*debug    if l_debug_level > 0 then
       select sysdate into l_sysdate from dual;
       insert into st_update_ssn_tmp (subntwk_list, created_dtm)
       values (p_subntwk_list, l_sysdate);
       insert into st_update_subsvc_tmp (unlocked_subsvc_list, created_dtm)
       values (p_unlocked_sub_svc_list, l_sysdate);
    end if;
*/
    -- LOOP for each sub_svc_id
    l_subsvc_iterator := p_unlocked_sub_svc_list.FIRST;
    WHILE l_subsvc_iterator IS NOT NULL
    LOOP
    -- delete subnetwork lookup table
      new_subntwk_lookup_tab.DELETE;
      l_list_seq := p_unlocked_sub_svc_list(l_subsvc_iterator).list_seq;
      SELECT ver INTO l_ver FROM SUB_SVC_DELIVERY_PLAT WHERE sub_svc_id = p_unlocked_sub_svc_list(l_subsvc_iterator).subsvc_id;
      IF l_ver = p_unlocked_sub_svc_list(l_subsvc_iterator).ver THEN
        NULL;
      ELSE
         RAISE_APPLICATION_ERROR(-20001, 'SUB_SVC VERSION has changed FROM "'||p_unlocked_sub_svc_list(l_subsvc_iterator).subsvc_id||'" TO "'||
           l_ver||'". Can''t proceed');
      END IF;
      FOR Ssn IN (SELECT * FROM TABLE(CAST(p_subntwk_list AS t_st_reprov_ssn))
                 --WHERE LIST_SEQ = l_list_seq --.  gives error PLS-00810: internal error [22914]. Fixed in oracle 9i
                 )
      LOOP
        -- only continue if this ssn is for the subsvc being processed
        IF Ssn.list_seq = l_list_seq THEN
          IF Ssn.old_subntwk_id != Ssn.new_subntwk_id
          THEN
            -- add entry in new subnetwork lookup table
            new_subntwk_lookup_tab(Ssn.old_subntwk_id) := Ssn.new_subntwk_id;
            -- this subnetwork has changed so update ssn
            begin
            l_sql := 'UPDATE SUB_SVC_NTWK SET    subntwk_id   =  '||Ssn.new_subntwk_id
            ||' WHERE  sub_svc_id  in ('||p_unlocked_sub_svc_list(l_subsvc_iterator).subsvc_id||
            ') AND subntwk_id = '||Ssn.old_subntwk_id||' AND ntwk_role_id IN ('
              ||Ssn.ntwk_role_id_list||')';
            execute immediate l_sql;
            EXCEPTION
              when others then
                 RAISE_APPLICATION_ERROR(-20001, 'problem in sql:'||l_sql);
            end;
            -- re-link to new subntwk if necessary
            -- assuming link will always be TO CPE_subnetwork, and not FROM CPE_subnetwork.
            -- eg from dns server to CPE_subnetwork
            UPDATE LINK
            SET    from_subntwk_id = Ssn.new_subntwk_id
            WHERE  from_subntwk_id = Ssn.old_subntwk_id
            AND    to_subntwk_id   = p_unlocked_sub_svc_list(l_subsvc_iterator).cpe_subntwk_id;
          END IF;
        END IF;
      END LOOP;
      -- update cpe subnetworks to the new subnetwork parent. This is only applicable for
      -- subnetworks with role cpe where the parent is not a cpe subnetwork type.
      l_cpe_parent := fnc_get_parent_subntwk_id(
        p_subntwk_id => p_unlocked_sub_svc_list(l_subsvc_iterator).cpe_subntwk_id);
/*debug
      if l_debug_level > 1 then
        insert into st_reprov_debug values ('checking parent '||l_cpe_parent, sysdate);
      end if;
*/
      IF new_subntwk_lookup_tab.EXISTS(l_cpe_parent)
      THEN -- cpe parent has changed so make a change to network
/*debug if l_debug_level > 1 then
        insert into st_reprov_debug values ('parent has changed from '||new_subntwk_lookup_tab(l_cpe_parent)||
                                            ' to '||p_unlocked_sub_svc_list(l_subsvc_iterator).cpe_subntwk_id, sysdate);
      end if;
*/
        UPDATE SUBNTWK
        SET    parent_subntwk_id = new_subntwk_lookup_tab(l_cpe_parent)
        WHERE  subntwk_id = p_unlocked_sub_svc_list(l_subsvc_iterator).cpe_subntwk_id;
      END IF;
      l_subsvc_iterator := p_unlocked_sub_svc_list.NEXT(l_subsvc_iterator);
    END LOOP;
  END;
  PROCEDURE prc_debug_print_ssn_tmp(p_impacted_ssn_tmp t_st_reprov_ssn_tmp)
  IS
    i NUMBER;
  BEGIN
    i := p_impacted_ssn_tmp.FIRST;
    WHILE i IS NOT NULL LOOP
      dbms_output.put_line('sub_svc_id: '||p_impacted_ssn_tmp(i).sub_svc_id);
      dbms_output.put_line('ssn_list  : '||p_impacted_ssn_tmp(i).ssn_list);
      dbms_output.put_line('cpe_role_subntwk_id: '||p_impacted_ssn_tmp(i).cpe_role_subntwk_id);
      dbms_output.put_line('nep_role_subntwk_id: '||p_impacted_ssn_tmp(i).nep_role_subntwk_id);
      dbms_output.put_line('delete_flag: '||p_impacted_ssn_tmp(i).delete_flag);
      i := p_impacted_ssn_tmp.NEXT(i);
    END LOOP;
  END;
END;
/
