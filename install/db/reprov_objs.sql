--==========================================================================
-- FILE INFO
--    $Id: reprov_objs.sql,v 1.7 2007/09/20 13:08:29 davidx Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_cm_csv_list force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
TYPE o_cm_csv_list
AS OBJECT
-- Purpose: Briefly explain the functionality of the type
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
(
  csv_list   varchar2(32000),
  MEMBER PROCEDURE add_element (p_element varchar2),
  MEMBER FUNCTION fnc_get_num_elements
     RETURN number,
  MEMBER FUNCTION fnc_get_next_element (SELF IN OUT o_cm_csv_list)
     RETURN  varchar2,
  MEMBER PROCEDURE remove_duplicates
)
/

CREATE OR REPLACE
TYPE BODY o_cm_csv_list
IS
-- Purpose: Briefly explain the functionality of the type body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- Enter procedure, function bodies as shown below

MEMBER PROCEDURE add_element (p_element varchar2) IS
  BEGIN
    if csv_list is not null then
      csv_list := csv_list || ',';
    end if;
    csv_list := csv_list || p_element;
  END;

MEMBER PROCEDURE remove_duplicates (SELF IN OUT o_cm_csv_list) IS
  l_num_elements number;
  l_orig  varchar2(32000);
  l_element varchar2(32000);
  l_orig_length number;
  l_new_list o_cm_csv_list;
  i number;
  BEGIN
    l_num_elements := fnc_get_num_elements;
    for i in 1 .. l_num_elements loop
      l_element := fnc_get_next_element; -- gets next element and removes it from list
      if length(replace(','||csv_list||',',
                        ','||l_element||',',
                        ''))                     = length(','||csv_list||',')
      then -- duplicates do not exist so copy to new otherwise skip and continue
        l_new_list.add_element(l_element);
      end if;
    end loop;
    csv_list := l_new_list.csv_list;
  END;


MEMBER FUNCTION fnc_get_num_elements
     RETURN number IS
  BEGIN
    return  length(csv_list) - length(replace(csv_list, ',','')) + 1;
  END;

  MEMBER FUNCTION fnc_get_next_element (SELF IN OUT o_cm_csv_list)
     RETURN  varchar2 IS
     l_element varchar2(32000);
     l_comma_pos number;
  BEGIN
    l_comma_pos := instr(csv_list, ',');
    if l_comma_pos = 0 then
      l_element := csv_list;
      csv_list := '';
    else
      l_element := substr(csv_list, 1, instr(csv_list, ',')-1);
      csv_list := substr(csv_list, instr(csv_list, ',')+1);
      if l_element is null then
        l_element := csv_list;
      end if;
    end if;
    return l_element;
  END;
END;
/


BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_reprov_ssn force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
TYPE o_st_reprov_ssn
AS OBJECT
(
   list_seq   number,
   old_subntwk_id number,
   ntwk_role_id_list varchar2(255),
   subntwk_typ_nm  varchar2(100),
   new_subntwk_id  number
)
/


BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_reprov_ssn_tmp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_reprov_sub_svc_ssn_tmp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

create TYPE O_ST_REPROV_SUB_SVC_SSN_TMP  AS 
    OBJECT ( SSN_LIST VARCHAR2(30000), SUB_SVC_LIST 
    VARCHAR2(30000));
/

create TYPE O_ST_REPROV_SSN_TMP  AS OBJECT ( 
    SUB_ID NUMBER(12, 0), SUB_SVC_ID NUMBER(12, 0), 
    SSN_LIST VARCHAR2(255), CPE_ROLE_SUBNTWK_ID NUMBER(12, 0),
    NEP_ROLE_SUBNTWK_ID NUMBER(12, 0), DELETE_FLAG CHAR(1), 
    VER NUMBER(12, 0), SSN_GROUP_ID NUMBER(2, 0)     );
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_reprov_subsvc force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
TYPE o_st_reprov_subsvc
AS OBJECT
(
   list_seq       number(8),
   subsvc_id      number(12),
   cpe_subntwk_id number(12),
   ver            number(12)
)
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_cm_csv_list force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
type t_cm_csv_list as table of o_cm_csv_list;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_reprov_ssn force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
TYPE t_st_reprov_ssn
 AS TABLE OF o_st_reprov_ssn
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_reprov_ssn_tmp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
TYPE t_st_reprov_ssn_tmp
 AS TABLE OF o_st_reprov_ssn_tmp
/


BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_reprov_sub_svc_ssn_tmp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE TYPE T_ST_REPROV_SUB_SVC_SSN_TMP AS 
    TABLE OF O_ST_REPROV_SUB_SVC_SSN_TMP;
/ 

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_reprov_subsvc force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
TYPE t_st_reprov_subsvc
 AS TABLE OF o_st_reprov_subsvc
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_subntwk_list force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
type t_st_subntwk_list as table of o_cm_csv_list;
/



