CREATE OR REPLACE VIEW V_STATUS ( SUB_ID, STATUS, STATUS_ID)
			AS SELECT DISTINCT s.sub_id, status,status_id
					FROM sub s, ref_status rf, sub_svc sc, svc sv, sub_svc_parm ssp
						WHERE rf.status_id = s.sub_status_id
						AND s.sub_id = sc.sub_id
						AND sc.svc_id = sv.svc_id
						AND ssp.sub_svc_id = sc.sub_svc_id
						AND rf.status IN ('active', 'mso_block', 'courtesy_block')
						AND sc.sub_svc_status_id != 29
						AND sv.svc_nm IN ('video_event', 'video_subscription')
						AND (    parm_id =
									get_parm_id ('end_date',
												100,
												get_svcid ('smp_video_entitlement')
												)
								 AND sc.svc_id IN
									(get_svcid ('video_event'),
										 get_svcid ('video_subscription')
									)
								 AND ssp.val < (SELECT TO_CHAR (SYSDATE, 'yyyyMMddhh24miss')
												  FROM DUAL)
							);