--=============================================================================
--    $Id: sd_plsql.sql,v 1.30 2008/04/22 14:35:43 mattl Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

spool sdm_plsql
----------------------------------------------------------------------
-- SERVICE
-----------------------------------------------------------------------
CREATE OR REPLACE  FUNCTION GET_SVCID  (p_svc_name IN  SVC.SVC_NM%TYPE) return SVC.SVC_ID%TYPE is
    v_svc_id number;
begin
   if length(p_svc_name) <= 30 then

      select sys_context('CTX_SVC', p_svc_name) into v_svc_id from v_dual;

      if v_svc_id is not null then
         return v_svc_id;
      end if;
   end if;

   select svc_id INTO v_svc_id from Svc where svc_nm = p_svc_name ;

   if length(p_svc_name) <= 30 then
     prc_smp_ctx('CTX_SVC', p_svc_name, v_svc_id);
   end if;

   return v_svc_id;

EXCEPTION
  when no_data_found then
    return 0;
END GET_SVCID;
/

SHOW ERRORS;


CREATE OR REPLACE FUNCTION SvcId (i_parm_name IN varchar2) return number is
    gg number;
begin

    select svc_id INTO gg from Svc where svc_nm = i_parm_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
SHOW ERRORS;

CREATE OR REPLACE FUNCTION BaseSvcId (i_parm_name IN varchar2) return number is
    gg number;
begin

    select base_svc_id INTO gg from Svc where svc_nm = i_parm_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
SHOW ERRORS;

CREATE OR REPLACE FUNCTION BaseSvcNm (i_parm_name IN varchar2) return varchar2 is
    gg varchar2(255);
begin

    select 
        svc2.svc_nm INTO gg 
    from 
        Svc svc1, Svc svc2 
    where 
        svc1.svc_nm = i_parm_name AND
        svc1.base_svc_id = svc2.svc_id;

    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
SHOW ERRORS;


CREATE OR REPLACE FUNCTION SvcParmId (i_svc_name IN varchar2, 
                                      i_parm_name IN varchar2) 
    return number is
    gg number;
begin

    select parm_id INTO gg from parm where 
        class_id = Get_Class_Id('SubSvcSpec') AND
        object_id = SvcId (i_svc_name) AND
        parm_nm = i_parm_name ;

    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
SHOW ERRORS;


CREATE OR REPLACE FUNCTION SvcActionId (i_svc_name IN varchar2, 
                                      i_action_name IN varchar2) 
    return number is
    gg number;
begin

    select svc_action_id INTO gg from svc_action where 
        svc_id = SvcId (i_svc_name) AND
        svc_action_nm = i_action_name ;

    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
SHOW ERRORS;

spool off
