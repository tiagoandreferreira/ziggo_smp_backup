--=============================================================================
--    $Id: sd_plsql_synonyms.sql,v 1.1 2001/09/05 01:58:38 germans Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================
set termout off
set heading off
set linesize 133
set pagesize 0
set feedback off
host rm sd_pl_synonyms.sql
spool sd_pl_synonyms.sql
@get_sd_pl_synonyms.sql
spool off
host rm sd_pl_names.sql
spool sd_pl_names.sql
@get_sd_pl_names.sql
spool off
