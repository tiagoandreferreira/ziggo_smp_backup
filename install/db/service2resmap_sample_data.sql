--============================================================================
--    $Id: service2resmap_sample_data.sql,v 1.4 2011/02/28 20:14:42 urmilk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
-- NOTE: CDR_49_sample_data file was renamed to service2resmap_sample_data.sql
--============================================================================
-- REF_REGIONAL_SVC_TYP -----------------------------------------------------------------------------

--insert into REF_REGIONAL_SVC_TYP (REGIONAL_SVC_TYP_ID, REGIONAL_SVC_TYP_DESC, CREATED_DTM, CREATED_BY)
--values (1, 'SUBSCRIPTION SERVICE', sysdate, 'mike');

--insert into REF_REGIONAL_SVC_TYP (REGIONAL_SVC_TYP_ID, REGIONAL_SVC_TYP_DESC, CREATED_DTM, CREATED_BY)
--values (2, 'SUBSCRIPTION WITH EXPIRY', sysdate, 'mike');

-- REGIONAL_SVC -------------------------------------------------------------------------------------

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (1, 'OD10', 1, 'Service Description for Service Code OD10', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (2, 'TMV0', 1, 'Service Description for Service Code TMV0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (3, 'APT0', 1, 'Service Description for Service Code APT0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (4, 'PLB0', 1, 'Service Description for Service Code PLB0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (5, 'ANJ0', 1, 'Service Description for Service Code ANJ0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (6, 'HED0', 1, 'Service Description for Service Code HED0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (7, 'HOD0', 1, 'Service Description for Service Code HOD0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (8, 'FAM', 1, 'Service Description for Service Code FAM', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (9, 'MOV', 1, 'Service Description for Service Code MOV', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (10, 'MVX', 1, 'Service Description for Service Code MVX', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (11, 'SPRS', 1, 'Service Description for Service Code SPRS', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (12, 'DT22', 1, 'Service Description for Service Code DT22', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (13, 'D0T4', 1, 'Service Description for Service Code D0T4', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (14, 'HDCH', 1, 'Service Description for Service Code HDCH', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (15, 'TO30', 1, 'Service Description for Service Code TO30', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (16, 'IP0', 1, 'Service Description for Service Code IP0', '', sysdate, 'mike' );

insert into REGIONAL_SVC (REGIONAL_SVC_ID, REGIONAL_SVC_CD, REGIONAL_SVC_TYP_ID, REGIONAL_SVC_DESC, EXPIRY_DATETIME, CREATED_DTM, CREATED_BY)
values (17, '0690', 1, 'Service Description for Service Code 0690', '', sysdate, 'mike' );

--SVC_RSRC_MAP -------------------------------------------------------------------------------------------------------------------------------

insert into SVC_RSRC_MAP (SVC_RSRC_MAP_ID, SVC_RSRC_MAP_NM, SVC_RSRC_MAP_DESC, SSN_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (1, 'Resource_to_Cas01', 'Resource_to_Cas01 Description', SubntwkId('Cas01'), sysdate, 'mike');

insert into SVC_RSRC_MAP (SVC_RSRC_MAP_ID, SVC_RSRC_MAP_NM, SVC_RSRC_MAP_DESC, SSN_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (2, 'Resource_to_Zone01', 'Resource_to_Zone01 Description', SubntwkId('Zone01'), sysdate, 'mike');

insert into SVC_RSRC_MAP (SVC_RSRC_MAP_ID, SVC_RSRC_MAP_NM, SVC_RSRC_MAP_DESC, SSN_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (3, 'Resource_to_ChLineup01', 'Resource_to_ChLineup01 Description', SubntwkId('ChLineup01'), sysdate, 'mike');

insert into SVC_RSRC_MAP (SVC_RSRC_MAP_ID, SVC_RSRC_MAP_NM, SVC_RSRC_MAP_DESC, SSN_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (4, 'Resource_to_Vod01', 'Resource_to_Vod01 Description', SubntwkId('Vod01'), sysdate, 'mike');

insert into SVC_RSRC_MAP (SVC_RSRC_MAP_ID, SVC_RSRC_MAP_NM, SVC_RSRC_MAP_DESC, SSN_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (5, 'Resource_Vod01_to_ChLineup01', 'Resource_Vod01_to_ChLineup01 Description', SubntwkId('ChLineup01'), sysdate, 'mike');

insert into SVC_RSRC_MAP (SVC_RSRC_MAP_ID, SVC_RSRC_MAP_NM, SVC_RSRC_MAP_DESC, SSN_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (6, 'Resource_to_Cas03', 'Resource_to_Cas03 Description', SubntwkId('Cas03'), sysdate, 'mike');

-- SVC_RSRC_MAP_SDP --------------------------------------------------------------------------------------------------------------------------

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (4, SubntwkId('Vod01'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (5, SubntwkId('Vod01'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'),  sysdate, 'mike');

insert into SVC_RSRC_MAP_SDP (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'),  sysdate, 'mike');

-- REF_CONSUMABLE_RSRC_TYP ----------------------------------------------------------------------------------------

--insert into REF_CONSUMABLE_RSRC_TYP (CONSUMABLE_RSRC_TYP_NM, CREATED_DTM, CREATED_BY)
--values ('Video Resource', sysdate, 'mike');

-- NTWK_RESOURCE 

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1000, '50001' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1001, '50005' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1002, '50101' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1003, '50108' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1004, '50103' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1005, '50109' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1006, '50110' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1007, '50113' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1008, '50114' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1009, '00001' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1010, '00005' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1011, '00012' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1012, '00013' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1013, '00004' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1014, '00002' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1015, '00003' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1016, '00009' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1017, '00236' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1018, '00268' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1019, '00248' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1020, '02900' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1021, '00550' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1022, '00551' || SubntwkId('Cas01'), SubntwkId('Cas01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1023, '41129' || SubntwkId('Vod01'), SubntwkId('Vod01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1024, '49999' || SubntwkId('Vod01'), SubntwkId('Vod01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1025, '41120' || SubntwkId('Vod01'), SubntwkId('Vod01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1026, '41122' || SubntwkId('Vod01'), SubntwkId('Vod01'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1027, '50001' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1028, '50005' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1029, '50101' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1030, '50108' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1031, '50103' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1032, '50109' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1033, '50110' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1034, '50113' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1035, '50114' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1036, '00001' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1037, '00005' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1038, '00012' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1039, '00013' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1040, '00004' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1041, '00002' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1042, '00003' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1043, '00009' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1044, '00236' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1045, '00268' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1046, '00248' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1047, '02900' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1048, '00550' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

insert into NTWK_RESOURCE (RSRC_ID, RSRC_NM, SUBNTWK_ID, CONSUMABLE_RSRC_TYP_NM, RSRC_STATE, CREATED_DTM, CREATED_BY)
values (1049, '00551' || SubntwkId('Cas03'), SubntwkId('Cas03'), 'Video Resource', 'active', sysdate, 'mike');

-- NTWK_RESOURCE_PARM ---------------------------------------------------------------------------------------------------------

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1000, Get_Parm_Id('resource_code', '100'), '50001', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1000, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1000, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1001, Get_Parm_Id('resource_code', '100'), '50005', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1001, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1001, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1002, Get_Parm_Id('resource_code', '100'), '50101', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1002, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1002, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1003, Get_Parm_Id('resource_code', '100'), '50108', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1003, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1003, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1004, Get_Parm_Id('resource_code', '100'), '50103', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1004, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1004, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1005, Get_Parm_Id('resource_code', '100'), '50109', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1005, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1005, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1006, Get_Parm_Id('resource_code', '100'), '50110', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1006, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1006, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1007, Get_Parm_Id('resource_code', '100'), '50113', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1007, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1007, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1008, Get_Parm_Id('resource_code', '100'), '50114', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1008, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1008, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1009, Get_Parm_Id('resource_code', '100'), '00001', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1009, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1009, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1010, Get_Parm_Id('resource_code', '100'), '00005', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1010, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1010, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1011, Get_Parm_Id('resource_code', '100'), '00012', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1011, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1011, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1012, Get_Parm_Id('resource_code', '100'), '00013', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1012, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1012, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1013, Get_Parm_Id('resource_code', '100'), '00004', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1013, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1013, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1014, Get_Parm_Id('resource_code', '100'), '00002', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1014, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1014, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1015, Get_Parm_Id('resource_code', '100'), '00003', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1015, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1015, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1016, Get_Parm_Id('resource_code', '100'), '00009', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1016, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1016, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1017, Get_Parm_Id('resource_code', '100'), '00236', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1017, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1017, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1018, Get_Parm_Id('resource_code', '100'), '00268', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1018, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1018, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1019, Get_Parm_Id('resource_code', '100'), '00248', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1019, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1019, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1020, Get_Parm_Id('resource_code', '100'), '02900', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1020, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1020, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1021, Get_Parm_Id('resource_code', '100'), '00550', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1021, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1021, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1022, Get_Parm_Id('resource_code', '100'), '00551', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1022, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1022, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1023, Get_Parm_Id('resource_code', '100'), '41129', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1023, Get_Parm_Id('has_vod', '100'), 'Y', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1023, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1024, Get_Parm_Id('resource_code', '100'), '49999', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1024, Get_Parm_Id('has_vod', '100'), 'Y', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1024, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1025, Get_Parm_Id('resource_code', '100'), '41120', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1025, Get_Parm_Id('has_vod', '100'), 'Y', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1025, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1026, Get_Parm_Id('resource_code', '100'), '41122', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1026, Get_Parm_Id('has_vod', '100'), 'Y', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1026, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1027, Get_Parm_Id('resource_code', '100'), '50001', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1027, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1027, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1028, Get_Parm_Id('resource_code', '100'), '50005', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1028, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1028, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1029, Get_Parm_Id('resource_code', '100'), '50101', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1029, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1029, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1030, Get_Parm_Id('resource_code', '100'), '50108', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1030, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1030, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1031, Get_Parm_Id('resource_code', '100'), '50103', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1031, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1031, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1032, Get_Parm_Id('resource_code', '100'), '50109', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1032, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1032, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1033, Get_Parm_Id('resource_code', '100'), '50110', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1033, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1033, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1034, Get_Parm_Id('resource_code', '100'), '50113', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1034, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1034, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1035, Get_Parm_Id('resource_code', '100'), '50114', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1035, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1035, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1036, Get_Parm_Id('resource_code', '100'), '00001', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1036, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1036, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1037, Get_Parm_Id('resource_code', '100'), '00005', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1037, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1037, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1038, Get_Parm_Id('resource_code', '100'), '00012', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1038, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1038, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1039, Get_Parm_Id('resource_code', '100'), '00013', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1039, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1039, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1040, Get_Parm_Id('resource_code', '100'), '00004', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1040, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1040, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1041, Get_Parm_Id('resource_code', '100'), '00002', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1041, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1041, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1042, Get_Parm_Id('resource_code', '100'), '00003', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1042, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1042, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1043, Get_Parm_Id('resource_code', '100'), '00009', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1043, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1043, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1044, Get_Parm_Id('resource_code', '100'), '00236', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1044, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1044, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1045, Get_Parm_Id('resource_code', '100'), '00268', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1045, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1045, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1046, Get_Parm_Id('resource_code', '100'), '00248', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1046, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1046, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1047, Get_Parm_Id('resource_code', '100'), '02900', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1047, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1047, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1048, Get_Parm_Id('resource_code', '100'), '00550', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1048, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1048, Get_Parm_Id('resource_type', '100'), 'Package', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1049, Get_Parm_Id('resource_code', '100'), '00551', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1049, Get_Parm_Id('has_vod', '100'), 'N', sysdate, 'mike');

insert into NTWK_RESOURCE_PARM (RSRC_ID, PARM_ID, VAL, CREATED_DTM, CREATED_BY)
values (1049, Get_Parm_Id('resource_type', '100'), 'Service', sysdate, 'mike');


-- SVC_RSRC_MAP_DTL ---------------------------------------------------------------------------------------------------------------------

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 1, 1000, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 2, 1001, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 3, 1002, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 3, 1003, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 4, 1004, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 4, 1005, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 5, 1006, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 6, 1007, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (1, SubntwkId('Cas01'), 7, 1008, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 8, 1010, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 8, 1009, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 8, 1011, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 9, 1010, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 9, 1009, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 9, 1012, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 10, 1010, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 10, 1009, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 10, 1013, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 11, 1010, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 11, 1014, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 11, 1015, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas01'), 11, 1016, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 12, 1017, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 12, 1018, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 12, 1019, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 13, 1018, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 13, 1019, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 13, 1020, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 14, 1021, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas01'), 14, 1022, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (4, SubntwkId('Vod01'), 15, 1023, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (4, SubntwkId('Vod01'), 16, 1024, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (4, SubntwkId('Vod01'), 17, 1025, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (5, SubntwkId('Vod01'), 12, 1026, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 1, 1027, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 2, 1028, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 3, 1029, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 3, 1030, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 4, 1031, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 4, 1032, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 5, 1033, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 6, 1034, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (6, SubntwkId('Cas03'), 7, 1035, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 8, 1037, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 8, 1036, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 8, 1038, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 9, 1037, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 9, 1036, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 9, 1039, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 10, 1037, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 10, 1036, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 10, 1040, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 11, 1037, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 11, 1041, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 11, 1042, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (2, SubntwkId('Cas03'), 11, 1043, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 12, 1044, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 12, 1045, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 12, 1046, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 13, 1045, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 13, 1046, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 13, 1047, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 14, 1048, sysdate, 'mike');

insert into SVC_RSRC_MAP_DTL (SVC_RSRC_MAP_ID, SDP_SUBNTWK_ID, REGIONAL_SVC_ID, RSRC_ID, CREATED_DTM, CREATED_BY)
values (3, SubntwkId('Cas03'), 14, 1049, sysdate, 'mike');

-- REGIONAL_EVENT ---------------------------------------------------------------------------------------------------

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG, SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFA', '20000', SubntwkId('Cas01'), '10000', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFB', '20001', SubntwkId('Cas01'), '10001', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFC', '20002', SubntwkId('Cas01'), '10002', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFD', '20003', SubntwkId('Cas01'), '10003', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG, SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFA', '20000', SubntwkId('Cas02'), '10000', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFB', '20001', SubntwkId('Cas02'), '10001', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFC', '20002', SubntwkId('Cas02'), '10002', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFD', '20003', SubntwkId('Cas02'), '10003', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG, SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFA', '20000', SubntwkId('Cas03'), '10000', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFB', '20001', SubntwkId('Cas03'), '10001', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFC', '20002', SubntwkId('Cas03'), '10002', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFD', '20003', SubntwkId('Cas03'), '10003', sysdate + 20, sysdate + 21, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG, SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFA', '20000', SubntwkId('Cas04'), '10000', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFB', '20001', SubntwkId('Cas04'), '10001', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFC', '20002', SubntwkId('Cas04'), '10002', sysdate + 21, sysdate + 22, sysdate, 'mike');

insert into REGIONAL_EVENT (REGIONAL_EVENT_CD, RESOURCE_TAG,  SDP_SUBNTWK_ID, SERVICE_HANDLE, EVENT_START_TIME, EVENT_END_TIME, CREATED_DTM, CREATED_BY)
values ('18QFD', '20003', SubntwkId('Cas04'), '10003', sysdate + 21, sysdate + 22, sysdate, 'mike');


DROP SEQUENCE REGIONAL_SVC_SEQ;
DROP SEQUENCE SVC_RSRC_SEQ;


CREATE SEQUENCE REGIONAL_SVC_SEQ
  START WITH 1000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;

CREATE SEQUENCE SVC_RSRC_SEQ
  START WITH 10000
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;
  
  
  
