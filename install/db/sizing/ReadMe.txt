#!/bin/ksh
#==============================================================================
#  $Id: ReadMe.txt,v 1.1.2.3 2011/03/02 21:29:26 vasyly Exp $
#==============================================================================

Introduction:
------------
This utility is to estimate a sizing for production databases.

It can be run for development, QA, UAT and production deployments without any modifications.

Prepare database
----------------
1) run . ./setEnv.sh under $DOMAIN_HOME/util
2) Run install_sizing_pkg.sh to install package and create tables. This step 
   will create tables required for sizing calculations and also load initial 
   values for business parameters. 
   
Calculate sizing
----------------
1)  run . ./setEnv.sh under $DOMAIN_HOME/util if didn't run it before
2)  The first step must be gathering statistics :
         ./gather_stats.sh <cfg_name>
	cfg_name is a sizing configuration name and it matches configuration name for business 
	parameters. If you are planning to use parameters loaded during installation than you 
	should use SMP_DEMO_HSD_VOICE as a cfg_name.
3)  If you want to reavaluate business parameters you execute:
			./get_parameters.sh <new_cfg_name>
4)  run 
			gen_report.sh <cfg_name>
	to produce sizing report or you can run your own queries against 
	sizing_objects table

5)  Review generated sizing report which will be availabe under reports diretory
