#!/bin/ksh
#==============================================================================
#  $Id: gather_stats.sh,v 1.1.2.4 2011/05/12 20:20:25 vasyly Exp $
#==============================================================================
sqlplus -S -L ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB} <<ENDSQL
    @gather_stats.sql $1 
    exit
ENDSQL
