--=============================================================================
--    $Id: gather_stats.sql,v 1.1.2.2 2011/05/12 20:20:24 vasyly Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

exec DBMS_STATS.GATHER_SCHEMA_STATS (ownname => user,estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE , no_invalidate => true);
exec dbms_stats.export_schema_stats(user,'SIGMA_STATS',upper('&1'));

