#!/bin/ksh
#==============================================================================
#  $Id: gen_report.sh,v 1.1.2.3 2011/05/12 20:20:25 vasyly Exp $
#==============================================================================
sqlplus -S -L ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}  <<ENDSQL
    @smp_objects_report.sql $1
    exit
ENDSQL
