--=============================================================================
--    $Id: get_bp.sql,v 1.1.2.3 2011/05/12 20:20:24 vasyly Exp $
--
--  DESCRIPTION
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================

DECLARE
    SUB_COUNT                     NUMBER;
    PARMS_PER_SUB                 NUMBER;
    ADDR_PER_SUB                  NUMBER;
    PARMS_PER_LOCATION            NUMBER;
    OIEC_PER_ORDR                 NUMBER;
    LAUNCH_40_DATE                DATE;
    LID_PER_ORDR                  NUMBER;
    ORDR_COUNT                    NUMBER;
    SVCS_PER_SUB                  NUMBER;
    PARMS_PER_ORDR_ITEM           NUMBER;
    PARMS_PER_ORDR                NUMBER;
    ADDR_PER_SUBSVC               NUMBER;
    DEPY_PER_SUB                  NUMBER;
    SSD_PER_SUB                   NUMBER;
    SSA_PER_SUB                   NUMBER;
    SSP_PER_SVC                   NUMBER;
    SSPD_PER_SUB                  NUMBER;
    SSN_PER_SUB                   NUMBER;
    SSDP_COUNT                    NUMBER;
    SS_COUNT                      NUMBER;
    SSPD_PERCENT                  NUMBER;
    INSTANCE_PER_ORDR             NUMBER;
    INIT_ORDR_COUNT               NUMBER;
    PARM_PER_SUBNTWK              NUMBER;
    NON_CPE_LINK                  NUMBER;
    PARMS_PER_LINK                NUMBER;
    NON_CPE_SUBNTWK               NUMBER;
    INST_ORD_COUNT                NUMBER;
    ITEMS_PER_ORDER               NUMBER;
    CONTACTS_PER_SUB              NUMBER;
    PARMS_PER_CONTACT             NUMBER;
    PARMS_PER_JEA_TASK            NUMBER;
    ORDERS_WITH_TASKS             NUMBER;
    ORDERS_WITH_ADD_DELETE        NUMBER;
    L_CFG_NAME                    VARCHAR2 (30) := upper('&1');
    L_CFG_DATE                    DATE := SYSDATE;
BEGIN
    SELECT MIN (CREATED_DTM)
      INTO LAUNCH_40_DATE
      FROM INSTANCE;

    DELETE FROM TMP_SUB_LIST;

    COMMIT;

    INSERT INTO TMP_SUB_LIST
        SELECT SUB_ID
          FROM SUB;


    COMMIT;

    --    WHERE sub_id IN (SELECT sub_id
    --                       FROM sub_svc
    --                      WHERE svc_id IN (SELECT svc_id
    --                                         FROM svc
    --                                        WHERE svc_nm = 'internet_access'))
    --      AND sub_id NOT IN (
    --             SELECT sub_id
    --               FROM sub_svc
    --              WHERE svc_id IN (
    --                       SELECT svc_id
    --                         FROM svc
    --                        WHERE svc_nm IN
    --                                 ('smp_switch_dial_tone_access',
    --                                  'moto_prop_stb',
    --                                  'cisco_prop_stb'
    --                                 )));
    DELETE FROM TMP_ORDR_LIST;

    COMMIT;

    INSERT INTO TMP_ORDR_LIST
        SELECT SUB_ORDR_ID
          FROM SUB_ORDR SO
              ,TMP_SUB_LIST T
         WHERE SO.CREATED_DTM > LAUNCH_40_DATE
           AND SO.SUB_ID = T.SUB_ID;


    COMMIT;

--Initial number of Subscribers
    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP1'
                ,'CM'
                ,'Initial number of subscribers'
                ,1
                );

    COMMIT;

--new subs per month
    SELECT COUNT (1)
      INTO SUB_COUNT
      FROM SUB
     WHERE SUB_ID IN (SELECT SUB_ID
                        FROM TMP_SUB_LIST);

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP2'
                ,'CM'
                ,'new subs per month'
                , SUB_COUNT / 24
                );
    COMMIT;

--sub purge interval
    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP3'
                ,'CM'
                ,'sub purge interval'
                ,24
                );
    COMMIT;

--Parameters per sub
    SELECT   AVG (COUNT (1))
        INTO PARMS_PER_SUB
        FROM SUB_PARM S
            ,TMP_SUB_LIST T
       WHERE S.SUB_ID = T.SUB_ID
    GROUP BY S.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP4'
                ,'CM'
                ,'Parameters per sub'
                ,NVL (PARMS_PER_SUB, 0)
                );
    COMMIT;

--contacts per sub
    SELECT   AVG (COUNT (1))
        INTO CONTACTS_PER_SUB
        FROM SUB_CONTACT SC
            ,TMP_SUB_LIST T
       WHERE T.SUB_ID = SC.SUB_ID
    GROUP BY SC.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP5'
                ,'CM'
                ,'contacts per sub'
                ,NVL (CONTACTS_PER_SUB, 0)
                );
    COMMIT;

--parms per contact
    SELECT   AVG (COUNT (1))
        INTO PARMS_PER_CONTACT
        FROM SUB_CONTACT_PARM P
            ,SUB_CONTACT C
            ,TMP_SUB_LIST T
       WHERE T.SUB_ID = C.SUB_ID
         AND C.SUB_CONTACT_ID = P.SUB_CONTACT_ID
    GROUP BY C.SUB_CONTACT_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP6'
                ,'CM'
                ,'parms per contact'
                ,NVL (PARMS_PER_CONTACT, 0)
                );
    COMMIT;

--addresses per sub
    SELECT   AVG (COUNT (1))
        INTO ADDR_PER_SUB
        FROM SUB_ADDR S
            ,TMP_SUB_LIST T
       WHERE T.SUB_ID = S.SUB_ID
    GROUP BY S.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP7'
                ,'CM'
                ,'addresses per sub'
                ,NVL (ADDR_PER_SUB, 0)
                );
    COMMIT;

--components per address
    SELECT   AVG (COUNT (1))
        INTO PARMS_PER_LOCATION
        FROM LOCATION_DTL LD
            ,SUB_ADDR S
            ,TMP_SUB_LIST T
       WHERE T.SUB_ID = S.SUB_ID
         AND S.LOCATION_ID = LD.LOCATION_ID
    GROUP BY S.LOCATION_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP8'
                ,'CM'
                ,'components per address'
                ,NVL (PARMS_PER_LOCATION, 0)
                );
    COMMIT;

--Order items related to entity change per order
    SELECT   AVG (COUNT (1))
        INTO OIEC_PER_ORDR
        FROM ORDR_ITEM_ENTITY_CHG O
            ,SUB_ORDR_ITEM I
            ,TMP_ORDR_LIST T
       WHERE I.SUB_ORDR_ITEM_ID = O.SUB_ORDR_ITEM_ID
         AND I.CREATED_DTM >= LAUNCH_40_DATE
         AND I.SUB_ORDR_ID = T.SUB_ORDR_ID
    GROUP BY I.SUB_ORDR_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP9'
                ,'CM'
                ,'order items related to entity change per order'
                ,NVL (OIEC_PER_ORDR, 0)
                );
    COMMIT;

--Number of line items per order
    SELECT   AVG (COUNT (1))
        INTO LID_PER_ORDR
        FROM LINE_ITEM_DEPY L
            ,TMP_ORDR_LIST T
       WHERE L.SUB_ORDR_ID = T.SUB_ORDR_ID
    GROUP BY L.SUB_ORDR_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP10'
                ,'CM'
                ,'Number of line items per order'
                ,NVL (LID_PER_ORDR, 0)
                );
    COMMIT;

--Initial Load of Orders
    SELECT COUNT (1)
      INTO INIT_ORDR_COUNT
      FROM SUB_ORDR SO
          ,TMP_SUB_LIST T
     WHERE SO.CREATED_DTM < LAUNCH_40_DATE
       AND SO.SUB_ID = T.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP11'
                ,'CM'
                ,'Initial Load of Orders'
                ,INIT_ORDR_COUNT
                );
    COMMIT;

--orders per month
    SELECT COUNT (1)
      INTO ORDR_COUNT
      FROM TMP_ORDR_LIST;

    IF ORDR_COUNT = 0
    THEN
        ORDR_COUNT := 1;
    END IF;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP12'
                ,'CM'
                ,'orders per month'
                , ORDR_COUNT / 12
                );
    COMMIT;

--SMP Orders purge interval
    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP13'
                ,'CM'
                ,'SMP Orders purge interval'
                ,12
                );
    COMMIT;

--Parameters per Order Item
    SELECT   AVG (COUNT (1))
        INTO PARMS_PER_ORDR_ITEM
        FROM SUB_ORDR_ITEM SOI
            ,TMP_ORDR_LIST T
       WHERE SOI.SUB_ORDR_ID = T.SUB_ORDR_ID
    GROUP BY SUB_ORDR_ITEM_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP14'
                ,'CM'
                ,'Parameters per Order Item'
                ,NVL (PARMS_PER_ORDR_ITEM, 0)
                );
    COMMIT;

--Parameters per Order
    SELECT   AVG (COUNT (1))
        INTO PARMS_PER_ORDR
        FROM SUB_ORDR_PARM P
            ,TMP_ORDR_LIST T
       WHERE P.SUB_ORDR_ID = T.SUB_ORDR_ID
    GROUP BY P.SUB_ORDR_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP15'
                ,'CM'
                ,'Parameters per Order'
                ,NVL (PARMS_PER_ORDR, 0)
                );
    COMMIT;

--Services per voice Sub service
    SELECT   AVG (COUNT (1))
        INTO SVCS_PER_SUB
        FROM SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP16'
                ,'CM'
                ,'Services per Sub'
                ,NVL (SVCS_PER_SUB, 0)
                );
    COMMIT;

--Addresses per voice Sub Service
    SELECT   AVG (COUNT (1))
        INTO ADDR_PER_SUBSVC
        FROM SUB_SVC_ADDR SSA
            ,SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SS.SUB_SVC_ID = SSA.SUB_SVC_ID
         AND SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_SVC_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP17'
                ,'CM'
                ,'Addresses per Sub Service'
                ,NVL (ADDR_PER_SUBSVC, 0)
                );
    COMMIT;

--sub_svc_depy
    SELECT   AVG (COUNT (1))
        INTO SSD_PER_SUB
        FROM SUB_SVC_DEPY SSD
            ,SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SS.SUB_SVC_ID = SSD.DEPENDENT_SUB_SVC_ID
         AND SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP18'
                ,'CM'
                ,'Services dependent on other services: sub_svc_depy'
                ,NVL (SSD_PER_SUB, 0)
                );
    COMMIT;

--Parameters per provisionable sub service
    SELECT   AVG (COUNT (1))
        INTO SSP_PER_SVC
        FROM SUB_SVC_PARM SSP
            ,SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SSP.SUB_SVC_ID = SS.SUB_SVC_ID
         AND SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_SVC_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP19'
                ,'CM'
                ,'Parameters per provisionable sub service'
                ,NVL (SSP_PER_SVC, 0)
                );
    COMMIT;

--sub_svc_parm_depy
    SELECT   AVG (COUNT (1))
        INTO SSPD_PER_SUB
        FROM SUB_SVC_PARM_DEPY SSPD
            ,SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SS.SUB_SVC_ID = SSPD.DEPY_SUB_SVC_ID
         AND SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP20'
                ,'CM'
                ,'Service parameters dependent on other parameters: sub_svc_parm_depy'
                ,NVL (SSPD_PER_SUB, 0)
                );
    COMMIT;

--sub_svc_ntwk
    SELECT   AVG (COUNT (1))
        INTO SSN_PER_SUB
        FROM SUB_SVC_NTWK SSN
            ,SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SS.SUB_SVC_ID = SSN.SUB_SVC_ID
         AND SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP21'
                ,'CM'
                ,'Sub service networks per sub: sub_svc_ntwk table'
                ,NVL (SSN_PER_SUB, 0)
                );
    COMMIT;

    SELECT COUNT (DISTINCT S.SUB_ORDR_ID)
      INTO ORDERS_WITH_ADD_DELETE
      FROM SUB_ORDR_ITEM S
          ,REF_ORDR_ACTION R
          ,TMP_ORDR_LIST T
     WHERE R.ORDR_ACTION_ID = S.ORDR_ACTION_ID
       AND ORDR_ACTION_NM IN ('add', 'delete')
       AND T.SUB_ORDR_ID = S.SUB_ORDR_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP22'
                ,'CM'
                ,'percentage of orders to contain additions or deletions'
                , ORDERS_WITH_ADD_DELETE / ORDR_COUNT
                );
    COMMIT;

    SELECT   AVG (COUNT (1))
        INTO SSA_PER_SUB
        FROM SUB_SVC_ASSOC SSA
            ,SUB_SVC SS
            ,TMP_SUB_LIST T
       WHERE SS.SUB_SVC_ID = SSA.ASSOC_SUB_SVC_ID
         AND SS.SUB_ID = T.SUB_ID
    GROUP BY SS.SUB_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP34'
                ,'CM'
                ,'Number of service associations per sub: sub_svc_assoc'
                ,NVL (SSA_PER_SUB, 0)
                );
    COMMIT;

-- order items per order
    SELECT   AVG (COUNT (1))
        INTO ITEMS_PER_ORDER
        FROM SUB_ORDR_ITEM S
            ,TMP_ORDR_LIST T
       WHERE T.SUB_ORDR_ID = S.SUB_ORDR_ID
    GROUP BY S.SUB_ORDR_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP35'
                ,'CM'
                ,'order items per order'
                ,NVL (ITEMS_PER_ORDER, 0)
                );
    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP23'
                ,'STM'
                ,'cpes per subscriber'
                ,1
                );
    COMMIT;

--subnwk_parm
    SELECT   AVG (COUNT (1))
        INTO PARM_PER_SUBNTWK
        FROM SUBNTWK_PARM
    GROUP BY SUBNTWK_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP24'
                ,'STM'
                ,'average number of parameters per subnetwork'
                ,NVL (PARM_PER_SUBNTWK, 0)
                );
    COMMIT;

    SELECT COUNT (1)
      INTO NON_CPE_LINK
      FROM LINK L
          ,SUBNTWK S_TO
          ,SUBNTWK S_FROM
          ,REF_SUBNTWK_TYP R_TO
          ,REF_SUBNTWK_TYP R_FROM
     WHERE L.FROM_SUBNTWK_ID = S_FROM.SUBNTWK_ID
       AND L.TO_SUBNTWK_ID = S_TO.SUBNTWK_ID
       AND S_TO.SUBNTWK_TYP_ID = R_TO.SUBNTWK_TYP_ID
       AND S_FROM.SUBNTWK_TYP_ID = R_FROM.SUBNTWK_TYP_ID
       AND R_TO.SUBNTWK_TYP_NM NOT IN ('mta', 'cpe');

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP25'
                ,'STM'
                ,'non cpe links'
                ,NON_CPE_LINK
                );
    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP26'
                ,'STM'
                ,'links per cpe'
                ,1
                );
    COMMIT;

    SELECT   AVG (COUNT (1))
        INTO PARMS_PER_LINK
        FROM LINK_PARM
    GROUP BY LINK_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP27'
                ,'STM'
                ,'Average number of parameters per link'
                ,NVL (PARMS_PER_LINK, 0)
                );
    COMMIT;

    SELECT COUNT (1)
      INTO NON_CPE_SUBNTWK
      FROM SUBNTWK
     WHERE SUBNTWK_TYP_ID NOT IN (SELECT SUBNTWK_TYP_ID
                                    FROM REF_SUBNTWK_TYP
                                   WHERE SUBNTWK_TYP_NM IN ('cm', 'mta'));

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CREATED_DTM
                ,PARM_NM
                ,CFG_TYPE
                ,PARM_COMMENT
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,L_CFG_DATE
                ,'BP33'
                ,'stm'
                ,'Number of non cpe subntwks'
                ,NON_CPE_SUBNTWK
                );
    COMMIT;
END;
/


