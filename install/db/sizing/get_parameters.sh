#!/bin/ksh
#==============================================================================
#  $Id: get_parameters.sh,v 1.1.2.3 2011/05/12 20:20:25 vasyly Exp $
#==============================================================================
set -x

sqlplus "${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}" <<ENDSQL
    @get_bp.sql $1
    @jwf_calc_bp.sql $1
    exit;
ENDSQL

exit

if [ $? -eq 0 ]
then
sqlplus -S -L ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB} <<EOF

SET PAGESIZE 1000
SET FEEDBACK OFF
SET HEADING OFF
SET SPOOL data/$1.csv
select S.CFG_NAME || ',' || S.PARM_NM || ',' || S.CFG_TYPE || ',' || S.VALUE || ',' || S.PARM_COMMENT 
from SIZING_PARMS s 
where S.CFG_NAME = upper('$1') order by 3;
exit;
EOF

else
echo "Could not calculate business parameters"
fi
