#!/bin/ksh
#==============================================================================
#  $Id: install_sizing_pkg.sh,v 1.1.2.4 2011/05/12 20:20:24 vasyly Exp $
#==============================================================================
sqlplus -S -L ${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB} <<ENDSQL
    @create_tables.sql
    @platform_sizing_pkg.sql
    show errors

    exit
ENDSQL

mkdir -p logs

sqlldr \
	userid=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB} \
	control=sizing.ctl \
	data=data/hsd_voice.csv \
	bad=logs/hsd_voice.csv.bad \
	log=logs/hsd_voice.csv.log \
	discard=logs/hsd_voice.csv.discard

