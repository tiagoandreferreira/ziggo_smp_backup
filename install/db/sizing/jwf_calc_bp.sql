--#==============================================================================
--#  $Id: jwf_calc_bp.sql,v 1.1.2.2 2011/05/12 20:20:25 vasyly Exp $
--#==============================================================================

DECLARE
L_N_SMP_REQUESTS              NUMBER;
L_N_OTHER_REQUESTS            NUMBER;
L_N_SMP_LI                    NUMBER;
L_N_HEADER_PARMS              NUMBER;
L_NON_ROOT_INSTANCES          NUMBER;
L_N_DEP_LINKS                 NUMBER;
L_N_JEA_TASKS                 NUMBER;
L_N_ASYNCH_JEA_TASKS          NUMBER;
L_N_JEA_FAILED_TASKS          NUMBER;
L_FAILURE_PATH_JEAS           NUMBER;
L_SUB_PROC_DEPTH              NUMBER;
L_FAILURE_LENGTH              NUMBER;
L_CFG_NAME                    SIZING_PARMS.CFG_NAME%TYPE := upper('&1');
L_CFG_DATE                    DATE := SYSDATE;
BEGIN
INSERT INTO SIZING_PARMS
	(CFG_NAME
	,CFG_TYPE
	,CREATED_DTM
	,PARM_COMMENT
	,PARM_NM
	,VALUE
	)
 VALUES (L_CFG_NAME
	,PLATFORM_SIZING.C_WFE_CFG_TYPE
	,L_CFG_DATE
	,'No comment'
	,PLATFORM_SIZING.C_WFE_RETENTION_PERIOD
	,1
	);

COMMIT;

INSERT INTO SIZING_PARMS
	(CFG_NAME
	,CFG_TYPE
	,CREATED_DTM
	,PARM_COMMENT
	,PARM_NM
	,VALUE
	)
SELECT L_CFG_NAME
      ,PLATFORM_SIZING.C_WFE_CFG_TYPE
      ,L_CFG_DATE
      ,'No comment'
      ,PLATFORM_SIZING.C_WFE_UNIT_OF_MESURE
      , MAX (I.MODIFIED_DTM) - MIN (I.CREATED_DTM)
  FROM INSTANCE I;

SELECT DECODE(NVL(COUNT (1),1),0,1,NVL(COUNT (1),1))
INTO L_N_SMP_REQUESTS
FROM INSTANCE I
WHERE I.INSTANCE_ID = I.ROOT_INSTANCE_ID
AND I.SMP_ORDR_ID IS NOT NULL;

SELECT DECODE(NVL(COUNT (1),1),0,1,NVL(COUNT (1),1))
INTO L_N_OTHER_REQUESTS
FROM INSTANCE I
WHERE I.INSTANCE_ID = I.ROOT_INSTANCE_ID
AND I.SMP_ORDR_ID IS NULL;

INSERT INTO SIZING_PARMS
	(CFG_NAME
	,CFG_TYPE
	,CREATED_DTM
	,PARM_COMMENT
	,PARM_NM
	,VALUE
	)
 VALUES (L_CFG_NAME
	,PLATFORM_SIZING.C_WFE_CFG_TYPE
	,L_CFG_DATE
	,'No comment'
	,PLATFORM_SIZING.C_WFE_N_SMP_REC
	,L_N_SMP_REQUESTS
	);

COMMIT;

INSERT INTO SIZING_PARMS
	(CFG_NAME
	,CFG_TYPE
	,CREATED_DTM
	,PARM_COMMENT
	,PARM_NM
	,VALUE
	)
 VALUES (L_CFG_NAME
	,PLATFORM_SIZING.C_WFE_CFG_TYPE
	,L_CFG_DATE
	,'No comment'
	,PLATFORM_SIZING.C_WFE_N_OTHER_REC
	,L_N_OTHER_REQUESTS
	);

COMMIT;

SELECT DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
      INTO L_N_SMP_LI
      FROM CONTROL C
          ,CONTROL_ATTR A
          ,REF_ATTR RA
     WHERE RA.ATTR_NM = 'groupID'
       AND RA.ATTR_ID = A.ATTR_ID
       AND C.CONTROL_ID = A.CONTROL_ID
       AND C.ROOT_INSTANCE_ID = A.ROOT_INSTANCE_ID
       AND C.INSTANCE_ID = C.ROOT_INSTANCE_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_LI_REQ
                , L_N_SMP_LI / L_N_SMP_REQUESTS
                );

    COMMIT;

    SELECT DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
      INTO L_N_HEADER_PARMS
      FROM VARIABLE V
          ,CONTROL C
     WHERE C.INSTANCE_ID = C.ROOT_INSTANCE_ID
       AND C.ROOT_INSTANCE_ID = V.ROOT_INSTANCE_ID
       AND C.CONTROL_ID = V.CONTROL_ID
       AND V.CHUNK_NUM = 0;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_HEADER_PARMS
                , L_N_HEADER_PARMS / (L_N_SMP_REQUESTS + L_N_OTHER_REQUESTS)
                );

    COMMIT;

    SELECT DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
      INTO L_NON_ROOT_INSTANCES
      FROM INSTANCE I
     WHERE I.INSTANCE_ID <> I.ROOT_INSTANCE_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_SUB_PROC_PER_REQ
                , L_NON_ROOT_INSTANCES / (L_N_SMP_REQUESTS + L_N_OTHER_REQUESTS)
                );

    COMMIT;

    SELECT DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
      INTO L_N_DEP_LINKS
      FROM DEPENDENCY_LINK DL;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_DEP_LINKS
                , L_N_DEP_LINKS / (L_N_SMP_REQUESTS + L_N_OTHER_REQUESTS)
                );

    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_IN_PARM_PER_PROC
                ,NVL (AVG (COUNT (1)), 0)
            FROM CONTROL C
                ,INPUT_PARMS I
           WHERE C.CONTROL_ID = C.INSTANCE_ID
             AND C.INSTANCE_ID <> C.ROOT_INSTANCE_ID
             AND C.ROOT_INSTANCE_ID = I.ROOT_INSTANCE_ID
             AND C.CONTROL_ID = I.CONTROL_ID
             AND I.CHUNK_NUM = 0
        GROUP BY C.ROOT_INSTANCE_ID
                ,C.CONTROL_ID;

    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_PARM_PER_PROC
                ,NVL (AVG (COUNT (1)), 0)
            FROM CONTROL C
                ,VARIABLE I
           WHERE C.CONTROL_ID = C.INSTANCE_ID
             AND C.INSTANCE_ID <> C.ROOT_INSTANCE_ID
             AND C.ROOT_INSTANCE_ID = I.ROOT_INSTANCE_ID
             AND C.CONTROL_ID = I.CONTROL_ID
             AND I.CHUNK_NUM = 0
        GROUP BY C.ROOT_INSTANCE_ID
                ,C.CONTROL_ID;

    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_VAR_SUB_PROCESS
                ,NVL (AVG (COUNT (1)), 0)
            FROM VARIABLE V
                ,CONTROL C
           WHERE C.INSTANCE_ID <> C.ROOT_INSTANCE_ID
             AND C.CONTROL_ID <> C.INSTANCE_ID
             AND C.ROOT_INSTANCE_ID = V.ROOT_INSTANCE_ID
             AND C.CONTROL_ID = V.CONTROL_ID
             AND V.CHUNK_NUM = 0
        GROUP BY C.ROOT_INSTANCE_ID
                ,C.INSTANCE_ID;

    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_IN_PARM_PER_JEA_TASK
                ,NVL (AVG (COUNT (1)), 0)
            FROM INPUT_PARMS I
                ,REF_PARM RP
                ,INPUT_PARMS I1
           WHERE RP.PARM_NM = 'taskTypeNm'
             AND RP.PARM_ID = I.PARM_ID
             AND I1.CONTROL_ID = I.CONTROL_ID
             AND I1.ROOT_INSTANCE_ID = I.ROOT_INSTANCE_ID
             AND I1.CHUNK_NUM = 0
             AND NOT EXISTS (SELECT 1
                               FROM MANUAL_TASK M
                              WHERE I.CONTROL_ID = M.CONTROL_ID)
        GROUP BY I.ROOT_INSTANCE_ID
                ,I.CONTROL_ID;

    COMMIT;

    SELECT DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
      INTO L_N_JEA_TASKS
      FROM INPUT_PARMS I
          ,REF_PARM RP
          ,CONTROL C
     WHERE RP.PARM_NM = 'taskTypeNm'
       AND RP.PARM_ID = I.PARM_ID
       AND C.CONTROL_ID = I.CONTROL_ID
       AND C.ROOT_INSTANCE_ID = I.ROOT_INSTANCE_ID
       AND NOT EXISTS (SELECT 1
                         FROM MANUAL_TASK M
                        WHERE I.CONTROL_ID = M.CONTROL_ID);

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_OUT_PARM_PER_JEA_TASK
                ,COUNT (1)/L_N_JEA_TASKS
            FROM INPUT_PARMS I
                ,REF_PARM RP
                ,OUTPUT_PARMS I1
           WHERE RP.PARM_NM = 'taskTypeNm'
             AND RP.PARM_ID = I.PARM_ID
             AND I1.CONTROL_ID = I.CONTROL_ID
             AND I1.ROOT_INSTANCE_ID = I.ROOT_INSTANCE_ID
             AND I1.CHUNK_NUM = 0
             AND NOT EXISTS (SELECT 1
                               FROM MANUAL_TASK M
                              WHERE I.CONTROL_ID = M.CONTROL_ID);

    COMMIT;

    SELECT DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
      INTO L_N_JEA_FAILED_TASKS
      FROM INPUT_PARMS I
          ,REF_PARM RP
          ,CONTROL C
     WHERE RP.PARM_NM = 'taskTypeNm'
       AND RP.PARM_ID = I.PARM_ID
       AND C.CONTROL_ID = I.CONTROL_ID
       AND C.ROOT_INSTANCE_ID = I.ROOT_INSTANCE_ID
       AND C.CONTROL_STATE_ID = 4
       AND C.CONTROL_STATUS_ID = 2
       AND NOT EXISTS (SELECT 1
                         FROM MANUAL_TASK M
                        WHERE I.CONTROL_ID = M.CONTROL_ID);

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_JEA_TASK_FAILURE
                , L_N_JEA_FAILED_TASKS / L_N_JEA_TASKS
                );

    COMMIT;

-- select jea tasks which are part of fault handler or compensation handler handler
    SELECT     DECODE(NVL (COUNT (1), 1),0,1,NVL (COUNT (1),1))
          INTO L_FAILURE_PATH_JEAS
          FROM CONTROL CTRL
         WHERE LEVEL > 1
           AND EXISTS (
                  SELECT I.CONTROL_ID
                    FROM INPUT_PARMS I
                        ,REF_PARM RP
                   WHERE RP.PARM_NM = 'taskTypeNm'
                     AND RP.PARM_ID = I.PARM_ID
                     AND I.ROOT_INSTANCE_ID = CTRL.ROOT_INSTANCE_ID
                     AND I.CONTROL_ID = CTRL.CONTROL_ID
                     AND NOT EXISTS (SELECT 1
                                       FROM MANUAL_TASK M
                                      WHERE I.CONTROL_ID = M.CONTROL_ID))
    CONNECT BY PRIOR CTRL.CONTROL_ID = CTRL.PARENT_CONTROL_ID
           AND PRIOR CTRL.ROOT_INSTANCE_ID = CTRL.ROOT_INSTANCE_ID
           AND PRIOR CTRL.INSTANCE_ID = CTRL.INSTANCE_ID
    START WITH EXISTS (
                   SELECT 1
                     FROM INPUT_PARMS I
                         ,REF_PARM RP
                    WHERE RP.PARM_NM = 'taskTypeNm'
                      AND RP.PARM_ID = I.PARM_ID
                      AND I.ROOT_INSTANCE_ID = CTRL.ROOT_INSTANCE_ID
                      AND I.CONTROL_ID = CTRL.CONTROL_ID
                      AND NOT EXISTS (SELECT 1
                                        FROM MANUAL_TASK M
                                       WHERE I.CONTROL_ID = M.CONTROL_ID));

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_JEA_TASK_PER_PROC
                , (L_N_JEA_TASKS - L_FAILURE_PATH_JEAS) / L_NON_ROOT_INSTANCES
                );

    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_IN_PARM_PER_MT
                ,NVL (AVG (COUNT (1)), 0)
            FROM MANUAL_TASK MT
                ,INPUT_PARMS I
           WHERE MT.CONTROL_ID = I.CONTROL_ID
             AND I.ROOT_INSTANCE_ID = MT.ROOT_INSTANCE_ID
        GROUP BY MT.CONTROL_ID;

    COMMIT;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
        SELECT   L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_OUT_PARM_PER_MT
                ,NVL (AVG (COUNT (1)), 0)
            FROM MANUAL_TASK MT
                ,OUTPUT_PARMS I
           WHERE MT.CONTROL_ID = I.CONTROL_ID
             AND I.ROOT_INSTANCE_ID = MT.ROOT_INSTANCE_ID
        GROUP BY MT.CONTROL_ID;

    COMMIT;

    SELECT     nvl(AVG (LEVEL),0)
          INTO L_FAILURE_LENGTH
          FROM CONTROL CTRL
         WHERE LEVEL > 1
    CONNECT BY PRIOR CTRL.CONTROL_ID = CTRL.PARENT_CONTROL_ID
           AND PRIOR CTRL.ROOT_INSTANCE_ID = CTRL.ROOT_INSTANCE_ID
           AND PRIOR CTRL.INSTANCE_ID = CTRL.INSTANCE_ID
    START WITH EXISTS (
                   SELECT 1
                     FROM INPUT_PARMS I
                         ,REF_PARM RP
                    WHERE RP.PARM_NM = 'taskTypeNm'
                      AND RP.PARM_ID = I.PARM_ID
                      AND I.ROOT_INSTANCE_ID = CTRL.ROOT_INSTANCE_ID
                      AND I.CONTROL_ID = CTRL.CONTROL_ID
                      AND NOT EXISTS (SELECT 1
                                        FROM MANUAL_TASK M
                                       WHERE I.CONTROL_ID = M.CONTROL_ID));

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_JEA_FREC_LENGTH
                ,L_FAILURE_LENGTH
                );

    COMMIT;

    SELECT     DECODE(NVL(AVG (MAX (LEVEL)),0),0,5,AVG (MAX (LEVEL)))
          INTO L_SUB_PROC_DEPTH
          FROM CONTROL C
    START WITH C.INSTANCE_ID <> C.ROOT_INSTANCE_ID
           AND C.CONTROL_ID = C.INSTANCE_ID
           AND NOT EXISTS (SELECT 1
                             FROM CONTROL CC
                            WHERE CC.INSTANCE_ID = C.INSTANCE_ID
                              AND CC.CONTROL_STATUS_ID NOT IN (1, 3))
    CONNECT BY PRIOR C.CONTROL_ID = C.PARENT_CONTROL_ID
      GROUP BY C.INSTANCE_ID;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_N_SUB_PROC_DEPTH
                ,L_SUB_PROC_DEPTH
                );

    COMMIT;

    SELECT nvl(COUNT (1),1)
      INTO L_N_ASYNCH_JEA_TASKS
      FROM JEA_TASK;

    INSERT INTO SIZING_PARMS
                (CFG_NAME
                ,CFG_TYPE
                ,CREATED_DTM
                ,PARM_COMMENT
                ,PARM_NM
                ,VALUE
                )
         VALUES (L_CFG_NAME
                ,PLATFORM_SIZING.C_WFE_CFG_TYPE
                ,L_CFG_DATE
                ,'No comment'
                ,PLATFORM_SIZING.C_WFE_PC_ASY_TASK
                ,L_N_ASYNCH_JEA_TASKS
                );

    COMMIT;
END;
/
