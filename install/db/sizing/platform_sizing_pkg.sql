--#==============================================================================
--#  $Id: platform_sizing_pkg.sql,v 1.1.2.2 2011/05/12 20:20:25 vasyly Exp $
--#==============================================================================

CREATE OR REPLACE PACKAGE PLATFORM_SIZING
AS
/******************************************************************************
   NAME:       PLATFORM_SIZING
   PURPOSE:

******************************************************************************/

v_small_num_rows number := 100;
v_med_num_rows number:=10000;
v_small_med_num_rows number:=1000;

   v_bp1    NUMBER;            --Average number of Actions per service
   v_bp2    NUMBER;            --Initial number of Subscribers
   v_bp3    NUMBER;            --Active Locales
   v_bp4    NUMBER;            --Labels per locale
   v_bp5    NUMBER;            --Parameters
   v_bp6    NUMBER;            --Number of XML documents
   v_bp7    NUMBER;            --Expected number of trouble tickets
   v_bp8    NUMBER;            --Components per trouble ticket
   v_bp9    NUMBER;            --Services offered
   v_bp10   NUMBER;            --New Subscribers per month
   v_bp11   NUMBER;            --Subscriber purge interval (months).
   v_bp12   NUMBER;            --Parameters per Sub
   v_bp13   NUMBER;            --Contacts per Sub
   v_bp14   NUMBER;            --Parameters per Contact
   v_bp15   NUMBER;            --Addresses per Sub
   v_bp16   NUMBER;            --Components per Address
   v_bp17   NUMBER;            --Order items related to entity change
   v_bp18   NUMBER;            --Number of line items per order
   v_bp19   NUMBER;            --Initial Load of Orders
   v_bp20   NUMBER;            --Orders per month
   v_bp21   NUMBER;            --Orders purge interval (months).
   v_bp22   NUMBER;            --Parameters per Order Item
   v_bp23   NUMBER;            --Parameters per Order
   v_bp24   NUMBER;            --Services per Sub
   v_bp25   NUMBER;            --Addresses per Sub Service
   v_bp26   NUMBER;            --Number of services per sub that are dependent on other services per subscriber
   v_bp27   NUMBER;            --Parameters per provisionable sub service (sub_svc_parm)
   v_bp33   NUMBER;            --Average number of Parameters per Subnetwork (subntwk_parm)
   v_bp34   NUMBER;            --Number of non cpe links
   v_bp35   NUMBER;            --Average number of links per cpe


C_WFE_CFG_TYPE       CONSTANT SIZING_PARMS.CFG_TYPE%TYPE := 'SmpWfEngine';
    C_SMP_CFG_TYPE       CONSTANT SIZING_PARMS.CFG_TYPE%TYPE := 'Smp';
    C_DEFAULT_CFG_VERSION CONSTANT SIZING_PARMS.CFG_NAME%TYPE := 'Default';
    /*
    **   General sizing parameters
    */
    C_WFE_UNIT_OF_MESURE CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'WFE_UNIT_OF_MESURE';
    C_WFE_RETENTION_PERIOD CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Workflow request retention period';
    /*
    ** General request characteristics
    */
    C_WFE_N_SMP_REC      CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Number of WF sequests from SMP';
    C_WFE_N_LI_REQ       CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of line items per SMP request';
    C_WFE_N_OTHER_REC    CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Number of WF requests not from SMP';
    C_WFE_N_HEADER_PARMS CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of header parms';
    C_WFE_N_SUB_PROC_PER_REQ CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of subprocesses in request';
    /* dependency links per wf request */
    C_WFE_N_DEP_LINKS    CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of dependency links per request';
    /*
    ** General processes caharcteristics
    */
    C_WFE_N_IN_PARM_PER_PROC CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of input parameters per subprocess';
    C_WFE_N_PARM_PER_PROC CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of output parameters per process';
    C_WFE_N_VAR_SUB_PROCESS CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg number of variables per subprocess';
    /* Activities per process */
    -- C_WFE_N_ACT_PER_PROC CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'WFE_N_ACT_PER_PROC';
    /*
    ** General tasks characteristics
    */
    C_WFE_N_JEA_TASK_PER_PROC CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Number of JEA tasks executed per process';
    /* ratio of failed tasks - 0.05 means that 5 percent of jea tasks failes*/
    C_WFE_N_JEA_TASK_FAILURE CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Ratio of failed jea tasks';
    C_WFE_N_JEA_FREC_LENGTH CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Avg jea faulre recovery length';
    C_WFE_N_IN_PARM_PER_JEA_TASK CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'AVG number of input parameters per JEA task';
    C_WFE_N_OUT_PARM_PER_JEA_TASK CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'AVG number of ouput parameters per JEA task';
    C_WFE_N_SUB_PROC_DEPTH CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Average template depth';
    C_WFE_N_IN_PARM_PER_MT CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Average number of input parameters per manual task';
    C_WFE_N_OUT_PARM_PER_MT CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Average number of output parameters per manual task';
    /* Percent of asynch tasks for example 0.2 means 20%*/
    C_WFE_PC_ASY_TASK    CONSTANT SIZING_PARMS.PARM_NM%TYPE := 'Ratio of async JEA tasks';

    TYPE T_PARAMETER_RECORD IS TABLE OF SIZING_PARMS.VALUE%TYPE
        INDEX BY SIZING_PARMS.PARM_NM%TYPE;

    /*
    ** Returns collection of parameters for a given type
    */
    FUNCTION GET_PARAMETERS (
        P_CFG_TYPE                         SIZING_PARMS.CFG_TYPE%TYPE
       ,P_CFG_NAME                         SIZING_PARMS.CFG_NAME%TYPE
    )
        RETURN T_PARAMETER_RECORD;

    PROCEDURE CALC_JWF_TAB_LENGTH (
        P_CFG_NAME                         SIZING_PARMS.CFG_NAME%TYPE
    );
    
    PROCEDURE CALC_OBJ_SIZE (
        P_CFG_NAME                         SIZING_PARMS.CFG_NAME%TYPE,
        P_STATID                           VARCHAR2
    );

   PROCEDURE update_computed_data(P_CFG_NAME VARCHAR2);
    
    
END PLATFORM_SIZING;
/



CREATE OR REPLACE PACKAGE BODY PLATFORM_SIZING
AS
    FUNCTION GET_PARAMETERS (
        P_CFG_TYPE                         SIZING_PARMS.CFG_TYPE%TYPE
       ,P_CFG_NAME                         SIZING_PARMS.CFG_NAME%TYPE
    )
        RETURN T_PARAMETER_RECORD
    IS
        L_PARM_COLLECTION             T_PARAMETER_RECORD;
    BEGIN
        FOR PARM_REC IN (SELECT S.PARM_NM
                               ,S.VALUE
                           FROM SIZING_PARMS S
                          WHERE S.CFG_TYPE = P_CFG_TYPE
                            AND S.CFG_NAME = P_CFG_NAME)
        LOOP
            L_PARM_COLLECTION (PARM_REC.PARM_NM) := PARM_REC.VALUE;
        END LOOP;

        RETURN L_PARM_COLLECTION;
    END GET_PARAMETERS;

    PROCEDURE CALC_JWF_TAB_LENGTH (
        P_CFG_NAME                         SIZING_PARMS.CFG_NAME%TYPE
    )
    IS
        L_RI_COUNT                    NUMBER;
        L_HD_PARM_COUNT               NUMBER;
        L_INS_COUNT                   NUMBER;
        L_DEPL_COUNT                  NUMBER;
        L_INS_IN_PARM_COUNT           NUMBER;
        L_INS_PARM_COUNT              NUMBER;
        L_CTRL_COUNT                  NUMBER;
        L_JEA_COUNT                   NUMBER;
        L_JEA_IN_PARMS                NUMBER;
        L_JEA_OUT_PARMS               NUMBER;
        L_MT_COUNT                    NUMBER;
        L_MT_IN_PARMS                 NUMBER;
        L_MT_OUT_PARMS                NUMBER;
        L_VARIABLE_COUNT              NUMBER;
        L_PARMS                       T_PARAMETER_RECORD := GET_PARAMETERS (C_WFE_CFG_TYPE, P_CFG_NAME);
        L_CREATED_DTM                 DATE := SYSDATE;
    BEGIN
        L_RI_COUNT := (L_PARMS (C_WFE_N_SMP_REC) + L_PARMS (C_WFE_N_OTHER_REC)) * L_PARMS (C_WFE_RETENTION_PERIOD);
        L_HD_PARM_COUNT := L_RI_COUNT * L_PARMS (C_WFE_N_HEADER_PARMS);
        L_INS_COUNT := L_RI_COUNT * L_PARMS (C_WFE_N_SUB_PROC_PER_REQ);
        L_DEPL_COUNT := L_RI_COUNT * L_PARMS (C_WFE_N_DEP_LINKS);
        L_INS_IN_PARM_COUNT := L_PARMS (C_WFE_N_IN_PARM_PER_PROC) * L_INS_COUNT;
        L_INS_PARM_COUNT := L_PARMS (C_WFE_N_PARM_PER_PROC) * L_INS_COUNT;
        L_JEA_COUNT := L_PARMS (C_WFE_N_JEA_TASK_PER_PROC) * L_INS_COUNT;
        -- * (1 + L_PARMS(C_WFE_N_JEA_TASK_FAILURE));
        L_JEA_IN_PARMS := L_PARMS (C_WFE_N_IN_PARM_PER_JEA_TASK) * L_JEA_COUNT;
        L_JEA_OUT_PARMS := L_PARMS (C_WFE_N_OUT_PARM_PER_JEA_TASK) * L_JEA_COUNT;
        L_MT_COUNT := L_PARMS (C_WFE_N_JEA_TASK_PER_PROC) * L_INS_COUNT * L_PARMS (C_WFE_N_JEA_TASK_FAILURE);
        L_MT_IN_PARMS := L_PARMS (C_WFE_N_IN_PARM_PER_MT) * L_MT_COUNT;
        L_MT_OUT_PARMS := L_PARMS (C_WFE_N_OUT_PARM_PER_MT) * L_MT_COUNT;
        L_VARIABLE_COUNT := L_HD_PARM_COUNT + L_INS_PARM_COUNT + L_PARMS (C_WFE_N_VAR_SUB_PROCESS) * L_INS_COUNT;
        /*
         * Every subprocess has a corresponding execSubprocess activity that should be accounted here -
         *      2 + L_PARMS(C_WFE_N_SUB_PROC_PER_REQ)
         */
        L_CTRL_COUNT :=
              L_RI_COUNT                                                                              -- root instances
            + L_RI_COUNT * 2                                                         -- assuming structure - scope->flow
            + L_RI_COUNT * L_PARMS (C_WFE_N_LI_REQ) * 2                           -- assuming every line item has a flow
            + L_INS_COUNT
            + L_INS_COUNT * L_PARMS (C_WFE_N_SUB_PROC_DEPTH)
            +   L_PARMS (C_WFE_N_JEA_TASK_PER_PROC)
              * L_INS_COUNT
              * L_PARMS (C_WFE_N_JEA_TASK_FAILURE)
              * L_PARMS (C_WFE_N_JEA_FREC_LENGTH);

--            + L_PARMS(C_WFE_N_JEA_TASK_PER_PROC) * L_INS_COUNT
--            + L_PARMS(C_WFE_N_JEA_TASK_FAILURE) * L_PARMS(C_WFE_N_JEA_FREC_LENGTH);
        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'ROOT_INSTANCE'
                    ,L_CREATED_DTM
                    ,L_RI_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'PROCESS_DEF'
                    ,L_CREATED_DTM
                    ,L_RI_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'INSTANCE'
                    ,L_CREATED_DTM
                    , L_INS_COUNT + L_RI_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'DEPENDENCY_LINK'
                    ,L_CREATED_DTM
                    ,L_DEPL_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'CONTROL'
                    ,L_CREATED_DTM
                    ,L_CTRL_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'INPUT_PARMS'
                    ,L_CREATED_DTM
                    , L_INS_IN_PARM_COUNT + L_JEA_IN_PARMS + L_MT_IN_PARMS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'OUTPUT_PARMS'
                    ,L_CREATED_DTM
                    , L_JEA_OUT_PARMS + L_MT_OUT_PARMS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'MANUAL_TASK'
                    ,L_CREATED_DTM
                    ,L_MT_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'MANUAL_TASK_LOG'
                    ,L_CREATED_DTM
                    ,L_MT_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'MANUAL_TASK_NOTE'
                    ,L_CREATED_DTM
                    ,L_MT_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'MANUAL_TASK_PARM'
                    ,L_CREATED_DTM
                    , L_MT_IN_PARMS + L_MT_OUT_PARMS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'VARIABLE'
                    ,L_CREATED_DTM
                    ,L_VARIABLE_COUNT
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'JEA_TASK'
                    ,L_CREATED_DTM
                    , L_JEA_COUNT * L_PARMS (C_WFE_PC_ASY_TASK)
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'JEA_TASK_ALT_KEY'
                    ,L_CREATED_DTM
                    , L_JEA_COUNT * L_PARMS (C_WFE_PC_ASY_TASK)
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,TABLE_NAME
                    ,CREATED_DTM
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,C_WFE_CFG_TYPE
                    ,'JEA_TASK_PARM'
                    ,L_CREATED_DTM
                    , (L_JEA_IN_PARMS + L_JEA_OUT_PARMS) * L_PARMS (C_WFE_PC_ASY_TASK)
                    );
/*

CONTROL_ATTR
*/
    END CALC_JWF_TAB_LENGTH;

    PROCEDURE CALC_OBJ_SIZE (
        P_CFG_NAME                         SIZING_PARMS.CFG_NAME%TYPE
       ,P_STATID                           VARCHAR2
    )
    IS
        L_USED_BYTES                  NUMBER;
        L_ALLOC_BYTES                 NUMBER;
        L_IND_ROWS                    NUMBER;
        L_NUMLBLKS                    NUMBER;
        L_NUMDIST                     NUMBER;
        L_AVGLBLK                     NUMBER;
        L_AVGDBLK                     NUMBER;
        L_CLSTFCT                     NUMBER;
        L_INDLEVEL                    NUMBER;
        L_CACHEDBLK                   NUMBER;
        L_CACHEHIT                    NUMBER;
        L_GUESSQ                      NUMBER;
    BEGIN
        FOR TREC IN (SELECT UT.TABLE_NAME
                           ,UT.TABLESPACE_NAME
                           ,UT.PCT_FREE
                           ,STL.VALUE ROW_COUNT
                           ,STL.CFG_TYPE CFG_TYPE
                           ,SS.N3 AVG_ROW_SIZE
                           ,SS.STATID
                       FROM SIZING_TABLE_LENGTH STL
                           ,USER_TABLES UT
                           ,SIGMA_STATS SS
                      WHERE UT.TABLE_NAME = STL.TABLE_NAME
                        AND STL.CFG_NAME = P_CFG_NAME
                        AND SS.TYPE = 'T'
                        AND SS.C1 = UT.TABLE_NAME
                        AND SS.STATID = P_STATID)
        LOOP
	    BEGIN
            IF TREC.AVG_ROW_SIZE = 0
            THEN
                TREC.AVG_ROW_SIZE := 1;
            END IF;

            IF TREC.ROW_COUNT = 0
            THEN
                TREC.ROW_COUNT := 1;
            END IF;

            DBMS_SPACE.CREATE_TABLE_COST (TREC.TABLESPACE_NAME
                                         ,TREC.AVG_ROW_SIZE
                                         ,TREC.ROW_COUNT
                                         ,TREC.PCT_FREE
                                         ,L_USED_BYTES
                                         ,L_ALLOC_BYTES
                                         );

            INSERT INTO SIZING_OBJECTS
                        (CFG_NAME
                        ,CFG_TYPE
                        ,TABLESPACE_NAME
                        ,SEGMENT_NAME
                        ,SEGMENT_TYPE
                        ,STATID
                        ,USED_BYTES
                        ,ALLOC_BYTES
                        )
                 VALUES (P_CFG_NAME
                        ,TREC.CFG_TYPE
                        ,TREC.TABLESPACE_NAME
                        ,TREC.TABLE_NAME
                        ,'TABLE'
                        ,TREC.STATID
                        ,L_USED_BYTES
                        ,L_ALLOC_BYTES
                        );
                        EXCEPTION
                            WHEN OTHERS THEN
                                dbms_output.put_line('Could no estimate size for table ' || TREC.TABLESPACE_NAME ||
                                ' ! Error: ' || SQLCODE || ' - ' || SQLERRM );
                            END;

	    
        END LOOP;

        FOR IREC IN (SELECT UTS.TABLESPACE_NAME
                           ,UI.INDEX_NAME
                           ,UTS.BLOCK_SIZE
                           ,STL.VALUE ROW_COUNT
                           ,STL.CFG_TYPE
                       FROM SIZING_TABLE_LENGTH STL
                           ,USER_TABLES UT
                           ,USER_INDEXES UI
                           ,USER_TABLESPACES UTS
                      WHERE UT.TABLE_NAME = STL.TABLE_NAME
                        AND STL.CFG_NAME = P_CFG_NAME
                        AND UT.TABLE_NAME = UI.TABLE_NAME
                        AND UI.TABLESPACE_NAME = UTS.TABLESPACE_NAME)
        LOOP
            BEGIN
            DBMS_STATS.GET_INDEX_STATS (OWNNAME        => USER
                                       ,INDNAME        => IREC.INDEX_NAME
                                       ,PARTNAME       => NULL
                                       ,STATTAB        => 'SIGMA_STATS'
                                       ,STATID         => P_STATID
                                       ,NUMROWS        => L_IND_ROWS
                                       ,NUMLBLKS       => L_NUMLBLKS
                                       ,NUMDIST        => L_NUMDIST
                                       ,AVGLBLK        => L_AVGLBLK
                                       ,AVGDBLK        => L_AVGDBLK
                                       ,CLSTFCT        => L_CLSTFCT
                                       ,INDLEVEL       => L_INDLEVEL
                                       );
            INSERT INTO SIZING_OBJECTS
                        (CFG_NAME
                        ,CFG_TYPE
                        ,TABLESPACE_NAME
                        ,SEGMENT_NAME
                        ,SEGMENT_TYPE
                        ,STATID
                        ,USED_BYTES
                        ,ALLOC_BYTES
                        )
                 VALUES (P_CFG_NAME
                        ,IREC.CFG_TYPE
                        ,IREC.TABLESPACE_NAME
                        ,IREC.INDEX_NAME
                        ,'INDEX'
                        ,P_STATID
                        , IREC.ROW_COUNT * (L_NUMLBLKS * IREC.BLOCK_SIZE) / DECODE (L_IND_ROWS
                                                                                   ,0, 1
                                                                                   ,L_IND_ROWS
                                                                                   )
                        , 1.25 * IREC.ROW_COUNT * (L_NUMLBLKS * IREC.BLOCK_SIZE) / DECODE (L_IND_ROWS
                                                                                          ,0, 1
                                                                                          ,L_IND_ROWS
                                                                                          )
                        );
                        EXCEPTION
                            WHEN OTHERS THEN
                                dbms_output.put_line('Could no estimate size for table ' || IREC.TABLESPACE_NAME || 
                                ' ! Error: ' || SQLCODE || ' - ' || SQLERRM );
                            END;
        END LOOP;

        COMMIT;
    END CALC_OBJ_SIZE;
    
    PROCEDURE UPDATE_COMPUTED_DATA (
        P_CFG_NAME                         VARCHAR2
    )
    IS
        L_CFG_DATE                    DATE := SYSDATE;
    BEGIN
        SELECT VALUE
          INTO V_BP1
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP1';

        SELECT VALUE
          INTO V_BP2
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP2';

        SELECT VALUE
          INTO V_BP3
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP3'; 

        SELECT VALUE
          INTO V_BP4
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP4';

        SELECT VALUE
          INTO V_BP5
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP5';

        SELECT VALUE
          INTO V_BP6
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP6';

        SELECT VALUE
          INTO V_BP7
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP7';

        SELECT VALUE
          INTO V_BP8
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP8';

        SELECT VALUE
          INTO V_BP9
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP9';

        SELECT VALUE
          INTO V_BP10
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP10';

        SELECT VALUE
          INTO V_BP11
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP11';

        SELECT VALUE
          INTO V_BP12
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP12';

        SELECT VALUE
          INTO V_BP13
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP13';

        SELECT VALUE
          INTO V_BP14
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP14';

        SELECT VALUE
          INTO V_BP15
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP15';

        SELECT VALUE
          INTO V_BP16
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP16';

        SELECT VALUE
          INTO V_BP17
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP17';

        SELECT VALUE
          INTO V_BP18
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP18';

        SELECT VALUE
          INTO V_BP19
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP19';

        SELECT VALUE
          INTO V_BP20
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP20';

        SELECT VALUE
          INTO V_BP21
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP21';

        SELECT VALUE
          INTO V_BP22
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP22';

        SELECT VALUE
          INTO V_BP23
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP23';

        SELECT VALUE
          INTO V_BP24
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP24';

        SELECT VALUE
          INTO V_BP25
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP25';

        SELECT VALUE
          INTO V_BP26
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP26';

        SELECT VALUE
          INTO V_BP27
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP27';

        SELECT VALUE
          INTO V_BP33
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP33';

        SELECT VALUE
          INTO V_BP34
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP34';

        SELECT VALUE
          INTO V_BP35
          FROM SIZING_PARMS
         WHERE PARM_NM = 'BP35';

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'LINE_ITEM_DEPY'
                    , V_BP10 * (V_BP11 + (V_BP12 * V_BP13)) * .3
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'LINK'
                    , (V_BP23 * (V_BP1 + (V_BP2 * V_BP3)) * V_BP26) + V_BP25
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'LINK_PARM'
                    , V_BP27 * ((V_BP23 * (V_BP1 + (V_BP2 * V_BP3)) * V_BP26) + V_BP25)
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'LOCATION'
                    , (V_BP7 * ((V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'LOCATION_DTL'
                    , (V_BP8 * ((V_BP7 * (V_BP1 + (V_BP2 * V_BP3)))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'ORDR_ITEM_ASSOC_CHG'
                    , (V_BP22 * 3 * (V_BP11 + (V_BP12 * V_BP13)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'ORDR_ITEM_ENTITY_CHG'
                    , (V_BP9 * (V_BP11 + (V_BP12 * V_BP13)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB'
                    , (V_BP1 + (V_BP2 * V_BP3))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_ADDR'
                    , (V_BP7 * (V_BP1 + (V_BP2 * V_BP3)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_CONTACT'
                    , (V_BP5 * (V_BP1 + (V_BP2 * V_BP3)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_CONTACT_PARM'
                    , (V_BP6 * (V_BP5 * (V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_ORDR'
                    , (V_BP11 + (V_BP12 * V_BP13))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_ORDR_ITEM'
                    , (V_BP35 * (V_BP11 + (V_BP12 * V_BP13)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_ORDR_ITEM_PARM'
                    , (V_BP14 * (V_BP35 * (V_BP11 + (V_BP12 * V_BP13))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_ORDR_PARM'
                    , (V_BP15 * (V_BP11 + (V_BP12 * V_BP13)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_PARM'
                    , (V_BP4 * (V_BP1 + (V_BP2 * V_BP3)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC'
                    , (V_BP16 * (V_BP1 + (V_BP2 * V_BP3)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_ADDR'
                    , (V_BP17 * (V_BP16 * (V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_ASSOC'
                    , (V_BP34 * (V_BP1 + (V_BP2 * V_BP3)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_DELIVERY_PLAT'
                    , ((V_BP16 * (V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_DEPY'
                    , (V_BP18 * ((V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_NTWK'
                    , V_BP21 * (V_BP1 + (V_BP2 * V_BP3))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_NTWK_STAGE'
                    , V_BP21 * (V_BP1 + (V_BP2 * V_BP3)) / 1000
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_PARM'
                    , (V_BP19 * (V_BP16 * (V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUB_SVC_PARM_DEPY'
                    , (V_BP20 * ((V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUBNTWK'
                    , (V_BP33 + (V_BP23 * (V_BP1 + (V_BP2 * V_BP3))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUBNTWK_PARM'
                    , (V_BP24 * (V_BP33 + (V_BP23 * (V_BP1 + (V_BP2 * V_BP3)))))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'SUBNTWK_PARM_STAGE'
                    , ((V_BP24 * (V_BP33 + (V_BP23 * (V_BP1 + (V_BP2 * V_BP3))))) * .01)
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CM'
                    ,L_CFG_DATE
                    ,'WRK_LOCK'
                    , (2 * (V_BP11 + (V_BP12 * V_BP13)))
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CATALOG_MANAGER'
                    ,L_CFG_DATE
                    ,'REF_SVC_ACTION_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CATALOG_MANAGER'
                    ,L_CFG_DATE
                    ,'SVC_ACTION'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'CATALOG_MANAGER'
                    ,L_CFG_DATE
                    ,'SVC'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'PARM'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'PARMVAL_EXCLUSION_GRP'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'PARM_PERMITTED_VAL'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'PARM_VLD_RULE'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'CFG_VERSION'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_CONVERSION_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_DATATYPE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_DATA_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_LANG'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_LOCALE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_LOCALE_COUNTRY'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'LOCATION_STRUC'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'XML_REPOSITORY'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'LBL'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'LOCATION_CATEGORY'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'VLD_RULE'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'VERSION'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'REF_CLASS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'COMMON'
                    ,L_CFG_DATE
                    ,'STRUC_HIERARCHY'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'DIAG_MANAGER'
                    ,L_CFG_DATE
                    ,'REF_TICKET_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'DIAG_MANAGER'
                    ,L_CFG_DATE
                    ,'REF_TICKET_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'DIAG_MANAGER'
                    ,L_CFG_DATE
                    ,'REF_SVC_ADVISORY_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'DIAG_MANAGER'
                    ,L_CFG_DATE
                    ,'SVC_ADVISORY'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'DIAG_MANAGER'
                    ,L_CFG_DATE
                    ,'SVC_ADVISORY_PARM'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_USER'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_USER_PARM'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_USER_SVC'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SVC_PROVIDER'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SERVER_NODE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'STATE_ACTION'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'STATE_TRANSITION'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'STATE_TRANSITION_EVENT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'STM_BATCH_REPLY_INFO'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_OBJ_ENQ'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_PRODUCT'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_PROD_ADDR'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_PROD_PARM'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'PROFILE_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_SVC_GOLDHSD_PARM'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_EVENT'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_CONTACT_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_IP_SVC_PROVIDER'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_LINK_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_LOCK_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_MGMT_MODE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_NTWK_CMPNT_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_NTWK_ROLE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_NTWK_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_OBJECT_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ORDR_ACTION'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ORDR_ITEM_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ORDR_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ORDR_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_PROJ_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_REPROV_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_RESOURCE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SCREEN'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SSN_GROUP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_STATUS_CHG_REASON'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_STATUS_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SUBNTWK_MDL'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SUBNTWK_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SUB_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SUB_SVC_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SUB_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_SVC_DELIVERY_PLAT'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_TASK_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_TOPOLOGY_ACTION_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_TOPOLOGY_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_USER_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ADDR_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ADDR_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_ASSOC_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_CARD_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_COMM_DIRECTION'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_CONSUMABLE_RSRC_TYP'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_CONTACT_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'REFERENCE'
                    ,L_CFG_DATE
                    ,'REF_IP_STATUS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'SITE_SURVEY'
                    ,L_CFG_DATE
                    ,'SITE_SURV'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'SITE_SURVEY'
                    ,L_CFG_DATE
                    ,'SITE_SURV_ADDR'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'SITE_SURVEY'
                    ,L_CFG_DATE
                    ,'SITE_SURV_PARM'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_SRC_EXT_PARMS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_GRP_QUALIFIERS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_GRP_EXT_PROC_COND'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_EXT_PROC_PARMS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_GRP_ACTION_PARMS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_GRP_ACTIONS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_GRPS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_ACTION_PARMS_TMP'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_DEBUG'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_EXT_PROCS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_SYNCHRONIZATION'
                    ,L_CFG_DATE
                    ,'SYNC_GRP_EXT_PROCS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_TMPLT_IMPL_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_TMPLT_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_TRANSITIONS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK_TMPLT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK_TMPLT_DEPY'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK_TMPLT_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_TMPLT_IMPL'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_TMPLT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_PARM'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'SRC_ACTION_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK_ACTION'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASKS_IN_PROJ_TMPLT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'ACTION_TMPLT_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'ACTION_TMPLT'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'ACTIONS_IN_TASK_TMPLT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ_IMPACTED_SUB_SVC'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'TASK_TRANSITIONS'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'LOCKED_RESOURCE'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'STM_WORKFLOW'
                    ,L_CFG_DATE
                    ,'PROJ'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'IP_ADDR'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'TECH_PLATFORM_TYP'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'IP_SBNT'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_CMPNT_CHG_IMPACT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_CMPNT_TYP_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_LOCATION'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_LOCATION_DTLS'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_LOCATION_STRUC'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_RESOURCE'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'NTWK_RESOURCE_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'PORT'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'PORT_PAIR'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'PORT_PAIR_LINK'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'CARD'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'CARD_MDL'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'CONSUMABLE_RSRC_MGMT'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'EXT_PARM_MAPPING'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'FREQ'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'FREQ_GRP'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'FREQ_GRP_LIST'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SUB_SVC_PLAT_STAGE'
                    ,V_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SVC_SUPPORTED_PLATFORM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'RESOURCE_ASSIGN'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SBNT_TECH_PLATFORM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SUBNTWK_CARD_LMT'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SUBNTWK_COVERAGE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SUBNTWK_MDL_LMT'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SUBNTWK_TYP_FREQ'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'SUBNTWK_TYP_FREQ_GRP'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'TECH_PLATFORM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'TECH_PLATFORM_MGMT_MODE'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'TECH_PLATFORM_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'TOPOLOGY_MANAGER'
                    ,L_CFG_DATE
                    ,'IP_BLOCK'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'WEBLOGIC'
                    ,L_CFG_DATE
                    ,'JEAWLSTORE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'WEBLOGIC'
                    ,L_CFG_DATE
                    ,'JWFWLSTORE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'WEBLOGIC'
                    ,L_CFG_DATE
                    ,'SMPWLSTORE'
                    ,V_SMALL_NUM_ROWS
                    );

        INSERT INTO SIZING_TABLE_LENGTH
                    (CFG_NAME
                    ,CFG_TYPE
                    ,CREATED_DTM
                    ,TABLE_NAME
                    ,VALUE
                    )
             VALUES (P_CFG_NAME
                    ,'WORKING_TABLES'
                    ,L_CFG_DATE
                    ,'WRK_UNIQUE_PARM'
                    ,V_SMALL_MED_NUM_ROWS
                    );

        COMMIT;
    END;
    
END PLATFORM_SIZING;
/

