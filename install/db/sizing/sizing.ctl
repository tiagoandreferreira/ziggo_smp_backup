LOAD DATA
    REPLACE
    INTO TABLE SIZING_PARMS
      trailing nullcols
(
	CFG_NAME 		char terminated by ',',
	PARM_NM     	char terminated by ',',
	CFG_TYPE    	char terminated by ',',
	CREATED_DTM		SYSDATE,
	VALUE       	decimal external terminated by ',',
	PARM_COMMENT    char terminated by ','
)

