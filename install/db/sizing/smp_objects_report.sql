--#==============================================================================
--#  $Id: smp_objects_report.sql,v 1.1.2.4 2011/05/12 20:20:25 vasyly Exp $
--#==============================================================================

set serveroutput on

define p_cfg_name=&1

exec platform_sizing.CALC_JWF_TAB_LENGTH (upper('&p_cfg_name'));
commit;
exec platform_sizing.update_computed_data(upper('&p_cfg_name'));
commit;
exec platform_sizing.CALC_OBJ_SIZE (upper('&p_cfg_name'),upper('&p_cfg_name'));
commit;

set pagesize 1000
set linesize 120
-- set feedback off

spool ./logs/table_storage_&p_cfg_name..report

column ddl format a5
column table_name format a40
column storage_mb format a20
column num_rows format a20

SELECT   STL.CFG_TYPE, stl.table_name, TO_CHAR (SO.ALLOC_BYTES/(1024*1024), '999,999,999.9999') storage_mb1,
         TO_CHAR (STL.VALUE, '999,999,999,999') num_rows
    FROM SIZING_TABLE_LENGTH stl,SIZING_OBJECTS so
   WHERE stl.table_name = so.segment_name and so.segment_type = 'TABLE'
   AND STL.CFG_NAME = upper('&p_cfg_name')
ORDER BY table_name;

spool off
--
--
spool ./logs/index_storage_&p_cfg_name..report

column ddl format a5
column table_name format a30
column index_name format a30
column storage_mb format a20
column num_rows format a20

SELECT   SO.CFG_TYPE, SO.SEGMENT_NAME, TO_CHAR (SO.ALLOC_BYTES/(1024*1024), '999,999,999.9999') storage_mb
    FROM SIZING_OBJECTS so
   WHERE so.segment_type = 'INDEX'
   AND SO.CFG_NAME = upper('&p_cfg_name')
ORDER BY SO.SEGMENT_NAME;

spool off
--
--
spool ./logs/all_tables_storage_GB_&p_cfg_name..report

column ddl format a5
column storage_gb format a20

SELECT   SO.TABLESPACE_NAME
        ,TO_CHAR (SUM (SO.ALLOC_BYTES)/(1024*1024), '999,999,999.9999') storage_mb
    FROM SIZING_OBJECTS SO
   WHERE SO.CFG_NAME = upper('&p_cfg_name')
GROUP BY SO.TABLESPACE_NAME;

exit;
