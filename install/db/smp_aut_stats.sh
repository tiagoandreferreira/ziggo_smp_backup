#!/usr/bin/ksh
#--=============================================================================
#--    $Id: smp_aut_stats.sh,v 1.5 2002/12/12 20:34:38 germans Exp $
#--
#--  DESCRIPTION
#--
#--  RELATED SCRIPTS
#--
#--  REVISION HISTORY
#--  * Based on CVS log
#--=============================================================================

. setEnv.sh

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    echo $USAGE
    exit 1
fi

CM=`echo ${SAMP_DB_USER_ROOT}CM | nawk '{ print toupper($1) }'`
LOGIN_CM=$CM/${SAMP_DB_PASSWORD}@${SAMP_DB}

sqlplus -s $LOGIN_CM <<END >/tmp/$$.tmp
    set pagesize 0
    set linesize 10000
    set heading off
    set feedback off
    select to_char(sysdate,'yyyy-mm-dd-hh24-mi-ss') from dual;
END

EXEC_TIME=`cat /tmp/$$.tmp`

LOG=/tmp/stats-$EXEC_TIME.log
FILE=/tmp/stats.sql

date | awk {'print $0 " .... Gathering stats and creating histograms ..."'} > $LOG

echo "execute dbms_stats.gather_database_stats (NULL,FALSE,'FOR ALL COLUMNS SIZE 1',NULL,'DEFAULT',TRUE,NULL,NULL,'GATHER STALE','LIST');" > $FILE

echo "exec dbms_stats.gather_table_stats ('"$CM"','SUB_CONTACT_MTHD_PARM',NULL,5,FALSE,'FOR ALL INDEXED COLUMNS SIZE 75',2,'DEFAULT',FALSE,NULL,NULL,NULL);" >> $FILE

echo "exec dbms_stats.gather_table_stats ('"$CM"','SUB_CONTACT_PARM',NULL,5,FALSE,'FOR ALL INDEXED COLUMNS SIZE 75',2,'DEFAULT',FALSE,NULL,NULL,NULL);" >> $FILE

echo "exec dbms_stats.gather_table_stats ('"$CM"','SUB_PARM',NULL,5,FALSE,'FOR ALL INDEXED COLUMNS SIZE 75',2,'DEFAULT',FALSE,NULL,NULL,NULL);" >> $FILE

echo "exec dbms_stats.gather_table_stats ('"$CM"','SUB_SVC_PARM',NULL,5,FALSE,'FOR ALL INDEXED COLUMNS SIZE 75',2,'DEFAULT',FALSE,NULL,NULL,NULL);" >> $FILE

echo "exec dbms_stats.gather_table_stats ('"$CM"','LOCATION_DTL',NULL,5,FALSE,'FOR ALL INDEXED COLUMNS SIZE 75',2,'DEFAULT',FALSE,NULL,NULL,NULL);" >> $FILE

cat $FILE  | sqlplus -s $LOGIN_CM  1>> $LOG  2>> $LOG 

rm /tmp/$$.tmp $FILE 

date | awk {'print $0 " .... "'} >> $LOG 

exit 1
