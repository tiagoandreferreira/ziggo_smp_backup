#!/usr/bin/ksh
#--============================================================================
#--    $Id: smp_db_stats.sh,v 1.10 2007/04/17 16:47:27 siarheig Exp $
#--
#--  DESCRIPTION
#--
#--  RELATED SCRIPTS
#--
#--  REVISION HISTORY
#--  * Based on CVS log
#--============================================================================

date | awk {'print $0 " .... Starting Gathering statistics processes ...."'} 

BASE_DB_DIR=../..

../../util/setEnv.sh

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    echo $USAGE
    exit 1
fi

AM=`echo ${SAMP_DB_USER_ROOT}AM | nawk '{ print toupper($1) }'`
CM=` echo ${SAMP_DB_USER_ROOT}CM | nawk '{ print toupper($1) }'`

LOGIN_AM=${SAMP_DB_USER_ROOT}am/${SAMP_DB_PASSWORD}@${SAMP_DB}
LOGIN_CM=${SAMP_DB_USER_ROOT}cm/${SAMP_DB_PASSWORD}@${SAMP_DB}

AM_ARG=/tmp/$AM.sql
CM_ARG=/tmp/$CM.sql

if [ -r $AM_ARG ]
 then
  rm -f $AM_ARG
fi

if [ -r $CM_ARG ]
 then
  rm -f $CM_ARG
fi

echo "exec dbms_stats.gather_schema_stats ('"$AM"',NULL,FALSE,'FOR ALL COLUMNS SIZE 1',2,'DEFAULT',TRUE);" > $AM_ARG
echo "exec dbms_stats.gather_schema_stats ('"$CM"',NULL,FALSE,'FOR ALL COLUMNS SIZE 1',2,'DEFAULT',TRUE);" > $CM_ARG

cat $AM_ARG | sqlplus -s $LOGIN_AM  1>> null  2>> null
date | awk {'print $0 " .... Statistics for AM completed"'}

cat $CM_ARG | sqlplus -s $LOGIN_CM  1>> null  2>> null
date | awk {'print $0 " .... Statistics for CM completed"'}

rm -f $AM_ARG
rm -f $CM_ARG

exit 1
