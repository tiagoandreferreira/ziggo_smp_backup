CREATE UNIQUE INDEX ACTION_TMPLT_PK ON ACTION_TMPLT
(
       action_tmplt_nm                ASC,
       action_tmplt_ver               ASC
);

CREATE INDEX ACTION_TMPLT_IX1 ON ACTION_TMPLT
(
       topology_action_typ            ASC
);

CREATE UNIQUE INDEX ACTION_TMPLT_PARM_PK ON ACTION_TMPLT_PARM
(
       action_tmplt_parm_id           ASC
);

CREATE INDEX ACTION_TMPLT_PARM_IX1 ON ACTION_TMPLT_PARM
(
       ntwk_cmpnt_typ_nm              ASC
);

CREATE INDEX ACTION_TMPLT_PARM_IX2 ON ACTION_TMPLT_PARM
(
       parm_id                        ASC
);

CREATE INDEX ACTION_TMPLT_PARM_IX3 ON ACTION_TMPLT_PARM
(
       action_tmplt_ver               ASC,
       action_tmplt_nm                ASC
);

CREATE UNIQUE INDEX ACTIONS_IN_TASK_TMPLT_PK ON ACTIONS_IN_TASK_TMPLT
(
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC,
       execution_seq                  ASC
);

CREATE INDEX ACTIONS_IN_TASK_TMPLT_IX1 ON ACTIONS_IN_TASK_TMPLT
(
       screen_nm                      ASC
);

CREATE INDEX ACTIONS_IN_TASK_TMPLT_IX3 ON ACTIONS_IN_TASK_TMPLT
(
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC
);

CREATE INDEX ACTIONS_IN_TASK_TMPLT_IX4 ON ACTIONS_IN_TASK_TMPLT
(
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC,
       parent_execution_seq           ASC
);

CREATE INDEX ACTIONS_IN_TASK_TMPLT_IX5 ON ACTIONS_IN_TASK_TMPLT
(
       action_tmplt_nm                ASC,
       action_tmplt_ver               ASC
);

CREATE UNIQUE INDEX ARCH_ADM_PK ON ARCH_ADM
(
       adm_nm                         ASC
);

CREATE UNIQUE INDEX ARCH_GRP_PK ON ARCH_GRP
(
       grp_id                         ASC
);

CREATE INDEX ARCH_GRP_IX1 ON ARCH_GRP
(
       grp_id                         ASC,
       root_tbl_nm                    ASC
);

CREATE UNIQUE INDEX ARCH_GRP_PARM_PK ON ARCH_GRP_PARM
(
       grp_id                         ASC,
       parm_nm                        ASC
);

CREATE INDEX ARCH_GRP_PARM_FK1 ON ARCH_GRP_PARM
(
       parm_nm                        ASC
);

CREATE UNIQUE INDEX ARCH_GRP_PART_PK ON ARCH_GRP_PART
(
       grp_id                         ASC,
       part_id                        ASC
);

CREATE UNIQUE INDEX ARCH_PARM_HIST_PK ON ARCH_PARM_HIST
(
       modified_dtm                   ASC,
       mod_tbl_nm                     ASC,
       parm_hist_id                   ASC
);

CREATE UNIQUE INDEX ARCH_RUN_PK ON ARCH_RUN
(
       grp_id                         ASC,
       run_id                         ASC
);

CREATE UNIQUE INDEX ARCH_STACK_PK ON ARCH_STACK
(
       ref_role                       ASC
);

CREATE UNIQUE INDEX ARCH_TBL_PK ON ARCH_TBL
(
       grp_id                         ASC,
       tbl_nm                         ASC
);

CREATE UNIQUE INDEX ARCH_TBL_LOG_PK ON ARCH_TBL_LOG
(
       grp_id                         ASC,
       run_id                         ASC,
       tbl_nm                         ASC,
       part_id                        ASC
);

CREATE INDEX ARCH_TBL_LOG_FK2 ON ARCH_TBL_LOG
(
       grp_id                         ASC,
       tbl_nm                         ASC,
       part_id                        ASC
);

CREATE UNIQUE INDEX ARCH_TBL_NET_PK ON ARCH_TBL_NET
(
       grp_id                         ASC,
       src_tbl_nm                     ASC,
       tgt_tbl_nm                     ASC,
       ref_role                       ASC
);

CREATE INDEX ARCH_TBL_NET_FK2 ON ARCH_TBL_NET
(
       grp_id                         ASC,
       tgt_tbl_nm                     ASC
);

CREATE UNIQUE INDEX ARCH_TBL_PART_PK ON ARCH_TBL_PART
(
       grp_id                         ASC,
       tbl_nm                         ASC,
       part_id                        ASC
);

CREATE INDEX ARCH_TBL_PART_FK2 ON ARCH_TBL_PART
(
       grp_id                         ASC,
       part_id                        ASC
);

CREATE UNIQUE INDEX CARD_PK ON CARD
(
       card_id                        ASC
);

CREATE INDEX CARD_IX1 ON CARD
(
       card_typ_id                    ASC,
       card_mdl_id                    ASC
);

CREATE INDEX CARD_IX2 ON CARD
(
       subntwk_id                     ASC
);

CREATE UNIQUE INDEX CARD_MDL_PK ON CARD_MDL
(
       card_typ_id                    ASC,
       card_mdl_id                    ASC
);

CREATE UNIQUE INDEX CARD_MDL_UK ON CARD_MDL
(
       card_typ_id                    ASC,
       card_mdl_nm                    ASC
);

CREATE UNIQUE INDEX CFG_VERSION_PK ON CFG_VERSION
(
       cfg_nm                         ASC
);

CREATE UNIQUE INDEX CONSUMABLE_RSRC_MGMT_PK ON CONSUMABLE_RSRC_MGMT
(
       provider_subntwk_typ_id        ASC,
       consumer_subntwk_typ_id        ASC,
       svc_delivery_plat_id           ASC,
       consumable_rsrc_typ_nm         ASC
);

CREATE UNIQUE INDEX EXT_PARM_MAPPING_PK ON EXT_PARM_MAPPING
(
       stm_parm_nm                    ASC,
       ext_parm_nm                    ASC
);

CREATE UNIQUE INDEX EXT_PARM_MAPPING_UK1 ON EXT_PARM_MAPPING
(
       ext_parm_nm                    ASC,
       stm_parm_nm                    ASC
);

CREATE UNIQUE INDEX FREQ_PK ON FREQ
(
       freq_id                        ASC
);

CREATE INDEX FREQ_IX1 ON FREQ
(
       comm_direction_id              ASC
);

CREATE UNIQUE INDEX FREQ_GRP_PK ON FREQ_GRP
(
       freq_grp_id                    ASC
);

CREATE UNIQUE INDEX FREQ_GRP_UK1 ON FREQ_GRP
(
       freq_grp_nm                    ASC
);

CREATE UNIQUE INDEX FREQ_GRP_LIST_PK ON FREQ_GRP_LIST
(
       freq_grp_id                    ASC,
       freq_id                        ASC
);

CREATE INDEX FREQ_GRP_LIST_IX1 ON FREQ_GRP_LIST
(
       freq_id                        ASC
);

CREATE UNIQUE INDEX IP_ADDR_PK ON IP_ADDR
(
       ip_addr                        ASC
);

CREATE INDEX IP_ADDR_IX2 ON IP_ADDR
(
       assigned_subntwk_id            ASC
);

CREATE INDEX IP_ADDR_IX1 ON IP_ADDR
(
       ip_sbnt_id                     ASC
);

CREATE INDEX IP_ADDR_IX3 ON IP_ADDR
(
       ip_status_nm                   ASC
);

CREATE UNIQUE INDEX IP_BLOCK_PK ON IP_BLOCK
(
       ip_block_id                    ASC
);

CREATE UNIQUE INDEX IP_BLOCK_UK1 ON IP_BLOCK
(
       ip_block_nm                    ASC
);

CREATE INDEX IP_BLOCK_IX2 ON IP_BLOCK
(
       ip_status_nm                   ASC
);

CREATE INDEX IP_BLOCK_IX1 ON IP_BLOCK
(
       managing_subntwk_id            ASC
);

CREATE UNIQUE INDEX IP_SBNT_PK ON IP_SBNT
(
       ip_sbnt_id                     ASC
);

CREATE INDEX IP_SBNT_IX1 ON IP_SBNT
(
       ip_status_nm                   ASC
);

CREATE INDEX IP_SBNT_IX5 ON IP_SBNT
(
       managing_subntwk_id            ASC
);

CREATE INDEX IP_SBNT_IX4 ON IP_SBNT
(
       ip_block_id                    ASC
);

CREATE UNIQUE INDEX LBL_PK ON LBL
(
       class_id                       ASC,
       obj_nm                         ASC,
       locale_cd                      ASC
);

CREATE UNIQUE INDEX LINE_ITEM_DEPY_PK ON LINE_ITEM_DEPY
(
       sub_ordr_id                    ASC,
       prereq_item_id                 ASC,
       depy_item_id                   ASC
);

CREATE UNIQUE INDEX LINK_PK ON LINK
(
       link_id                        ASC
);

CREATE UNIQUE INDEX LINK_UK1 ON LINK
(
       link_nm                        ASC
);

CREATE INDEX LINK_IX4 ON LINK
(
       link_typ_id                    ASC
);

CREATE INDEX LINK_IX3 ON LINK
(
       topology_status_id             ASC
);

CREATE INDEX LINK_IX2 ON LINK
(
       to_subntwk_id                  ASC
);

CREATE INDEX LINK_IX1 ON LINK
(
       from_subntwk_id                ASC
);

CREATE UNIQUE INDEX LINK_PARM_PK ON LINK_PARM
(
       link_id                        ASC,
       parm_id                        ASC
);

CREATE INDEX LINK_PARM_IX1 ON LINK_PARM
(
       parm_id                        ASC,
       val                            ASC
);


CREATE UNIQUE INDEX LOCATION_PK ON LOCATION
(
       location_id                    ASC
);

CREATE UNIQUE INDEX LOCATION_CATEGORY_PK ON LOCATION_CATEGORY
(
       location_category_id           ASC
);

CREATE UNIQUE INDEX LOCATION_CATEGORY_UK1 ON LOCATION_CATEGORY
(
       location_category_nm           ASC
);

CREATE UNIQUE INDEX LOCATION_DTL_PK ON LOCATION_DTL
(
       location_id                    ASC,
       parm_id                        ASC
);

CREATE INDEX LOCATION_DTL_IX1 ON LOCATION_DTL
(
       parm_id                        ASC,
       location_dtl_val               ASC
);

CREATE UNIQUE INDEX LOCATION_STRUC_PK ON LOCATION_STRUC
(
       location_struc_id              ASC
);

CREATE UNIQUE INDEX LOCATION_STRUC_UK1 ON LOCATION_STRUC
(
       location_struc_nm              ASC
);

CREATE INDEX LOCATION_STRUC_IX1 ON LOCATION_STRUC
(
       parent_location_struc_id       ASC
);

CREATE INDEX LOCATION_STRUC_IX2 ON LOCATION_STRUC
(
       location_category_id           ASC
);

CREATE UNIQUE INDEX LOCKED_RESOURCE_PK ON LOCKED_RESOURCE
(
       cmpnt_id                       ASC,
       ntwk_cmpnt_typ_nm              ASC
);

CREATE INDEX LOCKED_RESOURCE_IX1 ON LOCKED_RESOURCE
(
       lock_typ                       ASC
);

CREATE INDEX LOCKED_RESOURCE_IX2 ON LOCKED_RESOURCE
(
       proj_id                        ASC
);



CREATE UNIQUE INDEX NTWK_CMPNT_CHG_IMPACT_PK ON NTWK_CMPNT_CHG_IMPACT
(
       ntwk_cmpnt_chg_impact_id       ASC
);

CREATE UNIQUE INDEX NTWK_CMPNT_TYP_PARM_PK ON NTWK_CMPNT_TYP_PARM
(
       ntwk_cmpnt_typ_nm              ASC,
       parm_id                        ASC
);

CREATE INDEX NTWK_CMPNT_TYP_PARM_IX1 ON NTWK_CMPNT_TYP_PARM
(
       parm_id                        ASC
);

CREATE UNIQUE INDEX NTWK_LOCATION_PK ON NTWK_LOCATION
(
       ntwk_location_id               ASC
);

CREATE UNIQUE INDEX NTWK_LOCATION_UK1 ON NTWK_LOCATION
(
       ntwk_location_nm               ASC
);

CREATE UNIQUE INDEX NTWK_LOCATION_DTLS_PK ON NTWK_LOCATION_DTLS
(
       ntwk_location_id               ASC,
       location_category_id           ASC
);

CREATE INDEX NTWK_LOCATION_DTLS_IX1 ON NTWK_LOCATION_DTLS
(
       location_category_id           ASC,
       location_dtl_val               ASC
);

CREATE UNIQUE INDEX NTWK_LOCATION_STRUC_PK ON NTWK_LOCATION_STRUC
(
       ntwk_location_id               ASC,
       location_struc_id              ASC
);

CREATE INDEX NTWK_LOCATION_STRUC_IX1 ON NTWK_LOCATION_STRUC
(
       location_struc_id              ASC
);

CREATE UNIQUE INDEX NTWK_RESOURCE_PK ON NTWK_RESOURCE
(
       rsrc_id                        ASC
);

CREATE INDEX NTWK_RESOURCE_IX1 ON NTWK_RESOURCE
(
       rsrc_state                     ASC
);

CREATE INDEX NTWK_RESOURCE_IX2 ON NTWK_RESOURCE
(
       sub_svc_id                     ASC
);

CREATE INDEX NTWK_RESOURCE_IX3 ON NTWK_RESOURCE
(
       subntwk_id                     ASC
);


CREATE UNIQUE INDEX NTWK_RESOURCE_PARM_PK ON NTWK_RESOURCE_PARM
(
       rsrc_id                        ASC,
       parm_id                        ASC
);

CREATE UNIQUE INDEX ORDR_ITEM_ENTITY_CHG_UK ON ORDR_ITEM_ENTITY_CHG
(
       sub_ordr_item_id               ASC,
       parm_id                        ASC,
       sub_entity_id                  ASC
);

CREATE INDEX ORDR_ITEM_ENTITY_CHG_IX1 ON ORDR_ITEM_ENTITY_CHG
(
      SUB_ENTITY_ID		     ASC, 
      PARM_ID			     ASC, 
      SUB_ORDR_ITEM_ID               ASC
);

CREATE UNIQUE INDEX PARM_PK ON PARM
(
       parm_id                        ASC
);

CREATE UNIQUE INDEX PARM_UK1 ON PARM
(
       parm_nm                        ASC,
       class_id                       ASC,
       object_id                      ASC
);

CREATE UNIQUE INDEX PARM_UK3 ON PARM
(
       parm_nm                        ASC,
       parm_id                        ASC
);

CREATE UNIQUE INDEX PARM_UK2 ON PARM
(
       parm_nm                        ASC,
       class_id                       ASC,
       object_id                      ASC,
       parm_id                        ASC
);

CREATE INDEX PARM_IX1 ON PARM
(
       class_id                       ASC
);

CREATE UNIQUE INDEX PARM_PERMITTED_VAL_UK ON PARM_PERMITTED_VAL
(
       parm_id                        ASC,
       jmf_permitted_val_expr         ASC,
       val                            ASC
);

CREATE UNIQUE INDEX PARM_VLD_RULE_PK ON PARM_VLD_RULE
(
       parm_id                        ASC,
       vld_rule_id                    ASC
);

CREATE UNIQUE INDEX PARMVAL_EXCLUSION_GRP_PK ON PARMVAL_EXCLUSION_GRP
(
       parmval_exclusion_grp_nm       ASC,
       parm_id                        ASC
);

CREATE UNIQUE INDEX PORT_PK ON PORT
(
       port_id                        ASC
);

CREATE INDEX PORT_IX2 ON PORT
(
       freq_grp_id                    ASC
);

CREATE INDEX PORT_IX3 ON PORT
(
       freq_id                        ASC
);

CREATE INDEX PORT_IX4 ON PORT
(
       comm_direction_id              ASC
);

CREATE INDEX PORT_IX1 ON PORT
(
       card_id                        ASC
);

CREATE UNIQUE INDEX PORT_PAIR_PK ON PORT_PAIR
(
       downstream_port_id             ASC,
       upstream_port_id               ASC
);

CREATE INDEX PORT_PAIR_IX1 ON PORT_PAIR 
(
       MANAGING_SUBNTWK_ID 	ASC 
);

CREATE UNIQUE INDEX PORT_PAIR_LINK_PK ON PORT_PAIR_LINK
(
       link_id                        ASC,
       downstream_port_id             ASC,
       upstream_port_id               ASC
);

CREATE INDEX PORT_PAIR_LINK_IX1 ON PORT_PAIR_LINK
(
       downstream_port_id             ASC,
       upstream_port_id               ASC
);

CREATE UNIQUE INDEX PROJ_PK ON PROJ
(
       proj_id                        ASC
);

CREATE UNIQUE INDEX PROJ_UK1 ON PROJ
(
       proj_nm                        ASC
);

CREATE INDEX PROJ_IX1 ON PROJ
(
       proj_status_nm                 ASC
);

CREATE INDEX PROJ_IX2 ON PROJ
(
       proj_tmplt_impl_ver            ASC,
       proj_tmplt_impl_nm             ASC
);

CREATE INDEX PROJ_IX3 ON PROJ 
(
      hint_SUBNTWK_ID 		ASC
);

CREATE UNIQUE INDEX PROJ_IMPACTED_SUB_SVC_PK ON PROJ_IMPACTED_SUB_SVC
(
       proj_id                        ASC,
       sub_svc_id                     ASC
);

CREATE UNIQUE INDEX PROJ_IMPACTED_SUB_SVC_IX1 ON PROJ_IMPACTED_SUB_SVC
(
       sub_svc_id                     ASC,
       proj_id                        ASC
);

CREATE INDEX PROJ_IMPACTED_SUB_SVC_IX2 ON PROJ_IMPACTED_SUB_SVC
(
       reprov_status                  ASC
);

CREATE UNIQUE INDEX PROJ_PARM_PK ON PROJ_PARM
(
       proj_id                        ASC,
       parm_id                        ASC
);

CREATE INDEX PROJ_PARM_IX2 ON PROJ_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE UNIQUE INDEX PROJ_TMPLT_PK ON PROJ_TMPLT
(
       proj_tmplt_nm                  ASC,
       proj_tmplt_ver                 ASC
);

CREATE UNIQUE INDEX PROJ_TMPLT_IMPL_PK ON PROJ_TMPLT_IMPL
(
       proj_tmplt_impl_nm             ASC,
       proj_tmplt_impl_ver            ASC
);

CREATE INDEX PROJ_TMPLT_IMPL_IX1 ON PROJ_TMPLT_IMPL
(
       proj_tmplt_nm                  ASC,
       proj_tmplt_ver                 ASC
);

CREATE UNIQUE INDEX PROJ_TMPLT_IMPL_PARM_PK ON PROJ_TMPLT_IMPL_PARM
(
       proj_tmplt_parm_id             ASC,
       proj_tmplt_impl_nm             ASC,
       proj_tmplt_impl_ver            ASC
);

CREATE INDEX PROJ_TMPLT_IMPL_PARM_IX1 ON PROJ_TMPLT_IMPL_PARM
(
       proj_tmplt_parm_id             ASC,
       val                            ASC
);

CREATE UNIQUE INDEX PROJ_TMPLT_PARM_PK ON PROJ_TMPLT_PARM
(
       proj_tmplt_parm_id             ASC
);

CREATE INDEX PROJ_TMPLT_PARM_IX1 ON PROJ_TMPLT_PARM
(
       parm_id                        ASC
);

CREATE INDEX PROJ_TMPLT_PARM_IX2 ON PROJ_TMPLT_PARM
(
       proj_tmplt_nm                  ASC,
       proj_tmplt_ver                 ASC
);

CREATE UNIQUE INDEX PROJ_TRANSITIONS_PK ON PROJ_TRANSITIONS
(
       from_proj_status_nm            ASC,
       to_proj_status_nm              ASC
);

CREATE INDEX PROJ_TRANSITIONS_IX1 ON PROJ_TRANSITIONS
(
       to_proj_status_nm              ASC
);



CREATE UNIQUE INDEX REF_ADDR_STATUS_PK ON REF_ADDR_STATUS
(
       addr_status_id                 ASC
);

CREATE UNIQUE INDEX REF_ADDR_TYP_PK ON REF_ADDR_TYP
(
       addr_typ_id                    ASC
);

CREATE UNIQUE INDEX REF_ADDR_TYP_UK1 ON REF_ADDR_TYP
(
       addr_typ_nm                    ASC
);

CREATE UNIQUE INDEX REF_ARCH_PARM_PK ON REF_ARCH_PARM
(
       parm_nm                        ASC
);

CREATE UNIQUE INDEX REF_CARD_TYP_PK ON REF_CARD_TYP
(
       card_typ_id                    ASC
);

CREATE UNIQUE INDEX REF_CARD_TYP_UK1 ON REF_CARD_TYP
(
       card_typ_nm                    ASC
);

CREATE UNIQUE INDEX REF_CLASS_PK ON REF_CLASS
(
       class_id                       ASC
);

CREATE UNIQUE INDEX REF_CLASS_UK1 ON REF_CLASS
(
       class_nm                       ASC
);

CREATE UNIQUE INDEX REF_COMM_DIRECTION_PK ON REF_COMM_DIRECTION
(
       comm_direction_id              ASC
);

CREATE UNIQUE INDEX REF_COMM_DIRECTION_UK1 ON REF_COMM_DIRECTION
(
       comm_direction_nm              ASC
);

CREATE UNIQUE INDEX REF_CONSUMABLE_RSRC_TYP_PK ON REF_CONSUMABLE_RSRC_TYP
(
       consumable_rsrc_typ_nm         ASC
);

CREATE UNIQUE INDEX REF_CONTACT_STATUS_PK ON REF_CONTACT_STATUS
(
       contact_status_id              ASC
);

CREATE UNIQUE INDEX REF_CONTACT_TYP_PK ON REF_CONTACT_TYP
(
       contact_typ_id                 ASC
);

CREATE UNIQUE INDEX REF_CONTACT_TYP_UK1 ON REF_CONTACT_TYP
(
       contact_typ_nm                 ASC
);

CREATE UNIQUE INDEX REF_CONVERSION_TYP_PK ON REF_CONVERSION_TYP
(
       conversion_typ                 ASC
);

CREATE UNIQUE INDEX REF_DATA_TYP_PK ON REF_DATA_TYP
(
       data_typ_nm                    ASC
);

CREATE UNIQUE INDEX REF_EVENT_PK ON REF_EVENT
(
       event_nm                       ASC
);

CREATE UNIQUE INDEX REF_IP_STATUS_PK ON REF_IP_STATUS
(
       ip_status_nm                   ASC
);

CREATE UNIQUE INDEX REF_IP_SVC_PROVIDER_PK ON REF_IP_SVC_PROVIDER
(
       ip_svc_provider_id             ASC
);

CREATE UNIQUE INDEX REF_IP_SVC_PROVIDER_UK1 ON REF_IP_SVC_PROVIDER
(
       ip_svc_provider_nm             ASC
);

CREATE UNIQUE INDEX REF_LANG_PK ON REF_LANG
(
       lang_cd                        ASC
);

CREATE UNIQUE INDEX REF_LINK_TYP_PK ON REF_LINK_TYP
(
       link_typ_id                    ASC
);

CREATE UNIQUE INDEX REF_LINK_TYP_UK1 ON REF_LINK_TYP
(
       link_typ_nm                    ASC
);

CREATE UNIQUE INDEX REF_LOCALE_PK ON REF_LOCALE
(
       locale_cd                      ASC
);

CREATE INDEX REF_LOCALE_IX2 ON REF_LOCALE
(
       lang_cd                        ASC
);

CREATE INDEX REF_LOCALE_IX1 ON REF_LOCALE
(
       locale_country_cd              ASC
);

CREATE UNIQUE INDEX REF_LOCALE_COUNTRY_PK ON REF_LOCALE_COUNTRY
(
       locale_country_cd              ASC
);

CREATE UNIQUE INDEX REF_LOCK_TYP_PK ON REF_LOCK_TYP
(
       lock_typ                       ASC
);

CREATE UNIQUE INDEX REF_MGMT_MODE_PK ON REF_MGMT_MODE
(
       mgmt_mode_id                   ASC
);

CREATE UNIQUE INDEX REF_MGMT_MODE_UK1 ON REF_MGMT_MODE
(
       mgmt_mode_nm                   ASC
);

CREATE UNIQUE INDEX REF_NTWK_CMPNT_TYP_PK ON REF_NTWK_CMPNT_TYP
(
       ntwk_cmpnt_typ_nm              ASC
);

CREATE UNIQUE INDEX REF_NTWK_ROLE_PK ON REF_NTWK_ROLE
(
       ntwk_role_id                   ASC
);

CREATE UNIQUE INDEX REF_NTWK_ROLE_UK1 ON REF_NTWK_ROLE
(
       ntwk_role_nm                   ASC
);

CREATE UNIQUE INDEX REF_NTWK_TYP_PK ON REF_NTWK_TYP
(
       ntwk_typ_id                    ASC
);

CREATE UNIQUE INDEX REF_NTWK_TYP_UK1 ON REF_NTWK_TYP
(
       ntwk_typ_nm                    ASC
);

CREATE UNIQUE INDEX REF_ORDR_ACTION_PK ON REF_ORDR_ACTION
(
       ordr_action_id                 ASC
);

CREATE UNIQUE INDEX REF_ORDR_ACTION_UK ON REF_ORDR_ACTION
(
       ordr_action_nm                 ASC
);

CREATE UNIQUE INDEX REF_ORDR_ITEM_STATUS_PK ON REF_ORDR_ITEM_STATUS
(
       ordr_item_status_id            ASC
);

CREATE UNIQUE INDEX REF_ORDR_STATUS_PK ON REF_ORDR_STATUS
(
       ordr_status_id                 ASC
);

CREATE UNIQUE INDEX REF_PROJ_STATUS_PK ON REF_PROJ_STATUS
(
       proj_status_nm                 ASC
);


CREATE UNIQUE INDEX REF_REPROV_STATUS_PK ON REF_REPROV_STATUS
(
       reprov_status                  ASC
);

CREATE UNIQUE INDEX REF_RESOURCE_PK ON REF_RESOURCE
(
       resource_id                    ASC
);

CREATE UNIQUE INDEX REF_SCREEN_PK ON REF_SCREEN
(
       screen_nm                      ASC
);

CREATE UNIQUE INDEX REF_SSN_GROUP_PK ON REF_SSN_GROUP
(
       ssn_group_id                   ASC
);

CREATE UNIQUE INDEX STATUS_PK ON REF_STATUS
(
       status_id                      ASC
);


CREATE UNIQUE INDEX REF_STATUS_CHG_REASON_PK ON REF_STATUS_CHG_REASON
(
       reason_id                      ASC
);

CREATE UNIQUE INDEX REF_STATUS_TYP_PK ON REF_STATUS_TYP
(
       status_typ                     ASC
);

CREATE UNIQUE INDEX REF_SUB_STATUS_PK ON REF_SUB_STATUS
(
       sub_status_id                  ASC
);

CREATE UNIQUE INDEX REF_SUB_SVC_STATUS_PK ON REF_SUB_SVC_STATUS
(
       sub_svc_status_id              ASC
);

CREATE UNIQUE INDEX REF_SUB_TYP_PK ON REF_SUB_TYP
(
       sub_typ_id                     ASC
);

CREATE UNIQUE INDEX REF_SUB_TYP_UK1 ON REF_SUB_TYP
(
       sub_typ_nm                     ASC
);

CREATE UNIQUE INDEX REF_SUBNTWK_MDL_PK ON REF_SUBNTWK_MDL
(
       subntwk_typ_id                 ASC,
       subntwk_mdl_id                 ASC
);


CREATE UNIQUE INDEX REF_SUBNTWK_TYP_PK ON REF_SUBNTWK_TYP
(
       subntwk_typ_id                 ASC
);

CREATE UNIQUE INDEX REF_SUBNTWK_TYP_UK1 ON REF_SUBNTWK_TYP
(
       subntwk_typ_nm                 ASC
);

CREATE INDEX REF_SUBNTWK_TYP_IX1 ON REF_SUBNTWK_TYP
(
       ntwk_typ_id                    ASC
);

CREATE UNIQUE INDEX REF_SVC_ACTION_TYP_PK ON REF_SVC_ACTION_TYP
(
       svc_action_typ                 ASC
);

CREATE UNIQUE INDEX REF_SVC_ADVISORY_STATUS_PK ON REF_SVC_ADVISORY_STATUS
(
       svc_advisory_status_nm         ASC
);

CREATE UNIQUE INDEX REF_SVC_DELIVERY_PLAT_PK ON REF_SVC_DELIVERY_PLAT
(
       svc_delivery_plat_id           ASC
);

CREATE UNIQUE INDEX REF_SVC_DELIVERY_PLAT_UK1 ON REF_SVC_DELIVERY_PLAT
(
       svc_delivery_plat_nm           ASC
);


CREATE UNIQUE INDEX REF_TASK_STATUS_PK ON REF_TASK_STATUS
(
       task_status_nm                 ASC
);

CREATE UNIQUE INDEX REF_TICKET_STATUS_PK ON REF_TICKET_STATUS
(
       ticket_status_nm               ASC
);

CREATE UNIQUE INDEX REF_TICKET_TYP_PK ON REF_TICKET_TYP
(
       ticket_nm                      ASC
);

CREATE UNIQUE INDEX REF_TOPOLOGY_ACTION_TYP_PK ON REF_TOPOLOGY_ACTION_TYP
(
       topology_action_typ            ASC
);

CREATE UNIQUE INDEX REF_TOPOLOGY_STATUS_PK ON REF_TOPOLOGY_STATUS
(
       topology_status_id             ASC
);

CREATE UNIQUE INDEX REF_TOPOLOGY_STATUS_UK1 ON REF_TOPOLOGY_STATUS
(
       topology_status_nm             ASC
);

CREATE UNIQUE INDEX RESOURCE_ASSIGN_PK ON RESOURCE_ASSIGN
(
       resource_id                    ASC,
       subntwk_id                     ASC
);

CREATE INDEX RESOURCE_ASSIGN_IDX ON RESOURCE_ASSIGN 
(
       subntwk_id  		ASC
);

CREATE INDEX TECH_PLATFORM_MGMT_MODE_IX1 ON TECH_PLATFORM_MGMT_MODE
(
       mgmt_mode_id                   ASC,
       tech_platform_id               ASC
);


CREATE UNIQUE INDEX SBNT_TECH_PLATFORM_PK ON SBNT_TECH_PLATFORM
(
       subntwk_id                     ASC,
       tech_platform_id               ASC,
       mgmt_mode_id                   ASC
);

CREATE INDEX SBNT_TECH_PLATFORM_IX1 ON SBNT_TECH_PLATFORM
(
       tech_platform_id               ,
       mgmt_mode_id                   
);

CREATE UNIQUE INDEX SERVER_NODE_PK ON SERVER_NODE
(
       node_id                        ASC
);

CREATE UNIQUE INDEX SERVER_NODE_UK ON SERVER_NODE
(
       node_nm                        ASC
);

CREATE UNIQUE INDEX SRC_ACTION_PARM_PK ON SRC_ACTION_PARM
(
       action_tmplt_parm_id           ASC,
       scope_proj_tmplt_nm            ASC,
       scope_proj_tmplt_ver           ASC,
       scope_task_tmplt_nm            ASC,
       scope_task_tmplt_ver           ASC,
       scope_action_execution_seq     ASC
);

CREATE UNIQUE INDEX STATUS_CHG_ACTIONS_PK ON STATE_ACTION
(
       state_action_id                ASC
);

CREATE UNIQUE INDEX STATE_TRANSITION_PK ON STATE_TRANSITION
(
       state_transition_id            ASC
);

CREATE UNIQUE INDEX STATE_TRANSITION_EVENT_PK ON STATE_TRANSITION_EVENT
(
       state_transition_id            ASC,
       target_class_id                ASC,
       event_nm                       ASC
);

CREATE UNIQUE INDEX STM_BATCH_REPLY_INFO_PK ON STM_BATCH_REPLY_INFO
(
       sub_ordr_id                    ASC
);

CREATE UNIQUE INDEX STRUC_HIERARCHY_PK ON STRUC_HIERARCHY
(
       struc_hierarchy_id             ASC
);

CREATE INDEX STRUC_HIERARCHY_IX3 ON STRUC_HIERARCHY
(
       child_location_category_id     ASC
);

CREATE INDEX STRUC_HIERARCHY_IX2 ON STRUC_HIERARCHY
(
       parent_location_category_id    ASC
);

CREATE INDEX STRUC_HIERARCHY_IX1 ON STRUC_HIERARCHY
(
       location_struc_id              ASC
);

CREATE UNIQUE INDEX SUB_PK ON SUB
(
       sub_id                         ASC
);

CREATE INDEX SUB_IX4 ON SUB
(
       sub_typ_id                     ASC
);

CREATE INDEX SUB_IX2 ON SUB
(
       svc_provider_id                ASC
);

CREATE INDEX SUB_IX1 ON SUB
(
       parent_sub_id                  ASC
);

CREATE INDEX SUB_IX5 ON SUB
(
       external_key                   ASC
);

CREATE UNIQUE INDEX SUB_ADDR_PK ON SUB_ADDR
(
       sub_addr_id                    ASC
);

CREATE INDEX SUB_ADDR_IX3 ON SUB_ADDR
(
       location_id                    ASC
);

CREATE INDEX SUB_ADDR_IX2 ON SUB_ADDR
(
       addr_typ_id                    ASC
);

CREATE INDEX SUB_ADDR_IX1 ON SUB_ADDR
(
       sub_id                         ASC
);

CREATE UNIQUE INDEX SUB_CONTACT_PK ON SUB_CONTACT
(
       sub_contact_id                 ASC
);

CREATE INDEX SUB_CONTACT_IX3 ON SUB_CONTACT
(
       location_id                 ASC
);

CREATE INDEX SUB_CONTACT_IX1 ON SUB_CONTACT
(
       sub_id                         ASC
);

CREATE UNIQUE INDEX SUB_CONTACT_PARM_PK ON SUB_CONTACT_PARM
(
       sub_contact_id                 ASC,
       parm_id                        ASC
);

CREATE INDEX SUB_CONTACT_PARM_IX1 ON SUB_CONTACT_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE INDEX SUB_CONTACT_PARM_IX2 ON SUB_CONTACT_PARM
(
       upper(val)
);

CREATE INDEX SUB_OBJ_ENQ_IX4 ON SUB_OBJ_ENQ
(
       lock_id                        ASC
);

CREATE UNIQUE INDEX SUB_OBJ_ENQ_UK1 ON SUB_OBJ_ENQ
(
       obj_id                         ASC,
       obj_class_id                   ASC,
       sub_ordr_id                    ASC,
       sub_ordr_item_id               ASC
);

CREATE INDEX SUB_OBJ_ENQ_IX1 ON SUB_OBJ_ENQ
(
       sub_ordr_item_id               ASC
);

CREATE INDEX SUB_OBJ_ENQ_IX2 ON SUB_OBJ_ENQ
(
       sub_ordr_id                    ASC
);

CREATE INDEX SUB_OBJ_ENQ_IX3 ON SUB_OBJ_ENQ
(
       sub_id                         ASC
);

CREATE UNIQUE INDEX SUB_ORDR_PK ON SUB_ORDR
(
       sub_ordr_id                    ASC
);

CREATE UNIQUE INDEX SUB_ORDR_IX1 ON SUB_ORDR
(
       sub_id                         ASC,
       sub_ordr_id                    ASC
);

CREATE INDEX SUB_ORDR_IX2 ON SUB_ORDR
(
       external_key                   ASC
);

CREATE INDEX SUB_ORDR_IX3 ON SUB_ORDR
(
       created_dtm                   ASC
);

CREATE INDEX SUB_ORDR_IX4 ON SUB_ORDR
(
       ordr_status_id                   ASC
);

CREATE UNIQUE INDEX SUB_ORDR_ITEM_PK ON SUB_ORDR_ITEM
(
       sub_ordr_item_id               ASC
);

CREATE INDEX SUB_ORDR_ITEM_IX1 ON SUB_ORDR_ITEM
(
       sub_ordr_id                    ASC
);

CREATE INDEX SUB_ORDR_ITEM_IX2 ON SUB_ORDR_ITEM
(
       sub_entity_id                    ASC,
       OBJECT_TYP_id                    ASC
);


CREATE UNIQUE INDEX SUB_ORDR_ITEM_PARM_PK ON SUB_ORDR_ITEM_PARM
(
       sub_ordr_item_id               ASC,
       parm_id                        ASC
);

CREATE INDEX SUB_ORDR_ITEM_PARM_IX1 ON SUB_ORDR_ITEM_PARM
(
       parm_id                        ASC
);

CREATE UNIQUE INDEX SUB_ORDR_PARM_PK ON SUB_ORDR_PARM
(
       sub_ordr_id                    ASC,
       parm_id                        ASC
);

CREATE INDEX SUB_ORDR_PARM_IX2 ON SUB_ORDR_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE UNIQUE INDEX SUB_PARM_PK ON SUB_PARM
(
       sub_id                         ASC,
       parm_id                        ASC
);

CREATE INDEX SUB_PARM_IX1 ON SUB_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE INDEX SUB_PARM_IX2 ON SUB_PARM
(
       upper(val) 
);

CREATE UNIQUE INDEX SUB_SVC_PK ON SUB_SVC
(
       sub_svc_id                     ASC
);

CREATE INDEX SUB_SVC_IX4 ON SUB_SVC
(
       parent_sub_svc_id              ASC,
       sub_id			      ASC,
       svc_id                         ASC
);

CREATE INDEX SUB_SVC_IX5 ON SUB_SVC
(
       external_key                   ASC
);

CREATE INDEX SUB_SVC_IX6 ON SUB_SVC
(
       sub_id                         ASC,
       svc_id                         
);

CREATE UNIQUE INDEX SUB_SVC_ADDR_PK ON SUB_SVC_ADDR
(
       sub_svc_id                     ASC,
       sub_addr_id                    ASC
);

CREATE INDEX SUB_SVC_ADDR_IX1 ON SUB_SVC_ADDR
(
       sub_addr_id                    ASC
);

CREATE UNIQUE INDEX SUB_SVC_DELIVERY_PLAT_PK ON SUB_SVC_DELIVERY_PLAT
(
       sub_svc_id                     ASC
);

CREATE INDEX SUB_SVC_DELIVERY_PLAT_IX1 ON SUB_SVC_DELIVERY_PLAT
(
       svc_delivery_plat_id           ASC
);

CREATE UNIQUE INDEX SUB_SVC_DEPY_PK ON SUB_SVC_DEPY
(
       prerequisite_sub_svc_id        ASC,
       dependent_sub_svc_id           ASC
);

CREATE INDEX SUB_SVC_DEPY_IX1 ON SUB_SVC_DEPY
(
       dependent_sub_svc_id           ASC
);

CREATE UNIQUE INDEX SUB_SVC_NTWK_PK ON SUB_SVC_NTWK
(
       sub_svc_id                     ASC,
       subntwk_id                     ASC
);

CREATE INDEX SUB_SVC_NTWK_IX1 ON SUB_SVC_NTWK
(
       subntwk_id                     ASC
);

CREATE UNIQUE INDEX SUB_SVC_NTWK_STAGE_PK ON SUB_SVC_NTWK_STAGE
(
       sub_ordr_item_id               ASC,
       sub_svc_id                     ASC,
       subntwk_id                     ASC
);

CREATE INDEX SUB_SVC_NTWK_STAGE_IX1 ON SUB_SVC_NTWK_STAGE
(
       sub_svc_id                     ASC,
       sub_ordr_item_id               ASC
);

CREATE UNIQUE INDEX SUB_SVC_PARM_PK ON SUB_SVC_PARM
(
       sub_svc_id                     ASC,
       parm_id                        ASC
);

CREATE INDEX SUB_SVC_PARM_IX2 ON SUB_SVC_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE UNIQUE INDEX SUB_SVC_PARM_DEPY_PK ON SUB_SVC_PARM_DEPY
(
       depy_sub_svc_id                ASC,
       depy_parm_id                   ASC
);

CREATE INDEX SUB_SVC_PARM_DEPY_IX1 ON SUB_SVC_PARM_DEPY
(
       prereq_sub_svc_id              ASC
);

CREATE INDEX SUB_SVC_PARM_DEPY_IX2 ON SUB_SVC_PARM_DEPY
(
       sub_id                         ASC
);

CREATE UNIQUE INDEX SUB_SVC_PLAT_STAGE_PK ON SUB_SVC_PLAT_STAGE
(
       sub_svc_id                     ASC,
       sub_ordr_item_id               ASC
);

CREATE INDEX SUB_SVC_PLAT_STAGE_IX1 ON SUB_SVC_PLAT_STAGE
(
       sub_ordr_item_id               ASC
);

CREATE UNIQUE INDEX SUBNTWK_PK ON SUBNTWK
(
       subntwk_id                     ASC
);

CREATE UNIQUE INDEX SUBNTWK_UK1 ON SUBNTWK
(
       subntwk_nm                     ASC,
       subntwk_typ_id                 ASC,
       parent_subntwk_id              ASC
);

CREATE INDEX SUBNTWK_IX3 ON SUBNTWK
(
       topology_status_id             ASC
);

CREATE INDEX SUBNTWK_IX1 ON SUBNTWK
(
       parent_subntwk_id              ASC
);

CREATE INDEX SUBNTWK_IX2 ON SUBNTWK
(
       subntwk_typ_id                 ASC
);


CREATE INDEX SUBNTWK_IX4 ON SUBNTWK
(
       upper_subntwk_nm             ASC
);

CREATE INDEX SUBNTWK_FUNC_IX1 ON SUBNTWK 
(
   nvl (parent_subntwk_id, -9999)
);

CREATE UNIQUE INDEX SUBNTWK_CARD_LMT_PK ON SUBNTWK_CARD_LMT
(
       card_typ_id                    ASC,
       card_mdl_id                    ASC,
       subntwk_typ_id                 ASC,
       subntwk_mdl_id                 ASC
);

CREATE INDEX SUBNTWK_CARD_LMT_IX2 ON SUBNTWK_CARD_LMT
(
       subntwk_typ_id                 ASC,
       subntwk_mdl_id                 ASC
);

CREATE UNIQUE INDEX SUBNTWK_COVERAGE_PK ON SUBNTWK_COVERAGE
(
       subntwk_coverage_id            ASC
);

CREATE INDEX SUBNTWK_COVERAGE_IX3 ON SUBNTWK_COVERAGE
(
       ntwk_location_id               ASC
);

CREATE INDEX SUBNTWK_COVERAGE_IX2 ON SUBNTWK_COVERAGE
(
       location_struc_id              ASC
);

CREATE INDEX SUBNTWK_COVERAGE_IX1 ON SUBNTWK_COVERAGE
(
       subntwk_id                     ASC
);

CREATE UNIQUE INDEX SUBNTWK_MDL_LMT_PK ON SUBNTWK_MDL_LMT
(
       subntwk_typ_id                 ASC,
       subntwk_mdl_id                 ASC,
       parent_subntwk_typ_id          ASC,
       parent_subntwk_mdl_id          ASC
);

CREATE INDEX SUBNTWK_MDL_LMT_IX3 ON SUBNTWK_MDL_LMT
(
       subntwk_typ_id                 ASC,
       parent_subntwk_typ_id          ASC
);

CREATE INDEX SUBNTWK_MDL_LMT_IX2 ON SUBNTWK_MDL_LMT
(
       parent_subntwk_typ_id          ASC,
       parent_subntwk_mdl_id          ASC
);

CREATE UNIQUE INDEX SUBNTWK_MDL_PARM_PK ON SUBNTWK_MDL_PARM
(
       subntwk_typ_id                 ASC,
       subntwk_mdl_id                 ASC,
       parm_id                        ASC
);

CREATE INDEX SUBNTWK_MDL_PARM_IX2 ON SUBNTWK_MDL_PARM
(
       parm_id                        ASC
);

CREATE UNIQUE INDEX SUBNTWK_PARM_PK ON SUBNTWK_PARM
(
       subntwk_id                     ASC,
       parm_id                        ASC
);

CREATE INDEX SUBNTWK_PARM_IX1 ON SUBNTWK_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE UNIQUE INDEX SUBNTWK_PARM_STAGE_PK ON SUBNTWK_PARM_STAGE
(
       sub_ordr_item_id               ASC,
       parm_id                        ASC,
       sub_svc_id                     ASC,
       subntwk_id                     ASC
);


CREATE UNIQUE INDEX SUBNTWK_TYP_FREQ_PK ON SUBNTWK_TYP_FREQ
(
       freq_id                        ASC,
       subntwk_typ_id                 ASC
);

CREATE UNIQUE INDEX SUBNTWK_TYP_FREQ_GRP_PK ON SUBNTWK_TYP_FREQ_GRP
(
       freq_grp_id                    ASC,
       subntwk_typ_id                 ASC
);

CREATE UNIQUE INDEX SVC_PK ON SVC
(
       svc_id                         ASC
);

CREATE INDEX SVC_IX1 ON SVC
(
       loading_policy                 ASC,
       svc_id                         ASC
);

CREATE UNIQUE INDEX SVC_IX5 ON SVC
(
       svc_nm                         ASC,
       svc_id                         ASC
);

CREATE UNIQUE INDEX SVC_ACTION_PK ON SVC_ACTION
(
       svc_action_id                  ASC
);

CREATE INDEX SVC_ACTION_IX6 ON SVC_ACTION
(
       base_svc_action_id             ASC
);

CREATE INDEX SVC_ACTION_IX4 ON SVC_ACTION
(
       svc_action_typ                 ASC
);

CREATE UNIQUE INDEX SVC_ACTION_UK1 ON SVC_ACTION
(
       SVC_ID			      ASC, 
       SVC_ACTION_NM                  ASC
);


CREATE UNIQUE INDEX SVC_ADVISORY_PK ON SVC_ADVISORY
(
       svc_advisory_id                ASC
);

CREATE INDEX SVC_ADVISORY_IX4 ON SVC_ADVISORY
(
       link_id                        ASC
);

CREATE INDEX SVC_ADVISORY_IX3 ON SVC_ADVISORY
(
       svc_advisory_status_nm         ASC
);

CREATE INDEX SVC_ADVISORY_IX2 ON SVC_ADVISORY
(
       subntwk_id                     ASC
);

CREATE INDEX SVC_ADVISORY_IX1 ON SVC_ADVISORY
(
       subntwk_typ_id                 ASC
);

CREATE UNIQUE INDEX SVC_ADVISORY_PARM_PK ON SVC_ADVISORY_PARM
(
       svc_advisory_id                ASC,
       parm_id                        ASC
);


CREATE UNIQUE INDEX SVC_PROVIDER_PK ON SVC_PROVIDER
(
       svc_provider_id                ASC
);

CREATE UNIQUE INDEX SVC_PROVIDER_UK1 ON SVC_PROVIDER
(
       svc_provider_nm                ASC
);

CREATE UNIQUE INDEX SVC_SUPPORTED_PLATFORM_PK ON SVC_SUPPORTED_PLATFORM
(
       svc_supported_platform_id      ASC
);

CREATE INDEX SVC_SUPPORTED_PLATFORM_IX1 ON SVC_SUPPORTED_PLATFORM
(
       svc_id                         ASC
);

CREATE INDEX SVC_SUPPORTED_PLATFORM_IX2 ON SVC_SUPPORTED_PLATFORM
(
       svc_delivery_plat_id           ASC
);

CREATE INDEX SVC_SUPPORTED_PLATFORM_IX3 ON SVC_SUPPORTED_PLATFORM
(
       ssn_group_id                   ASC
);

CREATE UNIQUE INDEX SYNC_ACTION_PARMS_TMP_PK ON SYNC_ACTION_PARMS_TMP
(
       sync_action_parm_id            ASC,
       parm_nm                        ASC
);

CREATE UNIQUE INDEX SYNC_DEBUG_PK ON SYNC_DEBUG
(
       sync_debug_seq                 ASC
);

CREATE UNIQUE INDEX SYNC_EXT_PROC_PARMS_PK ON SYNC_EXT_PROC_PARMS
(
       ext_proc_nm                    ASC,
       ext_proc_parm_nm               ASC
);

CREATE UNIQUE INDEX SYNC_EXT_PROCS_PK ON SYNC_EXT_PROCS
(
       ext_proc_nm                    ASC
);

CREATE UNIQUE INDEX SYNC_GRP_ACTION_PARMS_PK ON SYNC_GRP_ACTION_PARMS
(
       grp_nm                         ASC,
       action_seq_within_grp          ASC,
       action                         ASC,
       parm_nm                        ASC
);

CREATE UNIQUE INDEX SYNC_ACTION_GRP_LIST_PK ON SYNC_GRP_ACTIONS
(
       grp_nm                         ASC,
       action_seq_within_grp          ASC,
       action                         ASC
);

CREATE UNIQUE INDEX SYNC_ACTION_EXT_PROC_COND_PK ON SYNC_GRP_EXT_PROC_COND
(
       ext_proc_nm                    ASC,
       check_parm_nm                  ASC,
       grp_nm                         ASC,
       action_seq_within_grp          ASC,
       action                         ASC
);

CREATE UNIQUE INDEX SYNC_ACTION_EXT_PROCS_PK ON SYNC_GRP_EXT_PROCS
(
       grp_nm                         ASC,
       ext_proc_nm                    ASC
);

CREATE UNIQUE INDEX SYNC_GRP_QUALIFIERS_PK ON SYNC_GRP_QUALIFIERS
(
       proj_tmplt_impl_ver            ASC,
       proj_tmplt_impl_nm             ASC,
       grp_nm                         ASC
);

CREATE UNIQUE INDEX SYNC_GRPS_PK ON SYNC_GRPS
(
       grp_nm                         ASC
);

CREATE UNIQUE INDEX SYNC_SRC_EXT_PARMS_PK ON SYNC_SRC_EXT_PARMS
(
       ext_proc_nm                    ASC,
       ext_proc_parm_nm               ASC,
       scope_grp                      ASC
);

CREATE UNIQUE INDEX TASK_PK ON TASK
(
       task_id                        ASC
);

CREATE UNIQUE INDEX TASK_UK1 ON TASK
(
       proj_id                        ASC,
       task_nm                        ASC
);

CREATE INDEX TASK_IX2 ON TASK
(
       proj_id                        ASC
);

CREATE INDEX TASK_IX1 ON TASK
(
       task_status_nm                 ASC
);

CREATE UNIQUE INDEX TASK_ACTION_PK ON TASK_ACTION
(
       task_action_id                 ASC
);

CREATE INDEX TASK_ACTION_IX1 ON TASK_ACTION
(
       task_id                        ASC
);

CREATE INDEX TASK_ACTION_IX2 ON TASK_ACTION
(
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC,
       action_tmplt_execution_seq     ASC
);

CREATE INDEX TASK_ACTION_IX3 ON TASK_ACTION
(
       parent_task_action_id          ASC
);

CREATE UNIQUE INDEX TASK_ACTION_PARM_PK ON TASK_ACTION_PARM
(
       task_action_parm_id            ASC
);

CREATE INDEX TASK_ACTION_PARM_IX1 ON TASK_ACTION_PARM
(
       ntwk_cmpnt_typ_nm              ASC
);

CREATE INDEX TASK_ACTION_PARM_IX2 ON TASK_ACTION_PARM
(
       parm_id                        ASC
);

CREATE INDEX TASK_ACTION_PARM_IX3 ON TASK_ACTION_PARM
(
       task_action_id                 ASC
);

CREATE UNIQUE INDEX TASK_PARM_PK ON TASK_PARM
(
       task_id                        ASC,
       parm_id                        ASC
);

CREATE INDEX TASK_PARM_IX1 ON TASK_PARM
(
       parm_id                        ASC,
       val                            ASC
);

CREATE UNIQUE INDEX TASK_TMPLT_PK ON TASK_TMPLT
(
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC
);

CREATE UNIQUE INDEX TASK_TMPLT_DEPY_PK ON TASK_TMPLT_DEPY
(
       proj_tmplt_nm                  ASC,
       proj_tmplt_ver                 ASC,
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC,
       dependent_task_tmplt_nm        ASC,
       dependent_task_tmplt_ver       ASC
);

CREATE UNIQUE INDEX TASK_TMPLT_PARM_PK ON TASK_TMPLT_PARM
(
       task_tmplt_parm_id             ASC
);

CREATE INDEX TASK_TMPLT_PARM_IX1 ON TASK_TMPLT_PARM
(
       parm_id                        ASC
);

CREATE INDEX TASK_TMPLT_PARM_IX2 ON TASK_TMPLT_PARM
(
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC
);

CREATE UNIQUE INDEX TASK_TRANSITIONS_PK ON TASK_TRANSITIONS
(
       from_task_status_nm            ASC,
       to_task_status_nm              ASC
);

CREATE INDEX TASK_TRANSITIONS_IX2 ON TASK_TRANSITIONS
(
       to_task_status_nm              ASC
);

CREATE UNIQUE INDEX TASKS_IN_PROJ_TMPLT_PK ON TASKS_IN_PROJ_TMPLT
(
       proj_tmplt_nm                  ASC,
       proj_tmplt_ver                 ASC,
       task_tmplt_nm                  ASC,
       task_tmplt_ver                 ASC
);

CREATE UNIQUE INDEX TECH_PLATFORM_PK ON TECH_PLATFORM
(
       tech_platform_id               ASC
);

CREATE UNIQUE INDEX TECH_PLATFORM_UK1 ON TECH_PLATFORM
(
       techn_platform_nm              ASC
);

CREATE INDEX TECH_PLATFORM_IX1 ON TECH_PLATFORM
(
       tech_platform_typ_id           ASC
);

CREATE UNIQUE INDEX TECH_PLATFORM_PARM_PK ON TECH_PLATFORM_PARM
(
       tech_platform_id               ASC,
       parm_id                        ASC
);

CREATE UNIQUE INDEX TECH_PLATFORM_TYP_PK ON TECH_PLATFORM_TYP
(
       tech_platform_typ_id           ASC
);

CREATE UNIQUE INDEX TECH_PLATFORM_TYP_IX1 ON TECH_PLATFORM_TYP
(
       tech_platform_typ_nm           ASC
);

CREATE UNIQUE INDEX TROUBLE_TICKET_PK ON TROUBLE_TICKET
(
       ticket_id                      ASC
);

CREATE INDEX TROUBLE_TICKET_IX1 ON TROUBLE_TICKET
(
       ticket_status_nm               ASC
);

CREATE UNIQUE INDEX TROUBLE_TICKET_PARM_PK ON TROUBLE_TICKET_PARM
(
       ticket_id                      ASC,
       parm_id                        ASC
);

CREATE UNIQUE INDEX TROUBLE_TICKET_PARM_IX2 ON TROUBLE_TICKET_PARM
(
       parm_id                        ASC,
       val                            ASC,
       ticket_id                      ASC
);


CREATE UNIQUE INDEX VERSION_PK ON VERSION
(
       module_name                    ASC
);

CREATE UNIQUE INDEX VLD_RULE_PK ON VLD_RULE
(
       vld_rule_id                    ASC
);

CREATE UNIQUE INDEX VLD_RULE_UK1 ON VLD_RULE
(
       vld_rule_nm                    ASC
);

CREATE INDEX WRK_ARCH_REC_IX1 ON WRK_ARCH_REC
(
       tbl_nm                         ASC,
       rid                            ASC
);

CREATE INDEX WRK_ARCH_SESSION_IX1 ON WRK_ARCH_SESSION
(
       tbl_nm                         ,
       rid                            
);



CREATE UNIQUE INDEX WRK_UNIQUE_PARM_UK1 ON WRK_UNIQUE_PARM
(
       grp_nm_or_parm_id              ASC,
       val                            ASC
);

CREATE INDEX WRK_UNIQUE_PARM_IX1 on WRK_UNIQUE_PARM
(
       sub_ordr_id                  ASC,
       sub_entity_id                ASC,
       parm_id                      ASC
);


CREATE UNIQUE INDEX XML_REPOSITORY_PK ON XML_REPOSITORY
(
       doc_nm                         ASC
);


CREATE UNIQUE INDEX WRK_LOCK_UK1 ON WRK_LOCK
(
       lock_nm       ASC,
       val                            ASC
);

CREATE UNIQUE INDEX REF_ORDR_TYP_PK ON REF_ORDR_TYP
(
       ordr_typ                       ASC
);


CREATE UNIQUE INDEX ORDR_ITEM_ASSOC_CHG_UK1 ON ORDR_ITEM_ASSOC_CHG
(
       sub_ordr_item_id               ASC,
       assoc_typ_id                   ASC,
       assoc_object_typ_id            ASC,
       assoc_sub_entity_id            ASC,
       parm_id                        ASC 
);

CREATE UNIQUE INDEX REF_ASSOC_TYP_PK ON REF_ASSOC_TYP
(
       assoc_typ_id                   ASC
);

CREATE UNIQUE INDEX REF_OBJECT_TYP_PK ON REF_OBJECT_TYP
(
       object_typ_id                  ASC
);

CREATE UNIQUE INDEX REF_ASSOC_TYP_UK1 ON REF_ASSOC_TYP
(
       assoc_typ_nm                   
);

CREATE UNIQUE INDEX REF_OBJECT_TYP_UK1 ON REF_OBJECT_TYP
(
       class_id                       ASC,
       object_nm                  
);


CREATE UNIQUE INDEX REF_USER_STATUS_PK ON REF_USER_STATUS
(
       user_status_id                 ASC
);


CREATE UNIQUE INDEX SUB_USER_PK ON SUB_USER
(
       sub_user_id                    ASC
);

CREATE UNIQUE INDEX SUB_USER_UK1 ON SUB_USER
(
       sub_id                        ASC,
       sub_user_id                   ASC
);



CREATE UNIQUE INDEX SUB_USER_PARM_PK ON SUB_USER_PARM
(
       sub_user_id                    ASC,
       parm_id                        ASC
);

CREATE UNIQUE INDEX SUB_USER_SVC_PK ON SUB_USER_SVC
(
       sub_user_id                    ASC,
       sub_svc_id                     ASC,
       assoc_rule_id                   ASC
);
CREATE INDEX SUB_USER_SVC_IX1 ON SUB_USER_SVC
(
       sub_svc_id     ASC
);

CREATE INDEX SUB_SVC_ASSOC_IX1 ON SUB_SVC_ASSOC
(
       assoc_sub_svc_id               ASC
);
CREATE UNIQUE INDEX SUB_SVC_ASSOC_PK ON SUB_SVC_ASSOC
(
       sub_svc_id                     ASC,
       assoc_rule_id                   ASC,
       assoc_sub_svc_id                ASC
);

CREATE INDEX ORDR_NOTIFY_ITEM_IX1 ON ORDR_NOTIFY_ITEM
(
       sub_ordr_id                     ASC
);

CREATE INDEX ORDR_NOTIFY_ITEM_IX2 ON ORDR_NOTIFY_ITEM
(
       sub_ordr_item_id                ASC
);

CREATE UNIQUE INDEX WSNOTIFY_REG_UX1 ON WSNOTIFY_REG
(
       id                         ASC, 
       topic                      ASC
);

CREATE INDEX REF_ASSOC_RULE_IX1 ON REF_ASSOC_RULE
(      assoc_rule_id               ASC, 
       aend_object_typ_id         ASC, 
       zend_loading_policy        ASC
);

CREATE UNIQUE INDEX REF_ASSOC_RULE_PK ON REF_ASSOC_RULE
(
       assoc_rule_id              ASC
);

CREATE UNIQUE INDEX REF_ASSOC_RULE_UK1 ON REF_ASSOC_RULE
(
        assoc_rule_name           ASC
);

CREATE UNIQUE INDEX REF_SSN_GROUP_U01 ON REF_SSN_GROUP
(
        ssn_group_nm              ASC
);

CREATE UNIQUE INDEX SVC_SUPPORTED_PLATFORM_U02 ON SVC_SUPPORTED_PLATFORM
(
        svc_id                   ASC,
        svc_delivery_plat_id     ASC,
        ssn_group_id             ASC
);
