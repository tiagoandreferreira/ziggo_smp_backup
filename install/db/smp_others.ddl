
CREATE SEQUENCE SUB_USER_SEQ
INCREMENT BY 1 
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;

CREATE SEQUENCE ACTION_TMPLT_PARM_SEQ 
START WITH 20000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE ARCH_GRP_PART_SEQ 
INCREMENT BY 1 
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE ARCH_GRP_SEQ 
INCREMENT BY 1 
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE ARCH_PARM_HIST_SEQ 
INCREMENT BY 1 
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE ARCH_RUN_SEQ 
INCREMENT BY 1 
START WITH 1 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE CARD_MDL_SEQ 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE CARD_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE DM_CALL_ID_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE FREQ_GRP_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE FREQ_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE IP_ADDR_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE IP_BLOCK_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE IP_SBNT_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE LINK_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE LOCATION_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE MIG_BATCH_SEQ 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE MIG_ERR_SEQ 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE MIG_SEQ 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;

CREATE SEQUENCE NTWK_RESOURCE_SEQ
START WITH 1
NOMAXVALUE
NOMINVALUE
NOCYCLE
NOORDER
;

CREATE SEQUENCE PARM_INSTANCE_DEPY_RULE_SEQ 
START WITH 20000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE PARM_SEQ 
START WITH 80000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE PORT_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE PROJ_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE PROJ_TMPLT_PARM_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE REF_CLASS_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE REF_CONTACT_TYP_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE REF_SUBNTWK_MDL_SEQ 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SAMP_VER_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SERVER_NODE_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE STATE_TRANSITION_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_ADDR_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_CONTACT_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
CACHE 20 NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_OBJ_ENQ_SEQ 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_ORDR_ITEM_SEQ 
INCREMENT BY 100 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
CACHE 20 NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_ORDR_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
CACHE 20 NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_REQUEST_QUEUE_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
CACHE 20 NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_SVC_ALIAS_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_SVC_PARM_SEQ 
INCREMENT BY 100 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
CACHE 20 NOCYCLE
NOORDER
;
CREATE SEQUENCE SUB_SVC_SEQ 
INCREMENT BY 1 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
CACHE 20 NOCYCLE
NOORDER
;

CREATE SEQUENCE SUBNTWK_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SVC_ACTION_SEQ 
START WITH 20000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SVC_AVAIL_CONSTR_SEQ 
START WITH 20000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE SVC_SEQ 
START WITH 20000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE sync_debug_seq 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TASK_ACTION_PARM_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TASK_ACTION_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TASK_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TASK_TMPLT_PARM_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TECH_PLATFORM_TYP_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TECH_PLATFORM_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE TMPLT_SEQ 
START WITH 10000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE VLD_RULE_SEQ 
START WITH 20000 
NOMAXVALUE 
NOMINVALUE 
NOCYCLE
NOORDER
;
CREATE SEQUENCE WSNOTIFY_REG_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER
;

ALTER TABLE REF_ASSOC_TYP
       ADD  ( PRIMARY KEY (assoc_typ_id) ) ;
       
ALTER TABLE REF_ASSOC_RULE 
       ADD  ( PRIMARY KEY (assoc_rule_id));

ALTER TABLE REF_OBJECT_TYP
       ADD  ( PRIMARY KEY (object_typ_id) ) ;

ALTER TABLE REF_ORDR_TYP
       ADD  ( CONSTRAINT REF_ORDR_TYP_PK PRIMARY KEY (
              ordr_typ) ) ;

ALTER TABLE TECH_PLATFORM_MGMT_MODE
       ADD  ( CONSTRAINT TECH_PLATFORM_MGMT_MODE_PK PRIMARY KEY (
              tech_platform_id, mgmt_mode_id) ) ;

ALTER TABLE ACTION_TMPLT
       ADD  ( CONSTRAINT ACTION_TMPLT_PK PRIMARY KEY (action_tmplt_nm, 
              action_tmplt_ver) ) ;


ALTER TABLE ACTION_TMPLT_PARM
       ADD  ( CONSTRAINT ACTION_TMPLT_PARM_PK PRIMARY KEY (
              action_tmplt_parm_id) ) ;


ALTER TABLE ACTIONS_IN_TASK_TMPLT
       ADD  ( CONSTRAINT ACTIONS_IN_TASK_TMPLT_PK PRIMARY KEY (
              task_tmplt_nm, task_tmplt_ver, execution_seq) ) ;


ALTER TABLE ARCH_ADM
       ADD  ( CONSTRAINT ARCH_ADM_PK PRIMARY KEY (adm_nm) ) ;


ALTER TABLE ARCH_GRP
       ADD  ( CONSTRAINT ARCH_GRP_PK PRIMARY KEY (grp_id) ) ;


ALTER TABLE ARCH_GRP_PARM
       ADD  ( CONSTRAINT ARCH_GRP_PARM_PK PRIMARY KEY (grp_id, 
              parm_nm) ) ;


ALTER TABLE ARCH_GRP_PART
       ADD  ( CONSTRAINT ARCH_GRP_PART_PK PRIMARY KEY (grp_id, 
              part_id) ) ;


ALTER TABLE ARCH_PARM_HIST
       ADD  ( CONSTRAINT ARCH_PARM_HIST_PK PRIMARY KEY (modified_dtm, 
              mod_tbl_nm, parm_hist_id) ) ;


ALTER TABLE ARCH_RUN
       ADD  ( CONSTRAINT ARCH_RUN_PK PRIMARY KEY (grp_id, run_id) ) ;


ALTER TABLE ARCH_STACK
       ADD  ( CONSTRAINT ARCH_STACK_PK PRIMARY KEY (ref_role) ) ;


ALTER TABLE ARCH_TBL
       ADD  ( CONSTRAINT ARCH_TBL_PK PRIMARY KEY (grp_id, tbl_nm) ) ;


ALTER TABLE ARCH_TBL_LOG
       ADD  ( CONSTRAINT ARCH_TBL_LOG_PK PRIMARY KEY (grp_id, run_id, 
              tbl_nm, part_id) ) ;


ALTER TABLE ARCH_TBL_NET
       ADD  ( CONSTRAINT ARCH_TBL_NET_PK PRIMARY KEY (grp_id, 
              src_tbl_nm, tgt_tbl_nm, ref_role) ) ;


ALTER TABLE ARCH_TBL_PART
       ADD  ( CONSTRAINT ARCH_TBL_PART_PK PRIMARY KEY (grp_id, tbl_nm, 
              part_id) ) ;


ALTER TABLE CARD
       ADD  ( CONSTRAINT CARD_PK PRIMARY KEY (card_id) ) ;


ALTER TABLE CARD_MDL
       ADD  ( CONSTRAINT CARD_MDL_PK PRIMARY KEY (card_typ_id, 
              card_mdl_id) ) ;


ALTER TABLE CFG_VERSION
       ADD  ( CONSTRAINT CFG_VERSION_PK PRIMARY KEY (cfg_nm) ) ;


ALTER TABLE CONSUMABLE_RSRC_MGMT
       ADD  ( CONSTRAINT CONSUMABLE_RSRC_MGMT_PK PRIMARY KEY (
              provider_subntwk_typ_id, consumer_subntwk_typ_id, 
              svc_delivery_plat_id, consumable_rsrc_typ_nm) ) ;


ALTER TABLE EXT_PARM_MAPPING
       ADD  ( CONSTRAINT EXT_PARM_MAPPING_PK PRIMARY KEY (stm_parm_nm, 
              ext_parm_nm) ) ;


ALTER TABLE FREQ
       ADD  ( CONSTRAINT FREQ_PK PRIMARY KEY (freq_id) ) ;


ALTER TABLE FREQ_GRP
       ADD  ( CONSTRAINT FREQ_GRP_PK PRIMARY KEY (freq_grp_id) ) ;


ALTER TABLE FREQ_GRP_LIST
       ADD  ( CONSTRAINT FREQ_GRP_LIST_PK PRIMARY KEY (freq_grp_id, 
              freq_id) ) ;


ALTER TABLE IP_ADDR
       ADD  ( CONSTRAINT IP_ADDR_PK PRIMARY KEY (ip_addr) ) ;


ALTER TABLE IP_BLOCK
       ADD  ( CONSTRAINT IP_BLOCK_PK PRIMARY KEY (ip_block_id) ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT IP_SBNT_PK PRIMARY KEY (ip_sbnt_id) ) ;


ALTER TABLE LBL
       ADD  ( CONSTRAINT LBL_PK PRIMARY KEY (class_id, obj_nm, 
              locale_cd) ) ;


ALTER TABLE LINE_ITEM_DEPY
       ADD  ( CONSTRAINT LINE_ITEM_DEPY_PK PRIMARY KEY (sub_ordr_id, 
              prereq_item_id, depy_item_id) ) ;


ALTER TABLE LINK
       ADD  ( CONSTRAINT LINK_PK PRIMARY KEY (link_id) ) ;


ALTER TABLE LINK_PARM
       ADD  ( CONSTRAINT LINK_PARM_PK PRIMARY KEY (link_id, parm_id) ) ;




ALTER TABLE LOCATION
       ADD  ( CONSTRAINT LOCATION_PK PRIMARY KEY (location_id) ) ;


ALTER TABLE LOCATION_CATEGORY
       ADD  ( CONSTRAINT LOCATION_CATEGORY_PK PRIMARY KEY (
              location_category_id) ) ;


ALTER TABLE LOCATION_DTL
       ADD  ( CONSTRAINT LOCATION_DTL_PK PRIMARY KEY (location_id, 
              parm_id) ) ;


ALTER TABLE LOCATION_STRUC
       ADD  ( CONSTRAINT LOCATION_STRUC_PK PRIMARY KEY (
              location_struc_id) ) ;


ALTER TABLE LOCKED_RESOURCE
       ADD  ( CONSTRAINT LOCKED_RESOURCE_PK PRIMARY KEY (cmpnt_id, 
              ntwk_cmpnt_typ_nm) ) ;





ALTER TABLE NTWK_CMPNT_CHG_IMPACT
       ADD  ( CONSTRAINT NTWK_CMPNT_CHG_IMPACT_PK PRIMARY KEY (
              ntwk_cmpnt_chg_impact_id) ) ;


ALTER TABLE NTWK_CMPNT_TYP_PARM
       ADD  ( CONSTRAINT NTWK_CMPNT_TYP_PARM_PK PRIMARY KEY (
              ntwk_cmpnt_typ_nm, parm_id) ) ;


ALTER TABLE NTWK_LOCATION
       ADD  ( CONSTRAINT NTWK_LOCATION_PK PRIMARY KEY (
              ntwk_location_id) ) ;


ALTER TABLE NTWK_LOCATION_DTLS
       ADD  ( CONSTRAINT NTWK_LOCATION_DTLS_PK PRIMARY KEY (
              ntwk_location_id, location_category_id) ) ;


ALTER TABLE NTWK_LOCATION_STRUC
       ADD  ( CONSTRAINT NTWK_LOCATION_STRUC_PK PRIMARY KEY (
              ntwk_location_id, location_struc_id) ) ;

ALTER TABLE NTWK_RESOURCE
       ADD  ( PRIMARY KEY (rsrc_id) ) ;

ALTER TABLE NTWK_RESOURCE_PARM
       ADD  ( PRIMARY KEY (rsrc_id, parm_id) ) ;

---Can't be PK since sub_entity_id may be null
---ALTER TABLE ORDR_ITEM_ENTITY_CHG
---       ADD  ( CONSTRAINT ORDR_ITEM_ENTITY_CHG_PK PRIMARY KEY (
---              sub_ordr_item_id, parm_id, sub_entity_id) ) ;


ALTER TABLE PARM
       ADD  ( CONSTRAINT PARM_PK PRIMARY KEY (parm_id) ) ;


ALTER TABLE PARM_VLD_RULE
       ADD  ( CONSTRAINT PARM_VLD_RULE_PK PRIMARY KEY (parm_id, 
              vld_rule_id) ) ;


ALTER TABLE PARMVAL_EXCLUSION_GRP
       ADD  ( CONSTRAINT PARMVAL_EXCLUSION_GRP_PK PRIMARY KEY (
              parmval_exclusion_grp_nm, parm_id) ) ;


ALTER TABLE PORT
       ADD  ( CONSTRAINT PORT_PK PRIMARY KEY (port_id) ) ;


ALTER TABLE PORT_PAIR
       ADD  ( CONSTRAINT PORT_PAIR_PK PRIMARY KEY (downstream_port_id, 
              upstream_port_id) ) ;


ALTER TABLE PORT_PAIR_LINK
       ADD  ( CONSTRAINT PORT_PAIR_LINK_PK PRIMARY KEY (link_id, 
              downstream_port_id, upstream_port_id) ) ;


ALTER TABLE PROJ
       ADD  ( CONSTRAINT PROJ_PK PRIMARY KEY (proj_id) ) ;


ALTER TABLE PROJ_IMPACTED_SUB_SVC
       ADD  ( CONSTRAINT PROJ_IMPACTED_SUB_SVC_PK PRIMARY KEY (
              proj_id, sub_svc_id) ) ;


ALTER TABLE PROJ_PARM
       ADD  ( CONSTRAINT PROJ_PARM_PK PRIMARY KEY (proj_id, parm_id) ) ;


ALTER TABLE PROJ_TMPLT
       ADD  ( CONSTRAINT PROJ_TMPLT_PK PRIMARY KEY (proj_tmplt_nm, 
              proj_tmplt_ver) ) ;


ALTER TABLE PROJ_TMPLT_IMPL
       ADD  ( CONSTRAINT PROJ_TMPLT_IMPL_PK PRIMARY KEY (
              proj_tmplt_impl_nm, proj_tmplt_impl_ver) ) ;


ALTER TABLE PROJ_TMPLT_IMPL_PARM
       ADD  ( CONSTRAINT PROJ_TMPLT_IMPL_PARM_PK PRIMARY KEY (
              proj_tmplt_parm_id, proj_tmplt_impl_nm, 
              proj_tmplt_impl_ver) ) ;


ALTER TABLE PROJ_TMPLT_PARM
       ADD  ( CONSTRAINT PROJ_TMPLT_PARM_PK PRIMARY KEY (
              proj_tmplt_parm_id) ) ;


ALTER TABLE PROJ_TRANSITIONS
       ADD  ( CONSTRAINT PROJ_TRANSITIONS_PK PRIMARY KEY (
              from_proj_status_nm, to_proj_status_nm) ) ;






ALTER TABLE REF_ADDR_STATUS
       ADD  ( CONSTRAINT REF_ADDR_STATUS_PK PRIMARY KEY (
              addr_status_id) ) ;


ALTER TABLE REF_ADDR_TYP
       ADD  ( CONSTRAINT REF_ADDR_TYP_PK PRIMARY KEY (addr_typ_id) ) ;


ALTER TABLE REF_ARCH_PARM
       ADD  ( CONSTRAINT REF_ARCH_PARM_PK PRIMARY KEY (parm_nm) ) ;


ALTER TABLE REF_CARD_TYP
       ADD  ( CONSTRAINT REF_CARD_TYP_PK PRIMARY KEY (card_typ_id) ) ;


ALTER TABLE REF_CLASS
       ADD  ( CONSTRAINT REF_CLASS_PK PRIMARY KEY (class_id) ) ;


ALTER TABLE REF_COMM_DIRECTION
       ADD  ( CONSTRAINT REF_COMM_DIRECTION_PK PRIMARY KEY (
              comm_direction_id) ) ;


ALTER TABLE REF_CONSUMABLE_RSRC_TYP
       ADD  ( CONSTRAINT REF_CONSUMABLE_RSRC_TYP_PK PRIMARY KEY (
              consumable_rsrc_typ_nm) ) ;


ALTER TABLE REF_CONTACT_STATUS
       ADD  ( CONSTRAINT REF_CONTACT_STATUS_PK PRIMARY KEY (
              contact_status_id) ) ;


ALTER TABLE REF_CONTACT_TYP
       ADD  ( CONSTRAINT REF_CONTACT_TYP_PK PRIMARY KEY (
              contact_typ_id) ) ;


ALTER TABLE REF_CONVERSION_TYP
       ADD  ( CONSTRAINT REF_CONVERSION_TYP_PK PRIMARY KEY (
              conversion_typ) ) ;


ALTER TABLE REF_DATA_TYP
       ADD  ( CONSTRAINT REF_DATA_TYP_PK PRIMARY KEY (data_typ_nm) ) ;


ALTER TABLE REF_EVENT
       ADD  ( CONSTRAINT REF_EVENT_PK PRIMARY KEY (event_nm) ) ;


ALTER TABLE REF_IP_STATUS
       ADD  ( CONSTRAINT REF_IP_STATUS_PK PRIMARY KEY (ip_status_nm) ) ;


ALTER TABLE REF_IP_SVC_PROVIDER
       ADD  ( CONSTRAINT REF_IP_SVC_PROVIDER_PK PRIMARY KEY (
              ip_svc_provider_id) ) ;


ALTER TABLE REF_LANG
       ADD  ( CONSTRAINT REF_LANG_PK PRIMARY KEY (lang_cd) ) ;


ALTER TABLE REF_LINK_TYP
       ADD  ( CONSTRAINT REF_LINK_TYP_PK PRIMARY KEY (link_typ_id) ) ;


ALTER TABLE REF_LOCALE
       ADD  ( CONSTRAINT REF_LOCALE_PK PRIMARY KEY (locale_cd) ) ;


ALTER TABLE REF_LOCALE_COUNTRY
       ADD  ( CONSTRAINT REF_LOCALE_COUNTRY_PK PRIMARY KEY (
              locale_country_cd) ) ;


ALTER TABLE REF_LOCK_TYP
       ADD  ( CONSTRAINT REF_LOCK_TYP_PK PRIMARY KEY (lock_typ) ) ;


ALTER TABLE REF_MGMT_MODE
       ADD  ( CONSTRAINT REF_MGMT_MODE_PK PRIMARY KEY (mgmt_mode_id) ) ;


ALTER TABLE REF_NTWK_CMPNT_TYP
       ADD  ( CONSTRAINT REF_NTWK_CMPNT_TYP_PK PRIMARY KEY (
              ntwk_cmpnt_typ_nm) ) ;


ALTER TABLE REF_NTWK_ROLE
       ADD  ( CONSTRAINT REF_NTWK_ROLE_PK PRIMARY KEY (ntwk_role_id) ) ;


ALTER TABLE REF_NTWK_TYP
       ADD  ( CONSTRAINT REF_NTWK_TYP_PK PRIMARY KEY (ntwk_typ_id) ) ;


ALTER TABLE REF_ORDR_ACTION
       ADD  ( CONSTRAINT REF_ORDR_ACTION_PK PRIMARY KEY (
              ordr_action_id) ) ;


ALTER TABLE REF_ORDR_ITEM_STATUS
       ADD  ( CONSTRAINT REF_ORDR_ITEM_STATUS_PK PRIMARY KEY (
              ordr_item_status_id) ) ;


ALTER TABLE REF_ORDR_STATUS
       ADD  ( CONSTRAINT REF_ORDR_STATUS_PK PRIMARY KEY (
              ordr_status_id) ) ;


ALTER TABLE REF_PROJ_STATUS
       ADD  ( CONSTRAINT REF_PROJ_STATUS_PK PRIMARY KEY (
              proj_status_nm) ) ;



ALTER TABLE REF_REPROV_STATUS
       ADD  ( CONSTRAINT REF_REPROV_STATUS_PK PRIMARY KEY (
              reprov_status) ) ;


ALTER TABLE REF_RESOURCE
       ADD  ( CONSTRAINT REF_RESOURCE_PK PRIMARY KEY (resource_id) ) ;


ALTER TABLE REF_SCREEN
       ADD  ( CONSTRAINT REF_SCREEN_PK PRIMARY KEY (screen_nm) ) ;


ALTER TABLE REF_SSN_GROUP
       ADD  ( CONSTRAINT REF_SSN_GROUP_PK PRIMARY KEY (ssn_group_id) ) ;


ALTER TABLE REF_STATUS
       ADD  ( CONSTRAINT STATUS_PK PRIMARY KEY (status_id) ) ;


ALTER TABLE REF_STATUS_CHG_REASON
       ADD  ( CONSTRAINT REF_STATUS_CHG_REASON_PK PRIMARY KEY (
              reason_id) ) ;


ALTER TABLE REF_STATUS_TYP
       ADD  ( CONSTRAINT REF_STATUS_TYP_PK PRIMARY KEY (status_typ) ) ;


ALTER TABLE REF_SUB_STATUS
       ADD  ( CONSTRAINT REF_SUB_STATUS_PK PRIMARY KEY (sub_status_id) ) ;


ALTER TABLE REF_SUB_SVC_STATUS
       ADD  ( CONSTRAINT REF_SUB_SVC_STATUS_PK PRIMARY KEY (
              sub_svc_status_id) ) ;


ALTER TABLE REF_SUB_TYP
       ADD  ( CONSTRAINT REF_SUB_TYP_PK PRIMARY KEY (sub_typ_id) ) ;


ALTER TABLE REF_SUBNTWK_MDL
       ADD  ( CONSTRAINT REF_SUBNTWK_MDL_PK PRIMARY KEY (
              subntwk_typ_id, subntwk_mdl_id) ) ;



ALTER TABLE REF_SUBNTWK_TYP
       ADD  ( CONSTRAINT REF_SUBNTWK_TYP_PK PRIMARY KEY (
              subntwk_typ_id) ) ;


ALTER TABLE REF_SVC_ACTION_TYP
       ADD  ( CONSTRAINT REF_SVC_ACTION_TYP_PK PRIMARY KEY (
              svc_action_typ) ) ;


ALTER TABLE REF_SVC_ADVISORY_STATUS
       ADD  ( CONSTRAINT REF_SVC_ADVISORY_STATUS_PK PRIMARY KEY (
              svc_advisory_status_nm) ) ;


ALTER TABLE REF_SVC_DELIVERY_PLAT
       ADD  ( CONSTRAINT REF_SVC_DELIVERY_PLAT_PK PRIMARY KEY (
              svc_delivery_plat_id) ) ;


ALTER TABLE REF_TASK_STATUS
       ADD  ( CONSTRAINT REF_TASK_STATUS_PK PRIMARY KEY (
              task_status_nm) ) ;


ALTER TABLE REF_TICKET_STATUS
       ADD  ( CONSTRAINT REF_TICKET_STATUS_PK PRIMARY KEY (
              ticket_status_nm) ) ;


ALTER TABLE REF_TICKET_TYP
       ADD  ( CONSTRAINT REF_TICKET_TYP_PK PRIMARY KEY (ticket_nm) ) ;


ALTER TABLE REF_TOPOLOGY_ACTION_TYP
       ADD  ( CONSTRAINT REF_TOPOLOGY_ACTION_TYP_PK PRIMARY KEY (
              topology_action_typ) ) ;


ALTER TABLE REF_TOPOLOGY_STATUS
       ADD  ( CONSTRAINT REF_TOPOLOGY_STATUS_PK PRIMARY KEY (
              topology_status_id) ) ;


ALTER TABLE RESOURCE_ASSIGN
       ADD  ( CONSTRAINT RESOURCE_ASSIGN_PK PRIMARY KEY (resource_id, 
              subntwk_id) ) ;


ALTER TABLE SBNT_TECH_PLATFORM
       ADD  ( CONSTRAINT SBNT_TECH_PLATFORM_PK PRIMARY KEY (
              subntwk_id, tech_platform_id, mgmt_mode_id) ) ;


ALTER TABLE SERVER_NODE
       ADD  ( CONSTRAINT SERVER_NODE_PK PRIMARY KEY (node_id) ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT SRC_ACTION_PARM_PK PRIMARY KEY (
              action_tmplt_parm_id, scope_proj_tmplt_nm, 
              scope_proj_tmplt_ver, scope_task_tmplt_nm, 
              scope_task_tmplt_ver, scope_action_execution_seq) ) ;


ALTER TABLE STATE_ACTION
       ADD  ( CONSTRAINT STATUS_CHG_ACTIONS_PK PRIMARY KEY (
              state_action_id) ) ;


ALTER TABLE STATE_TRANSITION
       ADD  ( CONSTRAINT STATE_TRANSITION_PK PRIMARY KEY (
              state_transition_id) ) ;


ALTER TABLE STATE_TRANSITION_EVENT
       ADD  ( CONSTRAINT STATE_TRANSITION_EVENT_PK PRIMARY KEY (
              state_transition_id, target_class_id, event_nm) ) ;


ALTER TABLE STM_BATCH_REPLY_INFO
       ADD  ( CONSTRAINT STM_BATCH_REPLY_INFO_PK PRIMARY KEY (
              sub_ordr_id) ) ;


ALTER TABLE STRUC_HIERARCHY
       ADD  ( CONSTRAINT STRUC_HIERARCHY_PK PRIMARY KEY (
              struc_hierarchy_id) ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT SUB_PK PRIMARY KEY (sub_id) ) ;


ALTER TABLE SUB_ADDR
       ADD  ( CONSTRAINT SUB_ADDR_PK PRIMARY KEY (sub_addr_id) ) ;


ALTER TABLE SUB_CONTACT
       ADD  ( CONSTRAINT SUB_CONTACT_PK PRIMARY KEY (sub_contact_id) ) ;


ALTER TABLE SUB_CONTACT_PARM
       ADD  ( CONSTRAINT SUB_CONTACT_PARM_PK PRIMARY KEY (
              sub_contact_id, parm_id) ) ;



ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT SUB_ORDR_PK PRIMARY KEY (sub_ordr_id) ) ;


ALTER TABLE SUB_ORDR_ITEM
       ADD  ( CONSTRAINT SUB_ORDR_ITEM_PK PRIMARY KEY (
              sub_ordr_item_id) ) ;


ALTER TABLE SUB_ORDR_ITEM_PARM
       ADD  ( CONSTRAINT SUB_ORDR_ITEM_PARM_PK PRIMARY KEY (
              sub_ordr_item_id, parm_id) ) ;


ALTER TABLE SUB_ORDR_PARM
       ADD  ( CONSTRAINT SUB_ORDR_PARM_PK PRIMARY KEY (sub_ordr_id, 
              parm_id) ) ;


ALTER TABLE SUB_PARM
       ADD  ( CONSTRAINT SUB_PARM_PK PRIMARY KEY (sub_id, parm_id) ) ;


ALTER TABLE SUB_SVC
       ADD  ( CONSTRAINT SUB_SVC_PK PRIMARY KEY (sub_svc_id) ) ;


ALTER TABLE SUB_SVC_ADDR
       ADD  ( CONSTRAINT SUB_SVC_ADDR_PK PRIMARY KEY (sub_svc_id, 
              sub_addr_id) ) ;


ALTER TABLE SUB_SVC_DELIVERY_PLAT
       ADD  ( CONSTRAINT SUB_SVC_DELIVERY_PLAT_PK PRIMARY KEY (
              sub_svc_id) ) ;


ALTER TABLE SUB_SVC_DEPY
       ADD  ( CONSTRAINT SUB_SVC_DEPY_PK PRIMARY KEY (
              prerequisite_sub_svc_id, dependent_sub_svc_id) ) ;


ALTER TABLE SUB_SVC_NTWK
       ADD  ( CONSTRAINT SUB_SVC_NTWK_PK PRIMARY KEY (sub_svc_id, 
              subntwk_id) ) ;


ALTER TABLE SUB_SVC_NTWK_STAGE
       ADD  ( CONSTRAINT SUB_SVC_NTWK_STAGE_PK PRIMARY KEY (
              sub_ordr_item_id, sub_svc_id, subntwk_id) ) ;


ALTER TABLE SUB_SVC_PARM
       ADD  ( CONSTRAINT SUB_SVC_PARM_PK PRIMARY KEY (sub_svc_id, 
              parm_id) ) ;


ALTER TABLE SUB_SVC_PARM_DEPY
       ADD  ( CONSTRAINT SUB_SVC_PARM_DEPY_PK PRIMARY KEY (
              depy_sub_svc_id, depy_parm_id) ) ;


ALTER TABLE SUB_SVC_PLAT_STAGE
       ADD  ( CONSTRAINT SUB_SVC_PLAT_STAGE_PK PRIMARY KEY (
              sub_svc_id, sub_ordr_item_id) ) ;


ALTER TABLE SUBNTWK
       ADD  ( CONSTRAINT SUBNTWK_PK PRIMARY KEY (subntwk_id) ) ;


ALTER TABLE SUBNTWK_CARD_LMT
       ADD  ( CONSTRAINT SUBNTWK_CARD_LMT_PK PRIMARY KEY (card_typ_id, 
              card_mdl_id, subntwk_typ_id, subntwk_mdl_id) ) ;


ALTER TABLE SUBNTWK_COVERAGE
       ADD  ( CONSTRAINT SUBNTWK_COVERAGE_PK PRIMARY KEY (
              subntwk_coverage_id) ) ;


ALTER TABLE SUBNTWK_MDL_LMT
       ADD  ( CONSTRAINT SUBNTWK_MDL_LMT_PK PRIMARY KEY (
              subntwk_typ_id, subntwk_mdl_id, parent_subntwk_typ_id, 
              parent_subntwk_mdl_id) ) ;


ALTER TABLE SUBNTWK_MDL_PARM
       ADD  ( CONSTRAINT SUBNTWK_MDL_PARM_PK PRIMARY KEY (
              subntwk_typ_id, subntwk_mdl_id, parm_id) ) ;


ALTER TABLE SUBNTWK_PARM
       ADD  ( CONSTRAINT SUBNTWK_PARM_PK PRIMARY KEY (subntwk_id, 
              parm_id) ) ;


ALTER TABLE SUBNTWK_PARM_STAGE
       ADD  ( CONSTRAINT SUBNTWK_PARM_STAGE_PK PRIMARY KEY (
              sub_ordr_item_id, parm_id, sub_svc_id, subntwk_id) ) ;



ALTER TABLE SUBNTWK_TYP_FREQ
       ADD  ( CONSTRAINT SUBNTWK_TYP_FREQ_PK PRIMARY KEY (freq_id, 
              subntwk_typ_id) ) ;


ALTER TABLE SUBNTWK_TYP_FREQ_GRP
       ADD  ( CONSTRAINT SUBNTWK_TYP_FREQ_GRP_PK PRIMARY KEY (
              freq_grp_id, subntwk_typ_id) ) ;




ALTER TABLE SVC
       ADD  ( CONSTRAINT SVC_PK PRIMARY KEY (svc_id) ) ;


ALTER TABLE SVC_ACTION
       ADD  ( CONSTRAINT SVC_ACTION_PK PRIMARY KEY (svc_action_id) ) ;


ALTER TABLE SVC_ADVISORY
       ADD  ( CONSTRAINT SVC_ADVISORY_PK PRIMARY KEY (svc_advisory_id) ) ;


ALTER TABLE SVC_ADVISORY_PARM
       ADD  ( CONSTRAINT SVC_ADVISORY_PARM_PK PRIMARY KEY (
              svc_advisory_id, parm_id) ) ;


ALTER TABLE SVC_PROVIDER
       ADD  ( CONSTRAINT SVC_PROVIDER_PK PRIMARY KEY (svc_provider_id) ) ;


ALTER TABLE SVC_SUPPORTED_PLATFORM
       ADD  ( CONSTRAINT SVC_SUPPORTED_PLATFORM_PK PRIMARY KEY (
              svc_supported_platform_id) ) ;


ALTER TABLE SYNC_ACTION_PARMS_TMP
       ADD  ( CONSTRAINT SYNC_ACTION_PARMS_TMP_PK PRIMARY KEY (
              sync_action_parm_id, parm_nm) ) ;


ALTER TABLE SYNC_DEBUG
       ADD  ( CONSTRAINT SYNC_DEBUG_PK PRIMARY KEY (sync_debug_seq) ) ;


ALTER TABLE SYNC_EXT_PROC_PARMS
       ADD  ( CONSTRAINT SYNC_EXT_PROC_PARMS_PK PRIMARY KEY (
              ext_proc_nm, ext_proc_parm_nm) ) ;


ALTER TABLE SYNC_EXT_PROCS
       ADD  ( CONSTRAINT SYNC_EXT_PROCS_PK PRIMARY KEY (ext_proc_nm) ) ;


ALTER TABLE SYNC_GRP_ACTION_PARMS
       ADD  ( CONSTRAINT SYNC_GRP_ACTION_PARMS_PK PRIMARY KEY (grp_nm, 
              action_seq_within_grp, action, parm_nm) ) ;


ALTER TABLE SYNC_GRP_ACTIONS
       ADD  ( CONSTRAINT SYNC_ACTION_GRP_LIST_PK PRIMARY KEY (grp_nm, 
              action_seq_within_grp, action) ) ;


ALTER TABLE SYNC_GRP_EXT_PROC_COND
       ADD  ( CONSTRAINT SYNC_ACTION_EXT_PROC_COND_PK PRIMARY KEY (
              ext_proc_nm, check_parm_nm, grp_nm, 
              action_seq_within_grp, action) ) ;


ALTER TABLE SYNC_GRP_EXT_PROCS
       ADD  ( CONSTRAINT SYNC_ACTION_EXT_PROCS_PK PRIMARY KEY (grp_nm, 
              ext_proc_nm) ) ;


ALTER TABLE SYNC_GRP_QUALIFIERS
       ADD  ( CONSTRAINT SYNC_GRP_QUALIFIERS_PK PRIMARY KEY (
              proj_tmplt_impl_ver, proj_tmplt_impl_nm, grp_nm) ) ;


ALTER TABLE SYNC_GRPS
       ADD  ( CONSTRAINT SYNC_GRPS_PK PRIMARY KEY (grp_nm) ) ;


ALTER TABLE SYNC_SRC_EXT_PARMS
       ADD  ( CONSTRAINT SYNC_SRC_EXT_PARMS_PK PRIMARY KEY (
              ext_proc_nm, ext_proc_parm_nm, scope_grp) ) ;


ALTER TABLE TASK
       ADD  ( CONSTRAINT TASK_PK PRIMARY KEY (task_id) ) ;


ALTER TABLE TASK_ACTION
       ADD  ( CONSTRAINT TASK_ACTION_PK PRIMARY KEY (task_action_id) ) ;


ALTER TABLE TASK_ACTION_PARM
       ADD  ( CONSTRAINT TASK_ACTION_PARM_PK PRIMARY KEY (
              task_action_parm_id) ) ;


ALTER TABLE TASK_PARM
       ADD  ( CONSTRAINT TASK_PARM_PK PRIMARY KEY (task_id, parm_id) ) ;


ALTER TABLE TASK_TMPLT
       ADD  ( CONSTRAINT TASK_TMPLT_PK PRIMARY KEY (task_tmplt_nm, 
              task_tmplt_ver) ) ;


ALTER TABLE TASK_TMPLT_DEPY
       ADD  ( CONSTRAINT TASK_TMPLT_DEPY_PK PRIMARY KEY (
              proj_tmplt_nm, proj_tmplt_ver, task_tmplt_nm, 
              task_tmplt_ver, dependent_task_tmplt_nm, 
              dependent_task_tmplt_ver) ) ;


ALTER TABLE TASK_TMPLT_PARM
       ADD  ( CONSTRAINT TASK_TMPLT_PARM_PK PRIMARY KEY (
              task_tmplt_parm_id) ) ;


ALTER TABLE TASK_TRANSITIONS
       ADD  ( CONSTRAINT TASK_TRANSITIONS_PK PRIMARY KEY (
              from_task_status_nm, to_task_status_nm) ) ;


ALTER TABLE TASKS_IN_PROJ_TMPLT
       ADD  ( CONSTRAINT TASKS_IN_PROJ_TMPLT_PK PRIMARY KEY (
              proj_tmplt_nm, proj_tmplt_ver, task_tmplt_nm, 
              task_tmplt_ver) ) ;


ALTER TABLE TECH_PLATFORM
       ADD  ( CONSTRAINT TECH_PLATFORM_PK PRIMARY KEY (
              tech_platform_id) ) ;


ALTER TABLE TECH_PLATFORM_PARM
       ADD  ( CONSTRAINT TECH_PLATFORM_PARM_PK PRIMARY KEY (
              tech_platform_id, parm_id) ) ;


ALTER TABLE TECH_PLATFORM_TYP
       ADD  ( CONSTRAINT TECH_PLATFORM_TYP_PK PRIMARY KEY (
              tech_platform_typ_id) ) ;


ALTER TABLE TROUBLE_TICKET
       ADD  ( CONSTRAINT TROUBLE_TICKET_PK PRIMARY KEY (ticket_id) ) ;


ALTER TABLE TROUBLE_TICKET_PARM
       ADD  ( CONSTRAINT TROUBLE_TICKET_PARM_PK PRIMARY KEY (
              ticket_id, parm_id) ) ;


ALTER TABLE VERSION
       ADD  ( CONSTRAINT VERSION_PK PRIMARY KEY (module_name) ) ;


ALTER TABLE VLD_RULE
       ADD  ( CONSTRAINT VLD_RULE_PK PRIMARY KEY (vld_rule_id) ) ;


ALTER TABLE XML_REPOSITORY
       ADD  ( CONSTRAINT XML_REPOSITORY_PK PRIMARY KEY (doc_nm) ) ;


ALTER TABLE ACTION_TMPLT
       ADD  ( CONSTRAINT action_tmplt_fk1
              FOREIGN KEY (topology_action_typ)
                             REFERENCES REF_TOPOLOGY_ACTION_TYP ) ;


ALTER TABLE ACTION_TMPLT_PARM
       ADD  ( CONSTRAINT action_tmplt_parm_fk2
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE ACTION_TMPLT_PARM
       ADD  ( CONSTRAINT action_tmplt_parm_fk3
              FOREIGN KEY (ntwk_cmpnt_typ_nm)
                             REFERENCES REF_NTWK_CMPNT_TYP ) ;


ALTER TABLE ACTION_TMPLT_PARM
       ADD  ( CONSTRAINT action_tmplt_parm_fk1
              FOREIGN KEY (action_tmplt_nm, action_tmplt_ver)
                             REFERENCES ACTION_TMPLT ) ;


ALTER TABLE ACTIONS_IN_TASK_TMPLT
       ADD  ( CONSTRAINT ACTIONS_IN_TASK_TMPLT_FK3
              FOREIGN KEY (repeat_parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE ACTIONS_IN_TASK_TMPLT
       ADD  ( CONSTRAINT ACTIONS_IN_TASK_TMPLT_FK1
              FOREIGN KEY (action_tmplt_nm, action_tmplt_ver)
                             REFERENCES ACTION_TMPLT ) ;


ALTER TABLE ACTIONS_IN_TASK_TMPLT
       ADD  ( CONSTRAINT ACTIONS_IN_TASK_TMPLT_FK2
              FOREIGN KEY (task_tmplt_nm, task_tmplt_ver, 
              parent_execution_seq)
                             REFERENCES ACTIONS_IN_TASK_TMPLT ) ;


ALTER TABLE ACTIONS_IN_TASK_TMPLT
       ADD  ( CONSTRAINT ACTIONS_IN_TASK_TMPLT_FK4
              FOREIGN KEY (screen_nm)
                             REFERENCES REF_SCREEN ) ;


ALTER TABLE ACTIONS_IN_TASK_TMPLT
       ADD  ( CONSTRAINT ACTIONS_IN_TASK_TMPLT_FK5
              FOREIGN KEY (task_tmplt_nm, task_tmplt_ver)
                             REFERENCES TASK_TMPLT ) ;


ALTER TABLE ARCH_GRP
       ADD  ( CONSTRAINT ARCH_GRP_ROOT_TBL
              FOREIGN KEY (grp_id, root_tbl_nm)
                             REFERENCES ARCH_TBL ) ;


ALTER TABLE ARCH_GRP_PARM
       ADD  ( CONSTRAINT ARCH_GRP_PARM_GRP
              FOREIGN KEY (grp_id)
                             REFERENCES ARCH_GRP
                             ON DELETE CASCADE ) ;


ALTER TABLE ARCH_GRP_PARM
       ADD  ( CONSTRAINT ARCH_GRP_PARM_PARM
              FOREIGN KEY (parm_nm)
                             REFERENCES REF_ARCH_PARM
                             ON DELETE CASCADE ) ;


ALTER TABLE ARCH_GRP_PART
       ADD  ( CONSTRAINT ARCH_GRP_PART_GRP
              FOREIGN KEY (grp_id)
                             REFERENCES ARCH_GRP
                             ON DELETE CASCADE ) ;


ALTER TABLE ARCH_RUN
       ADD  ( CONSTRAINT ARCH_RUN_GRP
              FOREIGN KEY (grp_id)
                             REFERENCES ARCH_GRP ) ;


ALTER TABLE ARCH_TBL
       ADD  ( CONSTRAINT ARCH_TBL_GRP
              FOREIGN KEY (grp_id)
                             REFERENCES ARCH_GRP
                             ON DELETE CASCADE ) ;


ALTER TABLE ARCH_TBL_LOG
       ADD  ( CONSTRAINT ARCH_TBL_LOG_RUN
              FOREIGN KEY (grp_id, run_id)
                             REFERENCES ARCH_RUN ) ;


ALTER TABLE ARCH_TBL_LOG
       ADD  ( CONSTRAINT ARCH_TBL_LOG_PART
              FOREIGN KEY (grp_id, tbl_nm, part_id)
                             REFERENCES ARCH_TBL_PART ) ;


ALTER TABLE ARCH_TBL_NET
       ADD  ( CONSTRAINT ARCH_PARM_TBL_NET_TGT
              FOREIGN KEY (grp_id, tgt_tbl_nm)
                             REFERENCES ARCH_TBL
                             ON DELETE CASCADE ) ;


ALTER TABLE ARCH_TBL_NET
       ADD  ( CONSTRAINT ARCH_PARM_TBL_NET_SRC
              FOREIGN KEY (grp_id, src_tbl_nm)
                             REFERENCES ARCH_TBL ) ;


ALTER TABLE ARCH_TBL_PART
       ADD  ( CONSTRAINT ARCH_TBL_PART_PART
              FOREIGN KEY (grp_id, part_id)
                             REFERENCES ARCH_GRP_PART
                             ON DELETE CASCADE ) ;


ALTER TABLE ARCH_TBL_PART
       ADD  ( CONSTRAINT ARCH_TBL_PART_TBL
              FOREIGN KEY (grp_id, tbl_nm)
                             REFERENCES ARCH_TBL
                             ON DELETE CASCADE ) ;


ALTER TABLE CARD
       ADD  ( CONSTRAINT CARD_CARD_MDL
              FOREIGN KEY (card_typ_id, card_mdl_id)
                             REFERENCES CARD_MDL ) ;


ALTER TABLE CARD
       ADD  ( CONSTRAINT card_fk2
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE CARD_MDL
       ADD  ( CONSTRAINT CARD_MDL_TYP
              FOREIGN KEY (card_typ_id)
                             REFERENCES REF_CARD_TYP ) ;


ALTER TABLE CONSUMABLE_RSRC_MGMT
       ADD  ( CONSTRAINT consumable_rsrc_mgmt_fk1
              FOREIGN KEY (consumable_rsrc_typ_nm)
                             REFERENCES REF_CONSUMABLE_RSRC_TYP ) ;


ALTER TABLE CONSUMABLE_RSRC_MGMT
       ADD  ( CONSTRAINT consumable_rsrc_mgmt_fk4
              FOREIGN KEY (svc_delivery_plat_id)
                             REFERENCES REF_SVC_DELIVERY_PLAT ) ;


ALTER TABLE CONSUMABLE_RSRC_MGMT
       ADD  ( CONSTRAINT consumable_rsrc_mgmt_fk2
              FOREIGN KEY (provider_subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE CONSUMABLE_RSRC_MGMT
       ADD  ( CONSTRAINT consumable_rsrc_mgmt_fk3
              FOREIGN KEY (consumer_subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE FREQ
       ADD  ( CONSTRAINT freq_fk1
              FOREIGN KEY (comm_direction_id)
                             REFERENCES REF_COMM_DIRECTION ) ;


ALTER TABLE FREQ_GRP_LIST
       ADD  ( CONSTRAINT freq_grp_list_fk1
              FOREIGN KEY (freq_id)
                             REFERENCES FREQ ) ;


ALTER TABLE FREQ_GRP_LIST
       ADD  ( CONSTRAINT freq_grp_list_fk2
              FOREIGN KEY (freq_grp_id)
                             REFERENCES FREQ_GRP ) ;


ALTER TABLE IP_ADDR
       ADD  ( CONSTRAINT ap_addr_fk2
              FOREIGN KEY (ip_status_nm)
                             REFERENCES REF_IP_STATUS ) ;


ALTER TABLE IP_ADDR
       ADD  ( CONSTRAINT ap_addr_fk1
              FOREIGN KEY (ip_sbnt_id)
                             REFERENCES IP_SBNT ) ;


ALTER TABLE IP_ADDR
       ADD  ( CONSTRAINT ap_addr_fk3
              FOREIGN KEY (assigned_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE IP_BLOCK
       ADD  ( CONSTRAINT ip_block_fk2
              FOREIGN KEY (managing_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE IP_BLOCK
       ADD  ( CONSTRAINT ip_block_fk1
              FOREIGN KEY (ip_status_nm)
                             REFERENCES REF_IP_STATUS ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT ip_sbnt_fk5
              FOREIGN KEY (ip_svc_provider_id)
                             REFERENCES REF_IP_SVC_PROVIDER ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT ip_sbnt_fk1
              FOREIGN KEY (ip_broadcast_addr)
                             REFERENCES IP_ADDR ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT ip_sbnt_fk2
              FOREIGN KEY (ip_sbnt_gateway_addr)
                             REFERENCES IP_ADDR ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT ip_sbnt_fk6
              FOREIGN KEY (managing_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT ip_sbnt_fk4
              FOREIGN KEY (ip_status_nm)
                             REFERENCES REF_IP_STATUS ) ;


ALTER TABLE IP_SBNT
       ADD  ( CONSTRAINT ip_sbnt_fk3
              FOREIGN KEY (ip_block_id)
                             REFERENCES IP_BLOCK ) ;


ALTER TABLE LBL
       ADD  ( CONSTRAINT LBL_FK2
              FOREIGN KEY (class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE LBL
       ADD  ( CONSTRAINT LBL_FK1
              FOREIGN KEY (locale_cd)
                             REFERENCES REF_LOCALE ) ;


ALTER TABLE LINE_ITEM_DEPY
       ADD  ( CONSTRAINT line_item_depy_fk3
              FOREIGN KEY (depy_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE LINE_ITEM_DEPY
       ADD  ( CONSTRAINT line_item_depy_fk2
              FOREIGN KEY (prereq_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE LINE_ITEM_DEPY
       ADD  ( CONSTRAINT line_item_depy_fk1
              FOREIGN KEY (sub_ordr_id)
                             REFERENCES SUB_ORDR ) ;


ALTER TABLE LINK
       ADD  ( CONSTRAINT link_fk3
              FOREIGN KEY (from_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE LINK
       ADD  ( CONSTRAINT link_fk4
              FOREIGN KEY (to_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE LINK
       ADD  ( CONSTRAINT link_fk2
              FOREIGN KEY (topology_status_id)
                             REFERENCES REF_TOPOLOGY_STATUS ) ;


ALTER TABLE LINK
       ADD  ( CONSTRAINT link_fk1
              FOREIGN KEY (link_typ_id)
                             REFERENCES REF_LINK_TYP ) ;


ALTER TABLE LINK_PARM
       ADD  ( CONSTRAINT link_parm_fk1
              FOREIGN KEY (link_id)
                             REFERENCES LINK ) ;


ALTER TABLE LINK_PARM
       ADD  ( CONSTRAINT link_parm_fk2
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE LOCATION_DTL
       ADD  ( CONSTRAINT location_dtl_fk2
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE LOCATION_DTL
       ADD  ( CONSTRAINT location_dtl_fk1
              FOREIGN KEY (location_id)
                             REFERENCES LOCATION ) ;


ALTER TABLE LOCATION_STRUC
       ADD  ( CONSTRAINT location_struc_fk1
              FOREIGN KEY (location_category_id)
                             REFERENCES LOCATION_CATEGORY ) ;


ALTER TABLE LOCATION_STRUC
       ADD  ( CONSTRAINT location_struc_fk1_2
              FOREIGN KEY (parent_location_struc_id)
                             REFERENCES LOCATION_STRUC ) ;


ALTER TABLE LOCKED_RESOURCE
       ADD  ( CONSTRAINT locked_resource_fk2
              FOREIGN KEY (lock_typ)
                             REFERENCES REF_LOCK_TYP ) ;


ALTER TABLE LOCKED_RESOURCE
       ADD  ( CONSTRAINT locked_resource_fk1
              FOREIGN KEY (proj_id)
                             REFERENCES PROJ ) ;




ALTER TABLE NTWK_CMPNT_CHG_IMPACT
       ADD  ( CONSTRAINT ntwk_cmpnt_chg_impact_fk1
              FOREIGN KEY (ntwk_cmpnt_typ_nm)
                             REFERENCES REF_NTWK_CMPNT_TYP ) ;


ALTER TABLE NTWK_CMPNT_CHG_IMPACT
       ADD  ( CONSTRAINT ntwk_cmpnt_chg_impact_fk2
              FOREIGN KEY (topology_action_typ)
                             REFERENCES REF_TOPOLOGY_ACTION_TYP ) ;


ALTER TABLE NTWK_CMPNT_TYP_PARM
       ADD  ( CONSTRAINT ntwk_cmpnt_typ_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE NTWK_CMPNT_TYP_PARM
       ADD  ( CONSTRAINT ntwk_cmpnt_typ_parm_fk2
              FOREIGN KEY (ntwk_cmpnt_typ_nm)
                             REFERENCES REF_NTWK_CMPNT_TYP ) ;


ALTER TABLE NTWK_LOCATION_DTLS
       ADD  ( CONSTRAINT ntwk_location_dtls_fk1
              FOREIGN KEY (location_category_id)
                             REFERENCES LOCATION_CATEGORY ) ;


ALTER TABLE NTWK_LOCATION_DTLS
       ADD  ( CONSTRAINT ntwk_location_dtls_fk2
              FOREIGN KEY (ntwk_location_id)
                             REFERENCES NTWK_LOCATION ) ;


ALTER TABLE NTWK_LOCATION_STRUC
       ADD  ( CONSTRAINT ntwk_location_struc_fk1
              FOREIGN KEY (location_struc_id)
                             REFERENCES LOCATION_STRUC ) ;


ALTER TABLE NTWK_LOCATION_STRUC
       ADD  ( CONSTRAINT ntwk_location_struc_fk2
              FOREIGN KEY (ntwk_location_id)
                             REFERENCES NTWK_LOCATION ) ;


ALTER TABLE NTWK_RESOURCE
       ADD  ( FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE NTWK_RESOURCE
       ADD  ( FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE NTWK_RESOURCE
       ADD  ( FOREIGN KEY (consumable_rsrc_typ_nm)
                             REFERENCES REF_CONSUMABLE_RSRC_TYP ) ;

ALTER TABLE NTWK_RESOURCE_PARM
       ADD  ( FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE NTWK_RESOURCE_PARM
       ADD  ( FOREIGN KEY (rsrc_id)
                             REFERENCES NTWK_RESOURCE ) ;



ALTER TABLE ORDR_ITEM_ENTITY_CHG
       ADD  ( CONSTRAINT ordr_item_entity_chg_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE ORDR_ITEM_ENTITY_CHG
       ADD  ( CONSTRAINT ordr_item_entity_chg_fk2
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE PARM
       ADD  ( CONSTRAINT parm_fk1
              FOREIGN KEY (conversion_typ)
                             REFERENCES REF_CONVERSION_TYP ) ;


ALTER TABLE PARM
       ADD  ( CONSTRAINT parm_fk3
              FOREIGN KEY (data_typ_nm)
                             REFERENCES REF_DATA_TYP ) ;


ALTER TABLE PARM
       ADD  ( CONSTRAINT parm_fk2
              FOREIGN KEY (class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE PARM_PERMITTED_VAL
       ADD  ( CONSTRAINT parm_permitted_val_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE PARM_VLD_RULE
       ADD  ( CONSTRAINT parm_vld_rule_FK1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM
                             ON DELETE CASCADE ) ;


ALTER TABLE PARM_VLD_RULE
       ADD  ( CONSTRAINT parm_vld_rule_FK2
              FOREIGN KEY (vld_rule_id)
                             REFERENCES VLD_RULE
                             ON DELETE CASCADE ) ;


ALTER TABLE PARMVAL_EXCLUSION_GRP
       ADD  ( CONSTRAINT parmval_exclusion_grp_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE PORT
       ADD  ( CONSTRAINT port_fk1
              FOREIGN KEY (card_id)
                             REFERENCES CARD ) ;


ALTER TABLE PORT
       ADD  ( CONSTRAINT port_fk4
              FOREIGN KEY (comm_direction_id)
                             REFERENCES REF_COMM_DIRECTION ) ;


ALTER TABLE PORT
       ADD  ( CONSTRAINT port_fk2
              FOREIGN KEY (freq_id)
                             REFERENCES FREQ ) ;


ALTER TABLE PORT
       ADD  ( CONSTRAINT port_fk3
              FOREIGN KEY (freq_grp_id)
                             REFERENCES FREQ_GRP ) ;


ALTER TABLE PORT_PAIR
       ADD  ( CONSTRAINT port_pair_fk3
              FOREIGN KEY (managing_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE PORT_PAIR
       ADD  ( CONSTRAINT port_pair_fk1
              FOREIGN KEY (upstream_port_id)
                             REFERENCES PORT ) ;


ALTER TABLE PORT_PAIR
       ADD  ( CONSTRAINT port_pair_fk2
              FOREIGN KEY (downstream_port_id)
                             REFERENCES PORT ) ;


ALTER TABLE PORT_PAIR_LINK
       ADD  ( CONSTRAINT port_pair_link_fk1
              FOREIGN KEY (link_id)
                             REFERENCES LINK ) ;


ALTER TABLE PORT_PAIR_LINK
       ADD  ( CONSTRAINT port_pair_link_fk2
              FOREIGN KEY (downstream_port_id, upstream_port_id)
                             REFERENCES PORT_PAIR ) ;


ALTER TABLE PROJ
       ADD  ( CONSTRAINT proj_fk3
              FOREIGN KEY (hint_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE PROJ
       ADD  ( CONSTRAINT proj_fk1
              FOREIGN KEY (proj_tmplt_impl_nm, proj_tmplt_impl_ver)
                             REFERENCES PROJ_TMPLT_IMPL ) ;


ALTER TABLE PROJ
       ADD  ( CONSTRAINT proj_fk2
              FOREIGN KEY (proj_status_nm)
                             REFERENCES REF_PROJ_STATUS ) ;


ALTER TABLE PROJ_IMPACTED_SUB_SVC
       ADD  ( CONSTRAINT proj_impacted_sub_svc_fk3
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE PROJ_IMPACTED_SUB_SVC
       ADD  ( CONSTRAINT proj_impacted_sub_svc_fk1
              FOREIGN KEY (proj_id)
                             REFERENCES PROJ ) ;


ALTER TABLE PROJ_IMPACTED_SUB_SVC
       ADD  ( CONSTRAINT proj_impacted_sub_svc_fk2
              FOREIGN KEY (reprov_status)
                             REFERENCES REF_REPROV_STATUS ) ;


ALTER TABLE PROJ_PARM
       ADD  ( CONSTRAINT proj_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE PROJ_PARM
       ADD  ( CONSTRAINT proj_parm_fk2
              FOREIGN KEY (proj_id)
                             REFERENCES PROJ ) ;


ALTER TABLE PROJ_TMPLT_IMPL
       ADD  ( CONSTRAINT proj_tmplt_impl_fk1
              FOREIGN KEY (proj_tmplt_nm, proj_tmplt_ver)
                             REFERENCES PROJ_TMPLT ) ;


ALTER TABLE PROJ_TMPLT_IMPL_PARM
       ADD  ( CONSTRAINT proj_tmplt_impl_parm_fk3
              FOREIGN KEY (src_parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE PROJ_TMPLT_IMPL_PARM
       ADD  ( CONSTRAINT proj_tmplt_impl_parm_fk2
              FOREIGN KEY (proj_tmplt_parm_id)
                             REFERENCES PROJ_TMPLT_PARM ) ;


ALTER TABLE PROJ_TMPLT_IMPL_PARM
       ADD  ( CONSTRAINT proj_tmplt_impl_parm_fk1
              FOREIGN KEY (proj_tmplt_impl_nm, proj_tmplt_impl_ver)
                             REFERENCES PROJ_TMPLT_IMPL ) ;


ALTER TABLE PROJ_TMPLT_PARM
       ADD  ( CONSTRAINT proj_tmplt_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE PROJ_TMPLT_PARM
       ADD  ( CONSTRAINT proj_tmplt_parm_fk2
              FOREIGN KEY (proj_tmplt_nm, proj_tmplt_ver)
                             REFERENCES PROJ_TMPLT ) ;


ALTER TABLE PROJ_TRANSITIONS
       ADD  ( CONSTRAINT proj_transitions_fk1
              FOREIGN KEY (from_proj_status_nm)
                             REFERENCES REF_PROJ_STATUS ) ;


ALTER TABLE PROJ_TRANSITIONS
       ADD  ( CONSTRAINT proj_transitions_fk2
              FOREIGN KEY (to_proj_status_nm)
                             REFERENCES REF_PROJ_STATUS ) ;




ALTER TABLE REF_ADDR_STATUS
       ADD  ( CONSTRAINT ref_addr_status_fk1
              FOREIGN KEY (addr_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE REF_CONTACT_STATUS
       ADD  ( CONSTRAINT ref_contact_status_fk1
              FOREIGN KEY (contact_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE REF_LOCALE
       ADD  ( CONSTRAINT ref_locale_fk2
              FOREIGN KEY (locale_country_cd)
                             REFERENCES REF_LOCALE_COUNTRY ) ;


ALTER TABLE REF_LOCALE
       ADD  ( CONSTRAINT ref_locale_fk1
              FOREIGN KEY (lang_cd)
                             REFERENCES REF_LANG ) ;


ALTER TABLE REF_ORDR_ITEM_STATUS
       ADD  ( CONSTRAINT REF_ORDR_ITEM_STATUS_FK1
              FOREIGN KEY (ordr_item_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE REF_ORDR_STATUS
       ADD  ( CONSTRAINT ref_order_status_fk1
              FOREIGN KEY (ordr_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE REF_STATUS
       ADD  ( CONSTRAINT ref_status_fk2
              FOREIGN KEY (status_typ)
                             REFERENCES REF_STATUS_TYP ) ;


ALTER TABLE REF_STATUS
       ADD  ( CONSTRAINT ref_status_fk1
              FOREIGN KEY (class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE REF_STATUS_CHG_REASON
       ADD  ( CONSTRAINT REF_STATUS_CHG_REASON_FK1
              FOREIGN KEY (class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE REF_SUB_STATUS
       ADD  ( CONSTRAINT sub_status_fk1
              FOREIGN KEY (sub_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE REF_SUB_SVC_STATUS
       ADD  ( CONSTRAINT ref_sub_svc_status_fk1
              FOREIGN KEY (sub_svc_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE REF_SUBNTWK_MDL
       ADD  ( CONSTRAINT subntwk_mdl_typ
              FOREIGN KEY (subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE REF_SUBNTWK_TYP
       ADD  ( CONSTRAINT ref_subntwk_typ_fk1
              FOREIGN KEY (ntwk_typ_id)
                             REFERENCES REF_NTWK_TYP ) ;


ALTER TABLE RESOURCE_ASSIGN
       ADD  ( CONSTRAINT RESOURCE_ASSIGN_FK2
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE RESOURCE_ASSIGN
       ADD  ( CONSTRAINT RESOURCE_ASSIGN_FK1
              FOREIGN KEY (resource_id)
                             REFERENCES REF_RESOURCE ) ;



ALTER TABLE SBNT_TECH_PLATFORM
       ADD  ( CONSTRAINT SBNT_TECH_PLATFORM_FK1
              FOREIGN KEY (tech_platform_id, mgmt_mode_id)
                             REFERENCES TECH_PLATFORM_MGMT_MODE ) ;




ALTER TABLE SBNT_TECH_PLATFORM
       ADD  ( CONSTRAINT sbnt_tech_platform_fk2
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE TECH_PLATFORM_MGMT_MODE
       ADD  ( CONSTRAINT TECH_PLATFORM_MGMT_MODE_FK1
              FOREIGN KEY (tech_platform_id)
                             REFERENCES TECH_PLATFORM ) ;


ALTER TABLE TECH_PLATFORM_MGMT_MODE
       ADD  ( CONSTRAINT TECH_PLATFORM_MGMT_MODE_FK2
              FOREIGN KEY (mgmt_mode_id)
                             REFERENCES REF_MGMT_MODE ) ;




ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk3
              FOREIGN KEY (src_task_tmplt_nm, src_task_tmplt_ver, 
              src_action_execution_seq)
                             REFERENCES ACTIONS_IN_TASK_TMPLT ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk4
              FOREIGN KEY (scope_task_tmplt_nm, scope_task_tmplt_ver, 
              scope_action_execution_seq)
                             REFERENCES ACTIONS_IN_TASK_TMPLT ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk6
              FOREIGN KEY (src_task_tmplt_parm_id)
                             REFERENCES TASK_TMPLT_PARM ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk5
              FOREIGN KEY (src_proj_tmplt_parm_id)
                             REFERENCES PROJ_TMPLT_PARM ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk1
              FOREIGN KEY (src_action_tmplt_parm_id)
                             REFERENCES ACTION_TMPLT_PARM ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk7
              FOREIGN KEY (scope_proj_tmplt_nm, scope_proj_tmplt_ver, 
              scope_task_tmplt_nm, scope_task_tmplt_ver)
                             REFERENCES TASKS_IN_PROJ_TMPLT ) ;


ALTER TABLE SRC_ACTION_PARM
       ADD  ( CONSTRAINT src_action_parm_fk2
              FOREIGN KEY (action_tmplt_parm_id)
                             REFERENCES ACTION_TMPLT_PARM ) ;


ALTER TABLE STATE_ACTION
       ADD  ( CONSTRAINT state_action_fk2
              FOREIGN KEY (svc_id)
                             REFERENCES SVC ) ;


ALTER TABLE STATE_ACTION
       ADD  ( CONSTRAINT state_action_fk1
              FOREIGN KEY (class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE STATE_TRANSITION
       ADD  ( CONSTRAINT state_transition_fk4
              FOREIGN KEY (scope_svc_id)
                             REFERENCES SVC ) ;


ALTER TABLE STATE_TRANSITION
       ADD  ( CONSTRAINT STATE_TRANSITION_FK3
              FOREIGN KEY (state_action_id)
                             REFERENCES STATE_ACTION ) ;


ALTER TABLE STATE_TRANSITION
       ADD  ( CONSTRAINT state_transition_fk1
              FOREIGN KEY (to_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE STATE_TRANSITION
       ADD  ( CONSTRAINT state_transition_fk2
              FOREIGN KEY (from_status_id)
                             REFERENCES REF_STATUS ) ;


ALTER TABLE STATE_TRANSITION_EVENT
       ADD  ( CONSTRAINT state_transition_event_fk3
              FOREIGN KEY (target_class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE STATE_TRANSITION_EVENT
       ADD  ( CONSTRAINT state_transition_event_fk2
              FOREIGN KEY (state_transition_id)
                             REFERENCES STATE_TRANSITION ) ;


ALTER TABLE STATE_TRANSITION_EVENT
       ADD  ( CONSTRAINT state_transition_event_fk1
              FOREIGN KEY (event_nm)
                             REFERENCES REF_EVENT ) ;


ALTER TABLE STM_BATCH_REPLY_INFO
       ADD  ( CONSTRAINT stm_batch_reply_info_fk1
              FOREIGN KEY (sub_ordr_id)
                             REFERENCES SUB_ORDR ) ;


ALTER TABLE STRUC_HIERARCHY
       ADD  ( CONSTRAINT struc_hierarchy_fk3
              FOREIGN KEY (location_struc_id)
                             REFERENCES LOCATION_STRUC ) ;


ALTER TABLE STRUC_HIERARCHY
       ADD  ( CONSTRAINT struc_hierarchy_fk1
              FOREIGN KEY (parent_location_category_id)
                             REFERENCES LOCATION_CATEGORY ) ;


ALTER TABLE STRUC_HIERARCHY
       ADD  ( CONSTRAINT struc_hierarchy_fk2
              FOREIGN KEY (child_location_category_id)
                             REFERENCES LOCATION_CATEGORY ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT sub_fk6
              FOREIGN KEY (pre_status_id)
                             REFERENCES REF_SUB_STATUS ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT SUB_FK5
              FOREIGN KEY (locale_cd)
                             REFERENCES REF_LOCALE ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT sub_fk1
              FOREIGN KEY (sub_status_id)
                             REFERENCES REF_SUB_STATUS ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT sub_fk3
              FOREIGN KEY (parent_sub_id)
                             REFERENCES SUB ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT sub_fk4
              FOREIGN KEY (svc_provider_id)
                             REFERENCES SVC_PROVIDER ) ;


ALTER TABLE SUB
       ADD  ( CONSTRAINT sub_fk2
              FOREIGN KEY (sub_typ_id)
                             REFERENCES REF_SUB_TYP ) ;


ALTER TABLE SUB_ADDR
       ADD  ( CONSTRAINT sub_addr_fk2
              FOREIGN KEY (addr_status_id)
                             REFERENCES REF_ADDR_STATUS ) ;


ALTER TABLE SUB_ADDR
       ADD  ( CONSTRAINT sub_addr_fk4
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;


ALTER TABLE SUB_ADDR
       ADD  ( CONSTRAINT sub_addr_fk3
              FOREIGN KEY (addr_typ_id)
                             REFERENCES REF_ADDR_TYP ) ;


ALTER TABLE SUB_ADDR
       ADD  ( CONSTRAINT sub_addr_fk1
              FOREIGN KEY (location_id)
                             REFERENCES LOCATION ) ;


ALTER TABLE SUB_CONTACT
       ADD  ( CONSTRAINT sub_contact_fk1
              FOREIGN KEY (location_id)
                             REFERENCES LOCATION ) ;


ALTER TABLE SUB_CONTACT
       ADD  ( CONSTRAINT sub_contact_fk4
              FOREIGN KEY (contact_status_id)
                             REFERENCES REF_CONTACT_STATUS ) ;


ALTER TABLE SUB_CONTACT
       ADD  ( CONSTRAINT sub_contact_fk3
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;


ALTER TABLE SUB_CONTACT
       ADD  ( CONSTRAINT sub_contact_fk2
              FOREIGN KEY (contact_typ_id)
                             REFERENCES REF_CONTACT_TYP ) ;


ALTER TABLE SUB_CONTACT_PARM
       ADD  ( CONSTRAINT sub_contact_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUB_CONTACT_PARM
       ADD  ( CONSTRAINT sub_contact_parm_fk2
              FOREIGN KEY (sub_contact_id)
                             REFERENCES SUB_CONTACT ) ;


ALTER TABLE SUB_OBJ_ENQ
       ADD  ( CONSTRAINT SUB_OBJ_ENQ_FK4
              FOREIGN KEY (obj_class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE SUB_OBJ_ENQ
       ADD  ( CONSTRAINT SUB_OBJ_ENQ_FK3
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;


ALTER TABLE SUB_OBJ_ENQ
       ADD  ( CONSTRAINT SUB_OBJ_ENQ_FK2
              FOREIGN KEY (sub_ordr_id)
                             REFERENCES SUB_ORDR ) ;


ALTER TABLE SUB_OBJ_ENQ
       ADD  ( CONSTRAINT SUB_OBJ_ENQ_FK1
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT SUB_ORDR_REPLY_FK
              FOREIGN KEY (reply_node_id)
                             REFERENCES SERVER_NODE ) ;


ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT SUB_ORDR_RQ_FK
              FOREIGN KEY (rq_node_id)
                             REFERENCES SERVER_NODE ) ;

ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT sub_ordr_fk2
              FOREIGN KEY (ordr_status_id)
                             REFERENCES REF_ORDR_STATUS ) ;

ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT sub_ordr_fk4
              FOREIGN KEY (ordr_previous_status_id)
                             REFERENCES REF_ORDR_STATUS ) ;

ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT sub_ordr_fk1
              FOREIGN KEY (ordr_typ)
                             REFERENCES REF_ORDR_TYP ) ;


ALTER TABLE SUB_ORDR
       ADD  ( CONSTRAINT sub_ordr_fk3
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;




ALTER TABLE SUB_ORDR_ITEM
       ADD  ( CONSTRAINT SUB_ORDR_ITEM_FK8
              FOREIGN KEY (reason_id)
                             REFERENCES REF_STATUS_CHG_REASON ) ;




ALTER TABLE SUB_ORDR_ITEM
       ADD  ( CONSTRAINT sub_ordr_item_fk3
              FOREIGN KEY (ordr_item_status_id)
                             REFERENCES REF_ORDR_ITEM_STATUS ) ;


ALTER TABLE SUB_ORDR_ITEM
       ADD  ( CONSTRAINT sub_ordr_item_fk2
              FOREIGN KEY (ordr_action_id)
                             REFERENCES REF_ORDR_ACTION ) ;



ALTER TABLE SUB_ORDR_ITEM
       ADD  ( CONSTRAINT sub_ordr_item_fk6
              FOREIGN KEY (sub_ordr_id)
                             REFERENCES SUB_ORDR ) ;


ALTER TABLE SUB_ORDR_ITEM_PARM
       ADD  ( CONSTRAINT SUB_ORDR_ITEM_PARM_FK1
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE SUB_ORDR_ITEM_PARM
       ADD  ( CONSTRAINT SUB_ORDR_ITEM_PARM_FK2
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUB_ORDR_PARM
       ADD  ( CONSTRAINT SUB_ORDR_PARM_FK1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUB_ORDR_PARM
       ADD  ( CONSTRAINT SUB_ORDR_PARM_FK2
              FOREIGN KEY (sub_ordr_id)
                             REFERENCES SUB_ORDR ) ;


ALTER TABLE SUB_PARM
       ADD  ( CONSTRAINT sub_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUB_PARM
       ADD  ( CONSTRAINT sub_parm_fk2
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;



ALTER TABLE SUB_SVC
       ADD  ( CONSTRAINT sub_svc_fk1
              FOREIGN KEY (pre_status_id)
                             REFERENCES REF_SUB_SVC_STATUS ) ;


ALTER TABLE SUB_SVC
       ADD  ( CONSTRAINT sub_svc_fk2
              FOREIGN KEY (sub_svc_status_id)
                             REFERENCES REF_SUB_SVC_STATUS ) ;


ALTER TABLE SUB_SVC
       ADD  ( CONSTRAINT sub_svc_fk6
              FOREIGN KEY (svc_id)
                             REFERENCES SVC ) ;


ALTER TABLE SUB_SVC
       ADD  ( CONSTRAINT sub_svc_fk5
              FOREIGN KEY (parent_sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_SVC
       ADD  ( CONSTRAINT sub_svc_fk3
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;


ALTER TABLE SUB_SVC_ADDR
       ADD  ( CONSTRAINT sub_svc_addr_fk2
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_SVC_ADDR
       ADD  ( CONSTRAINT sub_svc_addr_fk1
              FOREIGN KEY (sub_addr_id)
                             REFERENCES SUB_ADDR ) ;


ALTER TABLE SUB_SVC_DELIVERY_PLAT
       ADD  ( CONSTRAINT sub_svc_delivery_plat_fk2
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_SVC_DELIVERY_PLAT
       ADD  ( CONSTRAINT sub_svc_delivery_plat_fk1
              FOREIGN KEY (svc_delivery_plat_id)
                             REFERENCES REF_SVC_DELIVERY_PLAT ) ;


ALTER TABLE SUB_SVC_DEPY
       ADD  ( CONSTRAINT sub_svc_depy_fk1
              FOREIGN KEY (prerequisite_sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_SVC_DEPY
       ADD  ( CONSTRAINT sub_svc_depy_fk2
              FOREIGN KEY (dependent_sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_SVC_NTWK
       ADD  ( CONSTRAINT SUB_SVC_NTWK_FK3
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE SUB_SVC_NTWK
       ADD  ( CONSTRAINT SUB_SVC_NTWK_FK1
              FOREIGN KEY (ntwk_role_id)
                             REFERENCES REF_NTWK_ROLE ) ;


ALTER TABLE SUB_SVC_NTWK
       ADD  ( CONSTRAINT SUB_SVC_NTWK_FK2
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC_DELIVERY_PLAT ) ;


ALTER TABLE SUB_SVC_NTWK_STAGE
       ADD  ( CONSTRAINT SUB_SVC_NTWK_STAGE_FK3
              FOREIGN KEY (sub_svc_id, sub_ordr_item_id)
                             REFERENCES SUB_SVC_PLAT_STAGE ) ;


ALTER TABLE SUB_SVC_NTWK_STAGE
       ADD  ( CONSTRAINT SUB_SVC_NTWK_STAGE_FK2
              FOREIGN KEY (ntwk_role_id)
                             REFERENCES REF_NTWK_ROLE ) ;


ALTER TABLE SUB_SVC_NTWK_STAGE
       ADD  ( CONSTRAINT SUB_SVC_NTWK_STAGE_FK1
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE SUB_SVC_PARM
       ADD  ( CONSTRAINT sub_svc_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUB_SVC_PARM
       ADD  ( CONSTRAINT sub_svc_parm_fk2
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_SVC_PARM_DEPY ADD (
  CONSTRAINT SUB_SVC_PARM_DEPY_FK1 FOREIGN KEY (DEPY_PARM_ID) 
    REFERENCES PARM (PARM_ID));

ALTER TABLE SUB_SVC_PARM_DEPY ADD (
  CONSTRAINT SUB_SVC_PARM_DEPY_FK2 FOREIGN KEY (DEPY_SUB_SVC_ID) 
    REFERENCES SUB_SVC (SUB_SVC_ID));

ALTER TABLE SUB_SVC_PARM_DEPY ADD (
  CONSTRAINT SUB_SVC_PARM_DEPY_FK3 FOREIGN KEY (SUB_ID) 
    REFERENCES SUB (SUB_ID));

ALTER TABLE SUB_SVC_PARM_DEPY ADD (
  CONSTRAINT SUB_SVC_PARM_DEPY_FK4 FOREIGN KEY (PREREQ_PARM_ID) 
    REFERENCES PARM (PARM_ID));

ALTER TABLE SUB_SVC_PARM_DEPY ADD (
  CONSTRAINT SUB_SVC_PARM_DEPY_FK5 FOREIGN KEY (PREREQ_SUB_SVC_ID) 
    REFERENCES SUB_SVC (SUB_SVC_ID));



ALTER TABLE SUB_SVC_PLAT_STAGE
       ADD  ( CONSTRAINT SUB_SVC_PLAT_STAGE_FK3
              FOREIGN KEY (svc_delivery_plat_id)
                             REFERENCES REF_SVC_DELIVERY_PLAT ) ;


ALTER TABLE SUB_SVC_PLAT_STAGE
       ADD  ( CONSTRAINT SUB_SVC_PLAT_STAGE_FK1
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE SUBNTWK
       ADD  ( CONSTRAINT SUBNTWK_FK1
              FOREIGN KEY (subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE SUBNTWK
       ADD  ( CONSTRAINT subntwk_fk3
              FOREIGN KEY (parent_subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE SUBNTWK
       ADD  ( CONSTRAINT subntwk_fk2
              FOREIGN KEY (topology_status_id)
                             REFERENCES REF_TOPOLOGY_STATUS ) ;


ALTER TABLE SUBNTWK_CARD_LMT
       ADD  ( CONSTRAINT card_lmt_subntwk
              FOREIGN KEY (subntwk_typ_id, subntwk_mdl_id)
                             REFERENCES REF_SUBNTWK_MDL ) ;


ALTER TABLE SUBNTWK_CARD_LMT
       ADD  ( CONSTRAINT card_lmt_mdl
              FOREIGN KEY (card_typ_id, card_mdl_id)
                             REFERENCES CARD_MDL ) ;


ALTER TABLE SUBNTWK_COVERAGE
       ADD  ( CONSTRAINT subntwk_coverage_fk3
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE SUBNTWK_COVERAGE
       ADD  ( CONSTRAINT subntwk_coverage_fk1
              FOREIGN KEY (location_struc_id)
                             REFERENCES LOCATION_STRUC ) ;


ALTER TABLE SUBNTWK_COVERAGE
       ADD  ( CONSTRAINT subntwk_coverage_fk
              FOREIGN KEY (ntwk_location_id)
                             REFERENCES NTWK_LOCATION ) ;


ALTER TABLE SUBNTWK_MDL_LMT
       ADD  ( CONSTRAINT MDL_LMT_SUBNTWK
              FOREIGN KEY (subntwk_typ_id, subntwk_mdl_id)
                             REFERENCES REF_SUBNTWK_MDL ) ;


ALTER TABLE SUBNTWK_MDL_LMT
       ADD  ( CONSTRAINT MDL_LMT_SUBNTWK_PARENT
              FOREIGN KEY (parent_subntwk_typ_id, 
              parent_subntwk_mdl_id)
                             REFERENCES REF_SUBNTWK_MDL ) ;


ALTER TABLE SUBNTWK_MDL_PARM
       ADD  ( CONSTRAINT SUBNTWK_MDL_PARM_FK1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUBNTWK_MDL_PARM
       ADD  ( CONSTRAINT SUBNTWK_MDL_PARM_FK2
              FOREIGN KEY (subntwk_typ_id, subntwk_mdl_id)
                             REFERENCES REF_SUBNTWK_MDL ) ;


ALTER TABLE SUBNTWK_PARM
       ADD  ( CONSTRAINT subntwk_parm_fk2
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE SUBNTWK_PARM
       ADD  ( CONSTRAINT subntwk_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUBNTWK_PARM_STAGE
       ADD  ( CONSTRAINT SUBNTWK_PARM_STAGE_FK3
              FOREIGN KEY (sub_ordr_item_id, sub_svc_id, subntwk_id)
                             REFERENCES SUB_SVC_NTWK_STAGE ) ;


ALTER TABLE SUBNTWK_PARM_STAGE
       ADD  ( CONSTRAINT SUBNTWK_PARM_STAGE_FK2
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUBNTWK_PARM_STAGE
       ADD  ( CONSTRAINT SUBNTWK_PARM_STAGE_FK1
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE SUBNTWK_TYP_FREQ
       ADD  ( CONSTRAINT subntwk_typ_freq_fk2
              FOREIGN KEY (subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE SUBNTWK_TYP_FREQ
       ADD  ( CONSTRAINT subntwk_typ_freq_fk1
              FOREIGN KEY (freq_id)
                             REFERENCES FREQ ) ;


ALTER TABLE SUBNTWK_TYP_FREQ_GRP
       ADD  ( CONSTRAINT subntwk_typ_freq_grp_fk2
              FOREIGN KEY (subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE SUBNTWK_TYP_FREQ_GRP
       ADD  ( CONSTRAINT subntwk_typ_freq_grp_fk1
              FOREIGN KEY (freq_grp_id)
                             REFERENCES FREQ_GRP ) ;



ALTER TABLE SVC
       ADD  ( CONSTRAINT svc_FK4
              FOREIGN KEY (base_svc_id)
                             REFERENCES SVC ) ;


ALTER TABLE SVC_ACTION
       ADD  ( CONSTRAINT svc_action_fk1
              FOREIGN KEY (svc_id)
                             REFERENCES SVC ) ;


ALTER TABLE SVC_ACTION
       ADD  ( CONSTRAINT svc_action_fk2
              FOREIGN KEY (svc_action_typ)
                             REFERENCES REF_SVC_ACTION_TYP ) ;


ALTER TABLE SVC_ACTION
       ADD  ( CONSTRAINT svc_action_fk3
              FOREIGN KEY (base_svc_action_id)
                             REFERENCES SVC_ACTION ) ;


ALTER TABLE SVC_ADVISORY
       ADD  ( CONSTRAINT svc_advisory_fk2
              FOREIGN KEY (subntwk_typ_id)
                             REFERENCES REF_SUBNTWK_TYP ) ;


ALTER TABLE SVC_ADVISORY
       ADD  ( CONSTRAINT svc_advisory_fk4
              FOREIGN KEY (subntwk_id)
                             REFERENCES SUBNTWK ) ;


ALTER TABLE SVC_ADVISORY
       ADD  ( CONSTRAINT svc_advisory_fk3
              FOREIGN KEY (svc_advisory_status_nm)
                             REFERENCES REF_SVC_ADVISORY_STATUS ) ;


ALTER TABLE SVC_ADVISORY
       ADD  ( CONSTRAINT svc_advisory_fk1
              FOREIGN KEY (link_id)
                             REFERENCES LINK ) ;


ALTER TABLE SVC_ADVISORY_PARM
       ADD  ( CONSTRAINT svc_advisory_parm_fk2
              FOREIGN KEY (svc_advisory_id)
                             REFERENCES SVC_ADVISORY ) ;


ALTER TABLE SVC_ADVISORY_PARM
       ADD  ( CONSTRAINT svc_advisory_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SVC_SUPPORTED_PLATFORM
       ADD  ( CONSTRAINT svc_supported_platform_fk1
              FOREIGN KEY (ssn_group_id)
                             REFERENCES REF_SSN_GROUP ) ;


ALTER TABLE SVC_SUPPORTED_PLATFORM
       ADD  ( CONSTRAINT svc_supported_platform_fk3
              FOREIGN KEY (svc_id)
                             REFERENCES SVC ) ;


ALTER TABLE SVC_SUPPORTED_PLATFORM
       ADD  ( CONSTRAINT svc_supported_platform_fk2
              FOREIGN KEY (svc_delivery_plat_id)
                             REFERENCES REF_SVC_DELIVERY_PLAT ) ;


ALTER TABLE SYNC_EXT_PROC_PARMS
       ADD  ( CONSTRAINT sync_action_proc_parms_fk1
              FOREIGN KEY (ext_proc_nm)
                             REFERENCES SYNC_EXT_PROCS ) ;


ALTER TABLE SYNC_GRP_ACTION_PARMS
       ADD  ( CONSTRAINT sync_grp_action_parms_fk1
              FOREIGN KEY (grp_nm, action_seq_within_grp, action)
                             REFERENCES SYNC_GRP_ACTIONS ) ;


ALTER TABLE SYNC_GRP_ACTIONS
       ADD  ( CONSTRAINT sync_action_grp_list_fk1
              FOREIGN KEY (grp_nm)
                             REFERENCES SYNC_GRPS ) ;


ALTER TABLE SYNC_GRP_EXT_PROC_COND
       ADD  ( CONSTRAINT sync_action_ext_proc_cond_fk2
              FOREIGN KEY (grp_nm, action_seq_within_grp, action, 
              check_parm_nm)
                             REFERENCES SYNC_GRP_ACTION_PARMS ) ;


ALTER TABLE SYNC_GRP_EXT_PROC_COND
       ADD  ( CONSTRAINT sync_action_ext_proc_cond_fk1
              FOREIGN KEY (grp_nm, ext_proc_nm)
                             REFERENCES SYNC_GRP_EXT_PROCS ) ;


ALTER TABLE SYNC_GRP_EXT_PROCS
       ADD  ( CONSTRAINT sync_action_ext_procs_fk1
              FOREIGN KEY (grp_nm)
                             REFERENCES SYNC_GRPS ) ;


ALTER TABLE SYNC_GRP_EXT_PROCS
       ADD  ( CONSTRAINT sync_action_ecxt_procs_fk2
              FOREIGN KEY (ext_proc_nm)
                             REFERENCES SYNC_EXT_PROCS ) ;


ALTER TABLE SYNC_GRP_QUALIFIERS
       ADD  ( CONSTRAINT sync_grp_qualifiers_fk1
              FOREIGN KEY (proj_tmplt_impl_nm, proj_tmplt_impl_ver)
                             REFERENCES PROJ_TMPLT_IMPL ) ;


ALTER TABLE SYNC_GRP_QUALIFIERS
       ADD  ( CONSTRAINT sync_grp_qualifiers_fk2
              FOREIGN KEY (grp_nm)
                             REFERENCES SYNC_GRPS ) ;


ALTER TABLE SYNC_GRPS
       ADD  ( CONSTRAINT sync_action_grps_fk1
              FOREIGN KEY (parent_sync_action_grp_nm)
                             REFERENCES SYNC_GRPS ) ;


ALTER TABLE SYNC_SRC_EXT_PARMS
       ADD  ( CONSTRAINT sync_src_ext_parms_fk3
              FOREIGN KEY (scope_grp)
                             REFERENCES SYNC_GRPS ) ;


ALTER TABLE SYNC_SRC_EXT_PARMS
       ADD  ( CONSTRAINT sync_src_ext_src_parms_fk1
              FOREIGN KEY (src_grp_nm, src_action_seq_within_grp, 
              src_action, src_parm_nm)
                             REFERENCES SYNC_GRP_ACTION_PARMS ) ;


ALTER TABLE SYNC_SRC_EXT_PARMS
       ADD  ( CONSTRAINT sync_src_ext_src_parms_fk2
              FOREIGN KEY (ext_proc_nm, ext_proc_parm_nm)
                             REFERENCES SYNC_EXT_PROC_PARMS ) ;


ALTER TABLE TASK
       ADD  ( CONSTRAINT task_fk1
              FOREIGN KEY (proj_id)
                             REFERENCES PROJ ) ;


ALTER TABLE TASK
       ADD  ( CONSTRAINT task_fk3
              FOREIGN KEY (task_tmplt_nm, task_tmplt_ver)
                             REFERENCES TASK_TMPLT ) ;


ALTER TABLE TASK
       ADD  ( CONSTRAINT task_fk2
              FOREIGN KEY (task_status_nm)
                             REFERENCES REF_TASK_STATUS ) ;


ALTER TABLE TASK_ACTION
       ADD  ( CONSTRAINT task_action_fk3
              FOREIGN KEY (parent_task_action_id)
                             REFERENCES TASK_ACTION ) ;


ALTER TABLE TASK_ACTION
       ADD  ( CONSTRAINT task_action_fk1
              FOREIGN KEY (task_tmplt_nm, task_tmplt_ver, 
              action_tmplt_execution_seq)
                             REFERENCES ACTIONS_IN_TASK_TMPLT ) ;


ALTER TABLE TASK_ACTION
       ADD  ( CONSTRAINT task_action_fk2
              FOREIGN KEY (task_id)
                             REFERENCES TASK ) ;


ALTER TABLE TASK_ACTION_PARM
       ADD  ( CONSTRAINT task_action_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE TASK_ACTION_PARM
       ADD  ( CONSTRAINT task_action_parm_fk2
              FOREIGN KEY (ntwk_cmpnt_typ_nm)
                             REFERENCES REF_NTWK_CMPNT_TYP ) ;


ALTER TABLE TASK_ACTION_PARM
       ADD  ( CONSTRAINT task_action_parm_fk3
              FOREIGN KEY (task_action_id)
                             REFERENCES TASK_ACTION ) ;


ALTER TABLE TASK_PARM
       ADD  ( CONSTRAINT task_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE TASK_PARM
       ADD  ( CONSTRAINT task_parm_fk2
              FOREIGN KEY (task_id)
                             REFERENCES TASK ) ;


ALTER TABLE TASK_TMPLT_DEPY
       ADD  ( CONSTRAINT task_tmplt_depy_fk1
              FOREIGN KEY (proj_tmplt_nm, proj_tmplt_ver, 
              task_tmplt_nm, task_tmplt_ver)
                             REFERENCES TASKS_IN_PROJ_TMPLT ) ;


ALTER TABLE TASK_TMPLT_DEPY
       ADD  ( CONSTRAINT task_tmplt_depy_fk2
              FOREIGN KEY (proj_tmplt_nm, proj_tmplt_ver, 
              dependent_task_tmplt_nm, dependent_task_tmplt_ver)
                             REFERENCES TASKS_IN_PROJ_TMPLT ) ;


ALTER TABLE TASK_TMPLT_PARM
       ADD  ( CONSTRAINT task_tmplt_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE TASK_TMPLT_PARM
       ADD  ( CONSTRAINT task_tmplt_parm_fk2
              FOREIGN KEY (task_tmplt_nm, task_tmplt_ver)
                             REFERENCES TASK_TMPLT ) ;


ALTER TABLE TASK_TRANSITIONS
       ADD  ( CONSTRAINT task_transitions_fk1
              FOREIGN KEY (to_task_status_nm)
                             REFERENCES REF_TASK_STATUS ) ;


ALTER TABLE TASK_TRANSITIONS
       ADD  ( CONSTRAINT task_transitions_fk2
              FOREIGN KEY (from_task_status_nm)
                             REFERENCES REF_TASK_STATUS ) ;


ALTER TABLE TASKS_IN_PROJ_TMPLT
       ADD  ( CONSTRAINT tasks_in_proj_tmplt_fk2
              FOREIGN KEY (task_tmplt_nm, task_tmplt_ver)
                             REFERENCES TASK_TMPLT ) ;


ALTER TABLE TASKS_IN_PROJ_TMPLT
       ADD  ( CONSTRAINT tasks_in_proj_tmplt_fk1
              FOREIGN KEY (proj_tmplt_nm, proj_tmplt_ver)
                             REFERENCES PROJ_TMPLT ) ;


ALTER TABLE TECH_PLATFORM
       ADD  ( CONSTRAINT tech_platform_fk1
              FOREIGN KEY (topology_status_id)
                             REFERENCES REF_TOPOLOGY_STATUS ) ;


ALTER TABLE TECH_PLATFORM
       ADD  ( CONSTRAINT tech_platform_fk2
              FOREIGN KEY (tech_platform_typ_id)
                             REFERENCES TECH_PLATFORM_TYP ) ;


ALTER TABLE TECH_PLATFORM_PARM
       ADD  ( CONSTRAINT tech_platform_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE TECH_PLATFORM_PARM
       ADD  ( CONSTRAINT tech_platform_parm_fk2
              FOREIGN KEY (tech_platform_id)
                             REFERENCES TECH_PLATFORM ) ;


ALTER TABLE TROUBLE_TICKET
       ADD  ( CONSTRAINT trouble_ticket_fk2
              FOREIGN KEY (ticket_nm)
                             REFERENCES REF_TICKET_TYP ) ;


ALTER TABLE TROUBLE_TICKET
       ADD  ( CONSTRAINT trouble_ticket_fk1
              FOREIGN KEY (ticket_status_nm)
                             REFERENCES REF_TICKET_STATUS ) ;


ALTER TABLE TROUBLE_TICKET_PARM
       ADD  ( CONSTRAINT trouble_ticket_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE TROUBLE_TICKET_PARM
       ADD  ( CONSTRAINT trouble_ticket_parm_fk2
              FOREIGN KEY (ticket_id)
                             REFERENCES TROUBLE_TICKET ) ;


ALTER TABLE WRK_UNIQUE_PARM
       ADD  ( CONSTRAINT wrk_unique_parm_fk2
              FOREIGN KEY (sub_ordr_id)
                             REFERENCES SUB_ORDR ) ;


ALTER TABLE WRK_UNIQUE_PARM
       ADD  ( CONSTRAINT wrk_unique_parm_fk1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;



ALTER TABLE ORDR_ITEM_ASSOC_CHG
       ADD  ( CONSTRAINT ORDR_ITEM_ASSOC_CHG_FK1
              FOREIGN KEY (sub_ordr_item_id)
                             REFERENCES SUB_ORDR_ITEM ) ;


ALTER TABLE ORDR_ITEM_ASSOC_CHG
       ADD  ( CONSTRAINT ORDR_ITEM_ASSOC_CHG_FK2
              FOREIGN KEY (assoc_object_typ_id)
                             REFERENCES REF_OBJECT_TYP ) ;


ALTER TABLE ORDR_ITEM_ASSOC_CHG
       ADD  ( CONSTRAINT ORDR_ITEM_ASSOC_CHG_FK4
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE ORDR_ITEM_ASSOC_CHG
       ADD  ( CONSTRAINT ORDR_ITEM_ASSOC_CHG_FK5
              FOREIGN KEY (assoc_parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE ORDR_ITEM_ASSOC_CHG
       ADD  ( CONSTRAINT ORDR_ITEM_ASSOC_CHG_FK3
              FOREIGN KEY (assoc_typ_id)
                             REFERENCES REF_ASSOC_TYP ) ;


ALTER TABLE REF_OBJECT_TYP
       ADD  ( CONSTRAINT REF_OBJECT_TYP_FK1
              FOREIGN KEY (class_id)
                             REFERENCES REF_CLASS ) ;


ALTER TABLE SUB_ORDR_ITEM
       ADD  ( CONSTRAINT SUB_ORDR_ITEM_FK7
              FOREIGN KEY (object_typ_id)
                             REFERENCES REF_OBJECT_TYP ) ;


ALTER TABLE REF_USER_STATUS
       ADD  ( CONSTRAINT REF_USER_STATUS_PK PRIMARY KEY (
              user_status_id) ) ;



ALTER TABLE SUB_USER
       ADD  ( CONSTRAINT SUB_USER_PK PRIMARY KEY (sub_user_id) ) ;


ALTER TABLE SUB_USER_PARM
       ADD  ( CONSTRAINT SUB_USER_PARM_PK PRIMARY KEY (sub_user_id, 
              parm_id) ) ;


ALTER TABLE SUB_USER_SVC
       ADD  ( CONSTRAINT SUB_USER_SVC_PK PRIMARY KEY (sub_user_id, 
              sub_svc_id, assoc_rule_id) ) ;


ALTER TABLE SUB_USER
       ADD  ( CONSTRAINT SUB_USER_FK1
              FOREIGN KEY (object_typ_id)
                             REFERENCES REF_OBJECT_TYP ) ;


ALTER TABLE SUB_USER
       ADD  ( CONSTRAINT SUB_USER_FK2
              FOREIGN KEY (user_status_id)
                             REFERENCES REF_USER_STATUS ) ;


ALTER TABLE SUB_USER
       ADD  ( CONSTRAINT SUB_USER_FK3
              FOREIGN KEY (sub_id)
                             REFERENCES SUB ) ;


ALTER TABLE SUB_USER_PARM
       ADD  ( CONSTRAINT SUB_USER_PARM_FK1
              FOREIGN KEY (parm_id)
                             REFERENCES PARM ) ;


ALTER TABLE SUB_USER_PARM
       ADD  ( CONSTRAINT SUB_USER_PARM_FK2
              FOREIGN KEY (sub_user_id)
                             REFERENCES SUB_USER ) ;


ALTER TABLE SUB_USER_SVC
       ADD  ( CONSTRAINT SUB_USER_SVC_FK1
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;


ALTER TABLE SUB_USER_SVC
       ADD  ( CONSTRAINT SUB_USER_SVC_FK2
              FOREIGN KEY (sub_user_id)
                             REFERENCES SUB_USER ) ;
ALTER TABLE SUB_USER_SVC
       ADD  ( CONSTRAINT SUB_USER_SVC_FK3
              FOREIGN KEY (assoc_rule_id)
                             REFERENCES REF_ASSOC_RULE ) ;

ALTER TABLE SUB_SVC_ASSOC
       ADD  ( CONSTRAINT SUB_SVC_ASSOC_PK PRIMARY KEY (sub_svc_id,assoc_rule_id,assoc_sub_svc_id) ) ;

ALTER TABLE SUB_SVC_ASSOC
       ADD  ( CONSTRAINT sub_svc_assoc_fk1
              FOREIGN KEY (sub_svc_id)
                             REFERENCES SUB_SVC ) ;

ALTER TABLE SUB_SVC_ASSOC
       ADD  ( CONSTRAINT sub_svc_assoc_fk2
              FOREIGN KEY (assoc_sub_svc_id)
                             REFERENCES SUB_SVC ) ;

ALTER TABLE SUB_SVC_ASSOC
       ADD  ( CONSTRAINT sub_svc_assoc_fk3
              FOREIGN KEY (assoc_rule_id)
                             REFERENCES REF_ASSOC_RULE ) ;

ALTER TABLE ORDR_NOTIFY_ITEM
     ADD ( CONSTRAINT ORDR_NOTIFY_ITEM_FK1
    FOREIGN KEY (SUB_ORDR_ITEM_ID)
   REFERENCES SUB_ORDR_ITEM (SUB_ORDR_ITEM_ID));

ALTER TABLE WSNOTIFY_REG 
       ADD ( CONSTRAINT WSNOTIFY_REG_PK PRIMARY KEY (ID));

  
ALTER TABLE ref_assoc_rule ADD (
  CONSTRAINT ref_assoc_rule_FK1  FOREIGN KEY (assoc_typ_id)  
     REFERENCES REF_ASSOC_TYP (ASSOC_TYP_ID));

ALTER TABLE ref_assoc_rule ADD (
  CONSTRAINT ref_assoc_rule_FK2 FOREIGN KEY (aend_object_typ_id)  
     REFERENCES REF_OBJECT_TYP (OBJECT_TYP_ID));
 
 ALTER TABLE ref_assoc_rule ADD (
  CONSTRAINT ref_assoc_rule_FK3  FOREIGN KEY (zend_object_typ_id)  
     REFERENCES REF_OBJECT_TYP (OBJECT_TYP_ID));


create trigger ref_status_tai
  AFTER INSERT
  on REF_STATUS
  
  for each row

declare

v_class_nm ref_class.CLASS_NM%type;

BEGIN

select ref_class.CLASS_NM into v_class_nm from ref_class

where ref_class.CLASS_ID=:NEW.class_id;

if (v_class_nm='SubSpec')

then

insert into Ref_Sub_Status(SUB_STATUS_ID) values(:NEW.status_id);

elsif (v_class_nm='SubSvcSpec')

then

insert into Ref_Sub_Svc_Status(SUB_SVC_STATUS_ID) values(:NEW.status_id);

elsif (v_class_nm='Order')

then

insert into Ref_Ordr_Status(ORDR_STATUS_ID) values(:NEW.status_id);

elsif (v_class_nm='OrderItem')

then

insert into Ref_Ordr_Item_Status(ORDR_ITEM_STATUS_ID) values(:NEW.status_id);

elsif (v_class_nm='SubContactSpec')

then

insert into Ref_Contact_Status(CONTACT_STATUS_ID) values(:NEW.status_id);

elsif (v_class_nm='SubAddressSpec')

then

insert into Ref_Addr_Status(ADDR_STATUS_ID) values(:NEW.status_id);

elsif (v_class_nm='SubUserSpec')

then

insert into Ref_User_Status(USER_STATUS_ID) values(:NEW.status_id);

end if;

END;

/



create trigger server_node_bi
  BEFORE INSERT
  on SERVER_NODE
  
  for each row
/* ERwin Builtin Wed Jul 27 13:59:47 2005 */
begin
   :new.created_dtm := sysdate;
end;
/


create trigger subntwk_tbiu
  BEFORE INSERT or UPDATE OF 
         subntwk_nm
  on SUBNTWK
  
  for each row
begin
  :new.upper_subntwk_nm := upper(rtrim(:new.subntwk_nm));

end;
/




create or replace trigger sync_debug_tbi
  BEFORE INSERT
  on SYNC_DEBUG
  
  for each row
begin
 select sync_debug_seq.nextval into :new.sync_debug_seq from dual;
end;
/


CREATE OR REPLACE TRIGGER SUB_ORDR_EMPTY_LOB_TR1
BEFORE UPDATE
OF ORDR_STATUS_ID
ON SUB_ORDR 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
status_nm varchar2(100);
/******************************************************************************
   NAME:       SUB_ORDR_EMPTY_LOB_TR1
   PURPOSE:    Empty BLOB field content when an order become closed 

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/10/2006    Dan Tsipe         1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     SUB_ORDR_EMPTY_LOB_TR1
      Sysdate:         1/10/2006
      Date and Time:   1/10/2006, 12:08:52 PM, and 1/10/2006 12:08:52 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      SUB_ORDR (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/

BEGIN
select status into status_nm from ref_status where status_id=:NEW.ordr_status_id;
if status_nm = 'closed.completed.all' Then
   :NEW.ORDR_REQ:=empty_clob();
end if;
END SUB_ORDR_EMPTY_LOB_TR1;
/








