--============================================================================
--    $Id: smp_pack_indexes.sql,v 1.1 2003/05/14 20:20:13 vadimg Exp $
--
--  PURPOSE of this script to adjust index parameters to compact
--  a database
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================


alter index SUB_SVC_PARM_IX1        rebuild compress 1;

alter index SUB_SVC_PARM_IX2        rebuild compress 2;

alter index SUB_SVC_STATUS_HIST_PK  rebuild compress 1;

alter index SUB_SVC_NTWK_IX1        rebuild compress 1;

alter index SUB_SVC_STATUS_HIST_IX1 rebuild compress 2;

alter index SUB_SVC_NTWK_IX2        rebuild compress 1;

alter index SUB_SVC_NTWK_PK         rebuild compress 1;

alter index SUB_ORDR_ITEM_IX1       rebuild compress 1;

alter index SUB_SVC_IX5             rebuild compress 1;

alter index SUB_SVC_ADDR_AK1        rebuild compress 2;

alter index SUB_SVC_IX6             rebuild compress 2;

alter index SUB_ORDR_ITEM_IX2       rebuild compress 1;

alter index SUB_SVC_IX2             rebuild compress 1;

alter index SUB_SVC_IX4             rebuild compress 1;

alter index LOCATION_DTL_IX1        rebuild compress 2;

alter index CONT_MTHD_STATUS_HIST_IX1 rebuild compress 2;

alter index CONT_MTHD_STATUS_HIST_PK rebuild compress 1;

alter index LOCATION_DTL_PK         rebuild compress 1;

alter index SUBNTWK_PARM_PK         rebuild compress 1;

alter index SUB_PARM_PK             rebuild compress 1;

alter index SUB_PARM_IX1            rebuild compress 2;
