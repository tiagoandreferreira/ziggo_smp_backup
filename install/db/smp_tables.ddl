CREATE TABLE ACTION_TMPLT (
       action_tmplt_nm      VARCHAR2(30) NOT NULL,
       action_tmplt_ver     NUMBER(12) NOT NULL,
       topology_action_typ  VARCHAR2(30) NOT NULL,
       action_tmplt_descr   VARCHAR2(500) NULL,
       jmf_vld              VARCHAR2(4000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE ACTION_TMPLT_PARM (
       action_tmplt_parm_id NUMBER(12) NOT NULL,
       action_tmplt_nm      VARCHAR2(30) NULL,
       action_tmplt_ver     NUMBER(12) NULL,
       ntwk_cmpnt_typ_nm    VARCHAR2(30) NULL,
       parm_id              NUMBER(12) NOT NULL,
       is_out_parm          CHAR(1) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE ACTIONS_IN_TASK_TMPLT (
       task_tmplt_nm        VARCHAR2(30) NOT NULL,
       task_tmplt_ver       NUMBER(12) NOT NULL,
       execution_seq        NUMBER(3) NOT NULL,
       action_tmplt_nm      VARCHAR2(30) NOT NULL,
       action_tmplt_ver     NUMBER(12) NOT NULL,
       repeat_jmf_expr      VARCHAR2(4000) NULL,
       repeat_parm_id       NUMBER(12) NULL,
       screen_nm            VARCHAR2(100) NULL,
       parent_execution_seq NUMBER(3) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE ARCH_ADM (
       adm_nm               VARCHAR2(30) NOT NULL,
       pswd                 VARCHAR2(80) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE ARCH_GRP (
       grp_id               NUMBER(12) NOT NULL,
       grp_nm               VARCHAR2(30) NOT NULL,
       tgt_tablespace       VARCHAR2(30) NULL,
       status               VARCHAR2(10) NOT NULL
                                   CONSTRAINT arch_status69
                                          CHECK (status IN ('active', 'stop')),
       last_run             DATE NULL,
       next_run             DATE NULL,
       root_tbl_nm          VARCHAR2(30) NULL,
       root_arch_mode       VARCHAR2(5) NULL
);


CREATE TABLE ARCH_GRP_PARM (
       grp_id               NUMBER(12) NOT NULL,
       parm_nm              VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(255) NOT NULL
);


CREATE TABLE ARCH_GRP_PART (
       grp_id               NUMBER(12) NOT NULL,
       part_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       last_run             DATE NULL,
       status               VARCHAR2(20) NOT NULL
                                   CONSTRAINT arch_part_stat34
                                          CHECK (status IN ('current', 'closed')),
       first_run            DATE NULL
);


CREATE TABLE ARCH_PARM_HIST (
       modified_dtm         DATE NOT NULL,
       mod_tbl_nm           VARCHAR2(20) NOT NULL,
       parm_hist_id         NUMBER(12) NOT NULL,
       act                  VARCHAR2(20) NULL,
       old_val              sys.XMLType NULL,
       modified_by          VARCHAR2(30) NULL,
       status               VARCHAR(1) NOT NULL
);


CREATE TABLE ARCH_RUN (
       grp_id               NUMBER(12) NOT NULL,
       run_id               NUMBER(12) NOT NULL,
       started_dtm          DATE NULL,
       ended_dtm            DATE NULL,
       status               VARCHAR2(1) NULL
                                   CONSTRAINT run_status34
                                          CHECK (status IN ('s', 'f', 'r')),
       ses_id               VARCHAR2(30) NULL,
       total_cnt            NUMBER NULL,
       done_cnt             NUMBER NULL,
       err_msg              VARCHAR2(1000) NULL
);


CREATE TABLE ARCH_STACK (
       ref_role             VARCHAR2(30) NOT NULL
);


CREATE TABLE ARCH_TBL (
       grp_id               NUMBER(12) NOT NULL,
       tbl_nm               VARCHAR2(30) NOT NULL,
       filter               VARCHAR2(500) NULL,
       status               VARCHAR2(10) NOT NULL
                                   CONSTRAINT arch_status70
                                          CHECK (status IN ('active', 'stop')),
       tmplt                CLOB NULL
);


CREATE TABLE ARCH_TBL_LOG (
       grp_id               NUMBER(12) NOT NULL,
       run_id               NUMBER(12) NOT NULL,
       tbl_nm               VARCHAR2(30) NOT NULL,
       part_id              NUMBER(12) NOT NULL,
       started_dtm          DATE NOT NULL,
       ended_dtm            DATE NULL,
       rec_cnt              NUMBER NULL
);


CREATE TABLE ARCH_TBL_NET (
       grp_id               NUMBER(12) NOT NULL,
       src_tbl_nm           VARCHAR2(30) NOT NULL,
       tgt_tbl_nm           VARCHAR2(30) NOT NULL,
       ref_role             VARCHAR2(30) NOT NULL,
       arch_mode            VARCHAR2(5) NOT NULL
                                   CONSTRAINT arch_mode36
                                          CHECK (arch_mode IN ('move', 'copy', 'purge')),
       src_ref              VARCHAR2(10) NOT NULL
                                   CONSTRAINT arch_ref69
                                          CHECK (src_ref IN ('none', 'restrict', 'cascade')),
       tgt_ref              VARCHAR2(10) NOT NULL
                                   CONSTRAINT arch_ref70
                                          CHECK (tgt_ref IN ('none', 'restrict', 'cascade')),
       join_clause          VARCHAR2(255) NOT NULL,
       tgt_typ              VARCHAR2(1) NOT NULL
);


CREATE TABLE ARCH_TBL_PART (
       grp_id               NUMBER(12) NOT NULL,
       tbl_nm               VARCHAR2(30) NOT NULL,
       part_id              NUMBER(12) NOT NULL,
       arch_tbl_nm          VARCHAR2(30) NULL
);


CREATE TABLE CARD (
       card_id              NUMBER(12) NOT NULL,
       card_typ_id          NUMBER(12) NULL,
       subntwk_id           NUMBER(12) NOT NULL,
       srl_num              VARCHAR2(255) NOT NULL,
       chassis_slot_num     VARCHAR2(255) NULL,
       external_key         VARCHAR2(100) NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       card_mdl_id          NUMBER(12) NULL
);


CREATE TABLE CARD_MDL (
       card_typ_id          NUMBER(12) NOT NULL,
       card_mdl_id          NUMBER(12) NOT NULL,
       card_mdl_nm          VARCHAR2(20) NULL,
       max_port_num_up      NUMBER NULL,
       max_port_num_down    NUMBER NULL,
       max_port_num_any     NUMBER NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE CFG_VERSION (
       cfg_nm               VARCHAR2(80) NOT NULL,
       version              NUMBER(12) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE CONSUMABLE_RSRC_MGMT (
       provider_subntwk_typ_id NUMBER(12) NOT NULL,
       consumer_subntwk_typ_id NUMBER(12) NOT NULL,
       svc_delivery_plat_id NUMBER(12) NOT NULL,
       consumable_rsrc_typ_nm VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE EXT_PARM_MAPPING (
       stm_parm_nm          VARCHAR2(255) NOT NULL,
       ext_parm_nm          VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE FREQ (
       freq_id              NUMBER(12) NOT NULL,
       comm_direction_id    NUMBER(12) NULL,
       freq_hz              NUMBER(9) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE FREQ_GRP (
       freq_grp_id          NUMBER(12) NOT NULL,
       freq_grp_nm          VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE FREQ_GRP_LIST (
       freq_grp_id          NUMBER(12) NOT NULL,
       freq_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE IP_ADDR (
       ip_addr              VARCHAR2(20) NOT NULL,
       assigned_subntwk_id  NUMBER(12) NULL,
       ip_sbnt_id           NUMBER(12) NOT NULL,
       ip_status_nm         VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE IP_BLOCK (
       ip_block_id          NUMBER(12) NOT NULL,
       ip_block_nm          VARCHAR2(30) NOT NULL,
       start_ip_addr        VARCHAR2(20) NOT NULL,
       mask                 VARCHAR2(20) NOT NULL,
       managing_subntwk_id  NUMBER(12) NULL,
       ip_status_nm         VARCHAR2(30) NOT NULL,
       assignable_hosts     NUMBER(9) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE IP_SBNT (
       ip_sbnt_id           NUMBER(12) NOT NULL,
       ip_svc_provider_id   NUMBER(12) NULL,
       descr                VARCHAR2(500) NULL,
       ip_status_nm         VARCHAR2(30) NULL,
       ip_sbnt_addr         VARCHAR2(255) NOT NULL,
       ip_sbnt_msk          VARCHAR2(255) NOT NULL,
       ip_block_id          NUMBER(12) NOT NULL,
       ip_sbnt_gateway_addr VARCHAR2(20) NULL,
       ip_broadcast_addr    VARCHAR2(20) NULL,
       managing_subntwk_id  NUMBER(12) NULL,
       ip_max_hosts         NUMBER(9) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE LBL (
       class_id             NUMBER(12) NOT NULL,
       obj_nm               VARCHAR2(255) NOT NULL,
       locale_cd            VARCHAR2(12) NOT NULL,
       lbl_txt              VARCHAR2(2000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
);


CREATE TABLE LINE_ITEM_DEPY (
       sub_ordr_id          NUMBER(12) NOT NULL,
       prereq_item_id       NUMBER(12) NOT NULL,
       depy_item_id         NUMBER(12) NOT NULL,
       type                 NUMBER(6) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL
);


CREATE TABLE LINK (
       link_id              NUMBER(12) NOT NULL,
       link_nm              VARCHAR2(100) NULL,
       from_subntwk_id      NUMBER(12) NOT NULL,
       to_subntwk_id        NUMBER(12) NOT NULL,
       link_typ_id          NUMBER(12) NOT NULL,
       topology_status_id   NUMBER(12) NOT NULL,
       selection_priority   NUMBER(6) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE LINK_PARM (
       link_id              NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE LOCATION (
       location_id          NUMBER(12) NOT NULL,
       location_nm          VARCHAR2(100) NULL,
       external_key         VARCHAR2(100) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE LOCATION_CATEGORY (
       location_category_id NUMBER(12) NOT NULL,
       location_category_nm VARCHAR2(100) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE LOCATION_DTL (
       location_id          NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       location_dtl_val     VARCHAR2(255) NOT NULL
);


CREATE TABLE LOCATION_STRUC (
       location_struc_id    NUMBER(12) NOT NULL,
       location_category_id NUMBER(12) NOT NULL,
       location_struc_nm    VARCHAR2(100) NOT NULL,
       ext_location_ref     VARCHAR2(255) NULL,
       is_ntwk_only         CHAR(1) NOT NULL,
       parent_location_struc_id NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE LOCKED_RESOURCE (
       cmpnt_id             VARCHAR2(50) NOT NULL,
       ntwk_cmpnt_typ_nm    VARCHAR2(100) NOT NULL,
       proj_id              NUMBER(12) NOT NULL,
       lock_typ             VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);




CREATE TABLE NTWK_CMPNT_CHG_IMPACT (
       ntwk_cmpnt_chg_impact_id NUMBER(12) NOT NULL,
       topology_action_typ  VARCHAR2(30) NOT NULL,
       ntwk_cmpnt_typ_nm    VARCHAR2(30) NOT NULL,
       sub_cmpnt_typ_nm     VARCHAR2(30) NOT NULL,
       jmf_cond             VARCHAR2(2000) NULL,
       ntwk_action_cmd      VARCHAR2(100) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE NTWK_CMPNT_TYP_PARM (
       ntwk_cmpnt_typ_nm    VARCHAR2(30) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE NTWK_LOCATION (
       ntwk_location_id     NUMBER(12) NOT NULL,
       ntwk_location_nm     VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE NTWK_LOCATION_DTLS (
       ntwk_location_id     NUMBER(12) NOT NULL,
       location_category_id NUMBER(12) NOT NULL,
       location_dtl_val     VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE NTWK_LOCATION_STRUC (
       ntwk_location_id     NUMBER(12) NOT NULL,
       location_struc_id    NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE NTWK_RESOURCE (
       rsrc_id              number(12) NOT NULL,
       rsrc_nm              varchar2(100) NOT NULL,
       sub_svc_id           number(12) NULL,
       subntwk_id           number(12) NULL,
       consumable_rsrc_typ_nm VARCHAR2(30) NOT NULL,
       rsrc_state           varchar2(50) NOT NULL,
       rsrc_category        varchar2(50) NULL,
       created_dtm          date DEFAULT sysdate NULL,
       created_by           varchar2(30) NOT NULL,
       prev_sub_svc_id      NUMBER(12) NULL,
       prev_rsrc_state      varchar2(50) NULL,
       modified_dtm         date NULL,
       modified_by          VARCHAR2(30) NULL
);

CREATE TABLE NTWK_RESOURCE_PARM (
       rsrc_id              number(12) NOT NULL,
       parm_id              number(12) NOT NULL,
       val                  varchar2(300) NOT NULL,
       created_dtm          date DEFAULT sysdate NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         date NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE ORDR_ITEM_ENTITY_CHG (
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       need_upd             CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(2000) NULL,
       old_val              VARCHAR2(2000) NULL,
       sub_entity_id        NUMBER(12) NULL,
       LONGTEXT_VAL         CLOB NULL,
       LONGTEXT_OLD_VAL     CLOB NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PARM (
       parm_id              NUMBER(12) NOT NULL,
       conversion_typ       VARCHAR2(30) NULL,
       class_id             NUMBER(12) NOT NULL,
       parm_nm              VARCHAR2(255) NOT NULL,
       object_id            NUMBER(12) NULL,
       data_typ_nm          VARCHAR2(30) NOT NULL,
       capture_seq_num      NUMBER(9) NULL,
       base_parm_id         NUMBER(12) NULL,
       is_read_only         CHAR(1) NULL,
       is_required          CHAR(1) NULL,
       is_primary_key       CHAR(1) NULL,
       is_reserved          CHAR(1) NULL,
       is_unique            CHAR(1) NULL,
       dflt_val             VARCHAR2(2000) NULL,
       jmf_src              VARCHAR2(2000) NULL,
       jmf_dflt_val         VARCHAR2(2000) NULL,
       jmf_display_format   VARCHAR2(4000) NULL,
       db_col_nm            VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PARM_PERMITTED_VAL (
       parm_id              NUMBER(12) NOT NULL,
       jmf_permitted_val_expr VARCHAR2(2000) NULL,
       val                  VARCHAR2(1000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PARM_VLD_RULE (
       parm_id              NUMBER(12) NOT NULL,
       vld_rule_id          NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PARMVAL_EXCLUSION_GRP (
       parmval_exclusion_grp_nm VARCHAR2(30) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PORT (
       port_id              NUMBER(12) NOT NULL,
       card_id              NUMBER(12) NOT NULL,
       freq_grp_id          NUMBER(12) NULL,
       freq_id              NUMBER(12) NULL,
       comm_direction_id    NUMBER(12) NULL,
       port_descr           VARCHAR2(255) NULL,
       upstrm_chnl_id       VARCHAR2(255) NULL,
       external_key         VARCHAR2(100) NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PORT_PAIR (
       downstream_port_id   NUMBER(12) NOT NULL,
       upstream_port_id     NUMBER(12) NOT NULL,
       port_pair_info       VARCHAR2(500) NULL,
       external_key         VARCHAR2(100) NULL,
       is_free              CHAR(1) NOT NULL,
       managing_subntwk_id  NUMBER(12) NOT NULL,
       comments             VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PORT_PAIR_LINK (
       link_id              NUMBER(12) NOT NULL,
       downstream_port_id   NUMBER(12) NOT NULL,
       upstream_port_id     NUMBER(12) NOT NULL,
       comments             VARCHAR2(500) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PROJ (
       proj_id              NUMBER(12) NOT NULL,
       proj_nm              VARCHAR2(300) NOT NULL,
       proj_descr           VARCHAR2(500) NULL,
       proj_status_nm       VARCHAR2(30) NOT NULL,
       proj_tmplt_impl_nm   VARCHAR2(50) NULL,
       proj_tmplt_impl_ver  NUMBER(12) NULL,
       start_dt             DATE NOT NULL,
       completion_dt        DATE NULL,
       effective_dt         DATE NULL,
       ext_proj_key         VARCHAR2(100) NULL,
       locked_by            VARCHAR2(30) NULL,
       last_error_msg       VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       hint_subntwk_id      NUMBER(12) NULL
);


CREATE TABLE PROJ_IMPACTED_SUB_SVC (
       proj_id              NUMBER(12) NOT NULL,
       sub_svc_id           NUMBER(12) NOT NULL,
       reprov_status        VARCHAR2(30) NULL,
       err_msg              VARCHAR2(500) NULL,
       subntwk_info         VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PROJ_PARM (
       proj_id              NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(4000) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PROJ_TMPLT (
       proj_tmplt_nm        VARCHAR2(50) NOT NULL,
       proj_tmplt_ver       NUMBER(12) NOT NULL,
       proj_tmplt_descr     VARCHAR2(500) NULL,
       ver_descr            VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PROJ_TMPLT_IMPL (
       proj_tmplt_impl_nm   VARCHAR2(50) NOT NULL,
       proj_tmplt_impl_ver  NUMBER(12) NOT NULL,
       proj_tmplt_impl_descr VARCHAR2(500) NULL,
       proj_tmplt_nm        VARCHAR2(50) NOT NULL,
       proj_tmplt_ver       NUMBER(12) NOT NULL,
       sync_reqd            CHAR(1) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       req_context          VARCHAR2(3) NOT NULL
                                   CONSTRAINT yes_no
                                          CHECK (req_context IN ('y', 'n'))
);


CREATE TABLE PROJ_TMPLT_IMPL_PARM (
       proj_tmplt_parm_id   NUMBER(12) NOT NULL,
       proj_tmplt_impl_nm   VARCHAR2(50) NOT NULL,
       proj_tmplt_impl_ver  NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(2000) NULL,
       src_parm_id          NUMBER(12) NULL,
       jmf_src              VARCHAR2(4000) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PROJ_TMPLT_PARM (
       proj_tmplt_parm_id   NUMBER(12) NOT NULL,
       proj_tmplt_nm        VARCHAR2(50) NULL,
       proj_tmplt_ver       NUMBER(12) NULL,
       parm_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE PROJ_TRANSITIONS (
       from_proj_status_nm  VARCHAR2(30) NOT NULL,
       to_proj_status_nm    VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);





CREATE TABLE REF_ADDR_STATUS (
       addr_status_id       NUMBER(12) NOT NULL
);


CREATE TABLE REF_ADDR_TYP (
       addr_typ_id          NUMBER(12) NOT NULL,
       addr_typ_nm          VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_ARCH_PARM (
       parm_nm              VARCHAR2(30) NOT NULL,
       val                  VARCHAR2(255) NULL,
       is_required          VARCHAR2(1) NOT NULL
                                   CONSTRAINT yesno33
                                          CHECK (is_required IN ('Y', 'N')),
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_CARD_TYP (
       card_typ_id          NUMBER(12) NOT NULL,
       card_typ_nm          VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_CLASS (
       class_id             NUMBER(12) NOT NULL,
       max_obj_id           NUMBER(12) NULL,
       class_nm             VARCHAR2(255) NOT NULL,
       db_tbl_nm            VARCHAR2(32) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
);


CREATE TABLE REF_COMM_DIRECTION (
       comm_direction_id    NUMBER(12) NOT NULL,
       comm_direction_nm    VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_CONSUMABLE_RSRC_TYP (
       consumable_rsrc_typ_nm VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_CONTACT_STATUS (
       contact_status_id    NUMBER(12) NOT NULL
);


CREATE TABLE REF_CONTACT_TYP (
       contact_typ_id       NUMBER(12) NOT NULL,
       contact_typ_nm       VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_CONVERSION_TYP (
       conversion_typ       VARCHAR2(30) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_DATA_TYP (
       data_typ_nm          VARCHAR2(30) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_EVENT (
       event_nm             VARCHAR2(50) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_IP_STATUS (
       ip_status_nm         VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_IP_SVC_PROVIDER (
       ip_svc_provider_id   NUMBER(12) NOT NULL,
       ip_svc_provider_nm   VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_LANG (
       lang_cd              VARCHAR2(255) NOT NULL,
       is_dflt              CHAR(1) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_LINK_TYP (
       link_typ_id          NUMBER(12) NOT NULL,
       link_typ_nm          VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_LOCALE (
       locale_cd            VARCHAR2(12) NOT NULL,
       lang_cd              VARCHAR2(255) NOT NULL,
       locale_country_cd    VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_LOCALE_COUNTRY (
       locale_country_cd    VARCHAR2(255) NOT NULL,
       is_default           CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_LOCK_TYP (
       lock_typ             VARCHAR2(30) NOT NULL,
       lock_typ_descr       VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_MGMT_MODE (
       mgmt_mode_id         NUMBER(12) NOT NULL,
       mgmt_mode_nm         VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
);


CREATE TABLE REF_NTWK_CMPNT_TYP (
       ntwk_cmpnt_typ_nm    VARCHAR2(30) NOT NULL,
       ntwk_cmpnt_typ_descr VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_NTWK_ROLE (
       ntwk_role_id         NUMBER(12) NOT NULL,
       ntwk_role_nm         VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_NTWK_TYP (
       ntwk_typ_id          NUMBER(12) NOT NULL,
       ntwk_typ_nm          VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_ORDR_ACTION (
       ordr_action_id       NUMBER(12) NOT NULL,
       ordr_action_nm       VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_ORDR_ITEM_STATUS (
       ordr_item_status_id  NUMBER(12) NOT NULL
);


CREATE TABLE REF_ORDR_STATUS (
       ordr_status_id       NUMBER(12) NOT NULL
);


CREATE TABLE REF_PROJ_STATUS (
       proj_status_nm       VARCHAR2(30) NOT NULL,
       proj_status_descr    VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);



CREATE TABLE REF_REPROV_STATUS (
       reprov_status        VARCHAR2(30) NOT NULL,
       reprov_descr         VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_RESOURCE (
       resource_id          NUMBER(12) NOT NULL,
       resource_typ         VARCHAR2(256) NOT NULL,
       resource_tag         VARCHAR2(256) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SCREEN (
       screen_nm            VARCHAR2(100) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SSN_GROUP (
       ssn_group_id         NUMBER(2) NOT NULL,
       ssn_group_nm         VARCHAR2(100) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
);


CREATE TABLE REF_STATUS (
       status_id            NUMBER(12) NOT NULL,
       class_id             NUMBER(12) NULL,
       status               VARCHAR2(100) NULL,
       descr                VARCHAR2(500) NULL,
       status_typ           VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_STATUS_CHG_REASON (
       reason_id            NUMBER(12) NOT NULL,
       reason_nm            VARCHAR2(30) NOT NULL,
       descr                VARCHAR2(500) NULL,
       is_dflt              CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       class_id             NUMBER(12) NULL
);


CREATE TABLE REF_STATUS_TYP (
       status_typ           VARCHAR2(30) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SUB_STATUS (
       sub_status_id        NUMBER(12) NOT NULL
);


CREATE TABLE REF_SUB_SVC_STATUS (
       sub_svc_status_id    NUMBER(12) NOT NULL
);


CREATE TABLE REF_SUB_TYP (
       sub_typ_id           NUMBER(12) NOT NULL,
       sub_typ_nm           VARCHAR2(255) NOT NULL,
       descr                VARCHAR2(500) NULL,
       is_active            CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SUBNTWK_MDL (
       subntwk_typ_id       NUMBER(12) NOT NULL,
       subntwk_mdl_id       NUMBER(12) NOT NULL,
       subntwk_mdl_nm       VARCHAR2(20) NULL,
       vendor_nm            VARCHAR2(30) NULL,
       technology_nm        VARCHAR2(30) NULL,
       technology_ver       VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SUBNTWK_TYP (
       subntwk_typ_id       NUMBER(12) NOT NULL,
       subntwk_typ_nm       VARCHAR2(100) NULL,
       ntwk_typ_id          NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SVC_ACTION_TYP (
       svc_action_typ       CHAR(1) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SVC_ADVISORY_STATUS (
       svc_advisory_status_nm VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_SVC_DELIVERY_PLAT (
       svc_delivery_plat_id NUMBER(12) NOT NULL,
       svc_delivery_plat_nm VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);



CREATE TABLE REF_TASK_STATUS (
       task_status_nm       VARCHAR2(30) NOT NULL,
       task_status_descr    VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_TICKET_STATUS (
       ticket_status_nm     VARCHAR2(20) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_TICKET_TYP (
       ticket_nm            VARCHAR2(30) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_TOPOLOGY_ACTION_TYP (
       topology_action_typ  VARCHAR2(30) NOT NULL,
       topology_action_descr VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_TOPOLOGY_STATUS (
       topology_status_id   NUMBER(12) NOT NULL,
       topology_status_nm   VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE RESOURCE_ASSIGN (
       resource_id          NUMBER(12) NOT NULL,
       subntwk_id           NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR(30) NULL
);


CREATE TABLE SBNT_TECH_PLATFORM (
       subntwk_id           NUMBER(12) NOT NULL,
       mgmt_mode_id         NUMBER(12) NOT NULL,
       tech_platform_id     NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SERVER_NODE (
       node_id              NUMBER(12) NOT NULL,
       node_nm              VARCHAR2(100) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL
);


CREATE TABLE SRC_ACTION_PARM (
       action_tmplt_parm_id NUMBER(12) NOT NULL,
       scope_proj_tmplt_nm  VARCHAR2(50) NOT NULL,
       scope_proj_tmplt_ver NUMBER(12) NOT NULL,
       scope_task_tmplt_nm  VARCHAR2(30) NOT NULL,
       scope_task_tmplt_ver NUMBER(12) NOT NULL,
       scope_action_execution_seq NUMBER(3) NOT NULL,
       src_task_tmplt_nm    VARCHAR2(30) NULL,
       src_task_tmplt_ver   NUMBER(12) NULL,
       src_action_execution_seq NUMBER(3) NULL,
       src_proj_tmplt_parm_id NUMBER(12) NULL,
       src_task_tmplt_parm_id NUMBER(12) NULL,
       src_action_tmplt_parm_id NUMBER(12) NULL,
       dflt_val             VARCHAR2(2000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE STATE_ACTION (
       state_action_id      NUMBER(12) NOT NULL,
       class_id             NUMBER(12) NULL,
       action               VARCHAR2(50) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       svc_id               NUMBER(12) NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE STATE_TRANSITION (
       state_transition_id  NUMBER(12) NOT NULL,
       from_status_id       NUMBER(12) NOT NULL,
       to_status_id         NUMBER(12) NOT NULL,
       cond                 VARCHAR2(2000) NULL,
       state_action_id      NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       scope_svc_id         NUMBER(12) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE STATE_TRANSITION_EVENT (
       state_transition_id  NUMBER(12) NOT NULL,
       target_class_id      NUMBER(12) NOT NULL,
       event_nm             VARCHAR2(50) NOT NULL,
       event_seq            NUMBER(6) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE STM_BATCH_REPLY_INFO (
       sub_ordr_id          NUMBER(12) NOT NULL,
       proj_id              NUMBER(12) NOT NULL,
       sub_svc_id           NUMBER(12) NOT NULL,
       svc_action_nm        VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE STRUC_HIERARCHY (
       struc_hierarchy_id   NUMBER(12) NOT NULL,
       parent_location_category_id NUMBER(12) NULL,
       child_location_category_id NUMBER(12) NOT NULL,
       location_struc_id    NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB (
       sub_id               NUMBER(12) NOT NULL,
       sub_status_id        NUMBER(12) NOT NULL,
       sub_typ_id           NUMBER(12) NOT NULL,
       svc_provider_id      NUMBER(12) NOT NULL,
       samp_ver             NUMBER(9) NOT NULL,
       external_key         VARCHAR2(100) NOT NULL,
       locale_cd            VARCHAR2(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       parent_sub_id        NUMBER(12) NULL,
       pre_status_id        NUMBER(12) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_ADDR (
       sub_addr_id          NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL,
       addr_typ_id          NUMBER(12) NOT NULL,
       location_id          NUMBER(12) NOT NULL,
       preferred_nm         VARCHAR2(64) NOT NULL,
       is_dflt              CHAR(1) NOT NULL,
       created_dtm          DATE NOT NULL,
       addr_status_id       NUMBER(12) NOT NULL,
       samp_ver             NUMBER(9) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       pre_status_id        NUMBER(12) NULL
);


CREATE TABLE SUB_CONTACT (
       sub_contact_id       NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL,
       contact_typ_id       NUMBER(12) NOT NULL,
       samp_ver             NUMBER(9) NOT NULL,
       contact_status_id    NUMBER(12) NOT NULL,
       location_id          NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       preferred_nm         VARCHAR2(255) NULL,
       pre_status_id        NUMBER(12) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_CONTACT_PARM (
       sub_contact_id       NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NOT NULL
);


CREATE TABLE SUB_OBJ_ENQ (
       lock_id              NUMBER(12) NOT NULL,
       sub_ordr_item_id     NUMBER(12) NULL,
       sub_ordr_id          NUMBER(12) NOT NULL,
       obj_id               NUMBER(12) NOT NULL,
       obj_class_id         NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL
);


CREATE TABLE SUB_ORDR (
       sub_ordr_id          NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NULL,
       ordr_status_id       NUMBER(12) NOT NULL,
       ordr_typ          VARCHAR2(50) NOT NULL,
       ordr_src_nm          VARCHAR2(50) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       rq_node_id           NUMBER(12) NULL,
       reply_node_id        NUMBER(12) NULL,
       external_key         VARCHAR2(128) NULL,
       has_impact           CHAR(1) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       ordr_req             CLOB NULL,
       process_def          CLOB NULL,
       samp_ver             NUMBER(9) NOT NULL ,
       batch_notify_delay   NUMBER(7) ,
       notify_handler_sched CHAR(1),
       ordr_previous_status_id NUMBER(12)
);


CREATE TABLE SUB_ORDR_ITEM_PARM (
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(2000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_ORDR_PARM (
       sub_ordr_id          NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(2000) NULL,
       longtext_val         CLOB NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_PARM (
       sub_id               NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(2000) NOT NULL
);


CREATE TABLE SUB_SVC (
       sub_svc_id           NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL,
       start_dt             DATE NOT NULL,
       external_key         VARCHAR2(100) NOT NULL,
       samp_ver             NUMBER(9) NOT NULL,
       sub_svc_status_id    NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       end_dt               DATE NULL,
       purchase_dt          DATE NULL,
       parent_sub_svc_id    NUMBER(12) NULL,
       pre_status_id        NUMBER(12) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       svc_id               NUMBER(12) NULL
);


CREATE TABLE SUB_SVC_ADDR (
       sub_svc_id           NUMBER(12) NOT NULL,
       sub_addr_id          NUMBER(12) NOT NULL
);


CREATE TABLE SUB_SVC_DELIVERY_PLAT (
       sub_svc_id           NUMBER(12) NOT NULL,
       svc_delivery_plat_id NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       status               VARCHAR2(40) NOT NULL
                                   CONSTRAINT db_action
                                          CHECK (status IN ('add_in_progress', 'update_in_progress', 'delete_in_progress', 'active', 'invalid')),
       ver                  NUMBER(12) NOT NULL
);


CREATE TABLE SUB_SVC_DEPY (
       prerequisite_sub_svc_id NUMBER(12) NOT NULL,
       dependent_sub_svc_id NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_SVC_NTWK (
       sub_svc_id           NUMBER(12) NOT NULL,
       subntwk_id           NUMBER(12) NOT NULL,
       ntwk_role_id         NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_SVC_NTWK_STAGE (
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       sub_svc_id           NUMBER(12) NOT NULL,
       subntwk_id           NUMBER(12) NOT NULL,
       ntwk_role_id         NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL
);


CREATE TABLE SUB_SVC_PARM (
       sub_svc_id           NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(2000) NOT NULL
);


CREATE TABLE SUB_SVC_PARM_DEPY (
       depy_sub_svc_id      NUMBER(12) NOT NULL,
       depy_parm_id         NUMBER(12) NOT NULL,
       prereq_sub_svc_id    NUMBER(12) NOT NULL,
       prereq_parm_id       NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL
);


CREATE TABLE SUB_SVC_PLAT_STAGE (
       sub_svc_id           NUMBER(12) NOT NULL,
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       svc_delivery_plat_id NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL
);


CREATE TABLE SUBNTWK (
       subntwk_id           NUMBER(12) NOT NULL,
       subntwk_nm           VARCHAR2(255) NOT NULL,
       topology_status_id   NUMBER(12) NOT NULL,
       upper_subntwk_nm     VARCHAR2(255) NULL,
       subntwk_typ_id       NUMBER(12) NOT NULL,
       parent_subntwk_id    NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_CARD_LMT (
       card_typ_id          NUMBER(12) NOT NULL,
       card_mdl_id          NUMBER(12) NOT NULL,
       subntwk_typ_id       NUMBER(12) NOT NULL,
       subntwk_mdl_id       NUMBER(12) NOT NULL,
       max_num              NUMBER NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_COVERAGE (
       subntwk_coverage_id  NUMBER(12) NOT NULL,
       subntwk_id           NUMBER(12) NOT NULL,
       ntwk_location_id     NUMBER(12) NOT NULL,
       location_struc_id    NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_MDL_LMT (
       subntwk_typ_id       NUMBER(12) NOT NULL,
       subntwk_mdl_id       NUMBER(12) NOT NULL,
       parent_subntwk_typ_id NUMBER(12) NOT NULL,
       parent_subntwk_mdl_id NUMBER(12) NOT NULL,
       max_num              NUMBER NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_MDL_PARM (
       subntwk_typ_id       NUMBER(12) NOT NULL,
       subntwk_mdl_id       NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_PARM (
       subntwk_id           NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(100) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_PARM_STAGE (
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       sub_svc_id           NUMBER(12) NOT NULL,
       subntwk_id           NUMBER(12) NOT NULL,
       val                  VARCHAR2(100) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL
);


CREATE TABLE SUBNTWK_TYP_FREQ (
       freq_id              NUMBER(12) NOT NULL,
       subntwk_typ_id       NUMBER(12) NOT NULL,
       jmf_cond             VARCHAR2(2000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUBNTWK_TYP_FREQ_GRP (
       freq_grp_id          NUMBER(12) NOT NULL,
       subntwk_typ_id       NUMBER(12) NOT NULL,
       jmf_cond             VARCHAR2(2000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SVC (
       svc_id               NUMBER(12) NOT NULL,
       svc_nm               VARCHAR2(255) NOT NULL,
       is_orderable         CHAR(1) NOT NULL,
       loading_policy       VARCHAR2(30) NULL,
       exp_dtm              DATE NULL,
       eff_dtm              DATE NOT NULL,
       svc_state            CHAR(1) NOT NULL,
       base_svc_id          NUMBER(12) NULL
);


CREATE TABLE SVC_ACTION (
       svc_action_id        NUMBER(12) NOT NULL,
       svc_action_nm        VARCHAR2(255) NOT NULL,
       svc_action_typ       CHAR(1) NOT NULL,
       base_svc_action_id   NUMBER(12) NULL,
       svc_id               NUMBER(12) NOT NULL
);


CREATE TABLE SVC_ADVISORY (
       svc_advisory_id      NUMBER(12) NOT NULL,
       subntwk_id           NUMBER(12) NULL,
       subntwk_typ_id       NUMBER(12) NOT NULL,
       link_id              NUMBER(12) NULL,
       svc_advisory_status_nm VARCHAR2(30) NOT NULL,
       title                VARCHAR2(255) NOT NULL,
       start_dtm            DATE NOT NULL,
       exp_closure_dtm      DATE NULL,
       closure_dtm          DATE NULL,
       closure_reason       VARCHAR2(500) NULL,
       closed_by            VARCHAR2(100) NULL,
       closure_type         VARCHAR2(255) NULL,
       ext_ticket_id        VARCHAR2(255) NOT NULL,
       ext_ticket_type      VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SVC_ADVISORY_PARM (
       svc_advisory_id      NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NULL,
       extra_val            BLOB NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);



CREATE TABLE SVC_PROVIDER (
       svc_provider_id      NUMBER(12) NOT NULL,
       svc_provider_nm      VARCHAR2(255) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SVC_SUPPORTED_PLATFORM (
       svc_supported_platform_id NUMBER(12) NOT NULL,
       svc_id               NUMBER(12) NOT NULL,
       svc_delivery_plat_id NUMBER(12) NOT NULL,
       ssn_group_id         NUMBER(2) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       priority             NUMBER(9) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       jmf_expr             VARCHAR2(4000) NULL
);


CREATE TABLE SYNC_ACTION_PARMS_TMP (
       sync_action_parm_id  VARCHAR2(30) NOT NULL,
       parm_nm              VARCHAR2(255) NOT NULL,
       parm_val             VARCHAR2(1000) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_DEBUG (
       sync_debug_seq       NUMBER NOT NULL,
       proc_call            VARCHAR2(30) NULL,
       message              VARCHAR2(4000) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_EXT_PROC_PARMS (
       ext_proc_nm          VARCHAR2(50) NOT NULL,
       ext_proc_parm_nm     VARCHAR2(30) NOT NULL,
       ext_parm_seq         NUMBER(4) NOT NULL,
       direction            VARCHAR2(6) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_EXT_PROCS (
       ext_proc_nm          VARCHAR2(50) NOT NULL,
       descr                VARCHAR2(500) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_GRP_ACTION_PARMS (
       grp_nm               VARCHAR2(50) NOT NULL,
       action_seq_within_grp NUMBER(4) NOT NULL,
       action               VARCHAR2(100) NOT NULL,
       parm_nm              VARCHAR2(255) NOT NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_GRP_ACTIONS (
       grp_nm               VARCHAR2(50) NOT NULL,
       action_seq_within_grp NUMBER(4) NOT NULL,
       action               VARCHAR2(100) NOT NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_GRP_EXT_PROC_COND (
       ext_proc_nm          VARCHAR2(50) NOT NULL,
       check_parm_nm        VARCHAR2(255) NOT NULL,
       grp_nm               VARCHAR2(50) NOT NULL,
       action_seq_within_grp NUMBER(4) NOT NULL,
       action               VARCHAR2(100) NOT NULL,
       fixed_val            VARCHAR2(1000) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_GRP_EXT_PROCS (
       grp_nm               VARCHAR2(50) NOT NULL,
       ext_proc_nm          VARCHAR2(50) NOT NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_GRP_QUALIFIERS (
       proj_tmplt_impl_ver  NUMBER(12) NOT NULL,
       proj_tmplt_impl_nm   VARCHAR2(50) NOT NULL,
       grp_nm               VARCHAR2(50) NOT NULL,
       is_excluded          CHAR(1) NOT NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_GRPS (
       grp_nm               VARCHAR2(50) NOT NULL,
       descr                VARCHAR2(500) NULL,
       grp_action_list      VARCHAR2(2000) NULL,
       num_actions_in_grp   NUMBER(4) NULL,
       parent_sync_action_grp_nm VARCHAR2(50) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SYNC_SRC_EXT_PARMS (
       ext_proc_nm          VARCHAR2(50) NOT NULL,
       ext_proc_parm_nm     VARCHAR2(30) NOT NULL,
       scope_grp            VARCHAR2(50) NOT NULL,
       src_func_nm          VARCHAR2(30) NULL,
       src_grp_nm           VARCHAR2(50) NULL,
       src_parm_nm          VARCHAR2(255) NULL,
       src_fixed_val        VARCHAR2(100) NULL,
       src_action_seq_within_grp NUMBER(4) NULL,
       src_action           VARCHAR2(100) NULL,
       add_ext_key_proc_nm  VARCHAR2(30) NULL,
       ext_key_action_identifier VARCHAR2(100) NULL,
       ext_key_parm_nm_identifier VARCHAR2(255) NULL,
       created_dtm          DATE NULL,
       created_by           VARCHAR2(30) NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK (
       task_id              NUMBER(12) NOT NULL,
       proj_id              NUMBER(12) NOT NULL,
       task_nm              VARCHAR2(30) NOT NULL,
       task_status_nm       VARCHAR2(30) NOT NULL,
       task_descr           VARCHAR2(500) NULL,
       task_seq             NUMBER(3) NOT NULL,
       task_tmplt_nm        VARCHAR2(30) NOT NULL,
       task_tmplt_ver       NUMBER(12) NOT NULL,
       start_dt             DATE NULL,
       completion_dt        DATE NULL,
       assigned_to          VARCHAR2(30) NULL,
       locked_by            VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_ACTION (
       task_action_id       NUMBER(12) NOT NULL,
       task_id              NUMBER(12) NOT NULL,
       execution_seq        NUMBER(3) NOT NULL,
       task_tmplt_nm        VARCHAR2(30) NULL,
       task_tmplt_ver       NUMBER(12) NULL,
       action_tmplt_execution_seq NUMBER(3) NOT NULL,
       parent_task_action_id NUMBER(12) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_ACTION_PARM (
       task_action_parm_id  NUMBER(12) NOT NULL,
       task_action_id       NUMBER(12) NOT NULL,
       ntwk_cmpnt_typ_nm    VARCHAR2(30) NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(4000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_PARM (
       task_id              NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(1000) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_TMPLT (
       task_tmplt_nm        VARCHAR2(30) NOT NULL,
       task_tmplt_ver       NUMBER(12) NOT NULL,
       task_tmplt_descr     VARCHAR2(500) NULL,
       duration             NUMBER(9) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_TMPLT_DEPY (
       proj_tmplt_nm        VARCHAR2(50) NOT NULL,
       proj_tmplt_ver       NUMBER(12) NOT NULL,
       task_tmplt_nm        VARCHAR2(30) NOT NULL,
       task_tmplt_ver       NUMBER(12) NOT NULL,
       dependent_task_tmplt_nm VARCHAR2(30) NOT NULL,
       dependent_task_tmplt_ver NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_TMPLT_PARM (
       task_tmplt_parm_id   NUMBER(12) NOT NULL,
       task_tmplt_nm        VARCHAR2(30) NULL,
       task_tmplt_ver       NUMBER(12) NULL,
       parm_id              NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASK_TRANSITIONS (
       from_task_status_nm  VARCHAR2(30) NOT NULL,
       to_task_status_nm    VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TASKS_IN_PROJ_TMPLT (
       proj_tmplt_nm        VARCHAR2(50) NOT NULL,
       proj_tmplt_ver       NUMBER(12) NOT NULL,
       task_tmplt_nm        VARCHAR2(30) NOT NULL,
       task_tmplt_ver       NUMBER(12) NOT NULL,
       execution_seq        NUMBER(3) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TECH_PLATFORM (
       tech_platform_id     NUMBER(12) NOT NULL,
       tech_platform_typ_id NUMBER(12) NOT NULL,
       techn_platform_nm    VARCHAR2(100) NOT NULL,
       topology_status_id   NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TECH_PLATFORM_PARM (
       tech_platform_id     NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(200) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TECH_PLATFORM_TYP (
       tech_platform_typ_id NUMBER(12) NOT NULL,
       tech_platform_typ_nm VARCHAR2(100) NOT NULL,
       software_version     VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TROUBLE_TICKET (
       ticket_id            NUMBER(12) NOT NULL,
       ticket_nm            VARCHAR2(30) NULL,
       ticket_status_nm     VARCHAR2(20) NOT NULL,
       title                VARCHAR2(100) NOT NULL,
       descr                VARCHAR2(500) NOT NULL,
       closed_dtm           DATE NULL,
       closed_by            VARCHAR2(30) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE TROUBLE_TICKET_PARM (
       ticket_id            NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NULL,
       extra_val            BLOB NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE VERSION (
       module_name          VARCHAR2(100) NOT NULL,
       comments             VARCHAR2(255) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL,
       spec_title           VARCHAR2(80) NULL,
       spec_vendor          VARCHAR2(80) NULL,
       spec_version         VARCHAR2(80) NULL,
       impl_title           VARCHAR2(80) NULL,
       impl_vendor          VARCHAR2(80) NULL,
       impl_version         VARCHAR2(80) NULL,
       built_by             VARCHAR2(80) NULL,
       installed_by         VARCHAR2(30) NULL,
       built_dtm            DATE NOT NULL
);


CREATE TABLE VLD_RULE (
       vld_rule_id          NUMBER(12) NOT NULL,
       vld_rule_nm          VARCHAR2(255) NULL,
       test_result_inversion_ind CHAR(1) NOT NULL,
       min_inclusive        NUMBER NULL,
       min_exclusive        NUMBER NULL,
       max_inclusive        NUMBER NULL,
       max_exclusive        NUMBER NULL,
       min_char_num         NUMBER(9) NULL,
       max_char_num         NUMBER(9) NULL,
       err_code             NUMBER(9) NOT NULL,
       err_dflt_msg         VARCHAR2(255) NOT NULL,
       jmf_expr             VARCHAR2(2000) NULL,
       format               VARCHAR2(100) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE WRK_ARCH_REC (
       tbl_nm               VARCHAR2(30) NOT NULL,
       prio                 NUMBER NOT NULL,
       action               VARCHAR2(5) NULL,
       rid                  ROWID NOT NULL,
       seq                  NUMBER NULL
);


CREATE TABLE WRK_ARCH_SESSION (
       tbl_nm               VARCHAR2(30) NULL,
       rid                  ROWID NOT NULL
);


CREATE TABLE WRK_LOCK (
       lock_nm              VARCHAR2(30) NULL,
       val                  VARCHAR2(255) NULL
);


CREATE TABLE WRK_UNIQUE_PARM (
       grp_nm_or_parm_id    VARCHAR2(30) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(255) NOT NULL,
       sub_ordr_id          NUMBER(12) NOT NULL,
       sub_entity_id        NUMBER(12) NOT NULL
);


CREATE TABLE XML_REPOSITORY (
       doc_nm               VARCHAR2(256) NOT NULL,
       data                 LONG NOT NULL,
       samp_ver             NUMBER(9) NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);



CREATE TABLE TECH_PLATFORM_MGMT_MODE (
       tech_platform_id     NUMBER(12) NOT NULL,
       mgmt_mode_id         NUMBER(12) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);

CREATE TABLE REF_ORDR_TYP (
       ordr_typ             varchar2(60) NOT NULL,
       created_by           varchar2(30) NOT NULL,
       created_dtm          date DEFAULT sysdate NOT NULL,
       modified_dtm         date NULL,
       modified_by          varchar2(30) NULL
);

 

CREATE TABLE REF_ASSOC_TYP (
       assoc_typ_id         number(3) NOT NULL,   
       assoc_typ_nm         varchar2(50) NOT NULL  
);

CREATE TABLE REF_OBJECT_TYP (
       object_typ_id        number(12) NOT NULL,  
       class_id             NUMBER(12) NOT NULL,
       object_nm            varchar2(50) NULL,
       object_id            number(12) NULL
);

CREATE TABLE ORDR_ITEM_ASSOC_CHG (
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       assoc_typ_id         number(12) NOT NULL,
       action_nm             varchar2(20) NOT NULL,
       assoc_object_typ_id        number(12) NOT NULL,
       assoc_sub_entity_id  number(12) NOT NULL,
       assoc_parm_id        NUMBER(12) NULL,
       parm_id              NUMBER(12) NULL,
       need_upd		     char(1) not null
);


CREATE TABLE SUB_ORDR_ITEM (
       sub_ordr_item_id     NUMBER(12) NOT NULL,
       sub_ordr_id          NUMBER(12) NOT NULL,
       ordr_item_status_id  NUMBER(12) NOT NULL,
       object_typ_id        number(12) NOT NULL,
       sub_entity_id        NUMBER(12) NULL,
       ordr_action_id       NUMBER(12) NOT NULL,
       notify_seq_num       NUMBER(9) NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       created_dtm          DATE NOT NULL,
       display_info         VARCHAR2(500) NULL,
       has_impact           CHAR(1) NOT NULL,
       is_original          CHAR(1) NULL,
       reason_id            NUMBER(12) NULL,
       reason_notes         VARCHAR2(1000) NULL,
       modified_by          VARCHAR2(30) NULL,
       modified_dtm         DATE NULL
);




CREATE TABLE SUB_USER (
       sub_user_id          NUMBER(12) NOT NULL,
       sub_id               NUMBER(12) NOT NULL,
       external_key         VARCHAR2(100) NOT NULL,
       samp_ver             NUMBER(9) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       user_status_id       NUMBER(3) NULL,
       pre_status_id        NUMBER(12) NULL,
       modified_dtm         DATE NULL,
       object_typ_id        number(12) NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE SUB_USER_PARM (
       sub_user_id          NUMBER(12) NOT NULL,
       parm_id              NUMBER(12) NOT NULL,
       val                  VARCHAR2(2000) NOT NULL
);


CREATE TABLE SUB_USER_SVC (
       sub_user_id          NUMBER(12) NOT NULL,
       sub_svc_id           NUMBER(12) NOT NULL,
       assoc_rule_id         NUMBER(3) NOT NULL,
       created_dtm          DATE NOT NULL,
       created_by           VARCHAR2(30) NOT NULL,
       modified_dtm         DATE NULL,
       modified_by          VARCHAR2(30) NULL
);


CREATE TABLE REF_ASSOC_RULE (
    assoc_rule_id number(12) not null,
    assoc_rule_name varchar2(248) not null,
    assoc_typ_id number(3) not null,
    aend_object_typ_id number(12) not null,
    zend_object_typ_id number(12) not null,
    aend_loading_policy varchar(48) not null,
    zend_loading_policy varchar(48) not null
);

CREATE TABLE REF_USER_STATUS (
       user_status_id       NUMBER(3) NOT NULL
);

CREATE TABLE SUB_SVC_ASSOC (
  SUB_SVC_ID            NUMBER(12)              NOT NULL,
  ASSOC_SUB_SVC_ID     NUMBER(12)               NOT NULL,
  ASSOC_RULE_ID  NUMBER(3)                       NOT NULL,
  CREATED_DTM              DATE                 NOT NULL,
  CREATED_BY               VARCHAR2(30)    NOT NULL
);

CREATE TABLE ORDR_NOTIFY_ITEM (
  sub_ordr_id               NUMBER(12)   NOT NULL,
  sub_ordr_item_id          NUMBER(12)   NOT NULL,
  result                    VARCHAR2(24) NOT NULL,
  error_notes               VARCHAR2(2000),
  error_code                VARCHAR2(24),
  created_ts                TIMESTAMP(6) WITH TIME ZONE    NOT NULL
);

CREATE TABLE WSNOTIFY_REG
(
  ID               VARCHAR2(48)            NOT NULL,
  MGR_NAME         VARCHAR2(48)            NOT NULL,
  TOPIC            VARCHAR2(512)           NOT NULL,
  BY_AGENT         CHAR(1)                 NOT NULL,
  IGNORE_ERROR     CHAR(1)                 NOT NULL,
  TERMINATION_DTM  DATE,
  SUB_REF          VARCHAR2(2048)          NOT NULL,
  CREATED_DTM      DATE                    NOT NULL,
  MODIFIED_DTM     DATE,
  STATE            CHAR(1)                 NOT NULL,
  AUTH_CODE	   VARCHAR2(2048)          NULL
);

