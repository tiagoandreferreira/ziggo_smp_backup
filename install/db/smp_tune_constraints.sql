--============================================================================
--    $Id: smp_tune_constraints.sql,v 1.6 2005/08/16 15:44:33 dant Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

-- drop constrains for order archive


ALTER TABLE LINE_ITEM_DEPY drop CONSTRAINT LINE_ITEM_DEPY_FK2;

ALTER TABLE LINE_ITEM_DEPY drop CONSTRAINT LINE_ITEM_DEPY_FK3;



-- Defer  constraint for batch inserts

ALTER TABLE SUB_SVC DROP CONSTRAINT SUB_SVC_FK5;

ALTER TABLE SUB_SVC ADD CONSTRAINT SUB_SVC_FK5 FOREIGN KEY(PARENT_SUB_SVC_ID) 
    REFERENCES SUB_SVC(SUB_SVC_ID) INITIALLY DEFERRED DEFERRABLE;

--
ALTER TABLE PROJ DROP CONSTRAINT proj_fk3;

ALTER TABLE PROJ
       ADD  ( CONSTRAINT proj_fk3
              FOREIGN KEY (hint_subntwk_id)
                             REFERENCES SUBNTWK ON DELETE SET NULL) ;



