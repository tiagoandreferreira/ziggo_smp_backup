/*================================================================================================
#
#  FILE INFO
#
#    $Id: smp_tune_dual.sql,v 1.1 2003/04/04 15:30:27 vadimg Exp $
#
#  DESCRIPTION
#
#    Point SMP to available dual table
#
#  PARAMETERS
#    Must be executed as SMP schema owner
#
#  RELATED SCRIPTS
#    Oracle version: 8i and higher
#    dual_system.sql - must be executed as SYS before this script to take advantage of x$dual
#
#  REVISION HISTORY
#  * Based on CVS log
#================================================================================================*/



declare
  n number;
begin
  begin
    execute immediate 'select count(*) from sys.x_$dual' into n;
  exception
   when others then 
     n:=0;
  end;
  if n = 0 then 
     execute immediate 'create or replace synonym v_dual for sys.dual';
  else
     execute immediate 'create or replace synonym v_dual for sys.x_$dual';
  end if;
end;
/
