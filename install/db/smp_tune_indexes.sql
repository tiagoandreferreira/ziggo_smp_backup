--============================================================================
--    $Id: smp_tune_indexes.sql,v 1.11.2.1.4.1 2011/04/25 19:55:16 davidx Exp $
--
--  PURPOSE of this script to adjust index parameters for the sake of
--  performance
--    Use reverse indexes to spread head block contention
--    Use compression for better performance (less IO)
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

alter index SUB_PK                      rebuild reverse;
alter index SUB_ADDR_PK                 rebuild reverse;
alter index SUB_CONTACT_PK              rebuild reverse;


alter index sub_svc_parm_ix2            rebuild compress 2 ;
alter index SUB_ORDR_ITEM_IX1           rebuild compress 1 ;

alter index LINE_ITEM_DEPY_PK initrans 6;
alter index LINK_IX2 initrans 6;
alter index LINK_IX3 initrans 6;
alter index LINK_IX4 initrans 6;
alter index LOCATION_DTL_IX1 initrans 6;
alter index LOCATION_DTL_PK initrans 6;
alter index ORDR_ITEM_ENTITY_CHG_IX1 initrans 6;
alter index SUBNTWK_IX1 initrans 6;
alter index SUBNTWK_IX3 initrans 6;
alter index SUBNTWK_PARM_IX1 initrans 6;
alter index SUBNTWK_PARM_PK initrans 6;
alter index SUB_ADDR_IX1 initrans 6;
alter index SUB_ADDR_IX2 initrans 6;
alter index SUB_CONTACT_IX1 initrans 6;
alter index SUB_CONTACT_IX3 initrans 6;
alter index SUB_IX2 initrans 6;
alter index SUB_ORDR_ITEM_IX1 initrans 6;
alter index SUB_ORDR_ITEM_IX2 initrans 6;
alter index SUB_ORDR_ITEM_PARM_IX1 initrans 6;
alter index SUB_ORDR_ITEM_PARM_PK initrans 6;
alter index SUB_ORDR_ITEM_PK initrans 6;
alter index SUB_ORDR_PARM_PK initrans 6;
alter index SUB_SVC_ADDR_PK initrans 6;
alter index SUB_SVC_DELIVERY_PLAT_IX1 initrans 6;
alter index SUB_SVC_DELIVERY_PLAT_PK initrans 6;
alter index SUB_SVC_DEPY_IX1 initrans 6;
alter index SUB_SVC_DEPY_PK initrans 6;
alter index SUB_SVC_IX4 initrans 6;
alter index SUB_SVC_IX5 initrans 6;
alter index SUB_SVC_IX6 initrans 6;
alter index SUB_SVC_NTWK_IX1 initrans 6;
alter index SUB_SVC_NTWK_PK initrans 6;
alter index SUB_SVC_PARM_DEPY_IX2 initrans 6;
alter index SUB_SVC_PARM_DEPY_PK initrans 6;
alter index SUB_SVC_PARM_IX2 initrans 6;
alter index SUB_SVC_PARM_PK initrans 6;
alter index SUB_SVC_PK initrans 6;

ALTER TABLE SUB_ORDR INITRANS 20 ;
ALTER TABLE SUB_ORDR_ITEM INITRANS 20 ;
ALTER TABLE SUB_ORDR_ITEM_PARM INITRANS 20 ;
ALTER TABLE SUB_ORDR_PARM INITRANS 20 ;
ALTER TABLE WRK_LOCK INITRANS 20 ;

ALTER INDEX WRK_LOCK_UK1 INITRANS 20 ;
ALTER INDEX SUB_ORDR_PK INITRANS 40 ;
ALTER INDEX SUB_ORDR_IX1 INITRANS 40 ;
ALTER INDEX SUB_ORDR_IX2 INITRANS 40 ;
ALTER INDEX SUB_ORDR_IX3 INITRANS 40 ;
ALTER INDEX SUB_ORDR_IX4 INITRANS 40 ;
ALTER INDEX SUB_ORDR_ITEM_IX1 INITRANS 40 ;
ALTER INDEX SUB_ORDR_ITEM_IX2 INITRANS 40 ;
ALTER INDEX SUB_ORDR_ITEM_PARM_IX1 INITRANS 40 ;
ALTER INDEX SUB_ORDR_ITEM_PARM_PK INITRANS 40 ;
ALTER INDEX SUB_ORDR_PARM_PK INITRANS 40 ;
ALTER INDEX SUB_ORDR_PARM_IX2 INITRANS 40 ;
ALTER INDEX SUB_ORDR_ITEM_PK INITRANS 40 ;


