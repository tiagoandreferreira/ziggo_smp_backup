/*=========================================================================

  FILE INFO

    $Id: smp_tune_keep.sql,v 1.1 2003/03/25 16:17:50 vadimg Exp $

  DESCRIPTION

	Tune DB shared pool

  NOTES



  REVISION HISTORY
  * Based on CVS log
========================================================================*/

begin
  for cr in (select owner, NAME, TYPE from V$DB_OBJECT_CACHE where executions > 1000 and KEPT = 'NO') loop
     if cr.type in ('PROCEDURE', 'FUNCTION', 'PACKAGE BODY') then
        dbms_shared_pool.keep(cr.owner||'.'||cr.name, 'P');
     elsif cr.type in ('TRIGGER') then
        dbms_shared_pool.keep(cr.owner||'.'||cr.name, 'R');
     elsif cr.type in ('SEQUENCE') then
        dbms_shared_pool.keep(cr.owner||'.'||cr.name, 'Q');
     end if;
  end loop;

  for cr in (select address, HASH_VALUE , sql_text from v$sqlarea where executions > 1000) loop
        dbms_shared_pool.keep(cr.address||', '||cr.hash_value, 'C');

  end loop;
end;
/
