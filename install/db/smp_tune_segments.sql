--============================================================================
--    $Id: smp_tune_segments.sql,v 1.5.28.1 2010/11/05 19:51:19 davidx Exp $
--   Purpose: adjust segment space management params for high performance DML
--
--   Note: This script shall not be executed when DB uses ASSM
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
declare
  procedure tune_segment(p_seg_nm varchar2, p_initrans number, p_freelist number) is
    v_mode varchar2(30);
  begin
    begin
       select SEGMENT_SPACE_MANAGEMENT into v_mode from user_tablespaces t, user_tables s
          where table_name = upper(p_seg_nm) and t.tablespace_name = s.tablespace_name;
    exception
      when others then
          v_mode := 'MANUAL';
    end;
    if v_mode = 'MANUAL' then
       execute immediate 'alter table  '||p_seg_nm||' initrans '||p_initrans||' storage (freelists '||p_freelist||')';
    end if;
  end;
 begin

   tune_segment('sub_ordr_item'        ,  10,  10);

   tune_segment('sub_svc'              ,  10,  10);

   tune_segment('sub_ordr'             ,  10,  10);


end;
/
