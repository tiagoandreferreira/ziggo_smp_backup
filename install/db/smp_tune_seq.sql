--=============================================================================
--    $Id: smp_tune_seq.sql,v 1.13.16.1.8.2 2011/05/17 15:34:35 mattl Exp $
--
--  DESCRIPTION
--
--  Script to adjust sequence properties to the load
--
--  Usage: included in DB initialization scripts
--
--
--  RELATED SCRIPTS
--
--  REVISION HISTORY
--  * Based on CVS log
--=============================================================================


--alter sequence STATUS_HIST_SEQ increment by 1000    cache 10;

alter sequence SAMP_VER_SEQ     cache 5000;

-- App server responsibility is to fetch numbers from 100 batches
alter sequence SUB_ORDR_ITEM_SEQ increment by 100   cache 5;

alter sequence SUB_SVC_SEQ  increment by 50      cache 200;

alter sequence SUB_SEQ  increment by 50    cache 100;

alter sequence SUB_ORDR_SEQ     increment by 1     cache 5;

alter sequence LOCATION_SEQ           cache 50;

alter sequence SUB_ADDR_SEQ  increment by 50           cache 50;

alter sequence SUB_CONTACT_SEQ  increment by 50        cache 50;

-- disable cache to prevent seq loss

alter  SEQUENCE ARCH_GRP_PART_SEQ nocache;

alter  SEQUENCE ARCH_GRP_SEQ      nocache;

alter  SEQUENCE ARCH_RUN_SEQ      nocache;

alter sequence SUB_OBJ_ENQ_SEQ  increment by 1 cache 100;

alter sequence LINK_SEQ  cache 1000;

alter sequence SUBNTWK_SEQ  cache 1000;
