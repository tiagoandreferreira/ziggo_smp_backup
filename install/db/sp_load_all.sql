--============================================================================
--    $Id: sp_load_all.sql,v 1.1 2007/11/29 21:15:27 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on
------------------------------------------------------------------
---------   Data fill for table: Svc_Provider   --------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Svc_Provider   ---------')

Insert into Svc_Provider(svc_provider_id, svc_provider_nm, created_dtm, created_by, modified_dtm, modified_by)
Values (1,'Default', SYSDATE, 'INIT', NULL, NULL);
------------------------------------------------------------------
---------   Data fill for table: State_Action   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: State_Action   ---------')
------------------------------------------------------------------
---------   Data fill for table: State_Transition   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: State_Transition   ---------')

------------------------------------------------------------------
---------   Data fill for table: State_Transition_Event   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: State_Transition_Event   ---------')
------------------------------------------------------------------
