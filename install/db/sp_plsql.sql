--============================================================================
--    $Id: sp_plsql.sql,v 1.90 2005/03/10 19:58:40 davidx Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================


--set serveroutput on

@sp_plsql_objects;
@sp_plsql_functions;
@sp_plsql_views;
@sp_plsql_prcs;
@sp_plsql_pkgs;
-- no more view for testing
-- @sp_plsql_int_test;
/
--ECHO 'RUNNING RECOMPILE';

-- avoid error message due to dbms_output buffer overflow

exec dbms_output.enable(1000000); 


EXEC PKG_RECOMPILE_ALL.RECOMP;

SET LINE 100
SET FEEDBACK OFF
COLUMN TEXT FORMAT A40 WOR
SET RECSEP OFF
BREAK ON NAME SKIP 1 NODUP

SELECT NAME, LINE, POSITION, TEXT 
    FROM USER_ERRORS 
    ORDER BY NAME, LINE, POSITION;

SET FEEDBACK ON


