--============================================================================
--  $Id: sp_plsql_functions.sql,v 1.72.10.1.8.5 2011/09/12 18:38:34 davidx Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Start of DDL Script for Function FN_ADDR_TYP_ID
CREATE OR REPLACE
FUNCTION fn_addr_typ_id (
   p_addr_typ_nm   IN   ref_addr_typ.addr_typ_nm%TYPE
)
   RETURN ref_addr_typ.addr_typ_id%TYPE
IS
   v_addr_typ_id   ref_addr_typ.addr_typ_id%TYPE;
BEGIN
   SELECT addr_typ_id
     INTO v_addr_typ_id
     FROM ref_addr_typ
    WHERE addr_typ_nm = p_addr_typ_nm;

   RETURN v_addr_typ_id;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END;
/
-- End of DDL Script for Function FN_ADDR_TYP_ID

-- Start of DDL Script for Function FN_CHECK_PENDING_ORDR
CREATE OR REPLACE
FUNCTION FN_CHECK_PENDING_ORDR (p_sub_id sub_ordr.sub_id%TYPE)
   RETURN CHAR
IS
   result   CHAR;

   CURSOR c_chk_ordr
   IS
      SELECT sub_ordr.sub_ordr_id
        FROM sub_ordr, ref_status
       WHERE sub_ordr.sub_id = p_sub_id
         AND sub_ordr.ordr_status_id = ref_status.status_id
         AND substr(ref_status.status,1,5) = 'open.';

   r_ordr   c_chk_ordr%ROWTYPE;
BEGIN
   OPEN c_chk_ordr;
   FETCH c_chk_ordr INTO r_ordr;

   IF c_chk_ordr%FOUND
   THEN
      result := 'y';
   ELSE
      result := 'n';
   END IF;

   CLOSE c_chk_ordr;
   RETURN (result);
END fn_check_pending_ordr;
/
-- End of DDL Script for Function FN_CHECK_PENDING_ORDR

-- Start of DDL Script for Function FN_CM_GET_CLASS_ID
/* --- VadimG, Mar 14, 2003
CREATE OR REPLACE
function fn_cm_get_class_id(p_class_nm ref_class.class_nm%type)
return ref_class.class_id%type is
  cursor c_class is
  select class_id
  from   ref_class
  where  class_nm = p_class_nm;

  l_class_id  ref_class.class_id%type := 0;
begin
  for r_class in c_class loop
    l_class_id := r_class.class_id;
  end loop;
  return l_class_id;
end fn_cm_get_class_id;
*/
-- End of DDL Script for Function FN_CM_GET_CLASS_ID

-- Start of DDL Script for Function FN_CM_GET_PARM_ID
CREATE OR REPLACE
function fn_cm_get_parm_id(
  p_class_id  parm.class_id%type,
  p_parm_nm   parm.parm_nm%type)
return parm.parm_id%type is
  cursor c_parm is
  select parm_id
  from   parm
  where  class_id = p_class_id
  and    parm_nm = p_parm_nm;

  l_parm_id  parm.parm_id%type := 0;
begin
  for r_parm in c_parm loop
    l_parm_id := r_parm.parm_id;
  end loop;
  return l_parm_id;
end fn_cm_get_parm_id;
/
-- End of DDL Script for Function FN_CM_GET_PARM_ID

-- Start of DDL Script for Function FN_COUNT_TOKENS
CREATE OR REPLACE
FUNCTION fn_count_tokens (
   param     IN   VARCHAR2,
   delimit   IN   VARCHAR2
)
   RETURN INT
IS
   num      INT;
   pos      INT;
   newpos   INT;
BEGIN
   pos := INSTR (param, delimit, 1);
   num := 0;

   WHILE pos > 0
   LOOP
      num :=   num
             + 1;
      newpos :=   pos
                + 1;
      pos := INSTR (param, delimit, newpos);
   END LOOP;

   RETURN   num
          + 1;
END;
/
-- End of DDL Script for Function FN_COUNT_TOKENS

-- Start of DDL Script for Function FN_GET_ADDR_STATUS_ID
CREATE OR REPLACE FUNCTION fn_get_addr_status_id (
   p_status   ref_status.status%TYPE
)
   RETURN ref_status.status_id%TYPE
IS
   v_status_id   ref_status.status_id%TYPE;
BEGIN
   SELECT status_id
     INTO v_status_id
     FROM ref_status
    WHERE status = p_status
      AND class_id = get_class_id('SubAddressSpec');

   RETURN v_status_id;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END;
/
-- End of DDL Script for Function FN_GET_ADDR_STATUS_ID


-- Start of DDL Script for Function FN_GET_CONTACT_INFO
CREATE OR REPLACE
FUNCTION fn_get_contact_info (
   p_sub_contact_id        sub_contact_parm.sub_contact_id%TYPE,
   p_parm_nm          IN   parm.parm_nm%TYPE
)
   RETURN sub_contact_parm.val%TYPE
IS
   l_val sub_contact_parm.val%TYPE;
BEGIN
   SELECT val
     INTO l_val
     FROM sub_contact_parm, parm
    WHERE sub_contact_parm.sub_contact_id = p_sub_contact_id
      AND sub_contact_parm.parm_id = parm.parm_id
      AND parm.parm_nm = p_parm_nm;
   RETURN l_val;
END fn_get_contact_info;
/
-- End of DDL Script for Function FN_GET_CONTACT_INFO


-- Start of DDL Script for Function FN_GET_LOCATION_ID
CREATE OR REPLACE
FUNCTION fn_get_location_id (p_addr_id NUMBER)
   RETURN location.location_id%TYPE
IS
   v_location_id   location.location_id%TYPE;
BEGIN
   SELECT sub_addr.location_id
     INTO v_location_id
     FROM sub_addr
    WHERE sub_addr.sub_addr_id = p_addr_id;

   RETURN (v_location_id);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END fn_get_location_id;
/
-- End of DDL Script for Function FN_GET_LOCATION_ID



-- Start of DDL Script for Function FN_GET_REASON_CODE
CREATE OR REPLACE
FUNCTION fn_get_reason_code (
   p_class_nm    IN   ref_class.class_nm%TYPE,
   p_reason_nm   IN   ref_status_chg_reason.reason_nm%TYPE
)
   RETURN ref_status_chg_reason.reason_id%TYPE
IS
   result   ref_status_chg_reason.reason_id%TYPE;
BEGIN
   SELECT reason_id
     INTO result
     FROM ref_status_chg_reason
    WHERE ref_status_chg_reason.class_id = get_class_id (p_class_nm)
      AND ref_status_chg_reason.reason_nm = p_reason_nm;

   RETURN result;
END;
/
-- End of DDL Script for Function FN_GET_REASON_CODE

-- Start of DDL Script for Function FN_GET_REASON_NM
CREATE OR REPLACE
FUNCTION fn_get_reason_nm (
   p_class_nm    IN   ref_class.class_nm%TYPE,
   p_reason_id   IN   ref_status_chg_reason.reason_id%TYPE
)
   RETURN ref_status_chg_reason.reason_nm%TYPE
IS
   result   ref_status_chg_reason.reason_nm%TYPE;
BEGIN
   SELECT reason_nm
     INTO result
     FROM ref_status_chg_reason
    WHERE class_id = get_class_id (p_class_nm) AND reason_id = p_reason_id;

   RETURN result;
END fn_get_reason_nm;
/
-- End of DDL Script for Function FN_GET_REASON_NM

-- Start of DDL Script for Function FN_GET_SAMP_ACCESS_PARM
CREATE OR REPLACE
FUNCTION fn_get_samp_access_parm (
   p_userid   IN       sub_svc_parm.val%TYPE,
   p_parm     IN       parm.parm_nm%TYPE,
   p_status   OUT      ref_status.status%TYPE
)
   RETURN sub_svc_parm.val%TYPE
IS

-- Purpose: To return the value of the parameter requested as well as the
-- subscriber service status
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
   TYPE subsvccurtype IS REF CURSOR;

   c_db_col       subsvccurtype;
   v_unique_id    sub_svc_parm.val%TYPE     := 'email_login';
   v_svc_nm       svc.svc_nm%TYPE           := 'samp_access';
   v_parm_id      parm.parm_id%TYPE;
   v_db_col_nm    parm.db_col_nm%TYPE;
   v_sub_svc_id   sub_svc.sub_svc_id%TYPE;
   v_sql          VARCHAR2 (999);
   return_value   sub_svc_parm.val%TYPE;
BEGIN
   --- Based on the user id get the sub_svc_id.
   v_parm_id :=
        get_parm_id (v_unique_id, get_class_id ('SubSvcSpec'), get_svcid (v_svc_nm));

   ---SELECT sub_svc_id
   ---  INTO v_sub_svc_id
   ---  FROM SUB_SVC_PARM
   --- WHERE parm_id = v_parm_id AND val = p_userid;

   SELECT sub_svc_parm.sub_svc_id
     INTO v_sub_svc_id
     FROM sub_svc_parm, sub_svc, ref_status
    WHERE parm_id = v_parm_id
      AND val = p_userid
      AND sub_svc_parm.sub_svc_id = sub_svc.sub_svc_id
      AND sub_svc.sub_svc_status_id = ref_status.status_id
      AND ref_status.status NOT IN ('deleted', 'delete_in_progress');

   --- Get the parameter id for the service as well as reserved flag and db-col
   --- name
   v_parm_id :=
             get_parm_id (p_parm, get_class_id ('SubSvcSpec'), get_svcid (v_svc_nm));

   SELECT t.parm_id, t.db_col_nm
     INTO v_parm_id, v_db_col_nm
     FROM parm t
    WHERE t.parm_id = v_parm_id;

   IF v_db_col_nm IS NOT NULL
   THEN --- this is a query for a reserved agttribute
      v_sql :=    'SELECT a.'
               || v_db_col_nm
               || ', b.status FROM sub_svc a, REF_STATUS b '
               || 'WHERE a.sub_svc_status_id= b.status_id '
               || 'AND b.status NOT IN (''deleted'', ''delete_in_progress'') '
               || 'AND a.sub_svc_id =:s';
      OPEN c_db_col FOR v_sql USING v_sub_svc_id;

      LOOP
         FETCH c_db_col INTO return_value, p_status;
         EXIT WHEN c_db_col%NOTFOUND;
      END LOOP;

      CLOSE c_db_col; -- close cursor variable
   ELSE --- this is a query for parameters from sub_svc_parm
      SELECT b.val, c.status
        INTO return_value, p_status
        FROM sub_svc a, sub_svc_parm b, ref_status c
       WHERE a.sub_svc_id = b.sub_svc_id
         AND a.sub_svc_status_id = c.status_id
         AND a.sub_svc_id = v_sub_svc_id
         AND b.parm_id = v_parm_id
         AND c.status NOT IN ('deleted', 'delete_in_progress');
   END IF;

   RETURN return_value;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_samp_access_parm;
/
-- End of DDL Script for Function FN_GET_SAMP_ACCESS_PARM

-- Start of DDL Script for Function FN_GET_STATUS_CHG_REASON_ID
CREATE OR REPLACE
FUNCTION fn_get_status_chg_reason_id(
	   p_class_nm IN REF_CLASS.CLASS_NM%TYPE,
	   p_reason_nm IN REF_STATUS_CHG_REASON.REASON_NM%TYPE
	   ) RETURN REF_STATUS_CHG_REASON.REASON_ID%TYPE

IS
   v_reason_id REF_STATUS_CHG_REASON.REASON_ID%TYPE;
   v_class_id  REF_CLASS.CLASS_ID%TYPE;
   v_return    REF_STATUS_CHG_REASON.REASON_ID%TYPE;
BEGIN
   v_class_id := get_class_id(P_class_nm);
   if (p_reason_nm is null)
   then
      select sys_context('CTX_CHG_REASON_DFLT', v_class_id) into v_return from v_dual;
      return v_return;
   else

       if length(p_reason_nm) + 1 + length(v_class_id) <= 30 then
          select sys_context('CTX_CHG_REASON', v_class_id||'/'||p_reason_nm) into v_return from v_dual;
          return v_return;
       end if;

       select reason_id into v_reason_id
	   		  from REF_STATUS_CHG_REASON
			  where REF_STATUS_CHG_REASON.REASON_NM=p_reason_nm and
			  		REF_STATUS_CHG_REASON.CLASS_ID=v_class_id;
   end if;

   return v_reason_id;
end fn_get_status_chg_reason_id;
/
-- End of DDL Script for Function FN_GET_STATUS_CHG_REASON_ID

-- Start of DDL Script for Function FN_GET_STATUS_ID

CREATE OR REPLACE  FUNCTION FN_GET_STATUS_ID  (
   p_class    IN   ref_class.class_nm%TYPE,
   p_status   IN   ref_status.status%TYPE
)
   RETURN ref_status.status_id%TYPE
IS
   result       ref_status.status_id%TYPE;
   l_key        varchar2(200);
BEGIN
   l_key := p_class||'/'||p_status;
   if length(l_key) <= 30 then

      select sys_context('CTX_STATUS', l_key) into  result from v_dual;

      if result is not null then
         return result;
      end if;
   end if;

   SELECT s.status_id
     INTO result
     FROM ref_status s, ref_class c
    WHERE s.status = p_status AND s.class_id = c.class_id and c.class_nm = p_class;

   if length(l_key) <= 30 then
      prc_smp_ctx('CTX_STATUS', l_key, result);
   end if;

   RETURN (result);

END fn_get_status_id;
/
-- End of DDL Script for Function FN_GET_STATUS_ID

-- Start of DDL Script for Function FN_GET_STATUS_NM
CREATE OR REPLACE  FUNCTION FN_GET_STATUS_NM  (
   p_class       IN   ref_class.class_nm%TYPE,
   p_status_id   IN   ref_status.status_id%TYPE
)
   RETURN ref_status.status%TYPE
IS
   result       ref_status.status%TYPE;
BEGIN

   select sys_context('CTX_STATUS_NM', p_class||'/'||p_status_id) into result from v_dual ;

   if result is not null then
      return result;
   end if;

   SELECT s.status
     INTO result
     FROM ref_status s, ref_class c
    WHERE s.status_id = p_status_id AND s.class_id = c.class_id and c.class_nm = p_class;

   prc_smp_ctx('CTX_STATUS_NM', p_class||'/'||p_status_id, result);
   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_status_nm;
/
-- End of DDL Script for Function FN_GET_STATUS_NM

-- Start of DDL Script for Function FN_GET_SUB_EXT_KEY
CREATE OR REPLACE
FUNCTION fn_get_sub_ext_key (p_sub_id sub.sub_id%TYPE)
   RETURN sub.external_key%TYPE
IS
   result   sub_parm.val%TYPE;
BEGIN
   SELECT external_key
     INTO result
     FROM sub
    WHERE sub_id = p_sub_id;

   RETURN (result);
END fn_get_sub_ext_key;
/
-- End of DDL Script for Function FN_GET_SUB_EXT_KEY

-- Start of DDL Script for Function FN_GET_SUBID_BY_EXT_KEY
CREATE OR REPLACE
FUNCTION fn_get_subid_by_ext_key (p_extkey sub.external_key%TYPE)
   RETURN sub.sub_id%TYPE
IS
   result   sub.sub_id%TYPE;
BEGIN
   SELECT sub_id
     INTO result
     FROM sub
    WHERE external_key = p_extkey
      AND sub_status_id NOT IN (fn_get_status_id ('SubSpec', 'deleted'),
                                fn_get_status_id ('SubSpec', 'delete_in_progress')
                               );
   RETURN (result);
END fn_get_subid_by_ext_key;
/
-- End of DDL Script for Function FN_GET_SUBID_BY_EXT_KEY


-- Start of DDL Script for Function FN_GET_SVC_PROVIDER
CREATE OR REPLACE
FUNCTION fn_get_svc_provider (
   p_svc_provider_id   IN   svc_provider.svc_provider_id%TYPE
)
   RETURN svc_provider.svc_provider_nm%TYPE
IS

--
-- Purpose: To return the service provider name
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
   result   svc_provider.svc_provider_nm%TYPE;
BEGIN
   SELECT svc_provider_nm
     INTO result
     FROM svc_provider
    WHERE svc_provider_id = p_svc_provider_id;

   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_svc_provider;
/
-- End of DDL Script for Function FN_GET_SVC_PROVIDER




-- Start of DDL Script for Function FN_SUBSVC_GET_ISUNIQUE_PARM
CREATE OR REPLACE
FUNCTION fn_subsvc_get_isunique_parm (
   p_svcid      IN   sub_svc.svc_id%TYPE,
   p_parm_nm    IN   parm.parm_nm%TYPE,
   p_parm_val   IN   sub_svc_parm.val%TYPE
)
   RETURN CHAR
IS
   result     CHAR (1);
   v_status   ref_status.status%TYPE;
   v_enddt    sub_svc.end_dt%TYPE;
   v_status_deleted_id number := fn_get_status_id('SubSvcSpec', 'deleted');
   v_status_inactive_id NUMBER := fn_get_status_id('SubSvcSpec', 'inactive');
   v_parm_id  number;

   CURSOR c_check_parmval
   IS
      SELECT /*+first_rows*/
             sub_svc.sub_svc_id
        FROM sub_svc_parm, sub_svc
       WHERE sub_svc.sub_svc_id = sub_svc_parm.sub_svc_id
         AND (sub_svc.sub_svc_status_id NOT IN(v_status_deleted_id,v_status_inactive_id) OR sub_svc.end_dt > sysdate)
         AND sub_svc_parm.val = p_parm_val
         AND parm_id = v_parm_id
      UNION ALL
      SELECT
             sub_svc.sub_svc_id
        FROM sub_svc_parm, sub_svc
       WHERE sub_svc.sub_svc_id = sub_svc_parm.sub_svc_id
         AND (sub_svc.sub_svc_status_id NOT IN(v_status_deleted_id,v_status_inactive_id) OR sub_svc.end_dt > sysdate)
         AND sub_svc_parm.val = p_parm_val
         AND parm_id IN
                   (SELECT parm_id
                      FROM parmval_exclusion_grp a
                     WHERE a.parmval_exclusion_grp_nm IN
                                 (SELECT b.parmval_exclusion_grp_nm
                                    FROM parmval_exclusion_grp b
                                   WHERE b.parm_id = v_parm_id
                                             ));
   r_subsvcparm c_check_parmval%rowtype;

BEGIN
   v_parm_id := get_parm_id (p_parm_nm, get_class_id ('SubSvcSpec'), p_svcid);

   open c_check_parmval;
   fetch c_check_parmval into r_subsvcparm;
   if c_check_parmval%found then
      result := 'n';
   else
      result := 'y';
   end if;
   close c_check_parmval;
   /*
   FOR r_subsvcparm IN c_check_parmval
   LOOP
      v_status := r_subsvcparm.status;
      v_enddt := r_subsvcparm.end_dt;

      IF v_status != 'deleted'
      THEN
         result := 'n';
         EXIT;
      ELSIF      v_status = 'deleted'
             AND v_enddt > SYSDATE
      THEN
         result := 'n';
         EXIT;
      ELSE
         result := 'y';
      END IF;
   END LOOP;

   IF result IS NULL
   THEN
      result := 'y';
   END IF;
   */
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 'y';
END fn_subsvc_get_isunique_parm;
/
-- End of DDL Script for Function FN_SUBSVC_GET_ISUNIQUE_PARM


-- Start of DDL Script for Function FN_GET_PARENT_SVC_ID
CREATE OR REPLACE
FUNCTION fn_get_parent_svc_id (
   p_parent_sub_svc_id   IN   sub_svc.parent_sub_svc_id%TYPE
)
   RETURN sub_svc.svc_id%TYPE
IS
   result   sub_svc.svc_id%TYPE;
BEGIN
   SELECT svc_id
     INTO result
     FROM sub_svc
    WHERE sub_svc_id = p_parent_sub_svc_id;

   RETURN (result);
END fn_get_parent_svc_id;
/
-- End of DDL Script for Function FN_GET_PARENT_SVC_ID

-- Start of DDL Script for Function FN_GET_SUB_ADDR_ID
CREATE OR REPLACE
FUNCTION fn_get_sub_addr_id (p_location_id location.location_id%type)
   RETURN sub_addr.sub_addr_id%TYPE
IS
   v_sub_addr_id   sub_addr.sub_addr_id%TYPE;
BEGIN
   SELECT sub_addr.sub_addr_id
     INTO v_sub_addr_id
     FROM sub_addr
    WHERE sub_addr.location_id = p_location_id;

   RETURN (v_sub_addr_id);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN null;
END fn_get_sub_addr_id;
/
-- End of DDL Script for Function FN_GET_SUB_ADDR_ID

-- Start of DDL Script for Function FN_CONTACT_RESERVED_UPDATE
CREATE OR REPLACE
FUNCTION fn_contact_reserved_update (
   p_sub_contact_id   IN   sub_contact.sub_contact_id%TYPE,
   p_dbcol                 parm.db_col_nm%TYPE,
   p_dbval                 ordr_item_entity_chg.val%TYPE
)
   RETURN NUMBER
IS
   result           NUMBER;
   v_sql            VARCHAR2 (1999);
   v_parm_val       ordr_item_entity_chg.val%TYPE;
   v_parm_val_num   NUMBER;
   v_data_type      user_tab_columns.data_type%TYPE;
BEGIN
   SELECT user_tab_columns.data_type
     INTO v_data_type
     FROM sys.user_tab_columns
    WHERE user_tab_columns.table_name = 'SUB_CONTACT'
      AND user_tab_columns.column_name = UPPER (p_dbcol);

   IF p_dbcol = 'LOCATION_ID'
   THEN
      IF v_data_type = 'NUMBER'
      THEN
         v_parm_val_num := TO_NUMBER (p_dbval);
      END IF;

      v_parm_val_num := fn_get_location_id (v_parm_val_num);
      v_sql :=    'update sub_contact set '
               || p_dbcol
               || '='
               || v_parm_val_num
               || ' where sub_contact_id = '
               || TO_CHAR (p_sub_contact_id);
      EXECUTE IMMEDIATE v_sql;
      RETURN SQL%ROWCOUNT;
   END IF;
END fn_contact_reserved_update;
/
-- End of DDL Script for Function FN_CONTACT_RESERVED_UPDATE

-- Start of DDL Script for Function FN_SUBSVC_ADDR_RESERVED_UPDATE
CREATE OR REPLACE FUNCTION
fn_subsvc_addr_reserved_update (
   p_addr_id   sub_addr.sub_addr_id%TYPE,
   p_dbcol     parm.db_col_nm%TYPE,
   p_dbval     ordr_item_entity_chg.val%TYPE
)
   RETURN NUMBER
IS
   result   NUMBER;
BEGIN
   IF p_dbcol = 'default'
   THEN
      UPDATE sub_addr
         SET is_dflt = p_dbval
       WHERE sub_addr_id = p_addr_id;
   END IF;

   RETURN 1;
END fn_subsvc_addr_reserved_update;
/
-- End of DDL Script for Function FN_SUBSVC_ADDR_RESERVED_UPDATE

-- Start of DDL Script for Function fn_sp_get_prov_subsvc_parms
CREATE OR REPLACE
FUNCTION fn_sp_get_prov_subsvc_parms (p_sub_svc_id IN sub_svc.sub_svc_id%TYPE)
   RETURN t_cm_parmval
IS
   CURSOR c_child_svc
   IS
      SELECT sub_svc_id
        FROM sub_svc
       START WITH sub_svc_id = p_sub_svc_id
     CONNECT BY PRIOR sub_svc_id = parent_sub_svc_id
       ORDER BY LEVEL DESC;

   r_child_svc      c_child_svc%ROWTYPE;

   TYPE typ_parm_nm IS TABLE OF parm.parm_nm%TYPE;

   TYPE typ_val IS TABLE OF VARCHAR2 (1000);

   lt_parm_nm       typ_parm_nm;
   lt_val           typ_val;
   lt_cm_parmval   t_cm_parmval         := t_cm_parmval ();
BEGIN
   OPEN c_child_svc;
   FETCH c_child_svc INTO r_child_svc;
   CLOSE c_child_svc;

   SELECT p.parm_nm, ssp.val
     BULK COLLECT INTO lt_parm_nm, lt_val
     FROM sub_svc_parm ssp, parm p
    WHERE sub_svc_id = r_child_svc.sub_svc_id
      AND p.parm_id = ssp.parm_id;

   IF lt_parm_nm.LAST IS NOT NULL
   THEN
      lt_cm_parmval.EXTEND (
         lt_parm_nm.LAST
      );

      FOR i IN lt_parm_nm.FIRST .. lt_parm_nm.LAST
      LOOP
         lt_cm_parmval (i):= o_cm_parmval (
                                     lt_parm_nm (i),
                                     lt_val (i)
                                  );
      END LOOP;
   END IF;

   RETURN lt_cm_parmval;
EXCEPTION
   WHEN OTHERS
   THEN
      RAISE;
END;
/
-- End of DDL Script for Function fn_sp_get_prov_subsvc_parms


-- Start of DDL Script for Function FNC_SP_GET_SUB_EXT_KEY
CREATE OR REPLACE
FUNCTION fnc_sp_get_sub_ext_key (p_sub_id sub.sub_id%TYPE)
   RETURN sub.external_key%TYPE
IS
   result   sub.external_key%TYPE;
BEGIN
   SELECT sub.external_key
     INTO result
     FROM sub, ref_status
    WHERE sub.sub_status_id = ref_status.status_id
      AND sub.sub_id = p_sub_id;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_sub_ext_key;
/
-- End of DDL Script for Function FNC_SP_GET_SUB_EXT_KEY

-- Start of DDL Script for Function FNC_SP_GET_SUB_PARMVAL
CREATE OR REPLACE FUNCTION fnc_sp_get_sub_parmval (
   p_subid   sub.sub_id%TYPE
)
   RETURN t_sp_parmval
IS
   TYPE t_parm_id IS TABLE OF parm.parm_id%TYPE;


   TYPE t_val IS TABLE OF sub_parm.val%TYPE;

   result       t_sp_parmval := t_sp_parmval ();
   lt_parm_id   t_parm_id;
   lt_val       t_val;
BEGIN
   SELECT parm_id, val
     BULK COLLECT INTO lt_parm_id, lt_val
     FROM sub_parm
    WHERE sub_id = p_subid;

   IF lt_val.LAST IS NOT NULL
   THEN
      result.EXTEND (lt_val.LAST);

      FOR i IN lt_val.FIRST .. lt_val.LAST
      LOOP
         result (i) := o_sp_parmval (lt_parm_id (i), lt_val (i));
      END LOOP;
   END IF;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_sub_parmval;
/
-- End of DDL Script for Function FNC_SP_GET_SUB_PARMVAL

-- Start of DDL Script for Function FNC_SP_GET_SUBADDR_PARMVAL
CREATE OR REPLACE FUNCTION fnc_sp_get_subaddr_parmval (
   p_location_id   sub_addr.location_id%TYPE
)
   RETURN t_sp_parmval
IS
   TYPE t_parm_id IS TABLE OF parm.parm_id%TYPE;

   TYPE t_val IS TABLE OF sub_parm.val%TYPE;

   result       t_sp_parmval := t_sp_parmval ();
   lt_parm_id   t_parm_id;
   lt_val       t_val;
BEGIN
   SELECT parm_id, location_dtl_val
     BULK COLLECT INTO lt_parm_id, lt_val
     FROM location_dtl
    WHERE location_id = p_location_id;

   IF lt_val.LAST IS NOT NULL
   THEN
      result.EXTEND (lt_val.LAST);

      FOR i IN lt_val.FIRST .. lt_val.LAST
      LOOP
         result (i) := o_sp_parmval (lt_parm_id (i), lt_val (i));
      END LOOP;
   END IF;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_subaddr_parmval;
/
-- End of DDL Script for Function FNC_SP_GET_SUBADDR_PARMVAL

-- Start of DDL Script for Function FNC_SP_GET_SUBSVC_ADDRID
CREATE OR REPLACE
FUNCTION fnc_sp_get_subsvc_addrid (
   p_subsvcid   sub_svc.sub_svc_id%TYPE
)
   RETURN sub_svc_addr.sub_addr_id%TYPE
IS
   result   sub_svc_addr.sub_addr_id%TYPE;
BEGIN
   SELECT sub_addr_id
     INTO result
     FROM sub_svc_addr
    WHERE sub_svc_id = p_subsvcid;

   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_subsvc_addrid;
/
-- End of DDL Script for Function FNC_SP_GET_SUBSVC_ADDRID

-- Start of DDL Script for Function FNC_SP_GET_SUBSVC_PARMVAL
CREATE OR REPLACE FUNCTION fnc_sp_get_subsvc_parmval (
   p_subsvcid   sub_svc.sub_svc_id%TYPE
)
   RETURN t_sp_parmval
IS

   TYPE t_parm_id IS TABLE OF parm.parm_id%TYPE;

   TYPE t_val IS TABLE OF sub_parm.val%TYPE;

   result       t_sp_parmval := t_sp_parmval ();
   lt_parm_id   t_parm_id;
   lt_val       t_val;
BEGIN
   SELECT parm_id, val
     BULK COLLECT INTO lt_parm_id, lt_val
     FROM sub_svc_parm
    WHERE  sub_svc_id = p_subsvcid;

   IF lt_val.LAST IS NOT NULL
   THEN
      result.EXTEND (lt_val.LAST);

      FOR i IN lt_val.FIRST .. lt_val.LAST
      LOOP
         result (i) := o_sp_parmval (lt_parm_id (i), lt_val (i));
      END LOOP;
   END IF;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_subsvc_parmval;
/
-- End of DDL Script for Function FNC_SP_GET_SUBSVC_PARMVAL

-- Start of DDL Script for Function FNC_SP_GET_SUB_CONTACT_PARMVAL
CREATE OR REPLACE FUNCTION fnc_sp_get_sub_contact_parmval (
   p_sub_contact_id   sub_contact.SUB_CONTACT_ID%TYPE
)
   RETURN t_sp_parmval
IS
   TYPE t_parm_id IS TABLE OF parm.parm_id%TYPE;

   TYPE t_val IS TABLE OF sub_contact_parm.val%TYPE;

   result       t_sp_parmval := t_sp_parmval ();
   lt_parm_id   t_parm_id;
   lt_val       t_val;
BEGIN
   SELECT parm_id, val
     BULK COLLECT INTO lt_parm_id, lt_val
     FROM sub_contact_parm
    WHERE sub_contact_id = p_sub_contact_id;

   IF lt_val.LAST IS NOT NULL
   THEN
      result.EXTEND (lt_val.LAST);

      FOR i IN lt_val.FIRST .. lt_val.LAST
      LOOP
         result (i) := o_sp_parmval (lt_parm_id (i), lt_val (i));
      END LOOP;
   END IF;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_sub_contact_parmval;
/
-- End of DDL Script for Function FNC_SP_GET_SUB_CONTACT_PARMVAL


-- Start of DDL Script for Function FN_SUBSVC_GET_AREUNIQUE_PARMS
CREATE OR REPLACE FUNCTION fn_subsvc_get_areunique_parms (
   p_svcid        IN   sub_svc.svc_id%TYPE,
   p_parm_nm      IN   parm.parm_nm%TYPE,
   p_parm_val     IN   sub_svc_parm.val%TYPE,
   p_domain_nm    IN   parm.parm_nm%TYPE,
   p_domain_val   IN   sub_svc_parm.val%TYPE
)
   RETURN CHAR
IS
   TYPE t_sub_svc_id IS TABLE OF sub_svc_parm.sub_svc_id%TYPE;

   result          CHAR (1);
   v_status        ref_status.status%TYPE;
   v_enddt         sub_svc.end_dt%TYPE;
   v_parm_id       sub_svc_parm.parm_id%TYPE
                    := get_parm_id (p_parm_nm, get_class_id ('SubSvcSpec'), p_svcid);
   v_domain_id     sub_svc_parm.parm_id%TYPE
                  := get_parm_id (p_domain_nm, get_class_id ('SubSvcSpec'), p_svcid);
   lt_sub_svc_id   t_sub_svc_id;

   CURSOR c_status (
      v_sub_svc_id   sub_svc.sub_svc_id%TYPE
   )
   IS
      SELECT /*+first_rows*/
             ref_status.status, sub_svc.end_dt
        FROM ref_status, sub_svc, sub_svc_parm
       WHERE sub_svc.sub_svc_id = v_sub_svc_id
         AND sub_svc.sub_svc_id = sub_svc_parm.sub_svc_id
         AND parm_id = v_domain_id
         AND val = p_domain_val
         AND sub_svc.sub_svc_status_id = ref_status.status_id;
BEGIN
   SELECT /*+first_rows*/
          sub_svc_parm.sub_svc_id
     BULK COLLECT INTO lt_sub_svc_id
     FROM sub_svc_parm
    WHERE parm_id = v_parm_id
      AND sub_svc_parm.val = p_parm_val
   UNION
   SELECT /*+first_rows*/
          sub_svc_parm.sub_svc_id
     FROM sub_svc_parm
    WHERE parm_id IN
                (SELECT parm_id
                   FROM parmval_exclusion_grp a
                  WHERE a.parmval_exclusion_grp_nm IN
                              (SELECT b.parmval_exclusion_grp_nm
                                 FROM parmval_exclusion_grp b
                                WHERE b.parm_id = v_parm_id))
      AND sub_svc_parm.val = p_parm_val;

   IF lt_sub_svc_id.LAST IS NOT NULL
   THEN
      FOR i IN lt_sub_svc_id.FIRST .. lt_sub_svc_id.LAST
      LOOP
         OPEN c_status (lt_sub_svc_id (i));
         FETCH c_status INTO v_status, v_enddt;

         IF v_status != 'deleted'
         THEN
            result := 'n';
            EXIT;
         ELSIF      v_status = 'deleted'
                AND v_enddt > SYSDATE
         THEN
            result := 'n';
            EXIT;
         ELSE
            result := 'y';
         END IF;

         CLOSE c_status;
      END LOOP;
   END IF;

   IF result IS NULL
   THEN
      result := 'y';
   END IF;

   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 'y';
END fn_subsvc_get_areunique_parms;
/
-- End of DDL Script for Function FN_SUBSVC_GET_AREUNIQUE_PARMS


-- Start of DDL Script for Function Fnc_sp_get_server_node_id
CREATE OR REPLACE
FUNCTION  fnc_sp_get_server_node_id
(
   p_node_nm   SERVER_NODE.node_nm%TYPE
)
   RETURN SERVER_NODE.node_id%TYPE
IS
   result   SERVER_NODE.node_id%TYPE;
BEGIN
     SELECT node_id
     INTO result
     FROM  SERVER_NODE
     WHERE node_nm = p_node_nm;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND THEN -- 

   RETURN (null);
END fnc_sp_get_server_node_id;
/
-- End of DDL Script for Function Fnc_sp_get_server_node_id

-- Start of DDL Script for Function fnc_sp_get_subsvcid_by_ext_key
CREATE OR REPLACE FUNCTION fnc_sp_get_subsvcid_by_ext_key
(
   p_extkey   SUB_SVC.external_key%TYPE
)
   RETURN SUB_SVC.sub_svc_id%TYPE
IS
   result   SUB_SVC.sub_svc_id%TYPE;
BEGIN
   SELECT  SUB_SVC.sub_svc_id
     INTO result
     FROM  SUB_SVC, REF_STATUS
    WHERE SUB_SVC.sub_svc_status_id = REF_STATUS.status_id
      AND status != 'deleted'
      AND SUB_SVC.external_key = p_extkey;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN 
       RETURN NULL;
END fnc_sp_get_subsvcid_by_ext_key;
/
-- End of DDL Script for Function fnc_sp_get_subsvcid_by_ext_key

-- Start of DDL Script for Function fnc_sp_get_subsvc_ext_key
CREATE OR REPLACE FUNCTION fnc_sp_get_subsvc_ext_key (p_sub_svc_id SUB_SVC.sub_svc_id%TYPE)
   RETURN SUB_SVC.external_key%TYPE
IS
   result   SUB_SVC.external_key%TYPE;
BEGIN
   SELECT SUB_SVC.external_key
     INTO result
     FROM SUB_SVC
    WHERE SUB_SVC.sub_svc_id = p_sub_svc_id;

   RETURN (result);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fnc_sp_get_subsvc_ext_key;
/
-- End of DDL Script for Function fnc_sp_get_subsvc_ext_key

-- Start of DDL Script for Function FNC_SP_IS_SUB_LOCKED
CREATE OR REPLACE FUNCTION FNC_SP_IS_SUB_LOCKED
(
   p_extkey   SUB.external_key%TYPE
)
   RETURN CHAR
IS
   result CHAR;

   CURSOR c_chk_sub
   IS
      SELECT a.sub_ordr_id
         FROM SUB_OBJ_ENQ a, SUB b, REF_CLASS c
      WHERE a.sub_id = b.sub_id
         AND a.obj_class_id = c.class_id
         AND c.class_nm = 'SubSpec'
         AND b.external_key = p_extkey ;

   r_sub   c_chk_sub%ROWTYPE;
BEGIN
   OPEN c_chk_sub;
   FETCH c_chk_sub INTO r_sub;

   IF c_chk_sub%FOUND
   THEN
      result := 'y';
   ELSE
      result := 'n';
   END IF;

   CLOSE c_chk_sub;
   RETURN (result);
END FNC_SP_IS_SUB_LOCKED;
/
-- End of DDL Script for Function FNC_SP_IS_SUB_LOCKED

-- Start of DDL Script for Function FNC_SP_IS_SUBSVC_LOCKED
CREATE OR REPLACE FUNCTION FNC_SP_IS_SUBSVC_LOCKED
(
   p_subsvc_id   SUB_SVC.sub_svc_id%TYPE
)
   RETURN CHAR
IS
   result CHAR;

   CURSOR c_chk_subsvc
   IS
      SELECT  a.sub_ordr_id
         FROM  SUB_OBJ_ENQ a, REF_CLASS b  
      WHERE  a.obj_class_id = b.class_id
         AND b.class_nm = 'SubSvcSpec'
         AND obj_id IN (p_subsvc_id, (SELECT prerequisite_sub_svc_id FROM SUB_SVC_DEPY WHERE dependent_sub_svc_id = p_subsvc_id));

   r_subsvc   c_chk_subsvc%ROWTYPE;
BEGIN
   OPEN c_chk_subsvc;
   FETCH c_chk_subsvc INTO r_subsvc;

   IF c_chk_subsvc%FOUND
   THEN
      result := 'y';
   ELSE
      result := 'n';
   END IF;

   CLOSE c_chk_subsvc;
   RETURN (result);
END FNC_SP_IS_SUBSVC_LOCKED;
/
-- End of DDL Script for Function FNC_SP_IS_SUBSVC_LOCKED

CREATE OR REPLACE
FUNCTION fn_sp_get_ordr_line_parm (
   p_sub_ordr_item_id  number,
   p_parm_id           number) return varchar2 as
 v_val sub_ordr_item_parm.val%type;
begin
  select val into v_val from sub_ordr_item_parm 
    where sub_ordr_item_id = p_sub_ordr_item_id and parm_id = p_parm_id;
  return v_val;
exception
 when no_data_found then
   return null;
end;
/

CREATE OR REPLACE FUNCTION fnc_sp_get_old_entity_parms (
   p_orderid            IN       sub_ordr.SUB_ORDR_ID%TYPE,
   p_entityTypeId      IN        ref_object_typ.object_typ_id%TYPE,
   p_entityid           IN       sub_ordr_item.SUB_ENTITY_ID%TYPE
) return table_old_entity_parm_value PIPELINED
AS
    v_sql_string varchar2(800);
    TYPE CurTyp IS REF CURSOR;
    c_old_parms CurTyp ;
    c_parm_nms  CurTyp ;

    v_old_parm_val  ordr_item_entity_chg.OLD_VAL%TYPE;
    v_parm_id       parm.PARM_ID%TYPE;
    v_parm_nm       parm.parm_NM%type;

BEGIN

    open c_parm_nms for select nvl(base_parm_id,parm_id),parm_nm from parm,ref_object_typ typ
      where parm.class_id=typ.CLASS_ID and parm.is_reserved='n'
      and ((parm.object_id=typ.OBJECT_id) or (parm.object_id is null and typ.object_id is null))
      and object_typ_id=p_entityTypeId;

   loop
      fetch c_parm_nms into v_parm_id,v_parm_nm;
      exit when c_parm_nms%NOTFOUND;

      OPEN c_old_parms for 
	   select /*+  first_rows*/ chg.OLD_VAL 
  	   from ordr_item_entity_chg chg,sub_ordr_item item 
 	   where chg.SUB_ORDR_ITEM_ID=item.SUB_ORDR_ITEM_ID and 
 	   item.SUB_ORDR_ID > p_orderid and 
	   chg.parm_id =  v_parm_id and 
	   chg.sub_entity_id = p_entityid order by chg.sub_ordr_item_id;  

      FETCH c_old_parms INTO v_old_parm_val;

     IF c_old_parms%ROWCOUNT > 0
     THEN
         PIPE ROW (type_old_entity_parm_value(v_parm_nm,v_old_parm_val));
     END IF;
     close c_old_parms;
   end loop;

   close c_parm_nms;
   return;
END fnc_sp_get_old_entity_parms;
/

CREATE OR REPLACE FUNCTION fn_get_sub_type (
   p_sub_type_id   IN   ref_sub_typ.sub_typ_id%TYPE
)
   RETURN ref_sub_typ.sub_typ_nm%TYPE
IS
   result   ref_sub_typ.sub_typ_nm%TYPE;
BEGIN
   SELECT sub_typ_nm
     INTO result
     FROM ref_sub_typ
    WHERE sub_typ_id=p_sub_type_id;

   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_sub_type;
/

CREATE OR REPLACE FUNCTION fn_get_svc_provider (
   p_svc_provider_id   IN   svc_provider.svc_provider_id%TYPE
)
   RETURN svc_provider.svc_provider_nm%TYPE
IS
   result   svc_provider.svc_provider_nm%TYPE;
BEGIN
   SELECT svc_provider_nm
     INTO result
     FROM svc_provider
    WHERE svc_provider_id = p_svc_provider_id;

   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_svc_provider;
/

CREATE OR REPLACE FUNCTION fn_get_addr_typ (
   p_addr_typ_id   IN   ref_addr_typ.addr_typ_id%TYPE
)
   RETURN ref_addr_typ.addr_typ_nm%TYPE
IS
   v_addr_typ_nm   ref_addr_typ.addr_typ_nm%TYPE;
BEGIN
   SELECT addr_typ_nm
     INTO v_addr_typ_nm
     FROM ref_addr_typ
    WHERE addr_typ_id = p_addr_typ_id;

   RETURN v_addr_typ_nm;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END;
/

CREATE OR REPLACE FUNCTION fn_get_sub_contact_type (
   p_contact_typ_id   IN   ref_contact_typ.contact_typ_id%TYPE
)
   RETURN ref_contact_typ.contact_typ_nm%TYPE
IS

   result   ref_contact_typ.contact_typ_nm%TYPE;
BEGIN
   SELECT contact_typ_nm
     INTO result
     FROM ref_contact_typ
    WHERE contact_typ_id = p_contact_typ_id;

   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_sub_contact_type;
/

CREATE OR REPLACE FUNCTION fnc_sp_get_sub_user_parmval (
       p_sub_user_id   sub_user.SUB_USER_ID%TYPE
    )
       RETURN t_sp_parmval
    IS
       result       t_sp_parmval;
       
    BEGIN
       SELECT cast (multiset (select parm_id, val
       FROM sub_user_parm
	   WHERE sub_user_id = p_sub_user_id) AS t_sp_parmval)
	 into result 
		from dual;

       RETURN (result);
    EXCEPTION
       WHEN NO_DATA_FOUND
       THEN
          RETURN NULL;
END fnc_sp_get_sub_user_parmval;
/

CREATE OR REPLACE FUNCTION GetBaseSvcIdBySubSvcId (i_sub_svc_id IN number) 
return number is
    gg number;
    l_base_svc_id number;
begin
    select svc_id INTO l_base_svc_id from Sub_Svc where sub_svc_id = i_sub_svc_id;
    while l_base_svc_id is not null  
    loop
      gg := l_base_svc_id;
      select base_svc_id INTO l_base_svc_id from Svc where svc_id = l_base_svc_id;
    end loop;
    return gg;
EXCEPTION
  when no_data_found then
    return 0;
END;
/

CREATE OR REPLACE FUNCTION get_base_parm_id (pSvcId IN number,
                                      pParmName IN varchar2)
    return number is
    baseSvcId number;
    svcId number;
    gg number;
begin
    svcId := pSvcId;
    loop
      select base_svc_id into baseSvcId from svc where svc_id=svcId;
      exit when baseSvcId is null;     
      svcId :=baseSvcId;
    end loop;
    
    select parm_id INTO gg from parm where
        class_id = 100 AND
        object_id = svcId AND
        parm_nm = pParmName ;

    return gg;
END;
/
CREATE OR REPLACE FUNCTION fn_can_load_full_sub (p_sub_id SUB.sub_id%TYPE,p_number number)
   RETURN varchar
IS
   num number;
BEGIN
   --check whether this sub has selective loading service
   select sub_svc_id into num from sub_svc,svc where sub_id=p_sub_id and sub_svc_status_id!=29 
       and sub_svc.svc_id=svc.svc_id and svc.loading_policy='selective' and rownum=1;
   SELECT count(*) 
       INTO num
       FROM SUB_SVC
      WHERE sub_id = p_sub_id and rownum<=p_number+1 and sub_svc_status_id!=29;

   if (num>p_number) then
        return 'n';
   end if;
   RETURN 'y';
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 'y';
END fn_can_load_full_sub;
/

CREATE OR REPLACE FUNCTION fn_get_last_repairing_ordr(i_ordr_id IN number) return number is
    v_resolved_indicator V_ORDR_NQ_BY_ORIGINAL_ID.RESOLVED_INDICATOR%type;
    v_next_ordr_id sub_ordr.sub_ordr_id%type;
begin

    v_next_ordr_id:=i_ordr_id;    

   loop
    select v.sub_ordr_id,V.RESOLVED_INDICATOR into v_next_ordr_id,v_resolved_indicator from V_ORDR_NQ_BY_ORIGINAL_ID v where V.ORIGINAL_ORDR_ID=v_next_ordr_id;
    
    exit when v_resolved_indicator is null or v_resolved_indicator='n';
   end loop;
   return v_next_ordr_id;

   Exception  
      when no_data_found then
      begin
        if (v_resolved_indicator is null) then
          return null;
        else
          return v_next_ordr_id;
        end if;
      end;
END fn_get_last_repairing_ordr;
/