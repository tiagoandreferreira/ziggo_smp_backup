--============================================================================
--    $Id: sp_plsql_int_test.sql,v 1.22 2005/03/10 19:58:41 davidx Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Start of DDL Script for View V_SPM_INTTEST_ADDRESS
CREATE OR REPLACE FORCE VIEW v_spm_inttest_address (
   sub_id,
   sub_ext_key,
   addr_id,
   parm_id,
   parm_nm,
   capture_seq,
   parm_val )
AS
SELECT sa.sub_id, sub.external_key, sa.sub_addr_id, lc.parm_id, lc.parm_nm,
       lc.capture_seq_num seq, ld.location_dtl_val
  FROM sub_addr sa, parm lc, location_dtl ld, sub
 WHERE (sa.location_id = ld.location_id)
   AND (ld.parm_id = lc.parm_id)
   AND (sa.sub_id = sub.sub_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_ADDRESS


-- Start of DDL Script for View V_SPM_INTTEST_CONTACT_NAME
CREATE OR REPLACE FORCE VIEW v_spm_inttest_contact_name (
   sub_id,
   sub_ext_key,
   sub_contact_id,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix )
AS
SELECT sub.sub_id, sub.external_key, v_contact_name.contact_id,
          v_contact_name.salutation, v_contact_name.first_name,
          v_contact_name.middle_name, v_contact_name.last_name,
          v_contact_name.suffix
     FROM sub, v_contact_name
    WHERE (sub.sub_id = v_contact_name.sub_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_CONTACT_NAME


-- Start of DDL Script for View V_SPM_INTTEST_SUB
CREATE OR REPLACE VIEW v_spm_inttest_sub (
   sub_id,
   sub_ext_key,
   parent_sub_id,
   svc_provider_id,
   svc_provider,
   sub_status_id,
   sub_status,
   sub_type_id,
   sub_type )
AS
SELECT sub.sub_id, sub.external_key, sub.parent_sub_id, sub.svc_provider_id,
          svc_provider.svc_provider_nm, sub.sub_status_id,
          ref_status.status, sub.sub_typ_id, ref_sub_typ.sub_typ_nm
     FROM sub, ref_sub_typ, ref_status, svc_provider
    WHERE (sub.sub_typ_id = ref_sub_typ.sub_typ_id)
      AND (sub.svc_provider_id = svc_provider.svc_provider_id)
      AND (sub.sub_status_id = ref_status.status_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_SUB

-- Start of DDL Script for View V_SPM_INTTEST_SUBSVCMODEL
CREATE OR REPLACE FORCE VIEW v_spm_inttest_subsvcmodel (
   sub_id,
   sub_ext_key,
   subsvcid,
   svcid,
   svcnm,
   provisionstatus_id,
   provisionstatus,
   svclocationid,
   basesvcid,
   parentsvcid,
   level_id,
   sub_svc_parm_id,
   parm_nm,
   val )
AS
SELECT v_subsvcmodel.subid, sub.external_key, v_subsvcmodel.subsvcid,
       v_subsvcmodel.svcid, v_subsvcmodel.svcnm,
       v_subsvcmodel.provisionstatus_id, v_subsvcmodel.provisionstatus,
       v_subsvcmodel.svclocationid, v_subsvcmodel.basesvcid,
       v_subsvcmodel.parentsvcid, v_subsvcmodel.level_id, 0 sub_svc_parm_id,
       NULL parm_nm, NULL parm_val
  FROM v_subsvcmodel, sub
 WHERE (v_subsvcmodel.subid = sub.sub_id)
MINUS
SELECT v_subsvcmodel.subid, sub.external_key, v_subsvcmodel.subsvcid,
       v_subsvcmodel.svcid, v_subsvcmodel.svcnm,
       v_subsvcmodel.provisionstatus_id, v_subsvcmodel.provisionstatus,
       v_subsvcmodel.svclocationid, v_subsvcmodel.basesvcid,
       v_subsvcmodel.parentsvcid, v_subsvcmodel.level_id, 0 sub_svc_parm_id,
       NULL parm_nm, NULL val
  FROM v_subsvcmodel, sub, v_subsvcparm
 WHERE (v_subsvcmodel.subid = sub.sub_id)
   AND (v_subsvcmodel.subsvcid = v_subsvcparm.sub_svc_id)
UNION
SELECT v_subsvcmodel.subid, sub.external_key, v_subsvcmodel.subsvcid,
       v_subsvcmodel.svcid, v_subsvcmodel.svcnm,
       v_subsvcmodel.provisionstatus_id, v_subsvcmodel.provisionstatus,
       v_subsvcmodel.svclocationid, v_subsvcmodel.basesvcid,
       v_subsvcmodel.parentsvcid, v_subsvcmodel.level_id,
       v_subsvcparm.sub_svc_parm_id, v_subsvcparm.parm_nm, v_subsvcparm.val
  FROM v_subsvcmodel, sub, v_subsvcparm
 WHERE (v_subsvcmodel.subid = sub.sub_id)
   AND (v_subsvcmodel.subsvcid = v_subsvcparm.sub_svc_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_SUBSVCMODEL

-- Start of DDL Script for View V_SPM_INTTEST_SUB_ORDR
/* Remove view Apr-01 2003, not used 
CREATE OR REPLACE VIEW v_spm_inttest_sub_ordr (
   sub_id,
   sub_ext_key,
   sub_ordr_id,
   ordr_status_id,
   ordr_status_nm,
   modification_comments )
AS
SELECT sub_ordr.sub_id, sub.external_key, sub_ordr.sub_ordr_id,
       ref_status.status_id, ref_status.status,
       status_hist.reason_notes
  FROM status_hist, sub_ordr, ref_status, sub
 WHERE (status_hist.sub_ordr_id = sub_ordr.sub_ordr_id)
   AND (ref_status.status_id = status_hist.status_id)
   AND (sub_ordr.sub_id = sub.sub_id)
/
*/

SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_SUB_ORDR

-- Start of DDL Script for View V_SPM_INTTEST_SUB_ORDR_ITEM
CREATE OR REPLACE VIEW v_spm_inttest_sub_ordr_item (
   sub_id,
   sub_ext_key,
   sub_ordr_id,
   sub_ordr_item_id,
   ordr_action_id,
   ordr_action_nm,
   sub_contact_id,
   sub_svc_id,
   ordr_item_status_id,
   ordr_item_status_nm )
AS
SELECT sub_ordr.sub_id, sub.external_key, sub_ordr_item.sub_ordr_id,
       sub_ordr_item.sub_ordr_item_id, ref_ordr_action.ordr_action_id,
       ref_ordr_action.ordr_action_nm, sub_ordr_item.sub_contact_id,
       sub_ordr_item.sub_svc_id, ref_status.status_id,
       ref_status.status
  FROM sub_ordr_item, sub_ordr, ref_ordr_action, ref_status, sub
 WHERE (ref_ordr_action.ordr_action_id = sub_ordr_item.ordr_action_id)
   AND (sub_ordr_item.ordr_item_status_id =
                                      ref_status.status_id
       )
   AND (sub_ordr_item.sub_ordr_id = sub_ordr.sub_ordr_id)
   AND (sub_ordr.sub_id = sub.sub_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_SUB_ORDR_ITEM

-- Start of DDL Script for View V_SPM_INTTEST_SUB_PARM
CREATE OR REPLACE FORCE VIEW v_spm_inttest_sub_parm (
   sub_id,
   sub_ext_key,
   parm_nm,
   parm_val )
AS
SELECT sub.sub_id, sub.external_key, parm.parm_nm, sub_parm.val
  FROM sub, sub_parm, parm
 WHERE (sub_parm.sub_id = sub.sub_id) AND (sub_parm.parm_id = parm.parm_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_SUB_PARM

-- Start of DDL Script for View V_SPM_INTTEST_SVC_ORDR
CREATE OR REPLACE VIEW v_spm_inttest_svc_ordr (
   sub_ext_key,
   sub_id,
   sub_ordr_id,
   sub_ordr_item_id,
   svc_id,
   sub_svc_id,
   ordr_item_status_id,
   ordr_item_status_nm
)
AS
   SELECT sub.external_key, sub_ordr.sub_id, sub_ordr_item.sub_ordr_id,
          sub_ordr_item.sub_ordr_item_id, sub_svc.svc_id,
          sub_ordr_item.sub_svc_id, ref_status.status_id,
          ref_status.status
     FROM sub_ordr_item, sub_ordr, ref_status, sub, sub_svc
    WHERE (sub_ordr_item.ordr_item_status_id = ref_status.status_id)
      AND (sub_ordr_item.sub_ordr_id = sub_ordr.sub_ordr_id)
      AND (sub_ordr.sub_id = sub.sub_id)
      AND (sub_ordr_item.sub_svc_id = sub_svc.sub_svc_id)
/
SHOW ERRORS;
-- End of DDL Script for View V_SPM_INTTEST_SVC_ORDR
