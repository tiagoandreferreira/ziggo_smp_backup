--============================================================================
--    $Id: sp_plsql_objects.sql,v 1.12.18.1 2010/09/24 13:51:33 vaibhavn Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Clean-up existing ones
BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE O_SP_PARMVAL FORCE';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/


BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE T_SP_PARMVAL FORCE';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/


BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE o_sp_order_parm force';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE t_sp_order_parm force';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE o_sp_order_item force';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE SP_ARR_NUMBER_50 force';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE table_old_entity_parm_value force';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE type_old_entity_parm_value force';
EXCEPTION
   WHEN otherS
   THEN
      NULL;
END;
/


-- Start of DDL Script for Type SP_ARR_NUMBER_50
CREATE OR REPLACE TYPE SP_ARR_NUMBER_50 AS VARRAY(50) OF number;
/
-- End of DDL Script for Type SP_ARR_NUMBER_50

-- Start of DDL Script for Type O_SP_PARMVAL
CREATE OR REPLACE 
TYPE o_sp_parmval AS OBJECT (
   parm_id                       NUMBER (12),
   val                           VARCHAR2 (2000));

/
-- End of DDL Script for Type O_SP_PARMVAL


-- Start of DDL Script for Type T_SP_PARMVAL
CREATE OR REPLACE 
TYPE t_sp_parmval AS TABLE OF o_sp_parmval
/
-- End of DDL Script for Type T_SP_PARMVAL

create or replace 
type type_old_entity_parm_value  
as object (name varchar2(255),value varchar2(2000));
/

create or replace 
type table_old_entity_parm_value 
as table of type_old_entity_parm_value
/

--added type for submgr largesub implementation
CREATE OR REPLACE TYPE ID_ARRAY is VARRAY(10000) OF NUMBER(12,0);
/