--============================================================================
--    $Id: sp_plsql_pkgs.sql,v 1.42 2007/05/10 19:35:50 davidx Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================


-- Start of DDL Script for Package PKG_SP_SUBBRIEF
CREATE OR REPLACE
PACKAGE pkg_sp_subbrief
IS
   -- Author  : RFIKREE
   -- Created : 14/03/2002 5:45:05 PM
   -- Purpose : To used in congunction with the subsciber brief query

   FUNCTION get_sub_contact_parm (
      p_sub_id   sub_contact.sub_id%TYPE,
      p_parm     parm.parm_nm%TYPE
   )
      RETURN sub_contact_parm.val%TYPE;

   FUNCTION get_location_parm (
      p_sub_id   sub.sub_id%TYPE,
      p_parm     parm.parm_nm%TYPE
   )
      RETURN location_dtl.location_dtl_val%TYPE;

   FUNCTION get_subsvc_parm (
      p_sub_id   sub.sub_id%TYPE,
      p_parm     parm.parm_nm%TYPE,
      p_svc      svc.svc_nm%TYPE
   )
      RETURN sub_svc_parm.val%TYPE;

   FUNCTION get_subsvc_parm (
      p_sub_id   sub.sub_id%TYPE,
      p_parm     parm.parm_nm%TYPE,
      p_svc      svc.svc_nm%TYPE,
      p_parent_svc svc.svc_nm%TYPE
   )
      RETURN sub_svc_parm.val%TYPE;

   FUNCTION get_sub_parm (
      p_sub_id   sub_parm.sub_id%TYPE,
      p_parm     parm.parm_nm%TYPE
   )
      RETURN sub_parm.val%TYPE;

   FUNCTION get_sub_status (p_sub_id sub.sub_id%TYPE)
      RETURN ref_status.status%TYPE;

   FUNCTION get_svc_provider (p_sub_id IN sub.sub_id%TYPE)
      RETURN svc_provider.svc_provider_nm%TYPE;

  FUNCTION get_sub_typ (
      p_sub_id   IN   sub.sub_id%TYPE
   )
      RETURN ref_sub_typ.sub_typ_nm%TYPE;

  FUNCTION get_sub_ordr_parm (
      p_sub_ordr_id   sub_ordr.sub_ordr_id%TYPE,
      p_parm          parm.parm_nm%TYPE
   )
      RETURN sub_ordr_parm.val%TYPE;

  FUNCTION get_sub_ordr_owner(
     p_sub_ordr_id in sub_ordr.SUB_ORDR_ID%TYPE
  )
      RETURN sub_ordr_parm.val%TYPE;

 END pkg_sp_subbrief;
/


CREATE OR REPLACE
PACKAGE BODY pkg_sp_subbrief
IS
   -- Private constant declarations
   l_sub_contact_class_id         CONSTANT ref_class.class_id%TYPE
            := get_class_id ('SubContactSpec');
   l_sub_class_id                 CONSTANT ref_class.class_id%TYPE   := get_class_id (
                                                                           'SubSpec'
                                                                        );
   l_sub_svc_class_id             CONSTANT ref_class.class_id%TYPE
            := get_class_id ('SubSvcSpec');
   l_location_class_id            CONSTANT ref_class.class_id%TYPE
            := get_class_id ('SubAddressSpec');

   l_ordr_class_id   CONSTANT ref_class.class_id%TYPE := get_class_id ('Order');

   l_sub_svc_status_active_id constant number := fn_get_status_id('SubSvcSpec', 'active');

   -- Function and procedure implementations
   FUNCTION get_sub_ordr_owner(
     p_sub_ordr_id in sub_ordr.SUB_ORDR_ID%TYPE
   )
   RETURN sub_ordr_parm.val%TYPE
   is
     l_batch_id sub_ordr_parm.val%TYPE;
	 l_owner sub_ordr_parm.val%TYPE;
   begin
   	  select pkg_sp_subbrief.get_sub_ordr_parm(p_sub_ordr_id, 'batch_ordr_id') into l_batch_id from dual;
	  if (l_batch_id is null) then
	      select pkg_sp_subbrief.get_sub_ordr_parm(p_sub_ordr_id, 'ordr_owner') into l_owner from dual;
	  else
	      select pkg_sp_subbrief.get_sub_ordr_parm(l_batch_id, 'ordr_owner') into l_owner from dual;
	  end if;
	  return l_owner;
   end get_sub_ordr_owner;

   FUNCTION get_sub_contact_parm (
      p_sub_id   IN   sub_contact.sub_id%TYPE,
      p_parm     IN   parm.parm_nm%TYPE
   )
      RETURN sub_contact_parm.val%TYPE
   IS
      v_sub_contact_id sub_contact.SUB_CONTACT_ID%TYPE;
      result      sub_contact_parm.val%TYPE;
      l_parm_id   sub_contact_parm.parm_id%TYPE
                              := get_parm_id (p_parm, l_sub_contact_class_id);
   BEGIN
      --get most fresh contact
      --in normal case, we only have only contact for each sub
      --but we may have more than one. If it happens, 
      --this function returns parm from the most fresh contact in this sub
      select max(sub_contact_id)  into v_sub_contact_id 
	      from sub_contact where sub_contact.sub_id=p_sub_id;
      SELECT sub_contact_parm.val
        INTO result
        FROM sub_contact, sub_contact_parm
       WHERE sub_contact.sub_contact_id = sub_contact_parm.sub_contact_id
         AND SUB_CONTACT.contact_typ_id = 1
         AND sub_contact_parm.parm_id = l_parm_id
         AND sub_contact.sub_contact_id=v_sub_contact_id;

      RETURN result;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_sub_contact_parm;

   FUNCTION get_location_parm (
      p_sub_id   IN   sub.sub_id%TYPE,
      p_parm     IN   parm.parm_nm%TYPE
   )
      RETURN location_dtl.location_dtl_val%TYPE
   IS
      v_sub_addr_id sub_addr.SUB_ADDR_ID%TYPE;
      l_val    location_dtl.location_dtl_val%TYPE;
      l_parm   location_dtl.parm_id%TYPE
                                 := get_parm_id (p_parm, l_location_class_id);
   BEGIN
      --get address parm from most fresh default 'service' type address 
      select max(sub_addr_id) into v_sub_addr_id from sub_addr a, ref_addr_typ c
		   where a.SUB_ID=p_sub_id and c.addr_typ_nm = 'service' and a.addr_typ_id = c.addr_typ_id and a.IS_DFLT='y';

      SELECT b.location_dtl_val
        INTO l_val
        FROM sub_addr a, location_dtl b
       WHERE a.location_id = b.location_id
         AND b.parm_id = l_parm
         AND a.sub_addr_id=v_sub_addr_id;

      RETURN l_val;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_location_parm;

   FUNCTION get_subsvc_parm (
      p_sub_id   IN   sub.sub_id%TYPE,
      p_parm     IN   parm.parm_nm%TYPE,
      p_svc      IN   svc.svc_nm%TYPE
   )
      RETURN sub_svc_parm.val%TYPE
   IS
      l_val    sub_svc_parm.val%TYPE;
      l_svc    sub_svc.svc_id%TYPE         := get_svcid (p_svc);
      l_parm   sub_svc_parm.parm_id%TYPE
                           := get_parm_id (p_parm, l_sub_svc_class_id, l_svc);
   BEGIN
      SELECT b.val
        INTO l_val
        FROM sub_svc a, sub_svc_parm b
       WHERE a.sub_svc_id = b.sub_svc_id
         AND b.parm_id = l_parm
         AND a.sub_id = p_sub_id;

      RETURN l_val;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_subsvc_parm;

   FUNCTION get_subsvc_parm (
      p_sub_id       IN   sub.sub_id%TYPE,
      p_parm         IN   parm.parm_nm%TYPE,
      p_svc          IN   svc.svc_nm%TYPE,
      p_parent_svc   IN   svc.svc_nm%TYPE
   )
      RETURN sub_svc_parm.val%TYPE
   IS
      l_val          sub_svc_parm.val%TYPE;
      l_svc          sub_svc.svc_id%TYPE         := get_svcid (p_svc);
      l_parent_svc   sub_svc.svc_id%TYPE         := get_svcid (p_parent_svc);
      l_parm         sub_svc_parm.parm_id%TYPE
                           := get_parm_id (p_parm, l_sub_svc_class_id, l_svc);
   BEGIN
      SELECT val
        INTO l_val
        FROM sub_svc a,
             sub_svc b,
             sub_svc_parm c,
             sub d,
             svc_provider e,
             ref_sub_status f
       WHERE d.sub_id = a.sub_id
         AND d.svc_provider_id = e.svc_provider_id
         AND d.sub_status_id = f.sub_status_id
         AND a.parent_sub_svc_id = b.sub_svc_id
         AND a.sub_svc_id = c.sub_svc_id
         AND a.sub_id = p_sub_id
         AND b.sub_id = p_sub_id
         AND a.svc_id = l_svc
         AND b.svc_id = l_parent_svc
         AND c.parm_id = l_parm
         AND a.SUB_SVC_STATUS_ID = l_sub_svc_status_active_id
         AND b.SUB_SVC_STATUS_ID = l_sub_svc_status_active_id;

      RETURN l_val;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_subsvc_parm;

   FUNCTION get_sub_parm (
      p_sub_id   IN   sub_parm.sub_id%TYPE,
      p_parm     IN   parm.parm_nm%TYPE
   )
      RETURN sub_parm.val%TYPE
   IS
      result   sub_parm.val%TYPE;
      l_parm   sub_parm.parm_id%TYPE   := get_parm_id (
                                             p_parm,
                                             l_sub_class_id
                                          );
   BEGIN
      SELECT sub_parm.val
        INTO result
        FROM sub_parm
       WHERE sub_parm.parm_id = l_parm AND sub_parm.sub_id = p_sub_id;

      RETURN result;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_sub_parm;

   FUNCTION get_sub_status (p_sub_id IN sub.sub_id%TYPE)
      RETURN ref_status.status%TYPE
   IS
      result   ref_status.status%TYPE;
   BEGIN
      SELECT ref_status.status
        INTO result
        FROM sub, ref_sub_status, ref_status
       WHERE sub.sub_status_id = ref_sub_status.sub_status_id
         AND ref_sub_status.sub_status_id = ref_status.status_id
         AND sub.sub_id = p_sub_id;
   END get_sub_status;

   FUNCTION get_svc_provider (p_sub_id IN sub.sub_id%TYPE)
      RETURN svc_provider.svc_provider_nm%TYPE
   IS
      result   svc_provider.svc_provider_nm%TYPE;
   BEGIN
      SELECT svc_provider.svc_provider_nm
        INTO result
        FROM sub, svc_provider
       WHERE sub.svc_provider_id = svc_provider.svc_provider_id
         AND sub.sub_id = p_sub_id;
   END get_svc_provider;

   FUNCTION get_sub_typ (p_sub_id   IN   sub.sub_id%TYPE)
      RETURN ref_sub_typ.sub_typ_nm%TYPE
   IS
      result   ref_sub_typ.sub_typ_nm%TYPE;
   BEGIN
      SELECT b.sub_typ_nm
        INTO result
        FROM sub a, ref_sub_typ b
       WHERE a.sub_typ_id = b.sub_typ_id
         AND a.sub_id = p_sub_id;

         RETURN result;

   EXCEPTION
      WHEN TOO_MANY_ROWS
      THEN
         RETURN NULL;
   END get_sub_typ;

   FUNCTION get_sub_ordr_parm (
      p_sub_ordr_id   sub_ordr.sub_ordr_id%TYPE,
      p_parm     parm.parm_nm%TYPE
   )
      RETURN sub_ordr_parm.val%TYPE
    IS
      v_val sub_ordr_parm.val%TYPE;
    BEGIN
        select val into v_val from sub_ordr_parm
           where sub_ordr_id = p_sub_ordr_id  and parm_id = get_parm_id (p_parm, l_ordr_class_id);
        return v_val;
    exception
      when no_data_found then
        return null;
    END;


END pkg_sp_subbrief;
/
-- End of DDL Script for Package PKG_SP_SUBBRIEF

