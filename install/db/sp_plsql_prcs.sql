--============================================================================
--    $Id: sp_plsql_prcs.sql,v 1.114.10.3.2.13 2012/01/02 03:16:09 mattl Exp $
--
--  REVISION HISTORYf
--  * Based on CVS log
--============================================================================
CREATE OR REPLACE PROCEDURE syncSvcProvider (
   i_svc_provider           IN   VARCHAR2
)
IS
   c   NUMBER (10);
   max_id number(10);
BEGIN
   select nvl(max(svc_provider_id)+1,1) into max_id from svc_provider;
   SELECT COUNT (*)
     INTO c
     FROM svc_provider
    WHERE svc_provider_nm = i_svc_provider;

   IF c <> 1
   THEN
      INSERT INTO svc_provider
                  (svc_provider_id, svc_provider_nm, created_by,created_dtm)
           VALUES (max_id, i_svc_provider, 'sync',sysdate);
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncSubEntity (
   i_obj_typ_id           IN   NUMBER,
   i_class_id             IN   NUMBER,
   i_object_nm            IN   VARCHAR2,
   i_object_id            IN   NUMBER
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_object_typ
    WHERE object_typ_id = i_obj_typ_id;

   IF c = 1
   THEN
      UPDATE ref_object_typ
         SET class_id = i_class_id,
             object_nm = i_object_nm,
       object_id = i_object_id
       WHERE object_typ_id = i_obj_typ_id;
   ELSE
      INSERT INTO ref_object_typ
                  (object_typ_id, class_id, object_nm, object_id)
           VALUES (i_obj_typ_id, i_class_id, i_object_nm, i_object_id);
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncRefAssocs (
   i_assoc_typ_id           IN   NUMBER,
   i_assoc_typ_nm           IN   VARCHAR2
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_assoc_typ
    WHERE assoc_typ_id = i_assoc_typ_id;

   IF c = 1
   THEN
      UPDATE ref_assoc_typ
         SET assoc_typ_nm = i_assoc_typ_nm
       WHERE assoc_typ_id = i_assoc_typ_id;
   ELSE
      INSERT INTO ref_assoc_typ
                  (assoc_typ_id, assoc_typ_nm)
           VALUES (i_assoc_typ_id, i_assoc_typ_nm);
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncsvcaction (
   i_svc_action_id        IN   NUMBER,
   i_svc_action_nm        IN   VARCHAR2,
   i_svc_id               IN   NUMBER,
   i_svc_action_typ       IN   VARCHAR2,
   i_base_svc_action_id   IN   NUMBER
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM svc_action
    WHERE svc_action_id = i_svc_action_id;

   IF c = 1
   THEN
      UPDATE svc_action
         SET svc_action_nm = i_svc_action_nm,
             svc_id = i_svc_id,
             svc_action_typ = i_svc_action_typ,
             base_svc_action_id = i_base_svc_action_id
       WHERE svc_action_id = i_svc_action_id;
   ELSE
      INSERT INTO svc_action
                  (svc_action_id, svc_action_nm, svc_id,
                   svc_action_typ, base_svc_action_id
                  )
           VALUES (i_svc_action_id, i_svc_action_nm, i_svc_id,
                   i_svc_action_typ, i_base_svc_action_id
                  );
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE updatesvccatver (i_ver IN NUMBER)
AS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM VERSION
    WHERE module_name = 'SvcCat';

   IF c = 0
   THEN
      INSERT INTO VERSION
                  (module_name, created_dtm, created_by, spec_title,
                   spec_vendor, spec_version, impl_title, impl_version,
                   built_by, built_dtm
                  )
           VALUES ('SvcCat', SYSDATE, 'sync', 'SMP',
                   'Sigma Systems', '4', 'sp_plsql_prcs',
                   i_ver,
                   'Sigma', SYSDATE
                  );
   ELSE
      UPDATE VERSION
         SET impl_version = i_ver
       WHERE module_name = 'SvcCat';
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncparm (
   i_parm_id           IN   NUMBER,
   i_conv_typ          IN   VARCHAR2,
   i_class_id          IN   NUMBER,
   i_parm_nm           IN   VARCHAR2,
   i_object_id         IN   NUMBER,
   i_data_typ_nm       IN   VARCHAR2,
   i_cap_seq_num       IN   NUMBER,
   i_base_parm_id      IN   NUMBER,
   i_is_read_only      IN   VARCHAR2,
   i_is_required       IN   VARCHAR2,
   i_is_primary_key    IN   VARCHAR2,
   i_is_reserved       IN   VARCHAR2,
   i_is_unique         IN   VARCHAR2,
   i_dflt_val          IN   VARCHAR2,
   i_jmf_src           IN   VARCHAR2,
   i_jmf_dflt_val      IN   VARCHAR2,
   i_jmf_disp_format   IN   VARCHAR2,
   i_db_col_nm         IN   VARCHAR2
)
AS
   c   NUMBER (10);
BEGIN
   SELECT /*+ INDEX(parm parm_pk) */ COUNT (*)
     INTO c
     FROM parm
    WHERE parm_id = i_parm_id;

   IF c = 0
   THEN
      INSERT INTO parm
                  (parm_id, conversion_typ, class_id, parm_nm,
                   object_id, data_typ_nm, capture_seq_num,
                   base_parm_id, is_read_only, is_required,
                   is_primary_key, is_reserved, is_unique, dflt_val,
                   jmf_src, jmf_dflt_val, jmf_display_format,
                   db_col_nm, created_dtm, created_by
                  )
           VALUES (i_parm_id, i_conv_typ, i_class_id, i_parm_nm,
                   i_object_id, i_data_typ_nm, i_cap_seq_num,
                   i_base_parm_id, i_is_read_only, i_is_required,
                   i_is_primary_key, i_is_reserved, i_is_unique, i_dflt_val,
                   i_jmf_src, i_jmf_dflt_val, i_jmf_disp_format,
                   i_db_col_nm, SYSDATE, 'sync'
                  );
   ELSE
      UPDATE /*+ INDEX(parm parm_pk) */ parm
         SET conversion_typ = i_conv_typ,
             class_id = i_class_id,
             parm_nm = i_parm_nm,
             object_id = i_object_id,
             data_typ_nm = i_data_typ_nm,
             capture_seq_num = i_cap_seq_num,
             base_parm_id = i_base_parm_id,
             is_read_only = i_is_read_only,
             is_required = i_is_required,
             is_primary_key = i_is_primary_key,
             is_reserved = i_is_reserved,
             is_unique = i_is_unique,
             dflt_val = i_dflt_val,
             jmf_src = i_jmf_src,
             jmf_dflt_val = i_jmf_dflt_val,
             jmf_display_format = i_jmf_disp_format,
             db_col_nm = i_db_col_nm,
             modified_by = 'syncparm'
       WHERE parm_id = i_parm_id;
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncsvc (
   i_svcid         IN   NUMBER,
   i_svcnm         IN   VARCHAR2,
   i_isorderable   IN   VARCHAR2,
   i_effdtm        IN   DATE,
   i_expdtm        IN   DATE,
   i_svcstate      IN   VARCHAR2
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM svc
    WHERE svc_id = i_svcid;

   IF c = 1
   THEN
      UPDATE svc
         SET svc_nm = i_svcnm,
             is_orderable = i_isorderable,
             eff_dtm = i_effdtm,
             exp_dtm = i_expdtm,
             svc_state = i_svcstate
       WHERE svc_id = i_svcid;
   ELSE
      INSERT INTO svc
                  (svc_id, svc_nm, is_orderable, eff_dtm, exp_dtm,
                   svc_state
                  )
           VALUES (i_svcid, i_svcnm, i_isorderable, i_effdtm, i_expdtm,
                   i_svcstate
                  );
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncAssocRule (
   i_rulename             IN   VARCHAR2,
   i_typename             IN   VARCHAR2,
   i_a_endclass           IN   VARCHAR2,
   i_a_endtype            IN   VARCHAR2,
   i_z_endclass           IN   VARCHAR2,
   i_z_endtype            IN   VARCHAR2,
   i_a_end_loadingpolicy  IN   VARCHAR2,
   i_z_end_loadingpolicy  IN   VARCHAR2
)
IS
   p_rulecount     NUMBER (10);
   p_new_ruleid    NUMBER (12);
   p_typeid        NUMBER (10);
   p_a_endobjectid NUMBER (10);
   p_z_endobjectid NUMBER (10);
BEGIN
   SELECT ASSOC_TYP_ID
     INTO p_typeid
     FROM REF_ASSOC_TYP
     WHERE ASSOC_TYP_NM = i_typename;

   IF i_a_endtype IS NULL
   THEN
     SELECT OBJECT_TYP_ID
       INTO p_a_endobjectid
       FROM REF_OBJECT_TYP
       WHERE CLASS_ID = GET_CLASS_ID(i_a_endclass) and OBJECT_NM IS NULL;
   ELSE
     SELECT OBJECT_TYP_ID
       INTO p_a_endobjectid
       FROM REF_OBJECT_TYP
       WHERE CLASS_ID = GET_CLASS_ID(i_a_endclass) and OBJECT_NM = i_a_endtype;
   END IF;

   IF i_z_endtype IS NULL
   THEN
     SELECT OBJECT_TYP_ID
       INTO p_z_endobjectid
       FROM REF_OBJECT_TYP
       WHERE CLASS_ID = GET_CLASS_ID(i_z_endclass) and OBJECT_NM IS NULL;
   ELSE
     SELECT OBJECT_TYP_ID
       INTO p_z_endobjectid
       FROM REF_OBJECT_TYP
       WHERE CLASS_ID = GET_CLASS_ID(i_z_endclass) and OBJECT_NM = i_z_endtype;
    END IF;


   SELECT COUNT (*)
     INTO p_rulecount
     FROM REF_ASSOC_RULE
     WHERE ASSOC_RULE_NAME = i_rulename;

   IF p_rulecount = 1
   THEN
      UPDATE REF_ASSOC_RULE
         SET ASSOC_TYP_ID = p_typeid,
             AEND_OBJECT_TYP_ID = p_a_endobjectid,
             ZEND_OBJECT_TYP_ID = p_z_endobjectid,
             AEND_LOADING_POLICY = i_a_end_loadingpolicy,
             ZEND_LOADING_POLICY = i_z_end_loadingpolicy
       WHERE ASSOC_RULE_NAME = i_rulename;
   ELSE
      SELECT NVL(MAX(ASSOC_RULE_ID), 0) + 1
        INTO p_new_ruleid
        FROM REF_ASSOC_RULE;

      INSERT INTO REF_ASSOC_RULE
                  (ASSOC_RULE_ID, ASSOC_RULE_NAME, ASSOC_TYP_ID, AEND_OBJECT_TYP_ID,
                  ZEND_OBJECT_TYP_ID, AEND_LOADING_POLICY, ZEND_LOADING_POLICY)
           VALUES (p_new_ruleid, i_rulename, p_typeid, p_a_endobjectid, p_z_endobjectid,
                  i_a_end_loadingpolicy, i_z_end_loadingpolicy);
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE SP_CONTACT_ADD (
   p_subid            IN       sub_contact.sub_id%TYPE,
   p_sub_contact_id   in      sub_contact.sub_contact_id%TYPE,
   p_contact_typ_nm   IN       ref_contact_typ.contact_typ_nm%TYPE,
   p_status           IN       ref_status.status%TYPE,
   p_user             IN       sub_contact.created_by%TYPE
)
AS
   v_status_id        ref_status.status_id%TYPE;
   v_contact_typ_id   ref_contact_typ.contact_typ_id%TYPE;
BEGIN
   --- Based on the contact type get the type ID
   SELECT contact_typ_id
     INTO v_contact_typ_id
     FROM ref_contact_typ
    WHERE contact_typ_nm = p_contact_typ_nm;

   --- Based on the contact status, get the status ID
   v_status_id:=fn_get_status_id('SubContactSpec',p_status);

   INSERT INTO sub_contact
               (sub_contact_id, contact_typ_id, sub_id,
                sub_contact.contact_status_id, samp_ver, created_by, created_dtm)
        VALUES (p_sub_contact_id, v_contact_typ_id, p_subid,v_status_id, 1, p_user, sysdate);

END sp_contact_add;
/
-- End of DDL Script for Procedure SP_CONTACT_ADD


-- Start of DDL Script for Procedure SP_LOCATION_ADD
CREATE OR REPLACE
PROCEDURE sp_location_add (
   p_location_id   IN OUT   location.location_id%TYPE,
   p_user          IN       location.created_by%TYPE DEFAULT 'spm'
)
IS
BEGIN
   SELECT location_seq.NEXTVAL
     INTO p_location_id
     FROM DUAL;

   INSERT INTO location
               (location_id, created_by)
        VALUES (p_location_id, p_user);
END sp_location_add;
/
-- End of DDL Script for Procedure SP_LOCATION_ADD




-- Start of DDL Script for Procedure SP_SUB_CONTACT_UPDATE
CREATE OR REPLACE PROCEDURE sp_sub_contact_update (
   p_sub_contact_id   IN   sub_contact.sub_contact_id%TYPE,
   p_parm_id          IN   parm.parm_id%TYPE,
   p_parmval          IN   sub_parm.val%TYPE,
   p_user                  IN   sub_svc.modified_by%TYPE
)
AS
   v_db_col_nm        parm.db_col_nm%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
   v_class_id         parm.class_id%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
BEGIN
   SELECT db_col_nm, is_reserved, class_id, dflt_val
     INTO v_db_col_nm, v_is_reserved, v_class_id,v_dflt_val
     FROM parm
    WHERE parm_id = p_parm_id;

   IF v_is_reserved = 'y' AND v_class_id = get_class_id ('SubContactSpec')
   THEN
      -- reserve parm update
      sp_contact_reserved_update (p_sub_contact_id, v_db_col_nm,
                                     p_parmval);
   ELSE
      -- regular parm update
      IF p_parmval IS NULL and v_dflt_val is null or p_parmval=v_dflt_val
      THEN
         DELETE      sub_contact_parm
               WHERE parm_id = p_parm_id
                     AND sub_contact_id = p_sub_contact_id;
      ELSE
       update sub_contact_parm set val = p_parmval where sub_contact_id =p_sub_contact_id and parm_id=p_parm_id;
     if SQL%ROWCOUNT=0 then
        insert into sub_contact_parm(parm_id, sub_contact_id, val)
               VALUES (p_parm_id, p_sub_contact_id, p_parmval);
       end if;
      END IF;
   END IF;
END sp_sub_contact_update;
/
-- End of DDL Script for Procedure SP_SUB_CONTACT_UPDATE



-- Start of DDL Script for Procedure SP_SUB_UPDATE
CREATE OR REPLACE PROCEDURE sp_sub_update (
   p_subid                 IN   sub_svc.sub_svc_id%TYPE,
   p_parm_id               IN   parm.parm_id%TYPE,
   p_parmval               IN   sub_parm.val%TYPE,
   p_user                  IN   sub_svc.modified_by%TYPE
)
IS
   v_parm_nm       parm.parm_nm%TYPE;
   v_db_col_nm     parm.db_col_nm%TYPE;
   v_data_typ_nm   parm.data_typ_nm%TYPE;
   v_is_reserved   parm.is_reserved%TYPE;
   v_dflt_val      parm.dflt_val%TYPE;
   v_sub_typ_id    sub.SUB_TYP_ID%TYPE;
BEGIN
   --- Find out if it is reserved
   SELECT parm_nm, data_typ_nm, is_reserved, db_col_nm,dflt_val
     INTO v_parm_nm, v_data_typ_nm, v_is_reserved, v_db_col_nm,v_dflt_val
     FROM parm
    WHERE parm_id = p_parm_id;

   --- If it is reserved and this is a status change ,
   --- it should be done by state machine
  if (v_parm_nm != 'status')
  then
   IF  (LOWER (v_is_reserved) = 'y')
   then
      if (v_db_col_nm is not null)
      then
        if(UPPER(v_db_col_nm) = 'SUB_TYP_ID')
        then
          select SUB_TYP_ID into v_sub_typ_id from REF_SUB_TYP where SUB_TYP_NM = p_parmval;
          if(v_sub_typ_id is NULL)
          then
            raise_application_error(-20001, 'not defined sub_type_nm :'||p_parmval);
          else
            update sub set SUB_TYP_ID=v_sub_typ_id where sub_id = p_subid;
          end if;
        else
          sp_sub_reserved_update(p_subid, v_db_col_nm, p_parmval);
        end if;
      else
        raise_application_error(-20001, 'no db_col_nm define for reserved parm:'||v_parm_nm);
      end if;
   else
     IF p_parmval IS NULL and v_dflt_val is null or p_parmval=v_dflt_val
       THEN
          DELETE      sub_parm
            WHERE parm_id = p_parm_id AND sub_id = p_subid;
     ELSE
            update sub_parm set val=p_parmval where  parm_id = p_parm_id AND sub_id = p_subid;
                if SQL%ROWCOUNT=0 then
                   insert into sub_parm(parm_id, sub_id, val)
            VALUES (p_parm_id, p_subid, p_parmval);
            end if;
     END IF;
   END IF;
  end if;
END sp_sub_update;
/
-- End of DDL Script for Procedure SP_SUB_UPDATE


-- Start of DDL Script for Procedure SP_SUBADDR_ADD
CREATE OR REPLACE PROCEDURE sp_subaddr_add (
   p_subid         IN       sub_addr.sub_id%TYPE,
   p_sub_addr_id   IN      sub_addr.sub_addr_id%TYPE,
   p_addr_typ      IN       ref_addr_typ.addr_typ_nm%TYPE,
   p_preferrednm   IN       sub_addr.preferred_nm%TYPE,
   p_isdflt        IN       sub_addr.is_dflt%TYPE,
   p_status        IN       ref_status.status%TYPE,
   p_user          IN       sub_addr.created_by%TYPE
)
AS
   v_addr_typ_id      ref_addr_typ.addr_typ_id%TYPE;
   v_location_id      location.location_id%TYPE;
   v_addr_status_id   sub_addr.addr_status_id%TYPE;
BEGIN

   --- Get the address satus id passed in (or the default value)
   v_addr_status_id := fn_get_addr_status_id (p_status);

   --- Select the address type id based on the type passed in
   v_addr_typ_id := fn_addr_typ_id (p_addr_typ);
   --- Create the location
   sp_location_add (v_location_id, p_user);

   --- Once we have the location create the subscriber address
   INSERT INTO sub_addr
               (sub_id, sub_addr_id, addr_typ_id, location_id,
                preferred_nm, is_dflt, addr_status_id, created_by, samp_ver, created_dtm, pre_status_id)
        VALUES (p_subid, p_sub_addr_id, v_addr_typ_id, v_location_id,
                p_preferrednm, p_isdflt, v_addr_status_id, p_user, 1, sysdate, v_addr_status_id);

END sp_subaddr_add;
/

-- End of DDL Script for Procedure SP_SUBADDR_ADD



-- Start of DDL Script for Procedure SP_SUBADDR_PARM_ADD
CREATE OR REPLACE PROCEDURE sp_subaddr_parm_add (
   p_subaddrid   IN       sub_addr.sub_addr_id%TYPE,
   p_parm_id     IN       parm.parm_id%TYPE,
   p_parmval     IN       sub_parm.val%TYPE
)
AS
   v_loc_id         location.location_id%TYPE;
BEGIN
   -- Get the location id <<spm passes in its subaddr id we need to get the location id
   SELECT location_id
     INTO v_loc_id
     FROM sub_addr
    WHERE sub_addr_id = p_subaddrid;

   INSERT INTO location_dtl
     (location_id, parm_id, location_dtl_val)
     VALUES (v_loc_id, p_parm_id, p_parmval);

END sp_subaddr_parm_add;
/
-- End of DDL Script for Procedure SP_SUBADDR_PARM_ADD

-- Start of DDL Script for Procedure SP_SUBADDR_update
CREATE OR REPLACE PROCEDURE sp_subaddr_update (
   p_subaddrid   IN       sub_addr.sub_addr_id%TYPE,
   p_parm_id     IN       parm.parm_id%TYPE,
   p_parmval     IN       sub_parm.val%TYPE,
   p_user                  IN   sub_svc.modified_by%TYPE
)
AS
   v_loc_id         location.location_id%TYPE;
   v_parm_nm          parm.parm_nm%TYPE;
   v_db_col_nm        parm.db_col_nm%TYPE;
   v_data_typ_nm      parm.data_typ_nm%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
BEGIN
   -- Get the location id <<spm passes in its subaddr id we need to get the location id
   SELECT location_id
     INTO v_loc_id
     FROM sub_addr
    WHERE sub_addr_id = p_subaddrid;

   --- Find out if it is reserved
   SELECT parm_nm, data_typ_nm, is_reserved, db_col_nm, dflt_val
     INTO v_parm_nm, v_data_typ_nm, v_is_reserved, v_db_col_nm,v_dflt_val
     FROM parm
    WHERE parm_id = p_parm_id;

   --- If it is reserved and this is a status change then
   --- skip it, it should be done in state machine
   IF v_parm_nm != 'status'
   THEN
       IF  LOWER (v_is_reserved) = 'y'
       THEN
          if (v_db_col_nm IS NOT NULL)
          then
            sp_subaddr_reserved_update (p_subaddrid, v_db_col_nm, p_parmval);
          else
            if p_parmval IS NOT NULL
            then
                 raise_application_error(-20001, 'not db_col_nm defined for reserved parm:'||v_parm_nm);
            end if;
          end if;

       ELSE ---- this is a parameter change
          -- update the location_dtl table
          IF (p_parmval IS NULL and v_dflt_val is null or p_parmval = v_dflt_val)
          THEN
             DELETE  location_dtl
                WHERE location_id = v_loc_id AND parm_id = p_parm_id;
          ELSE
       update location_dtl set location_dtl_val = p_parmval
          where location_id=v_loc_id and parm_id=p_parm_id;
             if SQL%ROWCOUNT=0 then
           INSERT into location_dtl(location_id, parm_id, location_dtl_val)
                  VALUES (v_loc_id, p_parm_id, p_parmval);
       end if;
          END IF;
        END IF;
    END IF;
END sp_subaddr_update;
/
-- End of DDL Script for Procedure SP_SUBADDR_update

-- Fake view to help JDBC batch processing
create or replace view   v_sub_addr_parm as
   select 1 sub_addr_id, rpad('1', 255, '1') PARM_NM,
       rpad('1', 2000, '1') val
       from ref_class;

create or replace trigger v_sub_addr_parm_II instead of insert on v_sub_addr_parm
for each row
begin
    sp_subaddr_parm_add (:new.sub_addr_id, :new.parm_nm, :new.val);
end;
/
-- end fake view


-- Start of DDL Script for Procedure SP_SUBSVC_UPDATE
CREATE OR REPLACE PROCEDURE sp_subsvc_update (
   p_subsvcid              IN   sub_svc.sub_svc_id%TYPE,
   p_parm_id               IN   parm.parm_id%TYPE,
   p_parmval               IN   sub_svc_parm.val%TYPE,
   p_user                  IN   sub_svc.modified_by%TYPE
)
IS
   v_svcid            svc.svc_id%TYPE;
   v_parm_nm          parm.parm_nm%TYPE;
   v_db_col_nm        parm.db_col_nm%TYPE;
   v_data_typ_nm      parm.data_typ_nm%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
BEGIN
   --- Get the service id
   SELECT svc_id
     INTO v_svcid
     FROM sub_svc
    WHERE sub_svc_id = p_subsvcid;

   --- load sub-typed parm definition
   SELECT svc_parm.parm_nm,svc_parm.data_typ_nm, svc_parm.is_reserved, svc_parm.db_col_nm, svc_parm.dflt_val
     INTO v_parm_nm, v_data_typ_nm, v_is_reserved, v_db_col_nm,v_dflt_val
     FROM parm svc_parm,parm base_parm
    WHERE svc_parm.object_id=v_svcid and svc_parm.class_id=100 and svc_parm.parm_nm=base_parm.parm_nm and base_parm.parm_id=p_parm_id;


   --- If it is reserved and this is a status change then
   --- skip it, it should be done in state machine
   IF v_parm_nm != 'provision_status'
   THEN
       IF  LOWER (v_is_reserved) = 'y'
       THEN
          if (v_db_col_nm IS NOT NULL)
          then
            sp_subsvc_reserved_update (p_subsvcid, v_db_col_nm, p_parmval);
          else
            if p_parmval IS NOT NULL
            then
                 raise_application_error(-20001, 'not db_col_nm defined for reserved parm:'||v_parm_nm);
            end if;
          end if;

       ELSE ---- this is a parameter change
          -- update the sub_parm table
          IF (p_parmval IS NULL and v_dflt_val is null or p_parmval = v_dflt_val)
          THEN
             DELETE FROM sub_svc_parm
                WHERE sub_svc_id = p_subsvcid AND parm_id = p_parm_id;
          ELSE
       update sub_svc_parm set val=p_parmval where
         sub_svc_id=p_subsvcid and parm_id=p_parm_id;
       if SQL%ROWCOUNT=0 then
         insert into sub_svc_parm(sub_svc_id, parm_id, val)
                  VALUES (p_subsvcid, p_parm_id, p_parmval);
    end if;
       END IF;
     END IF;
   end if;
END sp_subsvc_update;
/
-- End of DDL Script for Procedure SP_SUBSVC_UPDATE


-- Start of DDL Script for Procedure SP_UPDATE_SAMP_ACCESS_PARM
CREATE OR REPLACE
PROCEDURE sp_update_samp_access_parm (
   p_userid   IN       sub_svc_parm.val%TYPE,
   p_parm_nm  IN       parm.parm_nm%TYPE,
   p_parm_val IN       sub_svc_parm.val%TYPE
)
IS

-- Purpose: Update the access service parameter based on user id and
--          parameter name to the specified value.
--
-- MODIFICATION HISTORY
-- Person      Date           Comments
-- ----------  -------------  -------------------------------------------
-- glennm      June 12, 2002  Original version of procedure created.

   v_unique_id    sub_svc_parm.val%TYPE     := 'email_login';
   v_svc_nm       svc.svc_nm%TYPE           := 'samp_access';
   v_parm_id      parm.parm_id%TYPE;
   v_sub_svc_id   sub_svc.sub_svc_id%TYPE;
BEGIN
   --- Based on the user id get the sub_svc_id.
   v_parm_id :=
        get_parm_id (v_unique_id, get_class_id ('Svc'), get_svcid (v_svc_nm));

   SELECT sub_svc_id
     INTO v_sub_svc_id
     FROM sub_svc_parm
    WHERE parm_id = v_parm_id AND val = p_userid;

   --- Get the parameter id for the service.
   v_parm_id :=
             get_parm_id (p_parm_nm, get_class_id ('Svc'), get_svcid (v_svc_nm));

   --- Update the access parm value.
   UPDATE sub_svc_parm
      SET val = p_parm_val
   WHERE sub_svc_id = v_sub_svc_id
     AND parm_id = v_parm_id;

END sp_update_samp_access_parm;
/
-- End of DDL Script for Procedure SP_UPDATE_SAMP_ACCESS_PARM



CREATE OR REPLACE PROCEDURE SP_GET_NEXT_KEY_VAL (i_key_nm IN varchar2,
                            o_next_val OUT number) is
    source_cursor integer;
    rows_processed integer;
    next_val number;
BEGIN
    source_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(source_cursor,
                   'select ' || i_key_nm || '.nextval from dual',
                   dbms_sql.native);
    dbms_sql.define_column(source_cursor, 1, next_val);
    rows_processed := dbms_sql.execute(source_cursor);
    rows_processed := dbms_sql.fetch_rows(source_cursor);
    dbms_sql.column_value(source_cursor, 1, next_val);
    dbms_sql.close_cursor(source_cursor);
    o_next_val := next_val;
EXCEPTION
    when others then
        if dbms_sql.is_open(source_cursor) then
          dbms_sql.close_cursor(source_cursor);
        end if;
        raise_application_error(-20008, '"Failed to get next value of sequence "'||i_key_nm||
                                        '", sql error code is "'||SQLCODE||'", messsage is "'||
                                        SQLERRM||'"');
END;
/



CREATE OR REPLACE PROCEDURE SP_GET_NEXT_N_KEY_VAL (i_key_nm IN varchar2,
                            i_count IN number,
                            o_next_val OUT SP_ARR_NUMBER_50) is
---------------------------------------------------------------------------
--  returns the next 'i_count' values from the sequence 'i_key_nm'
--  and returns it in the array 'o_next_val'
--  bounds checking is not done by the proc, client should ensure that
--  not more than 50 values are requested
---------------------------------------------------------------------------
    source_cursor integer;
    rows_processed integer;
    end_next_val number;
    key_ind number;
    seq_incr_by number;
    fetch_count number;
    next_val number;
BEGIN
    o_next_val := SP_ARR_NUMBER_50();
    o_next_val.extend(i_count);
    source_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(source_cursor,
                   'select ' || i_key_nm || '.nextval from dual',
                   dbms_sql.native);
    dbms_sql.define_column(source_cursor, 1, end_next_val);
    select increment_by into seq_incr_by
           from sys.user_sequences where SEQUENCE_NAME = upper(i_key_nm); --get the incr for the seq

    fetch_count := ceil(i_count/seq_incr_by); --there must be at least one fetch
    key_ind := 1;  --pl-sql arrays index from 1 (sic)

    WHILE (fetch_count > 0  AND key_ind <= i_count) LOOP
        rows_processed := dbms_sql.execute(source_cursor); --get the next val of the sequence
        rows_processed := dbms_sql.fetch_rows(source_cursor);
        dbms_sql.column_value(source_cursor, 1, end_next_val); --into end_next_val

        next_val := end_next_val - seq_incr_by + 1; --the start val for the current sequence range

        WHILE (next_val <= end_next_val AND key_ind <= i_count) LOOP
            o_next_val(key_ind) := next_val;
            next_val := next_val + 1;
            key_ind := key_ind + 1;
        END LOOP;

        fetch_count := fetch_count - 1;
    END LOOP;
    dbms_sql.close_cursor(source_cursor);

EXCEPTION
    when others then
        if dbms_sql.is_open(source_cursor) then
          dbms_sql.close_cursor(source_cursor);
        end if;
        raise;
END;
/


-- Start of DDL Script for procedure FN_SUBSVC_RESERVED_UPDATE
CREATE OR REPLACE PROCEDURE sp_subsvc_reserved_update
 (
   p_subsvc_id   sub_svc.sub_svc_id%TYPE,
   p_dbcol       parm.db_col_nm%TYPE,
   p_dbval       ordr_item_entity_chg.val%TYPE
)
IS
   v_sql VARCHAR2 (1999);
   v_parm_val ordr_item_entity_chg.val%TYPE;
   v_data_type user_tab_columns.data_type%TYPE;
BEGIN
   IF UPPER (p_dbcol) = 'SUB_ADDR_ID'
   THEN
      UPDATE sub_svc_addr
         SET sub_addr_id = TO_NUMBER (p_dbval)
       WHERE sub_svc_id = p_subsvc_id;
   ELSE
      SELECT user_tab_columns.data_type
        INTO v_data_type
        FROM sys.user_tab_columns
       WHERE user_tab_columns.table_name = 'SUB_SVC'
         AND user_tab_columns.column_name = UPPER (p_dbcol);

      IF v_data_type = 'DATE'  THEN
         v_parm_val := To_date(Substr(p_dbval, 1, 8), 'YYYYMMDD');
      ELSE
         v_parm_val := p_dbval;
      END IF;

      v_sql :=    'update sub_svc set '|| p_dbcol || '= :val  where sub_svc_id = :id';

      EXECUTE IMMEDIATE v_sql using v_parm_val, p_subsvc_id;
   END IF;
END sp_subsvc_reserved_update;
/
-- End of DDL Script for procedure FN_SUBSVC_RESERVED_UPDATE


-- Start of DDL Script for procedure SP_SUB_RESERVED_UPDATE
CREATE OR REPLACE
PROCEDURE sp_sub_reserved_update (
   p_sub_id   sub.sub_id%TYPE,
   p_dbcol    parm.db_col_nm%TYPE,
   p_dbval    ordr_item_entity_chg.val%TYPE
)
IS
   v_sql         VARCHAR2 (1999);
   v_parm_val    ordr_item_entity_chg.val%TYPE;
   v_data_type   user_tab_columns.data_type%TYPE;
BEGIN
   SELECT user_tab_columns.data_type
     INTO v_data_type
     FROM sys.user_tab_columns
    WHERE user_tab_columns.table_name = 'SUB'
      AND user_tab_columns.column_name = UPPER (p_dbcol);

   IF v_data_type = 'DATE'  THEN
         v_parm_val := To_date(Substr(p_dbval, 1, 8), 'YYYYMMDD');
   ELSE
         v_parm_val := p_dbval;
   END IF;

   v_sql :=    'update sub set ' || p_dbcol || '=:val where sub_id = :id';
   EXECUTE IMMEDIATE v_sql using v_parm_val,p_sub_id;
END sp_sub_reserved_update;
/
-- End of DDL Script for procedure SP_SUB_RESERVED_UPDATE

-- Start of DDL Script for Function SP_CONTACT_RESERVED_UPDATE
CREATE OR REPLACE PROCEDURE sp_contact_reserved_update (
   p_sub_contact_id   IN   sub_contact.sub_contact_id%TYPE,
   p_dbcol                 parm.db_col_nm%TYPE,
   p_dbval                 ordr_item_entity_chg.val%TYPE
)
IS
   v_sql            VARCHAR2 (1999);
   v_parm_val       ordr_item_entity_chg.val%TYPE;
   v_data_type      user_tab_columns.data_type%TYPE;
BEGIN
   SELECT user_tab_columns.data_type
     INTO v_data_type
     FROM sys.user_tab_columns
    WHERE user_tab_columns.table_name = 'SUB_CONTACT'
      AND user_tab_columns.column_name = UPPER (p_dbcol);

   if (lower(p_dbcol)='location_id') then
     v_parm_val := to_char(fn_get_location_id (to_number(p_dbval)));
   else
     IF v_data_type = 'DATE'  THEN
          v_parm_val := To_date(Substr(p_dbval, 1, 8), 'YYYYMMDD');
     ELSE
         v_parm_val := p_dbval;
     END IF;
   END IF;

   v_sql :=    'update sub_contact set '
             || p_dbcol
             || '=:val where sub_contact_id = :id';
   EXECUTE IMMEDIATE v_sql using v_parm_val,p_sub_contact_id;
END sp_contact_reserved_update;
/
-- End of DDL Script for Function SP_CONTACT_RESERVED_UPDATE

-- Start of DDL Script for Function SP_SUBADDR_RESERVED_UPDATE
CREATE OR REPLACE PROCEDURE
sp_subaddr_reserved_update (
   p_addr_id   sub_addr.sub_addr_id%TYPE,
   p_dbcol     parm.db_col_nm%TYPE,
   p_dbval     ordr_item_entity_chg.val%TYPE
)
IS
   v_sql            VARCHAR2 (1999);
   v_parm_val       ordr_item_entity_chg.val%TYPE;
   v_data_type      user_tab_columns.data_type%TYPE;
BEGIN
   SELECT user_tab_columns.data_type
     INTO v_data_type
     FROM sys.user_tab_columns
    WHERE user_tab_columns.table_name = 'SUB_ADDR'
      AND user_tab_columns.column_name = UPPER (p_dbcol);

   IF v_data_type = 'DATE'  THEN
         v_parm_val := To_date(Substr(p_dbval, 1, 8), 'YYYYMMDD');
    ELSE
         v_parm_val := p_dbval;
   END IF;

   v_sql :=    'update sub_addr set '
             || p_dbcol
             || '=:val where sub_addr_id = :id';
   EXECUTE IMMEDIATE v_sql using v_parm_val,p_addr_id;
END sp_subaddr_reserved_update;
/
-- End of DDL Script for Function SP_SUBADDR_RESERVED_UPDATE

CREATE OR REPLACE PROCEDURE sp_entity_parm_chg_update (
    p_ordr_item_id IN number,
  p_entity_id in number,
  p_parm_id in number,
    p_var in varchar2,
  p_old_val in varchar2,
  p_need_profile_change in char,
  p_user in varchar2)
IS
  v_ordr_item_id ORDR_ITEM_ENTITY_CHG.sub_ordr_item_id%TYPE;
BEGIN
  begin
    select chg.sub_ordr_item_id into v_ordr_item_id from
       ORDR_ITEM_ENTITY_CHG chg, sub_ordr_item item where
     chg.sub_ordr_item_id=item.sub_ordr_item_id
       and chg.sub_entity_id=p_entity_id AND chg.parm_id = p_parm_id
     and item.sub_ordr_id=
      (select sub_ordr_id from sub_ordr_item where sub_ordr_item_id=p_ordr_item_id);
    EXCEPTION
      WHEN NO_DATA_FOUND
   THEN null;
  end;
  if (v_ordr_item_id is null) then
     insert into ORDR_ITEM_ENTITY_CHG(sub_ordr_item_id,sub_entity_id, parm_id, val,old_val,NEED_UPD,created_by, created_dtm)
         values (p_ordr_item_id,p_entity_id,p_parm_id,p_var,p_old_val,p_need_profile_change,p_user,sysdate);
  else
     update ordr_item_entity_chg set val =p_var, NEED_UPD=p_need_profile_change where
        sub_ordr_item_id=v_ordr_item_id and parm_id=p_parm_id;
  end if;
END sp_entity_parm_chg_update;
/

CREATE OR REPLACE PROCEDURE sp_entity_parm_chg_upd_lt (
    p_ordr_item_id IN number,
    p_entity_id in number,
    p_parm_id in number,
    p_var in varchar2,
    p_old_val in varchar2,
    p_need_profile_change in char,
    p_user in varchar2,
    p_longtext_var in clob,
    p_longtext_old_val in clob)
IS
  v_ordr_item_id ORDR_ITEM_ENTITY_CHG.sub_ordr_item_id%TYPE;
BEGIN
  begin
    select chg.sub_ordr_item_id into v_ordr_item_id from
       ORDR_ITEM_ENTITY_CHG chg, sub_ordr_item item where
       chg.sub_ordr_item_id=item.sub_ordr_item_id
       and chg.sub_entity_id=p_entity_id AND chg.parm_id = p_parm_id
       and item.sub_ordr_id=
        (select sub_ordr_id from sub_ordr_item where sub_ordr_item_id=p_ordr_item_id);
    EXCEPTION
      WHEN NO_DATA_FOUND
   THEN null;
  end;
  if (v_ordr_item_id is null) then
     insert into ORDR_ITEM_ENTITY_CHG(sub_ordr_item_id,sub_entity_id, parm_id, val, old_val, longtext_val,longtext_old_val,NEED_UPD,created_by, created_dtm)
         values (p_ordr_item_id,p_entity_id,p_parm_id,p_var,p_old_val, p_longtext_var, p_longtext_old_val, p_need_profile_change,p_user,sysdate);
  else
     if(p_longtext_var is not null) then
         update ordr_item_entity_chg set longtext_val =p_longtext_var, NEED_UPD=p_need_profile_change where
            sub_ordr_item_id=v_ordr_item_id and parm_id=p_parm_id;
     else
         update ordr_item_entity_chg set val =p_var, NEED_UPD=p_need_profile_change where
            sub_ordr_item_id=v_ordr_item_id and parm_id=p_parm_id;
     end if;
  end if;
END sp_entity_parm_chg_upd_lt;
/

CREATE OR REPLACE PROCEDURE sp_flat_subsvc_update (
      p_subsvcid              IN   sub_svc.sub_svc_id%TYPE,
      p_tblColNm		   IN   sub_svc_parm.val%TYPE,
      p_parmval               IN   sub_svc_parm.val%TYPE,
      p_tblNm               IN   sub_svc_parm.val%TYPE
      )
   IS
      v_sql					varchar(1000);
   BEGIN

    v_sql := 'UPDATE ' ||
                        p_tblNm ||
                    ' SET '|| p_tblColNm || ' = :val WHERE sub_svc_id = :id';

   EXECUTE IMMEDIATE v_sql using p_parmval,p_subsvcid;

END sp_flat_subsvc_update;
/

CREATE OR REPLACE PROCEDURE sp_reg_unique_parm (
   p_name          IN 		wrk_unique_parm.grp_nm_or_parm_id%TYPE,
   p_parm_id       IN       wrk_unique_parm.parm_id%TYPE,
   p_val		   IN       wrk_unique_parm.val%TYPE,
   p_sub_entity_id IN       wrk_unique_parm.sub_entity_id%TYPE,
   p_sub_ordr_id   IN       wrk_unique_parm.sub_ordr_id%TYPE

)
IS
BEGIN
   update wrk_unique_parm set val=p_val where parm_id=p_parm_id and sub_entity_id=p_sub_entity_id and sub_ordr_id=p_sub_ordr_id;
   --not such record
   if SQL%ROWCOUNT=0 then
      insert into wrk_unique_parm(grp_nm_or_parm_id,parm_id,val,sub_entity_Id,sub_ordr_id)
      values(p_name,p_parm_id,p_val,p_sub_entity_id,p_sub_ordr_id);
   end if;
END sp_reg_unique_parm;
/

CREATE OR REPLACE PROCEDURE sp_subsvc_update_ext (
   p_subsvcid              IN   sub_svc.sub_svc_id%TYPE,
   p_parm_id               IN   parm.parm_id%TYPE,
   p_parmval               IN   varchar2,
   p_user                  IN   sub_svc.modified_by%TYPE,
   p_table_nm              IN   varchar2
)
IS
   v_svcid            svc.svc_id%TYPE;
   v_parm_nm          parm.parm_nm%TYPE;
   v_db_col_nm        parm.db_col_nm%TYPE;
   v_data_typ_nm      parm.data_typ_nm%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
   v_sql              varchar2(2000);
BEGIN
   --- Get the service id
   SELECT svc_id
     INTO v_svcid
     FROM sub_svc
    WHERE sub_svc_id = p_subsvcid;

   --- load sub-typed parm definition
   SELECT svc_parm.parm_nm,svc_parm.data_typ_nm, svc_parm.is_reserved, svc_parm.db_col_nm, svc_parm.dflt_val
     INTO v_parm_nm, v_data_typ_nm, v_is_reserved, v_db_col_nm,v_dflt_val
     FROM parm svc_parm,parm base_parm
    WHERE svc_parm.object_id=v_svcid and svc_parm.class_id=100 and svc_parm.parm_nm=base_parm.parm_nm and base_parm.parm_id=p_parm_id;


   --- If it is reserved and this is a status change then
   --- skip it, it should be done in state machine
   IF v_parm_nm != 'provision_status'
   THEN
       IF  LOWER (v_is_reserved) = 'y'
       THEN
          if (v_db_col_nm IS NOT NULL)
          then
            sp_subsvc_reserved_update (p_subsvcid, v_db_col_nm, p_parmval);
          else
            if p_parmval IS NOT NULL
            then
                 raise_application_error(-20001, 'not db_col_nm defined for reserved parm:'||v_parm_nm);
            end if;
          end if;

       ELSE ---- this is a parameter change
          -- update the sub_parm table
          IF (p_parmval IS NULL and v_dflt_val is null or p_parmval = v_dflt_val)
          THEN
         v_sql := 'DELETE FROM '||p_table_nm||' WHERE sub_svc_id = :p_subsvcid AND parm_id = :p_parm_id';
         execute  IMMEDIATE v_sql using p_subsvcid,p_parm_id;
          ELSE
         v_sql := 'update '||p_table_nm||' set val= :p_parmval where sub_svc_id=:p_subsvcid and parm_id=:p_parm_id';
             execute  IMMEDIATE v_sql using p_parmval,p_subsvcid,p_parm_id;

         if SQL%ROWCOUNT=0 then
           v_sql := 'insert into '||p_table_nm||'(sub_svc_id, parm_id, val) VALUES (:p_subsvcid, :p_parm_id, :p_parmval)';
                       execute immediate v_sql using p_subsvcid,p_parm_id,p_parmval;
               end if;
        END IF;
      END IF;
   end if;
END sp_subsvc_update_ext;
/
create or replace procedure prc_lock_object(p_object_type in varchar2 ,p_sub_ext_key in varchar2)
is
   unique_error EXCEPTION;
   v_val wrk_lock.val%TYPE;
   PRAGMA EXCEPTION_INIT(unique_error, -1);
begin
  select val into v_val from wrk_lock where lock_nm=p_object_type and val=p_sub_ext_key for update;
  EXCEPTION
    when no_data_found then
    begin
      insert into wrk_lock(lock_nm, val) values(p_object_type,p_sub_ext_key);
      EXCEPTION
        when unique_error then
        select val into v_val from wrk_lock where lock_nm=p_object_type and val=p_sub_ext_key for update;
    end;
end;
/
CREATE OR REPLACE PROCEDURE sp_order_item_update (
   p_order_item_id              IN   sub_ordr_item.sub_ordr_item_id%TYPE,
   p_parm_id               IN   parm.parm_id%TYPE,
   p_parm_val               IN   sub_ordr_parm.val%TYPE,
   p_user                  IN   sub_svc.modified_by%TYPE
)
IS
   v_parm_nm          parm.parm_nm%TYPE;
   v_parm_val         sub_ordr_parm.val%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
   v_err_reason       sub_ordr_parm.VAL%TYPE;
BEGIN

   --- load sub-typed parm definition
   SELECT parm_nm,is_reserved, dflt_val
     INTO v_parm_nm,v_is_reserved,v_dflt_val
     FROM parm
    WHERE class_id=507 and parm_id=p_parm_id;

   IF  LOWER (v_is_reserved) = 'y' then
     return;
   END IF;


   --- If it is reserved and this is a status change then
   --- skip it, it should be done in state machine

   IF v_parm_nm = 'err_reason'
   THEN
     begin
       select val into v_err_reason from sub_ordr_item_parm where sub_ordr_item_id=p_order_item_id and parm_id=p_parm_id;
     if (INSTR(v_err_reason,p_parm_val)>0) then
       return;  --reason already exists in exist value
     else
       v_parm_val :=substr(v_err_reason ||'|'|| p_parm_val,0,1999);
     end if;
     exception
      WHEN NO_DATA_FOUND THEN
              v_parm_val := substr(p_parm_val,0,1999);
   end;
   else
     v_parm_val :=p_parm_val;
   end if;

   IF  v_parm_val IS NULL and v_dflt_val is null or v_parm_val = v_dflt_val
   THEN
      delete sub_ordr_item_parm where sub_ordr_item_id=p_order_item_id and parm_id=p_parm_id;
   else
      update sub_ordr_item_parm set val=v_parm_val,modified_dtm=sysdate,modified_by=p_user where sub_ordr_item_id=p_order_item_id and parm_id=p_parm_id;
    if SQL%ROWCOUNT=0 then
         insert into sub_ordr_item_parm(sub_ordr_item_id, parm_id, val,created_by,created_dtm)
                  VALUES (p_order_item_id, p_parm_id, v_parm_val,p_user,sysdate);
    end if;
   end if;
END sp_order_item_update;
/

CREATE OR REPLACE PROCEDURE sp_order_update (
   p_order_id              IN   sub_ordr.sub_ordr_id%TYPE,
   p_parm_id               IN   parm.parm_id%TYPE,
   p_parm_val              IN   sub_ordr_parm.val%TYPE,
   p_parm_longtextval      IN   CLOB,
   p_user                  IN   sub_svc.modified_by%TYPE
)
IS
   v_parm_nm          parm.parm_nm%TYPE;
   v_parm_val         sub_ordr_parm.val%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
   v_err_reason       sub_ordr_parm.VAL%TYPE;
   v_data_typ         PARM.DATA_TYP_NM%TYPE;
BEGIN

   --- load sub-typed parm definition
   SELECT parm_nm,is_reserved, dflt_val, data_typ_nm
     INTO v_parm_nm,v_is_reserved,v_dflt_val, v_data_typ
     FROM parm
    WHERE class_id=324 and parm_id=p_parm_id;

   if LOWER (v_is_reserved) = 'y' then
     return;
   end if;

   --- If it is reserved and this is a status change then
   --- skip it, it should be done in state machine

   IF v_parm_nm = 'err_reason'
   THEN
     begin
       select val into v_err_reason from sub_ordr_parm where sub_ordr_id=p_order_id and parm_id=p_parm_id;
       if (INSTR(v_err_reason,p_parm_val)>0) then
         return;  --reason already exists in exist value
       else
         v_parm_val :=substr(v_err_reason ||'|'|| p_parm_val,0,1999);
       end if;
       exception
         WHEN NO_DATA_FOUND THEN
              v_parm_val := substr(p_parm_val,0,1999);
     end;
   ELSE
     v_parm_val :=p_parm_val;
   end if;

   IF v_data_typ ='LongText' 
   THEN
       IF  p_parm_longtextval IS NULL
       THEN
           delete sub_ordr_parm where sub_ordr_id=p_order_id and parm_id=p_parm_id;
       else
           update sub_ordr_parm set modified_dtm=sysdate,modified_by=p_user,val=null, longtext_val=p_parm_longtextval where sub_ordr_id=p_order_id and parm_id=p_parm_id;
           if SQL%ROWCOUNT=0 then
             insert into sub_ordr_parm(sub_ordr_id, parm_id, val,longtext_val,created_by,created_dtm)
                  VALUES (p_order_id, p_parm_id, null,p_parm_longtextval,p_user,sysdate);
           end if;
       end if;
   ELSE
       IF  v_parm_val IS NULL and v_dflt_val is null or v_parm_val = v_dflt_val
       THEN
           delete sub_ordr_parm where sub_ordr_id=p_order_id and parm_id=p_parm_id;
       else
           update sub_ordr_parm set modified_dtm=sysdate,modified_by=p_user,val=v_parm_val where sub_ordr_id=p_order_id and parm_id=p_parm_id;
           if SQL%ROWCOUNT=0 then
             insert into sub_ordr_parm(sub_ordr_id, parm_id, val,created_by,created_dtm)
                  VALUES (p_order_id, p_parm_id, v_parm_val,p_user,sysdate);
           end if;
       end if;
   END IF;
END sp_order_update;
/

CREATE OR REPLACE PROCEDURE sp_subentity_reserved_update (
   p_entity_id number,
   p_table_nm  user_tab_columns.table_name%TYPE,
   p_entity_id_col_nm user_tab_columns.column_name%TYPE,
   p_dbcol     parm.db_col_nm%TYPE,
   p_dbval     ordr_item_entity_chg.val%TYPE
)
IS
   v_sql            VARCHAR2 (2000);
   v_parm_val       ordr_item_entity_chg.val%TYPE;
   v_data_type      user_tab_columns.data_type%TYPE;
BEGIN
   SELECT user_tab_columns.data_type
     INTO v_data_type
     FROM sys.user_tab_columns
    WHERE user_tab_columns.table_name = p_table_nm
      AND user_tab_columns.column_name = UPPER (p_dbcol);

   IF v_data_type = 'DATE'  THEN
         v_parm_val := To_date(Substr(p_dbval, 1, 8), 'YYYYMMDD');
    ELSE
         v_parm_val := p_dbval;
   END IF;

   v_sql :=    'update ' || p_table_nm || ' set '
             || p_dbcol
             || '=:val where '||p_entity_id_col_nm||' = :id';
   EXECUTE IMMEDIATE v_sql using v_parm_val,p_entity_id;
END sp_subentity_reserved_update;
/

CREATE OR REPLACE PROCEDURE sp_subentity_update (
   p_subentity_id          IN   number,
   p_entity_table_nm       IN   user_tab_columns.table_name%TYPE,
   p_parm_table_nm         IN   user_tab_columns.table_name%TYPE,
   p_entity_id_col_nm      IN   user_tab_columns.column_name%TYPE,
   p_parm_id               IN   parm.parm_id%TYPE,
   p_parm_val               IN   sub_user_parm.val%TYPE,
   p_user                  IN   sub_user.modified_by%TYPE
)
IS
   v_parm_nm          parm.parm_nm%TYPE;
   v_db_col_nm        parm.db_col_nm%TYPE;
   v_dflt_val         parm.dflt_val%TYPE;
   v_is_reserved      parm.is_reserved%TYPE;
   v_sql              VARCHAR2 (2000);
BEGIN

   --- load sub-typed parm definition
   SELECT parm_nm,is_reserved,db_col_nm,dflt_val
     INTO v_parm_nm,v_is_reserved, v_db_col_nm,v_dflt_val
     FROM parm
    WHERE parm_id=p_parm_id;


   --- If it is reserved and this is a status change then
   --- skip it, it should be done in state machine
   IF v_parm_nm != 'status'
   THEN
       IF  LOWER (v_is_reserved) = 'y'
       THEN
          if (v_db_col_nm IS NOT NULL)
          then
            sp_subentity_reserved_update (p_subentity_id,p_entity_table_nm,p_entity_id_col_nm,
             v_db_col_nm, p_parm_val);
          else
            raise_application_error(-20001, 'not db_col_nm defined for reserved parm:'||v_parm_nm);
          end if;

       ELSE ---- this is a parameter change
          -- update the sub_parm table
          IF (p_parm_val IS NULL and v_dflt_val is null or p_parm_val = v_dflt_val)
          THEN
             v_sql :=    'delete ' || p_parm_table_nm || ' where parm_id=:parm_id and '
                        || p_entity_id_col_nm||' = :entity_id';
             EXECUTE IMMEDIATE v_sql using p_parm_id,p_subentity_id;
          ELSE
             v_sql :=    'update ' || p_parm_table_nm ||
                         ' set val=:parm_val where parm_id=:parm_id and '
                        || p_entity_id_col_nm||' = :entity_id';
             EXECUTE IMMEDIATE v_sql using p_parm_val,p_parm_id,p_subentity_id;
         if SQL%ROWCOUNT=0 then
             v_sql :=    'insert into ' || p_parm_table_nm ||
                         ' ('||p_entity_id_col_nm||',parm_id,val) values (:entity_id,:parm_id,:val)';
             EXECUTE IMMEDIATE v_sql using p_subentity_id,p_parm_id,p_parm_val;
           end if;
       END IF;
     END IF;
   end if;
END sp_subentity_update;
/

--SUBMGR LARGE SUB IMPLEMENTATION PROCEDURES
CREATE OR REPLACE procedure PRC_GET_SERVICES_SELECTIVE (v_svc_id IN NUMBER,v_sub_id in NUMBER,svc_array IN OUT ID_ARRAY) IS
--get all the children of sub_svc_id for a given subscriber
cursor child_svc_list is select sub.sub_svc_id from sub_svc sub, svc s
where sub.parent_sub_svc_id = v_svc_id and sub.svc_id = s.svc_id and sub.sub_id = v_sub_id
and s.loading_policy='always' and sub.sub_svc_status_id != 29;
BEGIN
    ---DBMS_OUTPUT.put_line('Before SVC : ' || v_svc_id);
    svc_array.extend;
    svc_array(svc_array.count) := v_svc_id;
    FOR child_svc in child_svc_list
    LOOP
        --DBMS_OUTPUT.put_line('Child SVC : ' || child_svc.sub_svc_id);
        PRC_GET_SERVICES_SELECTIVE(child_svc.sub_svc_id,v_sub_id, svc_array);
    END LOOP;
END;
/

CREATE OR REPLACE procedure PRC_GET_SLC_SVCS_BY_SUB_ID (v_sub_id IN NUMBER, svc_data OUT ID_ARRAY) IS
-- GET THE TOP LEVEL SERVICES FOR THE GIVEN SUBSCRIBER
cursor top_level_sub is
select sub_svc_id from sub_svc,svc where sub_id = v_sub_id and parent_sub_svc_id is null
and sub_svc_status_id != 29 and sub_svc.svc_id=svc.svc_id and svc.loading_policy='always';
--for each top level services get the the children upto selective service, stop at selective service
BEGIN
    svc_data := ID_ARRAY();
    FOR top_svc in top_level_sub
    LOOP
      --DBMS_OUTPUT.put_line('Top Level SVC : ' || top_svc.sub_svc_id);
      --call the procedure to get the children selective services
      PRC_GET_SERVICES_SELECTIVE(top_svc.sub_svc_id,v_sub_id, svc_data);
    END LOOP;
END;
/

CREATE OR REPLACE procedure PRC_LOAD_SVC_SELECTIVE
(svc_id_list in ID_ARRAY, svc_dsts OUT ID_ARRAY)
IS
   v_svc_id            SUB_SVC.SUB_SVC_ID%type;
   v_top_svc_id        sub_svc.parent_sub_svc_id%type;
   q_selective         SVC.LOADING_POLICY%type;
   q_parentsvcid       sub_svc.parent_sub_svc_id%type;
   q_sub_id            SUB_SVC.sub_id%type;
begin
  svc_dsts := ID_ARRAY();
  for i in 1..svc_id_list.count
    loop
      v_svc_id := svc_id_list(i);
      v_top_svc_id := null;
      while (v_top_svc_id is null)
      loop
           select  loading_policy, parentsvcid,subid
             INTO  q_selective, q_parentsvcid,q_sub_id
           from v_sp_subsvcmodel where subsvcid = v_svc_id;
          --DBMS_OUTPUT.put_line('PARENT_SUB_SVC_ID = ' || q_parentsvcid);
          if q_parentsvcid is null or q_selective = 'selective' then
               v_top_svc_id := v_svc_id;
             else
               v_svc_id := q_parentsvcid;
          end if;
      end loop;
      PRC_GET_SERVICES_SELECTIVE(v_top_svc_id,q_sub_id, svc_dsts);
    end loop;
end PRC_LOAD_SVC_SELECTIVE;
/