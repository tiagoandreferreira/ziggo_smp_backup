--============================================================================
--    $Id: sp_plsql_views.sql,v 1.90.6.2.2.1.2.2.2.11 2012/02/24 15:33:40 pankajgu Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

-- Start of DDL Script for View V_STATE
CREATE OR REPLACE FORCE VIEW v_state (
   state_name,
   class_name,
   state_type )
AS
SELECT STATUS,class_nm,STATUS_TYP  from ref_status, ref_class where ref_status.CLASS_ID=ref_class.CLASS_ID
/
-- End of DDL Script for View V_STATE

-- Start of DDL Script for View V_STATEACTION
CREATE OR REPLACE FORCE VIEW v_stateaction (
   action_name, class_name, action_type)
AS
   SELECT state_action.action, class_nm, 'input'
     FROM ref_class, state_action
    WHERE ref_class.class_id = state_action.class_id
      AND state_action.action NOT IN
                                    (SELECT ref_event.event_nm
                                       FROM ref_event)
   UNION
   SELECT state_action.action, class_nm, 'event'
     FROM ref_class, state_action
    WHERE ref_class.class_id = state_action.class_id
      AND state_action.action IN (SELECT ref_event.event_nm
                                    FROM ref_event)
   UNION
   SELECT state_transition_event.event_nm, class_nm, 'special'
     FROM state_transition_event, ref_class
    WHERE ref_class.class_id = state_transition_event.target_class_id
      AND    event_nm
          || target_class_id NOT IN
                              (SELECT    action
                                      || class_id
                                 FROM state_action)
/
-- End of DDL Script for View V_STATEACTION

-- Start of DDL Script for View V_STATECLASS
CREATE OR REPLACE FORCE VIEW v_stateclass (class_name)
AS
   SELECT UNIQUE class_name
            FROM v_stateaction
/
-- End of DDL Script for View V_STATECLASS

-- Start of DDL Script for View V_STATEEVENT
CREATE OR REPLACE FORCE VIEW v_stateevent (
   event_name,
   transition_id,
   event_target
)
AS
   SELECT   event_nm,
            state_transition_id,
            class_nm
       FROM state_transition_event, ref_class
      WHERE ref_class.class_id = state_transition_event.target_class_id
   ORDER BY state_transition_id, event_seq
/
-- End of DDL Script for View V_STATEEVENT

-- Start of DDL Script for View V_STATETRANSITION
CREATE OR REPLACE FORCE VIEW v_statetransition (
   transition_id,
   from_state_name,
   to_state_name,
   action_name,
   condition,
   class_name )
AS
SELECT state_transition_id, a.status, b.status, action, cond, class_nm
     FROM ref_class, state_action, ref_status a, ref_status b, state_transition
    WHERE state_transition.state_action_id = state_action.state_action_id
      AND ref_class.class_id = state_action.class_id
      AND state_transition.from_status_id = a.status_id
      AND state_transition.to_status_id = b.status_id
/
-- End of DDL Script for View V_STATETRANSITION


-- Start of DDL Script for View V_SUBSVC_BRIEF
CREATE OR REPLACE FORCE VIEW v_subsvc_brief (
   sub_id,
   sub_ext_key,
   sub_svc_id,
   sub_svc_ext_key,
   svc_id,
   svc_nm,
   status_id,
   status )
AS
SELECT sub_svc.sub_id sub_id,
          fn_get_sub_ext_key (sub_svc.sub_id) sub_ext_key,
          sub_svc.sub_svc_id sub_svc_id,
          sub_svc.external_key,
          sub_svc.svc_id svc_id, svc.svc_nm svc_nm,
          sub_svc.sub_svc_status_id status_id,
          fn_get_status_nm ( 'SubSvcSpec', sub_svc.sub_svc_status_id) status
     FROM sub_svc, svc
    WHERE (sub_svc.svc_id = svc.svc_id)
/
-- End of DDL Script for View V_SUBSVC_BRIEF



-- Start of DDL Script for View V_SUB_ORDR

CREATE OR REPLACE FORCE VIEW V_SUB_ORDR
(SUB_ID, SUB_ORDR_ID, ORDR_STATUS_ID, ORDR_STATUS_NM, ORDR_PREVIOUS_STATE_NM, CREATED_DTM, 
 CREATED_BY, MODIFIED_DTM, MODIFIED_BY, ORDR_TYP, ORDR_SRC, 
 EXTERNAL_KEY,VER, HAS_IMPACT)
AS 
SELECT sub_id, sub_ordr_id, ordr_status_id,
          fn_get_status_nm ('Order', ordr_status_id),
          fn_get_status_nm ('Order', ordr_previous_status_id),
          created_dtm, created_by,
          modified_dtm,MODIFIED_BY,ordr_typ,ordr_src_nm,
          external_key,samp_ver, has_impact 
     FROM sub_ordr;
/
-- End of DDL Script for View V_SUB_ORDR

-- Start of DDL Script for View V_SUB_ORDR_ITEMS
CREATE OR REPLACE FORCE VIEW V_SUB_ORDR_ITEMS
(SUB_ORDR_ID, SUB_ORDR_ITEM_ID, SUB_ENTITY_ID, OBJECT_TYP_ID, DISPLAY_INFO, 
 ORDR_ACTION_ID, ORDR_ACTION_NM, LINEITEM_STATUS_ID, ORDR_STATUS_NM, MODIFIED_BY, 
 MODIFIED_DTM, NOTIFY_SEQ_NUM, HAS_IMPACT)
AS 
SELECT sub_ordr_item.sub_ordr_id,sub_ordr_item.sub_ordr_item_id,
       sub_ordr_item.sub_entity_id,sub_ordr_item.object_typ_id,
          DECODE (
             sub_ordr_item.display_info,
             'unknown', '',
             sub_ordr_item.display_info
          ),
          ref_ordr_action.ordr_action_id, ref_ordr_action.ordr_action_nm,
          sub_ordr_item.ordr_item_status_id, ref_status.status,
          sub_ordr_item.modified_by, sub_ordr_item.modified_dtm,
          sub_ordr_item.notify_seq_num,
          sub_ordr_item.has_impact 
     FROM sub_ordr_item, ref_ordr_action,ref_status
WHERE ref_ordr_action.ordr_action_id = sub_ordr_item.ordr_action_id 
     and ref_status.status_id=sub_ordr_item.ordr_item_status_id;
/
-- End of DDL Script for View V_SUB_ORDR_ITEMS


-- Start of DDL Script for View V_SUB_ORDR_LINEITEM
CREATE OR REPLACE FORCE VIEW v_sub_ordr_lineitem (
   sub_ordr_id,
   sub_ordr_item_id,
   ordr_action_id,
   ordr_action_nm,
   lineitem_status_id,
   lineitem_status_nm,
   modification_comments )
AS
SELECT sub_ordr_item.sub_ordr_id,
       sub_ordr_item.sub_ordr_item_id,
       ref_ordr_action.ordr_action_id, ref_ordr_action.ordr_action_nm,
       sub_ordr_item.ordr_item_status_id,
       fn_get_status_nm ('OrderItem', sub_ordr_item.ordr_item_status_id), ''
  FROM sub_ordr_item, ref_ordr_action
 WHERE (ref_ordr_action.ordr_action_id = sub_ordr_item.ordr_action_id)
/
-- End of DDL Script for View V_SUB_ORDR_LINEITEM


-- Start of DDL Script for View V_SUB_SVC_DEPY
CREATE OR REPLACE FORCE VIEW v_sub_svc_depy (
   sub_svc_id,
   dependent_sub_svc_id,
   dependent_sub_svc_status,
   dependent_svc_id
)
AS
   (SELECT sub_svc_depy.prerequisite_sub_svc_id,
           sub_svc_depy.dependent_sub_svc_id,
           fn_get_status_nm ('SubSvcSpec', sub_svc.sub_svc_status_id), sub_svc.svc_id
      FROM sub_svc, sub_svc_depy
     WHERE (sub_svc_depy.dependent_sub_svc_id = sub_svc.sub_svc_id))
/
-- End of DDL Script for View V_SUB_SVC_DEPY

-- Start of DDL Script for View V_SUB_SVC_PREREQ
CREATE OR REPLACE FORCE VIEW V_SUB_SVC_PREREQ
(SUB_ID, SUB_SVC_ID, PREREQUISITE_SUB_SVC_ID, PREREQUISITE_SUB_SVC_STATUS, PREREQUISITE_SVC_ID)
AS 
(SELECT sub_svc.sub_id, sub_svc_depy.dependent_sub_svc_id,
           sub_svc_depy.prerequisite_sub_svc_id,
           fn_get_status_nm ('SubSvcSpec', sub_svc.sub_svc_status_id), sub_svc.svc_id
      FROM sub_svc, sub_svc_depy
     WHERE (sub_svc_depy.prerequisite_sub_svc_id = sub_svc.sub_svc_id));
/
-- End of DDL Script for View V_SUB_SVC_PREREQ


-- Start of DDL Script for View V_FULL_ADDRESS
CREATE OR REPLACE FORCE VIEW v_full_address (
   location_id,
   street_no,
   street_name,
   street_suffix,
   street_direction,
   apt_no,
   house_suffix_code,
   postal_code,
   city,
   prov,
   country )
AS
SELECT street_name.location_id, street_no.location_dtl_val,
          street_name.location_dtl_val, street_suffix.location_dtl_val,
          street_direction.location_dtl_val, apt_no.location_dtl_val,
          house_suffix_code.location_dtl_val,
          postal_code.location_dtl_val, city.location_dtl_val,
          prov.location_dtl_val, country.location_dtl_val
     FROM location_dtl street_no,
          location_dtl street_name,
          location_dtl street_suffix,
          location_dtl street_direction,
          location_dtl apt_no,
          location_dtl house_suffix_code,
          location_dtl postal_code,
          location_dtl city,
          location_dtl prov,
          location_dtl country
    WHERE (street_no.location_id = street_name.location_id)
      AND (street_no.location_id = street_suffix.location_id (+))
      AND (street_no.location_id = street_direction.location_id (+))
      AND (street_no.location_id = apt_no.location_id (+))
      AND (street_no.location_id = house_suffix_code.location_id (+))
      AND (street_no.location_id = postal_code.location_id (+))
      AND (street_no.location_id = city.location_id (+))
      AND (street_no.location_id = prov.location_id (+))
      AND (street_no.location_id = country.location_id (+))
      AND ((street_no.parm_id = 235)
           AND (street_name.parm_id = 236)
           AND (street_suffix.parm_id = 237)
           AND (street_direction.parm_id = 238)
           AND (apt_no.parm_id = 239)
           AND (house_suffix_code.parm_id = 241)
           AND (postal_code.parm_id = 242)
           AND (city.parm_id = 243)
           AND (prov.parm_id = 244)
           AND (country.parm_id = 245))
/
-- End of DDL Script for View V_FULL_ADDRESS

-- Start of DDL Script for View V_ORDR_BRIEF
CREATE OR REPLACE FORCE VIEW V_ORDR_BRIEF
(SUB_ID, SUB_ORDR_ID, ORDER_TYPE, CREATED_BY, HAS_IMPACT, HAS_GROUPS, ORDR_STATUS, 
 ORDR_STATUS_ID, LAST_NAME, FIRST_NAME, ACCT_NO, CREATED_DTM, 
 MODIFIED_DTM, ERR_CODE, ERR_REASON, DESCRIPTION, ORDR_BATCH_ID, 
 ORIGINAL_ORDR_ID, PREREQ_ORDR_ID, RESOLVED_INDICATOR, START_DATETIME, ORDR_OWNER, 
 EXTERNAL_KEY,LATEST_REPAIRING_ORDR_ID)
AS 
SELECT /*+ USE_NL(s,o) */
       o.sub_id,o.sub_ordr_id, o.ordr_typ, o.created_by, o.has_impact,
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'has_groups'),
       s.status, o.ordr_status_id,
       pkg_sp_subbrief.get_sub_contact_parm (o.sub_id, 'last_name'),
       pkg_sp_subbrief.get_sub_contact_parm (o.sub_id, 'first_name'),
       pkg_sp_subbrief.GET_SUB_PARM(o.sub_id,'acct'),
       o.created_dtm, o.modified_dtm,
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'err_code'),
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'err_reason'),
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'description'),
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'batch_ordr_id'),
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'original_ordr_id'),
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'prereq_ordr_id'),
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'resolved_indicator'),
           pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'start_datetime'),
       pkg_sp_subbrief.get_sub_ordr_owner(o.sub_ordr_id),
       external_key,
       fn_get_last_repairing_ordr(o.sub_ordr_id) latest_repairing_ordr_id
  FROM sub_ordr o, ref_status s
  where o.ORDR_STATUS_ID=s.status_id
/
-- End of DDL Script for View V_ORDR_BRIEF


-- Start of DDL Script for View V_BATCH_BRIEF
CREATE OR REPLACE FORCE VIEW V_BATCH_BRIEF
(SUB_ORDR_ID, CREATED_BY, CREATED_DTM, MODIFIED_DTM, DESCRIPTION, 
  ORDR_OWNER, EXTERNAL_BATCH_ID)
AS 
SELECT /*+ first_rows */ o.sub_ordr_id, created_by,
       created_dtm, modified_dtm,
       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'description'),
       q1.ordr_owner,q2.external_batch_id
  FROM sub_ordr o,
       (SELECT a.sub_ordr_id, a.val ordr_owner
             FROM sub_ordr_parm a
            WHERE a.parm_id = get_parm_id ('ordr_owner', get_class_id ('Order'))) q1,
       (SELECT a.sub_ordr_id, a.val external_batch_id
             FROM sub_ordr_parm a
            WHERE a.parm_id = get_parm_id ('external_batch_id', get_class_id ('Order'))) q2   
  where ordr_typ='com.sigma.samp.cmn.order.BatchOrderValue' and o.sub_ordr_id=q1.sub_ordr_id 
        and o.sub_ordr_id=q2.sub_ordr_id(+);
/
-- End of DDL Script for View V_BATCH_BRIEF


-- Start of DDL Script for View V_SP_SUB
CREATE OR REPLACE VIEW V_SP_SUB
(SUB_ID, SUB_TYP_ID, SUB_TYP, SUB_STATUS_ID, SUB_STATUS, 
 PRE_STATUS_ID, PRE_STATUS, SVC_PROVIDER_ID, SVC_PROVIDER, PARENT_SUB_ID, 
 SAMP_VER, SUB_EXT_KEY,HAS_SELECTIVE_SUB_SVC, PARMVAL_TBL, LOCALE_CD)
AS 
SELECT sub_id, sub_typ_id, fn_get_sub_type(sub_typ_id),
          sub_status_id, curr_state.status,pre_status_id,pre_state.status,
          svc_provider_id,FN_GET_SVC_PROVIDER(svc_provider_id),
          parent_sub_id, samp_ver,
          external_key, 
          (SELECT   'y'
               FROM   sub_svc, svc
              WHERE       sub_id = sub.sub_id
                      AND sub_svc.svc_id = svc.svc_id
                      AND sub_svc.sub_svc_status_id != 29
                      AND svc.loading_policy = 'selective'
                      AND ROWNUM = 1) has_sel_sub_svc,
          fnc_sp_get_sub_parmval (sub.sub_id),
          locale_cd
     FROM sub,ref_status curr_state,ref_status pre_state
where sub_status_id=curr_state.status_id and pre_state.status_id (+)= pre_status_id;
/
-- End of DDL Script for View V_SP_SUB

-- Start of DDL Script for View V_CONTACT
CREATE OR REPLACE FORCE VIEW V_CONTACT
(SUB_ID, CONTACT_ID, TYP, CONTACT_TYP_ID, SUB_ADDR_ID, 
 STATUS_ID, STATUS, PRE_STATUS_ID, PRE_STATUS, PARMVAL_TBL, 
 SAMP_VER)
AS 
SELECT sub_id, sub_contact_id,
          fn_get_sub_contact_type(CONTACT_TYP_ID), CONTACT_TYP_ID,
          fn_get_sub_addr_id(location_id),
                  contact_status_id,
          fn_get_status_nm('SubContactSpec',contact_status_id),
                  pre_status_id,
          fn_get_status_nm('SubContactSpec',pre_status_id),
          fnc_sp_get_sub_contact_parmval(sub_contact_id), samp_ver
     FROM sub_contact;
/
-- End of DDL Script for View V_CONTACT

-- Start of DDL Script for View V_SP_SUBSVCADDR
CREATE OR REPLACE FORCE VIEW V_SP_SUBSVCADDR
(SUB_ID, SUB_ADDR_ID, TYP, TYP_ID, PREF_NM, 
 STATUS, STATUS_ID, PRE_STATUS, PRE_STATUS_ID, IS_DFLT, 
 LOCATION_ID, PARMVAL_TBL, SAMP_VER)
AS 
SELECT sub_id, sub_addr_id, fn_get_addr_typ(addr_typ_id), addr_typ_id,
          preferred_nm, fn_get_status_nm('SubAddressSpec',addr_status_id), addr_status_id, 
                  fn_get_status_nm('SubAddressSpec',pre_status_id),pre_status_id,
                  is_dflt,location_id,
          fnc_sp_get_subaddr_parmval (location_id), samp_ver
     FROM sub_addr;
/
-- End of DDL Script for View V_SP_SUBSVCADDR

-- Start of DDL Script for View V_SP_SUBSVCMODEL
CREATE OR REPLACE VIEW V_SP_SUBSVCMODEL
(SUBID, SUBSVCID, SVCID, SVCNM, LOADING_POLICY, 
 PROVISIONSTATUS_ID, PROVISIONSTATUS, PRE_STATUS_ID, PRE_STATUS, SVCLOCATIONID, 
 PARENTSVCID, START_DT, END_DT, PURCHASE_DT, VER, 
 SUB_SVC_EXT_KEY, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY, 
 BASE_SVCID, BASE_SVCNM, PARMVAL_TBL)
AS 
SELECT sub_svc.sub_id, sub_svc.sub_svc_id, sub_svc.svc_id, svc.svc_nm, svc.loading_policy,
          sub_svc.sub_svc_status_id , curr_state.status,
          sub_svc.pre_status_id,pre_state.status,
          ssa.sub_addr_id,
          sub_svc.parent_sub_svc_id,
          sub_svc.start_dt, sub_svc.end_dt, sub_svc.purchase_dt,
          sub_svc.samp_ver, sub_svc.external_key,
          sub_svc.CREATED_DTM, sub_svc.CREATED_BY,
          sub_svc.MODIFIED_DTM, sub_svc.MODIFIED_BY,
          basesvcid(svc.svc_nm),
          decode(basesvcnm(svc.svc_nm), '0', null, basesvcnm(svc.svc_nm)),
          fnc_sp_get_subsvc_parmval (sub_svc.sub_svc_id)
     FROM  sub_svc, svc, sub_svc_addr ssa,
           ref_status curr_state,ref_status pre_state
    WHERE sub_svc.svc_id = svc.svc_id AND
          ssa.sub_svc_id(+) = sub_svc.sub_svc_id
          and sub_svc.sub_svc_status_id=curr_state.status_id
          and pre_state.status_id(+)=sub_svc.pre_status_id;
/
-- End of DDL Script for View V_SP_SUBSVCMODEL

-- Start of DDL Script for View V_SP_SUB_FINDER
CREATE OR REPLACE FORCE VIEW v_sp_sub_finder (
   sub_id,
   sub_status,
   subextkey )
AS
SELECT sub.sub_id, ref_status.status, sub.external_key
     FROM sub, ref_status
    WHERE sub.sub_status_id = ref_status.status_id
/
-- End of DDL Script for View V_SP_SUB_FINDER

-- Start of DDL Script for View V_SP_SUBSVC_FINDER
CREATE OR REPLACE FORCE VIEW v_sp_subsvc_finder (
   sub_svc_id,
   provisionstatus,
   sub_svc_ext_key )
AS
SELECT a.sub_svc_id, b.status, a.external_key
     FROM sub_svc a, ref_status b
    WHERE a.sub_svc_status_id = b.status_id
/
-- End of DDL Script for View V_SP_SUBSVC_FINDER

CREATE OR REPLACE FORCE VIEW V_STATE
( STATE_ID, STATE_NAME, CLASS_NAME, STATE_TYPE ) AS
  SELECT status_id,STATUS,class_nm,STATUS_TYP
  from ref_status, ref_class
  where ref_status.CLASS_ID=ref_class.CLASS_ID
/

CREATE OR REPLACE FORCE VIEW V_SUB_ORDR_PARM
 (SUB_ORDR_ID, PARM_NM, VAL) AS
  SELECT S.SUB_ORDR_ID, P.PARM_NM, S.VAL
  from SUB_ORDR_PARM S, PARM P
  where P.PARM_ID = S.PARM_ID
/


CREATE OR REPLACE FORCE VIEW V_ORDR_ITEM_ENTITY_CHG ( ORDR_ID, 
    SUB_ORDR_ITEM_ID, PARM_ID, PARM_NM, PARM_VAL
   ) AS SELECT sub_ordr_item.sub_ordr_id, ORDR_ITEM_ENTITY_CHG.sub_ordr_item_id,
          ORDR_ITEM_ENTITY_CHG.parm_id, parm.parm_nm,
          ORDR_ITEM_ENTITY_CHG.val
     FROM sub_ordr_item, ORDR_ITEM_ENTITY_CHG, parm
    WHERE (sub_ordr_item.sub_ordr_item_id = ORDR_ITEM_ENTITY_CHG.sub_ordr_item_id)
      AND (parm.parm_id = ORDR_ITEM_ENTITY_CHG.parm_id)
/


CREATE OR REPLACE FORCE VIEW V_SUB_ORDR_ITEM_PARM ( SUB_ORDR_ITEM_ID, 
PARM_NM, VAL ) AS SELECT S.SUB_ORDR_ITEM_ID, P.PARM_NM, S.VAL
  from SUB_ORDR_ITEM_PARM S, PARM P
  where P.PARM_ID = S.PARM_ID
/

CREATE OR REPLACE FORCE VIEW V_SUB_ORDR_ITEM_BRIEF
(SUB_ID, SUB_ORDR_ID, SUB_ORDR_ITEM_ID, CLASS_NM, ENTITY_TYPE,HAS_IMPACT, 
 ORDR_ITEM_DESCRIPTION, ORDR_ITEM_NOTE, MODIFIED_BY, ORDER_ITEM_STATUS, ORDR_ACTION_NM, 
 MODIFIED_DTM, OBJECT_ID, ERR_CODE, ERR_REASON, IS_OVERDUE)
AS 
with err_parm_id as
   (select get_parm_id('err_code', get_class_id('OrderItem')) err_parm_id ,
           get_parm_id('err_reason', get_class_id('OrderItem')) reason_parm_id,
           get_parm_id('is_overdue', get_class_id('OrderItem')) overdue_parm_id
    from dual)
SELECT q1.sub_id, q1.sub_ordr_id, q1.sub_ordr_item_id, q1.item_class,
       q1.entity_type, q1.has_impact, q1.display_info, q1.reason_notes, q1.modified_by, q1.status,
       q1.ordr_action_nm, q1.modified_dtm, q1.item_obj_id,
       fn_sp_get_ordr_line_parm(q1.sub_ordr_item_id,    err_parm_id),
       fn_sp_get_ordr_line_parm(q1.sub_ordr_item_id,    reason_parm_id),
       DECODE (fn_sp_get_ordr_line_parm(q1.sub_ordr_item_id,    overdue_parm_id),'yes','y','n')
  FROM (SELECT /*+first_rows*/
               d.sub_id, a.sub_ordr_id, a.sub_ordr_item_id,
               s.class_nm item_class,
                   s.class_nm || ':' || o.OBJECT_NM entity_type, a.has_impact,
               sub_entity_id item_obj_id,
               reason_notes,
               a.modified_by, a.modified_dtm, b.status,
               DECODE (a.display_info, 'unknown', '', a.display_info) display_info,
               c.ordr_action_nm
          FROM sub_ordr_item a, ref_status b, ref_ordr_action c, sub_ordr d, ref_object_typ o,ref_class s
         WHERE a.ordr_item_status_id = b.status_id
                   and a.OBJECT_TYP_ID=o.OBJECT_TYP_ID
                   and s.CLASS_ID=o.CLASS_ID
           AND a.ordr_action_id = c.ordr_action_id
           AND a.sub_ordr_id = d.sub_ordr_id
         ) q1,
         err_parm_id pe
/


CREATE OR REPLACE FORCE VIEW V_SUBNTWK
(SUBNTWK_ID, SUBNTWK_NM, TOPOLOGY_STATUS_ID, 
UPPER_SUBNTWK_NM, SUBNTWK_TYP_ID, PARENT_SUBNTWK_ID,
CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY,
PARENT_SUBNTWK_ID_ALIAS)
AS 
select 
SUBNTWK_ID,SUBNTWK_NM,TOPOLOGY_STATUS_ID,
UPPER_SUBNTWK_NM,SUBNTWK_TYP_ID, PARENT_SUBNTWK_ID,
CREATED_DTM,CREATED_BY,MODIFIED_DTM,MODIFIED_BY,
nvl(parent_subntwk_id,-9999) from subntwk
/

CREATE OR REPLACE FORCE VIEW V_SUB_USER ( SUB_USER_ID, 
SUB_ID, OBJECT_TYPE, STATUS, SAMP_VER, 
EXTERNAL_KEY, CREATED_BY, CREATED_DTM, PRE_STATUS, 
MODIFIED_DTM, MODIFIED_BY, PARMVAL_TBL ) AS SELECT 
  SUB_USER.sub_user_id,
  SUB_USER.sub_id,  
  REF_OBJECT_TYP.OBJECT_NM,
  fn_get_status_nm('SubUserSpec',sub_user.USER_STATUS_ID),
  SUB_USER.samp_ver,
  SUB_USER.external_key,
  SUB_USER.created_by,
  SUB_USER.created_dtm,
  fn_get_status_nm('SubUserSpec',sub_user.PRE_STATUS_ID),
  SUB_USER.modified_dtm,
  SUB_USER.modified_by,
  fnc_sp_get_sub_user_parmval(sub_user_id)
FROM 
  SUB_USER ,
  REF_OBJECT_TYP   
WHERE 
  (SUB_USER.OBJECT_TYP_ID = ref_object_typ.OBJECT_TYP_ID)
/

CREATE OR REPLACE force VIEW V_ORDR_NQ_BY_ACCT
(SUB_ID, SUB_ORDR_ID,  CREATED_BY, ORDR_STATUS, 
 ACCT_NO, CREATED_DTM, 
 MODIFIED_DTM)
AS 
SELECT
       o.sub_id,o.sub_ordr_id, o.created_by,
       s.status, 
       a.acct_no,
       o.created_dtm, o.modified_dtm
  FROM sub_ordr o, ref_status s,
       (SELECT a.sub_id, a.val acct_no
             FROM sub_parm a
            WHERE a.parm_id = get_parm_id ('acct', get_class_id ('SubSpec'))) a
  where o.sub_id = a.sub_id and o.ORDR_STATUS_ID=s.status_id
/

CREATE OR REPLACE force VIEW V_ORDR_NQ_BY_OWNER_RIND
(SUB_ID, SUB_ORDR_ID,  CREATED_BY, ORDR_STATUS, 
 CREATED_DTM, 
 MODIFIED_DTM, DESCRIPTION, ORDR_BATCH_ID,  
 RESOLVED_INDICATOR, ORDR_OWNER)
AS
SELECT null,null,null,null,null,null,null,null,null,null FROM dual;
/

--sample implementation of view V_ORDR_NQ_BY_OWNER_RIND
--SELECT /*+ first_rows */   o.sub_id,q.sub_ordr_id,  o.created_by,
--        s.status, 
--       o.created_dtm, o.modified_dtm, 
--       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'description'),
--       pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'batch_ordr_id'),
--       q.resolved_indicator,
--       pkg_sp_subbrief.get_sub_ordr_owner(o.sub_ordr_id)
--  FROM 
--       (SELECT /*+ NO_MERGE */  a.sub_ordr_id, a.val resolved_indicator
--             FROM sub_ordr_parm a
--            WHERE a.parm_id = get_parm_id ('resolved_indicator', get_class_id ('Order'))) q, sub_ordr o, ref_status s
--  where o.sub_ordr_id = q.sub_ordr_id   and  o.ORDR_STATUS_ID=s.status_id


CREATE OR REPLACE force VIEW V_ORDR_NQ_BY_BATCH_ID
(SUB_ID, SUB_ORDR_ID,  CREATED_BY, ORDR_STATUS, 
 CREATED_DTM, 
 MODIFIED_DTM, DESCRIPTION, ORDR_BATCH_ID,  
 ORDR_OWNER,external_key)
AS 
SELECT /*+ index(so sub_ordr_pk) */ 
       so.sub_id, 
       so.sub_ordr_id, 
       so.created_by, 
       FN_GET_STATUS_NM(get_class_id ('Order'), so.ordr_status_id) status, 
       so.created_dtm, 
       so.modified_dtm,
       pkg_sp_subbrief.get_sub_ordr_parm (so.sub_ordr_id, 'description'), 
       q.batch_ordr_id,
       pkg_sp_subbrief.get_sub_ordr_owner (so.sub_ordr_id), 
       so.external_key
  FROM sub_ordr so,
       (SELECT /*+ index(sop sub_ordr_parm_ix2) */ sub_ordr_id, val batch_ordr_id
          FROM sub_ordr_parm sop
         WHERE parm_id = get_parm_id ('batch_ordr_id', get_class_id ('Order'))) q
WHERE so.sub_ordr_id = q.sub_ordr_id 
/

 CREATE OR REPLACE force VIEW V_ORDR_NQ_BY_PREREQ_ID
(SUB_ID, SUB_ORDR_ID,  CREATED_BY, ORDR_STATUS_ID, 
 CREATED_DTM, MODIFIED_DTM,prereq_ordr_id)
AS 
SELECT /*+ first_rows(100) */ o.sub_id,o.sub_ordr_id,  o.created_by,
        o.ordr_status_id, 
       o.created_dtm, o.modified_dtm,q.prereq_ordr_id
   FROM sub_ordr o, 
       (SELECT a.sub_ordr_id, a.val prereq_ordr_id
             FROM sub_ordr_parm a
            WHERE a.parm_id = get_parm_id ('prereq_ordr_id', get_class_id ('Order'))) q
  where o.sub_ordr_id = q.sub_ordr_id  
/

CREATE OR REPLACE force VIEW V_ORDR_NQ_BY_ORIGINAL_ID
(SUB_ID, SUB_ORDR_ID,  CREATED_BY, ORDR_STATUS, 
 CREATED_DTM, MODIFIED_DTM,original_ordr_id,resolved_indicator)
AS 
SELECT /*+ first_rows(100) */ o.sub_id,o.sub_ordr_id,  o.created_by,
        s.status, 
       o.created_dtm, o.modified_dtm,q.original_ordr_id,
      pkg_sp_subbrief.get_sub_ordr_parm(o.sub_ordr_id, 'resolved_indicator')
   FROM sub_ordr o, ref_status s,
       (SELECT a.sub_ordr_id, a.val original_ordr_id
             FROM sub_ordr_parm a
            WHERE a.parm_id = get_parm_id ('original_ordr_id', get_class_id ('Order'))) q
  where o.sub_ordr_id = q.sub_ordr_id(+)   and ordr_status_id=s.status_id
/

