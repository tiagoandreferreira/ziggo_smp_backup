--==========================================================================
-- FILE INFO
-- $Id: sp_query_plsql.sql,v 1.7 2012/01/06 12:16:03 prithvim Exp $
--
-- REVISION HISTORY
-- * Based on CVS log
--==========================================================================

CREATE OR REPLACE FORCE VIEW v_know_id_search (
   sub_id,
   know_sub_id,
   ptn
)
AS
 select  sub_id, val know_sub_id
   from sub_parm where parm_id = get_parm_id('know_sub_id', get_class_id('SubSpec'))

---******************** NAMED QUERY SEARCHES **************************************************
CREATE OR REPLACE FORCE VIEW v_phone_account_search (
   sub_id,
   sub_status,
   svc_provider,
   account_number,
   home_phone_number
)
AS
SELECT /*+ first_rows*/
          a.sub_id,
          b.status,
          fn_get_svc_provider (a.svc_provider_id),
          q1.acct,
          q_phone.phone_number
     FROM (SELECT a.sub_id,
                  a.val acct
             FROM sub_parm a
            WHERE a.parm_id = get_parm_id ('acct', get_class_id ('SubSpec'))) q1,
          (SELECT c.val phone_number,
                  e.sub_id
             FROM sub_contact_parm c,
                  parm d,
                  sub_contact e
            WHERE c.parm_id = d.parm_id
              AND c.SUB_CONTACT_ID = e.SUB_CONTACT_ID
              AND d.parm_nm = 'phones.home.number') q_phone,
          sub a,
          ref_status b
    WHERE a.sub_id = q1.sub_id
      AND a.sub_id = q_phone.sub_id
      AND a.sub_status_id = b.status_id
/
SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW v_account_search (
   sub_id,
   sub_status,
   svc_provider,
   account_number,
   company_no )
AS
SELECT /*+ first_rows*/
          a.sub_id, b.status, fn_get_svc_provider (a.svc_provider_id),
          q1.acct, q1.co
     FROM (SELECT a.sub_id, a.val acct, b.val co
             FROM sub_parm a, sub_parm b
            WHERE a.parm_id = get_parm_id ('acct', get_class_id ('SubSpec'))
              AND b.parm_id = get_parm_id ('corp', get_class_id ('SubSpec'))
              AND a.sub_id = b.sub_id) q1,
          sub a,
          ref_status b
    WHERE a.sub_id = q1.sub_id AND a.sub_status_id = b.status_id
/
SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW v_email_search (
   sub_id,
   sub_svc_id,
   sub_status,
   svc_provider,
   email_id )
AS
SELECT /*+first_rows*/
       a.sub_id,
       a.sub_svc_id,
       d.status,
       fn_get_svc_provider (c.svc_provider_id),
       b.val email_id
  FROM sub_svc a,
       sub_svc_parm b,
       sub c,
       ref_status d
 WHERE a.sub_id = c.sub_id
   AND c.sub_status_id = d.status_id
   AND a.sub_svc_id = b.sub_svc_id
   AND b.parm_id in (
          get_parm_id('email_id',100,get_svcid('samp_email')),
          get_parm_id('incoming_user_name',100,get_svcid('samp_email_alias')),
          get_parm_id('user_name',100,get_svcid('smp_detailed_email')));
/
SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW v_internet_reg_cd_search (
   sub_id,
   sub_status,
   svc_provider,
   internet_reg_cd )
AS
SELECT /*+ first_rows*/
          a.sub_id, b.status, fn_get_svc_provider (a.svc_provider_id),
          q1.irc
     FROM (SELECT a.sub_id, a.val irc
             FROM sub_parm a
            WHERE a.parm_id =
                         get_parm_id ('internet_reg_cd', get_class_id ('SubSpec'))) q1,
          sub a,
          ref_status b
    WHERE a.sub_id = q1.sub_id AND a.sub_status_id = b.status_id
/
SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW v_name_search (
   sub_id,
   sub_status,
   svc_provider,
   first_name,
   last_name,
   addr_home_city,
   postal_cd )
AS
SELECT /*+first_rows*/
          a.sub_id, h.status, g.svc_provider_nm, c1.val first_name,
          c2.val last_name, f.location_dtl_val city,
          e.location_dtl_val postal_cd
     FROM sub a,
          sub_contact b,
          sub_contact_parm c1,
          sub_contact_parm c2,
          sub_addr d,
          location_dtl e,
          location_dtl f,
          svc_provider g,
          ref_status h
    WHERE a.sub_id = b.sub_id
      AND a.sub_id = d.sub_id
      AND b.sub_contact_id = c1.sub_contact_id
      AND b.sub_contact_id = c2.sub_contact_id
      AND d.location_id = f.location_id
      AND d.location_id = e.location_id
      AND a.svc_provider_id = g.svc_provider_id
      AND a.sub_status_id = h.status_id
      AND e.parm_id =
                 get_parm_id ('postal_cd', get_class_id ('SubAddressSpec'))
      AND d.is_dflt = 'y'
      AND f.parm_id = get_parm_id ('city', get_class_id ('SubAddressSpec'))
      AND c1.parm_id = get_parm_id (
                          'first_name',
                          get_class_id ('SubContactSpec')
                       )
      AND c2.parm_id = get_parm_id (
                          'last_name',
                          get_class_id ('SubContactSpec')
                       )
/
SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW v_primary_email_search (
   sub_id,
   sub_svc_id,
   sub_status,
   svc_provider,
   email_id )
AS
SELECT /*+first_rows*/
          a.sub_id, a.sub_svc_id, f.status,
          fn_get_svc_provider (d.svc_provider_id), c.val email_id
     FROM sub_svc a, sub_svc_parm c, sub d, ref_status f
    WHERE d.sub_id = a.sub_id
      AND d.sub_status_id = f.status_id
      AND a.sub_svc_id = c.sub_svc_id
      AND a.svc_id = get_svcid ('sigma_primary_email')
      AND c.parm_id = get_parm_id (
                         'email_id',
                         get_class_id ('SubSvcSpec'),
                         get_svcid ('samp_email')
                      )
/
SHOW ERRORS;

CREATE OR REPLACE FORCE VIEW v_phone_search (
   sub_id,
   sub_status,
   svc_provider,
   home_phone_number )
AS
SELECT /*+ first_rows */
       a.sub_id, b.status, c.svc_provider_nm, q1.phone_number
  FROM (SELECT /*+first_rows */
               e.sub_id, c.val phone_number
          FROM sub_contact_parm c,
               parm d,
               sub_contact e
         WHERE c.parm_id = d.parm_id
           AND d.parm_nm = 'phones.home.number'
           AND e.sub_contact_id = c.sub_contact_id) q1,
       sub a,
       ref_status b,
       svc_provider c
 WHERE a.sub_id = q1.sub_id
   AND a.sub_status_id = b.status_id
   AND a.svc_provider_id = c.svc_provider_id
/
SHOW ERRORS;
---******************** NEW NAMED QUERY SEARCHES **************************************************

-- Start of DDL Script for View V_ACCOUNT_SEARCH_NQ

CREATE OR REPLACE VIEW v_account_search_nq (sub_id,
                                                                sub_status,
                                                                svc_provider,
                                                                salutation,
                                                                first_name,
                                                                middle_name,
                                                                last_name,
                                                                suffix,
                                                                account_no
                                                               )
AS SELECT
        a.sub_id,
        b.status sub_status,
        fn_get_svc_provider (a.svc_provider_id) svc_provider,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'salutation' ) salutation,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'first_name') first_name,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'middle_name') middle_name,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'last_name') last_name,
        pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
        sp.val account_no
   FROM sub a,
        ref_status b, sub_parm sp
   WHERE a.sub_status_id = b.status_id AND b.class_id = 111 AND a.sub_typ_id = 1
           AND sp.parm_id = get_parm_id ('acct', get_class_id ('SubSpec'))
           AND sp.sub_id = a.sub_id;
/

-- End of DDL Script for View V_ACCOUNT_SEARCH_NQ

-- Start of DDL Script for View V_EMAIL_SEARCH_NQ
CREATE OR REPLACE FORCE VIEW v_email_search_nq (
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   email_id,
   company_no,
   account_no,
   home_phone_number,
   addr_home_street_no,
   addr_home_street_name,
   addr_home_street_suffix,
   addr_home_street_direction,
   addr_home_apt_no,
   addr_home_house_suffix_cd,
   addr_home_city,
   addr_home_prov,
   addr_home_postal_code,
   addr_home_country
)
AS
   SELECT /*+first_rows*/
          a.sub_id, d.status sub_status,
          fn_get_svc_provider (c.svc_provider_id) svc_provider,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'first_name') first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'last_name') last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'suffix') suffix,
          b.val email_id,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'co') company_no,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'phones.home.number') home_phone_number,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_num') addr_home_street_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_nm') addr_home_street_name,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_suffix')
                addr_home_street_suffix,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_dir')
                addr_home_street_direction,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'apt_num') addr_home_apt_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'house_suffix_cd')
                addr_home_house_suffix_cd,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'city') addr_home_city,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'prov') addr_home_prov,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'postal_cd') addr_home_postal_code,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'country') addr_home_country
     FROM sub_svc a, sub_svc_parm b, sub c, ref_status d
    WHERE a.sub_id = c.sub_id
      AND c.sub_status_id = d.status_id
      AND a.sub_svc_id = b.sub_svc_id
      AND b.parm_id in (
          get_parm_id('user_name',100,get_svcid('samp_detailed_email')),
          get_parm_id('email_id',100,get_svcid('samp_email')),
          get_parm_id('user_name',100,get_svcid('smp_email')));
/


-- End of DDL Script for View V_EMAIL_SEARCH_NQ

-- Start of DDL Script for View V_MAC_SEARCH_NQ
CREATE OR REPLACE VIEW V_MAC_SEARCH_NQ
(SUB_ID, SUB_STATUS, SVC_PROVIDER, SALUTATION, FIRST_NAME,
 MIDDLE_NAME, LAST_NAME, SUFFIX, MAC_ADDRESS, COMPANY_NO,
 ACCOUNT_NO, HOME_PHONE_NUMBER, ADDR_HOME_STREET_NO, ADDR_HOME_STREET_NAME, ADDR_HOME_STREET_SUFFIX,
 ADDR_HOME_STREET_DIRECTION, ADDR_HOME_APT_NO, ADDR_HOME_HOUSE_SUFFIX_CD, ADDR_HOME_CITY, ADDR_HOME_PROV,
 ADDR_HOME_POSTAL_CODE, ADDR_HOME_COUNTRY)
AS
SELECT /*+first_rows*/
          a.sub_id, f.status, fn_get_svc_provider (d.svc_provider_id),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'first_name') first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'last_name') last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'suffix') suffix,
          c.val mac_address,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'co') company_no,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'phones.home.number') home_phone_number,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_num') addr_home_street_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_nm') addr_home_street_name,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_suffix')
                addr_home_street_suffix,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_dir')
                addr_home_street_direction,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'apt_num') addr_home_apt_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'house_suffix_cd')
                addr_home_house_suffix_cd,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'city') addr_home_city,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'prov') addr_home_prov,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'postal_cd') addr_home_postal_code,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'country') addr_home_country
     FROM sub_svc a, sub_svc_parm c, sub d, ref_status f
    WHERE d.sub_id = a.sub_id
      AND d.sub_status_id = f.status_id
      AND a.sub_svc_id = c.sub_svc_id
      AND c.parm_id in (
          get_parm_id('mac_address',100,get_svcid('samp_detailed_hsd_access')),
          get_parm_id('cm_mac',100,get_svcid('samp_ds_hsd_access')),
          get_parm_id('cm_mac',100,get_svcid('samp_lancity_hsd_access')),
          get_parm_id('cm_mac',100,get_svcid('samp_motorola_hsd_access')),
          get_parm_id('cm_mac',100,get_svcid('samp_terayon_hsd_access')),
          get_parm_id('cm_mac',100,get_svcid('smp_hsd_access')));
/


-- End of DDL Script for View V_MAC_SEARCH_NQ

-- Start of DDL Script for View V_NAME_ACCOUNT_SEARCH_NQ
CREATE OR REPLACE FORCE VIEW v_name_account_search_nq (sub_id,
                                                         sub_status,
                                                         svc_provider,
                                                         salutation,
                                                         first_name,
                                                         middle_name,
                                                         last_name,
                                                         suffix,
                                                         company_no,
                                                         account_no,
                                                         home_phone_number,
                                                         addr_home_street_no,
                                                         addr_home_street_name,
                                                         addr_home_street_suffix,
                                                         addr_home_street_direction,
                                                         addr_home_apt_no,
                                                         addr_home_house_suffix_cd,
                                                         addr_home_city,
                                                         addr_home_prov,
                                                         addr_home_postal_code,
                                                         addr_home_country
                                                        )
AS
   SELECT /*+ first_rows*/
          a.sub_id, b.status, fn_get_svc_provider (a.svc_provider_id),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,
                                                'salutation'
                                               ) salutation,
          q2.first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,
                                                'middle_name'
                                               ) middle_name,
          q2.last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
          q1.co, q1.acct,
          pkg_sp_subbrief.get_sub_contact_parm
                                      (a.sub_id,
                                       'phones.home.number'
                                      ) home_phone_number,
          pkg_sp_subbrief.get_location_parm (a.sub_id,
                                             'street_num'
                                            ) addr_home_street_no,
          pkg_sp_subbrief.get_location_parm
                                           (a.sub_id,
                                            'street_nm'
                                           ) addr_home_street_name,
          pkg_sp_subbrief.get_location_parm
                                     (a.sub_id,
                                      'street_suffix'
                                     ) addr_home_street_suffix,
          pkg_sp_subbrief.get_location_parm
                                     (a.sub_id,
                                      'street_dir'
                                     ) addr_home_street_direction,
          pkg_sp_subbrief.get_location_parm (a.sub_id,
                                             'apt_num'
                                            ) addr_home_apt_no,
          pkg_sp_subbrief.get_location_parm
                                 (a.sub_id,
                                  'house_suffix_cd'
                                 ) addr_home_house_suffix_cd,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'city'),
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'prov') addr_home_prov,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'postal_cd'),
          pkg_sp_subbrief.get_location_parm (a.sub_id,
                                             'country'
                                            ) addr_home_country
     -->q2.first_name, q2.last_name, q1.acct, q1.co
   FROM   (SELECT a.sub_id, a.val acct, b.val co
             FROM sub_parm a, sub_parm b
            WHERE a.parm_id = get_parm_id ('acct', get_class_id ('SubSpec'))
              AND b.parm_id = get_parm_id ('corp', get_class_id ('SubSpec'))
              AND a.sub_id = b.sub_id) q1,
          (SELECT sc.sub_id, scp.val last_name, scp2.val first_name
             FROM sub_contact sc, sub_contact_parm scp, sub_contact_parm scp2
            WHERE sc.sub_contact_id = scp.sub_contact_id
              AND sc.sub_contact_id = scp2.sub_contact_id
              AND scp.parm_id =
                     get_parm_id ('last_name',
                                  get_class_id ('SubContactSpec'))
              AND scp2.parm_id =
                     get_parm_id ('first_name',
                                  get_class_id ('SubContactSpec')
                                 )) q2,
          sub a,
          ref_status b
    WHERE a.sub_id = q1.sub_id
      AND a.sub_id = q2.sub_id
      AND a.sub_status_id = b.status_id
/
-- End of DDL Script for View V_NAME_ACCOUNT_SEARCH_NQ



-- Start of DDL Script for View V_NAME_SEARCH_NQ

CREATE OR REPLACE VIEW v_name_search_nq ( 
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   account_no
 ) 
AS SELECT 
          a.sub_id, 
          h.status sub_status, 
          g.svc_provider_nm SVC_PROVIDER,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'salutation') salutation,
          c1.val first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'middle_name') middle_name,
          c2.val last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no
     FROM sub a,
          sub_contact b,
          sub_contact_parm c1,
          sub_contact_parm c2,
          sub_addr d,
          svc_provider g,
          ref_status h
    WHERE a.sub_id = b.sub_id
      AND a.sub_id = d.sub_id
      AND b.sub_contact_id = c1.sub_contact_id
      AND b.sub_contact_id = c2.sub_contact_id
      AND a.svc_provider_id = g.svc_provider_id
      AND a.sub_status_id = h.status_id
      AND h.class_id = 111
      AND d.is_dflt = 'y'
      AND c1.parm_id = get_parm_id (
                          'first_name',
                          get_class_id ('SubContactSpec')
                       )
      AND c2.parm_id = get_parm_id (
                          'last_name',
                          get_class_id ('SubContactSpec')
                       )
      AND a.sub_typ_id = 1
/
-- End of DDL Script for View V_NAME_SEARCH_NQ


-- Start of DDL Script for View V_SERVICE_IMSI_SEARCH_NQ

CREATE OR REPLACE VIEW v_service_imsi_search_nq ( 
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   account_no,
  imsi 
)
AS SELECT 
          a.sub_id, 
          f.status,
          fn_get_svc_provider (d.svc_provider_id),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'first_name') first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'last_name') last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          c.val service_phone
    FROM  SUB_SVC a, SUB_SVC_PARM c, SUB d, REF_STATUS f, SVC s, PARM p
    WHERE d.sub_id = a.sub_id
      AND d.sub_status_id = f.status_id
      AND a.sub_svc_id = c.sub_svc_id
      AND s.svc_id = a.svc_id
      AND c.parm_id = p.parm_id
      AND p.parm_nm = 'imsi'
      AND (s.svc_nm = 'mobile_sim_card' )
      AND a.sub_svc_status_id != 29
/
-- End of DDL Script for View V_SERVICE_IMSI_SEARCH_NQ

-- Start of DDL Script for View V_SERVICE_MSISDN_SEARCH_NQ

CREATE OR REPLACE VIEW v_service_msisdn_search_nq ( 
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   account_no,
  msisdn 
)
AS SELECT 
          a.sub_id, 
          f.status,
          fn_get_svc_provider (d.svc_provider_id),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'first_name') first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'last_name') last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          c.val service_phone
    FROM  SUB_SVC a, SUB_SVC_PARM c, SUB d, REF_STATUS f, SVC s, PARM p
    WHERE d.sub_id = a.sub_id
      AND d.sub_status_id = f.status_id
      AND a.sub_svc_id = c.sub_svc_id
      AND a.sub_svc_status_id not in ( 29 )
      AND s.svc_id = a.svc_id
      AND c.parm_id = p.parm_id
      AND p.parm_nm = 'msisdn'
      AND (s.svc_nm = 'mobile_service_identity' )
/

-- End of DDL Script for View V_SERVICE_MSISDN_SEARCH_NQ

-- Start of DDL Script for View V_PHONE_COMPANY_SEARCH_NQ
CREATE OR REPLACE FORCE VIEW v_phone_company_search_nq (
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   company_no,
   account_no,
   home_phone_number,
   addr_home_street_no,
   addr_home_street_name,
   addr_home_street_suffix,
   addr_home_street_direction,
   addr_home_apt_no,
   addr_home_house_suffix_cd,
   addr_home_city,
   addr_home_prov,
   addr_home_postal_code,
   addr_home_country
)
AS
   SELECT /*+ first_rows */
          q2.sub_id, q2.status, q2.svc_provider_nm,
          pkg_sp_subbrief.get_sub_contact_parm (q2.sub_id,'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (q2.sub_id,'first_name') first_name,
          pkg_sp_subbrief.get_sub_contact_parm (q2.sub_id,'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (q2.sub_id,'last_name') last_name,
          pkg_sp_subbrief.get_sub_contact_parm (q2.sub_id,'suffix') suffix,
          q2.co company_no,
          pkg_sp_subbrief.get_sub_parm (q2.sub_id, 'acct') account_no,
          q2.phone_number,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'street_num') addr_home_street_no,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'street_nm') addr_home_street_name,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'street_suffix')
                addr_home_street_suffix,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'street_dir')
                addr_home_street_direction,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'apt_num') addr_home_apt_no,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'house_suffix_cd')
                addr_home_house_suffix_cd,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'city'),
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'prov') addr_home_prov,
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'postal_cd'),
          pkg_sp_subbrief.get_location_parm (q2.sub_id, 'country') addr_home_country
     FROM (SELECT /*+ first_rows*/
                  a.sub_id, b.status,
                  fn_get_svc_provider (a.svc_provider_id) svc_provider_nm,
                  q1.co, q_phone.phone_number
             FROM (SELECT a.sub_id, a.val co
                     FROM sub_parm a
                    WHERE a.parm_id =
                                    get_parm_id ('corp', get_class_id ('SubSpec'))) q1,
                  (SELECT e.sub_id,
                          c.val phone_number
                     FROM sub_contact_parm c,
                          parm d,
                          sub_contact e
                    WHERE c.parm_id = d.parm_id
                      AND d.parm_nm = 'phones.home.number'
                      AND e.sub_contact_id = c.sub_contact_id) q_phone,
                  sub a,
                  ref_status b
            WHERE a.sub_id = q1.sub_id
              AND a.sub_id = q_phone.sub_id
              AND a.sub_status_id = b.status_id) q2
/


-- End of DDL Script for View V_PHONE_COMPANY_SEARCH_NQ

-- Start of DDL Script for View V_PHONE_NAME_SEARCH_NQ
CREATE OR REPLACE FORCE VIEW v_phone_name_search_nq (
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   company_no,
   account_no,
   home_phone_number,
   addr_home_street_no,
   addr_home_street_name,
   addr_home_street_suffix,
   addr_home_street_direction,
   addr_home_apt_no,
   addr_home_house_suffix_cd,
   addr_home_city,
   addr_home_prov,
   addr_home_postal_code,
   addr_home_country
)
AS
   SELECT /*+ first_rows */
          a.sub_id, b.status, c.svc_provider_nm,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'salutation') salutation,
          q3.first_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'middle_name') middle_name,
          q3.last_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'suffix') suffix,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'co') company_no,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          q1.phone_number,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_num') addr_home_street_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_nm') addr_home_street_name,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_suffix')
                addr_home_street_suffix,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_dir')
                addr_home_street_direction,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'apt_num') addr_home_apt_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'house_suffix_cd')
                addr_home_house_suffix_cd,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'city'),
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'prov') addr_home_prov,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'postal_cd'),
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'country') addr_home_country
     -->q3.phone_number, q3.last_name, q3.first_name
     FROM (SELECT /*+first_rows */
                  e.sub_id, c.val phone_number
             FROM sub_contact_parm c,
                  parm d,
                  sub_contact e
            WHERE c.parm_id = d.parm_id
              AND d.parm_nm = 'phones.home.number'
              AND e.sub_contact_id = c.sub_contact_id) q1,
          (SELECT a.sub_id, lp.val last_name, fp.val first_name
             FROM sub_contact a, sub_contact_parm lp, sub_contact_parm fp
            WHERE a.sub_contact_id = lp.sub_contact_id
              AND a.sub_contact_id = fp.sub_contact_id
              AND fp.parm_id = get_parm_id ('first_name',get_class_id ('SubContactSpec'))
              AND lp.parm_id = get_parm_id ('last_name',get_class_id ('SubContactSpec'))) q3,
          sub a,
          ref_status b,
          svc_provider c
    WHERE a.sub_id = q1.sub_id
      AND a.sub_id = q3.sub_id
      AND a.sub_status_id = b.status_id
      AND a.svc_provider_id = c.svc_provider_id
/


-- End of DDL Script for View V_PHONE_NAME_SEARCH_NQ

-- Start of DDL Script for View V_PHONE_SEARCH_NQ
CREATE OR REPLACE FORCE VIEW v_phone_search_nq (
   sub_id,
   sub_status,
   svc_provider,
   salutation,
   first_name,
   middle_name,
   last_name,
   suffix,
   company_no,
   account_no,
   home_phone_number,
   addr_home_street_no,
   addr_home_street_name,
   addr_home_street_suffix,
   addr_home_street_direction,
   addr_home_apt_no,
   addr_home_house_suffix_cd,
   addr_home_city,
   addr_home_prov,
   addr_home_postal_code,
   addr_home_country
)
AS
   SELECT /*+ first_rows */
          a.sub_id, b.status, c.svc_provider_nm,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'salutation') salutation,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'first_name'),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'middle_name') middle_name,
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'last_name'),
          pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'suffix') suffix,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'co') company_no,
          pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
          q1.phone_number,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_num') addr_home_street_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_nm') addr_home_street_name,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_suffix')
                addr_home_street_suffix,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'street_dir')
                addr_home_street_direction,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'apt_num') addr_home_apt_no,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'house_suffix_cd')
                addr_home_house_suffix_cd,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'city'),
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'prov') addr_home_prov,
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'postal_cd'),
          pkg_sp_subbrief.get_location_parm (a.sub_id, 'country') addr_home_country
     FROM (SELECT /*+first_rows */
                  e.sub_id, c.val phone_number
             FROM sub_contact_parm c,
                  parm d,
                  sub_contact e
            WHERE c.parm_id = d.parm_id
              AND d.parm_nm = 'phones.home.number'
              AND e.sub_contact_id = c.sub_contact_id) q1,
          sub a,
          ref_status b,
          svc_provider c
    WHERE a.sub_id = q1.sub_id
      AND a.sub_status_id = b.status_id
      AND a.svc_provider_id = c.svc_provider_id
/


-- End of DDL Script for View V_PHONE_SEARCH_NQ

--- START of Functions needed for NQ views ---
create or replace
FUNCTION fn_get_parm_val (
   p_sub_svc_id   IN   sub_svc.sub_svc_id%TYPE,
   p_sub_svc_nm   IN   svc.svc_nm%TYPE
)
   RETURN  sub_svc_parm.val%TYPE
IS
   result  sub_svc_parm.val%TYPE;
BEGIN
   select sub_svc_parm.val into result
   from sub_svc_parm, parm where
   sub_svc_parm.sub_svc_id = p_sub_svc_id AND
   sub_svc_parm.parm_id = parm.parm_id AND
   parm.parm_nm = p_sub_svc_nm;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      -- get the default value
      select parm.dflt_val into result from parm, ref_object_typ,
      sub_svc, svc
      where sub_svc.sub_svc_id = p_sub_svc_id AND
      sub_svc.sub_svc_status_id != 29 AND
      svc.svc_id = sub_svc.svc_id AND
      ref_object_typ.object_nm = svc.svc_nm AND
      parm.object_id = ref_object_typ.object_id AND
      parm.parm_nm = p_sub_svc_nm;
      RETURN result;
END fn_get_parm_val;
/

create or replace
FUNCTION fn_get_svc_for_assoc_svc (
   p_sub_svc_id   IN   sub_svc_assoc.assoc_sub_svc_id%TYPE,
   p_sub_svc_assoc_nm   IN   svc.svc_nm%TYPE
)
   RETURN  sub_svc_assoc.sub_svc_id%TYPE
IS
   result  sub_svc_assoc.sub_svc_id%TYPE;
BEGIN
   select sub_svc_assoc.sub_svc_id into result
   from sub_svc_assoc, ref_assoc_typ, ref_assoc_rule
   where ref_assoc_typ.assoc_typ_nm = p_sub_svc_assoc_nm AND
   ref_assoc_rule.assoc_typ_id = ref_assoc_typ.assoc_typ_id AND
   sub_svc_assoc.assoc_rule_id = ref_assoc_rule.assoc_rule_id AND
   sub_svc_assoc.assoc_sub_svc_id = p_sub_svc_id;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_svc_for_assoc_svc;
/

create or replace
FUNCTION fn_get_assoc_svc_for_svc (
   p_sub_svc_id   IN   sub_svc_assoc.assoc_sub_svc_id%TYPE,
   p_sub_svc_assoc_nm   IN   svc.svc_nm%TYPE
)
   RETURN  sub_svc_assoc.sub_svc_id%TYPE
IS
   result  sub_svc_assoc.sub_svc_id%TYPE;
BEGIN
   select sub_svc_assoc.assoc_sub_svc_id into result
   from sub_svc_assoc, ref_assoc_typ, ref_assoc_rule
   where ref_assoc_typ.assoc_typ_nm = p_sub_svc_assoc_nm AND
   ref_assoc_rule.assoc_typ_id = ref_assoc_typ.assoc_typ_id AND
   sub_svc_assoc.assoc_rule_id = ref_assoc_rule.assoc_rule_id AND
   sub_svc_assoc.sub_svc_id = p_sub_svc_id;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_assoc_svc_for_svc;
/

create or replace
FUNCTION fn_get_depy_svc (
   p_sub_svc_id   IN   sub_svc_depy.prerequisite_sub_svc_id%TYPE
)
   RETURN  sub_svc_depy.dependent_sub_svc_id%TYPE
IS
   result  sub_svc_depy.dependent_sub_svc_id%TYPE;
BEGIN
   select sub_svc_depy.dependent_sub_svc_id into result
   from sub_svc_depy where
   sub_svc_depy.prerequisite_sub_svc_id = p_sub_svc_id;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_depy_svc;
/

create or replace
FUNCTION fn_get_pre_req_svc (
   p_sub_svc_id   IN   sub_svc_depy.dependent_sub_svc_id%TYPE,
   p_sub_svc_nm   IN   svc.svc_nm%TYPE
)
   RETURN  sub_svc_depy.prerequisite_sub_svc_id%TYPE
IS
   result  sub_svc_depy.prerequisite_sub_svc_id%TYPE;
BEGIN
   select sub_svc_depy.prerequisite_sub_svc_id into result
   from sub_svc_depy, svc, sub_svc where
   sub_svc_depy.dependent_sub_svc_id = p_sub_svc_id AND
   sub_svc.sub_svc_id = sub_svc_depy.prerequisite_sub_svc_id AND
   sub_svc.svc_id = svc.svc_id AND
   svc_nm = p_sub_svc_nm;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_pre_req_svc;
/

create or replace
FUNCTION fn_get_sibling_svc (
   p_sub_svc_id   IN   sub_svc.sub_svc_id%TYPE,
   p_sub_svc_nm   IN   svc.svc_nm%TYPE
)
   RETURN  sub_svc.sub_svc_id%TYPE
IS
   result  sub_svc.sub_svc_id%TYPE;
BEGIN
   select device.sub_svc_id into result from sub_svc hsd,
   sub_svc device, svc dsvc where
   hsd.sub_svc_id = p_sub_svc_id AND
   device.parent_sub_svc_id = hsd.parent_sub_svc_id AND
   device.svc_id = dsvc.svc_id AND
   dsvc.svc_nm = p_sub_svc_nm;
   RETURN result;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END fn_get_sibling_svc;
/
--- END of Functions needed for NQ views ---

--- START of views for NQ views ---

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_INTERNET_NQ" ("SUB_ID", "SUB_SVC_ID",
"CM_MAC", "UP_BW", "DOWN_BW", "DEVICE_NICKNAME") AS
select internet.sub_id, internet.sub_svc_id,
fn_get_parm_val(internet.sub_svc_id, 'cm_mac') cm_mac,
up_bw.val up_stream_bw,
down_bw.val down_stream_bw,
fn_get_parm_val(fn_get_sibling_svc(fn_get_pre_req_svc(internet.sub_svc_id,
'commercial_hsd_device')
, 'emta_voice_device'),
'device_nickname') device_nickname
from
sub_svc internet, svc s1, sub_svc_parm up_bw, sub_svc_parm down_bw
where internet.svc_id = s1.svc_id AND
s1.svc_nm = 'selective_docsis_internet_access' AND
internet.sub_svc_status_id != 29 AND
up_bw.sub_svc_id = internet.sub_svc_id AND
down_bw.sub_svc_id = internet.sub_svc_id AND
up_bw.parm_id = get_base_parm_id (s1.svc_id, 'up_stream_bw') AND
down_bw.parm_id = get_base_parm_id (s1.svc_id, 'down_stream_bw');
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_PHONE_NQ" ("SUB_ID", "SUB_SVC_ID",
"TELEPHONE_NUMBER", "PACKAGE_NAME", "IS_PRIMARY", "PRIMARY_VL_TN", "LINE_TYPE") AS
select vl.sub_id, vl.sub_svc_id,
tn.val telephone_number,
fn_get_parm_val(smp_class.sub_svc_id, 'class_of_service_name') class_svc,
fn_get_parm_val(vl.sub_svc_id, 'is_primary') is_primary,
CASE fn_get_parm_val(vl.sub_svc_id, 'is_primary')
when 'y' then null
else fn_get_parm_val(fn_get_assoc_svc_for_svc(vl.sub_svc_id,
'vl_is_secondary_line'), 'telephone_number')
END PRIMARY_VL_TN,
fn_get_parm_val(vl.sub_svc_id, 'shared_line') shared_line
from sub_svc vl, svc s1, sub_svc voice_service_class, svc s2,
sub_svc smp_class, svc s3, sub_svc_parm tn
where
vl.svc_id = s1.svc_id AND
s1.svc_nm = 'primary_voice_line' AND
vl.sub_svc_status_id != 29 AND
tn.sub_svc_id = vl.sub_svc_id AND
tn.parm_id = get_base_parm_id(s1.svc_id, 'telephone_number') AND
voice_service_class.parent_sub_svc_id = vl.parent_sub_svc_id AND
voice_service_class.svc_id = s2.svc_id AND
s2.svc_nm = 'voice_service_class_of_service' AND
smp_class.parent_sub_svc_id = voice_service_class.sub_svc_id AND
smp_class.svc_id = s3.svc_id AND
s3.svc_nm = 'smp_class_of_service_association';
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_USER_NQ" ("SUB_ID", "SUB_SVC_ID",
"FIRST_NAME", "LAST_NAME", "TELEPHONE_NUMBER") AS
select distinct a.*, b.telephone_number from
(select subsvc1.sub_id, subsvc1.sub_svc_id,
fname.val first_name,
lname.val last_name
from sub_svc subsvc1, svc svc1, sub_svc_parm fname, sub_svc_parm lname
where
subsvc1.svc_id = svc1.svc_id AND
subsvc1.sub_svc_status_id != 29 AND
svc1.svc_nm = 'smp_user' AND
fname.sub_svc_id = subsvc1.sub_svc_id AND
fname.parm_id = get_base_parm_id (svc1.svc_id, 'user_first_name') AND
lname.sub_svc_id = subsvc1.sub_svc_id AND
lname.parm_id = get_base_parm_id (svc1.svc_id, 'user_last_name')) a
LEFT OUTER JOIN
(select assoc.sub_svc_id, assoc.assoc_sub_svc_id,
fn_get_parm_val(assoc.assoc_sub_svc_id, 'telephone_number') telephone_number
from sub_svc_assoc assoc, ref_assoc_rule rul, ref_assoc_typ typ,
sub_svc svc1, sub_svc svc2, sub_svc_parm tnparm
where
typ.assoc_typ_nm in ('user_has_voice_service', 'user_shares_voice_line') AND
rul.assoc_typ_id = typ.assoc_typ_id AND
assoc.assoc_rule_id = rul.assoc_rule_id AND
svc1.sub_svc_id = assoc.sub_svc_id AND
svc1.sub_svc_status_id != 29 AND
svc2.sub_svc_id = assoc.assoc_sub_svc_id AND
svc2.sub_svc_status_id != 29 AND
tnparm.sub_svc_id = assoc.assoc_sub_svc_id AND
tnparm.parm_id = get_base_parm_id (svc2.svc_id, 'telephone_number')) b
ON a.sub_svc_id = b.sub_svc_id;
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_PORT_NQ" ("SUB_ID", "SUB_SVC_ID",
"PORT_NUMBER", "DEVICE_NICKNAME", "MAC_ADDR", "TELEPHONE_NUMBER",
"FIRST_NAME", "LAST_NAME") AS
select c.SUB_ID, c.SUB_SVC_ID, c.PORT_NUMBER, c.DEVICE_NICKNAME, c.MAC_ADDR,
c.TELEPHONE_NUMBER, c.FIRST_NAME, c.LAST_NAME from
(select a.SUB_ID, a.SUB_SVC_ID, a.PORT_NUMBER, a.DEVICE_NICKNAME,
a.MAC_ADDR, a.TELEPHONE_NUMBER, b.FIRST_NAME, b.LAST_NAME,
a.left_typ_nm, b.right_typ_nm from
(select port.sub_id, port.sub_svc_id,
fn_get_parm_val(port.sub_svc_id, 'port_number') port_number,
fn_get_parm_val(device.sub_svc_id, 'device_nickname') device_nickname,
fn_get_parm_val(device.sub_svc_id, 'mac_addr') mac_addr,
fn_get_parm_val(sub_svc_assoc.sub_svc_id, 'telephone_number') telephone_number,
sub_svc_assoc.sub_svc_id vl_subsvc_id,
typ.assoc_typ_nm left_typ_nm
from sub_svc port, sub s, svc s1, sub_svc device, svc s2,
sub_svc_assoc, ref_assoc_typ ref_assoc_typ1, ref_assoc_typ ref_assoc_typ2,
ref_assoc_rule, ref_assoc_typ typ
where
port.sub_id = s.sub_id AND
port.svc_id = s1.svc_id AND
s1.svc_nm = 'port' AND
port.sub_svc_status_id != 29 AND
device.parent_sub_svc_id = port.parent_sub_svc_id AND
device.svc_id = s2.svc_id AND
s2.svc_nm = 'emta_voice_device' AND
ref_assoc_typ1.assoc_typ_nm = 'tn_uses_equipment' AND
ref_assoc_typ2.assoc_typ_nm = 'shared_tn_uses_equipment' AND
ref_assoc_rule.assoc_typ_id in (ref_assoc_typ1.assoc_typ_id, ref_assoc_typ2.assoc_typ_id) AND
typ.assoc_typ_id = ref_assoc_rule.assoc_typ_id AND
sub_svc_assoc.assoc_rule_id = ref_assoc_rule.assoc_rule_id AND
sub_svc_assoc.assoc_sub_svc_id = port.sub_svc_id) a
LEFT OUTER JOIN
(select distinct vl.sub_id, sub_svc_assoc.sub_svc_id user_subsvc_id,
sub_svc_assoc.assoc_sub_svc_id,
fn_get_parm_val(sub_svc_assoc.sub_svc_id, 'user_first_name') first_name,
fn_get_parm_val(sub_svc_assoc.sub_svc_id, 'user_last_name') last_name,
typ.assoc_typ_nm right_typ_nm
from sub_svc_assoc, ref_assoc_rule, sub_svc vl, svc vlsvc,
ref_assoc_typ ref_assoc_typ1, ref_assoc_typ ref_assoc_typ2, ref_assoc_typ typ
where
vlsvc.svc_nm = 'primary_voice_line' AND
vl.svc_id = vlsvc.svc_id AND
vl.sub_svc_status_id != 29 AND
ref_assoc_typ1.assoc_typ_nm = 'user_has_voice_service' AND
ref_assoc_typ2.assoc_typ_nm = 'user_shares_voice_line' AND
ref_assoc_rule.assoc_typ_id in (ref_assoc_typ1.assoc_typ_id, ref_assoc_typ2.assoc_typ_id) AND
typ.assoc_typ_id = ref_assoc_rule.assoc_typ_id AND
sub_svc_assoc.assoc_rule_id = ref_assoc_rule.assoc_rule_id AND
sub_svc_assoc.assoc_sub_svc_id = vl.sub_svc_id
) b ON a.sub_id = b.sub_id and a.VL_SUBSVC_ID = b.ASSOC_SUB_SVC_ID
) c
where (left_typ_nm = 'tn_uses_equipment' and right_typ_nm != 'user_shares_voice_line')
or (left_typ_nm = 'shared_tn_uses_equipment');
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_MODEM_NQ" ("SUB_ID", "SUB_SVC_ID", "IS_BOUND",
"CM_MAC", "MODEM_MODEL", "UP_BW", "DOWN_BW") AS
select a.*, b.up_bw, b.down_bw from
(select modem.sub_id, modem.sub_svc_id,
fn_get_parm_val(modem.sub_svc_id, 'is_bound') is_bound,
fn_get_parm_val(modem.sub_svc_id, 'cm_mac') cm_mac,
fn_get_parm_val(modem.sub_svc_id, 'model') modem_model
from
sub_svc modem, svc s1
where modem.svc_id = s1.svc_id AND
s1.svc_nm = 'selective_docsis_cable_modem' AND
modem.sub_svc_status_id != 29) a
LEFT OUTER JOIN
(select sub_svc_depy.prerequisite_sub_svc_id sub_svc_id, sub_svc_depy.dependent_sub_svc_id,
up.val up_bw,
down.val down_bw
from sub_svc_depy, sub_svc depysubsvc, sub_svc modem, svc modemsvc,
sub_svc_parm up, sub_svc_parm down
where modemsvc.svc_nm = 'selective_docsis_cable_modem' AND
modem.svc_id = modemsvc.svc_id AND
modem.sub_svc_status_id != 29 AND
sub_svc_depy.prerequisite_sub_svc_id = modem.sub_svc_id and
depysubsvc.sub_svc_id = sub_svc_depy.dependent_sub_svc_id and
depysubsvc.sub_svc_status_id != 29 AND
up.sub_svc_id = depysubsvc.sub_svc_id AND
down.sub_svc_id = depysubsvc.sub_svc_id AND
up.parm_id = get_base_parm_id (depysubsvc.svc_id, 'up_stream_bw') AND
down.parm_id = get_base_parm_id (depysubsvc.svc_id, 'down_stream_bw')
) b
ON a.sub_svc_id = b.sub_svc_id;
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_HUNT_GRP_NQ" ("SUB_ID", "SUB_SVC_ID",
"GROUP_NAME", "TELEPHONE_NUMBER", "MEMBER_COUNT") AS
select subsvc.sub_id, subsvc.sub_svc_id, c1.val group_name,
fn_get_parm_val(call_grp_pilot.sub_svc_id, 'telephone_number') telephone_number, (
select count(sub_svc_id) from sub_svc call_grp, svc csvc where call_grp.svc_id = csvc.svc_id
and csvc.svc_nm = 'call_group_member' and call_grp.parent_sub_svc_id = subsvc.sub_svc_id
and call_grp.sub_svc_status_id != 29) member_count
from sub_svc subsvc, svc s, sub_svc hunt_grp_svc, sub_svc call_grp_pilot,
svc s2, svc s3, sub_svc_parm c1, parm p1
where subsvc.svc_id = s.svc_id AND
s.svc_nm = 'hunt_group' AND
hunt_grp_svc.parent_sub_svc_id = subsvc.sub_svc_id AND
hunt_grp_svc.svc_id = s2.svc_id AND
hunt_grp_svc.sub_svc_status_id != 29 AND
s2.svc_nm = 'hunt_group_definition' AND
c1.sub_svc_id = hunt_grp_svc.sub_svc_id AND
c1.parm_id = p1.parm_id AND
p1.parm_nm = 'group_name' AND
call_grp_pilot.parent_sub_svc_id = subsvc.sub_svc_id AND
call_grp_pilot.svc_id = s3.svc_id AND
s3.svc_nm = 'call_group_pilot';
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_EMTA_NQ" ("SUB_ID", "SUB_SVC_ID", "DEVICE_NICKNAME", "MAC_ADDR", "CM_MAC", "DEVICE_MODEL", "CURRENT_PORTS", "MAX_PORTS")
AS
SELECT comm_emta.sub_id,
    comm_emta.sub_svc_id,
    fn_get_parm_val (voice_device.sub_svc_id, 'device_nickname') device_nickname,
    fn_get_parm_val (voice_device.sub_svc_id, 'mac_addr') mac_addr,
    fn_get_parm_val (voice_device.sub_svc_id, 'cm_mac') cm_mac,
    fn_get_parm_val (voice_device.sub_svc_id, 'model') device_model,
    (SELECT COUNT (sub_svc.sub_svc_id)
    FROM sub_svc,
      svc
    WHERE sub_svc.svc_id           = svc.svc_id
    AND svc.svc_nm                 = 'port'
    AND sub_svc.sub_svc_status_id != 29
    AND sub_svc.parent_sub_svc_id  = comm_emta.sub_svc_id
    ) current_ports,
    fn_get_parm_val (voice_device.sub_svc_id, 'max_port_num') max_ports
  FROM sub_svc comm_emta,
    svc s1,
    sub_svc voice_device,
    svc s2
  WHERE comm_emta.sub_id = 50000
  AND comm_emta.svc_id              = s1.svc_id
  AND comm_emta.sub_svc_status_id    != 29
  AND s1.svc_nm                       = 'commercial_emta'
  AND voice_device.parent_sub_svc_id  = comm_emta.sub_svc_id
  AND voice_device.sub_svc_status_id != 29
  AND voice_device.svc_id             = s2.svc_id
  AND s2.svc_nm                       = 'emta_voice_device'
  AND voice_device.sub_svc_status_id != 29;
/

CREATE OR REPLACE FORCE VIEW "V_SELECTIVE_SVC_CALL_MEMBER_NQ" ("SUB_ID", "SUB_SVC_ID", "GROUP_NAME",
"TELEPHONE_NUMBER","SEQ", "FIRST_NAME", "LAST_NAME") AS
select subsvc.sub_id, memberassoc.sub_svc_id,
grpnm.val group_nm,
tn.val telephone_number,
fn_get_parm_val(memberassoc.sub_svc_id, 'sequence') seq,
fname.val first_name,
lname.val last_name
from sub_svc subsvc, svc s1, sub_svc_assoc memberassoc,
ref_assoc_typ typ, ref_assoc_rule assocrule, sub_svc hunt_def, svc hunt_def_svc,
sub_svc_parm grpnm, sub_svc primaryvl, sub_svc_parm tn, sub_svc usersubsvc,
ref_assoc_typ typ1, ref_assoc_rule assocrule1, sub_svc_assoc uservs,
sub_svc_parm fname, sub_svc_parm lname, svc usersvc
where
subsvc.svc_id = s1.svc_id AND
s1.svc_nm = 'call_group_member' AND
subsvc.sub_svc_status_id != 29 AND
typ.assoc_typ_nm = 'call_group_has_member' AND
assocrule.assoc_typ_id = typ.assoc_typ_id AND
memberassoc.assoc_rule_id = assocrule.assoc_rule_id AND
memberassoc.sub_svc_id = subsvc.sub_svc_id AND
primaryvl.sub_svc_id = memberassoc.assoc_sub_svc_id AND
tn.sub_svc_id = primaryvl.sub_svc_id AND
tn.parm_id = get_base_parm_id(primaryvl.svc_id, 'telephone_number') AND
hunt_def_svc.svc_nm = 'hunt_group_definition' AND
hunt_def.svc_id = hunt_def_svc.svc_id AND
hunt_def.parent_sub_svc_id = subsvc.parent_sub_svc_id ANd
grpnm.sub_svc_id = hunt_def.sub_svc_id AND
grpnm.parm_id = get_base_parm_id(hunt_def.svc_id, 'group_name') AND
typ1.assoc_typ_nm = 'user_has_voice_service' AND
assocrule1.assoc_typ_id = typ1.assoc_typ_id AND
uservs.assoc_rule_id = assocrule1.assoc_rule_id AND
uservs.assoc_sub_svc_id = memberassoc.assoc_sub_svc_id AND
usersvc.svc_nm = 'smp_user' AND
usersubsvc.svc_id = usersvc.svc_id AND
usersubsvc.sub_svc_id = uservs.sub_svc_id AND
usersubsvc.sub_svc_status_id != 29 AND
fname.sub_svc_id = usersubsvc.sub_svc_id AND
lname.sub_svc_id = usersubsvc.sub_svc_id AND
fname.parm_id = get_base_parm_id(usersubsvc.svc_id, 'user_first_name') AND
lname.parm_id = get_base_parm_id(usersubsvc.svc_id, 'user_last_name');
/

CREATE OR REPLACE FORCE VIEW V_SELECTIVE_SVC_MAXPORT_NQ
(
   PORT_NUMBER,
   PORT_SUB_SVC_ID,
   EMTA_SUB_SVC_ID
)
AS
   SELECT   port_num.val,port.sub_svc_id,PORT.PARENT_SUB_SVC_ID
     FROM   sub_svc port,
            sub_svc_parm port_num
    WHERE   port.svc_id = svcid('port')
            AND port.sub_svc_status_id != 29
            AND port_num.sub_svc_id = port.sub_svc_id
            AND port_num.parm_id =
                  get_base_parm_id (svcid('port'), 'port_number');
/

CREATE OR REPLACE FORCE VIEW V_SUB_FULL_LOAD_QUALIFIER
(
   LOAD_FULL_SUB,
   SUB_ID
)
AS
  select fn_can_load_full_sub(sub_id,100),sub_id from sub;
/

--Customise V_ORDR_NQ_BY_ACCT  view  for adding extrenal key 
CREATE OR REPLACE force VIEW V_ORDR_NQ_BY_ACCT
(SUB_ID, SUB_ORDR_ID,  CREATED_BY, ORDR_STATUS,
 ACCT_NO, CREATED_DTM,
 MODIFIED_DTM,EXTERNAL_KEY)
AS
SELECT
       o.sub_id,o.sub_ordr_id, o.created_by,
       s.status,
       a.acct_no,
       o.created_dtm, o.modified_dtm,o.external_key
  FROM sub_ordr o, ref_status s,
       (SELECT a.sub_id, a.val acct_no
             FROM sub_parm a
            WHERE a.parm_id = get_parm_id ('acct', get_class_id ('SubSpec'))) a
  where o.sub_id = a.sub_id and o.ORDR_STATUS_ID=s.status_id
/
--New views  for new named quires 

--V_SVC_ADNT_IMSI_SEARCH_NQ
CREATE OR REPLACE VIEW V_SVC_ADNT_IMSI_SEARCH_NQ
(SUB_ID, SUB_STATUS, SVC_PROVIDER, SALUTATION, FIRST_NAME, MIDDLE_NAME, LAST_NAME, SUFFIX, ACCOUNT_NO, ADDITIONAL_IMSI)
   AS
     SELECT
          ss.sub_id, r.status,
          fn_get_svc_provider (a.svc_provider_id) as SVC_PROVIDER,
          pkg_sp_subbrief.get_sub_contact_parm (ss.sub_id, 'salutation') as SALUTATION,
          pkg_sp_subbrief.get_sub_contact_parm (ss.sub_id, 'first_name') as FIRST_NAME,
          pkg_sp_subbrief.get_sub_contact_parm (ss.sub_id, 'middle_name') as MIDDLE_NAME,
          pkg_sp_subbrief.get_sub_contact_parm (ss.sub_id, 'last_name') as LAST_NAME,
          pkg_sp_subbrief.get_sub_contact_parm (ss.sub_id, 'suffix') as SUFFIX,
          pkg_sp_subbrief.get_sub_parm (ss.sub_id, 'acct') as ACCOUNT_NO,
          ssp.val as SERVICE_PHONE
      FROM  SUB a, SVC s, PARM p, SUB_SVC ss, SUB_SVC_PARM ssp, REF_STATUS r
    WHERE a.sub_id = ss.sub_id
      AND a.sub_status_id = r.status_id
      AND ss.sub_svc_id = ssp.sub_svc_id
      AND s.svc_id = ss.svc_id
      AND ssp.parm_id = p.parm_id
      AND (p.parm_nm = 'additional_imsi')
      AND (s.svc_nm = 'mobile_sim_card')
      AND (ss.sub_svc_status_id != 29);
/

--V_ORDR_SEARCH_TIBCO_TRANS_ID
CREATE OR REPLACE VIEW V_ORDR_SEARCH_TIBCO_TRANS_ID
(SUB_ORDR_ID, SUB_ID, EXTERNAL_KEY, TIBCO_TRANSACTION_ID, CREATED_BY, CREATED_DTM, MODIFIED_DTM, ORDER_DATE, ORDR_STATUS)
  AS
     SELECT
         so.sub_ordr_id, s.sub_id, so.external_key,  pkg_sp_subbrief.GET_SUB_ORDR_PARM(so.sub_ordr_id,'tibcoTransactionId') as Tibco_Transaction_Id,
         so.created_by, so.created_dtm, so.modified_dtm, so.created_dtm, rs.status
       FROM sub s, sub_ordr so, ref_status rs
     WHERE so.ORDR_STATUS_ID = rs.status_id
       and so.SUB_ID = s.SUB_ID;
/