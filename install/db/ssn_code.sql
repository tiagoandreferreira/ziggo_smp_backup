--==========================================================================
-- FILE INFO
--    $Id: ssn_code.sql,v 1.4 2003/01/16 21:46:07 jimmyz Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * Jimmy Zhao 2002-12-13
--     Modified data to reflect the rogers implementation spec
--==========================================================================

BEGIN
    EXECUTE IMMEDIATE 'DROP PACKAGE pkg_st_ssn';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE
PACKAGE pkg_st_ssn
  IS
--
-- Purpose: Procedures to help with the performance of Service Diagnostics
--
-- MODIFICATION HISTORY
-- Person        Date        Comments
-- ---------     ------      ------------------------------------------
-- Jimmy Zhao  2002-02-13  Module creation

  PROCEDURE group_ssns (sub_service_ids IN t_number,
				ssn_grp_list OUT t_st_ssn_grp);

END; -- Package spec
/

CREATE OR REPLACE
PACKAGE BODY pkg_st_ssn
IS
--
  PROCEDURE group_ssns (sub_service_ids IN t_number,
				ssn_grp_list OUT t_st_ssn_grp)
    IS

    cursor subntwk_id_entry_cursor(id sub_svc_ntwk.sub_svc_id%type) is
    	select subntwk_id
	from sub_svc_ntwk ssn, ref_ntwk_role r
	where sub_svc_id = id
	and ssn.ntwk_role_id = r.ntwk_role_id
	and r.ntwk_role_nm = 'network_entry_point';
    subntwk_id_entry_r subntwk_id_entry_cursor%rowtype;

    cursor subntwk_id_distr_cursor(id sub_svc_ntwk.sub_svc_id%type) is
      select subntwk_id
	from sub_svc_ntwk ssn, ref_ntwk_role r
	where sub_svc_id = id
	and ssn.ntwk_role_id = r.ntwk_role_id
	and r.ntwk_role_nm = 'service_distribution_point';
    subntwk_id_distr_r subntwk_id_distr_cursor%rowtype;

    cursor subntwk_id_cursor (id sub_svc_ntwk.sub_svc_id%type) is
	  select subntwk_id
	  from sub_svc_ntwk ssn
	  where sub_svc_id = id;
    subntwk_id_r subntwk_id_cursor%rowtype;

    cursor parm_val_cursor (subntwk_id_r subntwk.subntwk_id%type) is
    	    select parm_nm, val
	    from subntwk_parm sp, parm p
	    where sp.subntwk_id = subntwk_id_r
	    and sp.parm_id = p.parm_id;
    parm_val_r parm_val_cursor%rowtype;

    cursor ssn_entry_role_nm_cursor (svc_id sub_svc_ntwk.sub_svc_id%type, 
	id subntwk.subntwk_id%type) is
	    select rnr.ntwk_role_nm
	    from sub_svc_ntwk ssn, ref_ntwk_role rnr
	    where ssn.sub_svc_id = svc_id
	    and ssn.subntwk_id = id
	    and ssn.ntwk_role_id = rnr.ntwk_role_id;
    ssn_entry_role_nm_r ssn_entry_role_nm_cursor%rowtype;

    cursor ssn_entry_subntwk_cursor (id subntwk.subntwk_id%type) is
	    select subntwk_nm
	    from subntwk
	    where subntwk_id = id;
    ssn_entry_subntwk_r ssn_entry_subntwk_cursor%rowtype;

    cursor ssn_entry_subntwk_typ_cursor (id subntwk.subntwk_id%type) is
	    select rst.subntwk_typ_nm
	    from subntwk sub, ref_subntwk_typ rst
	    where sub.subntwk_id = id
	    and sub.subntwk_typ_id = rst.subntwk_typ_id;
    ssn_entry_subntwk_typ_r ssn_entry_subntwk_typ_cursor%rowtype;

    cursor ssn_entry_topology_cursor (id subntwk.subntwk_id%type) is
	    select rts.topology_status_nm
	    from subntwk sub, ref_topology_status rts
	    where sub.subntwk_id = id
	    and sub.topology_status_id = rts.topology_status_id;
    ssn_entry_topology_r ssn_entry_topology_cursor%rowtype;

    cursor ssn_entry_ntwk_typ_cursor (id subntwk.subntwk_id%type) is
	    select rnt.ntwk_typ_nm
	    from subntwk sub, ref_subntwk_typ rst, ref_ntwk_typ rnt
	    where sub.subntwk_id = id
	    and sub.subntwk_typ_id = rst.subntwk_typ_id
	    and rst.ntwk_typ_id = rnt.ntwk_typ_id;
    ssn_entry_ntwk_typ_r ssn_entry_ntwk_typ_cursor%rowtype;

    subntwk_id_entry_p subntwk.subntwk_id%type;
    subntwk_id_distribution_p subntwk.subntwk_id%type;
    p_ssn_entry_list t_st_ssn_entry;
    l_ssn_entry_iterator number;
    p_subntwk_prop_list t_st_subntwk_prop;
    l_subntwk_prop_iterator number;
    p_subsvc_list t_number := t_number();
    l_subsvc_iterator number;
    p_subntwk_list t_st_subntwk_subsvc_grp := t_st_subntwk_subsvc_grp();
    l_subntwk_iterator number := 0;
    p_ntwk_entry_point o_st_ssn_entry;
    p_svc_distr_point o_st_ssn_entry;
    p_ssn_list o_ssn_grp;
    count_num number := 0;

  BEGIN
    ssn_grp_list := t_st_ssn_grp();
    p_ssn_list := o_ssn_grp(ssn_grp_list);

    FOR id IN sub_service_ids.FIRST .. sub_service_ids.LAST
    LOOP -- for all passing sub service ids
	-- if there is no ssn for this id, exit
	select count(*) into count_num from sub_svc_ntwk 
	where sub_svc_id = sub_service_ids(id);
	if count_num != 0
	then

	  -- entry point
	  open subntwk_id_entry_cursor(sub_service_ids(id));
        fetch subntwk_id_entry_cursor into subntwk_id_entry_r;
        close subntwk_id_entry_cursor;
        subntwk_id_entry_p := subntwk_id_entry_r.subntwk_id;

	  -- distribution point
	  open subntwk_id_distr_cursor(sub_service_ids(id));
        fetch subntwk_id_distr_cursor into subntwk_id_distr_r;
        close subntwk_id_distr_cursor;
        subntwk_id_distribution_p := subntwk_id_distr_r.subntwk_id;

	  -- go through all subntwk_id with sub_service_ids(id)
	  open subntwk_id_cursor(sub_service_ids(id));
    	  p_subntwk_prop_list := t_st_subntwk_prop();
	  p_ssn_entry_list := t_st_ssn_entry();
	  l_ssn_entry_iterator := 0;
	  l_subsvc_iterator := 0;

	  LOOP
	    fetch subntwk_id_cursor into subntwk_id_r;
	    exit when subntwk_id_cursor%NOTFOUND;

 	    if ((subntwk_id_entry_p is null) OR
		 (subntwk_id_r.subntwk_id != subntwk_id_entry_p))
	    then
	      if ((subntwk_id_distribution_p is null) OR
		   (subntwk_id_r.subntwk_id != subntwk_id_distribution_p))
	      then

		  -- get all parm_val for this subntwk_id
	        open parm_val_cursor(subntwk_id_r.subntwk_id);
	        l_subntwk_prop_iterator := 0;

	        LOOP
	          fetch parm_val_cursor into parm_val_r;
	          exit when parm_val_cursor%NOTFOUND;

	    	    l_subntwk_prop_iterator := l_subntwk_prop_iterator + 1;
	          p_subntwk_prop_list.EXTEND;
	          p_subntwk_prop_list(l_subntwk_prop_iterator) :=
	          o_st_subntwk_prop(parm_val_r.parm_nm, parm_val_r.val);

	        END LOOP;
	        close parm_val_cursor;

		  -- ntwk_type_nm
              open ssn_entry_ntwk_typ_cursor(subntwk_id_r.subntwk_id);
	        fetch ssn_entry_ntwk_typ_cursor into ssn_entry_ntwk_typ_r;
	        close ssn_entry_ntwk_typ_cursor;

	  	  if ssn_entry_ntwk_typ_r.ntwk_typ_nm != 'cpe'
	  	  then

		    -- ntwk_role_nm
	    	    open ssn_entry_role_nm_cursor(sub_service_ids(id),
		    subntwk_id_r.subntwk_id);
	    	    fetch ssn_entry_role_nm_cursor into ssn_entry_role_nm_r;
	    	    close ssn_entry_role_nm_cursor;

		    -- subntwk_id
	    	    open ssn_entry_subntwk_cursor(subntwk_id_r.subntwk_id);
	          fetch ssn_entry_subntwk_cursor into ssn_entry_subntwk_r;
	          close ssn_entry_subntwk_cursor;

		    -- subntwk_type_nm
	    	    open ssn_entry_subntwk_typ_cursor(subntwk_id_r.subntwk_id);
	    	    fetch ssn_entry_subntwk_typ_cursor into ssn_entry_subntwk_typ_r;
	    	    close ssn_entry_subntwk_typ_cursor;

		    -- topology_status_nm
	    	    open ssn_entry_topology_cursor(subntwk_id_r.subntwk_id);
	    	    fetch ssn_entry_topology_cursor into ssn_entry_topology_r;
	    	    close ssn_entry_topology_cursor;

		    -- create a new ssn_entry
  	    	    l_ssn_entry_iterator := l_ssn_entry_iterator + 1;
	    	    p_ssn_entry_list.EXTEND;
	    	    p_ssn_entry_list(l_ssn_entry_iterator) :=
	    	    o_st_ssn_entry(ssn_entry_role_nm_r.ntwk_role_nm, subntwk_id_r.subntwk_id,
	      	ssn_entry_subntwk_r.subntwk_nm, ssn_entry_subntwk_typ_r.subntwk_typ_nm,
	      	ssn_entry_topology_r.topology_status_nm, 
			ssn_entry_ntwk_typ_r.ntwk_typ_nm, p_subntwk_prop_list);

	  	  end if;
	  	end if;
	    end if;
	  END LOOP;
	  close subntwk_id_cursor;

	  -- create entry_point ssn_entry
	  if subntwk_id_entry_p is not null
	  then
	    open ssn_entry_role_nm_cursor(sub_service_ids(id), subntwk_id_entry_p);
	    fetch ssn_entry_role_nm_cursor into ssn_entry_role_nm_r;
	    close ssn_entry_role_nm_cursor;

	    open ssn_entry_subntwk_cursor(subntwk_id_entry_p);
	    fetch ssn_entry_subntwk_cursor into ssn_entry_subntwk_r;
	    close ssn_entry_subntwk_cursor;

	    open ssn_entry_subntwk_typ_cursor(subntwk_id_entry_p);
	    fetch ssn_entry_subntwk_typ_cursor into ssn_entry_subntwk_typ_r;
	    close ssn_entry_subntwk_typ_cursor;

	    open ssn_entry_topology_cursor(subntwk_id_entry_p);
	    fetch ssn_entry_topology_cursor into ssn_entry_topology_r;
	    close ssn_entry_topology_cursor;

	    open ssn_entry_ntwk_typ_cursor(subntwk_id_entry_p);
	    fetch ssn_entry_ntwk_typ_cursor into ssn_entry_ntwk_typ_r;
	    close ssn_entry_ntwk_typ_cursor;

	    p_ntwk_entry_point :=
	    o_st_ssn_entry(ssn_entry_role_nm_r.ntwk_role_nm, subntwk_id_entry_p,
	      ssn_entry_subntwk_r.subntwk_nm, ssn_entry_subntwk_typ_r.subntwk_typ_nm,
	      ssn_entry_topology_r.topology_status_nm, ssn_entry_ntwk_typ_r.ntwk_typ_nm,
	      p_subntwk_prop_list);

	    -- if distribution_point is null, 
	    -- put the entry_point ssn_entry into p_ssn_entry_list
	    if subntwk_id_distribution_p is null
	    then
  	    	l_ssn_entry_iterator := l_ssn_entry_iterator + 1;
	    	p_ssn_entry_list.EXTEND;
	    	p_ssn_entry_list(l_ssn_entry_iterator) := p_ntwk_entry_point;
	    end if;

	  end if;

	  -- create distribution_point ssn_entry
  	  if subntwk_id_distribution_p is not null
	  then -- both entry_point and distribution_point exist
          open ssn_entry_role_nm_cursor(sub_service_ids(id), subntwk_id_distribution_p);
	    fetch ssn_entry_role_nm_cursor into ssn_entry_role_nm_r;
	    close ssn_entry_role_nm_cursor;

	    open ssn_entry_subntwk_cursor(subntwk_id_distribution_p);
	    fetch ssn_entry_subntwk_cursor into ssn_entry_subntwk_r;
	    close ssn_entry_subntwk_cursor;

	    open ssn_entry_subntwk_typ_cursor(subntwk_id_distribution_p);
	    fetch ssn_entry_subntwk_typ_cursor into ssn_entry_subntwk_typ_r;
	    close ssn_entry_subntwk_typ_cursor;

	    open ssn_entry_topology_cursor(subntwk_id_distribution_p);
	    fetch ssn_entry_topology_cursor into ssn_entry_topology_r;
	    close ssn_entry_topology_cursor;

	    open ssn_entry_ntwk_typ_cursor(subntwk_id_distribution_p);
	    fetch ssn_entry_ntwk_typ_cursor into ssn_entry_ntwk_typ_r;
	    close ssn_entry_ntwk_typ_cursor;

	    p_svc_distr_point :=
	    o_st_ssn_entry(ssn_entry_role_nm_r.ntwk_role_nm, subntwk_id_distribution_p,
	      ssn_entry_subntwk_r.subntwk_nm, ssn_entry_subntwk_typ_r.subntwk_typ_nm,
	      ssn_entry_topology_r.topology_status_nm, ssn_entry_ntwk_typ_r.ntwk_typ_nm,
	      p_subntwk_prop_list);

	    -- if entry_point is null, 
	    -- put the distr_point ssn_entry into p_ssn_entry_list
	    if subntwk_id_entry_p is null
	    then
  	    	l_ssn_entry_iterator := l_ssn_entry_iterator + 1;
	    	p_ssn_entry_list.EXTEND;
	    	p_ssn_entry_list(l_ssn_entry_iterator) := p_svc_distr_point;
	    end if;

	  end if;

	  -- call add_element to p_ssn_list.ssn_grp_list
	  p_ssn_list.add_element(p_ntwk_entry_point, p_svc_distr_point, sub_service_ids(id),
	    p_ssn_entry_list);

      end if;
    END LOOP;

    ssn_grp_list := t_st_ssn_grp();
    ssn_grp_list := p_ssn_list.ssn_grp_list;

  EXCEPTION
    WHEN OTHERS THEN
	raise;
  END;

--
END;
/