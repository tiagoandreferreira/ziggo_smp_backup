--==========================================================================
-- FILE INFO
--    $Id: ssn_objs.sql,v 1.2 2003/01/16 15:17:55 jimmyz Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * Jimmy Zhao 2002-12-13
--     Modified data to reflect the rogers implementation spec
--==========================================================================

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_number force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE t_number
 AS TABLE OF number
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_subntwk_prop force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE o_st_subntwk_prop
AS OBJECT
(
   parm_nm varchar2(255),
   val varchar2(1000)
)
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_subntwk_prop force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE t_st_subntwk_prop
 AS TABLE OF o_st_subntwk_prop
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_ssn_entry force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE o_st_ssn_entry
AS OBJECT
(
   ntwk_role_nm varchar2(255),
   subntwk_id number(12),
   subntwk_nm varchar2(255),
   subntwk_type_nm varchar2(100),
   topology_type_nm varchar2(255),
   ntwk_type_nm varchar2(255),
   subntwk_props t_st_subntwk_prop
)
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_ssn_entry force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE t_st_ssn_entry
 AS TABLE OF o_st_ssn_entry
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_subntwk_subsvc_grp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE o_st_subntwk_subsvc_grp
AS OBJECT
(
   subntwks t_st_ssn_entry,
   subsvcs t_number
)
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_subntwk_subsvc_grp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE t_st_subntwk_subsvc_grp
 AS TABLE OF o_st_subntwk_subsvc_grp
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_st_ssn_grp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE o_st_ssn_grp
AS OBJECT
(
   ntwk_entry_point o_st_ssn_entry,
   svc_distr_point o_st_ssn_entry,
   subntwk_groups t_st_subntwk_subsvc_grp
)
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE t_st_ssn_grp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE t_st_ssn_grp
 AS TABLE OF o_st_ssn_grp
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE o_ssn_grp force';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

CREATE OR REPLACE 
TYPE o_ssn_grp
AS OBJECT
(
  ssn_grp_list t_st_ssn_grp,
  MEMBER PROCEDURE add_element (ntwk_entry_point o_st_ssn_entry,
	svc_distr_point o_st_ssn_entry,  subsvc_id number, ssn_entry_list t_st_ssn_entry)

)
/

CREATE OR REPLACE 
TYPE BODY o_ssn_grp
IS
-- Purpose: Briefly explain the functionality of the type body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- Enter procedure, function bodies as shown below
MEMBER PROCEDURE add_element (ntwk_entry_point o_st_ssn_entry,
	svc_distr_point o_st_ssn_entry,  subsvc_id number, ssn_entry_list t_st_ssn_entry) 
IS
  ssn_grp o_st_ssn_grp;
  subntwk_groups_list t_st_subntwk_subsvc_grp;
  subntwk_groups o_st_subntwk_subsvc_grp;
  ssn_entry_list_o t_st_ssn_entry;
  ssn_entry_list_o_len number;
  ssn_entry_list_len number;
  subsvcs_list t_number;
  ssn_entry_o t_st_ssn_entry;
  ssn_entry t_st_ssn_entry;
  length number;
  change_flag number;

  BEGIN
    dbms_output.put_line('PROCEDURE add_element starts here');

    change_flag := 0;
    if ssn_grp_list.FIRST is null 
	then 
	  change_flag := 0;
    else
      for id IN ssn_grp_list.FIRST .. ssn_grp_list.LAST
      LOOP
	  ssn_grp := ssn_grp_list(id);

	  -- check if there is same entry_point and distr_point in the ssn_grp_list
	  -- or, either entry_point or distr_point is null or both are null, 
	  -- put them in the same group
	  if ((ssn_grp.ntwk_entry_point is null OR ssn_grp.ntwk_entry_point is null) AND
		(ntwk_entry_point is null OR svc_distr_point is null)) OR
	     ((ssn_grp.ntwk_entry_point.subntwk_id = ntwk_entry_point.subntwk_id) AND
	      (ssn_grp.svc_distr_point.subntwk_id = svc_distr_point.subntwk_id))
	  then

	    subntwk_groups_list := ssn_grp.subntwk_groups;
	    for id2 IN subntwk_groups_list.FIRST .. subntwk_groups_list.LAST
	    LOOP
		subntwk_groups := subntwk_groups_list(id2);
		ssn_entry_list_o := subntwk_groups.subntwks;

		-- check if they have the same count(*)
	    	select count(*) into ssn_entry_list_o_len from 
		TABLE(CAST(ssn_entry_list_o as t_st_ssn_entry));
		select count(*) into ssn_entry_list_len from 
		TABLE(CAST(ssn_entry_list as t_st_ssn_entry));

		-- check if they have the same subntwk_ids
		if ssn_entry_list_o_len = ssn_entry_list_len
		then
		  change_flag := 1;
		  for ssn_entry_o in (select * from TABLE(CAST(ssn_entry_list_o 
		    as t_st_ssn_entry)))
		  LOOP
		    select count(*) into length
		    from TABLE(CAST(ssn_entry_list as t_st_ssn_entry))
		    where subntwk_id = ssn_entry_o.subntwk_id;

		    if length = 0 
		    then
			change_flag := 0;
			exit;
		    end if;
		  END LOOP;

		  -- if change_flag is 1, means all subntwk_ids are the same
		  if change_flag = 1
	        then
		    subsvcs_list := subntwk_groups.subsvcs;
		    select count(*) into length from 
		    TABLE(CAST(subsvcs_list as t_number));
		    length := length + 1;
		    subsvcs_list.EXTEND;
		    subsvcs_list(length) := subsvc_id;
		    subntwk_groups.subsvcs := subsvcs_list;
      	    subntwk_groups_list(id2) := subntwk_groups;
		    ssn_grp.subntwk_groups := subntwk_groups_list;
		    ssn_grp_list(id) := ssn_grp;
		  end if;
		end if;
	    END LOOP;

	    -- this case, entry_point and distr_point are the same, 
	    -- but subntwk_ids are different
          if change_flag = 0
	    then
		subsvcs_list := t_number();
		ssn_entry_list_len := 1;
		subsvcs_list.EXTEND;
		subsvcs_list(ssn_entry_list_len) := subsvc_id;
		select count(*) into length from 
		TABLE(CAST(subntwk_groups_list as t_st_subntwk_subsvc_grp));
		length := length + 1;
		subntwk_groups_list.EXTEND;
		subntwk_groups_list(length) := o_st_subntwk_subsvc_grp(
		ssn_entry_list, subsvcs_list);
		ssn_grp.subntwk_groups := subntwk_groups_list;
		ssn_grp_list(id) := ssn_grp;
		change_flag := 1;
	    end if;
	  end if;
      END LOOP;
    end if;

    -- this case, there is no same entry_point and distr_point found
    if change_flag = 0
    then
	subsvcs_list := t_number();
	ssn_entry_list_len := 1;
	subsvcs_list.EXTEND;
	subsvcs_list(ssn_entry_list_len) := subsvc_id;
	subntwk_groups_list := t_st_subntwk_subsvc_grp();
	subntwk_groups_list.EXTEND;
	subntwk_groups_list(ssn_entry_list_len) := o_st_subntwk_subsvc_grp(
	  ssn_entry_list, subsvcs_list);
	select count(*) into length from 
	TABLE(CAST(ssn_grp_list as t_st_ssn_grp));
	length := length + 1;
  	ssn_grp_list.EXTEND;
	ssn_grp_list(length) := o_st_ssn_grp(ntwk_entry_point, svc_distr_point,
	  subntwk_groups_list);
    end if;

  END;

END;
/
