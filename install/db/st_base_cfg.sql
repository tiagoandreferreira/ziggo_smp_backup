--============================================================================
--    $Id: st_base_cfg.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

--======================
-- Base Configuration data
--======================
@base_st_load_cm.sql
@base_st_load_all.sql
