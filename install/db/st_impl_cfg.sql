--============================================================================
--    $Id: st_impl_cfg.sql,v 1.1 2007/11/29 21:15:25 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

declare
  n  number;
  n1 number;
begin
  select nvl(max(parm_id), 0) into n from parm;
  loop
    select parm_seq.nextval into n1 from dual;
    exit when n1 >= n;
  end loop;
end;
/

--======================
-- Implementation configuration data
--======================
@./data/Ref_Ntwk_Typ.dat.sql
@./data/Ref_Link_Typ.dat.sql
@./data/Ref_Ntwk_Role.dat.sql
@./data/Ref_Svc_Delivery_Platform.dat.sql
@./data/Ref_Ntwk_Action_Typ.dat.sql
@./data/Ref_Consumable_Rsrc_Typ.dat.sql
@./data/Ref_Ip_Svc_Provider.dat.sql
@./data/Ref_Card_Typ.dat.sql
@./data/Ref_Subntwk_Typ.dat.sql
@./data/Subntwk_Typ_Parm.dat.sql
@./data/Subntwk_Typ_Hierarchy.dat.sql
--@./data/Subntwk_Selectn_Algorithm.dat.sql
@./data/Ref_Subntwk_SelGrp.dat.sql
@./data/Subntwk_Typ_SelGrp.dat.sql
--@./data/Tech_Platform_Typ.dat.sql
--@./data/Tech_Platform_Typ_Parm.dat.sql
@./data/Valid_Link_Conn.dat.sql
@./data/Svc_Supported_Platform.dat.sql
@./data/Ntwk_Action.dat.sql
@./data/Topology_Impact.dat.sql
@./data/Svc_Plat_Subntwk_Typ.dat.sql
@./data/Svc_Platform_Subntwk.dat.sql
@./data/Svc_Plat_Subntwk_Parm.dat.sql
@./data/Nep_Info.dat.sql
@./data/Parm_Chg_Ntwk_Impact.dat.sql
@./data/Ext_Parm_Mapping.dat.sql
@./data/Ntwk_Cmpnt_Chg_Impact.dat.sql
--@./data/Tech_Impact.dat.sql
--@./data/impact_tmplt_list.dat.sql
--@./data/Ntwk_Impact_Parm_Src.dat.sql
--@./data/ImpactTmpltNullify.dat.sql
