--============================================================================
--    $Id: st_load_all.sql,v 1.1 2007/11/29 21:15:27 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

declare
  n  number;
  n1 number;
begin
  select nvl(max(parm_id), 0) into n from parm;
  loop
    select parm_seq.nextval into n1 from dual;
    exit when n1 >= n;
  end loop;
end;
/

@base_st_load_cm.sql
@./data/Ref_Ntwk_Typ.dat.sql
@./data/Ref_Link_Typ.dat.sql
@./data/Ref_Ntwk_Role.dat.sql
---@./data/Ref_Selection_Algorithm.dat.sql
@./data/Ref_Svc_Delivery_Platform.dat.sql
@./data/Ref_Ntwk_Action_Typ.dat.sql
@./data/Ref_Consumable_Rsrc_Typ.dat.sql
---@./data/Ref_Ip_Mgmt_Mode.dat.sql
---@./data/Ref_Ip_Status.dat.sql
@./data/Ref_Ip_Svc_Provider.dat.sql
@./data/Ref_Card_Typ.dat.sql
----@./data/Ref_Communication_Directn.dat.sql
--@./data/Ref_Impactd_Sub_Svc_Status.dat.sql
--@./data/Parm.dat.sql
@./data/Tech_Platform_Typ.dat.sql
@./data/Tech_Platform_Typ_Parm.dat.sql
@./data/Tech_Platform.dat.sql
@./data/Tech_Platform_Parm.dat.sql
@./data/Tech_Platform_Mgmt_Mode.dat.sql
@./data/Ref_Subntwk_Typ.dat.sql
@./data/Subntwk_Typ_Parm.dat.sql
@./data/Subntwk_Typ_Hierarchy.dat.sql
--@./data/Subntwk_Selectn_Algorithm.dat.sql
@./data/Ref_Subntwk_SelGrp.dat.sql
@./data/Subntwk_Typ_SelGrp.dat.sql
@./data/Valid_Link_Conn.dat.sql
@./data/Svc_Supported_Platform.dat.sql
@./data/Ntwk_Action.dat.sql
@./data/Topology_Impact.dat.sql
@./data/Tech_Impact.dat.sql
@./data/impact_tmplt_list.dat.sql
@./data/Svc_Plat_Subntwk_Typ.dat.sql
@./data/Svc_Platform_Subntwk.dat.sql
@./data/Nep_Info.dat.sql
@./data/Subntwk.dat.sql
@./data/Consumable_Rsrc_Mgmt.dat.sql
@./data/Ip_Block.dat.sql
@./data/Ip_Sbnt.dat.sql
@./data/Ip_Address.dat.sql
@./data/Subntwk_Parm.dat.sql
@./data/Link.dat.sql
@./data/Sbnt_Tech_Platform.dat.sql
@./data/Parm_Chg_Ntwk_Impact.dat.sql
@./data/Ntwk_Impact_Parm_Src.dat.sql
@./data/Ext_Parm_Mapping.dat.sql
@./data/Freq_Grp.dat.sql
@./data/Freq.dat.sql
@./data/Freq_Grp_List.dat.sql

@./data/Card.dat.sql
@./data/Port.dat.sql
@./data/Port_Pair.dat.sql
@./data/Port_Pair_Link.dat.sql

@./data/subntwk_supported_platform.dat.sql
---================================
-- Project specific load
---================================
--@./data/Ref_proj_status.dat.sql
---@./data/Ref_task_status.dat.sql
--@./data/Ref_Ntwk_Cmpnt_typ.dat.sql
--@./data/Ref_Topology_Action_Typ.dat.sql
--@./data/Ref_Screen.dat.sql

----@./data/AllowableProjTransitions.dat.sql
----@./data/AllowableTaskTransitions.dat.sql

----@./data/Action_Tmplt.dat.sql
----@./data/Action_Tmplt_Parm.dat.sql

---================================
-- Project specific load (dynamic),
-- should be replaced by the XML converted scripts
---================================
@base_st_load_all.sql

--@./data/Proj_Tmplt.dat.sql
--@./data/Proj_Tmplt_Parm.dat.sql
--@./data/Proj_Tmplt_impl.dat.sql
--@./data/Proj_Tmplt_Impl_Parm.dat.sql
--@./data/Task_Tmplt.dat.sql
--@./data/Task_Tmplt_Parm.dat.sql
--@./data/Action_InTask_Tmplt.dat.sql
--@./data/TaskInProj_Tmplt.dat.sql
--@./data/Task_tmplt_depy.dat.sql
--@./data/Src_Action_Parm.dat.sql
@./data/Ntwk_Cmpnt_Chg_Impact.dat.sql

---==============================================
-- project package to delete project and show project information
---==============================================
----@./data/project.sql

---================================
-- Project specific load
---================================

---==============================================
-- scripts generate from xML
---==============================================
--@./data/project_create_mac_domain.data.sql
