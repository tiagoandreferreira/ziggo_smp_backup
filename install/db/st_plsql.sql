--==========================================================================
-- FILE INFO
--   $Id: st_plsql.sql,v 1.99.4.1.8.2 2011/06/03 16:26:47 mattl Exp $
--
-- DESCRIPTION
--   ParmId - function that returns the id for the given parameter name
--   AddParm- procedure that inserts the parameter and assigns the parm id
--            automatically (max+1)
--
-- REVISION HISTORY:
--   * Based on CVS log
--  * A Siddique    2001-08-20
--   ignore object id from parm table
--==========================================================================

--alter table port_pair_link
--add comments varchar2(1000);
--
--alter table ref_subntwk_typ
--add DFLT_DERIVED_NM_EXPR varchar2(2000);
--============================================================================
-- AddRefTopologyStatus
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefTopologyStatus (i_name IN varchar2) is
    next_topology_status_id number(9);
BEGIN
    select nvl(max(topology_status_id), 1) INTO next_topology_status_id
       FROM ref_topology_status ;
    next_topology_status_id := next_topology_status_id + 1;

    INSERT INTO ref_topology_status(
        topology_status_ID, topology_status_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_topology_status_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating topology_status : ' || i_name);
END;
/

--============================================================================
--  TopologyStatus
--============================================================================
CREATE OR REPLACE FUNCTION TopologyStatus (i_topology_status_name IN varchar2)
   return number is
    gg number;
begin

    select topology_status_id INTO gg from ref_topology_status
      where topology_status_nm = i_topology_status_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddRefLinkTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefLinkTyp (i_name IN varchar2) is
    next_link_typ_id number(9);
--    link_class_id number(9);
BEGIN

--    select class_id into link_class_id from ref_class
--    where class_nm = 'link_typ';

    select nvl(max(link_typ_id), 1) INTO next_link_typ_id
       FROM ref_link_typ ;
    next_link_typ_id := next_link_typ_id + 1;

    INSERT INTO ref_link_typ(
        link_typ_ID, link_typ_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_link_typ_id, i_name, SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating link_typ : ' || i_name);
END;
/

--============================================================================
--  LinkTyp
--============================================================================
CREATE OR REPLACE FUNCTION LinkTyp (i_link_typ_name IN varchar2)
   return number is
    gg number;
begin

    select link_typ_id INTO gg from ref_link_typ
      where link_typ_nm = i_link_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/




--============================================================================
-- AddRefMgmtMode
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefMgmtMode (i_name IN varchar2) is
    next_mgmt_mode_id number(9);
BEGIN
    select nvl(max(mgmt_mode_id), 1) INTO next_mgmt_mode_id
       FROM ref_mgmt_mode ;
    next_mgmt_mode_id := next_mgmt_mode_id + 1;

    INSERT INTO ref_mgmt_mode(
        mgmt_mode_ID, mgmt_mode_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_mgmt_mode_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating mgmt_mode : ' || i_name);
END;
/

--============================================================================
--  MgmtMode
--============================================================================
CREATE OR REPLACE FUNCTION MgmtMode (i_mgmt_mode_name IN varchar2)
   return number is
    gg number;
begin

    select mgmt_mode_id INTO gg from ref_mgmt_mode
      where mgmt_mode_nm = i_mgmt_mode_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddRefNtwkRole
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefNtwkRole (i_name IN varchar2) is
    next_ntwk_role_id number(9);
BEGIN
    select nvl(max(ntwk_role_id), 1) INTO next_ntwk_role_id
       FROM ref_ntwk_role ;
    next_ntwk_role_id := next_ntwk_role_id + 1;

    INSERT INTO ref_ntwk_role(
        ntwk_role_ID, ntwk_role_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_ntwk_role_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ntwk_role : ' || i_name);
END;
/

--============================================================================
--  NtwkRole
--============================================================================
CREATE OR REPLACE FUNCTION NtwkRole (i_ntwk_role_name IN varchar2)
   return number is
    gg number;
begin

    select ntwk_role_id INTO gg from ref_ntwk_role
      where ntwk_role_nm = i_ntwk_role_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/



--============================================================================
-- AddRefNtwkTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefNtwkTyp (i_name IN varchar2) is
    next_ntwk_typ_id number(9);
BEGIN
    select nvl(max(ntwk_typ_id), 1) INTO next_ntwk_typ_id
       FROM ref_ntwk_typ ;
    next_ntwk_typ_id := next_ntwk_typ_id + 1;

    INSERT INTO ref_ntwk_typ(
        ntwk_typ_ID, ntwk_typ_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_ntwk_typ_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ntwk_typ : ' || i_name);
END;
/

--============================================================================
--  NtwkTypId
--============================================================================
CREATE OR REPLACE FUNCTION NtwkTypId (i_ntwk_typ_name IN varchar2)
   return number is
    gg number;
begin

    select ntwk_typ_id INTO gg from ref_ntwk_typ
      where ntwk_typ_nm = i_ntwk_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddRefSvcDeliveryPlatform
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefSvcDeliveryPlatform (i_name IN varchar2) is
    next_svc_delivery_platform_id number(9);
BEGIN
    select nvl(max(svc_delivery_plat_id), 1)
       INTO next_svc_delivery_platform_id
       FROM ref_svc_delivery_plat ;
    next_svc_delivery_platform_id := next_svc_delivery_platform_id + 1;

    INSERT INTO ref_svc_delivery_plat(
        svc_delivery_plat_ID, svc_delivery_plat_NM, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_svc_delivery_platform_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating svc_delivery_platform : ' ||
         i_name);
END;
/

--============================================================================
--  SvcDeliveryPlatform
--============================================================================
CREATE OR REPLACE FUNCTION SvcDeliveryPlatform (i_svc_delivery_platform_name IN varchar2)
   return number is
    gg number;
begin

    select svc_delivery_plat_id INTO gg from ref_svc_delivery_plat
      where svc_delivery_plat_nm = i_svc_delivery_platform_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  SubntwkTyp
--============================================================================
CREATE OR REPLACE FUNCTION SubntwkTyp (i_subntwk_typ_name IN varchar2)
   return number is
    gg number;
begin

    select subntwk_typ_id INTO gg from ref_subntwk_typ
      where subntwk_typ_nm = i_subntwk_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddTechPlatformTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatformTyp (i_name IN varchar2,
                                                i_version_name IN varchar2) is
    next_tech_platform_typ_id number(9);
BEGIN
    select nvl(max(tech_platform_typ_id), 1) INTO next_tech_platform_typ_id
       FROM tech_platform_typ ;
    next_tech_platform_typ_id := next_tech_platform_typ_id + 1;

    INSERT INTO tech_platform_typ(
        tech_platform_typ_ID, tech_platform_typ_NM, software_version,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_tech_platform_typ_id, i_name, i_version_name,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform_typ : ' || i_name);
END;
/

--============================================================================
--  TechPlatformTyp
--============================================================================
CREATE OR REPLACE FUNCTION TechPlatformTyp (i_tech_platform_typ_name IN varchar2)
   return number is
    gg number;
begin

    select tech_platform_typ_id INTO gg from tech_platform_typ
      where tech_platform_typ_nm = i_tech_platform_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/





--============================================================================
-- AddTechPlatformMgmtMode
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatformMgmtMode (
                                       i_tech_platform_id IN NUMBER,
                                       i_mgmt_mode_id IN number) is
BEGIN
    INSERT INTO tech_platform_mgmt_mode(
         TECH_PLATFORM_ID, MGMT_MODE_ID,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_tech_platform_id, i_mgmt_mode_id,
         SYSDATE,'albeda', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform_mgmt_mode : ' ||
         i_tech_platform_id || ' ' || i_mgmt_mode_id);
END;
/
--============================================================================
--  TechPlatform
--============================================================================
CREATE OR REPLACE FUNCTION TechPlatform (i_tech_platform_name IN varchar2)
   return number is
    gg number;
begin

    select tech_platform_id INTO gg from tech_platform
      where techn_platform_nm = i_tech_platform_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddTechPlatform
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatform (i_name IN varchar2,
                                             i_platform_typ_id IN number)
                                             is
    next_tech_platform_id number(9);
BEGIN
    select nvl(max(tech_platform_id), 1) INTO next_tech_platform_id
       FROM tech_platform ;
    next_tech_platform_id := next_tech_platform_id + 1;

    INSERT INTO tech_platform(
        tech_platform_ID, tech_platform_typ_id,
        techn_platform_nm,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY, TOPOLOGY_STATUS_ID)
    VALUES(next_tech_platform_id, i_platform_typ_id,
            i_name, SYSDATE,'albeda', SYSDATE,'INIT', TopologyStatus('in_service'));
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform : ' || i_name );
END;
/

--============================================================================
-- AddSubntwkSelectionAlgorithm
--============================================================================
--CREATE OR REPLACE PROCEDURE AddSubntwkSelectionAlgorithm (
--                                             i_subntwk_typ_id IN number,
--                                             i_selection_id IN  number)
--                                            is
--BEGIN
--    INSERT INTO SUBNTWK_SEL_ALGORITHM(
--        selection_algorithm_id, subntwk_typ_id,
--         CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--    VALUES(i_selection_id, i_subntwk_typ_id,
--           SYSDATE,'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating subntwk_selection_algorithm : ' ||
--          i_subntwk_typ_id || ' ' || i_selection_id );
--END;
--/

--============================================================================
-- AddSTMParm
--============================================================================
--CREATE OR REPLACE PROCEDURE AddSTMParm (i_parm_nm IN varchar2,
--                                        i_data_typ_nm IN varchar2,
--                                        i_classid  IN number,
--                                        i_reserved IN char) is
--    next_parm_id number(9);
--    stm_parm_src_id number(9);
--BEGIN
--    select nvl(max(parm_id), 1) INTO next_parm_id
--       FROM parm ;
--    next_parm_id := next_parm_id + 1;
--
--    INSERT INTO Parm(
--        parm_id, parm_nm, parm_src_nm, data_typ_nm, class_id,is_reserved,
--         CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--   VALUES(next_parm_id, i_parm_nm, 'stm', i_data_typ_nm, i_classid, i_reserved,
--          SYSDATE,'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating parm : ' ||
--          i_parm_nm || ' ' || i_data_typ_nm || SQLERRM);
--END;
--/

--============================================================================
-- AddSubntwkTypParm
--============================================================================
--CREATE OR REPLACE PROCEDURE AddSubntwkTypParm (
--                                             i_subntwk_typ_id IN number,
--                                             i_parm_nm IN  varchar2,
--                                             i_data_typ_nm IN varchar2,
--                                             i_is_unique IN char,
--                                             i_is_required IN char,
--                                             i_is_read_only IN char default 'n'
--)
--                                            is
--    next_parm_id number(9);
--    this_parm_id number(9);
--    subntwk_typ_class_id number(9);
--BEGIN
--    select class_id INTO subntwk_typ_class_id
--       FROM ref_class where class_nm = 'subntwk_typ' ;
--
--    select count(*) into this_parm_id from parm
--      where parm_nm = i_parm_nm and
--      class_id = subntwk_typ_class_id;
--
--    if (this_parm_id = 0)
--    then
----        select nvl(max(parm_id), 1) INTO next_parm_id
----          FROM parm ;
----       next_parm_id := next_parm_id + 1;
--
--        Add_Parm( p_parm_nm => i_parm_nm,
--                 p_parm_src_nm => 'stm',
--                 p_data_typ_nm => i_data_typ_nm,
--                 p_is_unique => i_is_unique,
--                 p_is_required => i_is_required,
--                 p_class_id => subntwk_typ_class_id,
--                 p_created_by => 'INIT',
--                 p_is_read_only => i_is_read_only );
--
--        --INSERT INTO Parm(
--        --    parm_id, parm_nm, parm_src_nm, data_typ_nm, class_id,is_unique,
--        --     is_required,
--        --     CREATED_DTM, CREATED_BY,
--        --     MODIFIED_DTM, MODIFIED_BY)
--        --VALUES(next_parm_id, i_parm_nm, 'stm', i_data_typ_nm,
--        --       subntwk_typ_class_id, i_is_unique,i_is_required,
--        --       SYSDATE,'albeda', SYSDATE,'INIT');
--        select parm_id into this_parm_id from parm
--            where parm_nm = i_parm_nm and
--            class_id = subntwk_typ_class_id;
--            next_parm_id := this_parm_id;
--    else
--        select parm_id into this_parm_id from parm
--             where parm_nm = i_parm_nm and
--             class_id = subntwk_typ_class_id;
--        next_parm_id := this_parm_id;
--    end if;
--
--    INSERT INTO Subntwk_Typ_Parm (SUBNTWK_TYP_ID, PARM_ID,CREATED_DTM,
--           CREATED_BY, MODIFIED_DTM,MODIFIED_BY)
--    VALUES(i_subntwk_typ_id, next_parm_id, SYSDATE, 'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating SubntwkTyp parm : ' ||
--          i_subntwk_typ_id || ' ' || i_parm_nm || ' ' || SQLERRM );
--END;
--/

--============================================================================
--  ParmId
--============================================================================
--CREATE OR REPLACE FUNCTION ParmId (i_parm_name IN varchar2)
--   return number is
--    gg number;
--begin
--
--    select parm_id INTO gg from parm
--      where parm_nm = i_parm_name ;
--    return gg;
--
--EXCEPTION
--  when no_data_found then
--    return 0;
--END;
--/

--============================================================================
-- AddSvcSupportedPlatform
--============================================================================
CREATE OR REPLACE PROCEDURE AddSvcSupportedPlatform (
                                             i_platform_id IN number,
                                             i_svc_id IN  number,
                                             i_priority IN number,
                                             jmf_expr IN varchar2,
                                             ip_mgmt_mode IN varchar2) is

   next_svc_supported_platform_id number(9);
BEGIN
    select nvl(max(svc_supported_platform_id), 1)
       INTO next_svc_supported_platform_id
       FROM svc_supported_platform ;
    next_svc_supported_platform_id := next_svc_supported_platform_id + 1;

    INSERT INTO svc_supported_platform(
        SVC_SUPPORTED_PLATFORM_ID, SVC_DELIVERY_PLAT_ID,
         SVC_ID, PRIORITY, JMF_EXPR, 
         CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_svc_supported_platform_id,
           i_platform_id,i_svc_id, i_priority,
           jmf_expr, 
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating svc_supported_platform : ' ||
          i_platform_id || ' ' || i_svc_id );
END;
/

--============================================================================
-- AddSubntwk
--============================================================================
CREATE OR REPLACE PROCEDURE AddSubntwk (i_subntwk_name IN varchar2,
                                        i_subntwk_typ_id IN number,
                                        i_parent_subntwk_id IN number,
                                        i_topology_status_id IN number
                                        ) is
   next_subntwkid number(9);
BEGIN
    INSERT INTO subntwk(
        SUBNTWK_ID,
        PARENT_SUBNTWK_ID,
        SUBNTWK_NM, TOPOLOGY_STATUS_ID, SUBNTWK_TYP_ID,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(SUBNTWK_SEQ.nextval,
           i_parent_subntwk_id, i_subntwk_name,
           i_topology_status_id, i_subntwk_typ_id,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating subntwk_name : ' ||
          i_subntwk_name  || SQLERRM);
END;
/

--============================================================================
--  Subntwk
--============================================================================
CREATE OR REPLACE FUNCTION SubntwkId (i_subntwk_name IN varchar2)
   return number is
    gg number;
begin
    select subntwk_id INTO gg from subntwk
      where subntwk_nm = i_subntwk_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/


--============================================================================
-- AddLink
--============================================================================
CREATE OR REPLACE PROCEDURE AddLink (i_link_name IN varchar2,
                                        i_link_typ_id IN number,
                                        i_from_subntwk_id IN number,
                                        i_to_subntwk_id IN number
                                        ) is
   next_link_id number(9);
   this_link_id number(9);
BEGIN

    select count(*) into this_link_id from link where
    FROM_SUBNTWK_ID = i_from_subntwk_id and
    TO_SUBNTWK_ID = i_to_subntwk_id ;

    if (this_link_id = 0 )
    then

        INSERT INTO LINK(LINK_ID,
            FROM_SUBNTWK_ID, LINK_TYP_ID, LINK_NM, TO_SUBNTWK_ID,
            TOPOLOGY_STATUS_ID,
             CREATED_DTM, CREATED_BY,
             MODIFIED_DTM, MODIFIED_BY)
        VALUES(LINK_SEQ.nextval, i_from_subntwk_id, i_link_typ_id,
               i_link_name, i_to_subntwk_id,TopologyStatus('in_service'),
               SYSDATE,'albeda', SYSDATE,'INIT');
    end if;
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating link : ' || SQLERRM ||
          i_link_name );
END;
/

--============================================================================
--  Link
--============================================================================
CREATE OR REPLACE FUNCTION LinkId (i_link_name IN varchar2)
   return number is
    gg number;
begin
    select link_id INTO gg from link
      where link_nm = i_link_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddSubntwkParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddSubntwkParm (i_subntwk_id IN varchar2,
                                        i_parm_nm IN varchar2,
                                        i_parm_val IN varchar2
                                        ) is
   next_parm_id number(9);
   this_parm_id number(9);
   subntwk_typ_class_id number(9);
   subntwk_parm_id number(9);
   this_subntwk_typ_id number(9);
BEGIN
    select class_id INTO subntwk_typ_class_id
       FROM ref_class where class_nm = 'subntwk_typ' ;

    select count(*) into this_parm_id from parm
      where parm_nm = i_parm_nm and
      class_id = subntwk_typ_class_id;

    if (this_parm_id = 0)
    then
        select nvl(max(parm_id), 1) INTO next_parm_id
          FROM parm ;
        next_parm_id := next_parm_id + 1;


        INSERT INTO Parm(
            parm_id, parm_nm, data_typ_nm, class_id,is_unique,
             is_required,
             CREATED_DTM, CREATED_BY,
             MODIFIED_DTM, MODIFIED_BY)
        VALUES(next_parm_id, i_parm_nm, 'String',
               subntwk_typ_class_id, 'n','n',
               SYSDATE,'albeda', SYSDATE,'INIT');
        select parm_id into this_parm_id from parm
            where parm_nm = i_parm_nm and
            class_id = subntwk_typ_class_id;
            next_parm_id := this_parm_id;
    else
        select parm_id into this_parm_id from parm
             where parm_nm = i_parm_nm and
             class_id = subntwk_typ_class_id;
        next_parm_id := this_parm_id;
    end if;

    select r.subntwk_typ_id into this_subntwk_typ_id from
       ref_subntwk_typ r, subntwk s
       where s.subntwk_typ_id = r.subntwk_typ_id and
       s.SUBNTWK_ID = i_subntwk_id;

    select parm_id into subntwk_parm_id from parm p, ref_class r where
       parm_nm = i_parm_nm and
       p.class_id = r.class_id and r.class_nm = 'subntwk_typ';
    INSERT INTO SUBNTWK_PARM(
        SUBNTWK_ID, PARM_ID, VAL,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_subntwk_id, subntwk_parm_id,
           i_parm_val,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating SubntwkParm : ' ||
          i_subntwk_id || ' ' || i_parm_nm || ' ' ||SQLERRM);
END;
/
--============================================================================
-- AddTmplt
--============================================================================
--CREATE OR REPLACE PROCEDURE AddTmplt (i_tmplt_nm IN varchar2,
--                                      i_tmplt_id IN number default 0) is
--    next_tmplt_id number(9);
--BEGIN
--    if (i_tmplt_id = 0)
--    then
--
--    select nvl(max(tmplt_id), 1000) INTO next_tmplt_id
--       FROM tmplt ;
--    next_tmplt_id := next_tmplt_id + 1;
--    else
--       next_tmplt_id := i_tmplt_id;
--    end if;
--
--    INSERT INTO tmplt(
--        tmplt_id, tmplt_nm,
--         CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--    VALUES(next_tmplt_id, i_tmplt_nm,
--           SYSDATE,'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating tmplt : ' ||
--          i_tmplt_nm ||SQLERRM );
--END;
--/



--============================================================================
--  SvcSupportedPlatform
--============================================================================
CREATE OR REPLACE FUNCTION SvcSupportedPlatform (
                                   i_svc_delivery_platform_id IN number,
                                   i_svc_id in number)
   return number is
    gg number;
begin

    select svc_supported_platform_id INTO gg from svc_supported_platform
      where svc_delivery_plat_id = i_svc_delivery_platform_id and
            svc_id = i_svc_id ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/



--============================================================================
--  TechPlatform
--============================================================================
CREATE OR REPLACE FUNCTION TechPlatformId (i_tech_platform_name IN varchar2)
   return number is
    gg number;
begin
    select tech_platform_id INTO gg from tech_platform
      where techn_platform_nm = i_tech_platform_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  STMParmId
--============================================================================
CREATE OR REPLACE FUNCTION STMParmId (i_parm_name IN varchar2,
                                      i_class_id in number)
   return number is
    gg number;
begin
    select parm_id INTO gg from parm
      where parm_nm = i_parm_name and
      class_id = i_class_id  and
      rownum < 2;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--CREATE OR REPLACE FUNCTION SvcId (i_parm_name IN varchar2) return number is
--    gg number;
--begin
--
--    select svc_id INTO gg from Svc where svc_nm = i_parm_name ;
--    return gg;
--
--EXCEPTION
--  when no_data_found then
--    return 0;
--END;
--/
--
--CREATE OR REPLACE FUNCTION SvcActionId (i_svc_name IN varchar2,
--                                      i_action_name IN varchar2)
--    return number is
--    gg number;
--begin
--
--    select svc_action_id INTO gg from svc_action where
--        svc_id = SvcId (i_svc_name) AND
--        svc_action_nm = i_action_name ;
--
--    return gg;
--
--EXCEPTION
--  when no_data_found then
--    return 0;
--END;
--/

--============================================================================
-- Create sequences
-- Will be removed when added to the main file
--============================================================================

BEGIN
    EXECUTE IMMEDIATE 'DROP SEQUENCE IP_ADDR_SEQ';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP SEQUENCE IP_BLOCK_SEQ';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP SEQUENCE IP_SBNT_SEQ';
EXCEPTION
 WHEN others THEN
    NULL;
END;
/

--============================================================================
-- Create sequences
--============================================================================

CREATE SEQUENCE IP_ADDR_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 10  ORDER CYCLE;

CREATE SEQUENCE IP_BLOCK_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 10  ORDER CYCLE;

CREATE SEQUENCE IP_SBNT_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 10  ORDER CYCLE;

--============================================================================
-- AddIpAddress
--============================================================================
CREATE OR REPLACE PROCEDURE AddIpAddress (i_ip_addr IN varchar2,
                                        i_assigned_subntwk_id IN number,
                                        i_ip_sbnt_id IN number,
                                        i_ip_status_nm IN varchar2
                                        )
IS
BEGIN
    INSERT INTO ip_addr(
	IP_ADDR,
	ASSIGNED_SUBNTWK_ID,
	IP_SBNT_ID,
	IP_STATUS_NM,
        CREATED_DTM, CREATED_BY,
        MODIFIED_DTM, MODIFIED_BY)
    VALUES(
	i_ip_addr,
        i_assigned_subntwk_id,
	i_ip_sbnt_id,
        i_ip_status_nm,
        SYSDATE,'Harry', SYSDATE,'INIT');
    EXCEPTION
    	when others then
       	Dbms_output.put_line('Error Creating : ' ||
          	i_ip_addr  || SQLERRM);
END;
/

--============================================================================
-- AddIpSbnt
--============================================================================

CREATE OR REPLACE PROCEDURE AddIpSbnt (
                                        i_ip_block_id IN number,
					i_ip_sbnt_addr IN varchar2,
					i_ip_sbnt_msk IN varchar2,
					i_ip_sbnt_gateway IN varchar2,
					i_ip_broadcast_addr IN varchar2,
					i_ip_max_hosts IN number,
                                        i_managing_subntwk_id IN number,
					i_svc_provider_id IN number,
                                        i_ip_status_nm IN varchar2,
					i_descr IN varchar2
                                        )
IS
BEGIN
    INSERT INTO ip_sbnt(
	IP_SBNT_ID,
	IP_BLOCK_ID,
	IP_SBNT_ADDR,
	IP_SBNT_MSK,
	IP_SBNT_GATEWAY_ADDR,
	IP_BROADCAST_ADDR,
	IP_MAX_HOSTS,
	MANAGING_SUBNTWK_ID,
	IP_SVC_PROVIDER_ID,
	IP_STATUS_NM,
	DESCR,
        CREATED_DTM, CREATED_BY,
        MODIFIED_DTM, MODIFIED_BY)
    VALUES(
	IP_SBNT_SEQ.nextval,
	i_ip_block_id,
	i_ip_sbnt_addr,
	i_ip_sbnt_msk,
	i_ip_sbnt_gateway,
	i_ip_broadcast_addr,
	i_ip_max_hosts,
	i_managing_subntwk_id,
	i_svc_provider_id,
	i_ip_status_nm,
	i_descr,
        SYSDATE,'Harry', SYSDATE,'INIT');
    EXCEPTION
    	when others then
       	Dbms_output.put_line('Error Creating : ' ||
          	i_ip_sbnt_addr  || SQLERRM);
END;
/

--============================================================================
-- AddIpBlock
--============================================================================

CREATE OR REPLACE PROCEDURE AddIpBlock(
                                        i_assignable_hosts IN number,
                                        i_managing_subntwk_id IN number,
                                        i_ip_status_nm IN varchar2,
					i_mask IN varchar2,
					i_start_ip_addr IN varchar2,
					i_ip_block_nm IN varchar2
                                        )
IS
BEGIN
    INSERT INTO ip_block(
	IP_BLOCK_ID,
	ASSIGNABLE_HOSTS,
	MANAGING_SUBNTWK_ID,
	IP_STATUS_NM,
	MASK,
	START_IP_ADDR,
	IP_BLOCK_NM,
        CREATED_DTM, CREATED_BY,
        MODIFIED_DTM, MODIFIED_BY)
    VALUES(
	IP_BLOCK_SEQ.nextval,
	i_assignable_hosts,
        i_managing_subntwk_id,
        i_ip_status_nm,
	i_mask,
	i_start_ip_addr,
	i_ip_block_nm,
        SYSDATE,'Harry', SYSDATE,'INIT');
    EXCEPTION
    	when others then
       	Dbms_output.put_line('Error Creating : ' ||
          	i_ip_block_nm|| SQLERRM);
END;
/

--============================================================================
--  Subnt
--============================================================================
CREATE OR REPLACE FUNCTION SbntId (i_sbnt_addr IN varchar2)
   return number is
    gg number;
begin
    select ip_sbnt_id INTO gg from IP_SBNT
      where ip_sbnt_addr = i_sbnt_addr ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  IpBlockIdByName
--============================================================================
CREATE OR REPLACE FUNCTION IpBlockIdByName (i_ip_block_nm IN varchar2)
   return number is
    gg number;
begin
    select IP_BLOCK_ID INTO gg from IP_BLOCK
      where ip_block_nm = i_ip_block_nm ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
--============================================================================
-- AddProjTmplt
--============================================================================
CREATE OR REPLACE PROCEDURE AddProjTmplt (i_name IN varchar2,
                                  i_desc IN varchar2,
				  i_ver_desc IN varchar2) is
BEGIN
    INSERT INTO proj_tmplt(
        PROJ_TMPLT_NM, PROJ_TMPLT_VER, PROJ_TMPLT_DESCR, VER_DESCR, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_name, ProjTmpltVer(i_name)+1, i_desc, i_ver_desc, SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating project template : ' || i_name);
END;
/

--============================================================================
--  TopologyStatus
--============================================================================
CREATE OR REPLACE FUNCTION TopologyStatus (i_topology_status_name IN varchar2)
   return number is
    gg number;
begin

    select topology_status_id INTO gg from ref_topology_status
      where topology_status_nm = i_topology_status_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddRefLinkTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefLinkTyp (i_name IN varchar2) is
    next_link_typ_id number(9);
--    link_class_id number(9);
BEGIN

--    select class_id into link_class_id from ref_class
--    where class_nm = 'link_typ';

    select nvl(max(link_typ_id), 1) INTO next_link_typ_id
       FROM ref_link_typ ;
    next_link_typ_id := next_link_typ_id + 1;

    INSERT INTO ref_link_typ(
        link_typ_ID, link_typ_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_link_typ_id, i_name, SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating link_typ : ' || i_name);
END;
/

--============================================================================
--  LinkTyp
--============================================================================
CREATE OR REPLACE FUNCTION LinkTyp (i_link_typ_name IN varchar2)
   return number is
    gg number;
begin

    select link_typ_id INTO gg from ref_link_typ
      where link_typ_nm = i_link_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/



--============================================================================
-- AddRefMgmtMode
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefMgmtMode (i_name IN varchar2) is
    next_mgmt_mode_id number(9);
BEGIN
    select nvl(max(mgmt_mode_id), 1) INTO next_mgmt_mode_id
       FROM ref_mgmt_mode ;
    next_mgmt_mode_id := next_mgmt_mode_id + 1;

    INSERT INTO ref_mgmt_mode(
        mgmt_mode_ID, mgmt_mode_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_mgmt_mode_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating mgmt_mode : ' || i_name);
END;
/

--============================================================================
--  MgmtMode
--============================================================================
CREATE OR REPLACE FUNCTION MgmtMode (i_mgmt_mode_name IN varchar2)
   return number is
    gg number;
begin

    select mgmt_mode_id INTO gg from ref_mgmt_mode
      where mgmt_mode_nm = i_mgmt_mode_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddRefNtwkRole
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefNtwkRole (i_name IN varchar2) is
    next_ntwk_role_id number(9);
BEGIN
    select nvl(max(ntwk_role_id), 1) INTO next_ntwk_role_id
       FROM ref_ntwk_role ;
    next_ntwk_role_id := next_ntwk_role_id + 1;

    INSERT INTO ref_ntwk_role(
        ntwk_role_ID, ntwk_role_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_ntwk_role_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ntwk_role : ' || i_name);
END;
/

--============================================================================
--  NtwkRole
--============================================================================
CREATE OR REPLACE FUNCTION NtwkRole (i_ntwk_role_name IN varchar2)
   return number is
    gg number;
begin

    select ntwk_role_id INTO gg from ref_ntwk_role
      where ntwk_role_nm = i_ntwk_role_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/



--============================================================================
-- AddRefNtwkTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefNtwkTyp (i_name IN varchar2) is
    next_ntwk_typ_id number(9);
BEGIN
    select nvl(max(ntwk_typ_id), 1) INTO next_ntwk_typ_id
       FROM ref_ntwk_typ ;
    next_ntwk_typ_id := next_ntwk_typ_id + 1;

    INSERT INTO ref_ntwk_typ(
        ntwk_typ_ID, ntwk_typ_NM, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_ntwk_typ_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ntwk_typ : ' || i_name);
END;
/

--============================================================================
--  NtwkTypId
--============================================================================
CREATE OR REPLACE FUNCTION NtwkTypId (i_ntwk_typ_name IN varchar2)
   return number is
    gg number;
begin

    select ntwk_typ_id INTO gg from ref_ntwk_typ
      where ntwk_typ_nm = i_ntwk_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddRefSvcDeliveryPlatform
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefSvcDeliveryPlatform (i_name IN varchar2) is
    next_svc_delivery_platform_id number(9);
BEGIN
    select nvl(max(svc_delivery_plat_id), 1)
       INTO next_svc_delivery_platform_id
       FROM ref_svc_delivery_plat ;
    next_svc_delivery_platform_id := next_svc_delivery_platform_id + 1;

    INSERT INTO ref_svc_delivery_plat(
        svc_delivery_plat_ID, svc_delivery_plat_NM, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_svc_delivery_platform_id, i_name,SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating svc_delivery_platform : ' ||
         i_name);
END;
/

--============================================================================
--  SvcDeliveryPlatform
--============================================================================
CREATE OR REPLACE FUNCTION SvcDeliveryPlatform (i_svc_delivery_platform_name IN varchar2)
   return number is
    gg number;
begin

    select svc_delivery_plat_id INTO gg from ref_svc_delivery_plat
      where svc_delivery_plat_nm = i_svc_delivery_platform_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/
--============================================================================
--  SubntwkTyp
--============================================================================
CREATE OR REPLACE FUNCTION SubntwkTyp (i_subntwk_typ_name IN varchar2)
   return number is
    gg number;
begin

    select subntwk_typ_id INTO gg from ref_subntwk_typ
      where subntwk_typ_nm = i_subntwk_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddTechPlatformTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatformTyp (i_name IN varchar2,
                                                i_version_name IN varchar2) is
    next_tech_platform_typ_id number(9);
BEGIN
    select nvl(max(tech_platform_typ_id), 1) INTO next_tech_platform_typ_id
       FROM tech_platform_typ ;
    next_tech_platform_typ_id := next_tech_platform_typ_id + 1;

    INSERT INTO tech_platform_typ(
        tech_platform_typ_ID, tech_platform_typ_NM, software_version,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_tech_platform_typ_id, i_name, i_version_name,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform_typ : ' || i_name);
END;
/

--============================================================================
--  TechPlatformTyp
--============================================================================
CREATE OR REPLACE FUNCTION TechPlatformTyp (i_tech_platform_typ_name IN varchar2)
   return number is
    gg number;
begin

    select tech_platform_typ_id INTO gg from tech_platform_typ
      where tech_platform_typ_nm = i_tech_platform_typ_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/




--============================================================================
-- AddTechPlatformMgmtMode
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatformMgmtMode (
                                       i_tech_platform_id IN NUMBER,
                                       i_mgmt_mode_id IN number) is
BEGIN
    INSERT INTO tech_platform_mgmt_mode(
         TECH_PLATFORM_ID, MGMT_MODE_ID,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_tech_platform_id, i_mgmt_mode_id,
         SYSDATE,'albeda', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform_mgmt_mode : ' ||
         i_tech_platform_id || ' ' || i_mgmt_mode_id);
END;
/
--============================================================================
--  TechPlatform
--============================================================================
CREATE OR REPLACE FUNCTION TechPlatform (i_tech_platform_name IN varchar2)
   return number is
    gg number;
begin

    select tech_platform_id INTO gg from tech_platform
      where techn_platform_nm = i_tech_platform_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddTechPlatform
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatform (i_name IN varchar2,
                                             i_platform_typ_id IN number)
                                             is
    next_tech_platform_id number(9);
BEGIN
    select nvl(max(tech_platform_id), 1) INTO next_tech_platform_id
       FROM tech_platform ;
    next_tech_platform_id := next_tech_platform_id + 1;

    INSERT INTO tech_platform(
        tech_platform_ID, tech_platform_typ_id,
        techn_platform_nm,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY, TOPOLOGY_STATUS_ID)
    VALUES(next_tech_platform_id, i_platform_typ_id,
            i_name, SYSDATE,'albeda', SYSDATE,'INIT', TopologyStatus('in_service'));
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform : ' || i_name );
END;
/

--============================================================================
-- AddSubntwkSelectionAlgorithm
--============================================================================
--CREATE OR REPLACE PROCEDURE AddSubntwkSelectionAlgorithm (
--                                             i_subntwk_typ_id IN number,
--                                             i_selection_id IN  number)
--                                            is
--BEGIN
--    INSERT INTO SUBNTWK_SEL_ALGORITHM(
--        selection_algorithm_id, subntwk_typ_id,
--         CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--    VALUES(i_selection_id, i_subntwk_typ_id,
--           SYSDATE,'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating subntwk_selection_algorithm : ' ||
--          i_subntwk_typ_id || ' ' || i_selection_id );
--END;
--/

--============================================================================
-- AddSTMParm
--============================================================================
--CREATE OR REPLACE PROCEDURE AddSTMParm (i_parm_nm IN varchar2,
--                                        i_data_typ_nm IN varchar2,
--                                        i_classid  IN number,
--                                        i_reserved IN char) is
--    next_parm_id number(9);
--    stm_parm_src_id number(9);
--BEGIN
--    select nvl(max(parm_id), 1) INTO next_parm_id
--       FROM parm ;
--    next_parm_id := next_parm_id + 1;
--
--    INSERT INTO Parm(
--        parm_id, parm_nm, parm_src_nm, data_typ_nm, class_id,is_reserved,
--         CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--   VALUES(next_parm_id, i_parm_nm, 'stm', i_data_typ_nm, i_classid, i_reserved,
--          SYSDATE,'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating parm : ' ||
--          i_parm_nm || ' ' || i_data_typ_nm || SQLERRM);
--END;
--/

CREATE OR REPLACE PROCEDURE updatetopologycfgver (i_ver IN NUMBER)
AS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM VERSION
    WHERE module_name = 'TopologyCfg';

   IF c = 0
   THEN
      INSERT INTO VERSION
                  (module_name, created_dtm, created_by, spec_title,
                   spec_vendor, spec_version, impl_title, impl_version,
                   built_by, built_dtm
                  )
           VALUES ('TopologyCfg', SYSDATE, 'sync', 'SMP',
                   'Sigma Systems', '3', 'st_plsql_prcs',
                   i_ver,
                   'Sigma', SYSDATE
                  );
   ELSE
      UPDATE VERSION
         SET impl_version = i_ver
       WHERE module_name = 'TopologyCfg';
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncSubntwkTyp(
   i_subntwk_typ_nm         IN   VARCHAR2,
   i_ntwk_typ_nm            IN   VARCHAR2,
   next_subntwk_typ_id      OUT  number
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_subntwk_typ
    WHERE subntwk_typ_nm = i_subntwk_typ_nm;

   IF c = 0
   THEN
      select nvl(max(subntwk_typ_id), 1) INTO next_subntwk_typ_id
         FROM ref_subntwk_typ ;
      next_subntwk_typ_id := next_subntwk_typ_id + 1;

      INSERT INTO ref_subntwk_typ
                  (subntwk_typ_id, subntwk_typ_nm, ntwk_typ_id,
                   CREATED_DTM, CREATED_BY)
           VALUES (next_subntwk_typ_id, i_subntwk_typ_nm,
                   NtwkTypId(i_ntwk_typ_nm),SYSDATE,'init');
   ELSE
      select subntwk_typ_id INTO next_subntwk_typ_id
         FROM ref_subntwk_typ
      WHERE subntwk_typ_nm = i_subntwk_typ_nm;
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncSubntwkTypPermParm(
                                             i_parm_nm IN  varchar2,
                                             i_is_unique IN char,
                                             i_is_required IN char,
   i_is_read_only IN  char,
   next_parm_id      OUT  number)
IS
    this_parm_id number(9);
    subntwk_typ_class_id number(9);
   c   NUMBER (10);
BEGIN
    select class_id INTO subntwk_typ_class_id
       FROM ref_class where class_nm = 'subntwk_typ' ;

    select count(*) into this_parm_id from parm
      where parm_nm = i_parm_nm and
      class_id = subntwk_typ_class_id;

    if (this_parm_id = 0)
    then
        select nvl(max(parm_id), 1) INTO next_parm_id
          FROM parm ;
        next_parm_id := next_parm_id + 1;

        INSERT INTO Parm(
             parm_id, parm_nm, data_typ_nm, class_id,
             is_read_only,is_unique,is_required,
             CREATED_DTM, CREATED_BY,
             MODIFIED_DTM, MODIFIED_BY)
        VALUES(next_parm_id, i_parm_nm, 'String',
               subntwk_typ_class_id, i_is_read_only,i_is_unique,i_is_required,
               SYSDATE,'xml', SYSDATE,'INIT');
    else
        select parm_id into this_parm_id from parm
             where parm_nm = i_parm_nm and
             class_id = subntwk_typ_class_id;
        next_parm_id := this_parm_id;
    end if;
END;
/

CREATE OR REPLACE PROCEDURE syncLinkTyp(
   i_link_typ_nm         IN   VARCHAR2,
   next_link_typ_id      OUT  number
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_link_typ
    WHERE link_typ_nm = i_link_typ_nm;

   IF c = 0
   THEN
      select nvl(max(link_typ_id), 1) INTO next_link_typ_id
         FROM ref_link_typ ;
      next_link_typ_id := next_link_typ_id + 1;

      INSERT INTO ref_link_typ
                  (link_typ_id, link_typ_nm,
                   CREATED_DTM, CREATED_BY)
           VALUES (next_link_typ_id, i_link_typ_nm, SYSDATE,'init');
   ELSE
      select link_typ_id INTO next_link_typ_id
         FROM ref_link_typ
      WHERE link_typ_nm = i_link_typ_nm;
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncNetworkType(
   i_ntwk_typ_nm         IN   VARCHAR2,
   next_ntwk_typ_id      OUT  number
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_ntwk_typ
    WHERE ntwk_typ_nm = i_ntwk_typ_nm;

   IF c = 0
   THEN
      select nvl(max(ntwk_typ_id), 1) INTO next_ntwk_typ_id
         FROM ref_ntwk_typ ;
      next_ntwk_typ_id := next_ntwk_typ_id + 1;

      INSERT INTO ref_ntwk_typ
                  (ntwk_typ_id, ntwk_typ_nm,
                   CREATED_DTM, CREATED_BY)
           VALUES (next_ntwk_typ_id, i_ntwk_typ_nm, SYSDATE,'SMP');
   ELSE
      select ntwk_typ_id INTO next_ntwk_typ_id
         FROM ref_ntwk_typ
      WHERE ntwk_typ_nm = i_ntwk_typ_nm;
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncManagementMode(
   i_mgmt_mode_nm         IN   VARCHAR2,
   next_mgmt_mode_id      OUT  number
)
IS
   c   NUMBER (10);
BEGIN
   SELECT COUNT (*)
     INTO c
     FROM ref_mgmt_mode
    WHERE mgmt_mode_nm = i_mgmt_mode_nm;

   IF c = 0
   THEN
      select nvl(max(mgmt_mode_id), 1) INTO next_mgmt_mode_id
         FROM ref_mgmt_mode ;
      next_mgmt_mode_id := next_mgmt_mode_id + 1;

      INSERT INTO ref_mgmt_mode
                  (mgmt_mode_id, mgmt_mode_nm,
                   CREATED_DTM, CREATED_BY)
           VALUES (next_mgmt_mode_id, i_mgmt_mode_nm, SYSDATE,'SMP');
   ELSE
      select mgmt_mode_id INTO next_mgmt_mode_id
         FROM ref_mgmt_mode
      WHERE mgmt_mode_nm = i_mgmt_mode_nm;
   END IF;
END;
/

CREATE OR REPLACE PROCEDURE syncTechPlatformTypParm(
                                             i_parm_nm      IN  varchar2,
                                             i_is_unique    IN char,
                                             i_is_required  IN char,
                                             i_is_read_only IN  char,
                                             i_class_nm     IN  varchar2,
                                             next_parm_id   OUT  number)
IS
    this_parm_id number(9);
    tech_platform_typ_class_id number(9);
   c   NUMBER (10);
BEGIN
    select class_id INTO tech_platform_typ_class_id
       FROM ref_class where class_nm = i_class_nm ;

    select count(*) into this_parm_id from parm
      where parm_nm = i_parm_nm and
      class_id = tech_platform_typ_class_id;

    if (this_parm_id = 0)
    then
        select parm_seq.nextval INTO next_parm_id
          FROM dual;

        INSERT INTO Parm(
             parm_id, parm_nm, data_typ_nm, class_id,
             is_read_only,is_unique,is_required,
             CREATED_DTM, CREATED_BY,
             MODIFIED_DTM, MODIFIED_BY)
        VALUES(next_parm_id, i_parm_nm, 'String',
               tech_platform_typ_class_id, i_is_read_only,i_is_unique,i_is_required,
               SYSDATE,'xml', SYSDATE,'INIT');
    else
        select parm_id into this_parm_id from parm
             where parm_nm = i_parm_nm and
             class_id = tech_platform_typ_class_id;
        next_parm_id := this_parm_id;
    end if;
END;
/

--============================================================================
--  ParmId
--============================================================================
--CREATE OR REPLACE FUNCTION ParmId (i_parm_name IN varchar2)
--   return number is
--    gg number;
--begin
--
--    select parm_id INTO gg from parm
--      where parm_nm = i_parm_name ;
--    return gg;
--
--EXCEPTION
--  when no_data_found then
--    return 0;
--END;
--/


--============================================================================
-- AddSubntwk
--============================================================================
CREATE OR REPLACE PROCEDURE AddSubntwk (i_subntwk_name IN varchar2,
                                        i_subntwk_typ_id IN number,
                                        i_parent_subntwk_id IN number,
                                        i_topology_status_id IN number
                                        ) is
   next_subntwkid number(9);
BEGIN
    INSERT INTO subntwk(
        SUBNTWK_ID,
        PARENT_SUBNTWK_ID,
        SUBNTWK_NM, TOPOLOGY_STATUS_ID, SUBNTWK_TYP_ID,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(SUBNTWK_SEQ.nextval,
           i_parent_subntwk_id, i_subntwk_name,
           i_topology_status_id, i_subntwk_typ_id,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating subntwk_name : ' ||
          i_subntwk_name  || SQLERRM);
END;
/

--============================================================================
--  Subntwk by its name and type
--============================================================================
CREATE OR REPLACE FUNCTION SubntwkIdBYNmAndTYP (i_subntwk_name IN varchar2, i_subntwk_type IN varchar2)
   return number is
    gg number;
begin
    select subntwk_id INTO gg from subntwk
      where subntwk_nm = i_subntwk_name and SUBNTWK_TYP_ID =SUBNTWKTYP(i_subntwk_type);
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- Addntwk
--============================================================================
CREATE OR REPLACE PROCEDURE AddLink (i_link_name IN varchar2,
                                        i_link_typ_id IN number,
                                        i_from_subntwk_id IN number,
                                        i_to_subntwk_id IN number
                                        ) is
   next_link_id number(9);
BEGIN

    INSERT INTO LINK(LINK_ID,
        FROM_SUBNTWK_ID, LINK_TYP_ID, LINK_NM, TO_SUBNTWK_ID,
        TOPOLOGY_STATUS_ID,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(LINK_SEQ.nextval, i_from_subntwk_id, i_link_typ_id,
           i_link_name, i_to_subntwk_id,TopologyStatus('in_service'),
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating link : ' || SQLERRM ||
          i_link_name );
END;
/

--============================================================================
--  Link
--============================================================================
CREATE OR REPLACE FUNCTION LinkId (i_link_name IN varchar2)
   return number is
    gg number;
begin
    select link_id INTO gg from link
      where link_nm = i_link_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddSubntwkParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddSubntwkParm (i_subntwk_id IN varchar2,
                                        i_parm_nm IN varchar2,
                                        i_parm_val IN varchar2
                                        ) is
   next_parm_id number(9);
   this_parm_id number(9);
   subntwk_typ_class_id number(9);
   subntwk_parm_id number(9);
   this_subntwk_typ_id number(9);
BEGIN
    select class_id INTO subntwk_typ_class_id
       FROM ref_class where class_nm = 'subntwk_typ' ;

    select count(*) into this_parm_id from parm
      where parm_nm = i_parm_nm and
      class_id = subntwk_typ_class_id;

    if (this_parm_id = 0)
    then
        select nvl(max(parm_id), 1) INTO next_parm_id
          FROM parm ;
        next_parm_id := next_parm_id + 1;


        INSERT INTO Parm(
            parm_id, parm_nm, data_typ_nm, class_id,is_unique,
             is_required,
             CREATED_DTM, CREATED_BY,
             MODIFIED_DTM, MODIFIED_BY)
        VALUES(next_parm_id, i_parm_nm, 'String',
               subntwk_typ_class_id, 'n','n',
               SYSDATE,'albeda', SYSDATE,'INIT');
        select parm_id into this_parm_id from parm
            where parm_nm = i_parm_nm and
            class_id = subntwk_typ_class_id;
            next_parm_id := this_parm_id;
    else
        select parm_id into this_parm_id from parm
             where parm_nm = i_parm_nm and
             class_id = subntwk_typ_class_id;
        next_parm_id := this_parm_id;
    end if;

    select r.subntwk_typ_id into this_subntwk_typ_id from
       ref_subntwk_typ r, subntwk s
       where s.subntwk_typ_id = r.subntwk_typ_id and
       s.SUBNTWK_ID = i_subntwk_id;

    select parm_id into subntwk_parm_id from parm p, ref_class r where
       parm_nm = i_parm_nm and
       p.class_id = r.class_id and r.class_nm = 'subntwk_typ';
    INSERT INTO SUBNTWK_PARM(
        SUBNTWK_ID, PARM_ID, VAL,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_subntwk_id, subntwk_parm_id,
           i_parm_val,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating SubntwkParm : ' ||
          i_subntwk_id || ' ' || i_parm_nm || ' ' ||SQLERRM);
END;
/

--============================================================================
-- AddTmplt
--============================================================================
--CREATE OR REPLACE PROCEDURE AddTmplt (i_tmplt_nm IN varchar2,
--                                      i_tmplt_id IN number default 0) is
--    next_tmplt_id number(9);
--BEGIN
--    if (i_tmplt_id = 0)
--    then
--
--    select nvl(max(tmplt_id), 1000) INTO next_tmplt_id
--       FROM tmplt ;
--    next_tmplt_id := next_tmplt_id + 1;
--    else
--       next_tmplt_id := i_tmplt_id;
--    end if;
--
--    INSERT INTO tmplt(
--        tmplt_id, tmplt_nm,
--         CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--    VALUES(next_tmplt_id, i_tmplt_nm,
--           SYSDATE,'albeda', SYSDATE,'INIT');
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating tmplt : ' ||
--          i_tmplt_nm ||SQLERRM );
--END;
--/


--============================================================================
--  SvcSupportedPlatform
--============================================================================
CREATE OR REPLACE FUNCTION SvcSupportedPlatform (
                                   i_svc_delivery_platform_id IN number,
                                   i_svc_id in number)
   return number is
    gg number;
begin

    select svc_supported_platform_id INTO gg from svc_supported_platform
      where svc_delivery_plat_id = i_svc_delivery_platform_id and
            svc_id = i_svc_id ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddTechPlatformParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddTechPlatformParm (i_tech_platform_id IN varchar2,
                                        i_parm_nm IN varchar2,
                                        i_parm_val IN varchar2
                                        ) is
   tech_platform_parm_id number(9);
   tech_platform_class_id number(9);
BEGIN
    select class_id into tech_platform_class_id from ref_class where class_nm = 'tech_platform_typ';
	
	begin
       select parm_id into tech_platform_parm_id from parm where
          parm_nm = i_parm_nm and class_id = tech_platform_class_id;
	exception 
	   when no_data_found then
	   tech_platform_parm_id := null;
	end;
	
	if tech_platform_parm_id is null then
	   select nvl(max(parm_id)+1,100000) into tech_platform_parm_id from parm where parm_id > 99999;
	   INSERT INTO PARM (
           PARM_ID, CLASS_ID, 
           PARM_NM, DATA_TYP_NM, 
           CREATED_DTM, CREATED_BY) 
       VALUES (tech_platform_parm_id, tech_platform_class_id, i_parm_nm, 'String', sysdate, 'INIT');
	end if;
    INSERT INTO tech_platform_parm(
        TECH_PLATFORM_ID, PARM_ID, VAL,
         CREATED_DTM, CREATED_BY)
    VALUES(i_tech_platform_id, tech_platform_parm_id,
           i_parm_val,
           SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating TechPlatformParm : ' ||
          i_tech_platform_id || ' ' || i_parm_nm || SQLERRM );
END;
/

--============================================================================
--  TechPlatform
--============================================================================
CREATE OR REPLACE FUNCTION TechPlatformId (i_tech_platform_name IN varchar2)
   return number is
    gg number;
begin
    select tech_platform_id INTO gg from tech_platform
      where techn_platform_nm = i_tech_platform_name ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  STMParmId
--============================================================================
CREATE OR REPLACE FUNCTION STMParmId (i_parm_name IN varchar2,
                                      i_class_id in number)
   return number is
    gg number;
begin
    select parm_id INTO gg from parm
      where parm_nm = i_parm_name and
      class_id = i_class_id  and
      rownum < 2;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--CREATE OR REPLACE FUNCTION SvcId (i_parm_name IN varchar2) return number is
--    gg number;
--begin
--
--    select svc_id INTO gg from Svc where svc_nm = i_parm_name ;
--    return gg;
--
--EXCEPTION
--  when no_data_found then
--    return 0;
--END;
--/
--
--CREATE OR REPLACE FUNCTION SvcActionId (i_svc_name IN varchar2,
--                                      i_action_name IN varchar2)
--    return number is
--    gg number;
--begin
--
--    select svc_action_id INTO gg from svc_action where
--        svc_id = SvcId (i_svc_name) AND
--        svc_action_nm = i_action_name ;
--
--    return gg;
--
--EXCEPTION
--  when no_data_found then
--    return 0;
--END;
--/

--============================================================================
-- Create sequences
-- Will be removed when added to the main file
--============================================================================

DROP SEQUENCE IP_ADDR_SEQ;
DROP SEQUENCE IP_BLOCK_SEQ;
DROP SEQUENCE IP_SBNT_SEQ;

--============================================================================
-- Create sequences
--============================================================================

CREATE SEQUENCE IP_ADDR_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 10  ORDER CYCLE;

CREATE SEQUENCE IP_BLOCK_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 10  ORDER CYCLE;

CREATE SEQUENCE IP_SBNT_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 10  ORDER CYCLE;

--============================================================================
-- AddIpAddress
--============================================================================
CREATE OR REPLACE PROCEDURE AddIpAddress (i_ip_addr IN varchar2,
                                        i_assigned_subntwk_id IN number,
                                        i_ip_sbnt_id IN number,
                                        i_ip_status_nm IN varchar2
                                        )
IS
BEGIN
    INSERT INTO ip_addr(
	IP_ADDR,
	ASSIGNED_SUBNTWK_ID,
	IP_SBNT_ID,
	IP_STATUS_NM,
        CREATED_DTM, CREATED_BY,
        MODIFIED_DTM, MODIFIED_BY)
    VALUES(
	i_ip_addr,
        i_assigned_subntwk_id,
	i_ip_sbnt_id,
        i_ip_status_nm,
        SYSDATE,'Harry', SYSDATE,'INIT');
    EXCEPTION
    	when others then
       	Dbms_output.put_line('Error Creating : ' ||
          	i_ip_addr  || SQLERRM);
END;
/

--============================================================================
-- AddIpSbnt
--============================================================================

CREATE OR REPLACE PROCEDURE AddIpSbnt (
                                        i_ip_block_id IN number,
					i_ip_sbnt_addr IN varchar2,
					i_ip_sbnt_msk IN varchar2,
					i_ip_sbnt_gateway IN varchar2,
					i_ip_broadcast_addr IN varchar2,
					i_ip_max_hosts IN number,
                                        i_managing_subntwk_id IN number,
					i_svc_provider_id IN number,
                                        i_ip_status_nm IN varchar2,
					i_descr IN varchar2
                                        )
IS
BEGIN
    INSERT INTO ip_sbnt(
	IP_SBNT_ID,
	IP_BLOCK_ID,
	IP_SBNT_ADDR,
	IP_SBNT_MSK,
	IP_SBNT_GATEWAY_ADDR,
	IP_BROADCAST_ADDR,
	IP_MAX_HOSTS,
	MANAGING_SUBNTWK_ID,
	IP_SVC_PROVIDER_ID,
	IP_STATUS_NM,
	DESCR,
        CREATED_DTM, CREATED_BY,
        MODIFIED_DTM, MODIFIED_BY)
    VALUES(
	IP_SBNT_SEQ.nextval,
	i_ip_block_id,
	i_ip_sbnt_addr,
	i_ip_sbnt_msk,
	i_ip_sbnt_gateway,
	i_ip_broadcast_addr,
	i_ip_max_hosts,
	i_managing_subntwk_id,
	i_svc_provider_id,
	i_ip_status_nm,
	i_descr,
        SYSDATE,'Harry', SYSDATE,'INIT');
    EXCEPTION
    	when others then
       	Dbms_output.put_line('Error Creating : ' ||
          	i_ip_sbnt_addr  || SQLERRM);
END;
/

--============================================================================
-- AddIpBlock
--============================================================================

CREATE OR REPLACE PROCEDURE AddIpBlock(
                                        i_assignable_hosts IN number,
                                        i_managing_subntwk_id IN number,
                                        i_ip_status_nm IN varchar2,
					i_mask IN varchar2,
					i_start_ip_addr IN varchar2,
					i_ip_block_nm IN varchar2
                                        )
IS
BEGIN
    INSERT INTO ip_block(
	IP_BLOCK_ID,
	ASSIGNABLE_HOSTS,
	MANAGING_SUBNTWK_ID,
	IP_STATUS_NM,
	MASK,
	START_IP_ADDR,
	IP_BLOCK_NM,
        CREATED_DTM, CREATED_BY,
        MODIFIED_DTM, MODIFIED_BY)
    VALUES(
	IP_BLOCK_SEQ.nextval,
	i_assignable_hosts,
        i_managing_subntwk_id,
        i_ip_status_nm,
	i_mask,
	i_start_ip_addr,
	i_ip_block_nm,
        SYSDATE,'Harry', SYSDATE,'INIT');
    EXCEPTION
    	when others then
       	Dbms_output.put_line('Error Creating : ' ||
          	i_ip_block_nm|| SQLERRM);
END;
/


--============================================================================
--  Subnt
--============================================================================
CREATE OR REPLACE FUNCTION SbntId (i_sbnt_addr IN varchar2)
   return number is
    gg number;
begin
    select ip_sbnt_id INTO gg from IP_SBNT
      where ip_sbnt_addr = i_sbnt_addr ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  IpBlockIdByName
--============================================================================
CREATE OR REPLACE FUNCTION IpBlockIdByName (i_ip_block_nm IN varchar2)
   return number is
    gg number;
begin
    select IP_BLOCK_ID INTO gg from IP_BLOCK
      where ip_block_nm = i_ip_block_nm ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddProjTmpltImpl
--============================================================================
CREATE OR REPLACE PROCEDURE  Addprojtmpltimpl (i_tmpl_impl_nm IN VARCHAR2,
                                             i_tmpl_desc IN VARCHAR2,
                                             i_proj_tmpl IN VARCHAR2,
					     i_proj_tmplt_ver IN NUMBER,
                                             i_sync_reqd IN VARCHAR2,
                                             i_req_context IN VARCHAR2)
    IS

BEGIN

    INSERT INTO PROJ_TMPLT_IMPL (
        proj_tmplt_impl_nm, proj_tmplt_impl_ver, proj_tmplt_impl_descr, proj_tmplt_nm, proj_tmplt_ver,
          CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY, SYNC_REQD, REQ_CONTEXT)
    VALUES(i_tmpl_impl_nm, Projtmpltimplver(i_tmpl_impl_nm)+1, i_tmpl_desc, i_proj_tmpl, i_proj_tmplt_ver,
	 SYSDATE,'INIT',
         SYSDATE,'INIT', i_sync_reqd, i_req_context);

EXCEPTION
    WHEN OTHERS THEN
       Dbms_output.put_line('Error Proj Tmpl Impl : ' || SQLERRM ||
       i_tmpl_impl_nm || ' -' || i_proj_tmpl);
END;
/

--============================================================================
-- AddTaskInProjTmplt
--============================================================================
CREATE OR REPLACE PROCEDURE  AddTaskInProjTmplt (i_proj_tmplt IN varchar2,
                                                  i_task_tmplt IN varchar2,
                                             i_no IN number)
    is

BEGIN

    INSERT INTO tasks_in_proj_tmplt (
        proj_tmplt_nm, proj_tmplt_ver, task_tmplt_nm, task_tmplt_ver, execution_seq,
          CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_proj_tmplt, ProjTmpltVer(i_proj_tmplt), i_task_tmplt, TaskTmpltVer(i_task_tmplt), i_no,  SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Task in Proj Tmpl: ' || SQLERRM ||
        i_proj_tmplt || '-' || i_task_tmplt);
END;
/

--============================================================================
--AddTaskTmplt
--============================================================================
CREATE OR REPLACE PROCEDURE AddTaskTmplt (i_task_name IN varchar2,
                                          i_desc IN varchar2,
                                          i_duration IN number,
					  i_reuse IN varchar2)
    is
    cont number;
BEGIN
    if( i_reuse = 'y' )
    then
	select count(*) INTO cont from task_tmplt
	  where task_tmplt_nm = i_task_name;

	if( cont = 0 )
	then
	    INSERT INTO task_tmplt(
                task_tmplt_nm, task_tmplt_ver, task_tmplt_descr,duration,
                 CREATED_DTM, CREATED_BY,
                 MODIFIED_DTM, MODIFIED_BY)
            VALUES(i_task_name, 1, i_desc, i_duration,SYSDATE,
                'INIT', SYSDATE,'INIT');
	end if;
    else
	INSERT INTO task_tmplt(
                task_tmplt_nm, task_tmplt_ver, task_tmplt_descr,duration,
                 CREATED_DTM, CREATED_BY,
                 MODIFIED_DTM, MODIFIED_BY)
        VALUES(i_task_name, TaskTmpltVer(i_task_name)+1, i_desc, i_duration,SYSDATE,
                'INIT', SYSDATE,'INIT');
    end if;
EXCEPTION
    when others then
        Dbms_output.put_line('Error Creating task tmpl : ' || SQLERRM || ', ' || i_task_name );
END;
/

--============================================================================
--AddActionInTaskTmplt
--============================================================================
CREATE OR REPLACE PROCEDURE AddActionsInTaskTmplt
                                        (i_task_tmplt_nm IN varchar2,
                                         i_task_action_tmplt_nm IN varchar2,
                                         i_execution_seq IN number,
                                         i_repeat_jmf_expr IN varchar2,
                                         i_screen_nm  IN varchar2,
                                         i_repeat_parm_id in number,
                                         i_parent_execution_seq in number)
    is

BEGIN

    INSERT INTO actions_in_task_tmplt(
           TASK_TMPLT_NM, TASK_TMPLT_VER, ACTION_TMPLT_NM, ACTION_TMPLT_VER, EXECUTION_SEQ,
            REPEAT_JMF_EXPR, SCREEN_NM, REPEAT_PARM_ID, PARENT_EXECUTION_SEQ,
            CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_task_tmplt_nm, TaskTmpltVer(i_task_tmplt_nm), i_task_action_tmplt_nm, ActionTmpltVer(i_task_action_tmplt_nm), i_execution_seq,
           i_repeat_jmf_expr, i_screen_nm, i_repeat_parm_id, i_parent_execution_seq,SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating actions in task tmpl : ' || SQLERRM || i_task_tmplt_nm || '-' || i_task_action_tmplt_nm || '-' || i_execution_seq);
END;
/

--============================================================================
--AddActionTmplt
--============================================================================
CREATE OR REPLACE PROCEDURE AddActionTmplt
                                        (i_action_tmplt_nm IN varchar2,
                                         i_action_tmplt_descr IN varchar2,
                                         i_topology_action_typ IN varchar2,
                                         i_jmf_vld IN varchar2)
    is

BEGIN

    INSERT INTO action_tmplt(
           ACTION_TMPLT_NM, ACTION_TMPLT_VER, ACTION_TMPLT_DESCR,TOPOLOGY_ACTION_TYP,
           JMF_VLD, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_action_tmplt_nm, ActionTmpltVer(i_action_tmplt_nm)+1, i_action_tmplt_descr, i_topology_action_typ,
           i_jmf_vld, SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating action tmplt : ' || SQLERRM ||
        i_action_tmplt_nm);
END;
/

--============================================================================
--AddRefTopologyActionTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefTopologyActionTyp
                                        (i_topology_action_typ IN varchar2,
                                         i_topology_action_descr IN varchar2)
    is

BEGIN

    INSERT INTO ref_topology_action_typ(
      TOPOLOGY_ACTION_TYP, TOPOLOGY_ACTION_DESCR,
      CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_topology_action_typ, i_topology_action_descr,
           SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating topology action typ : ' ||
         SQLERRM || i_topology_action_typ);
END;
/

--============================================================================
-- AddtasktmpltDepy
--============================================================================
CREATE OR REPLACE PROCEDURE AddTaskTmpltDepy (i_proj_name IN varchar2,
                                             i_task_nm IN varchar2,
                                             i_dep_task_nm In varchar2 ) is
BEGIN

    INSERT INTO task_tmplt_depy (
        proj_tmplt_nm, proj_tmplt_ver, task_tmplt_nm, task_tmplt_ver,
        dependent_task_tmplt_nm, dependent_task_tmplt_ver, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_proj_name, ProjTmpltVer(i_proj_name), i_task_nm, TaskTmpltVer(i_task_nm), i_dep_task_nm, TaskTmpltVer(i_dep_task_nm), SYSDATE,'albeda',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating taskdepy : ' || SQLERRM || i_proj_name || ' - ' || i_task_nm || ' - ' || i_dep_task_nm);
END;
/

--============================================================================
--  AddRefScreens
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefScreen(i_name IN varchar2) is
BEGIN

    INSERT INTO ref_screen (
        screen_NM, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_name,SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ref screen:' || i_name ||SQLERRM);

END;
/

--============================================================================
--  AddRefNtwkCmpntTyp
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefNtwkCmpntTyp(i_name IN varchar2,
                                               i_descr IN varchar2) is
BEGIN

    INSERT INTO ref_ntwk_cmpnt_typ (
        NTWK_CMPNT_TYP_NM, NTWK_CMPNT_TYP_DESCR, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_name, i_descr, SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ref ntwk cmpnt typ: ' ||
                SQLERRM || i_name );

END;
/

--============================================================================
--  AddRefTaskStatus
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefTaskStatus(i_name IN varchar2,
                                               i_descr IN varchar2) is
BEGIN

    INSERT INTO ref_task_status (
        TASK_STATUS_NM, TASK_STATUS_DESCR, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_name, i_descr, SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ref task status: ' ||
                SQLERRM || i_name );

END;
/

--============================================================================
--  AddRefProjStatus
--============================================================================
CREATE OR REPLACE PROCEDURE AddRefProjStatus(i_name IN varchar2,
                                             i_descr IN varchar2) is
BEGIN

    INSERT INTO ref_proj_status (
        PROJ_STATUS_NM, PROJ_STATUS_DESCR, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_name, i_descr, SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ref project status: ' ||
                SQLERRM || i_name );

END;
/

--============================================================================
--  AddAllowableProjTransitions
--============================================================================
CREATE OR REPLACE PROCEDURE AddAllowableProjTransitions(i_to_proj_status IN varchar2,
                                             i_from_proj_status IN varchar2) is
BEGIN

    INSERT INTO proj_transitions (
        FROM_PROJ_STATUS_NM, TO_PROJ_STATUS_NM, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_from_proj_status, i_to_proj_status, SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating allowable proj transitions: ' ||
                SQLERRM || i_to_proj_status || ' - ' ||
                i_from_proj_status );

END;
/

--============================================================================
--  AddAllowableTaskTransitions
--============================================================================
CREATE OR REPLACE PROCEDURE AddAllowableTaskTransitions(
                                    i_from_task_status_nm IN varchar2,
                                    i_to_task_status_nm IN varchar2) is
BEGIN

    INSERT INTO task_transitions (
        FROM_TASK_STATUS_NM, TO_TASK_STATUS_NM, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_from_task_status_nm,i_to_task_status_nm , SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating allowable task transitions: ' ||
                SQLERRM || i_from_task_status_nm || ' - ' ||
                i_to_task_status_nm );

END;
/

--============================================================================
--  AddNtwkCmpntTypParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddNtwkCmpntTypParm(
                                    i_ntwk_cmpnt_typ_nm IN varchar2,
                                    i_parm_id IN number) is
BEGIN

    INSERT INTO ntwk_cmpnt_typ_Parm (
        NTWK_CMPNT_TYP_NM, PARM_ID, CREATED_DTM,
        CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_ntwk_cmpnt_typ_nm,i_parm_id , SYSDATE,'INIT',
         SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating ntwk cmpnt parm: ' ||
                SQLERRM || i_ntwk_cmpnt_typ_nm || ' - ' ||
                i_parm_id );

END;
/

--============================================================================
--  AddActionTmpltParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddActionTmpltParm(
                                    i_action_tmplt_nm IN varchar2,
                                    i_ntwk_cmpnt_typ_nm IN varchar2,
                                    i_parm_id   IN number,
                                    i_is_out  IN char) is
    next_parm_id number(9);
    parm_count   number;
BEGIN

    select count(*) into parm_count from action_tmplt_parm where
           action_tmplt_nm = i_action_tmplt_nm and parm_id = i_parm_id;

    if(parm_count = 0)
        then
          select nvl(max(ACTION_TMPLT_PARM_ID), 1) INTO next_parm_id
              FROM action_tmplt_parm ;
          next_parm_id := next_parm_id + 1;


          INSERT INTO action_tmplt_parm (
             ACTION_TMPLT_NM,ACTION_TMPLT_VER,NTWK_CMPNT_TYP_NM,PARM_ID,ACTION_TMPLT_PARM_ID,
             IS_OUT_PARM, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
          VALUES(i_action_tmplt_nm, ActionTmpltVer(i_action_tmplt_nm), i_ntwk_cmpnt_typ_nm, i_parm_id, next_parm_id,
                 i_is_out , SYSDATE,'INIT', SYSDATE,'INIT');
    else
          UPDATE action_tmplt_parm
              set NTWK_CMPNT_TYP_NM = i_ntwk_cmpnt_typ_nm , IS_OUT_PARM = i_is_out
                  where action_tmplt_nm = i_action_tmplt_nm and parm_id = i_parm_id;
    end if;

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating action tmplt parm: ' ||
                SQLERRM || i_action_tmplt_nm || ' - ' || i_ntwk_cmpnt_typ_nm ||
   next_parm_id || ' - ' || i_parm_id );

END;
/

--============================================================================
--  AddProjTmpltParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddProjTmpltParm(
                                    i_proj_tmplt_nm IN varchar2,
                                    i_parm_id   IN number) is
    next_parm_id number(9);
BEGIN

    select nvl(max(PROJ_TMPLT_PARM_ID), 1) INTO next_parm_id
       FROM proj_tmplt_Parm ;
    next_parm_id := next_parm_id + 1;

    INSERT INTO proj_tmplt_Parm (
        PROJ_TMPLT_NM, PROJ_TMPLT_PARM_ID, PROJ_TMPLT_VER, PARM_ID, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_proj_tmplt_nm, next_parm_id, ProjTmpltVer(i_proj_tmplt_nm), i_parm_id, SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating proj tmplt parm: ' ||
                SQLERRM || i_proj_tmplt_nm || ' - ' || next_parm_id ||            i_parm_id );

END;
/

--============================================================================
-- Addtasksinproj
--============================================================================
--CREATE OR REPLACE PROCEDURE Addtasksinproj (i_proj_name IN varchar2,
--                                            i_task_name IN varchar2,
--                                           i_exec_seq IN number) is
--BEGIN
--    INSERT INTO tasks_in_proj_tmplt(
--        proj_tmplt_nm, task_tmplt_NM, execution_seq, CREATED_DTM, CREATED_BY,
--         MODIFIED_DTM, MODIFIED_BY)
--    VALUES(_proj_name, i_task_name, i_exec_seq, SYSDATE,'INIT',
--         SYSDATE,'INIT');
--
--EXCEPTION
--    when others then
--       Dbms_output.put_line('Error Creating taskinProj : ' || i_proj_name);
--END;
--/
--============================================================================
--  AddTaskTmpltParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddTaskTmpltParm(
                                    i_task_tmplt_nm IN varchar2,
                                    i_parm_id   IN number) is
    next_parm_id number(9);
BEGIN

    select nvl(max(TASK_TMPLT_PARM_ID), 1) INTO next_parm_id
       FROM task_tmplt_parm ;
    next_parm_id := next_parm_id + 1;


    INSERT INTO task_tmplt_parm (
      TASK_TMPLT_NM,TASK_TMPLT_VER,PARM_ID,TASK_TMPLT_PARM_ID,CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_task_tmplt_nm, TaskTmpltVer(i_task_tmplt_nm), i_parm_id, next_parm_id,  SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating task tmplt parm: ' ||
                SQLERRM || i_task_tmplt_nm || ' - ' ||  next_parm_id || ' - ' || i_parm_id );

END;
/

--============================================================================
--  AddSrcActionParm
--============================================================================
CREATE OR REPLACE PROCEDURE AddSrcActionParm(
                                    i_scope_proj_tmplt_nm IN varchar2,
                                    i_scope_task_tmplt_nm   IN varchar2,
                                    i_scope_action_execution_seq in number,
                                    i_action_tmplt_parm_id  IN  number,
                                    i_src_proj_tmplt_parm_id in number,
                                    i_src_task_tmplt_parm_id in number,
                                    i_src_task_tmplt_nm in varchar2,
                                    i_src_action_execution_seq in number,
                                    i_src_action_tmplt_parm_id in number,
                                    i_default in varchar2
                                    ) is
BEGIN

    INSERT INTO src_action_parm (
      SCOPE_PROJ_TMPLT_NM, SCOPE_PROJ_TMPLT_VER, SCOPE_TASK_TMPLT_NM, SCOPE_TASK_TMPLT_VER, SCOPE_ACTION_EXECUTION_SEQ, ACTION_TMPLT_PARM_ID,
      SRC_PROJ_TMPLT_PARM_ID, SRC_TASK_TMPLT_PARM_ID, SRC_TASK_TMPLT_NM, SRC_TASK_TMPLT_VER, SRC_ACTION_EXECUTION_SEQ, SRC_ACTION_TMPLT_PARM_ID , DFLT_VAL, CREATED_DTM, CREATED_BY, MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_scope_proj_tmplt_nm, ProjTmpltVer(i_scope_proj_tmplt_nm), i_scope_task_tmplt_nm, TaskTmpltVer(i_scope_task_tmplt_nm), i_scope_action_execution_seq, i_action_tmplt_parm_id,
	   i_src_proj_tmplt_parm_id, i_src_task_tmplt_parm_id, i_src_task_tmplt_nm, TaskTmpltVer(i_src_task_tmplt_nm), i_src_action_execution_seq, i_src_action_tmplt_parm_id, i_default, SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating src action parm: ' ||
                SQLERRM || i_scope_proj_tmplt_nm || ' - ' ||  i_scope_task_tmplt_nm || ' - ' || i_scope_action_execution_seq || ' - ' || i_src_action_execution_seq );

END;
/

--============================================================================
--  ActionTmpltParmId
--============================================================================
CREATE OR REPLACE FUNCTION ActionTmpltParmId (i_action_tmplt_name IN varchar2,
                                      i_parm_nm IN varchar2)
   return number is
    gg number;
begin
    select ACTION_TMPLT_PARM_ID INTO gg from action_tmplt_parm
      where ACTION_TMPLT_NM = i_action_tmplt_name and
      PARM_ID = STMParmId(i_parm_nm,Get_Class_Id('stm_project')) and
      ACTION_TMPLT_VER = ActionTmpltVer(i_action_tmplt_name);
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  TaskTmpltParmId
--============================================================================
CREATE OR REPLACE FUNCTION TaskTmpltParmId (i_task_tmplt_name IN varchar2,
                                      i_parm_nm IN varchar2)
   return number is
    gg number;
begin
    select TASK_TMPLT_PARM_ID INTO gg from task_tmplt_parm
      where TASK_TMPLT_NM = i_task_tmplt_name and
      PARM_ID = STMParmId(i_parm_nm,Get_Class_Id('stm_project')) and
      TASK_TMPLT_VER = TaskTmpltVer(i_task_tmplt_name);
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  ProjTmpltParmId
--============================================================================
CREATE OR REPLACE FUNCTION ProjTmpltParmId (i_proj_tmplt_name IN varchar2,
                                      i_parm_id IN number)
   return number is
    gg number;
begin
    select PROJ_TMPLT_PARM_ID INTO gg from proj_tmplt_parm
      where PROJ_TMPLT_NM = i_proj_tmplt_name and
      PARM_ID = i_parm_id and
      PROJ_TMPLT_VER = ProjTmpltVer(i_proj_tmplt_name);
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
-- AddProjTmpltImplParm
--============================================================================
CREATE OR REPLACE PROCEDURE Addprojtmpltimplparm (i_proj_tmplt_impl_nm IN VARCHAR2,
                                          i_parm_id IN NUMBER,
                                          i_src_parm_id IN NUMBER,
                                          i_jmf_src IN VARCHAR2,
                                          i_val IN VARCHAR2) IS
BEGIN
    INSERT INTO PROJ_TMPLT_IMPL_PARM(
        PROJ_TMPLT_IMPL_NM, PROJ_TMPLT_IMPL_VER, PROJ_TMPLT_PARM_ID, SRC_PARM_ID, VAL, JMF_SRC, CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(i_proj_tmplt_impl_nm, Projtmpltimplver(i_proj_tmplt_impl_nm), i_parm_id, i_src_parm_id, i_val, i_jmf_src,
            SYSDATE,'INIT', SYSDATE,'INIT');

EXCEPTION
    WHEN OTHERS THEN
       Dbms_output.put_line('Error Creating project template Imp Parm : ' || i_proj_tmplt_impl_nm || i_parm_id || ' - ' || i_val || ' - ' ||i_src_parm_id|| SQLERRM);
END;
/

--============================================================================
-- IsTaskTmplt()
--============================================================================
CREATE OR REPLACE FUNCTION IsTaskTmplt(i_task_tmplt_nm IN varchar2)
   return number is
    gg number;
begin

        select count(*) INTO gg from task_tmplt
        where task_tmplt_nm = i_task_tmplt_nm;
        return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

--============================================================================
--  IsRefScreen()
--============================================================================
CREATE OR REPLACE FUNCTION IsRefScreen(i_screen_nm IN varchar2)
   return number is
    gg number;
begin

        select count(*) INTO gg from ref_screen
        where screen_nm = i_screen_nm;
        return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

-----------------------------------------------------------------------
-- PARM
-----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION IsParm
   ( i_parm_nm IN parm.parm_nm%type,
     i_class_nm IN ref_class.class_nm%type)
   return number is
    gg number;
BEGIN
        select count(*) INTO gg
	from parm
        where parm_nm = i_parm_nm
	and class_id = Get_Class_Id(i_class_nm);
        return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

-----------------------------------------------------------------------
-- Add PARM
-----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE PRC_ST_ADD_PARM
   ( i_parm_nm IN parm.parm_nm%type,
     i_data_typ_nm IN parm.data_typ_nm%type,
     i_class_nm IN ref_class.class_nm%type,
     i_is_required IN parm.is_required%type,
     i_default_value IN parm.dflt_val%type,
     i_created_by IN parm.created_by%type
)
   IS
BEGIN
    IF (IsParm( i_parm_nm, i_class_nm) = 0)
    THEN
	Add_Parm(p_parm_nm => i_parm_nm,
                p_parm_src_nm => 'stm',
		p_data_typ_nm => i_data_typ_nm,
		p_class_id => Get_Class_Id(i_class_nm),
		p_is_required => i_is_required,
		p_dflt_val => i_default_value,
		p_created_by => i_created_by);
    END IF;
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating Parm: ' || i_parm_nm || ' - ' || SQLERRM);
END;
/

-----------------------------------------------------------------------
-- IsParmPermittedVal()
-----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION IsParmPermittedVal
   ( i_parm_nm IN parm.parm_nm%type,
     i_val IN parm_permitted_val.val%type)
   return number is
    gg number;
BEGIN
        select count(*) INTO gg
        from parm_permitted_val
        where parm_id = STMParmId(i_parm_nm, Get_Class_Id('stm_project'))
        and val = i_val;
        return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

-----------------------------------------------------------------------
-- PARM PERMITTED VALUES
-----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE PRC_ST_ADD_PARM_PERMITTED_VAL
   ( i_parm_nm IN parm.parm_nm%type,
     i_val IN parm_permitted_val.val%type)
   IS
BEGIN
    IF (IsParmPermittedVal( i_parm_nm, i_val) = 0)
    THEN
    	PRC_CM_ADD_PARM_PERMITTED_VAL(STMParmId( i_parm_nm, Get_Class_Id('stm_project')), i_val, 'stm');
    END IF;
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating Parm Permitted Val: ' || i_parm_nm || ' - ' || i_val || ' - ' || SQLERRM);
END;
/

--=================================================================
-- Load project package which can delete and show project information
--=================================================================
CREATE OR REPLACE
PACKAGE project
IS
    PROCEDURE delete( i_proj_tmplt_nm IN VARCHAR2);
    PROCEDURE delete_task( i_proj_tmplt_nm IN VARCHAR2);
    PROCEDURE delete_task_tmplt( i_task_tmplt_nm IN VARCHAR2);
    PROCEDURE delete_tasks_in_proj_tmplt(i_proj_tmplt_nm IN VARCHAR2);
    PROCEDURE delete_project( i_proj_tmplt_nm IN VARCHAR2);
    PROCEDURE delete_project_tmplt( i_proj_tmplt_nm IN VARCHAR2);
    PROCEDURE show_actions_in_task(i_task_tmplt_nm IN
			task_tmplt.task_tmplt_nm%TYPE);
    --
    PROCEDURE show_src_action_parm(i_task_tmplt_nm IN
			task_tmplt.task_tmplt_nm%TYPE);
    --
    PROCEDURE show_action_tmplt_parm;
    --
END;
/
-- End of package specification

CREATE OR REPLACE
PACKAGE BODY project
IS
    PROCEDURE delete( i_proj_tmplt_nm IN VARCHAR2)
    IS
	CURSOR l_tasks_in_proj_tmplt_cur
	IS
		select task_tmplt_nm
		from tasks_in_proj_tmplt
		where proj_tmplt_nm = i_proj_tmplt_nm
		and task_tmplt_nm not in (
			select task_tmplt_nm
			from tasks_in_proj_tmplt
			where proj_tmplt_nm <> i_proj_tmplt_nm
		)
		order by execution_seq desc;
    BEGIN
        delete_task(i_proj_tmplt_nm);
        FOR l_task_tmplt_rec IN l_tasks_in_proj_tmplt_cur
        LOOP
  	    delete_task_tmplt(l_task_tmplt_rec.task_tmplt_nm);
        END LOOP;
	delete_tasks_in_proj_tmplt(i_proj_tmplt_nm);
	delete_project(i_proj_tmplt_nm);
	delete_project_tmplt(i_proj_tmplt_nm);
    EXCEPTION
    when others then
       Dbms_output.put_line('Error delete project tmplt: ' || i_proj_tmplt_nm||SQLERRM);
    END;

    PROCEDURE delete_task( i_proj_tmplt_nm IN VARCHAR2)
    IS
    BEGIN
	delete from task_action_parm
	where task_action_id in (
		select task_action_id
		from task_action a,
			task b
		where a.task_id = b.task_id
		and b.proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm)
		);

	delete from task_action
	where task_id in (
		select task_id
		from task
		where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm)
		);

	delete from task_parm
	where task_id in (
		select task_id
		from task
		where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm)
		);

	delete from task
	where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm
	);
    EXCEPTION
    when others then
       Dbms_output.put_line('Error delete tasks in project: ' || i_proj_tmplt_nm||SQLERRM);

    END;

    PROCEDURE delete_tasks_in_proj_tmplt(i_proj_tmplt_nm IN VARCHAR2)
    IS
	CURSOR l_tasks_in_proj_tmplt_cur
	IS
		select task_tmplt_nm
		from tasks_in_proj_tmplt
		where proj_tmplt_nm = i_proj_tmplt_nm;
    BEGIN
	delete from task_tmplt_depy
	where proj_tmplt_nm = i_proj_tmplt_nm;

        FOR l_task_tmplt_rec IN l_tasks_in_proj_tmplt_cur
        LOOP
		DBMS_OUTPUT.put_line('task_tmplt_nm : ' || l_task_tmplt_rec.task_tmplt_nm);
		delete from tasks_in_proj_tmplt
		where proj_tmplt_nm = i_proj_tmplt_nm
		and task_tmplt_nm = l_task_tmplt_rec.task_tmplt_nm;

		delete from task_tmplt
		where task_tmplt_nm = l_task_tmplt_rec.task_tmplt_nm
		and task_tmplt_nm not in (
			select task_tmplt_nm
			from tasks_in_proj_tmplt
			where proj_tmplt_nm <> i_proj_tmplt_nm
		);
	END LOOP;
    EXCEPTION
    when others then
       Dbms_output.put_line('Error delete tasks in project tmplt: proj_tmplt_nm: ' || i_proj_tmplt_nm||SQLERRM);
    END;

    PROCEDURE delete_task_tmplt(i_task_tmplt_nm IN VARCHAR2)
    IS
	gg number;
    BEGIN
	-- delete the action level tmplt
       Dbms_output.put_line('delete src_action_parm: task_tmplt_nm: ' || i_task_tmplt_nm);
	delete from src_action_parm
	where scope_task_tmplt_nm = i_task_tmplt_nm;

	select count(*) into gg from src_action_parm
	where scope_task_tmplt_nm = i_task_tmplt_nm;

       Dbms_output.put_line('number of  src_action_parm: '|| gg);

	delete from actions_in_task_tmplt
	where task_tmplt_nm = i_task_tmplt_nm;

	delete from task_tmplt_parm
	where task_tmplt_nm = i_task_tmplt_nm;
    EXCEPTION
    when others then
       Dbms_output.put_line('Error delete task tmplt: ' || i_task_tmplt_nm || SQLERRM);
    END;

    PROCEDURE delete_project( i_proj_tmplt_nm IN VARCHAR2)
    IS
    BEGIN
	delete from proj_parm
	where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm
	);

	delete from proj_impacted_sub_svc
	where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm
	);

	delete from locked_resource
	where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm
	);

	delete from proj
	where proj_id in ( select proj_id
				from proj a,
					proj_tmplt_impl b
				where b.proj_tmplt_nm = i_proj_tmplt_nm
				and a.proj_tmplt_impl_nm = b.proj_tmplt_impl_nm
	);
    EXCEPTION
    when others then
       Dbms_output.put_line('Error delete project: proj_tmplt_nm: ' || i_proj_tmplt_nm||SQLERRM);
    END;

    PROCEDURE delete_project_tmplt( i_proj_tmplt_nm IN VARCHAR2)
    IS
    BEGIN
	-- delete project level tmplt
	delete from proj_tmplt_impl_parm
	where proj_tmplt_impl_nm in (
		select proj_tmplt_impl_nm
		from proj_tmplt_impl
		where proj_tmplt_nm = i_proj_tmplt_nm
	);

--	where proj_tmplt_parm_id in (
--		select proj_tmplt_parm_id
--		from proj_tmplt_parm
--		where proj_tmplt_nm = i_proj_tmplt_nm
--	);

	delete from proj_tmplt_impl
		where proj_tmplt_nm = i_proj_tmplt_nm;

	delete from proj_tmplt_parm
		where proj_tmplt_nm = i_proj_tmplt_nm;

	delete from proj_tmplt
		where proj_tmplt_nm = i_proj_tmplt_nm;
    EXCEPTION
    when others then
       Dbms_output.put_line('Error delete project tmplt: proj_tmplt_nm: ' || i_proj_tmplt_nm||SQLERRM);
    END;
    --
    PROCEDURE show_actions_in_task(i_task_tmplt_nm IN
			task_tmplt.task_tmplt_nm%TYPE)
    IS
	CURSOR l_actions_in_task_cur
	IS
        	select  a.TASK_TMPLT_NM			TASK_TMPLT_NM,
        		a.ACTION_TMPLT_NM		ACTION_TMPLT_NM,
        		a.EXECUTION_SEQ			EXECUTION_SEQ,
        		a.REPEAT_JMF_EXPR 		REPEAT_JMF_EXPR,
        		a.SCREEN_NM 			SCREEN_NM,
        		b.parm_nm 			repeat_parm_nm,
        		a.PARENT_EXECUTION_SEQ 		PARENT_EXECUTION_SEQ
        	from actions_in_task_tmplt a, parm b
        	where task_tmplt_nm = i_task_tmplt_nm
        	and a.repeat_parm_id = b.parm_id (+);
    BEGIN
	FOR l_rec IN l_actions_in_task_cur
      	LOOP
            DBMS_OUTPUT.put_line(l_rec.EXECUTION_SEQ||' : '
		||l_rec.ACTION_TMPLT_NM||' : '
		||l_rec.PARENT_EXECUTION_SEQ||' : '
		||l_rec.SCREEN_NM||' : '
		||l_rec.REPEAT_JMF_EXPR||' : '
		||l_rec.repeat_parm_nm);
	END LOOP;
    END;
    --
    PROCEDURE show_src_action_parm(i_task_tmplt_nm IN
			task_tmplt.task_tmplt_nm%TYPE)
    IS
	CURSOR l_src_action_parm_cur
	IS
	select  a.SCOPE_PROJ_TMPLT_NM		SCOPE_PROJ_TMPLT_NM,
		a.SCOPE_TASK_TMPLT_NM 		SCOPE_TASK_TMPLT_NM,
		a.SCOPE_ACTION_EXECUTION_SEQ    SCOPE_ACTION_EXECUTION_SEQ,
		c.parm_nm 			SCOPE_ACTION_PARM,
		a.SRC_TASK_TMPLT_NM		SRC_TASK_TMPLT_NM,
		a.SRC_ACTION_EXECUTION_SEQ	SRC_ACTION_EXECUTION_SEQ,
		e.parm_nm			SRC_ACTION_PARM,
		a.dflt_val			default_val,
		g.parm_nm			SRC_PROJ_TMPLT_PARM
	from src_action_parm a, action_tmplt_parm b, parm c,
		action_tmplt_parm d, parm e,
		proj_tmplt_parm f, parm g
	where scope_task_tmplt_nm = i_task_tmplt_nm
	and a.action_tmplt_parm_id = b.action_tmplt_parm_id
	and b.parm_id = c.parm_id
	and a.src_action_tmplt_parm_id = d.action_tmplt_parm_id (+)
	and d.parm_id = e.parm_id (+)
	and a.src_proj_tmplt_parm_id = f.proj_tmplt_parm_id (+)
	and f.parm_id = g.parm_id (+)
	order by a.SCOPE_TASK_TMPLT_NM, a.SCOPE_ACTION_EXECUTION_SEQ;
    BEGIN
	FOR l_rec IN l_src_action_parm_cur
      	LOOP
            DBMS_OUTPUT.put_line(
		l_rec.SCOPE_ACTION_EXECUTION_SEQ||' : '
		||l_rec.SCOPE_ACTION_PARM||' : '
		||l_rec.SRC_TASK_TMPLT_NM||' : '
		||l_rec.SRC_ACTION_EXECUTION_SEQ||' : '
		||l_rec.SRC_ACTION_PARM||' : '
		||l_rec.default_val||' : '
		||l_rec.SRC_PROJ_TMPLT_PARM);
	END LOOP;
    END;
    --
    PROCEDURE show_action_tmplt_parm
    IS
	CURSOR l_action_tmplt_parm_cur
	IS
	    	select a.action_tmplt_nm 	action_tmplt_nm,
	    	        b.parm_nm	 	parm_nm,
	    	        a.ntwk_cmpnt_typ_nm	ntwk_cmpnt_typ_nm,
	    	        a.is_out_parm		is_out_parm
	    	from action_tmplt_parm a, parm b
	    	where a.parm_id = b.parm_id
		order by action_tmplt_nm, b.parm_nm;
    BEGIN
	FOR l_rec IN l_action_tmplt_parm_cur
      	LOOP
            DBMS_OUTPUT.put_line(
		l_rec.action_tmplt_nm||' : '
		||l_rec.parm_nm||' : '
		||l_rec.ntwk_cmpnt_typ_nm||' : '
		||l_rec.is_out_parm);
	END LOOP;
    END;
END; -- package body project
/
-- end of DDL script for project

--=================================================================
-- Load Synchronize Projects stored procedures
--=================================================================
@sync_objs.sql

@sync_code.sql


--=================================================================
-- Load Reprovisioning stored procedures
--=================================================================
@reprov_objs.sql

@reprov_code.sql

--=================================================================
-- Load SSN stored procedures
--=================================================================
@ssn_objs.sql

@ssn_code.sql

-- fix for trigger not being created in samp_triggers
create or replace trigger subntwk_tbiu
  BEFORE INSERT or UPDATE
  on SUBNTWK
  for each row
begin
  :new.upper_subntwk_nm := upper(rtrim(:new.subntwk_nm));
end;
/

/*
 * Code that was on the branch, before the merge
 *
 *
--============================================================================
-- prc_st_archive_proj_tmplt
--============================================================================

CREATE OR REPLACE
PROCEDURE prc_st_archive_proj_tmplt
   ( p_proj_tmplt_nm IN proj_tmplt.proj_tmplt_nm%TYPE,
     p_user varchar2)
   IS

-- Purpose: Procedure allows a project tmplt to be changed while still retaining the
-- old project templates that may still be referenced in existing projects (via the
-- project template implementations taht use them.
-- The proj_tmplt will be duplicated and the old one renamed to PROJ_TMPLT_NM_<datetime>
-- Likewise, all the proj_tmplt_impls will be duplicated and the old ones renamed
-- to PROJ_TMPLT_IMPL_NM_<datetime>
--
-- All existing projects will be repointed from PROJ_TMPLT_IMPL_NM to PROJ_TMPLT_IMPL_NM_<datetime>
-- All new projects will pick up the new implementation PROJ_TMPLT_IMPL_NM
--
-- MODIFICATION HISTORY
-- Person         Date        Comments
-- ---------      ------      -------------------------------------------
-- Parmi sahota   2002-07-31  Procedure Creation

   l_datetime_postfix                 varchar2(20);
   l_archived_proj_tmplt_nm  proj_tmplt.proj_tmplt_nm%TYPE;

BEGIN
    select to_char(sysdate, 'YYYYMMDDHH24MISS') into l_datetime_postfix from dual;
    l_archived_proj_tmplt_nm := p_proj_tmplt_nm||'_'||l_datetime_postfix;

    -- archive the existing proj_tmplt (the existing one becomes the new one)
    insert into proj_tmplt(proj_tmplt_nm, proj_tmplt_descr, created_by)
    select l_archived_proj_tmplt_nm, proj_tmplt_descr, p_user
    from proj_tmplt
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- archive the existing proj_tmplt_impl (the existing one becomes the new one)
    insert into proj_tmplt_impl(proj_tmplt_impl_nm, proj_tmplt_nm, proj_tmplt_impl_descr, sync_reqd, created_by)
    select proj_tmplt_impl_nm||'_'||l_datetime_postfix, l_archived_proj_tmplt_nm, proj_tmplt_impl_descr, sync_reqd, p_user
    from proj_tmplt_impl
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- point the existing projects to the archived proj_tmplt_impl
    update proj set proj_tmplt_impl_nm = proj_tmplt_impl_nm||'_'||l_datetime_postfix,
           modified_by = p_user
    where proj_tmplt_impl_nm in (
      select proj_tmplt_impl_nm from proj_tmplt_impl
      where proj_tmplt_nm = l_archived_proj_tmplt_nm);

    -- archive the existing tasks_in_proj_tmplt
    insert into tasks_in_proj_tmplt (proj_tmplt_nm, task_tmplt_nm, execution_seq, created_by)
    select l_archived_proj_tmplt_nm, task_tmplt_nm, execution_seq, p_user
    from tasks_in_proj_tmplt
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- archive the existing proj_tmplt_parm
    insert into proj_tmplt_parm (proj_tmplt_parm_id, proj_tmplt_nm, parm_id, created_by)
    select proj_tmplt_parm_seq.nextval, l_archived_proj_tmplt_nm, parm_id, p_user
    from proj_tmplt_parm
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- create the archives by cloning entries in proj_tmplt_impl_parm
    -- the proj_tmplt_parm_id must be taken from the archived proj_tmplt_parm and not the new proj_tmplt_parm.
    insert into proj_tmplt_impl_parm (proj_tmplt_impl_nm, proj_tmplt_parm_id, val, src_parm_id, created_by)
    select a.proj_tmplt_impl_nm||'_'||l_datetime_postfix,
           c.proj_tmplt_parm_id,
           a.val,
           a.src_parm_id,
           p_user
      from proj_tmplt_impl_parm a, proj_tmplt_parm b, proj_tmplt_parm c
     where a.proj_tmplt_parm_id = b.proj_tmplt_parm_id
     and   b.proj_tmplt_nm||'_'||l_datetime_postfix = c.proj_tmplt_nm
     and   b.parm_id = c.parm_id;

    -- archive the existing task_tmplt_depys
    insert into task_tmplt_depy (proj_tmplt_nm, task_tmplt_nm, dependent_task_tmplt_nm, created_by)
    select l_archived_proj_tmplt_nm, task_tmplt_nm, dependent_task_tmplt_nm, p_user
    from task_tmplt_depy
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- archive the existing src_action_parm
    insert into SRC_ACTION_PARM
      ( ACTION_TMPLT_PARM_ID , SCOPE_PROJ_TMPLT_NM , SCOPE_TASK_TMPLT_NM , SCOPE_ACTION_EXECUTION_SEQ , SRC_TASK_TMPLT_NM
      , SRC_ACTION_EXECUTION_SEQ , SRC_PROJ_TMPLT_PARM_ID , SRC_TASK_TMPLT_PARM_ID , SRC_ACTION_TMPLT_PARM_ID , DFLT_VAL
      , CREATED_BY)
    select ACTION_TMPLT_PARM_ID , l_archived_proj_tmplt_nm , SCOPE_TASK_TMPLT_NM , SCOPE_ACTION_EXECUTION_SEQ , SRC_TASK_TMPLT_NM
      , SRC_ACTION_EXECUTION_SEQ , SRC_PROJ_TMPLT_PARM_ID , SRC_TASK_TMPLT_PARM_ID , SRC_ACTION_TMPLT_PARM_ID , DFLT_VAL, p_user
    from src_action_parm
    where scope_proj_tmplt_nm = p_proj_tmplt_nm;

    -- readjust the src_proj_tmplt_parm_id to point to the archived proj_tmplt_parm
    update src_action_parm sap
    set    src_proj_tmplt_parm_id =
             (select proj_tmplt_parm_id
              from   proj_tmplt_parm
              where  proj_tmplt_nm = l_archived_proj_tmplt_nm
              and    parm_id in
                       (select parm_id
                        from   proj_tmplt_parm
                        where  proj_tmplt_parm_id = sap.src_proj_tmplt_parm_id)),
           modified_by = p_user
    where  scope_proj_tmplt_nm = l_archived_proj_tmplt_nm;

EXCEPTION
    WHEN others THEN
        raise ;
END; -- Procedure
/

*/
--============================================================================
-- Fnc_St_Find_Subntwk_Path
--============================================================================
create or replace FUNCTION Fnc_St_Find_Subntwk_Path(p_subntwk_id SUBNTWK.subntwk_id%TYPE) RETURN VARCHAR2 IS
-- function to find the path of subnetwork starting with the highest node (ie node without a parent)
-- this SHOULD NOT BE USED FOR SEARCHING, but used to find the path for a known subnetwork
-- Parmi Sahota 24 May 2002
  l_path VARCHAR2(2000):=NULL;
  CURSOR c_path IS
  SELECT RTRIM(subntwk_nm) subntwk_nm FROM SUBNTWK
  CONNECT BY PRIOR parent_subntwk_id = subntwk_id
  START WITH subntwk_id = p_subntwk_id;
BEGIN
  FOR r_path IN c_path LOOP
    IF l_path IS NULL THEN
      l_path := r_path.subntwk_nm;
    ELSE
      l_path :=  r_path.subntwk_nm|| '|' ||l_path;
    END IF;
  END LOOP;
  RETURN l_path;
END;
/


--============================================================================
-- prc_st_archive_proj_tmplt
--============================================================================

CREATE OR REPLACE
PROCEDURE prc_st_archive_proj_tmplt
   ( p_proj_tmplt_nm IN proj_tmplt.proj_tmplt_nm%TYPE,
     p_user varchar2)
   IS
-- Purpose: Procedure allows a project tmplt to be changed while still retaining the
-- old project templates that may still be referenced in existing projects (via the
-- project template implementations taht use them.
-- The proj_tmplt will be duplicated and the old one renamed to PROJ_TMPLT_NM_<datetime>
-- Likewise, all the proj_tmplt_impls will be duplicated and the old ones renamed
-- to PROJ_TMPLT_IMPL_NM_<datetime>
--
-- All existing projects will be repointed from PROJ_TMPLT_IMPL_NM to PROJ_TMPLT_IMPL_NM_<datetime>
-- All new projects will pick up the new implementation PROJ_TMPLT_IMPL_NM
--
-- MODIFICATION HISTORY
-- Person         Date        Comments
-- ---------      ------      -------------------------------------------
-- Parmi sahota   2002-07-31  Procedure Creation

   l_datetime_postfix                 varchar2(20);
   l_archived_proj_tmplt_nm  proj_tmplt.proj_tmplt_nm%TYPE;

BEGIN
    select to_char(sysdate, 'YYYYMMDDHH24MISS') into l_datetime_postfix from dual;
    l_archived_proj_tmplt_nm := p_proj_tmplt_nm||'_'||l_datetime_postfix;

    -- archive the existing proj_tmplt (the existing one becomes the new one)
    insert into proj_tmplt(proj_tmplt_nm, proj_tmplt_descr, created_by)
    select l_archived_proj_tmplt_nm, proj_tmplt_descr, p_user
    from proj_tmplt
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- archive the existing proj_tmplt_impl (the existing one becomes the new one)
    insert into proj_tmplt_impl(proj_tmplt_impl_nm, proj_tmplt_nm, proj_tmplt_impl_descr, sync_reqd, created_by)
    select proj_tmplt_impl_nm||'_'||l_datetime_postfix, l_archived_proj_tmplt_nm, proj_tmplt_impl_descr, sync_reqd, p_user
    from proj_tmplt_impl
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- point the existing projects to the archived proj_tmplt_impl
    update proj set proj_tmplt_impl_nm = proj_tmplt_impl_nm||'_'||l_datetime_postfix,
           modified_by = p_user
    where proj_tmplt_impl_nm in (
      select proj_tmplt_impl_nm from proj_tmplt_impl
      where proj_tmplt_nm = l_archived_proj_tmplt_nm);

    -- archive the existing tasks_in_proj_tmplt
    insert into tasks_in_proj_tmplt (proj_tmplt_nm, task_tmplt_nm, execution_seq, created_by)
    select l_archived_proj_tmplt_nm, task_tmplt_nm, execution_seq, p_user
    from tasks_in_proj_tmplt
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- archive the existing proj_tmplt_parm
    insert into proj_tmplt_parm (proj_tmplt_parm_id, proj_tmplt_nm, parm_id, created_by)
    select proj_tmplt_parm_seq.nextval, l_archived_proj_tmplt_nm, parm_id, p_user
    from proj_tmplt_parm
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- create the archives by cloning entries in proj_tmplt_impl_parm
    -- the proj_tmplt_parm_id must be taken from the archived proj_tmplt_parm and not the new proj_tmplt_parm.
    insert into proj_tmplt_impl_parm (proj_tmplt_impl_nm, proj_tmplt_parm_id, val, src_parm_id, created_by)
    select a.proj_tmplt_impl_nm||'_'||l_datetime_postfix,
           c.proj_tmplt_parm_id,
           a.val,
           a.src_parm_id,
           p_user
      from proj_tmplt_impl_parm a, proj_tmplt_parm b, proj_tmplt_parm c
     where a.proj_tmplt_parm_id = b.proj_tmplt_parm_id
     and   b.proj_tmplt_nm||'_'||l_datetime_postfix = c.proj_tmplt_nm
     and   b.parm_id = c.parm_id;

    -- archive the existing task_tmplt_depys
    insert into task_tmplt_depy (proj_tmplt_nm, task_tmplt_nm, dependent_task_tmplt_nm, created_by)
    select l_archived_proj_tmplt_nm, task_tmplt_nm, dependent_task_tmplt_nm, p_user
    from task_tmplt_depy
    where proj_tmplt_nm = p_proj_tmplt_nm;

    -- archive the existing src_action_parm
    insert into SRC_ACTION_PARM
      ( ACTION_TMPLT_PARM_ID , SCOPE_PROJ_TMPLT_NM , SCOPE_TASK_TMPLT_NM , SCOPE_ACTION_EXECUTION_SEQ , SRC_TASK_TMPLT_NM
      , SRC_ACTION_EXECUTION_SEQ , SRC_PROJ_TMPLT_PARM_ID , SRC_TASK_TMPLT_PARM_ID , SRC_ACTION_TMPLT_PARM_ID , DFLT_VAL
      , CREATED_BY)
    select ACTION_TMPLT_PARM_ID , l_archived_proj_tmplt_nm , SCOPE_TASK_TMPLT_NM , SCOPE_ACTION_EXECUTION_SEQ , SRC_TASK_TMPLT_NM
      , SRC_ACTION_EXECUTION_SEQ , SRC_PROJ_TMPLT_PARM_ID , SRC_TASK_TMPLT_PARM_ID , SRC_ACTION_TMPLT_PARM_ID , DFLT_VAL, p_user
    from src_action_parm
    where scope_proj_tmplt_nm = p_proj_tmplt_nm;

    -- readjust the src_proj_tmplt_parm_id to point to the archived proj_tmplt_parm
    update src_action_parm sap
    set    src_proj_tmplt_parm_id =
             (select proj_tmplt_parm_id
              from   proj_tmplt_parm
              where  proj_tmplt_nm = l_archived_proj_tmplt_nm
              and    parm_id in
                       (select parm_id
                        from   proj_tmplt_parm
                        where  proj_tmplt_parm_id = sap.src_proj_tmplt_parm_id)),
           modified_by = p_user
    where  scope_proj_tmplt_nm = l_archived_proj_tmplt_nm;

EXCEPTION
    WHEN others THEN
        raise ;
END; -- Procedure
/

--============================================================================
-- ProjTmpltVer
--============================================================================
CREATE OR REPLACE FUNCTION ProjTmpltVer (i_proj_tmplt_nm IN varchar2)
   return number is
    maxver number;
    cont number;
begin
    select count(*) INTO cont from proj_tmplt
      where proj_tmplt_nm = i_proj_tmplt_nm;

    if( cont = 0 )
    then
      maxver := 0;
    else
      select max(proj_tmplt_ver) INTO maxver from proj_tmplt
        where proj_tmplt_nm = i_proj_tmplt_nm;
    end if;
    return maxver;

EXCEPTION
  when others then
    raise;
END;
/

--============================================================================
-- ProjTmpltImplVer
--============================================================================
CREATE OR REPLACE FUNCTION ProjTmpltImplVer (i_proj_tmplt_impl_nm IN varchar2)
   return number is
    maxver number;
    cont number;
begin
    select count(*) INTO cont from proj_tmplt_impl
      where proj_tmplt_impl_nm = i_proj_tmplt_impl_nm;

    if( cont = 0 )
    then
      maxver := 0;
    else
      select max(proj_tmplt_impl_ver) INTO maxver from proj_tmplt_impl
        where proj_tmplt_impl_nm = i_proj_tmplt_impl_nm;
    end if;
    return maxver;

EXCEPTION
  when others then
    raise;
END;
/

--============================================================================
-- TaskTmpltVer
--============================================================================
CREATE OR REPLACE FUNCTION TaskTmpltVer (i_task_tmplt_nm IN varchar2)
   return number is
    maxver number;
    cont number;
begin
    select count(*) INTO cont from task_tmplt
      where task_tmplt_nm = i_task_tmplt_nm;

    if( cont = 0 )
    then
      maxver := 0;
    else
      select max(task_tmplt_ver) INTO maxver from task_tmplt
        where task_tmplt_nm = i_task_tmplt_nm;
    end if;
    return maxver;

EXCEPTION
  when others then
    raise;
END;
/

--============================================================================
-- ActionTmpltVer
--============================================================================
CREATE OR REPLACE FUNCTION ActionTmpltVer (i_action_tmplt_nm IN varchar2)
   return number is
    maxver number;
    cont number;
begin
    select count(*) INTO cont from action_tmplt
      where action_tmplt_nm = i_action_tmplt_nm;

    if( cont = 0 )
    then
      maxver := 0;
    else
      select max(action_tmplt_ver) INTO maxver from action_tmplt
        where action_tmplt_nm = i_action_tmplt_nm;
    end if;
    return maxver;

EXCEPTION
  when others then
    raise;
END;
/

--===Added from stm_patch_2_0_1_5.sql
CREATE OR REPLACE FUNCTION FreqId (i_freq_hz IN varchar2)
   return number is
    gg number;
begin
    select freq_id INTO gg from freq
      where FREQ_HZ = i_freq_hz ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

CREATE OR REPLACE FUNCTION FreqGrpId (i_freq_grp_nm IN varchar2)
   return number is
    gg number;
begin
    select freq_grp_id INTO gg from freq_grp
      where FREQ_GRP_NM = i_freq_grp_nm ;
    return gg;

EXCEPTION
  when no_data_found then
    return 0;
END;
/

CREATE OR REPLACE PROCEDURE XMLCFG_AddTechPlatformTyp (p_name IN varchar2,
                                                p_version_name IN varchar2) is
    next_tech_platform_typ_id number(9);
BEGIN
    select nvl(max(tech_platform_typ_id), 1) INTO next_tech_platform_typ_id
       FROM tech_platform_typ ;
    next_tech_platform_typ_id := next_tech_platform_typ_id + 1;

    INSERT INTO tech_platform_typ(
        tech_platform_typ_ID, tech_platform_typ_NM, software_version,
         CREATED_DTM, CREATED_BY,
         MODIFIED_DTM, MODIFIED_BY)
    VALUES(next_tech_platform_typ_id, p_name, p_version_name,
           SYSDATE,'albeda', SYSDATE,'INIT');
EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform_typ : ' || p_name);
END;
/


CREATE OR REPLACE PROCEDURE XMLCFG_UpdateTPTypParm (
                                             p_tech_platform_typ_nm IN VARCHAR2,
                                             p_parm_nm IN  VARCHAR2,
                                             p_data_typ_nm IN VARCHAR2,
                                             p_is_unique IN CHAR,
                                             p_is_required IN CHAR
                                             )
                                            IS
    tech_platform_typ_class_id NUMBER(9);
BEGIN
    SELECT class_id INTO tech_platform_typ_class_id
       FROM REF_CLASS WHERE class_nm = 'tech_platform_typ' ;

	UPDATE PARM SET
	      PARM.IS_UNIQUE = NVL(p_is_unique, IS_UNIQUE),
		  PARM.IS_REQUIRED = NVL(p_is_required, IS_REQUIRED),
		  PARM.DATA_TYP_NM = NVL(p_data_typ_nm, DATA_TYP_NM)
	WHERE
	      PARM.PARM_NM = p_parm_nm
		  AND PARM.CLASS_ID = tech_platform_typ_class_id;


--EXCEPTION
--    WHEN OTHERS THEN
--       Dbms_output.put_line('Error Creating TechPlatformTyp parm : ' ||
--          p_tech_platform_typ_nm || ' ' || p_parm_nm || ' ' ||
--          SQLERRM );
END;
/


CREATE OR REPLACE FUNCTION GET_ADD_TECH_PLATFORM_TYP_ID  (

    i_tech_platform_typ_nm IN  tech_platform_typ.tech_platform_typ_nm%TYPE)

  RETURN tech_platform_typ.tech_platform_typ_id%TYPE IS

  l_tech_platform_typ_id tech_platform_typ.tech_platform_typ_id%TYPE;
  number_of_tech_platform_typ_id tech_platform_typ.tech_platform_typ_id%TYPE;

BEGIN

    select count(*) into number_of_tech_platform_typ_id from tech_platform_typ
      where tech_platform_typ_nm = i_tech_platform_typ_nm;

    if (number_of_tech_platform_typ_id = 0)
    then
        select nvl(max(tech_platform_typ_id), 1) INTO l_tech_platform_typ_id
          FROM tech_platform_typ ;
        l_tech_platform_typ_id := l_tech_platform_typ_id + 1;
	    INSERT INTO TECH_PLATFORM_TYP (TECH_PLATFORM_TYP_ID, TECH_PLATFORM_TYP_NM, software_version, created_by, created_dtm)
	    VALUES(l_tech_platform_typ_id, i_tech_platform_typ_nm, 'x.x', 'init', sysdate);
    else
        select tech_platform_typ_id into l_tech_platform_typ_id from tech_platform_typ
             where tech_platform_typ_nm = i_tech_platform_typ_nm;
    end if;

  RETURN l_tech_platform_typ_id;

EXCEPTION
    when others then
       Dbms_output.put_line('Error Creating tech_platform_typ_id : ' ||
          i_tech_platform_typ_nm || ' ' || SQLERRM );
END;
/

CREATE OR REPLACE FUNCTION get_add_ssp_id (p_svc_id IN svc_supported_platform.SVC_ID%type, p_sdp_id in svc_supported_platform.SVC_DELIVERY_PLAT_ID%type, p_ssn_group_id in svc_supported_platform.SSN_GROUP_ID%type)
  RETURN svc_supported_platform.SVC_SUPPORTED_PLATFORM_ID%TYPE IS
  l_ssp_id svc_supported_platform.SVC_SUPPORTED_PLATFORM_ID%TYPE;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  SELECT NVL(max(svc_supported_platform_id),0)+1 into l_ssp_id from svc_supported_platform;
  insert into svc_supported_platform (svc_supported_platform_id,svc_delivery_plat_id, svc_id, ssn_group_id, created_dtm, created_by) values (l_ssp_id,p_sdp_id,p_svc_id,p_ssn_group_id,sysdate,'get_add_ssp_id');
  COMMIT;
  RETURN      l_ssp_id;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    BEGIN
        SELECT svc_supported_platform_id into l_ssp_id from svc_supported_platform WHERE svc_delivery_plat_id = p_sdp_id and svc_id = p_svc_id;
        RETURN l_ssp_id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RETURN   get_add_ssp_id(p_svc_id,p_sdp_id,p_ssn_group_id);
    END;
END;
/

CREATE OR REPLACE FUNCTION get_add_sdp_id (p_sdp_nm IN ref_svc_delivery_plat.SVC_DELIVERY_PLAT_NM%type)
  RETURN ref_svc_delivery_plat.SVC_DELIVERY_PLAT_ID%TYPE IS
  l_sdp_id ref_svc_delivery_plat.SVC_DELIVERY_PLAT_ID%TYPE;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  SELECT NVL(max(svc_delivery_plat_id),0)+1 into l_sdp_id from ref_svc_delivery_plat;
  insert into ref_svc_delivery_plat (svc_delivery_plat_id, svc_delivery_plat_nm, created_dtm, created_by) values (l_sdp_id,p_sdp_nm,sysdate,'get_add_sdp_id');
  COMMIT;
  RETURN      l_sdp_id;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    BEGIN
        SELECT svc_delivery_plat_id into l_sdp_id from ref_svc_delivery_plat WHERE svc_delivery_plat_nm = p_sdp_nm;
        RETURN      l_sdp_id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RETURN   get_add_sdp_id(p_sdp_nm);
    END;
END;
/

CREATE OR REPLACE FUNCTION get_add_ntwk_role_id (p_ntwk_role_nm IN ref_ntwk_role.NTWK_ROLE_NM%type) 
  RETURN ref_ntwk_role.NTWK_ROLE_ID%TYPE IS l_ntwk_role_id ref_ntwk_role.NTWK_ROLE_ID%TYPE;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  SELECT NVL(max(ntwk_role_id),0)+1 into l_ntwk_role_id from ref_ntwk_role;
  insert into ref_ntwk_role (ntwk_role_id, ntwk_role_nm, created_dtm, created_by) values (l_ntwk_role_id,p_ntwk_role_nm,sysdate,'get_add_ntwk_role_id');
  COMMIT;
  RETURN l_ntwk_role_id;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    BEGIN
        SELECT ntwk_role_id into l_ntwk_role_id from ref_ntwk_role WHERE ntwk_role_nm = p_ntwk_role_nm;
        RETURN      l_ntwk_role_id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RETURN   get_add_ntwk_role_id(p_ntwk_role_nm);
    END;
END;
/

CREATE OR REPLACE FUNCTION get_add_ssn_group_id (p_ssn_group_nm IN ref_ssn_group.SSN_GROUP_NM%type)
  RETURN ref_ssn_group.SSN_GROUP_ID%TYPE IS
  l_ssn_group_id ref_ssn_group.SSN_GROUP_ID%TYPE;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  SELECT NVL(max(ssn_group_id),0)+1 into l_ssn_group_id from ref_ssn_group;
  insert into ref_ssn_group (ssn_group_id, ssn_group_nm, created_dtm, created_by) values (l_ssn_group_id,p_ssn_group_nm,sysdate,'get_add_ssn_group_id');
  COMMIT;
  RETURN      l_ssn_group_id;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    BEGIN
        SELECT ssn_group_id into l_ssn_group_id from ref_ssn_group WHERE ssn_group_nm = p_ssn_group_nm;
        RETURN l_ssn_group_id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RETURN   get_add_ssn_group_id(p_ssn_group_nm);
    END;
END;
/

--ECHO 'RUNNING RECOMPILE';
EXEC PKG_RECOMPILE_ALL.RECOMP;

-- SHOW ALL THE ERRORS FOR INVALID OBJECTS
SET LINE 100
SET FEEDBACK OFF
COLUMN TEXT FORMAT A40 WOR
SET RECSEP OFF
BREAK ON NAME SKIP 1 NODUP

SELECT NAME, LINE, POSITION, TEXT
    FROM USER_ERRORS
    ORDER BY NAME, LINE, POSITION;


