--============================================================================
--    $Id: st_topology_instance.sql,v 1.1 2007/11/29 21:15:28 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set echo off
set serveroutput on

--=================
-- Topology Instance data
--=================
-- @./data/Tech_Platform.dat.sql
-- @./data/Tech_Platform_Parm.dat.sql
-- @./data/Tech_Platform_Mgmt_Mode.dat.sql
@./data/Subntwk.dat.sql
@./data/Consumable_Rsrc_Mgmt.dat.sql
@./data/Ip_Block.dat.sql
@./data/Ip_Sbnt.dat.sql
@./data/Ip_Address.dat.sql
@./data/Subntwk_Parm.dat.sql
@./data/Link.dat.sql
-- @./data/Sbnt_Tech_Platform.dat.sql
@./data/Freq_Grp.dat.sql
@./data/Freq.dat.sql
@./data/Freq_Grp_List.dat.sql
@./data/Card.dat.sql
@./data/Port.dat.sql
@./data/Port_Pair.dat.sql
@./data/Port_Pair_Link.dat.sql
@./data/subntwk_supported_platform.dat.sql
