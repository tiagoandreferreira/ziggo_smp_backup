#!/usr/bin/ksh
#==============================================================================
#
#  FILE INFO
#    $Id: stm_sync_ntd.sh,v 1.1 2011/12/05 16:11:27 prithvim Exp $
#
#  DESCRIPTION
#
#  REVISION HISTORY
#  * Harry Chen         2002-01-03
#       Original Version
# 	It will run midnight at 12:00 am. It should be configured into cron
#  * Based on CVS
#==============================================================================
#By default it will be run once a day.
INTERVAL=17

LOG_FILE=synchronization.log

echo "Starting synchronizing database..." > $LOG_FILE

USAGE="$0 [user/password]" 

if [ $# -ne 1 ] 
then
    LOGON="$ORACLE_LOGIN/$ORACLE_PASSWORD@$ORACLE_SID"
else
    LOGON=$1
fi

cat synchronize_pp.sql|sqlplus -s $LOGON >>$LOG_FILE

echo "set serveroutput on;" > run.sql
echo "set buffer 20000;" >> run.sql
echo "spool tmp.output;" >> run.sql
echo "exec synchronize_pp.synchronize_new($INTERVAL);" >> run.sql
echo "spool off;" >> run.sql
cat run.sql |sqlplus -s $LOGON 
cat tmp.output >>$LOG_FILE
