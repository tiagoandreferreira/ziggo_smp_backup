--==========================================================================
-- FILE INFO
--   $Id: stm_sync_ntd.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Harry Chen    2001-12-15
--      Initial version
--	A cron deamon will call the scripts by polling the task table to find 
--	out those task that task_tmplt_nm is 'congestion_relief', 
--	the task_status_nm is 'completed' and the created_dtm is later than 
-- 	last time the cron run.
--
--   * Based on CVS log
--==========================================================================
CREATE OR REPLACE
PACKAGE synchronize_pp
IS
   PROCEDURE synchronize_new(i_interval IN NUMBER);
   --
   PROCEDURE synchronize(i_task_id IN number);
   --
   Function synchronize_project(i_project_id IN number)
        return number;
   -- 0 is successful, any other value means error.
   --
   Function synchronize_task(i_task_id IN number)
        return number;
   -- 0 is successful, any other value means error.
   --
   Function getVal(
	i_task_id IN task.task_id%TYPE,
	i_action_tmplt_nm IN action_tmplt.action_tmplt_nm%TYPE,
	i_parm_nm IN parm.parm_nm%Type)
        return task_action_parm.val%TYPE;
END;
/

CREATE OR REPLACE
PACKAGE BODY synchronize_pp
IS
   PROCEDURE synchronize_new(i_interval IN NUMBER)
   IS
	CURSOR L_new_task_cur
	IS
	SELECT task_id
	FROM task
	WHERE task_tmplt_nm = 'congestion_relief'
	AND task_status_nm = 'completed'
	AND created_dtm > SYSDATE - i_interval;
   BEGIN
        FOR L_new_task_rec IN L_new_task_cur
        LOOP
           Dbms_output.put_line('congestion_relief::task_id: '||
		L_new_task_rec.task_id);
	   synchronize(L_new_task_rec.task_id);
	END LOOP;
   END;
   --
   PROCEDURE synchronize(i_task_id IN number)
   IS
    	-- get the port_pairs belong to the previous mac/cmts in STM
        --CURSOR L_port_pair_cur
        --IS
	--SELECT b.upstream_port_id, b.downstream_port_id
	--FROM subntwk a, port_pair b
	--WHERE a.subntwk_id = b.managing_subntwk_id;
	--
	L_mac_cmts_id task_action_parm.val%TYPE;
	L_rs_nm task_action_parm.val%TYPE;
	L_rs_id task_action_parm.val%TYPE;
	L_port_pair_seq ntd_port_pair.port_pair_seq%TYPE;
	L_RS_SEQ ntd_return_segment.return_segment_seq%TYPE;
	L_downstream_port_id task_action_parm.val%TYPE;
	L_upstream_port_id task_action_parm.val%TYPE;
   BEGIN
   Dbms_output.put_line('start synchronizing...');
   -- 1. get all the links between mac domain and return segment based on 
   --	subntwkId
    --1.1 get the mac domain/cmts name in STM
	L_mac_cmts_id := 
	    getVal(i_task_id, 'lookup_link', 'from_subntwk_id');
   Dbms_output.put_line('get from_subntwk_id: '||L_mac_cmts_id);
    --1.3 get the return segment name in STM
	L_rs_id := 
	    getVal(i_task_id, 'lookup_link', 'to_subntwk_id');
	SELECT subntwk_nm INTO L_rs_nm
	FROM subntwk
	WHERE subntwk_id = L_rs_id;
   Dbms_output.put_line('get to_subntwk_nm: '||L_rs_nm);
    --1.4 get the retrun segment sequence in NTD
	SELECT return_segment_seq INTO L_rs_seq
	FROM ntd_return_segment
	WHERE name = L_rs_nm;
   Dbms_output.put_line('get rs sequence in ntd: '||L_rs_seq);
   -- 2. delete all these links from ntd_rs_to_port_pair
    	-- get the port_pairs belong to the previous mac/cmts in STM
      BEGIN
        DECLARE CURSOR L_port_pair_cur
        IS
	SELECT b.upstream_port_id, b.downstream_port_id
	FROM subntwk a, port_pair b
	WHERE a.subntwk_id = b.managing_subntwk_id
	AND a.subntwk_id = L_mac_cmts_id;
	--
	BEGIN
        FOR L_port_pair_rec IN L_port_pair_cur
        LOOP
	    L_upstream_port_id := L_port_pair_rec.upstream_port_id - 100000;
        --2.1 get the port_pair_seq in NTD
	    SELECT port_pair_seq INTO L_port_pair_seq
	    FROM ntd_port_pair
	    WHERE upstream_port_seq = L_upstream_port_id
	    AND downstream_port_seq = L_port_pair_rec.downstream_port_id;
        Dbms_output.put_line('pp: ' ||L_port_pair_seq); 
	--2.2 delete the link if it exists
        --Dbms_output.put_line('Delete from ntd_rs_to_port_pair 
	--		if it is exists:' );
        Dbms_output.put_line('Delete:: '||'rs: '|| L_rs_seq ||
            	'pp: ' || L_port_pair_seq);
	    DELETE FROM ntd_rs_to_port_pair
	    WHERE port_pair_seq = L_port_pair_seq
	    AND ntd_return_segment_seq = L_rs_seq;
	END LOOP;
	END;
    END;
   --
   -- 3. find the port_pair by upstream_port_id and downstream_port_id
	L_downstream_port_id := 
	 getVal(i_task_id, 'assign_pp_to_link', 'downstream_port_id');
   	Dbms_output.put_line('get downstream_port_id: '||L_downstream_port_id);
	--
	L_upstream_port_id := 
	 getVal(i_task_id, 'assign_pp_to_link', 'upstream_port_id');
   	Dbms_output.put_line('get upstream_port_id '||L_upstream_port_id);
	--
	SELECT port_pair_seq INTO L_port_pair_seq 
	FROM ntd_port_pair
	WHERE upstream_port_seq = L_upstream_port_id - 100000
	AND   downstream_port_seq = L_downstream_port_id;
   	Dbms_output.put_line('get port_pair: '||L_port_pair_seq);
   -- 4. use the same return segment fonud in step 1.4 and port_pair found in 3 
   --	and create the record in ntd_rs_to_port_pair
        -- It is not right to have the column, CURRENT_NUMBER_DEVICES, 
	-- because it will cause potential inconsistence between SubDB and NTD
        --Dbms_output.put_line('Insert into ntd_rs_to_port_pair: ' );
        Dbms_output.put_line('Insert '||'return segment seq: ' || L_rs_seq||
        	' port pair seq: ' || L_port_pair_seq);
	INSERT INTO ntd_rs_to_port_pair(
        	RS_TO_PORT_PAIR_SEQ,
        	NTD_RETURN_SEGMENT_SEQ,
        	PORT_PAIR_SEQ,
        	CURRENT_NUMBER_DEVICES,
        	PROVISION_STATUS_CD,
        	COMMENTS,
        	MODIFY_DATE,
        	MODIFY_USERID,
        	CREATE_DATE,
        	CREATE_USERID)
	VALUES(
		NTD_RS_TO_PORT_PAIR_SEQ.nextval,
		L_rs_seq,
		L_port_pair_seq,
		null,
		'ASGN',
		'STM synchronized data',
		SYSDATE,
		'sAMP_STM',
		SYSDATE,
		'SAMP_STM');
   Dbms_output.put_line('=============done====================');
       EXCEPTION
        when others then
                Dbms_output.put_line('Error synchronizing port_pair_link: ' 
		|| SQLERRM);
   END;
   --
   Function synchronize_project(i_project_id IN number)
        return number
   IS
        gg proj.proj_id%TYPE;
   BEGIN
	SELECT task_id INTO gg
	FROM task
	WHERE task_tmplt_nm = 'congestion_relief'
	AND proj_id = i_project_id;
	--
	synchronize(gg);
        return 0;
   EXCEPTION
     when no_data_found then
        Dbms_output.put_line('Not a congestion relieaf'); 
        return 0;
     when others then
	return -1;
   END;
   --
   Function synchronize_task(i_task_id IN number)
        return number
   IS
        gg task.task_id%TYPE;
   BEGIN
	SELECT task_id INTO gg
	FROM task
	WHERE task_tmplt_nm = 'congestion_relief'
	AND task_id = i_task_id;
	--
	synchronize(i_task_id);
        return 0;
   EXCEPTION
     when no_data_found then
        Dbms_output.put_line('Not a congestion relieaf'); 
        return 0;
     when others then
	return -1;
   END;
   --
   Function getVal(
	i_task_id IN task.task_id%TYPE,
	i_action_tmplt_nm IN action_tmplt.action_tmplt_nm%TYPE,
	i_parm_nm IN parm.parm_nm%Type)
        return task_action_parm.val%TYPE
   IS
        gg task_action_parm.val%TYPE;
   BEGIN
	SELECT c.val into gg 
	FROM task_action b, task_action_parm c, parm d, 
		actions_in_task_tmplt e
	WHERE b.task_id = i_task_id
	AND b.task_tmplt_nm = e.task_tmplt_nm
	AND b.action_tmplt_execution_seq = e.execution_seq
	AND e.action_tmplt_nm = i_action_tmplt_nm
	AND b.task_action_id = c.task_action_id
	AND c.parm_id = d.parm_id	
	AND d.parm_nm = i_parm_nm;
        return gg;
   EXCEPTION
     when no_data_found then
       return 0;
   END;
   --
END;
/
