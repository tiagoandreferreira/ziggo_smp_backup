--============================================================================
--    $Id: sub_typ_update.sql,v 1.1 2008/01/31 20:55:59 siarheig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on

------------------------------------------------------------------
---------   Update for table: Ref_Sub_Typ   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Add commercial type to table: Ref_Sub_Typ   ---------')
Insert into Ref_Sub_Typ(sub_typ_id, sub_typ_nm, descr, is_active, created_dtm, created_by, modified_dtm, modified_by)
Values (4,'commercial','commercial','y', SYSDATE, 'INIT', NULL, NULL);

