---------------------------------------------------------------------------
-- FILE INFO
--    $Id: svc_cmp.sql,v 1.2 2003/11/13 20:05:23 vadimg Exp $
--
-- DESCRIPTION
--  Object type definitions for SVC configuration capture and compare
--  Build the types is a separate schema , the owner must have select
--  privilege on SMP schemas you wish to capture/compare
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------


/*  Object cleanup script. DO NOT EXECUTE the script when schema contains any object
    other than SVC capture/compare objects
begin
  for cr in (select object_name from user_objects where object_type = 'TYPE' )loop
     execute immediate 'DROP TYPE '||cr.object_NAME||' force';
     dbms_output.put_line('DROP TYPE '||cr.object_NAME||' force;');
  end loop;
end;
/
*/

create or replace procedure debug_log(p_message varchar2, p_level number default 0) as

begin
  dbms_output.put_line(lpad(p_message , p_level*2+length(p_message), '*'));
end;
/



create or replace type t_varchar255 is table of varchar2(255);
/


create or replace type o_alias_rule as object (
ALIAS_RULE_NM    VARCHAR2(255),
SCOPE_SVC_NM     VARCHAR2(255),
ALIAS_RULE_PARM  t_varchar255,
MEMBER PROCEDURE     DIFF (c o_alias_rule) ,
ORDER MEMBER FUNCTION match (c o_alias_rule) RETURN INTEGER );
/

create or replace type body o_alias_rule as
  MEMBER PROCEDURE     DIFF (c o_alias_rule) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_alias_rule) RETURN INTEGER IS
    n number;
  BEGIN
    IF ALIAS_RULE_NM = c.ALIAS_RULE_NM AND nvl(SCOPE_SVC_NM, 'null') = nvl(c.SCOPE_SVC_NM, 'null')
       AND ALIAS_RULE_PARM.count = c.ALIAS_RULE_PARM.count THEN
      null;
    ELSE
      RETURN -1;
    END IF;
    select count(*) into n
      from table(ALIAS_RULE_PARM) t1, table(c.ALIAS_RULE_PARM) t2
      where value(t1) = value(t2);
    if n <> ALIAS_RULE_PARM.count then
       return -1;
    end if;

    RETURN 0;

  END;

end;
/

ALTER TYPE o_alias_rule COMPILE DEBUG BODY;
ALTER TYPE o_alias_rule COMPILE DEBUG;


create or replace type t_alias_rule as table of o_alias_rule;
/

create or replace type o_folder_svc as object (
FOLDER_ID  number,
SVC_NMS    t_VARCHAR255,
MEMBER PROCEDURE     DIFF (c o_folder_svc ),
ORDER MEMBER FUNCTION match (c o_folder_svc) RETURN INTEGER );
/
create or replace type body o_folder_svc as
 MEMBER PROCEDURE     DIFF (c o_folder_svc ) is
 begin
    null;
 end;
 ORDER MEMBER FUNCTION match (c o_folder_svc) RETURN INTEGER is
  n number;
 begin
  if SVC_NMS.count = c.SVC_NMS.count then
    select count(*) into n from table(SVC_NMS) t1, table(c.SVC_NMS) t2 where value(t1) = value(t2); if n <> SVC_NMS.count then return 1; end if;
    return 0;
  else
    return 1;
  end if;
 end;
end;
/
ALTER TYPE o_folder_svc COMPILE DEBUG ;
ALTER TYPE o_folder_svc COMPILE DEBUG BODY;

create or replace type t_folder_svc as table of o_folder_svc ;
/

create or replace type o_parm_instance_depy_rule as object (
PREREQ_PARM_NM     VARCHAR2(255),
DEPENDENT_PARM_NM  VARCHAR2(255),
SCOPE_SVC_NM       VARCHAR2(255),
MEMBER PROCEDURE     DIFF (c o_parm_instance_depy_rule ),
ORDER MEMBER FUNCTION match (c o_parm_instance_depy_rule) RETURN INTEGER );
/

create or replace type body o_parm_instance_depy_rule as
  MEMBER PROCEDURE     DIFF (c o_parm_instance_depy_rule ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_parm_instance_depy_rule) RETURN INTEGER IS
  BEGIN
     if PREREQ_PARM_NM = c.PREREQ_PARM_NM AND DEPENDENT_PARM_NM = c.DEPENDENT_PARM_NM
        AND nvl(SCOPE_SVC_NM, 'null') = nvl(c.SCOPE_SVC_NM, 'null') then
        return 0;
     else
        return -1;
     end if;
  END;
end;
/

ALTER TYPE o_parm_instance_depy_rule COMPILE DEBUG ;
ALTER TYPE o_parm_instance_depy_rule COMPILE DEBUG BODY;

create or replace type t_parm_instance_depy_rule as table of o_parm_instance_depy_rule;
/

create or replace type o_state_transition as object (
FROM_STATUS_NM   VARCHAR2(255),
TO_STATUS_NM     VARCHAR2(255),
COND             VARCHAR2(2000),
STATE_ACTION_NM  VARCHAR2(50),
MEMBER PROCEDURE     DIFF (c o_state_transition) ,
ORDER MEMBER FUNCTION match (c o_state_transition) RETURN INTEGER );
/

create or replace type body o_state_transition as
  MEMBER PROCEDURE     DIFF (c o_state_transition) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_state_transition) RETURN INTEGER IS
  BEGIN
     if FROM_STATUS_NM = c.FROM_STATUS_NM AND TO_STATUS_NM = c.TO_STATUS_NM
        AND nvl(cond, 'null') = nvl(c.cond, 'null') AND nvl(STATE_ACTION_NM, 'null') = nvl(c.STATE_ACTION_NM, 'null') then
        return 0;
     else
        return -1;
     end if;
  END;
end;
/

ALTER TYPE o_state_transition COMPILE DEBUG ;
ALTER TYPE o_state_transition COMPILE DEBUG BODY;

create or replace type t_state_transition as table of o_state_transition;
/


create or replace type o_composite_action_rule as object(
CHILD_SVC_ACTION_NM   varchar2(255),
SVC_ACTION_SEQ        number,
TMPLT_EXEC_PRIORITY   number,
MEMBER PROCEDURE     DIFF (c o_composite_action_rule),
ORDER MEMBER FUNCTION match (c o_composite_action_rule) RETURN INTEGER );
/

create or replace type body o_composite_action_rule as
  MEMBER PROCEDURE     DIFF (c o_composite_action_rule) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_composite_action_rule) RETURN INTEGER is
  begin
     if CHILD_SVC_ACTION_NM    = c.CHILD_SVC_ACTION_NM AND
        SVC_ACTION_SEQ         = c.SVC_ACTION_SEQ      AND
        TMPLT_EXEC_PRIORITY    = c.TMPLT_EXEC_PRIORITY THEN
       return 0;
     else
       return -1;
     end if;
  end;
end;
/

ALTER TYPE o_composite_action_rule  COMPILE DEBUG ;
ALTER TYPE o_composite_action_rule  COMPILE DEBUG BODY;

create or replace type t_composite_action_rule as table of o_composite_action_rule;
/

create or replace type o_ntwk_action as object (
NTWK_ACTION_NM        VARCHAR2(100),
NTWK_ACTION_TYP_NM    VARCHAR2(100),
SUBNTWK_TYP_NM        VARCHAR2(100),
IS_SSN_CALC_REQUIRED  CHAR(1),
EXECUTION_SEQ         NUMBER(9),
MEMBER PROCEDURE     DIFF (c o_ntwk_action ),
ORDER MEMBER FUNCTION match (c o_ntwk_action) RETURN INTEGER );
/

create or replace type body o_ntwk_action as
  MEMBER PROCEDURE     DIFF (c o_ntwk_action ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_ntwk_action) RETURN INTEGER is
  begin
    if nvl(NTWK_ACTION_NM, 'null') = c.NTWK_ACTION_NM AND NTWK_ACTION_TYP_NM = c.NTWK_ACTION_TYP_NM AND
       nvl(SUBNTWK_TYP_NM, 'null') = nvl(c.SUBNTWK_TYP_NM, 'null') AND IS_SSN_CALC_REQUIRED = c.IS_SSN_CALC_REQUIRED AND
       nvl(EXECUTION_SEQ, -1)  = nvl(c.EXECUTION_SEQ, -1)
    then
       return 0;
    else
       return -1;
    end if;
  end;
end;
/

ALTER TYPE o_ntwk_action  COMPILE DEBUG ;
ALTER TYPE o_ntwk_action  COMPILE DEBUG BODY;

create or replace type o_parm_chg_ntwk_impact as object (
NTWK_ACTION            o_ntwk_action,
PARM_NM                varchar2(255),
SVC_PARM_CHG_TYP       CHAR(1),
EXECUTION_SEQ          NUMBER(9),
JMF_COND               VARCHAR2(4000),
MEMBER PROCEDURE     DIFF (c o_parm_chg_ntwk_impact ),
ORDER MEMBER FUNCTION match (c o_parm_chg_ntwk_impact) RETURN INTEGER );
/

create or replace type body o_parm_chg_ntwk_impact as
  MEMBER PROCEDURE     DIFF (c o_parm_chg_ntwk_impact ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_parm_chg_ntwk_impact) RETURN INTEGER is
  begin
     if NTWK_ACTION       = c.NTWK_ACTION     AND nvl(PARM_NM, 'null') = nvl(c.PARM_NM, 'null') AND
        nvl(SVC_PARM_CHG_TYP, 'null')  = nvl(c.SVC_PARM_CHG_TYP, 'null') AND
        nvl(EXECUTION_SEQ, -1)     = nvl(c.EXECUTION_SEQ, -1)  AND
        nvl(JMF_COND, 'null')      = nvl(c.JMF_COND, 'null')
     then
       return 0;
     else
       return -1;
     end if;
  end;
end;
/

ALTER TYPE o_parm_chg_ntwk_impact  COMPILE DEBUG ;
ALTER TYPE o_parm_chg_ntwk_impact  COMPILE DEBUG BODY;

create or replace type t_parm_chg_ntwk_impact as table of o_parm_chg_ntwk_impact;
/


create or replace type o_svc_action_exclusion as object (
EXCLUDED_SVC_ACTION_NM VARCHAR2(255),
JMF_COND               VARCHAR2(4000),
IS_DISTINCT            CHAR(1),
MEMBER PROCEDURE     DIFF (c o_svc_action_exclusion ),
ORDER MEMBER FUNCTION match (c o_svc_action_exclusion) RETURN INTEGER );
/

create or replace type body o_svc_action_exclusion as
  MEMBER PROCEDURE     DIFF (c o_svc_action_exclusion ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_action_exclusion) RETURN INTEGER is
  begin
    if EXCLUDED_SVC_ACTION_NM  = c.EXCLUDED_SVC_ACTION_NM AND
       nvl(JMF_COND, 'null') = nvl(c.JMF_COND, 'null') AND
       IS_DISTINCT = c.IS_DISTINCT
    then
       return 0;
    else
       return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_action_exclusion  COMPILE DEBUG ;
ALTER TYPE o_svc_action_exclusion  COMPILE DEBUG BODY;

create or replace type t_svc_action_exclusion as table of o_svc_action_exclusion ;
/

create or replace type o_svc_impact as object (
SEQ_NUM    NUMBER(12),
TMPLT_NM   VARCHAR2(255),
PRIORITY   NUMBER(9),
IMPACT_TYP CHAR(1),
COND       VARCHAR2(2000),
MEMBER PROCEDURE     DIFF (c o_svc_impact ) ,
ORDER MEMBER FUNCTION match (c o_svc_impact) RETURN INTEGER );
/

create or replace type body o_svc_impact as
  MEMBER PROCEDURE     DIFF (c o_svc_impact ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_impact) RETURN INTEGER is
  begin
    if SEQ_NUM    = c.SEQ_NUM    AND TMPLT_NM   = c.TMPLT_NM   AND
       PRIORITY   = c.PRIORITY   AND IMPACT_TYP = c.IMPACT_TYP AND
       nvl(COND, 'null')       = nvl(c.COND, 'null')
    then
       return 0;
    else
       return -1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_impact COMPILE DEBUG ;
ALTER TYPE o_svc_impact COMPILE DEBUG BODY;

create or replace type t_svc_impact as table of o_svc_impact;
/

create or replace type o_SVC_PARM_CHG_GRP_DTL as object (
parm_nm              VARCHAR2(255),
svc_parm_chg_typ     CHAR(1),
MEMBER PROCEDURE     DIFF (c o_SVC_PARM_CHG_GRP_DTL ) ,
ORDER MEMBER FUNCTION match (c o_SVC_PARM_CHG_GRP_DTL) RETURN INTEGER );
/

create or replace type body o_SVC_PARM_CHG_GRP_DTL as
 MEMBER PROCEDURE     DIFF (c o_SVC_PARM_CHG_GRP_DTL ) is
 begin
    null;
 end;

 ORDER MEMBER FUNCTION match (c o_SVC_PARM_CHG_GRP_DTL) RETURN INTEGER is
 begin
    if parm_nm = c.parm_nm AND svc_parm_chg_typ  = c.svc_parm_chg_typ then
       return 0;
    else
       return -1;
    end if;
 end;
end;
/

ALTER TYPE o_SVC_PARM_CHG_GRP_DTL COMPILE DEBUG ;
ALTER TYPE o_SVC_PARM_CHG_GRP_DTL COMPILE DEBUG BODY;

create or replace type t_SVC_PARM_CHG_GRP_DTL as table of o_SVC_PARM_CHG_GRP_DTL ;
/


create or replace type o_SVC_PARM_CHG_GRP as object (
svc_action_grp   t_varchar255,
IS_OVERRIDE CHAR(1),
IS_AND_GRP  CHAR(1),
SVC_PARM_CHG_GRP_DTLS t_SVC_PARM_CHG_GRP_DTL ,
MEMBER PROCEDURE     DIFF (c o_SVC_PARM_CHG_GRP ) ,
ORDER MEMBER FUNCTION match (c o_SVC_PARM_CHG_GRP) RETURN INTEGER );
/

create or replace type body o_SVC_PARM_CHG_GRP as
  MEMBER PROCEDURE     DIFF (c o_SVC_PARM_CHG_GRP ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_SVC_PARM_CHG_GRP) RETURN INTEGER is
     n number;
  begin
    if svc_action_grp.count = c.svc_action_grp.count AND IS_OVERRIDE  = c.IS_OVERRIDE  AND
       IS_AND_GRP           = c.IS_AND_GRP AND SVC_PARM_CHG_GRP_DTLS.count = c.SVC_PARM_CHG_GRP_DTLS.count
    then
       select count(*) into n from table(svc_action_grp) t1, table(c.svc_action_grp) t2
         where value(t1) = value(t2);
       if n <> svc_action_grp.count then
          return -1;
       end if;

       select count(*) into n from table(SVC_PARM_CHG_GRP_DTLS) t1, table(c.SVC_PARM_CHG_GRP_DTLS) t2
         where value(t1) = value(t2);

       if n <> SVC_PARM_CHG_GRP_DTLS.count then
          return -1;
       end if;

       return 0;
    else
       return -1;
    end if;
  end;
end;
/

ALTER TYPE o_SVC_PARM_CHG_GRP COMPILE DEBUG ;
ALTER TYPE o_SVC_PARM_CHG_GRP COMPILE DEBUG BODY;

create or replace type o_svc_parm_chg_grp_impact as object (
SVC_PARM_CHG_GRP_ID o_SVC_PARM_CHG_GRP,
SEQ_NUM             NUMBER(3),
JMF_COND            VARCHAR2(2000),
PRIORITY            NUMBER(9),
MEMBER PROCEDURE     DIFF (c o_svc_parm_chg_grp_impact ),
ORDER MEMBER FUNCTION match (c o_svc_parm_chg_grp_impact) RETURN INTEGER );
/

create or replace type body o_svc_parm_chg_grp_impact as
  MEMBER PROCEDURE     DIFF (c o_svc_parm_chg_grp_impact ) is
  begin
    null;
  end;
 ORDER MEMBER FUNCTION match (c o_svc_parm_chg_grp_impact) RETURN INTEGER is
 begin
    if SVC_PARM_CHG_GRP_ID  = c.SVC_PARM_CHG_GRP_ID AND SEQ_NUM = c.SEQ_NUM AND
       nvl(JMF_COND, 'null') = nvl(c.JMF_COND, 'null') AND PRIORITY = c.PRIORITY
    then
       return 0;
    else
       return -1;
    end if;
 end;
end;
/

ALTER TYPE o_svc_parm_chg_grp_impact COMPILE DEBUG ;
ALTER TYPE o_svc_parm_chg_grp_impact COMPILE DEBUG BODY;

create or replace type t_svc_parm_chg_grp_impact as table of o_svc_parm_chg_grp_impact;
/

create or replace type o_svc_parm_chg_impact as object (
PARM_NM          VARCHAR2(255),
SVC_PARM_CHG_TYP CHAR(1),
SEQ_NUM          NUMBER(3),
PRIORITY         NUMBER(9),
JMF_COND         VARCHAR2(2000),
MEMBER PROCEDURE     DIFF (c o_svc_parm_chg_impact ) ,
ORDER MEMBER FUNCTION match (c o_svc_parm_chg_impact) RETURN INTEGER );
/

create or replace type body o_svc_parm_chg_impact as
  MEMBER PROCEDURE     DIFF (c o_svc_parm_chg_impact ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_parm_chg_impact) RETURN INTEGER as
  begin
    if SVC_PARM_CHG_TYP = c.SVC_PARM_CHG_TYP AND PARM_NM  = c.PARM_NM
       AND SEQ_NUM  = c.SEQ_NUM AND nvl(PRIORITY, -1) = nvl(c.PRIORITY, -1) AND
       nvl(JMF_COND, 'null') = nvl(c.JMF_COND, 'null')
    then
       return 0;
    else
       return -1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_parm_chg_impact  COMPILE DEBUG ;
ALTER TYPE o_svc_parm_chg_impact  COMPILE DEBUG BODY;

create or replace type t_svc_parm_chg_impact as table of o_svc_parm_chg_impact ;
/



create or replace type o_svc_action as object (
SVC_ACTION_NM      VARCHAR2(255),
SVC_ACTION_GRP_NM  VARCHAR2(255),
EXEC_COST_NM       VARCHAR2(255),
IS_REQUIRED        CHAR(1)      ,
SVC_ACTION_TYP     CHAR(1)      ,
BASE_SVC_ACTION_NM VARCHAR2(255),
composite_action_rules   t_composite_action_rule,
parm_chg_ntwk_impacts    t_parm_chg_ntwk_impact,
svc_action_data_SRCS     t_varchar255,
svc_action_exclusions    t_svc_action_exclusion,
svc_impacts              t_svc_impact ,
svc_parm_chg_grp_impacts t_svc_parm_chg_grp_impact,
svc_parm_chg_impacts     t_svc_parm_chg_impact,
MEMBER PROCEDURE     DIFF (c o_svc_action ) ,
ORDER MEMBER FUNCTION match (c o_svc_action) RETURN INTEGER );
/

create or replace type body o_svc_action as
  MEMBER PROCEDURE     DIFF (c o_svc_action ) is
  begin
    null;
  end;
   ORDER MEMBER FUNCTION match (c o_svc_action) RETURN INTEGER is
     n number;
   begin
      if SVC_ACTION_NM  = c.SVC_ACTION_NM
         AND nvl(SVC_ACTION_GRP_NM, 'null') = nvl(c.SVC_ACTION_GRP_NM, 'null')
         AND nvl(EXEC_COST_NM, 'null')  = nvl(c.EXEC_COST_NM, 'null')
         AND IS_REQUIRED  = c.IS_REQUIRED
         AND SVC_ACTION_TYP  = c.SVC_ACTION_TYP AND BASE_SVC_ACTION_NM = c.BASE_SVC_ACTION_NM
         AND composite_action_rules.count = c.composite_action_rules.count
         AND parm_chg_ntwk_impacts.count = c.parm_chg_ntwk_impacts.count
         AND svc_action_data_SRCS.count = c.svc_action_data_SRCS.count
         AND svc_action_exclusions.count = c.svc_action_exclusions.count
         AND svc_impacts.count = c.svc_impacts.count
         AND svc_parm_chg_grp_impacts.count = c.svc_parm_chg_grp_impacts.count
         AND svc_parm_chg_impacts.count = c.svc_parm_chg_impacts.count
      then
         select count(*) into n from table(composite_action_rules  ) t1, table(c.composite_action_rules  ) t2 where value(t1) = value(t2); if n <> composite_action_rules  .count then return -1; end if;
         select count(*) into n from table(parm_chg_ntwk_impacts   ) t1, table(c.parm_chg_ntwk_impacts   ) t2 where value(t1) = value(t2); if n <> parm_chg_ntwk_impacts   .count then return -1; end if;
         select count(*) into n from table(svc_action_data_SRCS    ) t1, table(c.svc_action_data_SRCS    ) t2 where value(t1) = value(t2); if n <> svc_action_data_SRCS    .count then return -1; end if;
         select count(*) into n from table(svc_action_exclusions   ) t1, table(c.svc_action_exclusions   ) t2 where value(t1) = value(t2); if n <> svc_action_exclusions   .count then return -1; end if;
         select count(*) into n from table(svc_impacts             ) t1, table(c.svc_impacts             ) t2 where value(t1) = value(t2); if n <> svc_impacts             .count then return -1; end if;
         select count(*) into n from table(svc_parm_chg_grp_impacts) t1, table(c.svc_parm_chg_grp_impacts) t2 where value(t1) = value(t2); if n <> svc_parm_chg_grp_impacts.count then return -1; end if;
         select count(*) into n from table(svc_parm_chg_impacts    ) t1, table(c.svc_parm_chg_impacts    ) t2 where value(t1) = value(t2); if n <> svc_parm_chg_impacts    .count then return -1; end if;
         return 0;
      else
         return -1;
      end if;
   end;
end;
/

ALTER TYPE o_svc_action COMPILE DEBUG ;
ALTER TYPE o_svc_action COMPILE DEBUG BODY;

create or replace type t_svc_action as table of o_svc_action ;
/

create or replace type o_svc_avail_constr as object (
SVC_AVAIL_CONSTR_NM  VARCHAR2(255),
SUB_TYP_NM           VARCHAR2(255),
MANUAL_CONSTRAINT_NM VARCHAR2(255),
CHECK_NTWK_AVAIL     CHAR(1)      ,
JMF_COND             VARCHAR2(2000),
MEMBER PROCEDURE     DIFF (c o_svc_avail_constr),
ORDER MEMBER FUNCTION match (c o_svc_avail_constr) RETURN INTEGER );
/

create or replace type body o_svc_avail_constr as
  MEMBER PROCEDURE     DIFF (c o_svc_avail_constr) is
  begin
    null;
  end;
 ORDER MEMBER FUNCTION match (c o_svc_avail_constr) RETURN INTEGER is
 begin
    if SVC_AVAIL_CONSTR_NM  = c.SVC_AVAIL_CONSTR_NM
       AND nvl(SUB_TYP_NM, 'null') = nvl(c.SUB_TYP_NM, 'null')
       AND nvl(MANUAL_CONSTRAINT_NM, 'null') = nvl(c.MANUAL_CONSTRAINT_NM, 'null')
       AND CHECK_NTWK_AVAIL = c.CHECK_NTWK_AVAIL
       AND nvl(JMF_COND, 'null') = nvl(c.JMF_COND, 'null')
    then
       return 0;
    else
       return 1;
    end if;
 end;
end;
/

ALTER TYPE o_svc_avail_constr  COMPILE DEBUG ;
ALTER TYPE o_svc_avail_constr  COMPILE DEBUG BODY;

create or replace type t_svc_avail_constr as table of o_svc_avail_constr ;
/

create or replace type o_svc_composition_rule as object (
COMPONENT_SVC_NM VARCHAR2(255),
CAPTURE_SEQ_NUM  NUMBER(12),
MIN_INSTANCE_NUM NUMBER(9),
MAX_INSTANCE_NUM NUMBER(9),
MEMBER PROCEDURE     DIFF (c o_svc_composition_rule),
ORDER MEMBER FUNCTION match (c o_svc_composition_rule) RETURN INTEGER );
/

create or replace type body o_svc_composition_rule as
  MEMBER PROCEDURE     DIFF (c o_svc_composition_rule) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_composition_rule) RETURN INTEGER is
  begin
    if COMPONENT_SVC_NM = c.COMPONENT_SVC_NM AND CAPTURE_SEQ_NUM  = c.CAPTURE_SEQ_NUM
       AND MIN_INSTANCE_NUM = c.MIN_INSTANCE_NUM AND MAX_INSTANCE_NUM = c.MAX_INSTANCE_NUM
    then
       return 0;
    else
       return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_composition_rule  COMPILE DEBUG ;
ALTER TYPE o_svc_composition_rule  COMPILE DEBUG BODY;

create or replace type t_svc_composition_rule as table of o_svc_composition_rule ;
/


create or replace type o_svc_depy_rule as object (
DEPENDENT_SVC_NM     VARCHAR2(255),
SCOPE_SVC_NM         VARCHAR2(255),
IS_INSTANCE          CHAR(1)      ,
OR_GRP_ID            NUMBER(9)    ,
FORCE_CASCADE_DELETE CHAR(1)      ,
IS_AUTO              VARCHAR2(1)  ,
JMF_COND             VARCHAR2(4000),
MEMBER PROCEDURE     DIFF (c o_svc_depy_rule) ,
ORDER MEMBER FUNCTION match (c o_svc_depy_rule) RETURN INTEGER );
/

create or replace type body o_svc_depy_rule as
  MEMBER PROCEDURE     DIFF (c o_svc_depy_rule) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_depy_rule) RETURN INTEGER is
  begin
    if DEPENDENT_SVC_NM     = c.DEPENDENT_SVC_NM
       AND nvl(SCOPE_SVC_NM, 'null') = nvl(c.SCOPE_SVC_NM, 'null')
       AND IS_INSTANCE          = c.IS_INSTANCE
       AND nvl(OR_GRP_ID, -1)   = nvl(c.OR_GRP_ID, -1)
       AND FORCE_CASCADE_DELETE = c.FORCE_CASCADE_DELETE
       AND IS_AUTO  = c.IS_AUTO
       AND nvl(JMF_COND, 'null') = nvl(c.JMF_COND, 'null')
    then
       return 0;
    else
       return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_depy_rule  COMPILE DEBUG ;
ALTER TYPE o_svc_depy_rule  COMPILE DEBUG BODY;

create or replace type t_svc_depy_rule as table of o_svc_depy_rule ;
/


create or replace type o_svc_exclusion_grp as object(
SVC_EXCLUSION_GRP_NM VARCHAR2(255),
SCOPE_SVC_NM         VARCHAR2(255),
MEMBER PROCEDURE     DIFF (c o_svc_exclusion_grp ) ,
ORDER MEMBER FUNCTION match (c o_svc_exclusion_grp) RETURN INTEGER );
/

create or replace type body o_svc_exclusion_grp as
  MEMBER PROCEDURE     DIFF (c o_svc_exclusion_grp ) is
  begin
    null;
  end;
 ORDER MEMBER FUNCTION match (c o_svc_exclusion_grp) RETURN INTEGER is
 begin
    if SVC_EXCLUSION_GRP_NM = c.SVC_EXCLUSION_GRP_NM AND nvl(SCOPE_SVC_NM, 'null') = nvl(c.SCOPE_SVC_NM, 'null')
    then
      return 0;
    else
      return 1;
    end if;
 end;
end;
/

ALTER TYPE o_svc_exclusion_grp  COMPILE DEBUG ;
ALTER TYPE o_svc_exclusion_grp  COMPILE DEBUG BODY;

create or replace type t_svc_exclusion_grp as table of o_svc_exclusion_grp ;
/


create or replace type o_svc_migr_comp_rule as object(
MIGR_SEQ     NUMBER,
SVC_MIGR_NM  varchar2(100),
MEMBER PROCEDURE     DIFF (c o_svc_migr_comp_rule ),
ORDER MEMBER FUNCTION match (c o_svc_migr_comp_rule  ) RETURN INTEGER );
/

create or replace type body o_svc_migr_comp_rule as
  MEMBER PROCEDURE     DIFF (c o_svc_migr_comp_rule ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_migr_comp_rule  ) RETURN INTEGER is
  begin
    if  MIGR_SEQ    = c.MIGR_SEQ   AND SVC_MIGR_NM = c.SVC_MIGR_NM
    then
       return 0;
    else
       return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_migr_comp_rule  COMPILE DEBUG ;
ALTER TYPE o_svc_migr_comp_rule  COMPILE DEBUG BODY;

create or replace type t_svc_migr_comp_rule as table of o_svc_migr_comp_rule ;
/

create or replace type o_svc_migr_parm_src as object (
CAPTURE_SEQ      NUMBER(12)   ,
SCOPE_SVC_NM     VARCHAR2(255),
MIGR_PARM_NM     VARCHAR2(255),
SRC_SCOPE_SVC_NM VARCHAR2(255),
SRC_CAPTURE_SEQ  NUMBER(12)   ,
SRC_PARM_NM      VARCHAR2(255),
SRC_JMF_EXPR     VARCHAR2(1000),
MEMBER PROCEDURE     DIFF (c o_svc_migr_parm_src ) ,
ORDER MEMBER FUNCTION match (c o_svc_migr_parm_src) RETURN INTEGER );
/

create or replace type body o_svc_migr_parm_src as
  MEMBER PROCEDURE     DIFF (c o_svc_migr_parm_src ) is
  begin
    null;
  end;
 ORDER MEMBER FUNCTION match (c o_svc_migr_parm_src) RETURN INTEGER is
 begin
    if  CAPTURE_SEQ      = c.CAPTURE_SEQ     AND
        nvl(SCOPE_SVC_NM    , 'null') = nvl(c.SCOPE_SVC_NM    , 'null')  AND
        nvl(MIGR_PARM_NM    , 'null') = nvl(c.MIGR_PARM_NM    , 'null')  AND
        nvl(SRC_SCOPE_SVC_NM, 'null') = nvl(c.SRC_SCOPE_SVC_NM , 'null') AND
        nvl(SRC_CAPTURE_SEQ , -1) = nvl(c.SRC_CAPTURE_SEQ , -1)  AND
        nvl(SRC_PARM_NM     , 'null') = nvl(c.SRC_PARM_NM     , 'null')  AND
        nvl(SRC_JMF_EXPR    , 'null') = nvl(c.SRC_JMF_EXPR,   'null')
     then
        return 0;
     else
        return 1;
     end if;
 end;
end;
/

ALTER TYPE o_svc_migr_parm_src  COMPILE DEBUG ;
ALTER TYPE o_svc_migr_parm_src  COMPILE DEBUG BODY;

create or replace type t_svc_migr_parm_src as table of o_svc_migr_parm_src ;
/


create or replace type o_svc_migr as object (
MIGR_ACTION_NM    VARCHAR2(30),
SVC_MIGR_NM       VARCHAR2(100),
FROM_SCOPE_SVC_NM VARCHAR2(255),
FROM_SVC_NM       VARCHAR2(255),
TO_SCOPE_SVC_NM   VARCHAR2(255),
TO_SVC_NM         VARCHAR2(255),
IS_EXECUTABLE     CHAR(1),
svc_migr_comp_rules t_svc_migr_comp_rule,
svc_migr_parm_srcs  t_svc_migr_parm_src,
MEMBER PROCEDURE     DIFF (c o_svc_migr) ,
ORDER MEMBER FUNCTION match (c o_svc_migr ) RETURN INTEGER );
/

create or replace type body o_svc_migr as
  MEMBER PROCEDURE     DIFF (c o_svc_migr) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_migr ) RETURN INTEGER is
    n number;
  begin
    if MIGR_ACTION_NM     = c.MIGR_ACTION_NM  AND SVC_MIGR_NM        = c.SVC_MIGR_NM AND
       nvl(FROM_SCOPE_SVC_NM, 'null')  = nvl(c.FROM_SCOPE_SVC_NM, 'null') AND
       nvl(FROM_SVC_NM, 'null')        = nvl(c.FROM_SVC_NM, 'null') AND
       nvl(TO_SCOPE_SVC_NM, 'null')    = nvl(c.TO_SCOPE_SVC_NM, 'null') AND
       nvl(TO_SVC_NM, 'null')          = nvl(c.TO_SVC_NM, 'null') AND
       IS_EXECUTABLE      = c.IS_EXECUTABLE AND
       svc_migr_comp_rules.count = c.svc_migr_comp_rules.count AND
       svc_migr_parm_srcs.count = c.svc_migr_parm_srcs.count
    then
      select count(*) into n from table(svc_migr_comp_rules ) t1, table(c.svc_migr_comp_rules  ) t2 where value(t1) = value(t2); if n <> svc_migr_comp_rules.count then return -1; end if;
      select count(*) into n from table(svc_migr_parm_srcs ) t1, table(c.svc_migr_parm_srcs  ) t2 where value(t1) = value(t2); if n <> svc_migr_parm_srcs.count then return -1; end if;
      return 0;
    else
      return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_migr  COMPILE DEBUG ;
ALTER TYPE o_svc_migr  COMPILE DEBUG BODY;

create or replace type t_svc_migr as table of o_svc_migr ;
/


create or replace type o_nep_info as object (
NEP_JMF_EXPR         VARCHAR2(4000),
MEMBER PROCEDURE     DIFF (c o_nep_info ),
ORDER MEMBER FUNCTION match (c o_nep_info) RETURN INTEGER );
/

create or replace type body o_nep_info as
  MEMBER PROCEDURE     DIFF (c o_nep_info ) is
  begin
    null;
  end;

 ORDER MEMBER FUNCTION match (c o_nep_info) RETURN INTEGER is
 begin
    if  NEP_JMF_EXPR  = c.NEP_JMF_EXPR
    then
        return 0;
    else
        return 1;
    end if;

 end;
end;
/

ALTER TYPE o_nep_info  COMPILE DEBUG ;
ALTER TYPE o_nep_info  COMPILE DEBUG BODY;

create or replace type t_nep_info as table of o_nep_info;
/



create or replace type o_svc_platform_subntwk as object (
SUBNTWK_TYP_NM  VARCHAR2(100),
NTWK_ROLE_NM    VARCHAR2(255),
MGMT_MODE_NM    VARCHAR2(255),
nep_infos       t_nep_info   ,
MEMBER PROCEDURE     DIFF (c o_svc_platform_subntwk ) ,
ORDER MEMBER FUNCTION match (c o_svc_platform_subntwk) RETURN INTEGER );
/

create or replace type body o_svc_platform_subntwk as
  MEMBER PROCEDURE     DIFF (c o_svc_platform_subntwk ) is
  begin
    null;
  end;

  ORDER MEMBER FUNCTION match (c o_svc_platform_subntwk) RETURN INTEGER is
    n number;
  begin
    if  SUBNTWK_TYP_NM = c.SUBNTWK_TYP_NM AND
        NTWK_ROLE_NM   = c.NTWK_ROLE_NM   AND
        MGMT_MODE_NM   = c.MGMT_MODE_NM   AND
        nep_infos.count= c.nep_infos.count
    then
       select count(*) into n from table(nep_infos) t1, table(c.nep_infos) t2 where value(t1) = value(t2); if n <> nep_infos.count then return -1; end if;
       return 0;
    else
       return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_platform_subntwk  COMPILE DEBUG ;
ALTER TYPE o_svc_platform_subntwk  COMPILE DEBUG BODY;

create or replace type t_svc_platform_subntwk as table of o_svc_platform_subntwk;
/

create or replace type o_svc_supported_platform as object (
SVC_DELIVERY_PLAT_NM VARCHAR2(255),
IP_MGMT_MODE_NM      VARCHAR2(30),
PRIORITY             NUMBER(9),
JMF_EXPR             VARCHAR2(4000),
svc_platform_subntwks t_svc_platform_subntwk,
MEMBER PROCEDURE     DIFF (c o_svc_supported_platform ) ,
ORDER MEMBER FUNCTION match (c o_svc_supported_platform ) RETURN INTEGER );
/

create or replace type body o_svc_supported_platform as
  MEMBER PROCEDURE     DIFF (c o_svc_supported_platform ) is
  begin
    null;
  end;
  ORDER MEMBER FUNCTION match (c o_svc_supported_platform ) RETURN INTEGER is
    n number;
  begin
    if  SVC_DELIVERY_PLAT_NM = c.SVC_DELIVERY_PLAT_NM AND
        nvl(IP_MGMT_MODE_NM , 'null')     = nvl(c.IP_MGMT_MODE_NM  , 'null')    AND
        nvl(PRIORITY        , -1)         = nvl(c.PRIORITY         , -1)        AND
        nvl(JMF_EXPR        , 'null')     = nvl(c.JMF_EXPR         , 'null')    AND
        svc_platform_subntwks.count = c.svc_platform_subntwks.count
    then
       select count(*) into n from table(svc_platform_subntwks) t1, table(c.svc_platform_subntwks) t2 where value(t1) = value(t2); if n <> svc_platform_subntwks.count then return -1; end if;
       return 0;
    else
       return 1;
    end if;
  end;
end;
/

ALTER TYPE o_svc_supported_platform  COMPILE DEBUG ;
ALTER TYPE o_svc_supported_platform  COMPILE DEBUG BODY;

create or replace type t_svc_supported_platform as table of o_svc_supported_platform ;
/

create or replace type o_svc_vld_rule as object (
SEQ_NUM  NUMBER(3),
JMF_COND VARCHAR2(2000),
ERR_CD   VARCHAR2(255),
MEMBER PROCEDURE     DIFF (c o_svc_vld_rule ),
ORDER MEMBER FUNCTION match (c o_svc_vld_rule) RETURN INTEGER );
/

create or replace type body o_svc_vld_rule as
  MEMBER PROCEDURE     DIFF (c o_svc_vld_rule ) is
  begin
    null;
  end;
 ORDER MEMBER FUNCTION match (c o_svc_vld_rule) RETURN INTEGER is
 begin
    if  SEQ_NUM   = c.SEQ_NUM   AND
        nvl(JMF_COND, 'null')  = nvl(c.JMF_COND, 'null')  AND
        ERR_CD    = c.ERR_CD
    then
        return 0;
    else
        return 1;
    end if;
 end;
end;
/

ALTER TYPE o_svc_vld_rule COMPILE DEBUG ;
ALTER TYPE o_svc_vld_rule COMPILE DEBUG BODY;

create or replace type t_svc_vld_rule as table of o_svc_vld_rule ;
/

create or replace type o_parm_permitted_val as object (
JMF_PERMITTED_VAL_EXPR VARCHAR2(2000),
VAL                    VARCHAR2(1000),
MEMBER PROCEDURE     DIFF (c o_parm_permitted_val ),
ORDER MEMBER FUNCTION match (c o_parm_permitted_val) RETURN INTEGER );
/

create or replace type body o_parm_permitted_val as
 MEMBER PROCEDURE     DIFF (c o_parm_permitted_val ) is
 begin
   null;
 end;
 ORDER MEMBER FUNCTION match (c o_parm_permitted_val) RETURN INTEGER is
 begin
    if nvl(JMF_PERMITTED_VAL_EXPR , 'null') = nvl(c.JMF_PERMITTED_VAL_EXPR , 'null') AND
       nvl(VAL, 'null')                     = nvl(c.VAL, 'null')
    then
       return 0;
    else
       return -1;
    end if;
 end;
end;
/

ALTER TYPE o_parm_permitted_val COMPILE DEBUG ;
ALTER TYPE o_parm_permitted_val COMPILE DEBUG BODY;

create or replace type t_parm_permitted_val as table of o_parm_permitted_val ;
/


create or replace type o_vld_rule as object (
VLD_RULE_NM                VARCHAR2(255),
TEST_RESULT_INVERSION_IND  CHAR(1)      ,
MIN_INCLUSIVE              NUMBER       ,
MIN_EXCLUSIVE              NUMBER       ,
MAX_INCLUSIVE              NUMBER       ,
MAX_EXCLUSIVE              NUMBER       ,
MIN_CHAR_NUM               NUMBER(9)    ,
MAX_CHAR_NUM               NUMBER(9)    ,
ERR_CODE                   NUMBER(9)    ,
ERR_DFLT_MSG               VARCHAR2(255),
JMF_EXPR                   VARCHAR2(2000),
FORMAT                     VARCHAR2(100),
MEMBER PROCEDURE     DIFF (c o_vld_rule ),
ORDER MEMBER FUNCTION match (c o_vld_rule) RETURN INTEGER );
/

create or replace type body o_vld_rule as
 MEMBER PROCEDURE     DIFF (c o_vld_rule) is
 begin
    null;
 end;
 ORDER MEMBER FUNCTION match (c o_vld_rule) RETURN INTEGER is
 begin
    if  nvl(VLD_RULE_NM, 'null')   = nvl(c.VLD_RULE_NM, 'null')  AND
        TEST_RESULT_INVERSION_IND  = c.TEST_RESULT_INVERSION_IND AND
        nvl(MIN_INCLUSIVE, -1)     = nvl(c.MIN_INCLUSIVE, -1)    AND
        nvl(MIN_EXCLUSIVE, -1)     = nvl(c.MIN_EXCLUSIVE, -1)    AND
        nvl(MAX_INCLUSIVE, -1)     = nvl(c.MAX_INCLUSIVE, -1)    AND
        nvl(MAX_EXCLUSIVE, -1)     = nvl(c.MAX_EXCLUSIVE, -1)    AND
        nvl(MIN_CHAR_NUM, -1)      = nvl(c.MIN_CHAR_NUM, -1)     AND
        nvl(MAX_CHAR_NUM, -1)      = nvl(c.MAX_CHAR_NUM, -1)     AND
        ERR_CODE                   = c.ERR_CODE                  AND
        ERR_DFLT_MSG               = c.ERR_DFLT_MSG              AND
        nvl(JMF_EXPR, 'null')      = nvl(c.JMF_EXPR, 'null')     AND
        nvl(FORMAT, 'null')        = nvl(c.FORMAT, 'null')
    then
       return 0;
    else
       return -1;
    end if;

 end;
end;
/

ALTER TYPE o_vld_rule COMPILE DEBUG ;
ALTER TYPE o_vld_rule COMPILE DEBUG BODY;

create or replace type t_vld_rule as table of o_vld_rule ;
/




create or replace type o_parm as object (
CONVERSION_TYP     VARCHAR2(30),
PARM_NM            VARCHAR2(255),
OBJECT_NM          VARCHAR2(255),
DATA_TYP_NM        VARCHAR2(30) ,
CAPTURE_SEQ_NUM    NUMBER(9)    ,
BASE_PARM_nm       VARCHAR2(255),
IS_READ_ONLY       CHAR(1)      ,
IS_REQUIRED        CHAR(1)      ,
IS_PRIMARY_KEY     CHAR(1)      ,
IS_RESERVED        CHAR(1)      ,
IS_UNIQUE          CHAR(1)      ,
DFLT_VAL           VARCHAR2(255),
JMF_SRC            VARCHAR2(2000),
JMF_DFLT_VAL       VARCHAR2(2000),
JMF_DISPLAY_FORMAT VARCHAR2(4000),
PARM_SRC_NM        VARCHAR2(30),
DB_COL_NM          VARCHAR2(30),
parm_permitted_vals t_parm_permitted_val,
vld_rules           t_vld_rule,
MEMBER PROCEDURE     DIFF (c o_parm ),
ORDER MEMBER FUNCTION match (c o_parm) RETURN INTEGER );
/

create or replace type body o_parm as
 MEMBER PROCEDURE     DIFF (c o_parm )is
 begin
    null;
 end;
 ORDER MEMBER FUNCTION match (c o_parm) RETURN INTEGER is
   n number;
 begin
    if  nvl(CONVERSION_TYP, 'null')     = nvl(c.CONVERSION_TYP, 'null')     AND
        PARM_NM                         = c.PARM_NM                         AND
      --  nvl(OBJECT_NM, 'null')          = nvl(c.OBJECT_NM, 'null')          AND
        DATA_TYP_NM                     = c.DATA_TYP_NM                     AND
        nvl(CAPTURE_SEQ_NUM, -1)        = nvl(c.CAPTURE_SEQ_NUM, -1)        AND
        nvl(BASE_PARM_nm, 'null')       = nvl(c.BASE_PARM_nm, 'null')       AND
        nvl(IS_READ_ONLY, 'null')       = nvl(c.IS_READ_ONLY, 'null')       AND
        nvl(IS_REQUIRED, 'null')        = nvl(c.IS_REQUIRED, 'null')        AND
        nvl(IS_PRIMARY_KEY, 'null')     = nvl(c.IS_PRIMARY_KEY, 'null')     AND
        nvl(IS_RESERVED, 'null')        = nvl(c.IS_RESERVED, 'null')        AND
        nvl(IS_UNIQUE, 'null')          = nvl(c.IS_UNIQUE, 'null')          AND
        nvl(DFLT_VAL, 'null')           = nvl(c.DFLT_VAL, 'null')           AND
        nvl(JMF_SRC , 'null')           = nvl(c.JMF_SRC , 'null')           AND
        nvl(JMF_DFLT_VAL, 'null')       = nvl(c.JMF_DFLT_VAL, 'null')       AND
        nvl(JMF_DISPLAY_FORMAT, 'null') = nvl(c.JMF_DISPLAY_FORMAT, 'null') AND
        nvl(PARM_SRC_NM, 'null')        = nvl(c.PARM_SRC_NM, 'null')        AND
        nvl(DB_COL_NM, 'null')          = nvl(c.DB_COL_NM, 'null')          AND
        parm_permitted_vals.count       = c.parm_permitted_vals.count       AND
        vld_rules.count                 = c.vld_rules.count
    then
      select count(*) into n from table(parm_permitted_vals) t1, table(c.parm_permitted_vals) t2 where value(t1) = value(t2); if n <> parm_permitted_vals.count then return -1; end if;
      select count(*) into n from table(vld_rules) t1, table(c.vld_rules) t2 where value(t1) = value(t2); if n <> vld_rules.count then return -1; end if;
      return 0;
    else
      return -1;
    end if;

 end;
end;
/

ALTER TYPE o_parm COMPILE DEBUG ;
ALTER TYPE o_parm COMPILE DEBUG BODY;

create or replace type t_parm as table of o_parm ;
/


create or replace type o_svc as object (
SVC_NM                VARCHAR2(255) ,
IS_ENTRY_POINT        CHAR(1)       ,
ACCESS_SCOPE          VARCHAR2(30)  ,
IS_ORDERABLE          CHAR(1)       ,
NEEDS_SVC_ADDR        CHAR(1)       ,
REQUIRED_SVC_ADDR     NUMBER(9)     ,
EFF_DTM               DATE          ,
EXP_DTM               DATE          ,
MIN_PROF_INSTANCE_NUM NUMBER(9)     ,
MAX_PROF_INSTANCE_NUM NUMBER(9)     ,
HAS_NTWK_IMPACT       CHAR(1)       ,
CHECK_SERVICEABILITY  CHAR(1)       ,
SVC_STATE             CHAR(1)       ,
base_svc_nm           VARCHAR2(255) ,
DATA_SRC_NM           VARCHAR2(30)  ,
ALIAS_RULES                 t_alias_rule,
FOLDERS                     t_folder_svc,
parm_instance_depy_rules    t_parm_instance_depy_rule,
state_actions               t_varchar255,
svc_actions                 t_svc_action,
svc_avail_constrs           t_svc_avail_constr,
svc_composition_rules       t_svc_composition_rule ,
svc_depy_rules              t_svc_depy_rule,
svc_exclusion_grps          t_svc_exclusion_grp,
svc_migr_tos                t_svc_migr,
svc_migr_froms              t_svc_migr,
svc_supported_platforms     t_svc_supported_platform,
svc_vld_rules               t_svc_vld_rule,
parms                       t_parm,
MEMBER PROCEDURE     DIFF (c o_svc) ,
ORDER MEMBER FUNCTION match (c o_svc) RETURN INTEGER );
/

create or replace type body o_svc as
 MEMBER PROCEDURE     DIFF (c o_svc) is
  n number;
 begin
   if SVC_NM                         <> c.SVC_NM                         then debug_log('SVC_NM                        :'|| SVC_NM                         ||'/'|| c.SVC_NM                        ); end if;
   if IS_ENTRY_POINT                 <> c.IS_ENTRY_POINT                 then debug_log('IS_ENTRY_POINT                :'|| IS_ENTRY_POINT                 ||'/'|| c.IS_ENTRY_POINT                ); end if;
   if nvl(ACCESS_SCOPE, 'null')      <> nvl(c.ACCESS_SCOPE, 'null')      then debug_log('ACCESS_SCOPE                  :'|| nvl(ACCESS_SCOPE, 'null')      ||'/'|| nvl(c.ACCESS_SCOPE, 'null')     ); end if;
   if IS_ORDERABLE                   <> c.IS_ORDERABLE                   then debug_log('IS_ORDERABLE                  :'|| IS_ORDERABLE                   ||'/'|| c.IS_ORDERABLE                  ); end if;
   if NEEDS_SVC_ADDR                 <> c.NEEDS_SVC_ADDR                 then debug_log('NEEDS_SVC_ADDR                :'|| NEEDS_SVC_ADDR                 ||'/'|| c.NEEDS_SVC_ADDR                ); end if;
   if nvl(REQUIRED_SVC_ADDR, -1)     <> nvl(c.REQUIRED_SVC_ADDR, -1)     then debug_log('REQUIRED_SVC_ADDR             :'|| nvl(REQUIRED_SVC_ADDR, -1)     ||'/'|| nvl(c.REQUIRED_SVC_ADDR, -1)    ); end if;
   if EFF_DTM                        <> c.EFF_DTM                        then debug_log('EFF_DTM                       :'|| EFF_DTM                        ||'/'|| c.EFF_DTM                       ); end if;
   if nvl(EXP_DTM, sysdate - 10000)  <> nvl(c.EXP_DTM, sysdate -  10000) then debug_log('EXP_DTM                       :'|| nvl(EXP_DTM, sysdate - 10000)  ||'/'|| nvl(c.EXP_DTM, sysdate -  10000)); end if;
   if nvl(MIN_PROF_INSTANCE_NUM, -1) <> nvl(c.MIN_PROF_INSTANCE_NUM, -1) then debug_log('MIN_PROF_INSTANCE_NUM         :'|| nvl(MIN_PROF_INSTANCE_NUM, -1) ||'/'|| nvl(c.MIN_PROF_INSTANCE_NUM, -1)); end if;
   if nvl(MAX_PROF_INSTANCE_NUM, -1) <> nvl(c.MAX_PROF_INSTANCE_NUM, -1) then debug_log('MAX_PROF_INSTANCE_NUM         :'|| nvl(MAX_PROF_INSTANCE_NUM, -1) ||'/'|| nvl(c.MAX_PROF_INSTANCE_NUM, -1)); end if;
   if HAS_NTWK_IMPACT                <> c.HAS_NTWK_IMPACT                then debug_log('HAS_NTWK_IMPACT               :'|| HAS_NTWK_IMPACT                ||'/'|| c.HAS_NTWK_IMPACT               ); end if;
   if CHECK_SERVICEABILITY           <> c.CHECK_SERVICEABILITY           then debug_log('CHECK_SERVICEABILITY          :'|| CHECK_SERVICEABILITY           ||'/'|| c.CHECK_SERVICEABILITY          ); end if;
   if SVC_STATE                      <> c.SVC_STATE                      then debug_log('SVC_STATE                     :'|| SVC_STATE                      ||'/'|| c.SVC_STATE                     ); end if;
   if nvl(base_svc_nm, 'null')       <> nvl(c.base_svc_nm, 'null')       then debug_log('base_svc_nm,                  :'|| nvl(base_svc_nm, 'null')       ||'/'|| nvl(c.base_svc_nm, 'null')      ); end if;
   if nvl(DATA_SRC_NM, 'null')       <> nvl(c.DATA_SRC_NM, 'null')       then debug_log('DATA_SRC_NM,                  :'|| nvl(DATA_SRC_NM, 'null')       ||'/'|| nvl(c.DATA_SRC_NM, 'null')      ); end if;
   if ALIAS_RULES.count              <> c.ALIAS_RULES.count              then debug_log('ALIAS_RULES.count             :'|| ALIAS_RULES.count              ||'/'|| c.ALIAS_RULES.count             ); end if;
   if FOLDERS.count                  <> c.FOLDERS.count                  then debug_log('FOLDERS.count                 :'|| FOLDERS.count                  ||'/'|| c.FOLDERS.count                 ); end if;
   if parm_instance_depy_rules.count <> c.parm_instance_depy_rules.count then debug_log('parm_instance_depy_rules.count:'|| parm_instance_depy_rules.count ||'/'|| c.parm_instance_depy_rules.count); end if;
   if state_actions.count            <> c.state_actions.count            then debug_log('state_actions.count           :'|| state_actions.count            ||'/'|| c.state_actions.count           ); end if;
   if svc_actions.count              <> c.svc_actions.count              then debug_log('svc_actions.count             :'|| svc_actions.count              ||'/'|| c.svc_actions.count             ); end if;
   if svc_avail_constrs.count        <> c.svc_avail_constrs.count        then debug_log('svc_avail_constrs.count       :'|| svc_avail_constrs.count        ||'/'|| c.svc_avail_constrs.count       ); end if;
   if svc_composition_rules.count    <> c.svc_composition_rules.count    then debug_log('svc_composition_rules.count   :'|| svc_composition_rules.count    ||'/'|| c.svc_composition_rules.count   ); end if;
   if svc_depy_rules.count           <> c.svc_depy_rules.count           then debug_log('svc_depy_rules.count          :'|| svc_depy_rules.count           ||'/'|| c.svc_depy_rules.count          ); end if;
   if svc_exclusion_grps.count       <> c.svc_exclusion_grps.count       then debug_log('svc_exclusion_grps.count      :'|| svc_exclusion_grps.count       ||'/'|| c.svc_exclusion_grps.count      ); end if;
   if svc_migr_tos.count             <> c.svc_migr_tos.count             then debug_log('svc_migr_tos.count            :'|| svc_migr_tos.count             ||'/'|| c.svc_migr_tos.count            ); end if;
   if svc_migr_froms.count           <> c.svc_migr_froms.count           then debug_log('svc_migr_froms.count          :'|| svc_migr_froms.count           ||'/'|| c.svc_migr_froms.count          ); end if;
   if svc_supported_platforms.count  <> c.svc_supported_platforms.count  then debug_log('svc_supported_platforms.count :'|| svc_supported_platforms.count  ||'/'|| c.svc_supported_platforms.count ); end if;
   if svc_vld_rules.count            <> c.svc_vld_rules.count            then debug_log('svc_vld_rules.count           :'|| svc_vld_rules.count            ||'/'|| c.svc_vld_rules.count           ); end if;
   if parms.count                    <> c.parms.count                    then debug_log('parms.count                   :'|| parms.count                    ||'/'|| c.parms.count                   ); end if;
   select count(*) into n from table(ALIAS_RULES             ) t1, table(c.ALIAS_RULES             ) t2 where value(t1) = value(t2); if n <> ALIAS_RULES             .count or n <> c.ALIAS_RULES             .count
   then
     debug_log(chr(10)||'Table :ALIAS_RULES             Common cnt:'||n ||'<> Left cnt:'|| ALIAS_RULES             .count||' Right cnt:'|| c.ALIAS_RULES             .count);
     for cr in (select value(t0) ALIAS_RULE from table(ALIAS_RULES) t0
                  where not exists (
                      select null from table(ALIAS_RULES) t1, table(c.ALIAS_RULES) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for ALIAS_RULES:'||cr.ALIAS_RULE.ALIAS_RULE_NM, 0);
     end loop;
     for cr in (select value(t0) ALIAS_RULE from table(c.ALIAS_RULES) t0
                  where not exists (
                      select null from table(c.ALIAS_RULES) t1, table(ALIAS_RULES) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for ALIAS_RULES:'||cr.ALIAS_RULE.ALIAS_RULE_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(FOLDERS                 ) t1, table(c.FOLDERS                 ) t2 where value(t1) = value(t2); if n <> FOLDERS                 .count or n <> c.FOLDERS                 .count
   then
     debug_log(chr(10)||'Table :FOLDERS                 Common cnt:'||n ||'<> Left cnt:'|| FOLDERS                 .count||' Right cnt:'||c.FOLDERS                 .count);
     for cr in (select value(t0) FOLDER from table(FOLDERS) t0
                  where not exists (
                      select null from table(FOLDERS) t1, table(c.FOLDERS) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for FOLDERS: '||cr.folder.folder_id, 0);
     end loop;
     for cr in (select value(t0) FOLDER from table(c.FOLDERS) t0
                  where not exists (
                      select null from table(c.FOLDERS) t1, table(FOLDERS) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for FOLDERS: '||cr.folder.folder_id, 0);
     end loop;
   end if;
   select count(*) into n from table(parm_instance_depy_rules) t1, table(c.parm_instance_depy_rules) t2 where value(t1) = value(t2); if n <> parm_instance_depy_rules.count or n <> c.parm_instance_depy_rules.count
   then
     debug_log(chr(10)||'Table :parm_instance_depy_rules Common cnt:'||n ||'<> Left cnt:'|| parm_instance_depy_rules.count ||' Right cnt:'||c.parm_instance_depy_rules.count);
     for cr in (select value(t0) parm_instance_depy_rule from table(parm_instance_depy_rules) t0
                  where not exists (
                      select null from table(parm_instance_depy_rules) t1, table(c.parm_instance_depy_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for parm_instance_depy_rules:'||cr.parm_instance_depy_rule.PREREQ_PARM_NM||'->'||cr.parm_instance_depy_rule.DEPENDENT_PARM_NM, 0);
     end loop;
     for cr in (select value(t0) parm_instance_depy_rule from table(c.parm_instance_depy_rules) t0
                  where not exists (
                      select null from table(c.parm_instance_depy_rules) t1, table(parm_instance_depy_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for parm_instance_depy_rules:'||cr.parm_instance_depy_rule.PREREQ_PARM_NM||'->'||cr.parm_instance_depy_rule.DEPENDENT_PARM_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(state_actions           ) t1, table(c.state_actions           ) t2 where value(t1) = value(t2); if n <> state_actions           .count or n <> c.state_actions           .count
   then
     debug_log(chr(10)||'Table :state_actions            Common cnt:'||n ||'<> Left cnt:'|| state_actions           .count ||' Right cnt:'||c.state_actions           .count);
     for cr in (select value(t0) state_action from table(state_actions) t0
                  where not exists (
                      select null from table(state_actions) t1, table(c.state_actions) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for state_actions:'||cr.state_action, 0);
     end loop;
     for cr in (select value(t0) state_action from table(c.state_actions) t0
                  where not exists (
                      select null from table(c.state_actions) t1, table(state_actions) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for state_actions:'||cr.state_action, 0);
     end loop;
   end if;
   select count(*) into n from table(svc_actions             ) t1, table(c.svc_actions             ) t2 where value(t1) = value(t2); if n <> svc_actions             .count or n <> c.svc_actions             .count
   then
     debug_log(chr(10)||'Table :svc_actions              Common cnt:'||n ||'<> Left cnt:'|| svc_actions             .count ||' Right cnt:'||c.svc_actions             .count);
     for cr in (select value(t0) svc_action from table(svc_actions) t0
                  where not exists (
                      select null from table(svc_actions) t1, table(c.svc_actions) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for state_actions:'||cr.svc_action.SVC_ACTION_NM, 0);
     end loop;
     for cr in (select value(t0) svc_action from table(c.svc_actions) t0
                  where not exists (
                      select null from table(c.svc_actions) t1, table(svc_actions) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for state_actions:'||cr.svc_action.SVC_ACTION_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(svc_avail_constrs       ) t1, table(c.svc_avail_constrs       ) t2 where value(t1) = value(t2); if n <> svc_avail_constrs       .count or n <> c.svc_avail_constrs       .count
   then
     debug_log(chr(10)||'Table :svc_avail_constrs        Common cnt:'||n ||'<> Left cnt:'|| svc_avail_constrs       .count ||' Right cnt:'||c.svc_avail_constrs       .count);
     for cr in (select value(t0) svc_avail_constr from table(svc_avail_constrs) t0
                  where not exists (
                      select null from table(svc_avail_constrs) t1, table(c.svc_avail_constrs) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_avail_constr:'||cr.svc_avail_constr.SVC_AVAIL_CONSTR_NM, 0);
     end loop;
     for cr in (select value(t0) svc_avail_constr from table(c.svc_avail_constrs) t0
                  where not exists (
                      select null from table(c.svc_avail_constrs) t1, table(svc_avail_constrs) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_avail_constr:'||cr.svc_avail_constr.SVC_AVAIL_CONSTR_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(svc_composition_rules   ) t1, table(c.svc_composition_rules   ) t2 where value(t1) = value(t2); if n <> svc_composition_rules   .count or n <> c.svc_composition_rules   .count
   then
     debug_log(chr(10)||'Table :svc_composition_rules    Common cnt:'||n ||'<> Left cnt:'|| svc_composition_rules   .count ||' Right cnt:'||c.svc_composition_rules   .count);
     for cr in (select value(t0) svc_composition_rule from table(svc_composition_rules) t0
                  where not exists (
                      select null from table(svc_composition_rules) t1, table(c.svc_composition_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_composition_rule:'||cr.svc_composition_rule.CAPTURE_SEQ_NUM||'.'||cr.svc_composition_rule.COMPONENT_SVC_NM, 0);
     end loop;
     for cr in (select value(t0) svc_composition_rule from table(c.svc_composition_rules) t0
                  where not exists (
                      select null from table(c.svc_composition_rules) t1, table(svc_composition_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_composition_rule:'||cr.svc_composition_rule.CAPTURE_SEQ_NUM||'.'||cr.svc_composition_rule.COMPONENT_SVC_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(svc_depy_rules          ) t1, table(c.svc_depy_rules          ) t2 where value(t1) = value(t2); if n <> svc_depy_rules          .count or n <> c.svc_depy_rules          .count
   then
     debug_log(chr(10)||'Table :svc_depy_rules           Common cnt:'||n ||'<> Left cnt:'|| svc_depy_rules          .count ||' Right cnt:'||c.svc_depy_rules          .count);
     for cr in (select value(t0) svc_depy_rule from table(svc_depy_rules) t0
                  where not exists (
                      select null from table(svc_depy_rules) t1, table(c.svc_depy_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_depy_rule:'||cr.svc_depy_rule.DEPENDENT_SVC_NM, 0);
     end loop;
     for cr in (select value(t0) svc_depy_rule from table(c.svc_depy_rules) t0
                  where not exists (
                      select null from table(c.svc_depy_rules) t1, table(svc_depy_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_depy_rule:'||cr.svc_depy_rule.DEPENDENT_SVC_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(svc_exclusion_grps      ) t1, table(c.svc_exclusion_grps      ) t2 where value(t1) = value(t2); if n <> svc_exclusion_grps      .count or n <> c.svc_exclusion_grps      .count
   then
     debug_log(chr(10)||'Table :svc_exclusion_grps       Common cnt:'||n ||'<> Left cnt:'|| svc_exclusion_grps      .count ||' Right cnt:'||c.svc_exclusion_grps      .count);
     for cr in (select value(t0) svc_exclusion_grp from table(svc_exclusion_grps) t0
                  where not exists (
                      select null from table(svc_exclusion_grps) t1, table(c.svc_exclusion_grps) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_exclusion_grp:'||cr.svc_exclusion_grp.SVC_EXCLUSION_GRP_NM, 0);
     end loop;
     for cr in (select value(t0) svc_exclusion_grp from table(c.svc_exclusion_grps) t0
                  where not exists (
                      select null from table(c.svc_exclusion_grps) t1, table(svc_exclusion_grps) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_exclusion_grp:'||cr.svc_exclusion_grp.SVC_EXCLUSION_GRP_NM, 0);
     end loop;

   end if;
   select count(*) into n from table(svc_migr_tos            ) t1, table(c.svc_migr_tos            ) t2 where value(t1) = value(t2); if n <> svc_migr_tos            .count or n <> c.svc_migr_tos            .count
   then
     debug_log(chr(10)||'Table :svc_migr_tos             Common cnt:'||n ||'<> Left cnt:'|| svc_migr_tos            .count ||' Right cnt:'||c.svc_migr_tos            .count);
     for cr in (select value(t0) svc_mig from table(svc_migr_tos) t0
                  where not exists (
                      select null from table(svc_migr_tos) t1, table(c.svc_migr_tos) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_migr_tos:'||cr.svc_mig.TO_SCOPE_SVC_NM, 0);
     end loop;
     for cr in (select value(t0) svc_mig from table(c.svc_migr_tos) t0
                  where not exists (
                      select value(t1) from table(c.svc_migr_tos) t1, table(svc_migr_tos) t2
                        where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_migr_tos:'||cr.svc_mig.TO_SCOPE_SVC_NM, 0);
     end loop;
   end if;

   select count(*) into n from table(svc_migr_froms          ) t1, table(c.svc_migr_froms          ) t2 where value(t1) = value(t2); if n <> svc_migr_froms          .count or n <> c.svc_migr_froms          .count
   then
     debug_log(chr(10)||'Table :svc_migr_froms           Common cnt:'||n ||'<> Left cnt:'|| svc_migr_froms          .count ||' Right cnt:'||c.svc_migr_froms          .count);
     for cr in (select value(t0) svc_migr_from from table(svc_migr_froms) t0
                  where not exists (
                      select null from table(svc_migr_froms) t1, table(c.svc_migr_froms) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_migr_from:'||cr.svc_migr_from.FROM_SCOPE_SVC_NM, 0);
     end loop;
     for cr in (select value(t0) svc_migr_from from table(c.svc_migr_froms) t0
                  where not exists (
                      select null from table(c.svc_migr_froms) t1, table(svc_migr_froms) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_migr_from:'||cr.svc_migr_from.FROM_SCOPE_SVC_NM, 0);
     end loop;
   end if;
   select count(*) into n from table(svc_supported_platforms ) t1, table(c.svc_supported_platforms ) t2 where value(t1) = value(t2); if n <> svc_supported_platforms .count or n <> c.svc_supported_platforms .count
   then
     debug_log(chr(10)||'Table :svc_supported_platforms  Common cnt:'||n ||'<> Left cnt:'|| svc_supported_platforms .count ||' Right cnt:'||c.svc_supported_platforms .count);
     for cr in (select value(t0) svc_supported_platform from table(svc_supported_platforms) t0
                  where not exists (
                      select null from table(svc_supported_platforms) t1, table(c.svc_supported_platforms) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_supported_platform:'||cr.svc_supported_platform.SVC_DELIVERY_PLAT_NM, 0);
     end loop;
     for cr in (select value(t0) svc_supported_platform from table(c.svc_supported_platforms) t0
                  where not exists (
                      select null from table(c.svc_supported_platforms) t1, table(svc_supported_platforms) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_supported_platform:'||cr.svc_supported_platform.SVC_DELIVERY_PLAT_NM, 0);
     end loop;
   end if;

   select count(*) into n from table(svc_vld_rules           ) t1, table(c.svc_vld_rules           ) t2 where value(t1) = value(t2); if n <> svc_vld_rules           .count or n <> c.svc_vld_rules           .count
   then
     debug_log(chr(10)||'Table :svc_vld_rules            Common cnt:'||n ||'<> Left cnt:'|| svc_vld_rules           .count ||' Right cnt:'||c.svc_vld_rules           .count);
     for cr in (select value(t0) svc_vld_rule from table(svc_vld_rules) t0
                  where not exists (
                      select null from table(svc_vld_rules) t1, table(c.svc_vld_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for svc_vld_rule:'||cr.svc_vld_rule.SEQ_NUM||cr.svc_vld_rule.JMF_COND, 0);
     end loop;
     for cr in (select value(t0) svc_vld_rule from table(c.svc_vld_rules) t0
                  where not exists (
                      select null from table(c.svc_vld_rules) t1, table(svc_vld_rules) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for svc_vld_rule:'||cr.svc_vld_rule.SEQ_NUM||cr.svc_vld_rule.JMF_COND, 0);
     end loop;
   end if;

   select count(*) into n from table(parms           ) t1, table(c.parms           ) t2 where value(t1) = value(t2); if n <> parms           .count or n <> c.parms           .count
   then
     debug_log(chr(10)||'Table :parms            Common cnt:'||n ||'<> Left cnt:'|| parms           .count ||' Right cnt:'||c.parms           .count);
     for cr in (select value(t0) parm from table(parms) t0
                  where not exists (
                      select null from table(parms) t1, table(c.parms) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No right match for parm:'||cr.parm.parm_nm, 0);
     end loop;
     for cr in (select value(t0) parm from table(c.parms) t0
                  where not exists (
                      select null from table(c.parms) t1, table(parms) t2
                      where value(t1) = value(t2) and value(t1) = value(t0) )) loop
          debug_log('\_ No left match for parm:'||cr.parm.parm_nm, 0);
     end loop;
   end if;

 end;

 ORDER MEMBER FUNCTION match (c o_svc) RETURN INTEGER is
   n number;
 begin
    if  SVC_NM                         = c.SVC_NM                AND
        IS_ENTRY_POINT                 = c.IS_ENTRY_POINT        AND
        nvl(ACCESS_SCOPE, 'null')      = nvl(c.ACCESS_SCOPE, 'null') AND
        IS_ORDERABLE                   = c.IS_ORDERABLE          AND
        NEEDS_SVC_ADDR                 = c.NEEDS_SVC_ADDR        AND
        nvl(REQUIRED_SVC_ADDR, -1)     = nvl(c.REQUIRED_SVC_ADDR, -1) AND
        EFF_DTM                        = c.EFF_DTM               AND
        nvl(EXP_DTM, sysdate - 10000)  = nvl(c.EXP_DTM, sysdate -  10000) AND
        nvl(MIN_PROF_INSTANCE_NUM, -1) = nvl(c.MIN_PROF_INSTANCE_NUM, -1) AND
        nvl(MAX_PROF_INSTANCE_NUM, -1) = nvl(c.MAX_PROF_INSTANCE_NUM, -1) AND
        HAS_NTWK_IMPACT                = c.HAS_NTWK_IMPACT       AND
        CHECK_SERVICEABILITY           = c.CHECK_SERVICEABILITY  AND
        SVC_STATE                      = c.SVC_STATE             AND
        nvl(base_svc_nm, 'null')       = nvl(c.base_svc_nm, 'null') AND
        nvl(DATA_SRC_NM, 'null')       = nvl(c.DATA_SRC_NM, 'null')  AND
        ALIAS_RULES.count              = c.ALIAS_RULES.count     AND
        FOLDERS.count                  = c.FOLDERS.count         AND
        parm_instance_depy_rules.count = c.parm_instance_depy_rules.count AND
        state_actions.count            = c.state_actions.count            AND
        svc_actions.count              = c.svc_actions.count              AND
        svc_avail_constrs.count        = c.svc_avail_constrs.count        AND
        svc_composition_rules.count    = c.svc_composition_rules.count    AND
        svc_depy_rules.count           = c.svc_depy_rules.count           AND
        svc_exclusion_grps.count       = c.svc_exclusion_grps.count       AND
        svc_migr_tos.count             = c.svc_migr_tos.count             AND
        svc_migr_froms.count           = c.svc_migr_froms.count           AND
        svc_supported_platforms.count  = c.svc_supported_platforms.count  AND
        svc_vld_rules.count            = c.svc_vld_rules.count            AND
        parms.count                    = c.parms.count
    then
       select count(*) into n from table(ALIAS_RULES             ) t1, table(c.ALIAS_RULES             ) t2 where value(t1) = value(t2); if n <> ALIAS_RULES             .count then return -1; end if;
       select count(*) into n from table(FOLDERS                 ) t1, table(c.FOLDERS                 ) t2 where value(t1) = value(t2); if n <> FOLDERS                 .count then return -1; end if;
       select count(*) into n from table(parm_instance_depy_rules) t1, table(c.parm_instance_depy_rules) t2 where value(t1) = value(t2); if n <> parm_instance_depy_rules.count then return -1; end if;
       select count(*) into n from table(state_actions           ) t1, table(c.state_actions           ) t2 where value(t1) = value(t2); if n <> state_actions           .count then return -1; end if;
       select count(*) into n from table(svc_actions             ) t1, table(c.svc_actions             ) t2 where value(t1) = value(t2); if n <> svc_actions             .count then return -1; end if;
       select count(*) into n from table(svc_avail_constrs       ) t1, table(c.svc_avail_constrs       ) t2 where value(t1) = value(t2); if n <> svc_avail_constrs       .count then return -1; end if;
       select count(*) into n from table(svc_composition_rules   ) t1, table(c.svc_composition_rules   ) t2 where value(t1) = value(t2); if n <> svc_composition_rules   .count then return -1; end if;
       select count(*) into n from table(svc_depy_rules          ) t1, table(c.svc_depy_rules          ) t2 where value(t1) = value(t2); if n <> svc_depy_rules          .count then return -1; end if;
       select count(*) into n from table(svc_exclusion_grps      ) t1, table(c.svc_exclusion_grps      ) t2 where value(t1) = value(t2); if n <> svc_exclusion_grps      .count then return -1; end if;
       select count(*) into n from table(svc_migr_tos            ) t1, table(c.svc_migr_tos            ) t2 where value(t1) = value(t2); if n <> svc_migr_tos            .count then return -1; end if;
       select count(*) into n from table(svc_migr_froms          ) t1, table(c.svc_migr_froms          ) t2 where value(t1) = value(t2); if n <> svc_migr_froms          .count then return -1; end if;
       select count(*) into n from table(svc_supported_platforms ) t1, table(c.svc_supported_platforms ) t2 where value(t1) = value(t2); if n <> svc_supported_platforms .count then return -1; end if;
       select count(*) into n from table(svc_vld_rules           ) t1, table(c.svc_vld_rules           ) t2 where value(t1) = value(t2); if n <> svc_vld_rules           .count then return -1; end if;
       select count(*) into n from table(parms                   ) t1, table(c.parms                   ) t2 where value(t1) = value(t2); if n <> parms                   .count then return -1; end if;
       return 0;
    else
       return 1;
    end if;
 end;
end;
/

ALTER TYPE o_svc  COMPILE DEBUG ;
ALTER TYPE o_svc  COMPILE DEBUG BODY;


create or replace procedure show_diff(p_src_nm varchar2, p_tgt_nm varchar2) as
  TYPE SvcCurTyp IS REF CURSOR;
  SvcCur SvcCurTyp ;
  p_svc_nm    varchar2(255);
  b_diff_found boolean := false;

begin
    open SvcCur for 'select v1.svc_nm from '||p_src_nm||' v1, '|| p_tgt_nm||' v2 where v1.svc_nm  = v2.svc_nm and v1.svc <> v2.svc' ;
    loop
        fetch SvcCur into p_svc_nm;
        exit when SvcCur%notfound;
        dbms_output.put_line('Service "'||p_svc_nm||'" is different');
        b_diff_found := true;
    end loop;
    close SvcCur;

    open SvcCur for 'select svc_nm from '||p_src_nm||' minus select svc_nm from '|| p_tgt_nm ;
    loop
        fetch SvcCur into p_svc_nm;
        exit when SvcCur%notfound;
        dbms_output.put_line('Service "'||p_svc_nm||'" not found at '|| p_tgt_nm );
        b_diff_found := true;
    end loop;
    close SvcCur;
    open SvcCur for 'select svc_nm from '||p_tgt_nm||' minus select svc_nm from '|| p_src_nm ;
    loop
        fetch SvcCur into p_svc_nm;
        exit when SvcCur%notfound;
        dbms_output.put_line('Service "'||p_svc_nm||'" not found at '|| p_src_nm );
        b_diff_found := true;
    end loop;
    close SvcCur;

    if not b_diff_found  then
       dbms_output.put_line('No difference found');
    end if;

end;
/

create or replace procedure show_diff_detail(p_src_nm varchar2, p_tgt_nm varchar2, p_svc_nm varchar2) as
 s1 o_svc;
 s2 o_svc;
begin
    execute immediate 'select svc from '||p_src_nm ||' where svc_nm = '''||p_svc_nm||'''' into s1;
    execute immediate 'select svc from '||p_tgt_nm ||' where svc_nm = '''||p_svc_nm||'''' into s2;
    s1.diff(s2);
end;
/



