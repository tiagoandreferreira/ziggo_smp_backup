---------------------------------------------------------------------------
-- FILE INFO
--    $Id: svc_cmp_view.sql,v 1.2 2003/11/13 20:05:23 vadimg Exp $
--
-- DESCRIPTION
--  Object views definitions for SVC configuration capture and compare
--  Build the views pulling the data from SMP schemas you wish to capture/compare
--  One comparison schema may contain many views pointing to different data sources
--  Every such collection is identified by SUFFIX variable set below
--
--  Setup instruction
--  1. Set unique SUFFIX to identify a set of views associated with selected data source
--  2. Set SRC  - source schema name where source SMP DB is deployed. NB: "." (dot) at the end if important if SRC is not empty!
--  3. Set LINK to a database link name if the source SMP schema is in a remote DB accessible over db_link
--  Note: The above settings must produce valid Oracle object names referencing real database objects
--        Empty name is valid, it must be "" then.
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------

DEFINE SUFFIX = "08"
DEFINE SRC    = SMPDEV08CM.
DEFINE LINK   = ""

set verify off

create or replace view v_parm&SUFFIX as select parm_nm||'/'||class_id||'/'||object_id parm_nm, parm_id
 from &SRC.parm&LINK;



create or replace view vo_parm_instance_depy_rule&SUFFIX as
select PARM_INSTANCE_DEPY_RULE_ID, r.scope_svc_id svc_id,
 o_parm_instance_depy_rule(p.parm_nm, d.parm_nm, s.svc_nm) PARM_INSTANCE_DEPY_RULE
from &SRC.parm_instance_depy_rule&LINK r, v_parm&SUFFIX p, v_parm&SUFFIX d, &SRC.svc&LINK s
 where r.PREREQ_PARM_ID = p.parm_id and r.DEPENDENT_PARM_ID = d.parm_id
   and r.SCOPE_SVC_ID = s.svc_id(+);

create or replace view vo_folder_svc&SUFFIX as select svc_id,
 o_folder_svc (f.folder_id, cast(multiset(
  select svc_nm from &SRC.folder_svc&LINK c, &SRC.svc&LINK s where s.svc_id = c.svc_id and c.folder_id = f.folder_id
  ) as t_VARCHAR255)) folder_svc
 from &SRC.folder_svc&LINK f;


create or replace view vo_alias_rule&SUFFIX as select a.alias_rule_id, a.scope_svc_id svc_id,
 o_alias_rule(a.ALIAS_RULE_NM, s.svc_nm,
   cast(multiset(select p.parm_nm from v_parm&SUFFIX p, &SRC.alias_rule_parm&LINK r
     where p.parm_id = r.parm_id and r.alias_rule_id = a.alias_rule_id) as t_varchar255 )) alias_rule
 from &SRC.alias_rule&LINK a, &SRC.svc&LINK s
   where a.scope_svc_id = s.svc_id(+)
;

create or replace view vo_svc_exclusion_grp&SUFFIX as select m.svc_id,
 o_svc_exclusion_grp(
   m.SVC_EXCLUSION_GRP_NM,
   s.svc_nm
 ) svc_exclusion_grp
 from &SRC.svc_excl_grp_member&LINK m, &SRC.svc_exclusion_grp&LINK g, &SRC.svc&LINK s
  where m.SVC_EXCLUSION_GRP_NM = g.SVC_EXCLUSION_GRP_NM and g.scope_svc_id = s.svc_id(+);

create or replace view vo_state_transition&SUFFIX as select SCOPE_SVC_ID, STATE_TRANSITION_ID,
o_state_transition(f.status, t.status, cond, a.action) o_state_transition
 from &SRC.state_transition&LINK s, &SRC.ref_status&LINK f, &SRC.ref_status&LINK t, &SRC.state_action&LINK a
 where FROM_STATUS_ID = f.status_id and TO_STATUS_ID = t.status_id and s.STATE_ACTION_ID = a.STATE_ACTION_ID(+)
 ;

create or replace view  vo_composite_action_rule&SUFFIX as
 select  PARENT_SVC_ACTION_ID SVC_ACTION_ID, o_composite_action_rule (SVC_ACTION_NM, SVC_ACTION_SEQ, TMPLT_EXEC_PRIORITY) composite_action_rule
  from &SRC.composite_action_rule&LINK r, &SRC.svc_action&LINK c
    where r.CHILD_SVC_ACTION_ID = c.SVC_ACTION_ID;

create or replace view vo_parm_chg_ntwk_impact&SUFFIX as select svc_action_id,
o_parm_chg_ntwk_impact(
o_ntwk_action(
a.NTWK_ACTION_NM      ,
n.NTWK_ACTION_TYP_NM  ,
t.SUBNTWK_TYP_NM      ,
a.IS_SSN_CALC_REQUIRED,
a.EXECUTION_SEQ
),
p.parm_nm,
i.SVC_PARM_CHG_TYP,
i.EXECUTION_SEQ,
i.JMF_COND
) parm_chg_ntwk_impact
from &SRC.parm_chg_ntwk_impact&LINK i, &SRC.NTWK_action&LINK a, v_parm&SUFFIX p, &SRC.ref_subntwk_typ&LINK t,
  &SRC.ref_NTWK_ACTION_TYP&LINK n
  where a.ntwk_action_id = i.ntwk_action_id and p.parm_id(+) = i.parm_id and a.subntwk_typ_id = t.subntwk_typ_id(+)
   and a.NTWK_ACTION_TYP_ID=n.NTWK_ACTION_TYP_ID;


create or replace view vo_svc_action_exclusion&SUFFIX as select x.svc_action_id ,
 o_svc_action_exclusion(a.svc_action_nm, JMF_COND, is_distinct) svc_action_exclusion
from &SRC.svc_action_exclusion&LINK x, &SRC.svc_action&LINK a
 where x.excluded_svc_action_id = a.svc_action_id;

create or replace view vo_svc_impact&SUFFIX as select i.SVC_ACTION_ID,
 o_svc_impact(seq_num, tmplt_nm, priority, impact_typ, cond) svc_impact
from &SRC.svc_impact&LINK i, &SRC.tmplt&LINK t
where i.tmplt_id = t.tmplt_id;

create or replace view vo_SVC_PARM_CHG_GRP&SUFFIX as select SVC_PARM_CHG_GRP_ID,
  o_SVC_PARM_CHG_GRP (
   cast(multiset(
    select svc_action_nm
       from &SRC.svc_parm_chg_grp_impact&LINK i, &SRC.svc_action&LINK a
         where i.svc_action_id = a.svc_action_id and i.SVC_PARM_CHG_GRP_ID =  g.SVC_PARM_CHG_GRP_ID
   ) as t_varchar255),
   IS_OVERRIDE,
   is_and_grp,
   cast(multiset(SELECT O_SVC_PARM_CHG_GRP_DTL(parm_nm, svc_parm_chg_typ)
    from &SRC.SVC_PARM_CHG_GRP_DTL&LINK D, v_parm&SUFFIX p
      where d.SVC_PARM_CHG_GRP_ID = g.SVC_PARM_CHG_GRP_ID
            AND p.parm_id = d.parm_id) as T_SVC_PARM_CHG_GRP_DTL))  SVC_PARM_CHG_GRP
  from &SRC.SVC_PARM_CHG_GRP&LINK g;

create or replace view vo_svc_parm_chg_impact&SUFFIX as select svc_action_id,
 o_svc_parm_chg_impact(p.PARM_NM, i.SVC_PARM_CHG_TYP, i.SEQ_NUM, i.PRIORITY, i.JMF_COND) svc_parm_chg_impact
from &SRC.svc_parm_chg_impact&LINK i, v_parm&SUFFIX p
 where i.parm_id = p.parm_id;

create or replace view vo_svc_parm_chg_grp_impact&SUFFIX as select i.svc_action_id,
 o_svc_parm_chg_grp_impact(v.SVC_PARM_CHG_GRP, i.seq_num, i.jmf_cond, i.priority)  svc_parm_chg_grp_impact
from &SRC.svc_parm_chg_grp_impact&LINK i, vo_SVC_PARM_CHG_GRP&SUFFIX v
 where i.SVC_PARM_CHG_GRP_ID = v.SVC_PARM_CHG_GRP_ID;

create or replace view  vo_svc_action&SUFFIX as select a.svc_id,
o_svc_action(a.SVC_ACTION_NM, g.SVC_ACTION_GRP_NM, e.EXEC_COST_NM,
  a.IS_REQUIRED, a.SVC_ACTION_TYP, b.SVC_ACTION_NM,
  cast(multiset( select composite_action_rule   from vo_composite_action_rule&SUFFIX   t where t.svc_action_id = a.svc_action_id) as t_composite_action_rule),
  cast(multiset( select parm_chg_ntwk_impact    from vo_parm_chg_ntwk_impact&SUFFIX    t where t.svc_action_id = a.svc_action_id) as t_parm_chg_ntwk_impact   ),
  cast(multiset( select data_SRC_nm             from &SRC.svc_action_data_SRC&LINK        t where t.svc_action_id = a.svc_action_id) as t_varchar255             ),
  cast(multiset( select svc_action_exclusion    from vo_svc_action_exclusion&SUFFIX    t where t.svc_action_id = a.svc_action_id) as t_svc_action_exclusion   ),
  cast(multiset( select svc_impact              from vo_svc_impact&SUFFIX              t where t.svc_action_id = a.svc_action_id) as t_svc_impact             ),
  cast(multiset( select svc_parm_chg_grp_impact from vo_svc_parm_chg_grp_impact&SUFFIX t where t.svc_action_id = a.svc_action_id) as t_svc_parm_chg_grp_impact),
  cast(multiset( select svc_parm_chg_impact     from vo_svc_parm_chg_impact&SUFFIX     t where t.svc_action_id = a.svc_action_id) as t_svc_parm_chg_impact    )
  ) svc_action
from &SRC.svc_action&LINK a, &SRC.svc_action_grp&LINK g, &SRC.ref_exec_cost&LINK e, &SRC.svc_action&LINK b
 where a.SVC_ACTION_GRP_ID = g.SVC_ACTION_GRP_ID and a.exec_cost_id = e.exec_cost_id(+)
   and a.base_svc_action_id = b.svc_action_id(+);

create or replace view vo_svc_avail_constr&SUFFIX as select a.svc_id,
 o_svc_avail_constr(a.SVC_AVAIL_CONSTR_NM,s.SUB_TYP_NM,m.MANUAL_CONSTRAINT_NM,
    a.CHECK_NTWK_AVAIL,a.JMF_COND) svc_avail_constr
 from &SRC.svc_avail_constr&LINK a, &SRC.ref_sub_typ&LINK s, &SRC.MANUAL_CONSTRAINT&LINK m
   where a.sub_typ_id = s.sub_typ_id(+) and a.MANUAL_CONSTRAINT_ID = m.MANUAL_CONSTRAINT_ID(+);

create or replace view vo_svc_composition_rule&SUFFIX as select COMPOSED_SVC_ID svc_id,
o_svc_composition_rule ( s.svc_nm, CAPTURE_SEQ_NUM, MIN_INSTANCE_NUM,MAX_INSTANCE_NUM) svc_composition_rule
from &SRC.svc_composition_rule&LINK r, &SRC.svc&LINK s
 where COMPONENT_SVC_ID = s.svc_id;

create or replace view vo_svc_depy_rule&SUFFIX as select  PREREQ_SVC_ID SVC_ID,
/* o_svc_depy_rule(d.svc_nm, s.svc_nm, r.IS_INSTANCE, r.OR_GRP_ID, r.FORCE_CASCADE_DELETE, r.is_auto, r.JMF_COND) svc_depy_rule */
o_svc_depy_rule(d.svc_nm, s.svc_nm, r.IS_INSTANCE, r.OR_GRP_ID, r.FORCE_CASCADE_DELETE, 'n', '') svc_depy_rule
from &SRC.svc_depy_rule&LINK r, &SRC.svc&LINK d, &SRC.svc&LINK s
 where r.DEPENDENT_SVC_ID = d.svc_id and r.SCOPE_SVC_ID = s.svc_id(+);

create or replace view vo_svc_migr_from&SUFFIX as select FROM_SVC_ID svc_id ,
o_svc_migr (
MIGR_ACTION_NM   ,
SVC_MIGR_NM      ,
FS.SVC_NM,
F.SVC_NM      ,
TS.SVC_NM  ,
T.SVC_NM        ,
IS_EXECUTABLE,
cast(multiset(
  select o_svc_migr_comp_rule(r.MIGR_SEQ, s.SVC_MIGR_NM)
  from &SRC.svc_migr_comp_rule&LINK r, &SRC.SVC_MIGR&LINK s
    where r.CMPNT_SVC_MIGR_ID = s.SVC_MIGR_ID and r.SVC_MIGR_ID = m.SVC_MIGR_ID
  ) as t_svc_migr_comp_rule),
cast(multiset( select o_svc_migr_parm_src(CAPTURE_SEQ,
   c.SVC_NM, p1.PARM_NM, v.SVC_NM, SRC_CAPTURE_SEQ, p2.PARM_NM, SRC_JMF_EXPR)
   from &SRC.svc_migr_parm_src&LINK p, &SRC.svc&LINK c, v_parm&SUFFIX p1, &SRC.svc&LINK v, v_parm&SUFFIX p2
     where p.scope_svc_id = c.svc_id (+)  and p.MIGR_PARM_ID = p1.parm_id(+) and
         p.SRC_SCOPE_SVC_ID = v.svc_id (+)  and p.SRC_PARM_ID = p2.parm_id(+) and p.svc_migr_id = m.svc_migr_id
  ) as t_svc_migr_parm_src)
) svc_migr
from &SRC.svc_migr&LINK m, &SRC.svc&LINK fs, &SRC.svc&LINK f, &SRC.svc&LINK ts, &SRC.svc&LINK t
where m.from_svc_id = f.svc_id and m.from_scope_svc_id = fs.svc_id(+) and
      m.to_svc_id = t.svc_id(+) and m.to_scope_svc_id = ts.svc_id(+) ;


create or replace view vo_svc_migr_to&SUFFIX as select TO_SVC_ID svc_id ,
o_svc_migr (
MIGR_ACTION_NM   ,
SVC_MIGR_NM      ,
FS.SVC_NM,
F.SVC_NM      ,
TS.SVC_NM  ,
T.SVC_NM        ,
IS_EXECUTABLE,
cast(multiset(
  select o_svc_migr_comp_rule(r.MIGR_SEQ, s.SVC_MIGR_NM)
  from &SRC.svc_migr_comp_rule&LINK r, &SRC.SVC_MIGR&LINK s
    where r.CMPNT_SVC_MIGR_ID = s.SVC_MIGR_ID and r.SVC_MIGR_ID = m.SVC_MIGR_ID
  ) as t_svc_migr_comp_rule),
cast(multiset( select o_svc_migr_parm_src(CAPTURE_SEQ,
   c.SVC_NM, p1.PARM_NM, v.SVC_NM, SRC_CAPTURE_SEQ, p2.PARM_NM, SRC_JMF_EXPR)
   from &SRC.svc_migr_parm_src&LINK p, &SRC.svc&LINK c, v_parm&SUFFIX p1, &SRC.svc&LINK v, v_parm&SUFFIX p2
     where p.scope_svc_id = c.svc_id (+)  and p.MIGR_PARM_ID = p1.parm_id(+) and
         p.SRC_SCOPE_SVC_ID = v.svc_id (+)  and p.SRC_PARM_ID = p2.parm_id(+) and p.svc_migr_id = m.svc_migr_id
  ) as t_svc_migr_parm_src)
) svc_migr
from &SRC.svc_migr&LINK m, &SRC.svc&LINK fs, &SRC.svc&LINK f, &SRC.svc&LINK ts, &SRC.svc&LINK t
where m.from_svc_id = f.svc_id(+) and m.from_scope_svc_id = fs.svc_id(+) and
      m.to_svc_id = t.svc_id and m.to_scope_svc_id = ts.svc_id(+) ;

create or replace view vo_svc_platform_subntwk&SUFFIX as select SVC_SUPPORTED_PLATFORM_ID,
o_svc_platform_subntwk(SUBNTWK_TYP_NM, NTWK_ROLE_NM, MGMT_MODE_NM,
 cast(multiset(select o_nep_info( DATA_SRC_NM||'/'||NEP_SRC_CATEGORY||'/'||NEP_PARM_NM)
  from &SRC.nep_info&LINK n where
    n.SUBNTWK_TYP_ID            = p.SUBNTWK_TYP_ID                AND
    n.SVC_SUPPORTED_PLATFORM_ID = p.SVC_SUPPORTED_PLATFORM_ID     AND
    n.NTWK_ROLE_ID              = p.NTWK_ROLE_ID                  AND
    n.MGMT_MODE_ID              = p.MGMT_MODE_ID
  ) as t_nep_info)
) svc_platform_subntwk
  from &SRC.svc_platform_subntwk&LINK p, &SRC.ref_SUBNTWK_TYP&LINK t, &SRC.ref_NTWK_ROLE&LINK r, &SRC.ref_MGMT_MODE&LINK m
    where p.SUBNTWK_TYP_ID = t.SUBNTWK_TYP_ID and p.NTWK_ROLE_ID = r.NTWK_ROLE_ID
       and p.MGMT_MODE_ID = m.MGMT_MODE_ID;

create or replace view vo_svc_supported_platform&SUFFIX as select svc_id,
 o_svc_supported_platform( d.SVC_DELIVERY_PLAT_NM ,
   s.IP_MGMT_MODE_NM, s.PRIORITY, JMF_EXPR,
  cast(multiset(select SVC_PLATFORM_SUBNTWK from vo_svc_platform_subntwk&SUFFIX v
    where v.SVC_SUPPORTED_PLATFORM_ID = s.SVC_SUPPORTED_PLATFORM_ID) as t_svc_platform_subntwk)
 ) svc_supported_platform
 from &SRC.svc_supported_platform&LINK s, &SRC.REF_SVC_DELIVERY_PLAT&LINK D
   where s.SVC_DELIVERY_PLAT_ID = d.SVC_DELIVERY_PLAT_ID;


create or replace view vo_svc_vld_rule&SUFFIX as select svc_id,
o_svc_vld_rule(SEQ_NUM, JMF_COND, ERR_CD) svc_vld_rule
  from &SRC.svc_vld_rule&LINK;


create or replace view vo_svc_parm&SUFFIX as select p.object_id svc_id ,
o_parm (
p.CONVERSION_TYP     ,
p.PARM_NM            ,
s.SVC_NM             ,
p.DATA_TYP_NM        ,
p.CAPTURE_SEQ_NUM    ,
b.PARM_nm            ,
p.IS_READ_ONLY       ,
p.IS_REQUIRED        ,
p.IS_PRIMARY_KEY     ,
p.IS_RESERVED        ,
p.IS_UNIQUE          ,
p.DFLT_VAL           ,
p.JMF_SRC            ,
p.JMF_DFLT_VAL       ,
p.JMF_DISPLAY_FORMAT ,
p.PARM_SRC_NM        ,
p.DB_COL_NM          ,
cast(multiset(select o_parm_permitted_val(JMF_PERMITTED_VAL_EXPR, VAL)
  from &SRC.parm_permitted_val&LINK v where v.parm_id = p.parm_id) as t_parm_permitted_val),
cast(multiset(select o_vld_rule(
  r.VLD_RULE_NM,r.TEST_RESULT_INVERSION_IND,r.MIN_INCLUSIVE
 ,r.MIN_EXCLUSIVE,r.MAX_INCLUSIVE,r.MAX_EXCLUSIVE
 ,r.MIN_CHAR_NUM,r.MAX_CHAR_NUM,r.ERR_CODE,r.ERR_DFLT_MSG
 ,r.JMF_EXPR,r.FORMAT
                                )
 from &SRC.parm_vld_rule&LINK v, &SRC.vld_rule&LINK r
 where v.parm_id = p.parm_id and v.vld_rule_id = r.vld_rule_id) as t_vld_rule)
        ) parm
        from &SRC.parm&LINK p, &SRC.svc&LINK s, &SRC.parm&LINK b
        where p.class_id = 100 and s.svc_id(+) = p.object_id and p.base_parm_id = b.parm_id(+);

create or replace view vo_svc&SUFFIX as select s.svc_id, s.svc_nm,
o_svc(
s.SVC_NM                ,
s.IS_ENTRY_POINT        ,
s.ACCESS_SCOPE          ,
s.IS_ORDERABLE          ,
s.NEEDS_SVC_ADDR        ,
s.REQUIRED_SVC_ADDR     ,
s.EFF_DTM               ,
s.EXP_DTM               ,
s.MIN_PROF_INSTANCE_NUM ,
s.MAX_PROF_INSTANCE_NUM ,
s.HAS_NTWK_IMPACT       ,
s.CHECK_SERVICEABILITY  ,
s.SVC_STATE             ,
b.svc_nm                ,
s.DATA_SRC_NM           ,
cast(multiset(select ALIAS_RULE from vo_ALIAS_RULE&SUFFIX v where v.svc_id = s.svc_id) as t_alias_rule),
cast(multiset(select folder_svc from vo_folder_svc&SUFFIX v where v.svc_id = s.svc_id) as t_folder_svc),
cast(multiset(select parm_instance_depy_rule from vo_parm_instance_depy_rule&SUFFIX v where v.svc_id = s.svc_id) as t_parm_instance_depy_rule),
cast(multiset(select action from &SRC.state_action&LINK v where v.svc_id = s.svc_id) as t_varchar255),
cast(multiset(select svc_action from vo_svc_action&SUFFIX v where v.svc_id = s.svc_id) as t_svc_action),
cast(multiset(select svc_avail_constr from vo_svc_avail_constr&SUFFIX v where v.svc_id = s.svc_id) as t_svc_avail_constr),
cast(multiset(select svc_composition_rule from vo_svc_composition_rule&SUFFIX v where v.svc_id = s.svc_id) as t_svc_composition_rule ),
cast(multiset(select svc_depy_rule from vo_svc_depy_rule&SUFFIX v where v.svc_id = s.svc_id) as t_svc_depy_rule),
cast(multiset(select svc_exclusion_grp from vo_svc_exclusion_grp&SUFFIX v where v.svc_id = s.svc_id) as t_svc_exclusion_grp),
cast(multiset(select svc_migr from vo_svc_migr_to&SUFFIX  v where v.svc_id = s.svc_id) as t_svc_migr),
cast(multiset(select svc_migr from vo_svc_migr_from&SUFFIX v where v.svc_id = s.svc_id) as t_svc_migr),
cast(multiset(select svc_supported_platform from vo_svc_supported_platform&SUFFIX v where v.svc_id = s.svc_id) as t_svc_supported_platform),
cast(multiset(select svc_vld_rule from vo_svc_vld_rule&SUFFIX v where v.svc_id = s.svc_id) as t_svc_vld_rule),
cast(multiset(select parm from vo_svc_parm&SUFFIX v where v.svc_id = s.svc_id) as t_parm)
) svc
from &SRC.svc&LINK s, &SRC.svc&LINK b where b.svc_id(+) = s.base_svc_id;


