---------------------------------------------------------------------------
-- FILE INFO
--    $Id: svc_snapshot.sql,v 1.1 2003/11/13 16:20:29 vadimg Exp $
--
-- DESCRIPTION
--  An example of creating SVC definition snapshot.
--  Customize actual svc$snap structure to fit you needs, add tags, timestamps, etc.
--
-- Revision History
-- * Based on CVS log
---------------------------------------------------------------------------

create table svc$snap
nested table SVC.ALIAS_RULES              store as ALIAS_RULES$snap
 (nested table ALIAS_RULE_PARM store as ALIAS_RULE_PARM$snap )
nested table SVC.FOLDERS                  store as FOLDERS$snap
 (nested table SVC_NMS store as SVC_NMS$snap)
nested table SVC.parm_instance_depy_rules store as parm_instance_depy_rules$snap
nested table SVC.state_actions            store as state_actions$snap
nested table SVC.svc_actions              store as svc_actions$snap (
  nested table COMPOSITE_ACTION_RULES    store as COMPOSITE_ACTION_RULES$snap
  nested table PARM_CHG_NTWK_IMPACTS     store as PARM_CHG_NTWK_IMPACTS$snap
  nested table SVC_ACTION_DATA_SRCS      store as SVC_ACTION_DATA_SRCS$snap
  nested table SVC_ACTION_EXCLUSIONS     store as SVC_ACTION_EXCLUSIONS$snap
  nested table SVC_IMPACTS               store as SVC_IMPACTS$snap
  nested table SVC_PARM_CHG_GRP_IMPACTS  store as SVC_PARM_CHG_GRP_IMPACTS$snap
   (nested table SVC_PARM_CHG_GRP_ID.SVC_ACTION_GRP  store as  SVC_ACTION_GRP$snap
    nested table SVC_PARM_CHG_GRP_ID.SVC_PARM_CHG_GRP_DTLS  store as  SVC_PARM_CHG_GRP_DTLS$snap)
  nested table SVC_PARM_CHG_IMPACTS      store as SVC_PARM_CHG_IMPACTS$snap
)
nested table SVC.svc_avail_constrs        store as svc_avail_constrs$snap
nested table SVC.svc_composition_rules    store as svc_composition_rules$snap
nested table SVC.svc_depy_rules           store as svc_depy_rules$snap
nested table SVC.svc_exclusion_grps       store as svc_exclusion_grps$snap
nested table SVC.svc_migr_tos             store as svc_migr_tos$snap (
  nested table SVC_MIGR_COMP_RULES         store as SVC_MIGR_COMP_RULES_t$snap
  nested table SVC_MIGR_PARM_SRCS          store as SVC_MIGR_PARM_SRCS_t$snap)
nested table SVC.svc_migr_froms           store as svc_migr_froms$snap (
  nested table SVC_MIGR_COMP_RULES         store as SVC_MIGR_COMP_RULES_f$snap
  nested table SVC_MIGR_PARM_SRCS          store as SVC_MIGR_PARM_SRCS_f$snap)
nested table SVC.svc_supported_platforms  store as svc_supported_platforms$snap (
 nested table SVC_PLATFORM_SUBNTWKS        store as SVC_PLATFORM_SUBNTWKS$snap (
  nested table NEP_INFOS                    store as NEP_INFOS$snap))
nested table SVC.svc_vld_rules            store as svc_vld_rules$snap
nested table SVC.parms                    store as parms$snap (
 nested table PARM_PERMITTED_VALS          store as PARM_PERMITTED_VALS$SNAP
 nested table VLD_RULES                    store as VLD_RULES$SNAP)
as select * from vo_svc;

