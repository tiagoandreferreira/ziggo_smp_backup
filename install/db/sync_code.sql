--==========================================================================
-- FILE INFO
--    $Id: sync_code.sql,v 1.7 2003/01/31 19:15:04 parmis Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * Parmi Sahota
--==========================================================================

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_parentsubntwknm
  ( p_subntwk_id  varchar2)
  RETURN  varchar2 IS
  cursor c_parent_subntwk is
    select subntwk_nm
    from   subntwk s
    where  s.subntwk_id = (select parent_subntwk_id from subntwk where subntwk_id = to_number(p_subntwk_id));
   r_parent_subntwk c_parent_subntwk%rowtype;
BEGIN
    open c_parent_subntwk;
    fetch c_parent_subntwk into r_parent_subntwk;
    RETURN r_parent_subntwk.subntwk_nm;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding parent subntwk_nm for subnetwork'||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_get_sysdate
  RETURN  varchar IS
  l_date varchar2(30);
BEGIN
    select to_char(sysdate,'YYYYMMDDHH24MISS') into l_date from dual;
    RETURN l_date ;
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_sbnt_msk
  ( p_sbnt_id  varchar2)
  RETURN  varchar2 IS
  cursor c_sbnt_msk is
    select ip_sbnt_msk
    from   ip_sbnt i
    where  i.ip_sbnt_id = to_number(p_sbnt_id);
   r_sbnt_msk c_sbnt_msk%rowtype;
BEGIN
    open c_sbnt_msk;
    fetch c_sbnt_msk into r_sbnt_msk;
    RETURN r_sbnt_msk.ip_sbnt_msk;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding subnet mask for subnet '||p_sbnt_id);
END fn_st_sync_get_sbnt_msk;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_freq_extkey
  ( p_freq_id  varchar2)
  RETURN  varchar2 IS
  cursor c_freq_extkey is
    select freq_hz
    from   freq
    where  freq_id = to_number(p_freq_id);
   r_freq_extkey c_freq_extkey%rowtype;
BEGIN
    open c_freq_extkey;
    fetch c_freq_extkey into r_freq_extkey;
    close c_freq_extkey;
    RETURN r_freq_extkey.freq_hz;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding external key for freq id '||p_freq_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_create_full_list (
   p_sync_action_tab   IN   sync_action_tab_type
)
   RETURN VARCHAR2
IS

-- Purpose: Creates a dot separated list of all the projects component types,
--          component subtypes and component actions.
--
-- MODIFICATION HISTORY
-- Person        Date        Comments
-- ---------     ------      -------------------------------------------
-- Parmi Sahota  2002-03-11  Function Creation

   v_list        VARCHAR2 (30000);
   v_actionnum   NUMBER (9)      := p_sync_action_tab.FIRST;

BEGIN
   WHILE v_actionnum IS NOT NULL
   LOOP
      v_list    :=
             v_list
          || p_sync_action_tab(v_actionnum).action || '.';

      v_actionnum := p_sync_action_tab.NEXT(v_actionnum);
   END LOOP;
   -- remove the last dot
   v_list := substr(v_list,1,length(v_list)-1);
   RETURN v_list;
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_prtsubntwk_id
  ( p_subntwk_id  subntwk.subntwk_id%type)
  RETURN  varchar2 IS
  cursor c_parent_subntwk is
    select parent_subntwk_id
    from   subntwk s
    where  subntwk_id = to_number(p_subntwk_id);
   r_parent_subntwk c_parent_subntwk%rowtype;
BEGIN
    open c_parent_subntwk;
    fetch c_parent_subntwk into r_parent_subntwk;
    RETURN r_parent_subntwk.parent_subntwk_id;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding parent subntwk_id for subnetwork '||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_sbnt_ip_addr
  ( p_sbnt_id  varchar2)
  RETURN  varchar2 IS
  cursor c_sbnt_ip_addr is
    select ip_sbnt_addr
    from   ip_sbnt i
    where  i.ip_sbnt_id = to_number(p_sbnt_id);
   r_sbnt_ip_addr c_sbnt_ip_addr%rowtype;
BEGIN
    open c_sbnt_ip_addr;
    fetch c_sbnt_ip_addr into r_sbnt_ip_addr;
    RETURN r_sbnt_ip_addr.ip_sbnt_addr;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding sbnt_ip_addr for subnet '||p_sbnt_id);
END fn_st_sync_get_sbnt_ip_addr;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_subntwk_typ
  ( p_subntwk_id  varchar2)
  RETURN  varchar2 IS
  cursor c_subntwk_typ is
    select subntwk_typ_nm
    from   subntwk s, ref_subntwk_typ st
    where  s.subntwk_id = to_number(p_subntwk_id)
    and    s.subntwk_typ_id = st.subntwk_typ_id;
   r_subntwk_typ c_subntwk_typ%rowtype;
BEGIN
    open c_subntwk_typ;
    fetch c_subntwk_typ into r_subntwk_typ;
    RETURN r_subntwk_typ.subntwk_typ_nm;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding subntwk typ id'||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_extkey4ip_sbnt
  ( p_subntwk_id  varchar2)
  RETURN  varchar2 IS
  l_subntwk_typ_nm  ref_subntwk_typ.subntwk_typ_nm%type;
  l_extkey  subntwk_parm.val%type;
BEGIN
    select typ.subntwk_typ_nm into l_subntwk_typ_nm
    from   ref_subntwk_typ typ, subntwk snw
    where  snw.subntwk_id = p_subntwk_id
    and    typ.subntwk_typ_id = snw.subntwk_typ_id;

    if l_subntwk_typ_nm = 'pop_router' then
      l_extkey := fn_st_sync_get_parent_extkey(p_subntwk_id => p_subntwk_id);
    else
      l_extkey := fn_st_sync_get_subntwk_extkey(p_subntwk_id => p_subntwk_id);
    end if;

    return l_extkey;

EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding ip subnet external key for subntwk_id '||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_card_extkey
  ( p_card_id  varchar2)
  RETURN  varchar2 IS
  cursor c_card_extkey is
    select external_key
    from   card
    where  card_id = to_number(p_card_id);
   r_card_extkey c_card_extkey%rowtype;
BEGIN
    open c_card_extkey;
    fetch c_card_extkey into r_card_extkey;
    close c_card_extkey;
    RETURN r_card_extkey.external_key ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding external key for card id '||p_card_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_sbnt_bcast_addr
  ( p_sbnt_id  varchar2)
  RETURN  varchar2 IS
  cursor c_sbnt_bcast_addr is
    select ip_broadcast_addr
    from   ip_sbnt i
    where  i.ip_sbnt_id = to_number(p_sbnt_id);
   r_sbnt_bcast_addr c_sbnt_bcast_addr%rowtype;
BEGIN
    open c_sbnt_bcast_addr;
    fetch c_sbnt_bcast_addr into r_sbnt_bcast_addr;
    RETURN r_sbnt_bcast_addr.ip_broadcast_addr;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding broadcast address for subnet '||p_sbnt_id);
END fn_st_sync_get_sbnt_bcast_addr;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_parm_val
  ( p_sync_action_tab IN sync_action_tab_type,
    p_actionnum       IN number,
    p_action          IN varchar2,
    p_parm_nm         IN varchar2)
  RETURN  varchar2 IS
--
-- To modify this template, edit file FUNC.TXT in TEMPLATE
-- directory of SQL Navigator
--
-- Purpose: returns the value of the specified parameter for the next action that
--          matches p_action beginning from p_actionnum
--
-- MODIFICATION HISTORY
-- Person        Date        Comments
-- ---------     ------      -------------------------------------------
-- Parmi Sahota  2002-03-16  Function Creation

   l_actionnum   number;
   l_parmnum     number;
   l_parm_val    varchar2(1000);

   e_actionnotfound  exception;

   cursor c_parm_val(
     p_sync_action_parm_id sync_action_parms_tmp.sync_action_parm_id%type,
     p_parm_nm  varchar2) is
     select parm_val
     from   sync_action_parms_tmp
     where  sync_action_parm_id = p_sync_action_parm_id
     and    parm_nm = p_parm_nm;

BEGIN
    -- find next matching action
    l_actionnum:=p_actionnum;
    while l_actionnum IS NOT NULL and p_sync_action_tab(l_actionnum).action!=p_action loop
      l_actionnum := p_sync_action_tab.NEXT(l_actionnum);
    end loop;

    if l_actionnum is null then
        raise_application_error (-20103, 'FN_ST_SYNC_GET_PARM_VAL: Could not find matching action '||p_action||
        ' starting at element '||p_actionnum);
    end if;

    -- find parameter within this action
    l_parm_val:=null;
    for r_parm_val in c_parm_val(
      p_sync_action_parm_id => p_sync_action_tab(l_actionnum).sync_action_parm_id,
      p_parm_nm  =>  p_parm_nm)
    loop
      l_parm_val := r_parm_val.parm_val;
    end loop;

    RETURN l_parm_val ;
EXCEPTION
   WHEN others THEN
       raise ;
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_port_extkey
  ( p_port_id  varchar2)
  RETURN  varchar2 IS
  cursor c_port_extkey is
    select external_key
    from   port
    where  port_id = to_number(p_port_id);
   r_port_extkey c_port_extkey%rowtype;
BEGIN
    open c_port_extkey;
    fetch c_port_extkey into r_port_extkey;
    close c_port_extkey;
    RETURN r_port_extkey.external_key;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding external key for port id '||p_port_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_fromlink_typ
  ( p_link_id  varchar2)
  RETURN  varchar2 IS
  cursor c_link_extkey is
    select subntwk_typ_nm
    from   link a, subntwk b, ref_subntwk_typ c
    where  a.link_id = p_link_id
    and    a.from_subntwk_id = b.subntwk_id
    and    b.subntwk_typ_id = c.subntwk_typ_id;
   r_link_extkey c_link_extkey%rowtype;
BEGIN
    open c_link_extkey;
    fetch c_link_extkey into r_link_extkey;
    close c_link_extkey;
    RETURN r_link_extkey.subntwk_typ_nm ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding from subnetwork type for link_id '||p_link_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_subntwk_nm
  ( p_subntwk_id  varchar2)
  RETURN  varchar2 IS
  cursor c_subntwk_nm is
    select subntwk_nm
    from   subntwk s
    where  s.subntwk_id = to_number(p_subntwk_id);
  r_subntwk_nm c_subntwk_nm%rowtype;
BEGIN
    open c_subntwk_nm;
    fetch c_subntwk_nm into r_subntwk_nm;
    RETURN r_subntwk_nm.subntwk_nm;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding subntwk_nm for subntwk_id'||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_tolink_typ
  ( p_link_id  varchar2)
  RETURN  varchar2 IS
  cursor c_link_extkey is
    select subntwk_typ_nm
    from   link a, subntwk b, ref_subntwk_typ c
    where  a.link_id = p_link_id
    and    a.to_subntwk_id = b.subntwk_id
    and    b.subntwk_typ_id = c.subntwk_typ_id;
   r_link_extkey c_link_extkey%rowtype;
BEGIN
    open c_link_extkey;
    fetch c_link_extkey into r_link_extkey;
    close c_link_extkey;
    RETURN r_link_extkey.subntwk_typ_nm ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding to subnetwork type for link_id '||p_link_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_parent_extkey
  ( p_subntwk_id  varchar2)
  RETURN  varchar2 IS
  l_parent_subntwk_id subntwk.parent_subntwk_id%type;
BEGIN
  select parent_subntwk_id into l_parent_subntwk_id
  from subntwk
  where subntwk_id = p_subntwk_id;

  return (fn_st_sync_get_subntwk_extkey(p_subntwk_id => l_parent_subntwk_id));

EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding parent external key for subntwk_id '||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_subntwk_extkey
  ( p_subntwk_id  varchar2)
  RETURN  varchar2 IS
  cursor c_subntwk_extkey is
    select val
    from   subntwk_parm
    where  subntwk_id = to_number(p_subntwk_id)
    and    parm_id in
        (select parm_id
         from   parm
         where  parm_nm = 'external_key'
         and    class_id in
           (select class_id
            from   ref_class
            where  class_nm = 'subntwk_typ'));
   r_subntwk_extkey c_subntwk_extkey%rowtype;
BEGIN
    open c_subntwk_extkey;
    fetch c_subntwk_extkey into r_subntwk_extkey;
    close c_subntwk_extkey;
    RETURN r_subntwk_extkey.val ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding external key for subntwk_id '||p_subntwk_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_sbnt_dflt_gtwy
  ( p_sbnt_id  varchar2)
  RETURN  varchar2 IS
  cursor c_sbnt_dflt_gtwy is
    select ip_sbnt_gateway_addr
    from   ip_sbnt i
    where  i.ip_sbnt_id = to_number(p_sbnt_id);
   r_sbnt_dflt_gtwy c_sbnt_dflt_gtwy%rowtype;
BEGIN
    open c_sbnt_dflt_gtwy;
    fetch c_sbnt_dflt_gtwy into r_sbnt_dflt_gtwy;
    RETURN r_sbnt_dflt_gtwy.ip_sbnt_gateway_addr;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding default gateway for subnet '||p_sbnt_id);
END fn_st_sync_get_sbnt_dflt_gtwy;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_fromlink_extkey
  ( p_link_id  varchar2)
  RETURN  varchar2 IS
  cursor c_link_extkey is
    select val
    from   link a, subntwk_parm b
    where  a.link_id = p_link_id
    and    a.from_subntwk_id = b.subntwk_id
    and    b.parm_id in
        (select parm_id
         from   parm
         where  parm_nm = 'external_key'
         and    class_id in
           (select class_id
            from   ref_class
            where  class_nm = 'subntwk_typ'));
   r_link_extkey c_link_extkey%rowtype;
BEGIN
    open c_link_extkey;
    fetch c_link_extkey into r_link_extkey;
    close c_link_extkey;
    RETURN r_link_extkey.val ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding from subnetwork external key for link_id '||p_link_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_sbnt_max_hosts
  ( p_sbnt_id  varchar2)
  RETURN  varchar2 IS
  cursor c_sbnt_max_hosts is
    select ip_max_hosts
    from   ip_sbnt i
    where  i.ip_sbnt_id = to_number(p_sbnt_id);
   r_sbnt_max_hosts c_sbnt_max_hosts%rowtype;
BEGIN
    open c_sbnt_max_hosts;
    fetch c_sbnt_max_hosts into r_sbnt_max_hosts;
    RETURN r_sbnt_max_hosts.ip_max_hosts;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding max hosts for subnet '||p_sbnt_id);
END fn_st_sync_get_sbnt_max_hosts;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_tolink_extkey
  ( p_link_id  varchar2)
  RETURN  varchar2 IS
  cursor c_link_extkey is
    select val
    from   link a, subntwk_parm b
    where  a.link_id = p_link_id
    and    a.to_subntwk_id = b.subntwk_id
    and    b.parm_id in
        (select parm_id
         from   parm
         where  parm_nm = 'external_key'
         and    class_id in
           (select class_id
            from   ref_class
            where  class_nm = 'subntwk_typ'));
   r_link_extkey c_link_extkey%rowtype;
BEGIN
    open c_link_extkey;
    fetch c_link_extkey into r_link_extkey;
    close c_link_extkey;
    RETURN r_link_extkey.val ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding external key for link_id '||p_link_id);
END;
/

CREATE OR REPLACE 
FUNCTION fn_st_sync_get_card_srlnum
  ( p_card_id  varchar2)
  RETURN  varchar2 IS
  cursor c_card_srlnum is
    select srl_num
    from   card
    where  card_id = to_number(p_card_id);
   r_card_srlnum c_card_srlnum%rowtype;
BEGIN
    open c_card_srlnum;
    fetch c_card_srlnum into r_card_srlnum;
    close c_card_srlnum;
    RETURN r_card_srlnum.srl_num ;
EXCEPTION
 WHEN others THEN
    RAISE_APPLICATION_ERROR(-20100, 'Error finding serial number for card id '||p_card_id);
END;
/

CREATE OR REPLACE 
PROCEDURE prc_st_sync_add_debug_msg(p_proc in varchar2, p_message varchar2) is
  l_temp varchar2(4000);
begin
  l_temp := substr(p_message,1,3999);
  insert into sync_debug(sync_debug_seq, proc_call, message)
  values(sync_debug_seq.nextval, p_proc, l_temp);
  commit;
end prc_st_sync_add_debug_msg;
/

CREATE OR REPLACE 
PROCEDURE prc_st_sync_create_actionlists
   IS
-- Purpose: Creates all the actions lists and num_sctions_in_grp for each group
--
  cursor c_grp is
    select grp_nm
      from sync_grps;

  cursor c_action (p_grp_nm in sync_grps.grp_nm%type) is
    select action
      from sync_grp_actions
      where grp_nm = p_grp_nm
      order by action_seq_within_grp;

  l_list                  sync_grps.grp_action_list%type;
  l_num_actions_in_grp    sync_grps.num_actions_in_grp%type;

BEGIN
    for r_grp in c_grp loop
      l_list := null;
      l_num_actions_in_grp := 0;
      for r_action in c_action(p_grp_nm => r_grp.grp_nm) loop
        l_list := l_list ||r_action.action||'.';
        l_num_actions_in_grp := l_num_actions_in_grp + 1;
      end loop;
      -- remove trailing dot from action list
      l_list := substr(l_list,1,length(l_list)-1);
      update sync_grps
      set    grp_action_list = l_list,
             num_actions_in_grp = l_num_actions_in_grp
      where  grp_nm = r_grp.grp_nm;
    end loop;
    commit;
EXCEPTION
   WHEN OTHERS
   THEN
      raise_application_error (
         -20101,
         'Error creating grp_action_lists in proc PRC_ST_SYNC_CREATE_ACTIONLISTS. '
      );
END; -- Procedure
/

CREATE OR REPLACE 
PROCEDURE prc_find_largest_matching_grp
   ( p_action_list        IN  varchar2,
     p_grp_nm_found       OUT varchar2,
     p_grp_list_found     OUT varchar2,
     p_num_actions_in_grp OUT number)
   IS
--
-- Purpose: Finds the largest group that matches the passed in list p_action_list
--
-- MODIFICATION HISTORY
-- Person        Date       Comments
-- ---------     ------     -------------------------------------------
-- Parmi Sahota  2002-03-11 Procedure Creation
--

   CURSOR c_matching_grp (p_list VARCHAR2)
   IS
      SELECT grp_nm, grp_action_list, num_actions_in_grp
        FROM sync_grps
       WHERE grp_action_list =
                           SUBSTR (
                              p_list,
                              1,
                              LENGTH (
                                 grp_action_list
                              )
                           )
       ORDER BY num_actions_in_grp desc;
   r_matching_grp   c_matching_grp%ROWTYPE;

BEGIN
   OPEN c_matching_grp (
      p_list=> substr(p_action_list,1,4000)
   );
   FETCH c_matching_grp INTO r_matching_grp;
   CLOSE c_matching_grp;
   p_grp_nm_found        := r_matching_grp.grp_nm;
   p_grp_list_found      := r_matching_grp.grp_action_list;
   p_num_actions_in_grp  := r_matching_grp.num_actions_in_grp;
EXCEPTION
WHEN OTHERS
   THEN
      raise_application_error (
         -20102,
         'Error in proc PRC_FIND_LARGEST_MATCHING_GRP'
      );
END; -- Procedure
/

CREATE OR REPLACE 
PROCEDURE prc_stm_sync_proj (p_sync_action_tab IN sync_action_tab_type)
IS
   e_actionlist_notfound   EXCEPTION;
   l_remaining_action_list     VARCHAR2 (30000);
   l_new_action_list           VARCHAR2 (30000);
   l_grp_nm_found          SYNC_GRPS.grp_nm%TYPE;
   l_grp_list_found            SYNC_GRPS.grp_action_list%TYPE;

   l_external_key          VARCHAR2(1000); -- used to hold external key return values
   l_call                  VARCHAR2(3000); -- used to call the stored procs
   l_func_call             VARCHAR2(1000); -- used to call internal functions to obtain values for the main proc call
   l_func_returnval        VARCHAR2(1000); -- holds the return value of internal function calls
   l_call_portion          VARCHAR2(1000); -- holds a temporary portion of an action call

   l_has_external_key            CHAR(1);  -- determines whether the external procedure call will return an external key or not
                                           -- for now this is determined by whether or not the procedure has any out parameters
   l_add_ext_key_call            VARCHAR2(3000); -- makes the call to add the external_key
   l_add_ext_key_proc_nm         SYNC_SRC_EXT_PARMS.add_ext_key_proc_nm%TYPE;
   l_ext_key_action_identifier   SYNC_SRC_EXT_PARMS.ext_key_action_identifier%TYPE;
   l_ext_key_parm_nm_identifier  SYNC_SRC_EXT_PARMS.ext_key_parm_nm_identifier%TYPE;
   l_ext_key_identifier_parm_val VARCHAR2(2000);  -- usually number but may be string

   l_debug_level           NUMBER := -1;

   l_num_actions_in_grp NUMBER;
   l_starting_element_in_grp NUMBER:=1;

   CURSOR c_grps_to_procs(p_grp_nm SYNC_GRPS.grp_nm%TYPE) IS
   SELECT ext_proc_nm
     FROM SYNC_GRP_EXT_PROCS
    WHERE grp_nm = p_grp_nm;

   CURSOR c_procs_to_parms(p_ext_proc_nm IN SYNC_EXT_PROCS.ext_proc_nm%TYPE) IS
   SELECT ext_proc_parm_nm,
          direction
     FROM SYNC_EXT_PROC_PARMS
    WHERE ext_proc_nm = p_ext_proc_nm
    ORDER BY ext_parm_seq;

   CURSOR c_parms_src (p_ext_proc_nm       SYNC_SRC_EXT_PARMS.ext_proc_nm%TYPE,
                       p_ext_proc_parm_nm  SYNC_SRC_EXT_PARMS.ext_proc_parm_nm%TYPE,
                       p_scope_grp         SYNC_SRC_EXT_PARMS.scope_grp%TYPE) IS
   SELECT src_func_nm,
          src_grp_nm,
          src_parm_nm,
          src_action,
          src_fixed_val,
          add_ext_key_proc_nm,
          ext_key_action_identifier,
          ext_key_parm_nm_identifier
     FROM SYNC_SRC_EXT_PARMS
    WHERE ext_proc_nm      = p_ext_proc_nm
      AND ext_proc_parm_nm = p_ext_proc_parm_nm
      AND NVL(scope_grp, p_scope_grp) = p_scope_grp;

BEGIN

  -- clear debug table
  IF l_debug_level >= 1 THEN
    DELETE SYNC_DEBUG; COMMIT;
    Prc_St_Sync_Add_Debug_Msg('PRC_STM_SYNC_PROJ ', 'Starting...');
  END IF;


-- get complete list of all actions
  l_remaining_action_list    :=
         Fn_St_Sync_Create_Full_List (
            p_sync_action_tab=> p_sync_action_tab
         );

  IF l_debug_level >= 1 THEN
    Prc_St_Sync_Add_Debug_Msg('Initial list ', l_remaining_action_list);
  END IF;

  -- while there are more actions to be synchronized
  WHILE LENGTH (l_remaining_action_list) > 0
  LOOP
    -- find the next matching group
    Prc_Find_Largest_Matching_Grp (
       p_action_list        => substr(l_remaining_action_list,1,4000),
       p_grp_nm_found       => l_grp_nm_found,
       p_grp_list_found     => l_grp_list_found,
       p_num_actions_in_grp => l_num_actions_in_grp);

    IF l_debug_level >= 2 THEN
      Prc_St_Sync_Add_Debug_Msg('Remaining list is ', l_remaining_action_list);
      Prc_St_Sync_Add_Debug_Msg ('grp Found:', l_grp_nm_found);
      Prc_St_Sync_Add_Debug_Msg ('List Found:', l_grp_list_found);
      Prc_St_Sync_Add_Debug_Msg ('l_starting_element_in_grp',l_starting_element_in_grp);
    END IF;

    -- Check if a valid set of sync actions could be found.
    -- If so then cut the sync actions from the main list
    IF l_grp_nm_found IS NULL
    THEN
       -- If no group is found then check to see this is a xxx.yyy.lookup action. If yes then prune it and move on
       IF SUBSTR(l_remaining_action_list,INSTR(l_remaining_action_list,'.',1,2)+1) LIKE 'lookup%' THEN

         IF l_debug_level >= 2 THEN
           Prc_St_Sync_Add_Debug_Msg('PRE LOOKUP PRUNE =',l_remaining_action_list);
         END IF;
         l_new_action_list := SUBSTR (l_remaining_action_list, INSTR(l_remaining_action_list,'.',1,3)+1);
         IF l_new_action_list = l_remaining_action_list THEN
           l_remaining_action_list := NULL;
         ELSE
           l_remaining_action_list := l_new_action_list;
         END IF;

         l_num_actions_in_grp := 1;
         IF l_debug_level >= 2 THEN
            Prc_St_Sync_Add_Debug_Msg('POST LOOKUP PRUNE =',l_remaining_action_list);
         END IF;
       ELSE
         -- if not then we have a non lookup action that we cannot find in our table of groups (grps.list)
         RAISE e_actionlist_notfound;
       END IF;
    ELSE
       l_remaining_action_list := SUBSTR (l_remaining_action_list, LENGTH (l_grp_list_found)+2);
       -- move to end of list + dot + 1 = grp_list_length + 2

      -- we have found the group, so check to see what external procedures we need to call
      FOR r_grps_to_procs IN c_grps_to_procs(p_grp_nm => l_grp_nm_found) LOOP
        l_has_external_key := 'n';
        l_call:='call '||UPPER(r_grps_to_procs.ext_proc_nm)||'(';

        -- find the parameters for the external procedure
        FOR r_procs_to_parms IN c_procs_to_parms(p_ext_proc_nm => r_grps_to_procs.ext_proc_nm) LOOP
          -- dynamic sql does not support named parameters,k so they need to be positional otherwise we could use:
          --l_call:=l_call||r_procs_to_parms.ext_proc_parm_nm||'=>';

          -- find the source for each ext proc parameter
          FOR r_parms_src IN c_parms_src (
            p_ext_proc_nm      => r_grps_to_procs.ext_proc_nm,
            p_ext_proc_parm_nm => r_procs_to_parms.ext_proc_parm_nm,
            p_scope_grp        => l_grp_nm_found) LOOP

            -- call portion is one peice of the whole call. It is either a fixed value (from fixed value or from object collection)
            --  or a function call (from fixed value or from object collection)
            l_call_portion:=NULL;

            -- if out parameter
            IF r_procs_to_parms.direction = 'out' THEN
              l_has_external_key := 'y';
              l_call:=l_call ||':l_external_key';

              l_add_ext_key_proc_nm := r_parms_src.add_ext_key_proc_nm;
              l_ext_key_action_identifier := r_parms_src.ext_key_action_identifier;
              l_ext_key_parm_nm_identifier := r_parms_src.ext_key_parm_nm_identifier;

            -- else if fixed value
            ELSIF r_parms_src.src_fixed_val IS NOT NULL THEN
              l_call_portion  := ''''||r_parms_src.src_fixed_val||'''';

            -- else if values is sourced from the passed in object collection
            ELSIF r_parms_src.src_grp_nm IS NOT NULL THEN

              IF l_debug_level > 1 THEN
                Prc_St_Sync_Add_Debug_Msg('FN_ST_SYNC_GET_PARM_VAL', 'Finding val for parm '||r_procs_to_parms.ext_proc_parm_nm||'*p_actionnum='||l_starting_element_in_grp||
                     '*p_action='||r_parms_src.src_action||'*p_parm_nm='||r_parms_src.src_parm_nm);
              END IF;

              -- set l_call_portion to object parameter value
              l_call_portion  := ''''||
                Fn_St_Sync_Get_Parm_Val
                ( p_sync_action_tab => p_sync_action_tab,
                  p_actionnum       => l_starting_element_in_grp,
                  p_action          => r_parms_src.src_action,
                  p_parm_nm         => r_parms_src.src_parm_nm)||'''';

              IF l_debug_level > 1 THEN
                Prc_St_Sync_Add_Debug_Msg('FN_ST_SYNC_GET_PARM_VAL', 'Found val for parm '||r_procs_to_parms.ext_proc_parm_nm||'  **p_actionnum='||l_starting_element_in_grp
                   ||'**p_action='||r_parms_src.src_action||'**p_parm_nm='||r_parms_src.src_parm_nm||'****returns='||l_call_portion);
              END IF;

            END IF;

            -- if a function name is supplied call it with the l_call_portion
            IF r_parms_src.src_func_nm IS NOT NULL THEN
              l_func_call := 'call '||r_parms_src.src_func_nm ||'('||l_call_portion||') into :p1';

              EXECUTE IMMEDIATE l_func_call USING OUT l_func_returnval;
              l_call := l_call ||''''||l_func_returnval||'''';

              IF l_debug_level > 1 THEN
                Prc_St_Sync_Add_Debug_Msg('Calling func to source val', 'l_func_call: '||l_func_call||', returned_val='||l_func_returnval);
              END IF;

            ELSE
              l_call:=l_call||l_call_portion;
            END IF;
            l_call:=l_call||', ';

          END LOOP;  -- source of external parameter
        END LOOP;   -- parameters of external procedure

        --remove last comma and insert end bracket
        l_call:=SUBSTR(l_call,1,LENGTH(RTRIM(l_call))-1)||')';

        -- MAKE EXTERNAL PROCEDURE CALL. If external key then get it otherwise just execute
        IF (l_has_external_key = 'y' AND l_add_ext_key_proc_nm IS NOT NULL) THEN
          IF l_debug_level >=0 THEN
            Prc_St_Sync_Add_Debug_Msg('l_has_external_key = y', l_call);
          END IF;

          EXECUTE IMMEDIATE l_call USING OUT l_external_key;
          IF l_debug_level >=0 THEN
            Prc_St_Sync_Add_Debug_Msg('external key returned is', l_external_key);
          END IF;


          -- find the id sting of the subnetwork to which the external key will be attached
          l_call_portion  := ''''||
                Fn_St_Sync_Get_Parm_Val
                ( p_sync_action_tab => p_sync_action_tab,
                  p_actionnum       => l_starting_element_in_grp,
                  p_action          => l_ext_key_action_identifier,
                  p_parm_nm         => l_ext_key_parm_nm_identifier)||'''';


          -- network_id is always the first parameter followed by the external kay
          l_add_ext_key_call := 'call '||l_add_ext_key_proc_nm||'('||l_call_portion||','||''''||l_external_key||''''||')';

          IF l_debug_level >=0 THEN
            Prc_St_Sync_Add_Debug_Msg('update external key call', l_add_ext_key_call);
          END IF;

          EXECUTE IMMEDIATE l_add_ext_key_call;

        ELSE
          IF l_debug_level >=0 THEN
            Prc_St_Sync_Add_Debug_Msg('l_has_external_key = N', l_call);
          END IF;

          EXECUTE IMMEDIATE l_call;
        END IF;
      END LOOP;

    END IF;

    -- advance starting point to element immediately after the end of the current grp
    l_starting_element_in_grp := l_starting_element_in_grp + l_num_actions_in_grp;
  END LOOP;

  IF l_debug_level >=1 THEN Prc_St_Sync_Add_Debug_Msg('Remaining list ', l_remaining_action_list); END IF;
EXCEPTION
  WHEN e_actionlist_notfound
  THEN
    RAISE_APPLICATION_ERROR (
         -20200,
            'There are no group actions defined for input action list '
         || SUBSTR (l_remaining_action_list, 1, 100));
END Prc_Stm_Sync_Proj;
/



