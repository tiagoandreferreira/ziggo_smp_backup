--==========================================================================
-- FILE INFO
--    $Id: sync_objs.sql,v 1.4 2003/01/07 00:21:22 parmis Exp $
--
-- DESCRIPTION
--
-- REVISION HISTORY:
--   * Based on CVS log
--   * A Siddique    2001-08-20
--     Modified data to reflect the rogers implementation spec
--==========================================================================
BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE parm_type FORCE';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE 
TYPE parm_type
AS OBJECT
--
-- Purpose: Object type to hold name/value pairs for parameters
--
-- MODIFICATION HISTORY
-- Person        Date        Comments
-- ---------     ------      ------------------------------------------
-- Parmi sahota  7 Mar 2002  Type creation

(
   parm_nm   varchar2(255),
   parm_val  varchar2(1000)
)
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE parm_tab_type FORCE';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE 
TYPE parm_tab_type
 AS TABLE OF PARM_TYPE
/


BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE sync_action_type FORCE';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE 
TYPE sync_action_type
AS OBJECT
(
   ntwk_cmpnt_typ_nm    varchar2(30),
   ntwk_cmpnt_subtyp    varchar2(50),
   topology_action_typ  varchar2(20),
   action               varchar2(100),
   sync_action_parm_id  varchar2(30)
)
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TYPE sync_action_tab_type FORCE';
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE 
TYPE sync_action_tab_type
 AS TABLE OF SYNC_ACTION_TYPE
/


