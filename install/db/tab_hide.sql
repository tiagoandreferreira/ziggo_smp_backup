--==========================================================================
-- FILE INFO
--    $Id: tab_hide.sql,v 1.2 2011/12/12 12:30:35 prithvim Exp $
--
-- DESCRIPTION
--
--========================================================================== 

exec am_add_secure_obj('samp.web.menu.admin', 'Access Control Tab');
exec am_add_privilege('samp.web.menu.admin', 'y', 'csr_admin', 'view');
exec am_delete_privilege('samp.web.menu.admin','csr_admin','view')
exec am_add_privilege('samp.web.menu.admin', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.menu.profile', 'Profile Tab');
exec am_add_privilege('samp.web.menu.profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.profile', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.menu.reports', 'Reports Tab');
exec am_add_privilege('samp.web.menu.reports', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.reports', 'y', 'csr_admin', 'edit');


exec am_add_secure_obj('samp.web.menu.search.help', 'Serach Help Tab');
exec am_add_privilege('samp.web.menu.search.help', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.search.help', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.menu.subscriber.help', 'Subscriber Help Tab');
exec am_add_privilege('samp.web.menu.subscriber.help', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.subscriber.help', 'y', 'csr_admin', 'edit');


exec am_add_secure_obj('samp.web.menu.orders.help', 'Order Help Tab');
exec am_add_privilege('samp.web.menu.orders.help', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.orders.help', 'y', 'csr_admin', 'edit');



exec am_add_secure_obj('samp.web.menu.manual_task_search.help', 'Manual Task Help Tab');
exec am_add_privilege('samp.web.menu.manual_task_search.help', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.manual_task_search.help', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.menu.app_cfg.help', 'Configuration Help Tab');
exec am_add_privilege('samp.web.menu.app_cfg.help', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.app_cfg.help', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.menu.search.batchorder', 'Batch Order Tab');
exec am_add_privilege('samp.web.menu.search.batchorder', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.search.batchorder', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.menu.app_cfg.large_sub', 'large_sub  Tab');
exec am_add_privilege('samp.web.menu.app_cfg.large_sub', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.app_cfg.large_sub', 'y', 'csr_admin', 'edit');



commit;
