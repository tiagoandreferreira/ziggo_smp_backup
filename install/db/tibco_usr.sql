--============================================================================
--    $Id: tibco_usr.sql,v 1.1 2012/01/06 12:18:09 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

exec am_create_user('tibco_usr', 'pwtibco_usr', 'Canadian Customer Sales Representative Administrator');
exec am_add_user_parm('tibco_usr', 'country', 'CA');
exec am_add_user_parm('tibco_usr', 'language', 'en');
exec am_add_grp_user('tibco_usr', 'csr_admin');
exec am_add_grp_user('tibco_usr', 'Administrators');
