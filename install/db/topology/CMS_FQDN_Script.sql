spool CMS_FQDN_Script.log
-- ============================================================================
--  Id: CMS_FQDN_Script.sh,v 1.0 2014/06/04 11:28:28 Jalay Exp $


exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS05N1GN.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS05N2GN.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS01N1MND.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS01N2MND.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS02N1MND.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS02N2MND.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS01N1GV.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS01N2GV.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS02N1GV.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS02N2GV.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS05N1TB.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS05N2TB.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS06N1TB.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS06N2TB.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS07N1TB.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'cmsfqdn',      'CMS07N2TB.TELEFONIE.ZIGGO.LOCAL');

exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'cmsfqdn',      'bwcms01.telefonie.ziggo.local');

exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'cmsfqdn',      'bwcms02.telefonie.ziggo.local');

exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'cmsfqdn',      'bwcms03.telefonie.ziggo.local');

-- Entry for updatating Device type of Broadsoft--

update subntwk_parm set val='Generic MGCP NCS - Europe' where parm_id = (select parm_id from parm where parm_nm ='device_type') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%');

commit;

spool off

exit

