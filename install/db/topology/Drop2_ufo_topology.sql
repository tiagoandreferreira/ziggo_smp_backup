	--============================================================================
-- $Id: Drop2_ufo_topology,v 1.1 2013/10/14 Jalay Exp $ Updated to add links between NSN and HiQ/BS
-- $Id: Drop2_ufo_topology,v 1.2 2013/10/30 Jalay Exp $ Updated to add link between BreezPortaOne and BS
--  REVISION HISTORY

--============================================================================
		spool ufo_topology_2_combined.log
		set escape on
		set serveroutput on


exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'device_type', 'Ziggo TEMP NCS');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'device_type', 'Ziggo TEMP NCS');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'device_type', 'Ziggo TEMP NCS');

update subntwk_parm set val='1141' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_3_10'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_2_10'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_1_10'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

-- Link added for BreezPortaOne and BS

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Breezz_PortaOne_1'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

update subntwk_parm set val='9' where parm_id = (select parm_id from parm where parm_nm ='grp_id') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%')	

EXECUTE DBMS_OUTPUT.PUT_LINE('---- UFO Topology 2 loaded successfuly ----');
commit;

spool off
exit;