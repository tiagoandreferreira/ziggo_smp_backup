	--============================================================================
-- $Id: Drop4_ufo_topology,v 1.1 2014/01/15 Jalay Exp $ 
--  REVISION HISTORY

--============================================================================
		spool ufo_topology_4_combined.log
		set escape on
		set serveroutput on


update subntwk_parm set val='false' where parm_id = (select parm_id from parm where parm_nm ='default_scr_sub_controlled') and subntwk_id in(select subntwk_id from subntwk where  subntwk_nm like '%TELEFONIE.ZIGGO.LOCAL');

INSERT INTO Sbnt_Tech_Platform
  (
    subntwk_id,
    tech_platform_id,
    mgmt_mode_id,
    created_dtm,
    created_by,
    modified_dtm,
    modified_by
  )
  VALUES
  (
    SubntwkId('box_1'),
    TechPlatform('notification_01'),
    MgmtMode('provision'),
    SYSDATE,
    'INIT',
    NULL,
    NULL
  );

update subntwk_parm set val='rdu1.mnd.bacc.oss.local,rdu1.gn.bacc.oss.local,rdu2.emn.bacc.oss.local' where parm_id = (select parm_id from parm where parm_nm ='rdu_list') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm = 'NAMDPM');

update subntwk_parm set val='rdu1.mnd.bacc.oss.local,rdu1.gn.bacc.oss.local,rdu2.emn.bacc.oss.local' where parm_id = (select parm_id from parm where parm_nm ='rdu_list') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm = 'NAMBAC');

update subntwk_parm set val='rdu1.mnd.bacc.oss.local,rdu1.gn.bacc.oss.local,rdu2.emn.bacc.oss.local' where parm_id = (select parm_id from parm where parm_nm ='rdu_list') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm = 'NAMALU');

update subntwk_parm set val='Generic MGCP NCS - Europe' where parm_id = (select parm_id from parm where parm_nm ='device_type') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%');
	

EXECUTE DBMS_OUTPUT.PUT_LINE('---- UFO Topology 4 loaded successfuly ----');
commit;

spool off
exit;