--============================================================================
-- $Id: Gateway_IP_address_selection.sql,v 1.1 2014/07/01 sunil Exp $ 
--									 ,
--  REVISION HISTORY

--============================================================================

update subntwk_parm set val='GwIPAddrVirtualIP1' where parm_id = (select parm_id from parm where parm_nm ='default_mgc_ip_assignment') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm IN ('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL','CMS06N1TB.TELEFONIE.ZIGGO.LOCAL','CMS05N1TB.TELEFONIE.ZIGGO.LOCAL','CMS02N1GV.TELEFONIE.ZIGGO.LOCAL','CMS01N1GV.TELEFONIE.ZIGGO.LOCAL','CMS02N1MND.TELEFONIE.ZIGGO.LOCAL','CMS01N1MND.TELEFONIE.ZIGGO.LOCAL','CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'));


update subntwk_parm set val='GwIPAddrVirtualIP2' where parm_id = (select parm_id from parm where parm_nm ='default_mgc_ip_assignment') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm IN ('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL','CMS06N2TB.TELEFONIE.ZIGGO.LOCAL','CMS05N2TB.TELEFONIE.ZIGGO.LOCAL','CMS02N2GV.TELEFONIE.ZIGGO.LOCAL','CMS01N2GV.TELEFONIE.ZIGGO.LOCAL','CMS02N2MND.TELEFONIE.ZIGGO.LOCAL','CMS01N2MND.TELEFONIE.ZIGGO.LOCAL','CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'));

commit;