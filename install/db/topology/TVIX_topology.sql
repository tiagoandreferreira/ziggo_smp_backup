----------------------
---- Adding TechPlatform ---- 
----------------------

exec AddTechPlatform('tvix_01', TechPlatformTyp('tvix'));

exec AddTechPlatformMgmtMode(TechPlatform('tvix_01'), MgmtMode('provision'));


--------------
--- Adding subntwk for TVIX ----
--------------
exec AddSubntwk('ZiggoVideoIP', SubntwkTyp('video_ip_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

--------------
--- Adding links between subntwk and super region ----
--------------

exec AddLink('ZiggoVideoIP_TO_Netherlands', LinkTyp('logical'), SubntwkId('ZiggoVideoIP'), SubntwkId('Netherlands'));

--------------
--- Adding links between subntwk and Tech Platform ----
--------------

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('ZiggoVideoIP'), TechPlatform('tvix_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------
--- Adding subntwk Parms for Prodis ----
--------------

exec AddSubntwkParm(SubntwkId('ZiggoVideoIP'), 'name', 'ZiggoVideoIP');
exec AddSubntwkParm(SubntwkId('ZiggoVideoIP'), 'manufacturer', 'StandardCAS');
exec AddSubntwkParm(SubntwkId('ZiggoVideoIP'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('ZiggoVideoIP'), 'version', '2.1');

commit;




