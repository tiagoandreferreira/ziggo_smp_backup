	
	-- Deleting subnetwork parms
	
		DELETE
		FROM SUBNTWK_PARM
		WHERE SUBNTWK_ID IN
		  (SELECT SUBNTWK_ID FROM SUBNTWK WHERE subntwk_nm = 'ZiggoVideoIP'
		  );
	
	-- Deleting mapping between subnetwork and tech platforms
	
		DELETE
		FROM Sbnt_Tech_Platform
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'tvix_01'
		  );
	
	-- Deleting links
	
		DELETE
		FROM LINK
		WHERE LINK_NM LIKE 'ZiggoVideoIP_TO_Netherlands';
	
	-- Deleting subnetwork
	
		DELETE FROM subntwk WHERE subntwk_nm = 'ZiggoVideoIP';
	
	-- Deleting techPlatform mgt mode
	
		DELETE
		FROM tech_platform_mgmt_mode
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'tvix_01'
		  );
	
	-- Deleting techPlatforms
	
		DELETE
		FROM TECH_PLATFORM
		WHERE TECHN_PLATFORM_NM = 'tvix_01';
	
	-- Deleting techPlatformtypes
	
		DELETE
		FROM TECH_PLATFORM_TYP
		WHERE TECH_PLATFORM_TYP_NM = 'tvix';