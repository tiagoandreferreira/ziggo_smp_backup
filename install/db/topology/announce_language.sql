--============================================================================
-- $Id: announce_language.sql,v 1.1 2014/08/14 Divya Exp $ 
--									 ,
--  REVISION HISTORY

--============================================================================

exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'announce_language',      'Nederlands');

exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'announce_language',      'Nederlands');

exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'announce_language',      'Nederlands');

commit;