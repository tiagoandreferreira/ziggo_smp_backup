--============================================================================
--    $Id: area_code_network.sql,v 1.4 2011/07/28 16:42:41 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  NPA/NXX/Rate Center/Line Option Network
--============================================================================
spool area_code_network.log

set escape on
set serveroutput onEXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions NPA/NXX/Rate Center/Line Option Network ----');

--------------
---- NPA -----
--------------
exec AddSubntwk('NPA-404', SubntwkTyp('npa'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('NPA-418', SubntwkTyp('npa'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('NPA-604', SubntwkTyp('npa'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('NPA-647', SubntwkTyp('npa'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('NPA-804', SubntwkTyp('npa'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('NPA-127', SubntwkTyp('npa'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('NPA-404'), 'name', 'NPA-404');
exec AddSubntwkParm(SubntwkId('NPA-404'), 'npa',  '404');
exec AddSubntwkParm(SubntwkId('NPA-418'), 'name', 'NPA-418');
exec AddSubntwkParm(SubntwkId('NPA-418'), 'npa',  '418');
exec AddSubntwkParm(SubntwkId('NPA-604'), 'name', 'NPA-604');
exec AddSubntwkParm(SubntwkId('NPA-604'), 'npa',  '604');
exec AddSubntwkParm(SubntwkId('NPA-647'), 'name', 'NPA-647');
exec AddSubntwkParm(SubntwkId('NPA-647'), 'npa',  '647');
exec AddSubntwkParm(SubntwkId('NPA-804'), 'name', 'NPA-804');
exec AddSubntwkParm(SubntwkId('NPA-804'), 'npa',  '804');
exec AddSubntwkParm(SubntwkId('NPA-127'), 'name', 'NPA-127');
exec AddSubntwkParm(SubntwkId('NPA-127'), 'npa',  '127');

---------------------
---- Rate Center ----
---------------------
exec AddSubntwk('RC-404-Atlanta',   SubntwkTyp('rate_center'), SubntwkId('NPA-404'), TopologyStatus('in_service'));
exec AddSubntwk('RC-418-OTTAWA',    SubntwkTyp('rate_center'), SubntwkId('NPA-418'), TopologyStatus('in_service'));
exec AddSubntwk('RC-604-VANCOUVER', SubntwkTyp('rate_center'), SubntwkId('NPA-604'), TopologyStatus('in_service'));
exec AddSubntwk('RC-647-Atlanta',   SubntwkTyp('rate_center'), SubntwkId('NPA-647'), TopologyStatus('in_service'));
exec AddSubntwk('RC-804-RICHMOND',  SubntwkTyp('rate_center'), SubntwkId('NPA-804'), TopologyStatus('in_service'));
exec AddSubntwk('RC-127-PARIS',     SubntwkTyp('rate_center'), SubntwkId('NPA-127'), TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('RC-404-Atlanta'),   'name', 'ATLANTA');
exec AddSubntwkParm(SubntwkId('RC-404-Atlanta'),   'extended_rate_center_nm','Atlanta, GA');
exec AddSubntwkParm(SubntwkId('RC-418-OTTAWA'),    'name', 'OTTAWA');
exec AddSubntwkParm(SubntwkId('RC-418-OTTAWA'),    'extended_rate_center_nm','Ottawa, ON');
exec AddSubntwkParm(SubntwkId('RC-647-Atlanta'),   'name', 'ATLANTA')
exec AddSubntwkParm(SubntwkId('RC-647-Atlanta'),   'extended_rate_center_nm','Atlanta, GA');
exec AddSubntwkParm(SubntwkId('RC-604-VANCOUVER'), 'name', 'VANCOUVER');
exec AddSubntwkParm(SubntwkId('RC-604-VANCOUVER'), 'extended_rate_center_nm','Vancouver, BC');
exec AddSubntwkParm(SubntwkId('RC-127-PARIS'),     'name', 'PARIS');
exec AddSubntwkParm(SubntwkId('RC-127-PARIS'),     'extended_rate_center_nm','Paris');

-------------
---- NXX ----
-------------
exec AddSubntwk('NXX-404-Atlanta-943', SubntwkTyp('nxx'), SubntwkId('RC-404-Atlanta'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('NXX-404-Atlanta-943'), 'name', 'NXX-404-Atlanta-943');
exec AddSubntwkParm(SubntwkId('NXX-404-Atlanta-943'), 'npa','404');
exec AddSubntwkParm(SubntwkId('NXX-404-Atlanta-943'), 'nxx','943');

---------------------
---- Line Option ----
---------------------
exec AddSubntwk('LO-NPA-404', SubntwkTyp('line_option'), SubntwkId('NPA-404'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'name', 'LO-NPA-404');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'suspend_ld_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'suspend_intl_line_option','3');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'suspend_900_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'forced_block','1');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'courtesy_block', '1');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'timezone','EDT');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'ncos_normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'ncos_suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'ncos_suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NPA-404'), 'ncos_suspend_intl_line_option','3');

exec AddSubntwk('LO-NPA-804', SubntwkTyp('line_option'), SubntwkId('NPA-804'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'name', 'LO-NPA-804');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'suspend_ld_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'suspend_intl_line_option','3');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'suspend_900_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'forced_block','1');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'courtesy_block', '1');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'timezone','EDT');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'ncos_normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'ncos_suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'ncos_suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NPA-804'), 'ncos_suspend_intl_line_option','3');

-------------------------------------
---- Rate Center and Line Option ----
-------------------------------------
exec AddSubntwk('LO-RC-404-ATLANTA',   SubntwkTyp('line_option'), SubntwkId('RC-404-Atlanta'),  TopologyStatus('in_service'));
exec AddSubntwk('LO-RC-418-OTTAWA',    SubntwkTyp('line_option'), SubntwkId('RC-418-OTTAWA'),   TopologyStatus('in_service'));
exec AddSubntwk('LO-RC-604-VANCOUVER', SubntwkTyp('line_option'), SubntwkId('RC-604-VANCOUVER'),TopologyStatus('in_service'));
exec AddSubntwk('LO-RC-647-ATLANTA',   SubntwkTyp('line_option'), SubntwkId('RC-647-Atlanta'),  TopologyStatus('in_service'));
exec AddSubntwk('LO-RC-804-RICHMOND',  SubntwkTyp('line_option'), SubntwkId('RC-804-RICHMOND'), TopologyStatus('in_service'));
exec AddSubntwk('LO-RC-127-PARIS',     SubntwkTyp('line_option'), SubntwkId('RC-127-PARIS'),    TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'name', 'LO-RC-404-ATLANTA');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'suspend_ld_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'suspend_intl_line_option','3');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'suspend_900_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'forced_block','1');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'courtesy_block', '1');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'timezone','EDT');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'ncos_normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'ncos_suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'ncos_suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-404-ATLANTA'), 'ncos_suspend_intl_line_option','3');

exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'name', 'LO-RC-418-OTTAWA');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'vm_pilot_tn','2002016245');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'suspend_ld_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'suspend_intl_line_option','3');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'suspend_900_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'forced_block','1');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'courtesy_block', '1');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'timezone','LOC');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'ncos_normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'ncos_suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'ncos_suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-418-OTTAWA'), 'ncos_suspend_intl_line_option','3');

exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'name', 'LO-RC-604-VANCOUVER');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'normal_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'suspend_ld_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'suspend_intl_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'suspend_all_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'suspend_900_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'forced_block','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'courtesy_block', '12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'timezone','LOC');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'ncos_normal_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'ncos_suspend_all_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'ncos_suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-604-VANCOUVER'), 'ncos_suspend_intl_line_option','12');

exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'name', 'LO-RC-647-ATLANTA');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'normal_line_option','10');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'suspend_ld_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'suspend_intl_line_option','13');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'suspend_all_line_option','11');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'suspend_900_line_option','10');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'forced_block','11');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'courtesy_block', '11');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'timezone','EDT');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'ncos_normal_line_option','10');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'ncos_suspend_all_line_option','11');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'ncos_suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-647-ATLANTA'), 'ncos_suspend_intl_line_option','13');

exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'name', 'LO-RC-804-RICHMOND');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'normal_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'suspend_ld_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'suspend_intl_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'suspend_all_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'suspend_900_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'forced_block','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'courtesy_block', '12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'timezone','LOC');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'ncos_normal_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'ncos_suspend_all_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'ncos_suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-804-RICHMOND'), 'ncos_suspend_intl_line_option','12');

exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'name', 'LO-RC-127-PARIS');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'normal_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'suspend_ld_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'suspend_intl_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'suspend_all_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'suspend_900_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'forced_block','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'courtesy_block', '12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'timezone','LOC');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'ncos_normal_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'ncos_suspend_all_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'ncos_suspend_toll_line_option','12');
exec AddSubntwkParm(SubntwkId('LO-RC-127-PARIS'), 'ncos_suspend_intl_line_option','12');

-----------------------------
---- NXX and Line Option ----
-----------------------------
exec AddSubntwk('LO-NXX-404-ATLANTA-943', SubntwkTyp('line_option'), SubntwkId('NXX-404-Atlanta-943'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'name', 'LO-NXX-404-ATLANTA-943');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'criteria','true');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'suspend_ld_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'suspend_intl_line_option','3');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'suspend_900_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'forced_block','1');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'courtesy_block', '1');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'vm_pilot_tn','2002006245');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'lataname','lata1');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'timezone','EDT')
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'ncos_normal_line_option','0');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'ncos_suspend_all_line_option','1');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'ncos_suspend_toll_line_option','2');
exec AddSubntwkParm(SubntwkId('LO-NXX-404-ATLANTA-943'), 'ncos_suspend_intl_line_option','3');

spool off
