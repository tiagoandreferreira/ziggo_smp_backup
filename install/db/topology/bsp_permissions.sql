spool consolidated_UFO_AM_Permissions.log

exec am_add_secure_obj('samp.web.svcparm.mobile_service_identity.gsa_value', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.mobile_service_identity.gsa_value', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.mobile_service_identity.gsa_value', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.mobile_service_identity.blacklist_tag', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.mobile_service_identity.blacklist_tag', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.mobile_service_identity.blacklist_tag', 'y', 'csr_admin', 'edit');

commit;

spool off

exit
