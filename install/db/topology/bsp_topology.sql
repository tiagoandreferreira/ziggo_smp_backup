-----------------------------
---- Adding TechPlatform ----
-----------------------------
EXEC AddTechPlatform('ericssonemasapc_01', TechPlatformTyp('ericssonemasapc'));
EXEC AddTechPlatform('mvnonotification_01', TechPlatformTyp('mvnonotification'));
--------------------------------------------
---- Adding TechPlatform Management Mode----
--------------------------------------------
EXEC AddTechPlatformMgmtMode(TechPlatform('ericssonemasapc_01'), MgmtMode('provision'));
EXEC AddTechPlatformMgmtMode(TechPlatform('mvnonotification_01'), MgmtMode('provision'));
-------------------------------------------------------
--- Adding links between subntwk and Tech Platform ----
-------------------------------------------------------
 INSERT
   INTO Sbnt_Tech_Platform
  (
    subntwk_id      ,
    tech_platform_id,
    mgmt_mode_id    ,
    created_dtm     ,
    created_by      ,
    modified_dtm    ,
    modified_by
  )
  VALUES
  (
    SubntwkId('NL_home_network')      ,
    TechPlatform('ericssonemasapc_01'),
    MgmtMode('provision')             ,
    SYSDATE                           ,
    'INIT'                            ,
    NULL                              ,
    NULL
  );
COMMIT;

INSERT
   INTO Sbnt_Tech_Platform
  (
    subntwk_id      ,
    tech_platform_id,
    mgmt_mode_id    ,
    created_dtm     ,
    created_by      ,
    modified_dtm    ,
    modified_by
  )
  VALUES
  (
    SubntwkId('NL_home_network')      ,
    TechPlatform('mvnonotification_01'),
    MgmtMode('provision')             ,
    SYSDATE                           ,
    'INIT'                            ,
    NULL                              ,
    NULL
  );
-------------------------------------------------------
--- Adding subntework parmeter for gsa ----
--------------------------------------------------------
EXEC AddSubntwkParm(SubntwkId('NL_home_network'),'gsa','');

COMMIT;