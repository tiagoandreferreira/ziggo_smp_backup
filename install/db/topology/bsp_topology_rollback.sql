--============================================================================
-- $Id: bsp_topology_rollback.sql,v 1.1 2014/09/03 Manikanta Exp $ 
--  REVISION HISTORY
--============================================================================

spool bsp_topology_rollback.log

DELETE
		FROM Sbnt_Tech_Platform
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'ericssonemasapc_01'
		  OR TECHN_PLATFORM_NM = 'mvnonotification_01'
		  );
		  
		  
DELETE
		FROM tech_platform_mgmt_mode
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'ericssonemasapc_01'
		  OR TECHN_PLATFORM_NM = 'mvnonotification_01'
		  );
		  
DELETE
		FROM TECH_PLATFORM
		WHERE TECHN_PLATFORM_NM = 'ericssonemasapc_01'
		OR TECHN_PLATFORM_NM = 'mvnonotification_01';
		
DELETE
		FROM TECH_PLATFORM_TYP
		WHERE TECH_PLATFORM_TYP_NM = 'ericssonemasapc'
		OR TECH_PLATFORM_TYP_NM = 'mvnonotification';
		
COMMIT;

spool off


		
		