spool clean_routing_topology.log
-- ============================================================================
--  Id: clean_routing_topology.sql,v 1.0 2013/12/07 07:49:28 Amit gupta Exp $
-- 	Subnetwork created for clean routing cr
	
	------------------------------------------------------------------------------------------------------------------------------
	--						   ENTRIES FOR CROUTE																	--
	------------------------------------------------------------------------------------------------------------------------------
	
	
	-- entries for adding SubNetwork
		
	exec AddSubntwk('CROUTE', SubntwkTyp('croute'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('box_1', SubntwkTyp('call_server'), SubntwkId('Ziggo_Network'), TopologyStatus('in_service'));
		
	-- Insert into sbnttechplatform
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CROUTE'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('box_1'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	--Adding Links
	
	exec AddLink('CROUTE_DEFAULT_NAD', LinkTyp('logical'), SubntwkId('CROUTE'), SubntwkId('default_NAD'));
	
	exec AddLink('NAD_CS_default_box_1', LinkTyp('logical'), SubntwkId('box_1'), SubntwkId('default_NAD'));
	
	--Adding Subnetwork Parameters
	
	exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'profile_key', '1142');
	exec AddSubntwkParm(SubntwkId('box_1'), 'vendor',      'unknown');
	exec AddSubntwkParm(SubntwkId('box_1'), 'in_service',      'n');
	exec AddSubntwkParm(SubntwkId('box_1'), 'num_of_lines_provisioned',      '70');

	update subntwk_parm set val='1001' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS01N1MND.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS01N2MND.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1002' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS02N1MND.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS02N2MND.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1011' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS01N1GV.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS01N2GV.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1012' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS02N1GV.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS02N2GV.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1021' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS05N1GN.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS05N2GN.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1031' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS05N1TB.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS05N2TB.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1032' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS06N1TB.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS06N2TB.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1033' where parm_id = (select parm_id from parm where parm_nm ='profile_key') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'CMS07N1TB.TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'CMS07N2TB.TELEFONIE.ZIGGO.LOCAL');

	update subntwk_parm set val='1' where parm_id = (select parm_id from parm where parm_nm ='announcement') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%' or subntwk_nm like '%TELEFONIE.ZIGGO.LOCAL');

        update subntwk_parm set val='9' where parm_id = (select parm_id from parm where parm_nm ='grp_id') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'SP_AS%');
	
	update subntwk set topology_status_id = 2 where subntwk_typ_id = (select subntwk_typ_id from ref_subntwk_typ where subntwk_typ_nm = 'call_server') and (subntwk_nm like 'box_1');

	commit;

spool off

exit
