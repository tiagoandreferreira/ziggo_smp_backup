spool clean_routing_topology_rollbk.log
-- ============================================================================
--  Id: clean_routing_topology_rollbk.sql,v 1.0 2013/12/07 07:49:28 Amit gupta Exp $
-- 	Rollback script to revert Subnetwork created for clean routing cr	
	
	------------------------------------------------------------------------------------------------------------------------------
	--						   DELETING ENTRIES FOR CROUTE																	--
	------------------------------------------------------------------------------------------------------------------------------
	
	
	-- Deleting Links
	
	delete from LINK where FROM_SUBNTWK_ID = (select SUBNTWK_ID from SUBNTWK where SUBNTWK_NM = 'CROUTE');
	
	-- Deleting entry for subntwk and techplatform mapping
	
	delete from SBNT_TECH_PLATFORM where SUBNTWK_ID = (select SUBNTWK_ID from SUBNTWK where SUBNTWK_NM = 'CROUTE');
	
	-- Deleting entriy for adding SubNetwork
		
	delete from SUBNTWK where SUBNTWK_NM = 'CROUTE';
	
	commit;

spool off

exit