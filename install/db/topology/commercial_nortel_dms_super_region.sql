--============================================================================
--    $Id: commercial_nortel_dms_super_region.sql,v 1.1 2011/09/28 18:25:54 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  Commercial DMS Super Region
--============================================================================
spool commercial_nortel_dms_super_region.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Commercial Nortel DMS Super Region ----');
-----------------
---- Atlanta ----
-----------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Atlanta-SR9', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('ATL9-1', SubntwkTyp('region'), SubntwkId('Atlanta-SR9'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ATL9-1'), 'clec_supplied_e911', 'Y');

-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Atlanta9', SubntwkTyp('network_access_device'), SubntwkId('Atlanta-SR9'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_ATL9-1', LinkTyp('logical'), SubntwkId('Atlanta9'), SubntwkId('ATL9-1'));

-----------------------------
---- Parameters for NADs ----
-----------------------------
exec AddSubntwkParm(SubntwkId('Atlanta9'), 'desc',                'Atlanta9');
exec AddSubntwkParm(SubntwkId('Atlanta9'), 'dpm_location',        'Atlanta9');

spool off

