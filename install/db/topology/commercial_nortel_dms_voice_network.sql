--============================================================================
--    $Id: commercial_nortel_dms_voice_network.sql,v 1.3 2012/03/14 19:26:13 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  Commercial Nortel DMS Voice Network
--============================================================================
spool commercial_nortel_dms_voice_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Commercial Nortel DMS Voice Network ----');

---------------------
---- Call Server ----
---------------------
exec AddSubntwk('CMS_ND_COM1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-------------------------------------
---- Gateway Controller Clusters ----
-------------------------------------
exec AddSubntwk('GWCC-ND1-CL3', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_ND_COM1'), TopologyStatus('in_service'));

-----------------------------
---- Gateway Controllers ----
-----------------------------
exec AddSubntwk('GWC-ND1-CL3-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_ND_COM1'), TopologyStatus('in_service'));

-----------------------------------
---- Links beween GWCC and GWC ----
-----------------------------------
exec AddLink('GWC1_GWCC-ND1-CL3', LinkTyp('logical'), SubntwkId('GWC-ND1-CL3-1'), SubntwkId('GWCC-ND1-CL3'));

--------------------------------------------------
---- Parameters for Call Server - CMS_ND_COM1 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'auto_attn_route',          '1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_cust_group',       'RESGRP');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_inter_cic',        'nilc');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_inter_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_intl_cic',         'nilc');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_intl_cic_choice',  'Y');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_intra_cic' ,       'nilc');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_intra_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_lata_name',        'LATA1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_lcc',              'IBN');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mgsitename',       '');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_announcement', 'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_car',          'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_cmwiring',     'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_cmwistd',      'Y');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_crrcfw',       'ALL');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_crx',          'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_notice',       'MWL');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_mwt_status',       'ACT');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_name_netname',     'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_ncos_normal',      '0');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_ncos_suspend_all', '3');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_ncos_suspend_intl','1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_ncos_suspend_toll','2');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sdn_sdn_dny',      'NODNY');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sdn_sdn_opt',      'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sonumber',         '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_suppress_netname', 'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_ac_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_ar_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfbl_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfbl_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfda_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfda_ringctrl', 'FIXRING');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfda_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfra_firstuse', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfw_cfwtype',   'C');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfw_fdn',       '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfw_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfw_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cfw_state',     'I');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cnamd_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cnd_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cndb_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cot_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_cxfertype', 'CTALL');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_cxrrcl',    'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_method',    'STD');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_orginter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_orgintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_rcltim',    '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_trminter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_cxr_trmintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_ddn_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_dnd_dndgrp',    '1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_drcw_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_lcdr_bill_records',   'TRUE');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_sacb_sacbsus',  'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_sca_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_sca_dndonly',   'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_scf_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_scf_forwardto_dn',   '$');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_scf_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_scf_ringrem',   'NORING');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_scf_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_scrj_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_sw_wml_timeout',   '20');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_wml_custmod',      'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_wml_ftcode',       'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'vendor',                   'nortel');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'version',                  '1300');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'pool',                     'N');
exec AddSubntwkParm(SubntwkId('CMS_ND_COM1'), 'default_scr_enable_lcb',   '');

----------------------------
---- Parameters for GWC ----
----------------------------
exec AddSubntwkParm(SubntwkId('GWC-ND1-CL3-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-ND1-CL3-1'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-ND1-CL3-1'), 'gwc_name',         'GWC-82');
exec AddSubntwkParm(SubntwkId('GWC-ND1-CL3-1'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-ND1-CL3-1'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-ND1-CL3-1'), 'skip_load_balance','y');

--------------------
---- Voice Mail ----
--------------------
exec AddSubntwk('VM-ND-COM1', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-ND-COM1'), 'org_id_business', '112');

--------------------------------------------------
---- Links between Voice Mail and Call Server ----
--------------------------------------------------
exec AddLink('VM-ND-COM1_TO_CMS', LinkTyp('logical'), SubntwkId('VM-ND-COM1'), SubntwkId('CMS_ND_COM1'));

---------------------------------------------
---- Links between GWCC and Super Region ----
---------------------------------------------
------------------
---- Atlanta -----
------------------
exec AddLink('GWCC_TO_SRGN_Atlanta-SR9', LinkTyp('logical'), SubntwkId('GWCC-ND1-CL3'), SubntwkId('Atlanta-SR9'));

------------------------------------------------
---- The links between NAM and Super Region ----
------------------------------------------------
exec AddLink('NAM_NAD_ATLANTA-SR9', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Atlanta-SR9'));

------------------------------
---- Technology Platforms ----
------------------------------
---------------------
---- Call Server ----
---------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_ND_COM1'), TechPlatform('nortel_cs2000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------------
---- Voice Mail ----
--------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-ND-COM1'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

spool off

