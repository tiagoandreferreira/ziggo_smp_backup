--============================================================================
-- $Id: consolidated_ufo_topology_rollback.sql,v 1.1 2014/04/02 Jalay Exp $ 
--  REVISION HISTORY
--============================================================================

spool consolidated_ufo_topology_rollback.log

		-- Deleting subnetwork parms
		DELETE
		FROM SUBNTWK_PARM
		WHERE SUBNTWK_ID IN
		  (SELECT SUBNTWK_ID
		  FROM SUBNTWK
		  WHERE subntwk_nm = 'VMSIRES2'
		  OR subntwk_nm    = 'CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'
		  OR subntwk_nm    = 'SP_AS_1_10'
		  OR subntwk_nm    = 'SP_AS_2_10'
		  OR subntwk_nm    = 'SP_AS_3_10'
		  OR subntwk_nm    = 'CROUTE'
		  OR subntwk_nm    = 'box_1'
		  );
		-- Deleting mapping between subnetwork and tech platforms
		DELETE
		FROM Sbnt_Tech_Platform
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'comsys'
		  OR TECHN_PLATFORM_NM    = 'ciscobac40'
		  OR TECHN_PLATFORM_NM    = 'ziggonis_01'
		  OR TECHN_PLATFORM_NM    = 'broadsoft_01'
		  OR TECHN_PLATFORM_NM    = 'notification_01'
		  );
		  
		DELETE
		FROM Sbnt_Tech_Platform
		WHERE SUBNTWK_ID IN
		  (SELECT SUBNTWK_ID
		  FROM SUBNTWK
		  WHERE SUBNTWK_NM = 'SP_AS_1_10'
		  OR SUBNTWK_NM    = 'SP_AS_2_10'
		  OR SUBNTWK_NM    = 'SP_AS_3_10'
		  OR SUBNTWK_NM    = 'box_1'
		  OR SUBNTWK_NM    = 'CROUTE'
		  );
		  
		-- Deleting links
		DELETE
		FROM LINK
		WHERE LINK_NM LIKE 'VM-SI-RES2N_TO_CMS'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS05N1GN'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS05N2GN'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS01N1MND'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS01N2MND'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS02N1MND'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS02N2MND'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS01N1GV'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS01N2GV'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS02N1GV'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS02N2GV'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS05N1TB'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS05N2TB'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS06N1TB'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS06N2TB'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS07N1TB'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_CMS07N2TB'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_SP_AS_1_10'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_SP_AS_2_10'
		OR LINK_NM LIKE 'VM-SI-RES2N_TO_SP_AS_3_10'
		OR LINK_NM LIKE 'VM-SI-RES3-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-SI-RES3-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-SI-RES3-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-SI-RES2-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-SI-RES2-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-SI-RES2-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-SI-RES1-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-SI-RES1-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-SI-RES1-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-NT-RES3-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-NT-RES3-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-NT-RES3-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-NT-RES2-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-NT-RES2-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-NT-RES2-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-NT-RES1-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-NT-RES1-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-NT-RES1-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-NT-INC1-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-NT-INC1-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-NT-INC1-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-ND-COM1-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-ND-COM1-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-ND-COM1-SP_AS_3_10'
		OR LINK_NM LIKE 'VM-ND-RES3-SP_AS_1_10'
		OR LINK_NM LIKE 'VM-ND-RES3-SP_AS_2_10'
		OR LINK_NM LIKE 'VM-ND-RES3-SP_AS_3_10'
		OR LINK_NM LIKE 'NAM_NAD_VAN13'
		OR LINK_NM LIKE 'SP_AS_1_10_default-SR'
		OR LINK_NM LIKE 'SP_AS_2_10_default-SR'
		OR LINK_NM LIKE 'SP_AS_3_10_default-SR'
		OR LINK_NM LIKE 'NAD_CS_default_CMS05N1GN'
		OR LINK_NM LIKE 'NAD_CS_default_CMS05N2GN'
		OR LINK_NM LIKE 'NAD_CS_default_CMS01N1MND'
		OR LINK_NM LIKE 'NAD_CS_default_CMS01N2MND'
		OR LINK_NM LIKE 'NAD_CS_default_CMS02N1MND'
		OR LINK_NM LIKE 'NAD_CS_default_CMS02N2MND'
		OR LINK_NM LIKE 'NAD_CS_default_CMS01N1GV'
		OR LINK_NM LIKE 'NAD_CS_default_CMS01N2GV'
		OR LINK_NM LIKE 'NAD_CS_default_CMS02N1GV'
		OR LINK_NM LIKE 'NAD_CS_default_CMS02N2GV'
		OR LINK_NM LIKE 'NAD_CS_default_CMS05N1TB'
		OR LINK_NM LIKE 'NAD_CS_default_CMS05N2TB'
		OR LINK_NM LIKE 'NAD_CS_default_CMS06N1TB'
		OR LINK_NM LIKE 'NAD_CS_default_CMS06N2TB'
		OR LINK_NM LIKE 'NAD_CS_default_CMS07N1TB'
		OR LINK_NM LIKE 'NAD_CS_default_CMS07N2TB'
		OR LINK_NM LIKE 'CROUTE_DEFAULT_NAD'
		OR LINK_NM LIKE 'NAD_CS_default_box_1';
		-- Deleting subnetwork
		DELETE
		FROM subntwk
		WHERE subntwk_nm = 'VMSIRES2'
		OR subntwk_nm    = 'SP_AS_1_10'
		OR subntwk_nm    = 'SP_AS_2_10'
		OR subntwk_nm    = 'SP_AS_3_10'
		OR subntwk_nm    = 'CROUTE'
		OR subntwk_nm    = 'box_1';
		-- Deleting techPlatform mgt mode
		DELETE
		FROM tech_platform_mgmt_mode
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'comsys'
		  OR TECHN_PLATFORM_NM    = 'ciscobac40'
		  OR TECHN_PLATFORM_NM    = 'ziggonis_01'
		  OR TECHN_PLATFORM_NM    = 'broadsoft_01'
		  OR TECHN_PLATFORM_NM    = 'notification_01'
		  );
		--Deleting techlatform parms
		DELETE
		FROM TECH_PLATFORM_PARM
		WHERE tech_platform_id IN
		  (SELECT tech_platform_id
		  FROM Tech_Platform
		  WHERE TECHN_PLATFORM_NM = 'comsys'
		  OR TECHN_PLATFORM_NM    = 'ciscobac40'
		  OR TECHN_PLATFORM_NM    = 'ziggonis_01'
		  OR TECHN_PLATFORM_NM    = 'broadsoft_01'
		  OR TECHN_PLATFORM_NM    = 'notification_01'
		  );
		-- Deleting techPlatforms
		DELETE
		FROM TECH_PLATFORM
		WHERE TECHN_PLATFORM_NM = 'comsys'
		OR TECHN_PLATFORM_NM    = 'ciscobac40'
		OR TECHN_PLATFORM_NM    = 'ziggonis_01'
		OR TECHN_PLATFORM_NM    = 'broadsoft_01'
		OR TECHN_PLATFORM_NM    = 'notification_01';
		-- Deleting techPlatformtypes
		DELETE
		FROM TECH_PLATFORM_TYP
		WHERE TECH_PLATFORM_TYP_NM = 'comsys'
		OR TECH_PLATFORM_TYP_NM    = 'ciscobac40'
		OR TECH_PLATFORM_TYP_NM    = 'ziggonis'
		OR TECH_PLATFORM_TYP_NM    = 'broadsoft'
		OR TECH_PLATFORM_TYP_NM    = 'notification';
		-- Droping tables
		DROP TABLE service_provider_4_digit;
		DROP TABLE service_provider_3_digit;
		DROP TABLE CUST_DIN_QOS_FLAVOR;
		DROP TABLE CUST_ALLOWED_MODEM_PROFILE;
		DROP TABLE CUST_DIN_QOS;
		DROP TABLE CUST_BAC_MODEM_MFG;
		DROP TABLE CUST_SEQUENCE;
		-- Updating Subnetwork name
		UPDATE SUBNTWK
		SET subntwk_nm   = 'NAM_DPM'
		WHERE subntwk_nm = 'NAMDPM';
		UPDATE SUBNTWK SET subntwk_nm = 'NAM_BAC' WHERE subntwk_nm = 'NAMBAC';
		UPDATE SUBNTWK SET subntwk_nm = 'NAM_ALU' WHERE subntwk_nm = 'NAMALU';
		COMMIT;
		
		update subntwk set topology_status_id = 2 where subntwk_typ_id = (select subntwk_typ_id from ref_subntwk_typ where subntwk_typ_nm = 'call_server');

spool off
exit