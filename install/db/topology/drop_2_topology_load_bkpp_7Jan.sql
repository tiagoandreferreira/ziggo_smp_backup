--============================================================================
--    $Id: drop_2_topology_load.sql,v 1.6 2012/02/27 13:40:27 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================

spool drop_2_topology_load.log

set escape on
set serveroutput on
--commented cpe entry as this entry is present in root_network.sql file
--exec AddRefNtwkRole('cpe');
--exec AddRefNtwkRole('network_entry_point');
exec AddRefNtwkRole('service_distribution_point');
exec AddRefNtwkRole('vmail');

update subntwk
   set subntwk_nm = 'NL_home_network'
 where subntwk_nm = 'mobile_dp';

update subntwk_parm
   set val = '1'
 where subntwk_id in (select subntwk_id from subntwk where subntwk_nm = 'NL_home_network')
   and parm_id in (select parm_id from parm where parm_nm = 'default_telephony_profileid');

exec AddTechPlatform('nsnnetmanager', TechPlatformTyp('nsnnetmanager'));
exec AddTechPlatformParm(TechPlatformId('nsnnetmanager'), 'auto_correct_on_add_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('nsnnetmanager'), 'auto_correct_on_update_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('nsnnetmanager'), 'auto_correct_on_delete_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('nsnnetmanager'), 'template_error_handling_behavior', 'manual');
exec AddTechPlatformParm(TechPlatformId('nsnnetmanager'), 'np_entity', '2');
exec AddTechPlatformParm(TechPlatformId('nsnnetmanager'), 'async_timeout', '240 sec');
exec AddTechPlatformMgmtMode(TechPlatform('nsnnetmanager'), MgmtMode('provision'));

exec AddTechPlatform('ericssonemahlr', TechPlatformTyp('ericssonemahlr'));
exec AddTechPlatformParm(TechPlatformId('ericssonemahlr'), 'auto_correct_on_add_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ericssonemahlr'), 'auto_correct_on_update_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ericssonemahlr'), 'auto_correct_on_delete_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ericssonemahlr'), 'template_error_handling_behavior', 'manual');
exec AddTechPlatformMgmtMode(TechPlatform('ericssonemahlr'), MgmtMode('provision'));

exec AddTechPlatform('ericssonemamio', TechPlatformTyp('ericssonemamio'));
exec AddTechPlatformParm(TechPlatformId('ericssonemamio'), 'auto_correct_on_add_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ericssonemamio'), 'auto_correct_on_update_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ericssonemamio'), 'auto_correct_on_delete_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ericssonemamio'), 'template_error_handling_behavior', 'manual');
exec AddTechPlatformMgmtMode(TechPlatform('ericssonemamio'), MgmtMode('provision'));


Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('NL_home_network'), TechPlatform('nsnnetmanager'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('NL_home_network'), TechPlatform('ericssonemahlr'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('NL_home_network'), TechPlatform('ericssonemamio'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

commit;

spool off

