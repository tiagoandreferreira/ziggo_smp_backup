----------------------
---- Adding TechPlatform ---- 
----------------------


exec AddTechPlatform('pisys_01', TechPlatformTyp('irdeto'));
exec AddTechPlatform('prodis_01', TechPlatformTyp('seachangeeventis'));
exec AddTechPlatform('itv_01', TechPlatformTyp('itv'));
exec AddTechPlatform('dtvnotification_01', TechPlatformTyp('dtvnotification'));


exec AddTechPlatformMgmtMode(TechPlatform('pisys_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('prodis_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('itv_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('dtvnotification_01'), MgmtMode('provision'));

----------------------
----Adding Super Region ---- 
----------------------

exec AddSubntwk('Netherlands', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

------------------
----Adding Region ---- 
------------------

exec AddSubntwk('NL_1', SubntwkTyp('region'), SubntwkId('Netherlands'), TopologyStatus('in_service'));


-----------------------------------------------------
---- Adding link between Super Region and Region ---- 
------------------------------------------------------

exec AddLink('NL_1_TO_Netherlands', LinkTyp('logical'), SubntwkId('NL_1'), SubntwkId('Netherlands'));



--------------
--- Adding subntwk for Pisys ----
--------------
exec AddSubntwk('AtHome', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Multikabel', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Casema', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

--------------
--- Adding subntwk for Prodis ----
--------------
exec AddSubntwk('ZiggoVod', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

--------------
--- Adding links between subntwk and super region ----
--------------

exec AddLink('AtHome_TO_Netherlands', LinkTyp('logical'), SubntwkId('AtHome'), SubntwkId('Netherlands'));
exec AddLink('Multikabel_TO_Netherlands', LinkTyp('logical'), SubntwkId('Multikabel'), SubntwkId('Netherlands'));
exec AddLink('Casema_TO_Netherlands', LinkTyp('logical'), SubntwkId('Casema'), SubntwkId('Netherlands'));

exec AddLink('ZiggoVod_TO_Netherlands', LinkTyp('logical'), SubntwkId('ZiggoVod'), SubntwkId('Netherlands'));

--------------
--- Adding links between subntwk and Tech Platform ----
--------------

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('AtHome'), TechPlatform('pisys_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Multikabel'), TechPlatform('pisys_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Casema'), TechPlatform('pisys_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('AtHome'), TechPlatform('dtvnotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Multikabel'), TechPlatform('dtvnotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Casema'), TechPlatform('dtvnotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('ZiggoVod'), TechPlatform('prodis_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('ZiggoVod'), TechPlatform('dtvnotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


--------------
--- Adding subntwk Parms for Pisys ----
--------------

exec AddSubntwkParm(SubntwkId('AtHome'), 'name', 'AtHome');
exec AddSubntwkParm(SubntwkId('AtHome'), 'manufacturer', 'StandardCAS');
exec AddSubntwkParm(SubntwkId('AtHome'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('AtHome'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('AtHome'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('AtHome'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('AtHome'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('AtHome'), 'vod_authorization_handle', '0');
exec AddSubntwkParm(SubntwkId('AtHome'), 'nationality', 'NLD');
exec AddSubntwkParm(SubntwkId('AtHome'), 'operatortag', 'ESSENT');
exec AddSubntwkParm(SubntwkId('AtHome'), 'regiontag', 'ZH');


exec AddSubntwkParm(SubntwkId('Multikabel'), 'name', 'Multikabel');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'manufacturer', 'StandardCAS');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'vod_authorization_handle', '0');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'nationality', 'NLD');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'operatortag', 'MULTIK');
exec AddSubntwkParm(SubntwkId('Multikabel'), 'regiontag', 'NH');


exec AddSubntwkParm(SubntwkId('Casema'), 'name', 'Casema');
exec AddSubntwkParm(SubntwkId('Casema'), 'manufacturer', 'StandardCAS');
exec AddSubntwkParm(SubntwkId('Casema'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Casema'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Casema'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('Casema'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Casema'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Casema'), 'vod_authorization_handle', '0');
exec AddSubntwkParm(SubntwkId('Casema'), 'nationality', 'NLD');
exec AddSubntwkParm(SubntwkId('Casema'), 'operatortag', 'CASEMA');
exec AddSubntwkParm(SubntwkId('Casema'), 'regiontag', 'df');



--------------
--- Adding subntwk Parms for Prodis ----
--------------

exec AddSubntwkParm(SubntwkId('ZiggoVod'), 'name', 'ZiggoVod');
exec AddSubntwkParm(SubntwkId('ZiggoVod'), 'manufacturer', 'StandardCAS');
exec AddSubntwkParm(SubntwkId('ZiggoVod'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('ZiggoVod'), 'version', '2.1');

commit;




