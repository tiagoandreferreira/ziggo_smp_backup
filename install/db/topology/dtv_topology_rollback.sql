	--============================================================================
-- $Id: dtv_topology_rollback,v 1.1 2014/03/20 Jalay Exp $ 
--  REVISION HISTORY

--============================================================================
		spool dtv_topology_rollback.log
		set escape on
		set serveroutput on
		
	delete from SUBNTWK_PARM where SUBNTWK_ID IN (select SUBNTWK_ID from SUBNTWK where subntwk_nm = 'AtHome' or subntwk_nm = 'Multikabel' or subntwk_nm = 'Casema' or subntwk_nm = 'ZiggoVod');

	delete from Sbnt_Tech_Platform where SUBNTWK_ID IN (select SUBNTWK_ID from SUBNTWK where subntwk_nm = 'AtHome' or subntwk_nm = 'Multikabel' or subntwk_nm = 'Casema' or subntwk_nm = 'ZiggoVod');
	
	delete from LINK where LINK_NM like 'ZiggoVod_TO_Netherlands' or LINK_NM like 'Casema_TO_Netherlands' or LINK_NM like 'Multikabel_TO_Netherlands' or LINK_NM like 'AtHome_TO_Netherlands' or LINK_NM like 'NL_1_TO_Netherlands';
	
	delete from subntwk where SUBNTWK_NM like 'ZiggoVod' or SUBNTWK_NM like 'Casema' or SUBNTWK_NM like 'Multikabel' or SUBNTWK_NM like 'AtHome' or SUBNTWK_NM like 'NL_1' or SUBNTWK_NM like 'Netherlands';
	
	delete from tech_platform_mgmt_mode where TECH_PLATFORM_ID in (select TECH_PLATFORM_ID from TECH_PLATFORM where TECHN_PLATFORM_NM = 'pisys_01' or TECHN_PLATFORM_NM = 'prodis_01' or TECHN_PLATFORM_NM = 'itv_01' or TECHN_PLATFORM_NM = 'dtvnotification_01');
	
	delete from TECH_PLATFORM where TECHN_PLATFORM_NM = 'pisys_01' or TECHN_PLATFORM_NM = 'prodis_01' or TECHN_PLATFORM_NM = 'itv_01' or TECHN_PLATFORM_NM = 'dtvnotification_01';

	delete from TECH_PLATFORM_TYP where TECH_PLATFORM_TYP_NM = 'dtvnotification' or TECH_PLATFORM_TYP_NM = 'itv' or TECH_PLATFORM_TYP_NM = 'seachangeeventis' or TECH_PLATFORM_TYP_NM = 'irdeto';		
		
		
		commit;
		spool off
		exit;