--============================================================================
-- $Id: gsa_value_update.sql,v 1.1 2014/09/26 Manikanta Exp $ 
--  REVISION HISTORY
--============================================================================

update subntwk_parm set val='31686506591' where parm_id = (select parm_id from parm where parm_nm ='gsa') and subntwk_id in(select subntwk_id from subntwk where subntwk_nm like 'NL_home_network');

COMMIT;

