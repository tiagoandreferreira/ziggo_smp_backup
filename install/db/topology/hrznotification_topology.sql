--============================================================================
-- $Id: hrznotification_topology.sql,v 1.1 2014/11/17 divyap Exp $ 
--									 
--  REVISION HISTORY

--============================================================================
spool hrznotification_topology.log
-----------------------------
---- Adding TechPlatform ----
-----------------------------
EXEC AddTechPlatform('hrznotification_01', TechPlatformTyp('hrznotification'));
--------------------------------------------
---- Adding TechPlatform Management Mode----
--------------------------------------------
EXEC AddTechPlatformMgmtMode(TechPlatform('hrznotification_01'), MgmtMode('provision'));
-------------------------------------------------------
--- Adding links between subntwk and Tech Platform ----
-------------------------------------------------------

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('NAMDPM'), TechPlatform('hrznotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

COMMIT;
spool off
exit