--============================================================================
-- $Id: hrznotification_topology_rollback.sql,v 1.1 2014/11/17 Divya Exp $ 
--  REVISION HISTORY
--============================================================================

spool hrznotification_topology_rollback.log

DELETE
		FROM Sbnt_Tech_Platform
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'hrznotification_01'
		  );
		  
		  
DELETE
		FROM tech_platform_mgmt_mode
		WHERE TECH_PLATFORM_ID IN
		  (SELECT TECH_PLATFORM_ID
		  FROM TECH_PLATFORM
		  WHERE TECHN_PLATFORM_NM = 'hrznotification_01'
		  );
		  
DELETE
		FROM TECH_PLATFORM
		WHERE TECHN_PLATFORM_NM = 'hrznotification_01';
		
DELETE
		FROM TECH_PLATFORM_TYP
		WHERE TECH_PLATFORM_TYP_NM = 'hrznotification_01';
		
COMMIT;

spool off


