--============================================================================
--    $Id: isp_layer.sql,v 1.1 2010/01/13 21:22:39 dilekk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 03 Oct 2007
--  ISP Network
--============================================================================
spool isp_layer.log

set escape on
set serveroutput on
set define off


EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions ISP Network ----');

----------------
---- Apache ----
----------------
exec AddTechPlatform('apache_pweb', TechPlatformTyp('apache'));
exec AddTechPlatformMgmtMode(TechPlatform('apache_pweb'), MgmtMode('provision'));

exec AddSubntwk('Apache_Personal_Web_Platform', SubntwkTyp('personal_web_service_cluster'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddLink('Apache_Personal_Web_Platform_TO_Vancouver', LinkTyp('logical'), SubntwkId('Apache_Personal_Web_Platform'), SubntwkId('Vancouver'));
exec AddLink('Apache_Personal_Web_Platform_TO_Richmond', LinkTyp('logical'), SubntwkId('Apache_Personal_Web_Platform'), SubntwkId('Richmond'));
exec AddLink('Apache_Personal_Web_Platform_TO_Burnaby', LinkTyp('logical'), SubntwkId('Apache_Personal_Web_Platform'), SubntwkId('Burnaby'));
exec AddLink('Apache_Personal_Web_Platform_TO_Victoria', LinkTyp('logical'), SubntwkId('Apache_Personal_Web_Platform'), SubntwkId('Victoria'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Apache_Personal_Web_Platform'), TechPlatform('apache_pweb'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

------------------
---- Openwave ----
------------------
exec AddTechPlatform('openwave_email', TechPlatformTyp('openwavemx'));
exec AddTechPlatformMgmtMode(TechPlatform('openwave_email'), MgmtMode('provision'));

exec AddSubntwk('Openwave_Email_Platform', SubntwkTyp('email_service_cluster'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('Openwave_Email_Platform'), 'host_autoreply', 'unknown');
exec AddSubntwkParm(SubntwkId('Openwave_Email_Platform'), 'host_mss', 'unknown');
exec AddSubntwkParm(SubntwkId('Openwave_Email_Platform'), 'ldap_adapter_id', 'unknown');

exec AddLink('Openwave_Email_Platform_TO_Vancouver', LinkTyp('logical'), SubntwkId('Openwave_Email_Platform'), SubntwkId('Vancouver'));
exec AddLink('Openwave_Email_Platform_TO_Richmond', LinkTyp('logical'), SubntwkId('Openwave_Email_Platform'), SubntwkId('Richmond'));
exec AddLink('Openwave_Email_Platform_TO_Burnaby', LinkTyp('logical'), SubntwkId('Openwave_Email_Platform'), SubntwkId('Burnaby'));
exec AddLink('Openwave_Email_Platform_TO_Victoria', LinkTyp('logical'), SubntwkId('Openwave_Email_Platform'), SubntwkId('Victoria'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Openwave_Email_Platform'), TechPlatform('openwave_email'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

set define on

spool off