---------------------------------------------------------
--- Deleting links between subntwk and Tech Platform ----
---------------------------------------------------------

DELETE
FROM Sbnt_Tech_Platform
WHERE subntwk_id     = SubntwkId('AtHome')
AND tech_platform_id = TechPlatform('itv_01')
AND mgmt_mode_id     = MgmtMode('provision');
DELETE
FROM Sbnt_Tech_Platform
WHERE subntwk_id     = SubntwkId('Multikabel')
AND tech_platform_id = TechPlatform('itv_01')
AND mgmt_mode_id     = MgmtMode('provision');
DELETE
FROM Sbnt_Tech_Platform
WHERE subntwk_id     = SubntwkId('Casema')
AND tech_platform_id = TechPlatform('itv_01')
AND mgmt_mode_id     = MgmtMode('provision');
COMMIT;