--============================================================================
-- $Id: mvno_routing_topology.sql,v 1.1 2014/04/09 Rahul Exp $ 
--  REVISION HISTORY

--============================================================================

spool mvno_routing_topology.log

--Additional Parameters added for Routing on MVNO subnetworks

exec AddSubntwkParm(SubntwkId('NL_home_network'), 'profile_key', '1200');
exec AddSubntwkParm(SubntwkId('NL_home_network'), 'announcement',  '2');

commit;
spool off
exit