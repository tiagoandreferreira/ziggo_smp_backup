	--============================================================================
-- $Id: netnumber_topology.sql,v 1.1 2014/01/24 divya-sristi Exp $ 
-- $Id: netnumber_topology.sql,v 1.2 2015/02/06 divya-sristi Exp $
--									 
--  REVISION HISTORY

--============================================================================
-----------------------------
---- Adding TechPlatform ----
-----------------------------
EXEC AddTechPlatform('netnumber_01', TechPlatformTyp('netnumber'));
EXEC AddTechPlatform('netnumnotification_01', TechPlatformTyp('netnumnotification'));
--------------------------------------------
---- Adding TechPlatform Management Mode----
--------------------------------------------
EXEC AddTechPlatformMgmtMode(TechPlatform('netnumber_01'), MgmtMode('provision'));
EXEC AddTechPlatformMgmtMode(TechPlatform('netnumnotification_01'), MgmtMode('provision'));
-------------------------------------------------------
--- Adding links between subntwk and Tech Platform ----
-------------------------------------------------------

-- Mobile

 INSERT
   INTO Sbnt_Tech_Platform
  (
    subntwk_id      ,
    tech_platform_id,
    mgmt_mode_id    ,
    created_dtm     ,
    created_by      ,
    modified_dtm    ,
    modified_by
  )
  VALUES
  (
    SubntwkId('NL_home_network')      ,
    TechPlatform('netnumber_01'),
    MgmtMode('provision')             ,
    SYSDATE                           ,
    'INIT'                            ,
    NULL                              ,
    NULL
  );

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('NL_home_network'), TechPlatform('netnumnotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('box_1'), TechPlatform('netnumnotification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

-- Clean Routing

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CROUTE'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


-- UFO Routing

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS04N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS04N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_3_10'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_2_10'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_1_10'), TechPlatform('netnumber_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

-- Operator Code subnetwork parm added for all switch id's of Voice and Mobile

exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS04N1MND.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('CMS04N2MND.TELEFONIE.ZIGGO.LOCAL'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'operator_code',      'CSMA');

exec AddSubntwkParm(SubntwkId('box_1'), 'operator_code',      'ZIMO');

exec AddSubntwkParm(SubntwkId('NL_home_network'), 'operator_code','ZIMO');

COMMIT;
spool off
exit