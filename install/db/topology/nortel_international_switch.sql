--============================================================================
--    $Id: nortel_international_switch.sql,v 1.3 2012/03/14 19:26:20 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
spool nortel_international_switch.log

-- add super region
exec AddSubntwk('Paris-SR4', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Cannes-SR4', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Paris', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Cannes', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-- add region
exec AddSubntwk('PAR4-1', SubntwkTyp('region'), SubntwkId('Paris-SR4'), TopologyStatus('in_service'));
exec AddSubntwk('PAR5-1', SubntwkTyp('region'), SubntwkId('Paris'), TopologyStatus('in_service'));
exec AddSubntwk('CAN4-1', SubntwkTyp('region'), SubntwkId('Cannes-SR4'), TopologyStatus('in_service'));
exec AddSubntwk('CAN5-1', SubntwkTyp('region'), SubntwkId('Cannes'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('PAR4-1'), 'clec_supplied_e911', 'Y');
exec AddSubntwkParm(SubntwkId('PAR5-1'), 'clec_supplied_e911', 'Y');
exec AddSubntwkParm(SubntwkId('CAN4-1'), 'clec_supplied_e911', 'Y');
exec AddSubntwkParm(SubntwkId('CAN5-1'), 'clec_supplied_e911', 'Y');

-- add NAD
exec AddSubntwk('Paris4', SubntwkTyp('network_access_device'), SubntwkId('Paris-SR4'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Paris4'), 'desc', 'Paris4');
exec AddSubntwkParm(SubntwkId('Paris4'), 'dpm_location', 'Paris4');

exec AddSubntwk('Paris5', SubntwkTyp('network_access_device'), SubntwkId('Paris'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Paris5'), 'pep_name', '10k.CiscoDQoS.Pepserver');
exec AddSubntwkParm(SubntwkId('Paris5'), 'desc', 'Paris5');
exec AddSubntwkParm(SubntwkId('Paris5'), 'dpm_location', 'Paris5');

exec AddSubntwk('Cannes4', SubntwkTyp('network_access_device'), SubntwkId('Cannes-SR4'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cannes4'), 'desc', 'Cannes4');
exec AddSubntwkParm(SubntwkId('Cannes4'), 'dpm_location', 'Cannes4');

exec AddSubntwk('Cannes5', SubntwkTyp('network_access_device'), SubntwkId('Cannes'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cannes5'), 'pep_name', '10k.CiscoDQoS.Pepserver');
exec AddSubntwkParm(SubntwkId('Cannes5'), 'desc', 'Cannes5');
exec AddSubntwkParm(SubntwkId('Cannes5'), 'dpm_location', 'Cannes5');

-- add link between NAD and region
exec AddLink('NAD_REGION_PAR4-1', LinkTyp('logical'), SubntwkId('Paris4'), SubntwkId('PAR4-1'));
exec AddLink('NAD_REGION_PAR5-1', LinkTyp('logical'), SubntwkId('Paris5'), SubntwkId('PAR5-1'));
exec AddLink('NAD_REGION_CAN4-1', LinkTyp('logical'), SubntwkId('Cannes4'), SubntwkId('CAN4-1'));
exec AddLink('NAD_REGION_CAN5-1', LinkTyp('logical'), SubntwkId('Cannes5'), SubntwkId('CAN5-1'));

-- add CMS
exec AddSubntwk('CMS_NT_IN1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'auto_attn_route',          '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_cust_group',       'RESGRP');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_inter_cic',        '');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_inter_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_intl_cic',         '');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_intl_cic_choice',  'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_intra_cic' ,       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_intra_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_lata_name',        '');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_lcc',              'IBN');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mgprotocolport',   '2427');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mgprotocoltype',   '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mgprotocolversion','1.0');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mgsitename',       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mta_profile_id',   'TOUCHTONE_NN01_2');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_announcement', 'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_car',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_cmwiring',     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_cmwistd',      'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_crrcfw',       'ALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_crx',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_notice',       'MWL');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_mwt_status',       'ACT');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_name_netname',     'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_ncos_normal',      '0');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_ncos_suspend_all', '3');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_ncos_suspend_intl','1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_ncos_suspend_toll','2');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sdn_sdn_dny',      'NODNY');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sdn_sdn_opt',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sip_client',       'nortelpcc');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sip_location',     'sigma');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sonumber',         '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_suppress_netname', 'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_ac_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_ar_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfbl_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfbl_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfda_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfda_ringctrl', 'FIXRING');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfda_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfra_firstuse', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfw_cfwtype',   'C');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfw_fdn',       '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfw_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfw_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cfw_state',     'I');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cnamd_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cnd_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cndb_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cot_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_cxfertype', 'CTALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_cxrrcl',    'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_method',    'STD');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_orginter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_orgintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_rcltim',    '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_trminter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_cxr_trmintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_ddn_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_dnd_dndgrp',    '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_drcw_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_lcdr_bill_records',   'TRUE');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_sacb_sacbsus',  'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_sca_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_sca_dndonly',   'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_scf_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_scf_forwardto_dn',   '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_scf_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_scf_ringrem',   'NORING');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_scf_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_scrj_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_sw_wml_timeout',   '20');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_wml_custmod',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_wml_ftcode',       'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'vendor',                   'nortel');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'version',                  '1300i');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'pool',                     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'default_scr_enable_lcb',   '');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'mta_fqdn_prefix',          'mta');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'mta_fqdn_suffix',          'sigma.net');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('CMS_NT_IN1'), 'secondary_cms_address',    '192.168.1.101');

-- add GWCC
exec AddSubntwk('GWCC-IN1-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-IN1-CL2', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-IN1-CL3', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-IN1-CL4', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));

-- add GWC
exec AddSubntwk('GWC-IN1-CLI1-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-IN1-CLI2-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-IN1-CLI3-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-IN1-CLI4-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_IN1'), TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI1-1'), 'Dqos_enabled',     'y');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI1-1'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI1-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI1-1'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI1-1'), 'resv_left',        '13797');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI1-1'), 'skip_load_balance','n');

exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI2-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI2-1'), 'ep_left',          '99999');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI2-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI2-1'), 'max_no_line_supp', '99999');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI2-1'), 'resv_left',        '99999');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI2-1'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI3-1'), 'Dqos_enabled',     'y');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI3-1'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI3-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI3-1'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI3-1'), 'resv_left',        '13797');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI3-1'), 'skip_load_balance','n');

exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI4-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI4-1'), 'ep_left',          '99999');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI4-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI4-1'), 'max_no_line_supp', '99999');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI4-1'), 'resv_left',        '99999');
exec AddSubntwkParm(SubntwkId('GWC-IN1-CLI4-1'), 'skip_load_balance','y');

-- add link between GWCC and GWC
exec AddLink('GWC_GWCC-CLI1-1-CL1', LinkTyp('logical'), SubntwkId('GWC-IN1-CLI1-1'), SubntwkId('GWCC-IN1-CL1'));
exec AddLink('GWC_GWCC-CLI1-2-CL2', LinkTyp('logical'), SubntwkId('GWC-IN1-CLI2-1'), SubntwkId('GWCC-IN1-CL2'));
exec AddLink('GWC_GWCC-CLI1-3-CL3', LinkTyp('logical'), SubntwkId('GWC-IN1-CLI3-1'), SubntwkId('GWCC-IN1-CL3'));
exec AddLink('GWC_GWCC-CLI1-4-CL4', LinkTyp('logical'), SubntwkId('GWC-IN1-CLI4-1'), SubntwkId('GWCC-IN1-CL4'));

-- add link between GWCC and NAD for NCS
exec AddLink('GWCC_TO_NAD_Paris', LinkTyp('logical'), SubntwkId('GWCC-IN1-CL1'), SubntwkId('Paris5'));
exec AddLink('GWCC_TO_NAD_Cannes', LinkTyp('logical'), SubntwkId('GWCC-IN1-CL3'), SubntwkId('Cannes5'));

-- add link between NAM and NAD for NCS
exec AddLink('NAM_NAD_PAR', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Paris5'));
exec AddLink('NAM_NAD_CAN', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Cannes5'));

-- add link between GWCC and Super Region for SIP
exec AddLink('GWCC_TO_SRGN_Paris-SR4', LinkTyp('logical'), SubntwkId('GWCC-IN1-CL2'), SubntwkId('Paris-SR4'));
exec AddLink('GWCC_TO_SRGN_Cannes-SR4', LinkTyp('logical'), SubntwkId('GWCC-IN1-CL4'), SubntwkId('Cannes-SR4'));

-- add link between NAM and Super Region for SIP
exec AddLink('NAM_NAD_Paris-SR4', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Paris-SR4'));
exec AddLink('NAM_NAD_Cannes-SR4', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Cannes-SR4'));

-- add vmail
exec AddSubntwk('VM-NT-INC1', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-NT-INC1'), 'org_id_business', '112');

-- add link between vmail and CMS
exec AddLink('VM-NT-INC1_TO_CMS', LinkTyp('logical'), SubntwkId('VM-NT-INC1'), SubntwkId('CMS_NT_IN1'));

-- tech platform
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-NT-INC1'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_NT_IN1'), TechPlatform('nortel_cs2000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

