--============================================================================
--    $Id: residential_lec_network.sql,v 1.1 2010/01/19 20:28:45 dilekk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential LEC Network
--============================================================================
spool residential_lec_network.log

set escape on
set serveroutput on

-------------
---- LEC ----
-------------
exec AddSubntwk('Level3_01', SubntwkTyp('lec'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Level3_01'), 'Application', 'CableOne-SMP');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'customer_service_name', 'CableOne-ELS-1');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'Department', 'CableOne-VoIP');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'LEC_Partner_ID', 'CableOne_Test');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'Name', 'unknown');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'Organization', 'CableOne');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'ProductID', '2042');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'ResponseURL', 'unknown');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'UserID', 'cableone');
exec AddSubntwkParm(SubntwkId('Level3_01'), 'default_service_package', 'Primary Local and Domestic LD Flat Rate');

------------------------------
---- Technology Platforms ----
------------------------------
-------------
---- LEC ----
-------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Level3_01'), TechPlatform('level_three_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


spool off