--============================================================================
--    $Id: residential_ncs_dqos_lec_network.sql,v 1.1 2010/01/19 20:29:02 dilekk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential NCS DQOS LEC Network
--============================================================================
spool residential_ncs_dqos_lec_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential NCS DQOS LEC Network ----');

-------------------------------------------
---- Links between LEC and Call Server ----
-------------------------------------------
exec AddLink('LEC_TO_CMS_NT_RES1', LinkTyp('logical'), SubntwkId('Level3_01'), SubntwkId('CMS_NT_RES1'));
exec AddLink('LEC_TO_CMS_NT_RES2', LinkTyp('logical'), SubntwkId('Level3_01'), SubntwkId('CMS_NT_RES2'));

spool off