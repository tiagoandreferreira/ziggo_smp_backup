--============================================================================
--    $Id: residential_ncs_dqos_super_region.sql,v 1.10 2011/07/12 17:53:21 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential DQOS Super Region
--============================================================================
spool residential_ncs_dqos_super_region.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential DQOS Super Region ----');

--------------------
----- Vancouver ----
--------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Vancouver', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('VAN2-1', SubntwkTyp('region'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN2-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN2-2', SubntwkTyp('region'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN2-2'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN5-1', SubntwkTyp('region'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN5-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN5-2', SubntwkTyp('region'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN5-2'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN7-1', SubntwkTyp('region'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN7-1'), 'clec_supplied_e911', 'Y');
-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Vancouver2', SubntwkTyp('network_access_device'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwk('Vancouver5', SubntwkTyp('network_access_device'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
exec AddSubntwk('Vancouver7', SubntwkTyp('network_access_device'), SubntwkId('Vancouver'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VAN2-1', LinkTyp('logical'), SubntwkId('Vancouver2'), SubntwkId('VAN2-1'));
exec AddLink('NAD_REGION_VAN2-2', LinkTyp('logical'), SubntwkId('Vancouver2'), SubntwkId('VAN2-2'));

exec AddLink('NAD_REGION_VAN5-1', LinkTyp('logical'), SubntwkId('Vancouver5'), SubntwkId('VAN5-1'));
exec AddLink('NAD_REGION_VAN5-2', LinkTyp('logical'), SubntwkId('Vancouver5'), SubntwkId('VAN5-2'));

exec AddLink('NAD_REGION_VAN7-1', LinkTyp('logical'), SubntwkId('Vancouver7'), SubntwkId('VAN7-1'));

-----------------
---- Burnaby ----
-----------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Burnaby', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('BUR2-1', SubntwkTyp('region'), SubntwkId('Burnaby'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR2-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('BUR2-2', SubntwkTyp('region'), SubntwkId('Burnaby'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR2-2'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('BUR5-1', SubntwkTyp('region'), SubntwkId('Burnaby'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR5-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('BUR5-2', SubntwkTyp('region'), SubntwkId('Burnaby'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR5-2'), 'clec_supplied_e911', 'Y');
-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Burnaby2', SubntwkTyp('network_access_device'), SubntwkId('Burnaby'), TopologyStatus('in_service'));
exec AddSubntwk('Burnaby5', SubntwkTyp('network_access_device'), SubntwkId('Burnaby'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_BUR2-1', LinkTyp('logical'), SubntwkId('Burnaby2'), SubntwkId('BUR2-1'));
exec AddLink('NAD_REGION_BUR2-2', LinkTyp('logical'), SubntwkId('Burnaby2'), SubntwkId('BUR2-2'));

exec AddLink('NAD_REGION_BUR5-1', LinkTyp('logical'), SubntwkId('Burnaby5'), SubntwkId('BUR5-1'));
exec AddLink('NAD_REGION_BUR5-2', LinkTyp('logical'), SubntwkId('Burnaby5'), SubntwkId('BUR5-2'));

------------------
---- Richmond ----
------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Richmond', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('RIC2-1', SubntwkTyp('region'), SubntwkId('Richmond'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('RIC2-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('RIC5-1', SubntwkTyp('region'), SubntwkId('Richmond'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('RIC5-1'), 'clec_supplied_e911', 'Y');
-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Richmond2', SubntwkTyp('network_access_device'), SubntwkId('Richmond'), TopologyStatus('in_service'));
exec AddSubntwk('Richmond5', SubntwkTyp('network_access_device'), SubntwkId('Richmond'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_RIC2-1', LinkTyp('logical'), SubntwkId('Richmond2'), SubntwkId('RIC2-1'));
exec AddLink('NAD_REGION_RIC5-1', LinkTyp('logical'), SubntwkId('Richmond5'), SubntwkId('RIC5-1'));

------------------
---- Victoria ----
------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Victoria', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('VIC2-1', SubntwkTyp('region'), SubntwkId('Victoria'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VIC2-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VIC5-1', SubntwkTyp('region'), SubntwkId('Victoria'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VIC5-1'), 'clec_supplied_e911', 'Y');
-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Victoria2', SubntwkTyp('network_access_device'), SubntwkId('Victoria'), TopologyStatus('in_service'));
exec AddSubntwk('Victoria5', SubntwkTyp('network_access_device'), SubntwkId('Victoria'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VIC2-1', LinkTyp('logical'), SubntwkId('Victoria2'), SubntwkId('VIC2-1'));
exec AddLink('NAD_REGION_VIC5-1', LinkTyp('logical'), SubntwkId('Victoria5'), SubntwkId('VIC5-1'));


-----------------
----- Regina ----
-----------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Regina', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('REG2-1', SubntwkTyp('region'), SubntwkId('Regina'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('REG2-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('REG5-1', SubntwkTyp('region'), SubntwkId('Regina'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('REG5-1'), 'clec_supplied_e911', 'Y');
-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Regina2', SubntwkTyp('network_access_device'), SubntwkId('Regina'), TopologyStatus('in_service'));
exec AddSubntwk('Regina5', SubntwkTyp('network_access_device'), SubntwkId('Regina'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_REG2-1', LinkTyp('logical'), SubntwkId('Regina2'), SubntwkId('REG2-1'));
exec AddLink('NAD_REGION_REG5-1', LinkTyp('logical'), SubntwkId('Regina5'), SubntwkId('REG5-1'));

-----------------
---- Toronto ----
-----------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Toronto', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('TOR2-1', SubntwkTyp('region'), SubntwkId('Toronto'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('TOR2-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('TOR5-1', SubntwkTyp('region'), SubntwkId('Toronto'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('TOR5-1'), 'clec_supplied_e911', 'Y');
-------------------------------
---- Network Access Device ----
-------------------------------
exec AddSubntwk('Toronto2', SubntwkTyp('network_access_device'), SubntwkId('Toronto'), TopologyStatus('in_service'));
exec AddSubntwk('Toronto5', SubntwkTyp('network_access_device'), SubntwkId('Toronto'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_TOR2-1', LinkTyp('logical'), SubntwkId('Toronto2'), SubntwkId('TOR2-1'));
exec AddLink('NAD_REGION_TOR5-1', LinkTyp('logical'), SubntwkId('Toronto5'), SubntwkId('TOR5-1'));

---------------------------------
---- Parameters for the NADs ----
---------------------------------
exec AddSubntwkParm(SubntwkId('Vancouver2'), 'pep_name',               'C4pep');
exec AddSubntwkParm(SubntwkId('Vancouver2'), 'dpm_location',           'Vancouver2');
exec AddSubntwkParm(SubntwkId('Vancouver5'), 'pep_name',               'C4pep');
exec AddSubntwkParm(SubntwkId('Vancouver7'), 'pep_name',               'C4pep');

exec AddSubntwkParm(SubntwkId('Burnaby2'), 'pep_name',                 'Cisco10Kpep');
exec AddSubntwkParm(SubntwkId('Burnaby2'), 'dpm_location',             'Burnaby2');
exec AddSubntwkParm(SubntwkId('Burnaby5'), 'pep_name',                 'Cisco10Kpep');

exec AddSubntwkParm(SubntwkId('Richmond2'), 'pep_name',                'testpep');
exec AddSubntwkParm(SubntwkId('Richmond2'), 'dpm_location',            'Richmond2');
exec AddSubntwkParm(SubntwkId('Richmond5'), 'pep_name',                'testpep');

exec AddSubntwkParm(SubntwkId('Victoria2'), 'pep_name',                'testpep');
exec AddSubntwkParm(SubntwkId('Victoria2'), 'dpm_location',            'Victoria2');
exec AddSubntwkParm(SubntwkId('Victoria5'), 'pep_name',                'testpep');

exec AddSubntwkParm(SubntwkId('Regina2'), 'pep_name',                  'testpep');
exec AddSubntwkParm(SubntwkId('Regina2'), 'dpm_location',              'Regina2');
exec AddSubntwkParm(SubntwkId('Regina5'), 'pep_name',                  'testpep');

exec AddSubntwkParm(SubntwkId('Toronto2'), 'pep_name',                 'Cisco10Kpep');
exec AddSubntwkParm(SubntwkId('Toronto2'), 'dpm_location',             'Toronto2');
exec AddSubntwkParm(SubntwkId('Toronto5'), 'pep_name',                 'Cisco10Kpep');

spool off
