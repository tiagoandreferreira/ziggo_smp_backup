--============================================================================
--    $Id: residential_ncs_dqos_video_network.sql,v 1.11 2010/09/21 02:58:32 jerryc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential DQOS Video Network
--============================================================================
spool residential_ncs_dqos_video_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential DQOS Video Network ----');

--------------
--- CAS01 ----
--------------
exec AddSubntwk('Cas01', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas01'), 'name', 'Cas01');
exec AddSubntwkParm(SubntwkId('Cas01'), 'manufacturer', 'Motorola');
exec AddSubntwkParm(SubntwkId('Cas01'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas01'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas01'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('Cas01'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas01'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas01'), 'vod_authorization_handle', '0');


exec AddLink('CAS01_TO_Vancouver', LinkTyp('logical'), SubntwkId('Cas01'), SubntwkId('Vancouver'));
exec AddLink('CAS01_TO_Regina', LinkTyp('logical'), SubntwkId('Cas01'), SubntwkId('Regina'));
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas01'), TechPlatform('motoroladac_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas01'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


exec AddSubntwk('cas01_motorola_dcx3400', SubntwkTyp('cpe_model'), SubntwkId('Cas01'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3400'), 'model', 'DCX3400');
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3400'), 'manufacturer', 'Motorola');
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3400'), 'use_xml_repo', 'y');
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3400'), 'cpe_model_id', '3400');
exec AddSubntwk('cas01_motorola_dcx3416', SubntwkTyp('cpe_model'), SubntwkId('Cas01'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3416'), 'model', 'DCX3416');
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3416'), 'manufacturer', 'Motorola');
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3416'), 'use_xml_repo', 'n');
exec AddSubntwkParm(SubntwkId('cas01_motorola_dcx3416'), 'cpe_model_id', '3416');


--------------
--- CAS02 ----
--------------
exec AddSubntwk('Cas02', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas02'), 'name', 'Cas02');
exec AddSubntwkParm(SubntwkId('Cas02'), 'manufacturer', 'Motorola');
exec AddSubntwkParm(SubntwkId('Cas02'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas02'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas02'), 'assume_preloaded_stb_flag', 'Y');
exec AddSubntwkParm(SubntwkId('Cas02'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas02'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas02'), 'vod_authorization_handle', '0');


exec AddLink('CAS02_TO_Richmond', LinkTyp('logical'), SubntwkId('Cas02'), SubntwkId('Richmond'));
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas02'), TechPlatform('motoroladac_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas02'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

exec AddSubntwk('cas02_motorola_dcx3400', SubntwkTyp('cpe_model'), SubntwkId('Cas02'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3400'), 'model', 'DCX3400');
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3400'), 'manufacturer', 'Motorola');
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3400'), 'use_xml_repo', 'y');
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3400'), 'cpe_model_id', '3400');
exec AddSubntwk('cas02_motorola_dcx3416', SubntwkTyp('cpe_model'), SubntwkId('Cas02'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3416'), 'model', 'DCX3416');
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3416'), 'manufacturer', 'Motorola');
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3416'), 'use_xml_repo', 'n');
exec AddSubntwkParm(SubntwkId('cas02_motorola_dcx3416'), 'cpe_model_id', '3416');

--------------
--- CAS03 ----
--------------
exec AddSubntwk('Cas03', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas03'), 'name', 'Cas03');
exec AddSubntwkParm(SubntwkId('Cas03'), 'manufacturer', 'CiscoDNCS');
exec AddSubntwkParm(SubntwkId('Cas03'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas03'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas03'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('Cas03'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas03'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas03'), 'vod_authorization_handle', '0');

exec AddLink('CAS03_TO_Burnaby', LinkTyp('logical'), SubntwkId('Cas03'), SubntwkId('Burnaby'));
exec AddLink('CAS03_TO_Toronto', LinkTyp('logical'), SubntwkId('Cas03'), SubntwkId('Toronto'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas03'), TechPlatform('ciscodncs_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas03'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

exec AddSubntwk('cas03_cisco_3240', SubntwkTyp('cpe_model'), SubntwkId('Cas03'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas03_cisco_3240'), 'model', '3240');
exec AddSubntwkParm(SubntwkId('cas03_cisco_3240'), 'manufacturer', 'Cisco_SA');
exec AddSubntwkParm(SubntwkId('cas03_cisco_3240'), 'use_xml_repo', 'y');
exec AddSubntwkParm(SubntwkId('cas03_cisco_3240'), 'cpe_model_id', '3240');
exec AddSubntwkParm(SubntwkId('cas03_cisco_3240'), 'default_cpe_model_version', '3240');
exec AddSubntwk('cas03_cisco_8300c', SubntwkTyp('cpe_model'), SubntwkId('Cas03'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas03_cisco_8300c'), 'model', '8300C');
exec AddSubntwkParm(SubntwkId('cas03_cisco_8300c'), 'manufacturer', 'Cisco_SA');
exec AddSubntwkParm(SubntwkId('cas03_cisco_8300c'), 'use_xml_repo', 'n');
exec AddSubntwkParm(SubntwkId('cas03_cisco_8300c'), 'cpe_model_id', '8300');
exec AddSubntwkParm(SubntwkId('cas03_cisco_8300c'), 'default_cpe_model_version', '8300');

--------------
--- CAS04 ----
--------------
exec AddSubntwk('Cas04', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas04'), 'name', 'Cas04');
exec AddSubntwkParm(SubntwkId('Cas04'), 'manufacturer', 'CiscoDNCS');
exec AddSubntwkParm(SubntwkId('Cas04'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas04'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas04'), 'assume_preloaded_stb_flag', 'Y');
exec AddSubntwkParm(SubntwkId('Cas04'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas04'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas04'), 'vod_authorization_handle', '0');

exec AddLink('CAS04_TO_Victoria', LinkTyp('logical'), SubntwkId('Cas04'), SubntwkId('Victoria'));
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas04'), TechPlatform('ciscodncs_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas04'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

exec AddSubntwk('cas04_cisco_3240', SubntwkTyp('cpe_model'), SubntwkId('Cas04'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas04_cisco_3240'), 'model', '3240');
exec AddSubntwkParm(SubntwkId('cas04_cisco_3240'), 'manufacturer', 'Cisco_SA');
exec AddSubntwkParm(SubntwkId('cas04_cisco_3240'), 'use_xml_repo', 'y');
exec AddSubntwkParm(SubntwkId('cas04_cisco_3240'), 'cpe_model_id', '3240');
exec AddSubntwkParm(SubntwkId('cas04_cisco_3240'), 'default_cpe_model_version', '3240');
exec AddSubntwk('cas04_cisco_8300c', SubntwkTyp('cpe_model'), SubntwkId('Cas04'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('cas04_cisco_8300c'), 'model', '8300C');
exec AddSubntwkParm(SubntwkId('cas04_cisco_8300c'), 'manufacturer', 'Cisco_SA');
exec AddSubntwkParm(SubntwkId('cas04_cisco_8300c'), 'use_xml_repo', 'n');
exec AddSubntwkParm(SubntwkId('cas04_cisco_8300c'), 'cpe_model_id', '8300');
exec AddSubntwkParm(SubntwkId('cas04_cisco_8300c'), 'default_cpe_model_version', '8300');

--------------------
---- Vod Server ----
--------------------
---------------
---- VOD01 ----
---------------
exec AddSubntwk('Vod01', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod01'), 'name', 'Vod01');
exec AddSubntwkParm(SubntwkId('Vod01'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod01'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod01'), 'version', '1.0');
exec AddLink('VOD01_TO_Vancouver', LinkTyp('logical'), SubntwkId('Vod01'), SubntwkId('Vancouver'));
exec AddLink('VOD01_TO_Regina', LinkTyp('logical'), SubntwkId('Vod01'), SubntwkId('Regina'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod01'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
---------------
---- VOD02 ----
---------------
exec AddSubntwk('Vod02', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod02'), 'name', 'Vod02');
exec AddSubntwkParm(SubntwkId('Vod02'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod02'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod02'), 'version', '1.0');
exec AddLink('VOD02_TO_Richmond', LinkTyp('logical'), SubntwkId('Vod02'), SubntwkId('Richmond'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod02'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
---------------
---- VOD03 ----
---------------
exec AddSubntwk('Vod03', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod03'), 'name', 'Vod03');
exec AddSubntwkParm(SubntwkId('Vod03'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod03'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod03'), 'version', '1.0');
exec AddLink('Vod03_TO_Burnaby', LinkTyp('logical'), SubntwkId('Vod03'), SubntwkId('Burnaby'));
exec AddLink('Vod03_TO_Toronto', LinkTyp('logical'), SubntwkId('Vod03'), SubntwkId('Toronto'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod03'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
---------------
---- VOD04 ----
---------------
exec AddSubntwk('Vod04', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod04'), 'name', 'Vod04');
exec AddSubntwkParm(SubntwkId('Vod04'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod04'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod04'), 'version', '1.0');
exec AddLink('Vod04_TO_Victoria', LinkTyp('logical'), SubntwkId('Vod04'), SubntwkId('Victoria'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod04'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

------------------------
---- CHANNEL LINEUP ----
------------------------
--------------------
---- CHLINEUP01 ----
--------------------
exec AddSubntwk('ChLineup01', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup01'), 'virtual_channel_map_hdl', '9');
exec AddLink('CHLINEUP01_TO_Vancouver', LinkTyp('logical'), SubntwkId('ChLineup01'), SubntwkId('Vancouver'));
exec AddLink('CHLINEUP01_TO_Regina', LinkTyp('logical'), SubntwkId('ChLineup01'), SubntwkId('Regina'));

--------------------
---- CHLINEUP02 ----
--------------------
exec AddSubntwk('ChLineup02', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup02'), 'virtual_channel_map_hdl', '9');
exec AddLink('CHLINEUP02_TO_Richmond', LinkTyp('logical'), SubntwkId('ChLineup02'), SubntwkId('Richmond'));

--------------------
---- CHLINEUP03 ----
--------------------
exec AddSubntwk('ChLineup03', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup03'), 'virtual_channel_map_hdl', '9');
exec AddLink('CHLINEUP03_TO_Burnaby', LinkTyp('logical'), SubntwkId('ChLineup03'), SubntwkId('Burnaby'));
exec AddLink('CHLINEUP03_TO_Toronto', LinkTyp('logical'), SubntwkId('ChLineup03'), SubntwkId('Toronto'));

--------------------
---- CHLINEUP04 ----
--------------------
exec AddSubntwk('ChLineup04', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup04'), 'virtual_channel_map_hdl', '9');
exec AddLink('CHLINEUP04_TO_Victoria', LinkTyp('logical'), SubntwkId('ChLineup04'), SubntwkId('Victoria'));

--------------------
---- VIDEO ZONE ----
--------------------
----------------
---- ZONE01 ----
----------------
exec AddSubntwk('Zone01', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone01'), 'name', 'Zone01');
exec AddSubntwkParm(SubntwkId('Zone01'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone01'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone01'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone01'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone01'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone01'), 'timezone_id', '9');
exec AddSubntwkParm(SubntwkId('Zone01'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone01'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone01'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone01'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone01'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone01'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone01'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone01'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone01'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone01'), 'population_id', 'POPID');
exec AddLink('ZONE01_TO_Vancouver', LinkTyp('logical'), SubntwkId('Zone01'), SubntwkId('Vancouver'));

----------------
---- ZONE02 ----
----------------
exec AddSubntwk('Zone02', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone02'), 'name', 'Zone02');
exec AddSubntwkParm(SubntwkId('Zone02'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone02'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone02'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone02'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone02'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone02'), 'timezone_id', '9');
exec AddSubntwkParm(SubntwkId('Zone02'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone02'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone02'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone02'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone02'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone02'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone02'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone02'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone02'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone02'), 'population_id', 'POPID');
exec AddLink('ZONE02_TO_Richmond', LinkTyp('logical'), SubntwkId('Zone02'), SubntwkId('Richmond'));
exec AddLink('ZONE02_TO_Regina', LinkTyp('logical'), SubntwkId('Zone02'), SubntwkId('Regina'));

----------------
---- ZONE03 ----
----------------
exec AddSubntwk('Zone03', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone03'), 'name', 'Zone03');
exec AddSubntwkParm(SubntwkId('Zone03'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone03'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone03'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone03'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone03'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone03'), 'timezone_id', '9');
exec AddSubntwkParm(SubntwkId('Zone03'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone03'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone03'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone03'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone03'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone03'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone03'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone03'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone03'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone03'), 'population_id', 'POPID');
exec AddLink('ZONE03_TO_Burnaby', LinkTyp('logical'), SubntwkId('Zone03'), SubntwkId('Burnaby'));

----------------
---- ZONE04 ----
----------------
exec AddSubntwk('Zone04', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone04'), 'name', 'Zone04');
exec AddSubntwkParm(SubntwkId('Zone04'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone04'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone04'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone04'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone04'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone04'), 'timezone_id', '9');
exec AddSubntwkParm(SubntwkId('Zone04'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone04'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone04'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone04'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone04'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone04'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone04'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone04'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone04'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone04'), 'population_id', 'POPID');
exec AddLink('ZONE04_TO_Victoria', LinkTyp('logical'), SubntwkId('Zone04'), SubntwkId('Victoria'));
exec AddLink('ZONE04_TO_Toronto', LinkTyp('logical'), SubntwkId('Zone04'), SubntwkId('Toronto'));


--------------------------------------
---- Parameters for Super Regions ----
--------------------------------------
exec AddSubntwkParm(SubntwkId('Vancouver'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby'),   'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Richmond'),  'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Victoria'),  'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Regina'),    'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Toronto'),   'downstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('Vancouver'), 'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby'),   'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Richmond'),  'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Victoria'),  'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Regina'),    'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Toronto'),   'headend_hdl', '9');

--------------------------------
---- Parameters for Regions ----
--------------------------------
exec AddSubntwkParm(SubntwkId('VAN2-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VAN2-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN2-2'), 'video_plant_capability', '2 way');
exec AddSubntwkParm(SubntwkId('VAN2-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN5-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('VAN5-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN5-2'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VAN5-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN7-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('VAN7-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR2-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('BUR2-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR2-2'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('BUR2-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR5-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('BUR5-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR5-2'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('BUR5-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('RIC2-1'), 'video_plant_capability', '2 way');
exec AddSubntwkParm(SubntwkId('RIC2-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('RIC5-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('RIC5-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VIC2-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VIC2-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VIC5-1'), 'video_plant_capability', '2 way');
exec AddSubntwkParm(SubntwkId('VIC5-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('REG2-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('REG2-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('REG5-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('REG5-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('TOR2-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('TOR2-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('TOR5-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('TOR5-1'), 'upstream_plant_hdl', '9');

spool off
