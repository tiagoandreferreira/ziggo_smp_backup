--============================================================================
--    $Id: residential_ncs_dqos_voice_network.sql,v 1.20 2012/03/14 19:26:36 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--============================================================================
spool residential_ncs_dqos_voice_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential NCS DQOS Voice Network ----');

---------------------
---- Call Server ----
---------------------
exec AddSubntwk('CMS_NT_RES1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('CMS_NT_RES2', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_SI_RES1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-------------------------------------
---- Gateway Controller Clusters ----
-------------------------------------
exec AddSubntwk('GWCC-NR1-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_RES1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-NR1-CL2', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_RES1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-NR2-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_RES2'), TopologyStatus('in_service'));

exec AddSubntwk('GWCC-SR1-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_SI_RES1'), TopologyStatus('in_service'));

-----------------------------
---- Gateway Controllers ----
----------------------------- 
exec AddSubntwk('GWC-NR1-CL1-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_RES1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-NR1-CL2-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_RES1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-NR2-CL1-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_RES2'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-NR2-CL1-2', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_RES2'), TopologyStatus('in_service'));

exec AddSubntwk('GWC-SR1-CL1-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_SI_RES1'), TopologyStatus('in_service'));

------------------------------------
---- Links between GWCC and GWC ----
------------------------------------
exec AddLink('GWC_GWCC-NR1-CL1', LinkTyp('logical'), SubntwkId('GWC-NR1-CL1-1'), SubntwkId('GWCC-NR1-CL1'));
exec AddLink('GWC_GWCC-NR1-CL2', LinkTyp('logical'), SubntwkId('GWC-NR1-CL2-1'), SubntwkId('GWCC-NR1-CL2'));
exec AddLink('GWC1_GWCC-NR2-CL1', LinkTyp('logical'), SubntwkId('GWC-NR2-CL1-1'), SubntwkId('GWCC-NR2-CL1'));
exec AddLink('GWC2_GWCC-NR2-CL1', LinkTyp('logical'), SubntwkId('GWC-NR2-CL1-2'), SubntwkId('GWCC-NR2-CL1'));

exec AddLink('GWC_GWCC-SR1-CL1', LinkTyp('logical'), SubntwkId('GWC-SR1-CL1-1'), SubntwkId('GWCC-SR1-CL1'));

--------------------------------------------------
---- Parameters for Call Server - CMS_NT_RES1 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'auto_attn_route',          '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_cust_group',       'RESGRP');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_inter_cic',        '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_inter_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_intl_cic',         '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_intl_cic_choice',  'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_intra_cic' ,       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_intra_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_lata_name',        'LATA1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_lcc',              '1FR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mgprotocolport',   '2427');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mgprotocoltype',   '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mgprotocolversion','1.0');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mgsitename',       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mta_profile_id',   'TOUCHTONE_NN01_2');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_announcement', 'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_car',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_cmwiring',     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_cmwistd',      'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_crrcfw',       'ALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_crx',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_notice',       'CMWI');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_mwt_status',       'ACT');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_name_netname',     'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_ncos_normal',      '0');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_ncos_suspend_all', '3');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_ncos_suspend_intl','1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_ncos_suspend_toll','2');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sdn_sdn_dny',      'NODNY');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sdn_sdn_opt',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sip_client',       'nortelpcc');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sip_location',     'sigma');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sonumber',         '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_suppress_netname', 'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_ac_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_ar_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfbl_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfbl_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfda_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfda_ringctrl', 'FIXRING');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfda_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfra_firstuse', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfw_cfwtype',   'C');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfw_fdn',       '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfw_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfw_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cfw_state',     'I');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cnamd_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cnd_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cndb_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cot_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_cxfertype', 'CTALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_cxrrcl',    'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_method',    'STD');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_orginter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_orgintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_rcltim',    '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_trminter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_cxr_trmintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_ddn_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_dnd_dndgrp',    '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_drcw_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_lcdr_bill_records',   'TRUE');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_sacb_sacbsus',  'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_sca_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_sca_dndonly',   'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_scf_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_scf_forwardto_dn',   '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_scf_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_scf_ringrem',   'NORING');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_scf_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_scrj_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_sw_wml_timeout',   '20');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_wml_custmod',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_wml_ftcode',       'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'vendor',                   'nortel');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'version',                  '1300');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'pool',                     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'default_scr_enable_lcb',   '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'mta_fqdn_prefix',   'mta');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'mta_fqdn_suffix',   'sigma.net');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES1'), 'primary_cms_address',      'cms-nt-res1.sigma.net');

--------------------------------------------------
---- Parameters for Call Server - CMS_NT_RES2 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'auto_attn_route',          '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_cust_group',       'RESGRP');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_inter_cic',        'nilc');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_inter_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_intl_cic',         'nilc');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_intl_cic_choice',  'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_intra_cic' ,       'nilc');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_intra_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_lata_name',        'LATA1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_lcc',              '1FR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mgprotocolport',   '2427');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mgprotocoltype',   '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mgprotocolversion','1.0');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mgsitename',       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mta_profile_id',   'TOUCHTONE_NN01_2');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_announcement', 'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_car',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_cmwiring',     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_cmwistd',      'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_crrcfw',       'ALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_crx',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_notice',       'CMWI');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_mwt_status',       'ACT');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_name_netname',     'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_ncos_normal',      '0');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_ncos_suspend_all', '3');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_ncos_suspend_intl','1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_ncos_suspend_toll','2');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sdn_sdn_dny',      'NODNY');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sdn_sdn_opt',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sip_client',       'nortelpcc');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sip_location',     'sigma');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sonumber',         '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_suppress_netname', 'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_ac_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_ar_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfbl_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfbl_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfda_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfda_ringctrl', 'FIXRING');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfda_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfra_firstuse', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfw_cfwtype',   'C');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfw_fdn',       '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfw_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfw_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cfw_state',     'I');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cnamd_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cnd_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cndb_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cot_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_cxfertype', 'CTALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_cxrrcl',    'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_method',    'STD');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_orginter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_orgintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_rcltim',    '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_trminter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_cxr_trmintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_ddn_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_dnd_dndgrp',    '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_drcw_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_lcdr_bill_records',   'TRUE');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_sacb_sacbsus',  'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_sca_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_sca_dndonly',   'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_scf_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_scf_forwardto_dn',   '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_scf_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_scf_ringrem',   'NORING');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_scf_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_scrj_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_sw_wml_timeout',   '20');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_wml_custmod',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_wml_ftcode',       'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'vendor',                   'nortel');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'version',                  '1300');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'pool',                     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'default_scr_enable_lcb',   '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'mta_fqdn_prefix',   'mta');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'mta_fqdn_suffix',   'sigma.net');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES2'), 'primary_cms_address',      'cms-nt-res2.sigma.net');

--------------------------------------------------
---- Parameters for Call Server - CMS_SI_RES1 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_line_type',      'POTS_LINE');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_long_tran_timer',       '5');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_max_timer',      '20');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_mgc_ip_assignment',      'GwIPAddrAutomatic');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_retry_value',    '3');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_rtp_security_mode',      'Use-GW-Default');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_timeout',        '500');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_timer_hist',     '30');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_timer_t1',       '200');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_timer_t7',       '200');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_timer_t8',       '0');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');

----------------------------
---- Parameters for GWC ----
----------------------------
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL1-1'), 'Dqos_enabled',     'y');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL1-1'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL1-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL1-1'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL1-1'), 'resv_left',        '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL1-1'), 'skip_load_balance','n');

exec AddSubntwkParm(SubntwkId('GWC-NR1-CL2-1'), 'Dqos_enabled',     'y');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL2-1'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL2-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL2-1'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR1-CL2-1'), 'resv_left',        '25600');

exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-1'), 'Dqos_enabled',     'y');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-1'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-1'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-1'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-1'), 'resv_left',        '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-1'), 'skip_load_balance','n');

exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-2'), 'Dqos_enabled',     'y');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-2'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-2'), 'gwc_name',         'GWC-8');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-2'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-2'), 'resv_left',        '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR2-CL1-2'), 'skip_load_balance','n');

exec AddSubntwkParm(SubntwkId('GWC-SR1-CL1-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-SR1-CL1-1'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-SR1-CL1-1'), 'gwc_name',         'GWC-11');
exec AddSubntwkParm(SubntwkId('GWC-SR1-CL1-1'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-SR1-CL1-1'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-SR1-CL1-1'), 'skip_load_balance','y');

--------------------
---- Voice Mail ----
--------------------
exec AddSubntwk('VM-NT-RES1', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-NT-RES1'), 'org_id_business', '112');
exec AddSubntwk('VM-NT-RES2', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-NT-RES2'), 'org_id_business', '112');

exec AddSubntwk('VM-SI-RES1', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-SI-RES1'), 'org_id_business', '112');

--------------------------------------------------
---- Links between Voice Mail and Call Server ----
--------------------------------------------------
exec AddLink('VM-NT-RES1_TO_CMS', LinkTyp('logical'), SubntwkId('VM-NT-RES1'), SubntwkId('CMS_NT_RES1'));
exec AddLink('VM-NT-RES2_TO_CMS', LinkTyp('logical'), SubntwkId('VM-NT-RES2'), SubntwkId('CMS_NT_RES2'));

exec AddLink('VM-SI-RES1_TO_CMS', LinkTyp('logical'), SubntwkId('VM-SI-RES1'), SubntwkId('CMS_SI_RES1'));

------------------------------------
---- Links between GWCC and NAD ----
------------------------------------
-------------------
---- Vancouver ----
-------------------
exec AddLink('GWCC_TO_NAD_Vancouver2', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Vancouver2'));
exec AddLink('GWCC_TO_NAD_Vancouver5', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Vancouver5'));
exec AddLink('GWCC_TO_NAD_Vancouver7', LinkTyp('logical'), SubntwkId('GWCC-SR1-CL1'), SubntwkId('Vancouver7'));

-----------------
---- Burnaby ----
-----------------
exec AddLink('GWCC_TO_NAD_Burnaby2', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Burnaby2'));
exec AddLink('GWCC_TO_NAD_Burnaby5', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Burnaby5'));

------------------
---- Richmond ----
------------------
exec AddLink('GWCC_TO_NAD_Richmond2', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL2'), SubntwkId('Richmond2'));
exec AddLink('GWCC_TO_NAD_Richmond5', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL2'), SubntwkId('Richmond5'));

------------------
---- Victoria ----
------------------
exec AddLink('GWCC_TO_NAD_Victoria2', LinkTyp('logical'), SubntwkId('GWCC-NR2-CL1'), SubntwkId('Victoria2'));
exec AddLink('GWCC_TO_NAD_Victoria5', LinkTyp('logical'), SubntwkId('GWCC-NR2-CL1'), SubntwkId('Victoria5'));

----------------
---- Regina ----
----------------
exec AddLink('GWCC_TO_NAD_Regina2', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Regina2'));
exec AddLink('GWCC_TO_NAD_Regina5', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Regina5'));

-----------------
---- Toronto ----
-----------------
exec AddLink('GWCC_TO_NAD_Toronto2', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Toronto2'));
exec AddLink('GWCC_TO_NAD_Toronto5', LinkTyp('logical'), SubntwkId('GWCC-NR1-CL1'), SubntwkId('Toronto5'));

-----------------------------------
---- Links between NAM AND NAD ----
-----------------------------------
exec AddLink('NAM_NAD_VAN2', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Vancouver2'));
exec AddLink('NAM_NAD_VAN5', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Vancouver5'));
exec AddLink('NAM_NAD_BUR2', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Burnaby2'));
exec AddLink('NAM_NAD_BUR5', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Burnaby5'));
exec AddLink('NAM_NAD_RIC2', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Richmond2'));
exec AddLink('NAM_NAD_RIC5', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Richmond5'));
exec AddLink('NAM_NAD_VIC2', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Victoria2'));
exec AddLink('NAM_NAD_VIC5', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Victoria5'));
exec AddLink('NAM_NAD_REG2', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Regina2'));
exec AddLink('NAM_NAD_REG5', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Regina5'));
exec AddLink('NAM_NAD_TOR2', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Toronto2'));
exec AddLink('NAM_NAD_TOR5', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Toronto5'));
exec AddLink('NAM_NAD_VAN7', LinkTyp('logical'), SubntwkId('NAM_BAC'), SubntwkId('Vancouver7'));

------------------------------
---- Technology Platforms ----
------------------------------
--------------------
---- Voice Mail ----
--------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-NT-RES1'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-NT-RES2'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-SI-RES1'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_NT_RES1'), TechPlatform('nortel_cs2000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_NT_RES2'), TechPlatform('nortel_cs2000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_SI_RES1'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


spool off
