--============================================================================
--    $Id: residential_nortel_dms_super_region.sql,v 1.1 2011/09/28 18:25:54 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  Residential Nortel DMS Super Region
--============================================================================
spool residential_nortel_dms_super_region.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential Nortel DMS Super Region ----');
-------------------
---- Vancouver ----
-------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Vancouver-SR9', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

----------------
---- Region ---- 
----------------
exec AddSubntwk('VAN9-1', SubntwkTyp('region'), SubntwkId('Vancouver-SR9'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN9-1'), 'clec_supplied_e911', 'Y');

-----------------------------------
------- Network Access Device -----
-----------------------------------
exec AddSubntwk('Vancouver9', SubntwkTyp('network_access_device'), SubntwkId('Vancouver-SR9'), TopologyStatus('in_service'));

--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VAN9-1', LinkTyp('logical'), SubntwkId('Vancouver9'), SubntwkId('VAN9-1'));

-----------------------------
---- Parameters for NADs ----
-----------------------------
exec AddSubntwkParm(SubntwkId('Vancouver9'), 'desc',                'Vancouver9');
exec AddSubntwkParm(SubntwkId('Vancouver9'), 'dpm_location',        'Vancouver9');

spool off

