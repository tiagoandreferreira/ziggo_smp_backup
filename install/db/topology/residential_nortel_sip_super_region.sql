--============================================================================
--    $Id: residential_nortel_sip_super_region.sql,v 1.4 2010/02/04 18:51:17 dilekk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential Nortel SIP Super Region
--============================================================================
spool residential_nortel_sip_super_region.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential Nortel SIP Super Region ----');
-------------------
---- Vancouver ----
-------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Vancouver-SR4', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Vancouver-SR6', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('VAN4-1', SubntwkTyp('region'), SubntwkId('Vancouver-SR4'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN4-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN4-2', SubntwkTyp('region'), SubntwkId('Vancouver-SR4'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN4-2'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN6-1', SubntwkTyp('region'), SubntwkId('Vancouver-SR6'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN6-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN6-2', SubntwkTyp('region'), SubntwkId('Vancouver-SR6'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN6-2'), 'clec_supplied_e911', 'Y');
-----------------------------------
------- Network Access Device -----
-----------------------------------
exec AddSubntwk('Vancouver4', SubntwkTyp('network_access_device'), SubntwkId('Vancouver-SR4'), TopologyStatus('in_service'));
exec AddSubntwk('Vancouver6', SubntwkTyp('network_access_device'), SubntwkId('Vancouver-SR6'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VAN4-1', LinkTyp('logical'), SubntwkId('Vancouver4'), SubntwkId('VAN4-1'));
exec AddLink('NAD_REGION_VAN4-2', LinkTyp('logical'), SubntwkId('Vancouver4'), SubntwkId('VAN4-2'));

exec AddLink('NAD_REGION_VAN6-1', LinkTyp('logical'), SubntwkId('Vancouver6'), SubntwkId('VAN6-1'));
exec AddLink('NAD_REGION_VAN6-2', LinkTyp('logical'), SubntwkId('Vancouver6'), SubntwkId('VAN6-2'));

-----------------
---- Burnaby ----
-----------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Burnaby-SR4', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('Burnaby-SR6', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('BUR4-1', SubntwkTyp('region'), SubntwkId('Burnaby-SR4'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR4-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('BUR6-1', SubntwkTyp('region'), SubntwkId('Burnaby-SR6'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR6-1'), 'clec_supplied_e911', 'Y');
--------------------------------
---- Network Access Device -----
--------------------------------
exec AddSubntwk('Burnaby4', SubntwkTyp('network_access_device'), SubntwkId('Burnaby-SR4'), TopologyStatus('in_service'));
exec AddSubntwk('Burnaby6', SubntwkTyp('network_access_device'), SubntwkId('Burnaby-SR6'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_BUR4-1', LinkTyp('logical'), SubntwkId('Burnaby4'), SubntwkId('BUR4-1'));
exec AddLink('NAD_REGION_BUR6-1', LinkTyp('logical'), SubntwkId('Burnaby6'), SubntwkId('BUR6-1'));

-----------------------------
---- Parameters for NADs ----
-----------------------------
exec AddSubntwkParm(SubntwkId('Vancouver4'), 'desc',                'Vancouver4');
exec AddSubntwkParm(SubntwkId('Vancouver4'), 'dpm_location',        'Vancouver4');
exec AddSubntwkParm(SubntwkId('Vancouver6'), 'desc',                'Vancouver6');

exec AddSubntwkParm(SubntwkId('Burnaby4'), 'desc',                'Burnaby4');
exec AddSubntwkParm(SubntwkId('Burnaby4'), 'dpm_location',        'Burnaby4');
exec AddSubntwkParm(SubntwkId('Burnaby6'), 'desc',                'Burnaby6');

spool off