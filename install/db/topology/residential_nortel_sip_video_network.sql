--============================================================================
--    $Id: residential_nortel_sip_video_network.sql,v 1.3 2010/04/06 06:54:56 pallavig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential Nortel SIP Video Network
--============================================================================
spool residential_nortel_sip_video_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential Nortel SIP Video Network ----');
--------------------------------------------------
---- Links between Super Region and Video Cas ----
--------------------------------------------------
--------------
--- CAS05 ----
--------------
exec AddLink('CAS05_TO_Vancouver-SR4', LinkTyp('logical'), SubntwkId('Cas05'), SubntwkId('Vancouver-SR4'));
--------------
--- CAS06 ----
--------------
exec AddLink('CAS06_TO_Vancouver-SR6', LinkTyp('logical'), SubntwkId('Cas06'), SubntwkId('Vancouver-SR6'));
--------------
--- CAS07 ----
--------------
exec AddLink('CAS07_TO_Burnaby-SR4', LinkTyp('logical'), SubntwkId('Cas07'), SubntwkId('Burnaby-SR4'));
--------------
--- CAS08 ----
--------------
exec AddLink('CAS08_TO_Burnaby-SR6', LinkTyp('logical'), SubntwkId('Cas08'), SubntwkId('Burnaby-SR6'));
---------------------------------------------------
---- Links between Super Region and Vod Server ----
---------------------------------------------------
---------------
---- VOD05 ----
---------------
exec AddLink('VOD05_TO_Vancouver-SR4', LinkTyp('logical'), SubntwkId('Vod05'), SubntwkId('Vancouver-SR4'));
---------------
---- VOD06 ----
---------------
exec AddLink('VOD06_TO_Vancouver-SR6', LinkTyp('logical'), SubntwkId('Vod06'), SubntwkId('Vancouver-SR6'));
---------------
---- VOD07 ----
---------------
exec AddLink('VOD07_TO_Burnaby-SR4', LinkTyp('logical'), SubntwkId('Vod07'), SubntwkId('Burnaby-SR4'));
---------------
---- VOD04 ----
---------------
exec AddLink('VOD08_TO_Burnaby-SR6', LinkTyp('logical'), SubntwkId('Vod08'), SubntwkId('Burnaby-SR6'));
-------------------------------------------------------
---- Links between Super Region and CHANNEL LINEUP ----
-------------------------------------------------------
--------------------
---- CHLINEUP05 ----
--------------------
exec AddLink('CHLINEUP05_TO_Vancouver-SR4', LinkTyp('logical'), SubntwkId('ChLineup05'), SubntwkId('Vancouver-SR4'));
--------------------
---- CHLINEUP06 ----
--------------------
exec AddLink('CHLINEUP06_TO_Vancouver-SR6', LinkTyp('logical'), SubntwkId('ChLineup06'), SubntwkId('Vancouver-SR6'));
--------------------
---- CHLINEUP07 ----
--------------------
exec AddLink('CHLINEUP07_TO_Burnaby-SR4', LinkTyp('logical'), SubntwkId('ChLineup07'), SubntwkId('Burnaby-SR4'));
--------------------
---- CHLINEUP04 ----
--------------------
exec AddLink('CHLINEUP08_TO_Burnaby-SR6', LinkTyp('logical'), SubntwkId('ChLineup08'), SubntwkId('Burnaby-SR6'));
---------------------------------------------------
---- Links between Super Region and VIDEO ZONE ----
---------------------------------------------------
----------------
---- ZONE05 ----
----------------
exec AddLink('Zone05_TO_Vancouver-SR4', LinkTyp('logical'), SubntwkId('Zone05'), SubntwkId('Vancouver-SR4'));
----------------
---- Zone06 ----
----------------
exec AddLink('Zone06_TO_Vancouver-SR6', LinkTyp('logical'), SubntwkId('Zone06'), SubntwkId('Vancouver-SR6'));
----------------
---- ZONE07 ----
----------------
exec AddLink('Zone07_TO_Burnaby-SR4', LinkTyp('logical'), SubntwkId('Zone07'), SubntwkId('Burnaby-SR4'));
----------------
---- ZONE08 ----
----------------
exec AddLink('Zone08_TO_Burnaby-SR6', LinkTyp('logical'), SubntwkId('Zone08'), SubntwkId('Burnaby-SR6'));
--------------------------------------
---- Parameters for Super Regions ----
--------------------------------------
exec AddSubntwkParm(SubntwkId('Vancouver-SR4'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Vancouver-SR6'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby-SR4'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby-SR6'), 'downstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('Vancouver-SR4'), 'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Vancouver-SR6'), 'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby-SR4'), 'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby-SR6'), 'headend_hdl', '9');
--------------------------------
---- Parameters for Regions ----
--------------------------------
exec AddSubntwkParm(SubntwkId('VAN4-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VAN4-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN4-2'), 'video_plant_capability', '2 way');
exec AddSubntwkParm(SubntwkId('VAN4-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN6-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('VAN6-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN6-2'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VAN6-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR4-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('BUR4-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR6-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('BUR6-1'), 'upstream_plant_hdl', '9');

spool off