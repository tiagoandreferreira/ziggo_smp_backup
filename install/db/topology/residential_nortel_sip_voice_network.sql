--============================================================================
--    $Id: residential_nortel_sip_voice_network.sql,v 1.11 2011/12/12 20:43:56 ketans Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential Nortel SIP Voice Network
--============================================================================
spool residential_nortel_sip_voice_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential Nortel SIP Voice Network ----');

---------------------
---- Call Server ----
---------------------
exec AddSubntwk('CMS_NT_RES3', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-------------------------------------
---- Gateway Controller Clusters ----
-------------------------------------
exec AddSubntwk('GWCC-NR3-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_NT_RES3'), TopologyStatus('in_service'));

-----------------------------
---- Gateway Controllers ----
----------------------------- 
exec AddSubntwk('GWC-NR3-CL1-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_NT_RES3'), TopologyStatus('in_service'));

------------------------------------
---- Links between GWCC and GWC ----
------------------------------------
exec AddLink('GWC_GWCC-NR3-CL1', LinkTyp('logical'), SubntwkId('GWC-NR3-CL1-1'), SubntwkId('GWCC-NR3-CL1'));

--------------------------------------------------
---- Parameters for Call Server - CMS_NT_RES3 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'auto_attn_route',          '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_cust_group',       'RESGRP');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_inter_cic',        '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_inter_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_intl_cic',         '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_intl_cic_choice',  'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_intra_cic' ,       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_intra_cic_choice', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_lata_name',        'LATA1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_lcc',              '1FR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mgprotocolport',   '2427');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mgprotocoltype',   '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mgprotocolversion','1.0');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mgsitename',       '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mta_profile_id',   'TOUCHTONE_NN01_2');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_announcement', 'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_car',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_cmwiring',     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_cmwistd',      'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_crrcfw',       'ALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_crx',          'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_notice',       'MWL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_mwt_status',       'ACT');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_name_netname',     'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_ncos_normal',      '0');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_ncos_suspend_all', '3');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_ncos_suspend_intl','1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_ncos_suspend_toll','2');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sdn_sdn_dny',      'NODNY');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sdn_sdn_opt',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sip_client',       'nortelpcc');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sip_location',     'sigma');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sonumber',         '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_suppress_netname', 'PUBLIC');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_ac_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_ar_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfbl_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfbl_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfda_numcalls', '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfda_ringctrl', 'FIXRING');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfda_scrncl',   'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfra_firstuse', 'Y');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfw_cfwtype',   'C');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfw_fdn',       '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfw_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfw_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cfw_state',     'I');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cnamd_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cnd_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cndb_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cot_billing_option',   'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_cxfertype', 'CTALL');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_cxrrcl',    'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_method',    'STD');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_orginter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_orgintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_rcltim',    '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_trminter',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_cxr_trmintra',  '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_ddn_billing_option',  'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_dnd_dndgrp',    '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_drcw_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_lcdr_bill_records',   'TRUE');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_sacb_sacbsus',  'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_sca_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_sca_dndonly',   'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_scf_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_scf_forwardto_dn',   '$');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_scf_numcalls',  '1');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_scf_ringrem',   'NORING');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_scf_scrncl',    'NSCR');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_scrj_billing_option', 'AMA');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_sw_wml_timeout',   '20');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_wml_custmod',      'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_wml_ftcode',       'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'vendor',                   'nortel');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'version',                  '1300');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'pool',                     'N');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'default_scr_enable_lcb',   '');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'mta_fqdn_prefix',          'mta');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'mta_fqdn_suffix',          'sigma.net');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('CMS_NT_RES3'), 'secondary_cms_address',    '192.168.1.101');


----------------------------
---- Parameters for GWC ----
----------------------------
exec AddSubntwkParm(SubntwkId('GWC-NR3-CL1-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-NR3-CL1-1'), 'ep_left',          '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR3-CL1-1'), 'gwc_name',         'GWC-99');
exec AddSubntwkParm(SubntwkId('GWC-NR3-CL1-1'), 'max_no_line_supp', '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR3-CL1-1'), 'resv_left',        '25600');
exec AddSubntwkParm(SubntwkId('GWC-NR3-CL1-1'), 'skip_load_balance','y');

--------------------
---- Voice Mail ----
--------------------
exec AddSubntwk('VM-NT-RES3', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-NT-RES3'), 'org_id_business', '112');

--------------------------------------------------
---- Links between Voice Mail and Call Server ----
--------------------------------------------------
exec AddLink('VM-NT-RES3_TO_CMS', LinkTyp('logical'), SubntwkId('VM-NT-RES3'), SubntwkId('CMS_NT_RES3'));

---------------------------------------------
---- Links between GWCC and Super Region ----
---------------------------------------------
-------------------
---- Vancouver ----
-------------------
exec AddLink('GWCC_TO_SRGN_Vancouver-SR4', LinkTyp('logical'), SubntwkId('GWCC-NR3-CL1'), SubntwkId('Vancouver-SR4'));
exec AddLink('GWCC_TO_SRGN_Vancouver-SR6', LinkTyp('logical'), SubntwkId('GWCC-NR3-CL1'), SubntwkId('Vancouver-SR6'));

-----------------
---- Burnaby ----
-----------------
exec AddLink('GWCC_TO_SRGN_Burnaby-SR4', LinkTyp('logical'), SubntwkId('GWCC-NR3-CL1'), SubntwkId('Burnaby-SR4'));
exec AddLink('GWCC_TO_SRGN_Burnaby-SR6', LinkTyp('logical'), SubntwkId('GWCC-NR3-CL1'), SubntwkId('Burnaby-SR6'));

------------------------------------------------
---- The links between NAM and Super Region ----
------------------------------------------------
exec AddLink('NAM_SRGN_Vancouver-SR4', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Vancouver-SR4'));
exec AddLink('NAM_SRGN_Vancouver-SR6', LinkTyp('logical'), SubntwkId('NAM_ALU'), SubntwkId('Vancouver-SR6'));

exec AddLink('NAM_SRGN_Burnaby-SR4', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Burnaby-SR4'));
exec AddLink('NAM_SRGN_Burnaby-SR6', LinkTyp('logical'), SubntwkId('NAM_ALU'), SubntwkId('Burnaby-SR6'));

------------------------------
---- Technology Platforms ----
------------------------------
---------------------
---- Call Server ----
---------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_NT_RES3'), TechPlatform('nortel_cs2000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------------
---- Voice Mail ----
--------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-NT-RES3'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

spool off
