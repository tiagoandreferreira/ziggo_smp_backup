--============================================================================
--    $Id: residential_siemens_sip_super_region.sql,v 1.1 2010/09/23 01:49:06 jerryc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Super Region (Broadsoft)
--============================================================================
spool residential_siemens_sip_super_region.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential SIP Super Region (Broadsoft) ----');

--------------------
---- Vancouver -----
--------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Vancouver-SR8', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('VAN8-1', SubntwkTyp('region'), SubntwkId('Vancouver-SR8'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN8-1'), 'clec_supplied_e911', 'Y');

--------------------------------
---- Network Access Device -----
--------------------------------
exec AddSubntwk('Vancouver8', SubntwkTyp('network_access_device'), SubntwkId('Vancouver-SR8'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VAN8-1', LinkTyp('logical'), SubntwkId('Vancouver8'), SubntwkId('VAN8-1'));

-----------------------------
---- Parameters for NADs ----
-----------------------------
exec AddSubntwkParm(SubntwkId('Vancouver8'), 'desc',                'Vancouver8');
exec AddSubntwkParm(SubntwkId('Vancouver8'), 'dpm_location',        'Vancouver8');

spool off
