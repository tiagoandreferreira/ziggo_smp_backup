--============================================================================
--    $Id: residential_siemens_sip_video_network.sql,v 1.1 2010/09/23 01:49:06 jerryc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Video Network
--============================================================================
spool residential_siemens_sip_video_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential Nortel SIP Video Network ----');
--------------------------------------------------
---- Links between Super Region and Video Cas ----
--------------------------------------------------
--------------
--- CAS05 ----
--------------
exec AddLink('CAS05_TO_Vancouver-SR8', LinkTyp('logical'), SubntwkId('Cas05'), SubntwkId('Vancouver-SR8'));

---------------------------------------------------
---- Links between Super Region and Vod Server ----
---------------------------------------------------
---------------
---- VOD05 ----
---------------
exec AddLink('VOD05_TO_Vancouver-SR8', LinkTyp('logical'), SubntwkId('Vod05'), SubntwkId('Vancouver-SR8'));

-------------------------------------------------------
---- Links between Super Region and CHANNEL LINEUP ----
-------------------------------------------------------
--------------------
---- CHLINEUP05 ----
--------------------
exec AddLink('CHLINEUP05_TO_Vancouver-SR8', LinkTyp('logical'), SubntwkId('ChLineup05'), SubntwkId('Vancouver-SR8'));

---------------------------------------------------
---- Links between Super Region and VIDEO ZONE ----
---------------------------------------------------
----------------
---- ZONE05 ----
----------------
exec AddLink('Zone05_TO_Vancouver-SR8', LinkTyp('logical'), SubntwkId('Zone05'), SubntwkId('Vancouver-SR8'));

--------------------------------------
---- Parameters for Super Regions ----
--------------------------------------
exec AddSubntwkParm(SubntwkId('Vancouver-SR8'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Vancouver-SR8'), 'headend_hdl', '9');

--------------------------------
---- Parameters for Regions ----
--------------------------------
exec AddSubntwkParm(SubntwkId('VAN8-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VAN8-1'), 'upstream_plant_hdl', '9');

spool off
