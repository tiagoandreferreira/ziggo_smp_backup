--============================================================================
--    $Id: residential_siemens_sip_voice_network.sql,v 1.4 2011/12/12 20:43:56 ketans Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Voice Network (Broadsoft)
--============================================================================
spool residential_siemens_sip_voice_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential SIP Voice Network (Broadsoft) ----');

---------------------
---- Call Server ----
---------------------
exec AddSubntwk('CMS_SI_RES2', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-------------------------------------
---- Gateway Controller Clusters ----
-------------------------------------
exec AddSubntwk('GWCC-SR2-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_SI_RES2'), TopologyStatus('in_service'));

-----------------------------
---- Gateway Controllers ----
-----------------------------
exec AddSubntwk('GWC-SR2-CL1-1', SubntwkTyp('gateway_controller'), SubntwkId('CMS_SI_RES2'), TopologyStatus('in_service'));

------------------------------------
---- Links between GWCC and GWC ----
------------------------------------
exec AddLink('GWC_GWCC-SR2-CL1', LinkTyp('logical'), SubntwkId('GWC-SR2-CL1-1'), SubntwkId('GWCC-SR2-CL1'));

--------------------------------------------------
---- Parameters for Call Server - CMS_SI_RES2 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_line_type',      'POTS_LINE');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_long_tran_timer',        '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_retry_value',    'Y');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');

----------------------------
---- Parameters for GWC ----
----------------------------
exec AddSubntwkParm(SubntwkId('GWC-SR2-CL1-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-SR2-CL1-1'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-SR2-CL1-1'), 'gwc_name',         'GWC-11');
exec AddSubntwkParm(SubntwkId('GWC-SR2-CL1-1'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-SR2-CL1-1'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-SR2-CL1-1'), 'skip_load_balance','y');

--------------------
---- Voice Mail ----
--------------------
exec AddSubntwk('VM-SI-RES2', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-SI-RES2'), 'org_id_business', '112');

--------------------------------------------------
---- Links between Voice Mail and Call Server ----
--------------------------------------------------
exec AddLink('VM-SI-RES2_TO_CMS', LinkTyp('logical'), SubntwkId('VM-SI-RES2'), SubntwkId('CMS_SI_RES2'));

----------------------------------------------------
---- Links between Call Server and Super Region ----
----------------------------------------------------
-------------------
---- Vancouver ----
-------------------
exec AddLink('GWCC_TO_NAD_Vancouver-SR8', LinkTyp('logical'), SubntwkId('GWCC-SR2-CL1'), SubntwkId('Vancouver-SR8'));

------------------------------------------------
---- The links between NAM and Super Region ----
------------------------------------------------
exec AddLink('NAM_NAD_VANCOUVER-SR8', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Vancouver-SR8'));


------------------------------
---- Technology Platforms ----
------------------------------
---------------------
---- Call Server ----
---------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_SI_RES2'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------------
---- Voice Mail ----
--------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-SI-RES2'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

spool off
