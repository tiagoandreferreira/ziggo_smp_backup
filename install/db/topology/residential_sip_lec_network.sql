--============================================================================
--    $Id: residential_sip_lec_network.sql,v 1.2 2010/01/19 20:31:06 dilekk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential NCS DQOS LEC Network
--============================================================================
spool residential_sip_lec_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential SIP LEC Network ----');

-------------------------------------------
---- Links between LEC and Call Server ----
-------------------------------------------
exec AddLink('LEC_TO_CMS_BS_RES3', LinkTyp('logical'), SubntwkId('Level3_01'), SubntwkId('CMS_BS_RES3'));
exec AddLink('LEC_TO_CMS_BS_RES4', LinkTyp('logical'), SubntwkId('Level3_01'), SubntwkId('CMS_BS_RES4'));

spool off