--============================================================================
--    $Id: residential_sip_super_region.sql,v 1.3 2010/02/23 16:01:53 dilekk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Super Region (Broadsoft)
--============================================================================
spool residential_sip_super_region.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential SIP Super Region (Broadsoft) ----');

--------------------
---- Vancouver -----
--------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Vancouver-SR1', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('VAN1-1', SubntwkTyp('region'), SubntwkId('Vancouver-SR1'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN1-1'), 'clec_supplied_e911', 'Y');

exec AddSubntwk('VAN1-2', SubntwkTyp('region'), SubntwkId('Vancouver-SR1'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VAN1-2'), 'clec_supplied_e911', 'Y');

--------------------------------
---- Network Access Device -----
--------------------------------
exec AddSubntwk('Vancouver1', SubntwkTyp('network_access_device'), SubntwkId('Vancouver-SR1'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VAN1-1', LinkTyp('logical'), SubntwkId('Vancouver1'), SubntwkId('VAN1-1'));
exec AddLink('NAD_REGION_VAN1-2', LinkTyp('logical'), SubntwkId('Vancouver1'), SubntwkId('VAN1-2'));

-----------------
---- Burnaby ----
-----------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Burnaby-SR1', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('BUR1-1', SubntwkTyp('region'), SubntwkId('Burnaby-SR1'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('BUR1-1'), 'clec_supplied_e911', 'Y');

--------------------------------
---- Network Access Device -----
--------------------------------
exec AddSubntwk('Burnaby1', SubntwkTyp('network_access_device'), SubntwkId('Burnaby-SR1'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_BUR1-1', LinkTyp('logical'), SubntwkId('Burnaby1'), SubntwkId('BUR1-1'));

------------------
---- Victoria ----
------------------
----------------------
---- Super Region ---- 
----------------------
exec AddSubntwk('Victoria-SR1', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
---- Region ---- 
----------------
exec AddSubntwk('VIC1-1', SubntwkTyp('region'), SubntwkId('Victoria-SR1'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VIC1-1'), 'clec_supplied_e911', 'Y');

--------------------------------
---- Network Access Device -----
--------------------------------
exec AddSubntwk('Victoria1', SubntwkTyp('network_access_device'), SubntwkId('Victoria-SR1'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_VIC1-1', LinkTyp('logical'), SubntwkId('Victoria1'), SubntwkId('VIC1-1'));

-----------------------------
---- Parameters for NADs ----
-----------------------------
exec AddSubntwkParm(SubntwkId('Vancouver1'), 'desc',                'Vancouver1');
exec AddSubntwkParm(SubntwkId('Vancouver1'), 'dpm_location',        'Vancouver1');

exec AddSubntwkParm(SubntwkId('Burnaby1'),   'desc',                'Burnaby1');
exec AddSubntwkParm(SubntwkId('Burnaby1'),   'dpm_location',        'Burnaby1');

exec AddSubntwkParm(SubntwkId('Victoria1'),  'desc',                'Victoria1');
exec AddSubntwkParm(SubntwkId('Victoria1'),  'dpm_location',        'Victoria1');

spool off