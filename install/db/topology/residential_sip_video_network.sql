--============================================================================
--    $Id: residential_sip_video_network.sql,v 1.3 2010/04/06 06:54:56 pallavig Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Video Network
--============================================================================
spool residential_sip_video_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential Nortel SIP Video Network ----');
--------------------------------------------------
---- Links between Super Region and Video Cas ----
--------------------------------------------------
--------------
--- CAS05 ----
--------------
exec AddLink('CAS05_TO_Vancouver-SR1', LinkTyp('logical'), SubntwkId('Cas05'), SubntwkId('Vancouver-SR1'));
--------------
--- CAS06 ----
--------------
exec AddLink('CAS06_TO_Burnaby-SR1', LinkTyp('logical'), SubntwkId('Cas06'), SubntwkId('Burnaby-SR1'));
--------------
--- CAS07 ----
--------------
exec AddLink('CAS07_TO_Victoria-SR1', LinkTyp('logical'), SubntwkId('Cas07'), SubntwkId('Victoria-SR1'));
---------------------------------------------------
---- Links between Super Region and Vod Server ----
---------------------------------------------------
---------------
---- VOD05 ----
---------------
exec AddLink('VOD05_TO_Vancouver-SR1', LinkTyp('logical'), SubntwkId('Vod05'), SubntwkId('Vancouver-SR1'));
---------------
---- VOD06 ----
---------------
exec AddLink('VOD06_TO_Burnaby-SR1', LinkTyp('logical'), SubntwkId('Vod06'), SubntwkId('Burnaby-SR1'));
---------------
---- VOD07 ----
---------------
exec AddLink('VOD07_TO_Victoria-SR1', LinkTyp('logical'), SubntwkId('Vod07'), SubntwkId('Victoria-SR1'));
-------------------------------------------------------
---- Links between Super Region and CHANNEL LINEUP ----
-------------------------------------------------------
--------------------
---- CHLINEUP05 ----
--------------------
exec AddLink('CHLINEUP05_TO_Vancouver-SR1', LinkTyp('logical'), SubntwkId('ChLineup05'), SubntwkId('Vancouver-SR1'));
--------------------
---- CHLINEUP06 ----
--------------------
exec AddLink('CHLINEUP06_TO_Burnaby-SR1', LinkTyp('logical'), SubntwkId('ChLineup06'), SubntwkId('Burnaby-SR1'));
--------------------
---- CHLINEUP07 ----
--------------------
exec AddLink('CHLINEUP07_TO_Victoria-SR1', LinkTyp('logical'), SubntwkId('ChLineup07'), SubntwkId('Victoria-SR1'));
---------------------------------------------------
---- Links between Super Region and VIDEO ZONE ----
---------------------------------------------------
----------------
---- ZONE05 ----
----------------
exec AddLink('Zone05_TO_Vancouver-SR1', LinkTyp('logical'), SubntwkId('Zone05'), SubntwkId('Vancouver-SR1'));
----------------
---- Zone06 ----
----------------
exec AddLink('Zone06_TO_Burnaby-SR1', LinkTyp('logical'), SubntwkId('Zone06'), SubntwkId('Burnaby-SR1'));
----------------
---- ZONE07 ----
----------------
exec AddLink('Zone07_TO_Victoria-SR1', LinkTyp('logical'), SubntwkId('Zone07'), SubntwkId('Victoria-SR1'));
--------------------------------------
---- Parameters for Super Regions ----
--------------------------------------
exec AddSubntwkParm(SubntwkId('Vancouver-SR1'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby-SR1'), 'downstream_plant_hdl', '9');
exec AddSubntwkParm(SubntwkId('Victoria-SR1'), 'downstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('Vancouver-SR1'), 'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Burnaby-SR1'), 'headend_hdl', '9');
exec AddSubntwkParm(SubntwkId('Victoria-SR1'), 'headend_hdl', '9');

--------------------------------
---- Parameters for Regions ----
--------------------------------
exec AddSubntwkParm(SubntwkId('VAN1-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('VAN1-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VAN1-2'), 'video_plant_capability', '2 way');
exec AddSubntwkParm(SubntwkId('VAN1-2'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('BUR1-1'), 'video_plant_capability', 'ocap');
exec AddSubntwkParm(SubntwkId('BUR1-1'), 'upstream_plant_hdl', '9');

exec AddSubntwkParm(SubntwkId('VIC1-1'), 'video_plant_capability', '1 way');
exec AddSubntwkParm(SubntwkId('VIC1-1'), 'upstream_plant_hdl', '9');

spool off