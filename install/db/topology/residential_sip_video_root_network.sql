--============================================================================
--    $Id: residential_sip_video_root_network.sql,v 1.8 2010/10/14 21:18:19 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Video Root Network
--============================================================================
spool residential_sip_video_root_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential SIP Video Root Network ----');

--------------
--- CAS05 ----
--------------
exec AddSubntwk('Cas05', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas05'), 'name', 'Cas05');
exec AddSubntwkParm(SubntwkId('Cas05'), 'manufacturer', 'Microsoft');
exec AddSubntwkParm(SubntwkId('Cas05'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas05'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas05'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('Cas05'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas05'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas05'), 'vod_authorization_handle', '0');


Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas05'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------
--- CAS06 ----
--------------
exec AddSubntwk('Cas06', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas06'), 'name', 'Cas06');
exec AddSubntwkParm(SubntwkId('Cas06'), 'manufacturer', 'Microsoft');
exec AddSubntwkParm(SubntwkId('Cas06'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas06'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas06'), 'assume_preloaded_stb_flag', 'Y');
exec AddSubntwkParm(SubntwkId('Cas06'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas06'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas06'), 'vod_authorization_handle', '0');


Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas06'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas06'), TechPlatform('msmediaroom_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------
--- CAS07 ----
--------------
exec AddSubntwk('Cas07', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas07'), 'name', 'Cas07');
exec AddSubntwkParm(SubntwkId('Cas07'), 'manufacturer', 'Microsoft');
exec AddSubntwkParm(SubntwkId('Cas07'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas07'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas07'), 'assume_preloaded_stb_flag', 'N');
exec AddSubntwkParm(SubntwkId('Cas07'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas07'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas07'), 'vod_authorization_handle', '0');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas07'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------
--- CAS08 ----
--------------
exec AddSubntwk('Cas08', SubntwkTyp('video_cas'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Cas08'), 'name', 'Cas08');
exec AddSubntwkParm(SubntwkId('Cas08'), 'manufacturer', 'Microsoft');
exec AddSubntwkParm(SubntwkId('Cas08'), 'model', '6000');
exec AddSubntwkParm(SubntwkId('Cas08'), 'version', '2.1');
exec AddSubntwkParm(SubntwkId('Cas08'), 'assume_preloaded_stb_flag', 'Y');
exec AddSubntwkParm(SubntwkId('Cas08'), 'action_on_delete', 'delete');
exec AddSubntwkParm(SubntwkId('Cas08'), 'collect_purchs_on_upd', 'Y');
exec AddSubntwkParm(SubntwkId('Cas08'), 'vod_authorization_handle', '0');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas08'), TechPlatform('prepaidacc_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Cas08'), TechPlatform('msmediaroom_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------------
---- Vod Server ----
--------------------
---------------
---- VOD05 ----
---------------
exec AddSubntwk('Vod05', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod05'), 'name', 'Vod05');
exec AddSubntwkParm(SubntwkId('Vod05'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod05'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod05'), 'version', '1.0');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod05'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
---------------
---- VOD06 ----
---------------
exec AddSubntwk('Vod06', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod06'), 'name', 'Vod06');
exec AddSubntwkParm(SubntwkId('Vod06'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod06'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod06'), 'version', '1.0');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod06'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

---------------
---- VOD07 ----
---------------
exec AddSubntwk('Vod07', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod07'), 'name', 'Vod07');
exec AddSubntwkParm(SubntwkId('Vod07'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod07'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod07'), 'version', '1.0');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod07'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

---------------
---- VOD08 ----
---------------
exec AddSubntwk('Vod08', SubntwkTyp('vod_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Vod08'), 'name', 'Vod08');
exec AddSubntwkParm(SubntwkId('Vod08'), 'manufacturer', 'Tandberg');
exec AddSubntwkParm(SubntwkId('Vod08'), 'model', '1000');
exec AddSubntwkParm(SubntwkId('Vod08'), 'version', '1.0');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Vod08'), TechPlatform('tandberg_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

------------------------
---- CHANNEL LINEUP ----
------------------------
--------------------
---- CHLINEUP05 ----
--------------------
exec AddSubntwk('ChLineup05', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup05'), 'virtual_channel_map_hdl', '9');

--------------------
---- CHLINEUP06 ----
--------------------
exec AddSubntwk('ChLineup06', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup06'), 'virtual_channel_map_hdl', '9');

--------------------
---- CHLINEUP07 ----
--------------------
exec AddSubntwk('ChLineup07', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup07'), 'virtual_channel_map_hdl', '9');

--------------------
---- CHLINEUP08 ----
--------------------
exec AddSubntwk('ChLineup08', SubntwkTyp('channel_lineup'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('ChLineup08'), 'virtual_channel_map_hdl', '9');

--------------------
---- VIDEO ZONE ----
--------------------
----------------
---- ZONE05 ----
----------------
exec AddSubntwk('Zone05', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone05'), 'name', 'Zone05');
exec AddSubntwkParm(SubntwkId('Zone05'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone05'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone05'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone05'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone05'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone05'), 'timezone_id', '9');
exec AddSubntwkParm(SubntwkId('Zone05'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone05'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone05'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone05'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone05'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone05'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone05'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone05'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone05'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone05'), 'population_id', 'POPID');

----------------
---- Zone06 ----
----------------
exec AddSubntwk('Zone06', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone06'), 'name', 'Zone06');
exec AddSubntwkParm(SubntwkId('Zone06'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone06'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone06'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone06'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone06'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone06'), 'timezone_id', 'GMT_Minus_0500_EasternTimeUSAndCanada');
exec AddSubntwkParm(SubntwkId('Zone06'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone06'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone06'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone06'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone06'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone06'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone06'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone06'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone06'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone06'), 'population_id', 'POPID');

----------------
---- ZONE07 ----
----------------
exec AddSubntwk('Zone07', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone07'), 'name', 'Zone07');
exec AddSubntwkParm(SubntwkId('Zone07'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone07'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone07'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone07'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone07'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone07'), 'timezone_id', '9');
exec AddSubntwkParm(SubntwkId('Zone07'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone07'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone07'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone07'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone07'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone07'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone07'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone07'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone07'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone07'), 'population_id', 'POPID');

----------------
---- ZONE08 ----
----------------
exec AddSubntwk('Zone08', SubntwkTyp('video_zone'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('Zone08'), 'name', 'Zone08');
exec AddSubntwkParm(SubntwkId('Zone08'), 'type', 'Franchise');
exec AddSubntwkParm(SubntwkId('Zone08'), 'emergency_response_zone', 'ERZ');
exec AddSubntwkParm(SubntwkId('Zone08'), 'epg_region', '9');
exec AddSubntwkParm(SubntwkId('Zone08'), 'region_cfg', '9');
exec AddSubntwkParm(SubntwkId('Zone08'), 'fips_code', '9');
exec AddSubntwkParm(SubntwkId('Zone08'), 'timezone_id', 'GMT_Minus_0800_PacificTimeUSAndCanada_Tijuana');
exec AddSubntwkParm(SubntwkId('Zone08'), 'time_offset_minutes', '123');
exec AddSubntwkParm(SubntwkId('Zone08'), 'day_light_saving', '1');
exec AddSubntwkParm(SubntwkId('Zone08'), 'basic_list_of_entitlements', '100');
exec AddSubntwkParm(SubntwkId('Zone08'), 'post_paid_credit', '100');
exec AddSubntwkParm(SubntwkId('Zone08'), 'post_paid_purchase', '200');
exec AddSubntwkParm(SubntwkId('Zone08'), 'fast_refresh_flag_enabled', 'N');
exec AddSubntwkParm(SubntwkId('Zone08'), 'reportback_backward_tolerance', '14');
exec AddSubntwkParm(SubntwkId('Zone08'), 'reportback_forward_tolerance', '7');
exec AddSubntwkParm(SubntwkId('Zone08'), 'region_key', 'REGKEY00');
exec AddSubntwkParm(SubntwkId('Zone08'), 'population_id', 'POPID');

spool off
