--============================================================================
--    $Id: residential_sip_voice_network.sql,v 1.7 2012/03/14 19:26:44 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Residential SIP Voice Network (Broadsoft)
--============================================================================
spool residential_sip_voice_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Solutions Residential SIP Voice Network (Broadsoft) ----');

---------------------
---- Call Server ----
---------------------
exec AddSubntwk('CMS_BS_RES3', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('CMS_BS_RES4', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

--------------------------------------------------
---- Parameters for Call Server - CMS_BS_RES3 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'login',                    'sigmaAdmin');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'password',                 'sigma123');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'vendor',                   'broadsoft');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'version',                  'R14');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'sp_add_dn',                'N');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES3'), 'secondary_cms_address',    '192.168.1.101');


--------------------------------------------------
---- Parameters for Call Server - CMS_BS_RES4 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'call_agent_id',            '101');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'login',                    'sigmaAdmin');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'password',                 'sigma123');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'vendor',                   'broadsoft');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'version',                  'R14');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'sp_add_dn',                'N');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('CMS_BS_RES4'), 'secondary_cms_address',    '192.168.1.101');

--------------------
---- Voice Mail ----
--------------------
exec AddSubntwk('VM-BS-RES3', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-BS-RES3'), 'org_id_business', '112');
exec AddSubntwk('VM-BS-RES4', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'cos_id_residential', '102');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'cos_id_business', '107');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'org_id', '105');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-BS-RES4'), 'org_id_business', '112');
--------------------------------------------------
---- Links between Voice Mail and Call Server ----
--------------------------------------------------
exec AddLink('VM-BS-RES3_TO_CMS', LinkTyp('logical'), SubntwkId('VM-BS-RES3'), SubntwkId('CMS_BS_RES3'));
exec AddLink('VM-BS-RES4_TO_CMS', LinkTyp('logical'), SubntwkId('VM-BS-RES4'), SubntwkId('CMS_BS_RES4'));

----------------------------------------------------
---- Links between Call Server and Super Region ----
----------------------------------------------------
-------------------
---- Vancouver ----
-------------------
exec AddLink('CMS_TO_SRGN_Vancouver-SR1', LinkTyp('logical'), SubntwkId('CMS_BS_RES3'), SubntwkId('Vancouver-SR1'));

-----------------
---- Burnaby ----
-----------------
exec AddLink('CMS_TO_SRGN_Burnaby-SR1', LinkTyp('logical'), SubntwkId('CMS_BS_RES3'), SubntwkId('Burnaby-SR1'));

------------------
---- Victoria ----
------------------
exec AddLink('CMS_TO_SRGN_Victoria-SR1', LinkTyp('logical'), SubntwkId('CMS_BS_RES4'), SubntwkId('Victoria-SR1'));

------------------------------------------------
---- The links between NAM and Super Region ----
------------------------------------------------
exec AddLink('NAM_SRGN_VANCOUVER-SR1', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Vancouver-SR1'));
exec AddLink('NAM_SRGN_BURNABY-SR1', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Burnaby-SR1'));
exec AddLink('NAM_SRGN_VICTORIA-SR1', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('Victoria-SR1'));


------------------------------
---- Technology Platforms ----
------------------------------
---------------------
---- Call Server ----
---------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_BS_RES3'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_BS_RES4'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

--------------------
---- Voice Mail ----
--------------------
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-BS-RES3'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('VM-BS-RES4'), TechPlatform('ipunity_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

spool off
