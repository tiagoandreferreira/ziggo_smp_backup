--============================================================================
--    $Id: root_network.sql,v 1.7 2010/09/27 19:58:54 jerryc Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Common Definitions for Solution Root Network
--============================================================================
spool root_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Common Definitions for Solution Root Network ----');

----------------------
---- ROOT ELEMENT ----
----------------------
exec AddSubntwk('Solution_Network', SubntwkTyp('root_network'), NULL, TopologyStatus('in_service'));
exec AddRefNtwkRole('cpe');

--------------------------------
---- Network Access Manager ----
--------------------------------
exec AddSubntwk('NAM_BAC', SubntwkTyp('network_access_manager'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('NAM_BAC'), 'cm_fqdn_suffix', 'sigma.net');

exec AddSubntwk('NAM_ALU', SubntwkTyp('network_access_manager'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
exec AddSubntwk('NAM_DPM', SubntwkTyp('network_access_manager'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-----------------------------------------
---- Technology Platform Definitions ----
-----------------------------------------
---exec AddTechPlatform('Baccldap0',        TechPlatformTyp('ciscobac40'));
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_cpe_dhcp_criteria', 'unknown');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_service_provider', 'Sigma');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_modem_dhcp_criteria', 'MGMT');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_mta_dhcp_criteria', 'provisioned-MTA');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'auto_correct_on_add_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'auto_correct_on_chg_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'auto_correct_on_del_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_snmp_cm_read_string', 'none');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_snmp_cm_write_string', 'none');
---exec AddTechPlatformParm(TechPlatformId('Baccldap0'), 'default_snmp_mta_write_string', 'none');

---exec AddTechPlatform('Dpm_01',           TechPlatformTyp('sigmadpm10'));
---exec AddTechPlatformParm(TechPlatformId('Dpm_01'), 'auto_correct_on_add_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Dpm_01'), 'auto_correct_on_chg_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Dpm_01'), 'auto_correct_on_del_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Dpm_01'), 'default_snmp_cm_read_string', 'none');
---exec AddTechPlatformParm(TechPlatformId('Dpm_01'), 'default_snmp_cm_write_string', 'none');
---exec AddTechPlatformParm(TechPlatformId('Dpm_01'), 'default_snmp_mta_write_string', 'none');


---exec AddTechPlatform('Alu5529_01',       TechPlatformTyp('alu5529apc'));
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'auto_correct_on_add_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'auto_correct_on_chg_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'auto_correct_on_del_failure', 'Y');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'ont_tmplt_nm', 'G7342_FG461_ONT_O-210E-B');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'video_tmplt_nm', 'G7342_FG461_ONT_O-210E-B_TV');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'internet_tmplt_nm', 'G7342_FG461_ONT_O-210E-B_HSI');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'voip_tmplt_nm', 'G7342_FG461_ONT_O-210E-B_FTP_VOIP_SERVICE_1');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'sip_line_tmplt_nm', 'G7342_FG461_ONT_O-210E-B_POTS_SIP_1');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'default_downstream_bw_profile', 'T3_CIR0_AIR100_EIR1050_DT4ms');
---exec AddTechPlatformParm(TechPlatformId('Alu5529_01'), 'default_upstream_bw_profile', 'T3_CIR0_AIR100_EIR1050_DT4ms');

---exec AddTechPlatform('nortel_cs2000_01', TechPlatformTyp('nortelsuccessionsn06x'));

---exec AddTechPlatform('ipunity_01', TechPlatformTyp('ipunityharmony2x'));
---exec AddTechPlatformParm(TechPlatformId('ipunity_01'), 'access_mgmt_user', 'unknown');
---exec AddTechPlatformParm(TechPlatformId('ipunity_01'), 'access_mgmt_pswd', 'unknown');
---exec AddTechPlatformParm(TechPlatformId('ipunity_01'), 'access_mgmt_ip_addr', 'unknown');

---exec AddTechPlatform('level_three_01',   TechPlatformTyp('l3_01'));
---exec AddTechPlatform('sprint_01',        TechPlatformTyp('sprint'));
---exec AddTechPlatform('broadsoft_01',     TechPlatformTyp('broadsoft'));

exec AddTechPlatform('siemenshiq8000_01', TechPlatformTyp('siemenshiq8000'));
---exec AddTechPlatform('surgemail_01', TechPlatformTyp('surgemail'));


---------------------------------------------
---- Technology Platform Management Mode ----
---------------------------------------------
---exec AddTechPlatformMgmtMode(TechPlatform('Baccldap0'),        MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('nortel_cs2000_01'), MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('ipunity_01'),       MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('level_three_01'),   MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('sprint_01'),        MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('Alu5529_01'),       MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('Dpm_01'),           MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('broadsoft_01'),     MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('siemenshiq8000_01'), MgmtMode('provision'));
---exec AddTechPlatformMgmtMode(TechPlatform('surgemail_01'), MgmtMode('provision'));

------------------------------
---- Technology Platforms ----
------------------------------
--------------------------------
---- Network Access Manager ----
--------------------------------
---Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
---Values (SubntwkId('NAM_BAC'), TechPlatform('Baccldap0'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
---Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
---Values (SubntwkId('NAM_ALU'), TechPlatform('Alu5529_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
---Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
---Values (SubntwkId('NAM_DPM'), TechPlatform('Dpm_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

spool off
