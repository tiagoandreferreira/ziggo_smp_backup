	--============================================================================
--    $Id: load_ziggo_ufo_topology.sql,v 1.1 2013/06/20 Exp $
--    $Id: ufo_topology.sql,v 1.2 2013/06/28 Jalay Exp $
-- $Id: ufo_topology.sql,v 1.3 2013/09/24 Jalay Exp $ Updated to make Topology_status_id for BS to 2
--  REVISION HISTORY

--============================================================================
		spool ufo_topology_combined.log
		set escape on
		set serveroutput on
	------------------------------------------------------------------------------------------------------------------------------
	--						   ENTRIES FOR VOICEMAIL (START)																	--
	------------------------------------------------------------------------------------------------------------------------------

	-- entries for adding techplatform
		
	exec AddTechPlatform('comsys', TechPlatformTyp('comsys'));
		
	-- entries for adding prms for techplatform 'comsys' 
		
	exec AddTechPlatformParm(TechPlatformId('comsys'), 'auto_correct_on_add_failure', 'Y');
	exec AddTechPlatformParm(TechPlatformId('comsys'), 'auto_correct_on_chg_failure', 'Y');
	exec AddTechPlatformParm(TechPlatformId('comsys'), 'auto_correct_on_del_failure', 'Y');
	
	-- entries for adding mgmt mode for techplatform
	
	exec AddTechPlatformMgmtMode(TechPlatform('comsys'), MgmtMode('provision'));
		
	-- entries for adding SubNetwork
		
	exec AddSubntwk('VMSIRES2', SubntwkTyp('vmail'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
		
	-- Entries for adding subnetwork parms for newly added subnetwork 'VMSIRES2' 
	EXECUTE DBMS_OUTPUT.PUT_LINE('---- Adding Parm for VMSIRES2----');	
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'cos_id_residential', '102');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'cos_id_business', '107');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'default_phone_type', '1');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'default_reuse_acct', 'No');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'org_id', '105');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'vm_pilot_residential', '2002006245');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'vm_pilot_business', '2002016245');
	exec AddSubntwkParm(SubntwkId('VMSIRES2'), 'org_id_business', '112');
	
	-- Entries for adding subnetwork for newly added subnetwork for 'call server' 
	EXECUTE DBMS_OUTPUT.PUT_LINE('---- Add subnetwork for (Broadsoft and HIQ) ----');

	exec AddSubntwk('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('SP_AS_1_10', SubntwkTyp('call_server'), SubntwkId('Ziggo_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('SP_AS_2_10', SubntwkTyp('call_server'), SubntwkId('Ziggo_Network'), TopologyStatus('in_service'));
	exec AddSubntwk('SP_AS_3_10', SubntwkTyp('call_server'), SubntwkId('Ziggo_Network'), TopologyStatus('in_service'));
	
	
	-- entries for mapping subnetworks to techplatforms
	EXECUTE DBMS_OUTPUT.PUT_LINE('---- Mapping subnetworks to techplatforms----');
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-SI-RES3'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-SI-RES2'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-SI-RES1'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-NT-RES3'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-NT-RES2'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-NT-RES1'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-NT-INC1'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-ND-RES3'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VM-ND-COM1'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VMSIRES2'), TechPlatform('comsys'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
	
	-- entries for adding link
	EXECUTE DBMS_OUTPUT.PUT_LINE('---- Adding link between voice mail and Call Server----');
	
	exec AddLink('VM-SI-RES2N_TO_CMS', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS_SI_RES2'));
	exec AddLink('VM-SI-RES2N_TO_CMS05N1GN', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS05N2GN', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS01N1MND', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS01N2MND', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS02N1MND', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS02N2MND', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS01N1GV', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS01N2GV', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS02N1GV', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS02N2GV', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS05N1TB', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS05N2TB', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS06N1TB', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS06N2TB', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS07N1TB', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_CMS07N2TB', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'));
	exec AddLink('VM-SI-RES2N_TO_SP_AS_1_10', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('SP_AS_1_10'));
	exec AddLink('VM-SI-RES2N_TO_SP_AS_2_10', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('SP_AS_2_10'));
	exec AddLink('VM-SI-RES2N_TO_SP_AS_3_10', LinkTyp('logical'), SubntwkId('VMSIRES2'), SubntwkId('SP_AS_3_10'));
	
	
		exec AddLink('VM-SI-RES3-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-SI-RES3'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-SI-RES3-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-SI-RES3'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-SI-RES3-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-SI-RES3'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-SI-RES2-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-SI-RES2'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-SI-RES2-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-SI-RES2'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-SI-RES2-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-SI-RES2'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-SI-RES1-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-SI-RES1'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-SI-RES1-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-SI-RES1'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-SI-RES1-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-SI-RES1'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-NT-RES3-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-NT-RES3'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-NT-RES3-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-NT-RES3'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-NT-RES3-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-NT-RES3'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-NT-RES2-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-NT-RES2'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-NT-RES2-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-NT-RES2'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-NT-RES2-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-NT-RES2'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-NT-RES1-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-NT-RES1'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-NT-RES1-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-NT-RES1'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-NT-RES1-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-NT-RES1'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-NT-INC1-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-NT-INC1'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-NT-INC1-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-NT-INC1'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-NT-INC1-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-NT-INC1'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-ND-COM1-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-ND-COM1'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-ND-COM1-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-ND-COM1'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-ND-COM1-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-ND-COM1'), SubntwkId('SP_AS_3_10'));

exec AddLink('VM-ND-RES3-SP_AS_1_10', LinkTyp('logical'), SubntwkId('VM-ND-RES3'), SubntwkId('SP_AS_1_10'));
exec AddLink('VM-ND-RES3-SP_AS_2_10', LinkTyp('logical'), SubntwkId('VM-ND-RES3'), SubntwkId('SP_AS_2_10'));
exec AddLink('VM-ND-RES3-SP_AS_3_10', LinkTyp('logical'), SubntwkId('VM-ND-RES3'), SubntwkId('SP_AS_3_10'));

	
	
	------------------------------------------------------------------------------------------------------------------------------
	--						   ENTRIES FOR CISCOBAC (START)																		--
	------------------------------------------------------------------------------------------------------------------------------
	

	-- entries for adding techplatform
	
	exec AddTechPlatform('ciscobac40',TechPlatformTyp('ciscobac40'));
	
    -- entries for tech_platformmgmtmode
	
	exec AddTechPlatformMgmtMode(TechPlatform('ciscobac40'),MgmtMode('provision'));
	
	-- entries for adding prms for techplatform 
	EXECUTE DBMS_OUTPUT.PUT_LINE('---- Add Subnetwork Parm for (Ciscobac) ----');
	
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_cpe_dhcp_criteria', 'unknown');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_service_provider', 'Sigma');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_modem_dhcp_criteria', 'MGMT');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_mta_dhcp_criteria', 'provisioned-MTA');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'auto_correct_on_add_failure', 'Y');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'auto_correct_on_chg_failure', 'Y');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'auto_correct_on_del_failure', 'Y');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_snmp_cm_read_string', 'none');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_snmp_cm_write_string', 'none');
	exec AddTechPlatformParm(TechPlatformId('ciscobac40'), 'default_snmp_mta_write_string', 'none');
	
	-- update existing subnetwork NAM_DPM to NAMDPM 
	
	UPDATE SUBNTWK set subntwk_nm = 'NAMDPM' WHERE subntwk_nm = 'NAM_DPM';
	UPDATE SUBNTWK set subntwk_nm = 'NAMBAC' WHERE subntwk_nm = 'NAM_BAC';
	UPDATE SUBNTWK set subntwk_nm = 'NAMALU' WHERE subntwk_nm = 'NAM_ALU';
	
	--Adding Subnetwork Parm
	
	exec AddSubntwkParm(SubntwkId('NAMDPM'), 'cm_fqdn_suffix', 'sigma.net');
	exec AddSubntwkParm(SubntwkId('NAMDPM'), 'rdu_list','NAMDPM,NAMBAC,NAMALU');
	
	exec AddSubntwkParm(SubntwkId('NAMBAC'), 'cm_fqdn_suffix', 'sigma.net');
	exec AddSubntwkParm(SubntwkId('NAMBAC'), 'rdu_list','NAMDPM,NAMBAC,NAMALU');
	
	exec AddSubntwkParm(SubntwkId('NAMALU'), 'cm_fqdn_suffix', 'sigma.net');
	exec AddSubntwkParm(SubntwkId('NAMALU'), 'rdu_list','NAMDPM,NAMBAC,NAMALU');
	
	
	---Insert into sbnttechplatform
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('NAMDPM'), TechPlatform('ciscobac40'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	--Adding Links
	
	exec AddLink('NAM_NAD_VAN13', LinkTyp('logical'), SubntwkId('NAMDPM'), SubntwkId('Vancouver4'));
	
	
	
	------------------------------------------------------------------------------------------------------------------------------
	--						   ENTRIES FOR ZIGGONIS (START)																		--
	------------------------------------------------------------------------------------------------------------------------------
	
	
	
	-- entries for adding techplatform
	
	exec AddTechPlatform('ziggonis_01', TechPlatformTyp('ziggonis'));

	-- entries for adding mgmt mode for techplatform

	exec AddTechPlatformMgmtMode(TechPlatform('ziggonis_01'), MgmtMode('provision'));
	
	---Insert into sbnttechplatform
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('NAMDPM'), TechPlatform('ziggonis_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
	

---------------------------------------------
---- Call Server ----
---------------------------------------------

-----------------------------------------
---- Technology Platform Definitions ----
-----------------------------------------

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Adding Tech Paltform as Broadsoft ----');


exec AddTechPlatform('broadsoft_01',     TechPlatformTyp('broadsoft'));


---------------------------------------------
---- Technology Platform Management Mode ----
---------------------------------------------

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Adding Tech Paltform Management mode as Broadsoft ----');


exec AddTechPlatformMgmtMode(TechPlatform('broadsoft_01'),     MgmtMode('provision'));


---------------------------------------------
---- Subnetwork Technology Platform ----
---------------------------------------------

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Adding Subnetowrk Tech Paltform ----');

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_1_10'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_2_10'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_3_10'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_3_10'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_2_10'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('SP_AS_1_10'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('broadsoft_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

------------------------------------------------
---- Links between Call Server and Super Region ----
----------------------------------------------------

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Adding link between Call Server(Broadsoft) and Super Region ----');

exec AddLink('SP_AS_1_10_default-SR', LinkTyp('logical'), SubntwkId('SP_AS_1_10'), SubntwkId('default-SR'));
exec AddLink('SP_AS_2_10_default-SR', LinkTyp('logical'), SubntwkId('SP_AS_2_10'), SubntwkId('default-SR'));
exec AddLink('SP_AS_3_10_default-SR', LinkTyp('logical'), SubntwkId('SP_AS_3_10'), SubntwkId('default-SR'));
exec AddLink('NAD_CS_default_CMS05N1GN', LinkTyp('logical'), SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS05N2GN', LinkTyp('logical'), SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS01N1MND', LinkTyp('logical'), SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS01N2MND', LinkTyp('logical'), SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS02N1MND', LinkTyp('logical'), SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS02N2MND', LinkTyp('logical'), SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS01N1GV', LinkTyp('logical'), SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS01N2GV', LinkTyp('logical'), SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS02N1GV', LinkTyp('logical'), SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS02N2GV', LinkTyp('logical'), SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS05N1TB', LinkTyp('logical'), SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS05N2TB', LinkTyp('logical'), SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS06N1TB', LinkTyp('logical'), SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS06N2TB', LinkTyp('logical'), SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS07N1TB', LinkTyp('logical'), SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_CMS07N2TB', LinkTyp('logical'), SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), SubntwkId('default_NAD'));



--Parameters added for Load Balancing Bean
EXECUTE DBMS_OUTPUT.PUT_LINE('---- Add Subnetwork Parm for (SiemensHiQ8000) ----');
--#############################################################################################################
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'num_of_lines_provisioned',      '0');

exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'num_of_lines_provisioned',      '0');

exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'num_of_lines_provisioned',      '0');

exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'num_of_lines_provisioned',      '0');

exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'num_of_lines_provisioned',      '0');

exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'num_of_lines_provisioned',      '0');

exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'vendor',      'HiQ');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'in_service',      'y');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'num_of_lines_provisioned',      '974');

--Additional Parameters added for HIQ

exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1001');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '6');


exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1002');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '6');


exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1003');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '1');


exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1004');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '1');


exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1005');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '2');


exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1006');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '2');


exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1007');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '3');


exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1008');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '3');


exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1009');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '4');


exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1010');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '4');


exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1011');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '5');


exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1012');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '5');

exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1013');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '8');


exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1014');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '8');


exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1015');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '7');


exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ac_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_acr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ar_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_aul_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_3_1k',    'false');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_56k',     'false');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_64k',     'false');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_bearer_speech',  'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfbl_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_activateable',  'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfda_sub_dn_controllable',       'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_court_call', 'NoCourtesyCall');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_notify_act', 'false');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_activateable',   'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cfv_sub_dn_controllable',        'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_active',    'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnam_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_active',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cnd_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_cot_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_drcw_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_billing_option',    'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_eacr_sub_controlled',    'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_echo_cancellation',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_fax_mode',       'G711');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ike_prof_name',  'Default');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_conn_type',   'UDP');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_max_sessions',        '3');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_realm',       'upcsk');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_reg_type',    'Dynamic');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_scheme',      'digest-authentication');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_line_type',      'POTS-LINE');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_long_tran_timer',        '5');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_activity_count',     '0');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_max_timer',      '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_mgc_ip_assignment',      '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_level_controllable', 'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ocb_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_qos_profile',    '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_retry_value',    '0');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtcp_cipher_suite',      '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_cipher_suite',       '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_rtp_security_mode',      '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_sca_term_treat', 'ScaTerminationDenialAnnouncement');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_black_list', 'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scf_splash_ring',        'false');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_billing_option',     'FlatRate');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_scr_sub_controlled',     'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_security_name',  '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_silence_suppression',    'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_spcall_sub_controlled',  'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timeout',        '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_hist',     '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t1',       '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t7',       '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_timer_t8',       '');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_vm_active',      'true');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_wml_sub_activatable',    'All');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_address',    'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'primary_cms_proxy_addr', 'sk-bts01a-sipproxy.chello.sk');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'default_ip_addr_alloc',  'GwIPAddrMethodDNSQuery');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'profile_key', '1016');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), 'grp_id', '7');


--#############################################################################################################
EXECUTE DBMS_OUTPUT.PUT_LINE('---- Add Subnetwork Parm for (Broadsoft) ----');
--------------------------------------------------
---- Parameters for Call Server - SP_AS_1_10 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'call_agent_id',            '102');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'login',                    'sigmaAdmin');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'password',                 'sigma123');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'vendor',                   'Broadsoft');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'version',                  '1800');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'sp_add_dn',                'N');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'secondary_cms_address',    '192.168.1.101');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'in_service',               'n');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'num_of_lines_provisioned', '969');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'profile_key', '1017');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('SP_AS_1_10'), 'grp_id', '9');

--------------------------------------------------
---- Parameters for Call Server - SP_AS_2_10 ----
--------------------------------------------------

exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'call_agent_id',            '102');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'login',                    'sigmaAdmin');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'password',                 'sigma123');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'vendor',                   'Broadsoft');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'version',                  '1800');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'sp_add_dn',                'N');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'secondary_cms_address',    '192.168.1.101');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'in_service',               'n');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'num_of_lines_provisioned', '965');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'profile_key', '1018');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('SP_AS_2_10'), 'grp_id', '10');

--------------------------------------------------
---- Parameters for Call Server - SP_AS_3_10 ----
--------------------------------------------------
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'call_agent_id',            '102');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'login',                    'sigmaAdmin');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'password',                 'sigma123');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'vendor',                   'Broadsoft');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'version',                  '1800');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'sp_add_dn',                'N');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'primary_cms_address',      '10.0.210.83');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'secondary_cms_address',    '192.168.1.101');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'in_service',               'n');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'num_of_lines_provisioned', '960');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'logical_sim_ring_dn_prefix',      '9799');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'profile_key', '1019');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'announcement',  'announcement');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'lrn_nadi', '3');
exec AddSubntwkParm(SubntwkId('SP_AS_3_10'), 'grp_id', '11');


exec AddSubntwkParm(SubntwkId('ICC_HiqInstance_1'), 'vendor',      'unknown');
exec AddSubntwkParm(SubntwkId('ICC_HiqInstance_1'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('ICC_HiqInstance_1'), 'num_of_lines_provisioned',      '970');
exec AddSubntwkParm(SubntwkId('ICC_HiqInstance_1'), 'grp_id', '12');

exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'vendor',      'unknown');
exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'in_service',      'n');
exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'num_of_lines_provisioned',      '970');
exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'grp_id', '13');



	------------------------------------------------------------------------------------------------------------------------------
	--						ENTRIES FOR Notification (START) 																	--
	------------------------------------------------------------------------------------------------------------------------------
	
	
	-- entries for adding techplatform
	
	exec AddTechPlatform('notification_01', TechPlatformTyp('notification'));
		
	-- entries for tech_platformmgmtmode
	
	exec AddTechPlatformMgmtMode(TechPlatform('notification_01'), MgmtMode('provision'));
	
	---Insert into sbnttechplatform
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('NAMDPM'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);
	
	Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
	Values (SubntwkId('VMSIRES2'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

	
	-- Insert values of HIQ and BroadSoft Instances to Notification Tech Platform
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS05N1GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS05N2GN.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS01N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS01N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS02N1MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS02N2MND.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS01N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS01N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

                
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS02N1GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS02N2GV.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS05N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS05N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS06N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS06N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS07N1TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('CMS07N2TB.TELEFONIE.ZIGGO.LOCAL'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


	
Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('SP_AS_1_10'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('SP_AS_2_10'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by) Values (SubntwkId('SP_AS_3_10'), TechPlatform('notification_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);


--------------------------------------------------

update subntwk set topology_status_id = 3 where subntwk_typ_id = (select subntwk_typ_id from ref_subntwk_typ where subntwk_typ_nm = 'call_server');

update subntwk set topology_status_id = 2 where subntwk_typ_id = (select subntwk_typ_id from ref_subntwk_typ where subntwk_typ_nm = 'call_server') and (subntwk_nm like '%TELEFONIE.ZIGGO.LOCAL' or subntwk_nm like 'Breezz_PortaOne%' or subntwk_nm like 'ICC_HiqInstance%' or subntwk_nm like 'SP_AS_%');
	
EXECUTE DBMS_OUTPUT.PUT_LINE('---- UFO Topology loaded successfuly ----');
commit;

spool off
exit;
	