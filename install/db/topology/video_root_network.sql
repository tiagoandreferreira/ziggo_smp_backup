--============================================================================
--    $Id: video_root_network.sql,v 1.3 2010/09/14 15:53:09 zhenl Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--  KTA 09 DEC 2009
--  Common definition for Video Topology
--============================================================================
spool video_root_network.log

set escape on
set serveroutput on

EXECUTE DBMS_OUTPUT.PUT_LINE('---- Common Definitions for Solution Video Network ----');

------------------------------------
---- Video Technology Platforms ----
------------------------------------
exec AddTechPlatform('motoroladac_01', TechPlatformTyp('motoroladac6000'));
exec AddTechPlatformParm(TechPlatformId('motoroladac_01'), 'business_system_code', '123');
exec AddTechPlatformParm(TechPlatformId('motoroladac_01'), 'auto_correct_on_add_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('motoroladac_01'), 'auto_correct_on_chg_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('motoroladac_01'), 'auto_correct_on_del_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('motoroladac_01'), 'template_error_handling_behavior', 'manual');

exec AddTechPlatform('ciscodncs_01', TechPlatformTyp('ciscodncs'));
exec AddTechPlatformParm(TechPlatformId('ciscodncs_01'), 'boot_term_pgname', '123');
exec AddTechPlatformParm(TechPlatformId('ciscodncs_01'), 'auto_correct_on_add_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ciscodncs_01'), 'auto_correct_on_chg_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ciscodncs_01'), 'auto_correct_on_del_failure', 'Y');
exec AddTechPlatformParm(TechPlatformId('ciscodncs_01'), 'template_error_handling_behavior', 'manual');

exec AddTechPlatform('tandberg_01', TechPlatformTyp('tandbergbms'));
exec AddTechPlatformParm(TechPlatformId('tandberg_01'), 'template_error_handling_behavior', 'manual');

exec AddTechPlatform('prepaidacc_01', TechPlatformTyp('prepaidacc'));
exec AddTechPlatform('vstagupdate_01', TechPlatformTyp('vstagupdate'));
exec AddTechPlatform('msmediaroom_01', TechPlatformTyp('msmediaroom'));

---------------------------------------------
---- Technology Platform Management Mode ----
---------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---- Technology Platform Management Mode ----');

exec AddTechPlatformMgmtMode(TechPlatform('motoroladac_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('ciscodncs_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('tandberg_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('prepaidacc_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('vstagupdate_01'), MgmtMode('provision'));
exec AddTechPlatformMgmtMode(TechPlatform('msmediaroom_01'), MgmtMode('provision'));

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Solution_Network'), TechPlatform('vstagupdate_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

spool off
