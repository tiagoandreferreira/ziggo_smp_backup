-- Default CMS instance data

exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES1'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_SI_RES2'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMSOT1N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMSOT1N1'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMSOT1N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMSOT1N2'), 'logical_sim_ring_dn_prefix',      '979');

-- New HiQ instance data
exec AddSubntwk('CMSOT1N1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMSOT1N2', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_HIQ1_N1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_HIQ1_N2', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_HIQ2_N1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_HIQ2_N2', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_HIQ3_N1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('CMS_HIQ3_N2', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('Breezz_PortaOne_1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

exec AddSubntwk('ICC_HiqInstance_1', SubntwkTyp('call_server'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));

-- Parms added for new HiQ instance data

exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N1'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N1'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N1'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ1_N2'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ2_N2'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('CMS_HIQ3_N2'), 'logical_sim_ring_dn_prefix',      '979');

exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'technology',      'SIP');
exec AddSubntwkParm(SubntwkId('Breezz_PortaOne_1'), 'logical_sim_ring_dn_prefix','979');

exec AddSubntwkParm(SubntwkId('ICC_HiqInstance_1'), 'technology',      'PacketCable');
exec AddSubntwkParm(SubntwkId('ICC_HiqInstance_1'), 'logical_sim_ring_dn_prefix','979');

-- Technology Platform Definition & Technology Platform Management Mode

exec AddTechPlatform('portaone_01', TechPlatformTyp('portaone'));
exec AddTechPlatformMgmtMode(TechPlatform('portaone_01'), MgmtMode('provision'));

exec AddTechPlatform('hiqinstance_01', TechPlatformTyp('hiqinstancedetails'));
exec AddTechPlatformMgmtMode(TechPlatform('hiqinstance_01'), MgmtMode('provision'));

-- Technology Platforms added for new HiQ instance data

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_HIQ1_N1'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_HIQ1_N2'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_HIQ2_N1'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_HIQ2_N2'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_HIQ3_N1'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('CMS_HIQ3_N2'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Breezz_PortaOne_1'), TechPlatform('portaone_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Breezz_PortaOne_1'), TechPlatform('hiqinstance_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

Insert into Sbnt_Tech_Platform(subntwk_id, tech_platform_id, mgmt_mode_id, created_dtm, created_by, modified_dtm, modified_by)
Values (SubntwkId('Breezz_PortaOne_1'), TechPlatform('siemenshiq8000_01'), MgmtMode('provision'), SYSDATE, 'INIT', NULL, NULL);

----------------------
---- Super Region ----
----------------------
exec AddSubntwk('default-SR', SubntwkTyp('super_region'), SubntwkId('Solution_Network'), TopologyStatus('in_service'));
----------------
----------------
---- Region ----
----------------
exec AddSubntwk('default', SubntwkTyp('region'), SubntwkId('default-SR'), TopologyStatus('in_service'));
exec AddSubntwkParm(SubntwkId('default'), 'clec_supplied_e911', 'Y');

--------------------------------
---- Network Access Device -----
--------------------------------
exec AddSubntwk('default_NAD', SubntwkTyp('network_access_device'), SubntwkId('default-SR'), TopologyStatus('in_service'));
--------------------------------------
---- Links between NAD and Region ----
--------------------------------------
exec AddLink('NAD_REGION_default', LinkTyp('logical'), SubntwkId('default_NAD'), SubntwkId('default'));

exec AddLink('NAD_CS_default', LinkTyp('logical'), SubntwkId('Breezz_PortaOne_1'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_ICC', LinkTyp('logical'), SubntwkId('ICC_HiqInstance_1'), SubntwkId('default_NAD'));

exec AddLink('NAD_CS_default_HIQ1', LinkTyp('logical'), SubntwkId('CMSOT1N1'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ2', LinkTyp('logical'), SubntwkId('CMSOT1N2'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ1', LinkTyp('logical'), SubntwkId('CMS_SI_RES1'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ2', LinkTyp('logical'), SubntwkId('CMS_SI_RES2'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ3', LinkTyp('logical'), SubntwkId('CMS_HIQ1_N1'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ4', LinkTyp('logical'), SubntwkId('CMS_HIQ1_N2'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ5', LinkTyp('logical'), SubntwkId('CMS_HIQ2_N1'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ6', LinkTyp('logical'), SubntwkId('CMS_HIQ2_N2'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ7', LinkTyp('logical'), SubntwkId('CMS_HIQ3_N1'), SubntwkId('default_NAD'));
exec AddLink('NAD_CS_default_HIQ8', LinkTyp('logical'), SubntwkId('CMS_HIQ3_N2'), SubntwkId('default_NAD'));

-----------------------------
---- Parameters for NADs ----
-----------------------------
exec AddSubntwkParm(SubntwkId('default_NAD'), 'desc',                'default');
exec AddSubntwkParm(SubntwkId('default_NAD'), 'dpm_location',        'default');

-------------------------------------
---- Gateway Controller Clusters ----
-------------------------------------
exec AddSubntwk('GWCC-default-CL1', SubntwkTyp('gateway_controller_cluster'), SubntwkId('Breezz_PortaOne_1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL2', SubntwkTyp('gateway_controller_cluster'), SubntwkId('ICC_HiqInstance_1'), TopologyStatus('in_service'));

exec AddSubntwk('GWCC-default-CL3', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_SI_RES1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL4', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_SI_RES2'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL5', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_HIQ1_N1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL6', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_HIQ1_N2'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL7', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_HIQ2_N1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL8', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_HIQ2_N2'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL9', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_HIQ3_N1'), TopologyStatus('in_service'));
exec AddSubntwk('GWCC-default-CL10', SubntwkTyp('gateway_controller_cluster'), SubntwkId('CMS_HIQ3_N2'), TopologyStatus('in_service'));

-----------------------------
---- Gateway Controllers ----
-----------------------------
exec AddSubntwk('GWC-default-CL1-1', SubntwkTyp('gateway_controller'), SubntwkId('Breezz_PortaOne_1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-2', SubntwkTyp('gateway_controller'), SubntwkId('ICC_HiqInstance_1'), TopologyStatus('in_service'));

exec AddSubntwk('GWC-default-CL1-3', SubntwkTyp('gateway_controller'), SubntwkId('CMS_SI_RES1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-4', SubntwkTyp('gateway_controller'), SubntwkId('CMS_SI_RES2'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-5', SubntwkTyp('gateway_controller'), SubntwkId('CMS_HIQ1_N1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-6', SubntwkTyp('gateway_controller'), SubntwkId('CMS_HIQ1_N2'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-7', SubntwkTyp('gateway_controller'), SubntwkId('CMS_HIQ2_N1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-8', SubntwkTyp('gateway_controller'), SubntwkId('CMS_HIQ2_N2'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-9', SubntwkTyp('gateway_controller'), SubntwkId('CMS_HIQ3_N1'), TopologyStatus('in_service'));
exec AddSubntwk('GWC-default-CL1-10', SubntwkTyp('gateway_controller'), SubntwkId('CMS_HIQ3_N2'), TopologyStatus('in_service'));

------------------------------------
---- Links between GWCC and GWC ----
------------------------------------
exec AddLink('GWC_GWCC-default-CL1', LinkTyp('logical'), SubntwkId('GWC-default-CL1-1'), SubntwkId('GWCC-default-CL1'));
exec AddLink('GWC_GWCC-default-CL2', LinkTyp('logical'), SubntwkId('GWC-default-CL1-2'), SubntwkId('GWCC-default-CL2'));
exec AddLink('GWC_GWCC-default-CL3', LinkTyp('logical'), SubntwkId('GWC-default-CL1-3'), SubntwkId('GWCC-default-CL3'));
exec AddLink('GWC_GWCC-default-CL4', LinkTyp('logical'), SubntwkId('GWC-default-CL1-4'), SubntwkId('GWCC-default-CL4'));
exec AddLink('GWC_GWCC-default-CL5', LinkTyp('logical'), SubntwkId('GWC-default-CL1-5'), SubntwkId('GWCC-default-CL5'));
exec AddLink('GWC_GWCC-default-CL6', LinkTyp('logical'), SubntwkId('GWC-default-CL1-6'), SubntwkId('GWCC-default-CL6'));
exec AddLink('GWC_GWCC-default-CL7', LinkTyp('logical'), SubntwkId('GWC-default-CL1-7'), SubntwkId('GWCC-default-CL7'));
exec AddLink('GWC_GWCC-default-CL8', LinkTyp('logical'), SubntwkId('GWC-default-CL1-8'), SubntwkId('GWCC-default-CL8'));
exec AddLink('GWC_GWCC-default-CL9', LinkTyp('logical'), SubntwkId('GWC-default-CL1-9'), SubntwkId('GWCC-default-CL9'));
exec AddLink('GWC_GWCC-default-CL10', LinkTyp('logical'), SubntwkId('GWC-default-CL1-10'), SubntwkId('GWCC-default-CL10'));

----------------------------
---- Parameters for GWC ----
----------------------------
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-1'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-1'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-1'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-1'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-1'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-1'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-2'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-2'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-2'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-2'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-2'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-2'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-3'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-3'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-3'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-3'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-3'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-3'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-4'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-4'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-4'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-4'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-4'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-4'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-5'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-5'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-5'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-5'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-5'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-5'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-6'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-6'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-6'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-6'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-6'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-6'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-7'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-7'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-7'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-7'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-7'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-7'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-8'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-8'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-8'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-8'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-8'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-8'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-9'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-9'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-9'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-9'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-9'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-9'), 'skip_load_balance','y');

exec AddSubntwkParm(SubntwkId('GWC-default-CL1-10'), 'Dqos_enabled',     'n');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-10'), 'ep_left',          '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-10'), 'gwc_name',         'GWC-default');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-10'), 'max_no_line_supp', '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-10'), 'resv_left',        '9999999');
exec AddSubntwkParm(SubntwkId('GWC-default-CL1-10'), 'skip_load_balance','y');

----------------------------------------------------
---- Links between Call Server and Super Region ----
----------------------------------------------------
-------------------
---- Vancouver ----
-------------------
exec AddLink('GWCC_TO_NAD_default', LinkTyp('logical'), SubntwkId('GWCC-default-CL1'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default1', LinkTyp('logical'), SubntwkId('GWCC-default-CL2'), SubntwkId('default-SR'));

exec AddLink('GWCC_TO_NAD_default2', LinkTyp('logical'), SubntwkId('GWCC-default-CL3'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default3', LinkTyp('logical'), SubntwkId('GWCC-default-CL4'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default4', LinkTyp('logical'), SubntwkId('GWCC-default-CL5'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default5', LinkTyp('logical'), SubntwkId('GWCC-default-CL6'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default6', LinkTyp('logical'), SubntwkId('GWCC-default-CL7'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default7', LinkTyp('logical'), SubntwkId('GWCC-default-CL8'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default8', LinkTyp('logical'), SubntwkId('GWCC-default-CL9'), SubntwkId('default-SR'));
exec AddLink('GWCC_TO_NAD_default9', LinkTyp('logical'), SubntwkId('GWCC-default-CL10'), SubntwkId('default-SR'));

------------------------------------------------
---- The links between NAM and Super Region ----
------------------------------------------------
exec AddLink('NAM_NAD_default-SR', LinkTyp('logical'), SubntwkId('NAM_DPM'), SubntwkId('default-SR'));



commit;
exit;






