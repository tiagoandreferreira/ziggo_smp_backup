--============================================================================
--    $Id: ziggo_topology.sql,v 1.3 2011/12/15 12:16:09 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
spool ziggo_topology.log

set escape on
set serveroutput on

 -- add root_network
dbms_output.put_line('----- adding root_network: Ziggo_Network -----');

exec AddSubntwk('Ziggo_Network', SubntwkTyp('root_network'), NULL, TopologyStatus('in_service'));

-- add voice_mal
 dbms_output.put_line('----- adding voice mail ---');

exec AddSubntwk('VM-SI-RES3', SubntwkTyp('vmail'), SubntwkId('Ziggo_Network'), TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'default_cos_id', '116');
exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'default_phone_type', '1');
exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'default_reuse_acct', 'No');
exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'org_id', '112');
exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'vm_pilot_residential', '2002006245');
exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'vm_pilot_business', '2002016245');
exec AddSubntwkParm(SubntwkId('VM-SI-RES3'), 'org_id_business', '114');

-- add mobile_delivery_point
 dbms_output.put_line('----- mobile_delivery_point ---');

exec AddSubntwk('mobile_dp', SubntwkTyp('mobile_delivery_point'), SubntwkId('Ziggo_Network'), TopologyStatus('in_service'));

exec AddSubntwkParm(SubntwkId('mobile_dp'), 'lrn_ref', '116');
exec AddSubntwkParm(SubntwkId('mobile_dp'), 'lrn_nadi', '1');
exec AddSubntwkParm(SubntwkId('mobile_dp'), 'default_gprs_defn', 'No');
exec AddSubntwkParm(SubntwkId('mobile_dp'), 'default_telephony_profileid', '1121');

exec AddLink('VM-SI-RES3_to_mobile_dp', LinkTyp('logical'), SubntwkId('VM-SI-RES3'),SubntwkId('mobile_dp'));
commit;

UPDATE SUBNTWK_PARM
SET VAL=3
WHERE PARM_ID=(SELECT PARM_ID FROM PARM WHERE PARM_NM='lrn_nadi');
commit;

UPDATE SUBNTWK_PARM
SET VAL='C1200'
WHERE PARM_ID=(SELECT PARM_ID FROM PARM WHERE PARM_NM='lrn_ref');
commit;

CREATE OR REPLACE FORCE VIEW "V_TN_SEARCH" ("SUB_ID", "TN") AS 
SELECT SB.SUB_ID, SSP.VAL FROM PARM P, SUB_SVC SS, SUB_SVC_PARM SSP, SUB SB, SVC S
WHERE SB.SUB_ID = SS.SUB_ID
AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
AND P.PARM_ID = SSP.PARM_ID
AND S.SVC_ID = SS.SVC_ID
AND P.PARM_NM = 'telephone_number'
AND S.SVC_NM = 'smp_switch_dial_tone_access';
commit;

CREATE OR REPLACE FORCE VIEW "V_SIP_URI_SEARCH" ("SUB_ID", "SUB_ORDR_ID", "CREATED_BY", "ORDR_STATUS", "SIP_URI", "CREATED_DTM", "MODIFIED_DTM", "EXTERNAL_KEY") AS 
  SELECT
       o.sub_id,o.sub_ordr_id, o.created_by,
       s.status,
       a.sip_uri,
       o.created_dtm, o.modified_dtm,o.external_key
  FROM sub_ordr o, ref_status s,
       (SELECT SB.SUB_ID, SSP.VAL sip_uri FROM PARM P, SUB_SVC SS, SUB_SVC_PARM SSP, SUB SB, SVC S
WHERE SB.SUB_ID = SS.SUB_ID
AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
AND P.PARM_ID = SSP.PARM_ID
AND S.SVC_ID = SS.SVC_ID
AND P.PARM_NM = 'sip_uri'
AND S.SVC_NM = 'voip_dial_tone') a
  where o.sub_id = a.sub_id and o.ORDR_STATUS_ID=s.status_id;
commit;

CREATE OR REPLACE FORCE VIEW "V_SVC_PRO_CRM_TYPE_SEARCH" ("SUB_ID", "SVC_PROVIDER_NAME", "CRM_TYPE") AS 
select s.sub_id, sp1.val svc_provider_name, sp2.val crm_type from sub s, sub_parm sp1, sub_parm sp2 
where s.sub_id = sp1.sub_id 
and s.sub_id = sp2.sub_id
and sp1.parm_id = get_parm_id ('svc_provider_name', get_class_id ('SubSpec'))
and sp2.parm_id = get_parm_id ('crm_type', get_class_id ('SubSpec'));
commit;


spool off
