CREATE OR REPLACE VIEW V_CM_MAC_QUERY_SEARCH_NQ
  (SUB_ID, SUB_STATUS, SVC_PROVIDER, SALUTATION, FIRST_NAME, 
 MIDDLE_NAME, LAST_NAME, SUFFIX, ACCOUNT_NO, CM_MAC)
  AS 
SELECT
            a.sub_id,
            f.status,
            fn_get_svc_provider (d.svc_provider_id),
            pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'salutation') salutation,
            pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'first_name') first_name,
            pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'middle_name') middle_name,
            pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'last_name') last_name,
            pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
            pkg_sp_subbrief.get_sub_parm (a.sub_id, 'acct') account_no,
            c.val cm_mac
      FROM  SUB_SVC a, SUB_SVC_PARM c, SUB d, REF_STATUS f
      WHERE d.sub_id = a.sub_id
        AND d.sub_status_id = f.status_id
        AND a.sub_svc_id = c.sub_svc_id
        AND c.parm_id IN (get_parm_id('cm_mac',100,svcid('smp_internet_access')))
        AND a.sub_svc_status_id not in (29, 33);

		
CREATE OR REPLACE FORCE VIEW V_PNUMBER_SEARCH_NQ 
("SUB_ID", "SUB_STATUS", "SVC_PROVIDER", "SALUTATION", "FIRST_NAME", "MIDDLE_NAME", "LAST_NAME", "SUFFIX", "P_NUMBER")
AS
  SELECT a.sub_id,
    b.status sub_status,
    fn_get_svc_provider (a.svc_provider_id) svc_provider,
    pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'salutation' ) salutation,
    pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'first_name') first_name,
    pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'middle_name') middle_name,
    pkg_sp_subbrief.get_sub_contact_parm (a.sub_id,'last_name') last_name,
    pkg_sp_subbrief.get_sub_contact_parm (a.sub_id, 'suffix') suffix,
    sp.val p_number
  FROM sub a,
    ref_status b,
    sub_parm sp
  WHERE a.sub_status_id = b.status_id
  AND b.class_id        = 111
  AND a.sub_typ_id      = 1
  AND sp.parm_id        = get_parm_id ('p_number', get_class_id ('SubSpec'))
  AND sp.sub_id         = a.sub_id;

CREATE OR REPLACE FORCE VIEW V_SIP_URI_SEARCH_QUERY ("SUB_ID", "SIP_URI") AS 
  SELECT SB.SUB_ID,
    SSP.VAL SIP_URI
  FROM PARM P,
    SUB_SVC SS,
    SUB_SVC_PARM SSP,
    SUB SB,
    SVC S
  WHERE SB.SUB_ID   = SS.SUB_ID
  AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
  AND P.PARM_ID     = SSP.PARM_ID
  AND S.SVC_ID      = SS.SVC_ID
  AND P.PARM_NM     = 'sip_uri'
  AND S.SVC_NM      = 'voip_dial_tone'
  AND SS.sub_svc_status_id not in (29, 33);

commit; 

exit
