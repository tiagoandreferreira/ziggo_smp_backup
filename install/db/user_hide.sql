--============================================================================
--    $Id: user_hide.sql,v 1.1 2011/12/09 11:59:02 prithvim Exp $
--  For adding User Entity
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
exec am_add_secure_obj('samp.web.entity.SubUser', 'User');
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.entity.SubUser', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.orders.actions.replicate', 'Replicate order');
exec am_add_privilege('samp.web.orders.actions.replicate', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.orders.actions.replicate', 'y', 'csr_admin', 'edit');
