--============================================================================
--    $Id: version.sql,v 1.1 2011/12/05 16:11:27 prithvim Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
set escape on
set serveroutput on

------------------------------------------------------------------
---------   Data fill for table: Version   ---------
------------------------------------------------------------------
EXECUTE DBMS_OUTPUT.PUT_LINE('---------   Data fill for table: Version   ---------')

begin
 PKG_VERSION.PRC_SET_VER(
    P_MOD_NM => 'SMP Demo Data', 
    P_COMMENT => 'SMP Core Testing Data', 
    P_SETTER => USER,     
    P_SPEC_TITLE => 'SMP',  
    P_SPEC_VENDOR => 'Sigma Systems',  
    P_SPEC_VER => '@major.version@', 
    P_IMPL_TITLE => 'version',  
    P_IMPL_VENDOR => 'Core Eng', 
    P_IMPL_VER  => '@impl.version@', 
    P_BUILT_BY  => '@built.by@', 
    P_CREATED_BY => 'INIT',  
    P_BUILT_DTM => to_date('@built.time@', 'dd/mm/yyyy hh24:mi:ss'));  
end;
/
