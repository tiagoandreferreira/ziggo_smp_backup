CREATE OR REPLACE FORCE VIEW "V_TN_SEARCH" ("SUB_ID", "TN") AS 
SELECT SB.SUB_ID, SSP.VAL FROM PARM P, SUB_SVC SS, SUB_SVC_PARM SSP, SUB SB, SVC S
WHERE SB.SUB_ID = SS.SUB_ID
AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
AND P.PARM_ID = SSP.PARM_ID
AND S.SVC_ID = SS.SVC_ID
AND P.PARM_NM = 'telephone_number'
AND S.SVC_NM = 'smp_switch_dial_tone_access'
AND SS.sub_svc_status_id not in (29, 33);



CREATE OR REPLACE FORCE VIEW "V_SIP_URI_SEARCH" ("SUB_ID", "SUB_ORDR_ID", "CREATED_BY", "ORDR_STATUS", "SIP_URI", "CREATED_DTM", "MODIFIED_DTM", "EXTERNAL_KEY") AS 
  SELECT
       o.sub_id,o.sub_ordr_id, o.created_by,
       s.status,
       a.sip_uri,
       o.created_dtm, o.modified_dtm,o.external_key
  FROM sub_ordr o, ref_status s,
       (SELECT SB.SUB_ID, SSP.VAL sip_uri FROM PARM P, SUB_SVC SS, SUB_SVC_PARM SSP, SUB SB, SVC S
WHERE SB.SUB_ID = SS.SUB_ID
AND SS.SUB_SVC_ID = SSP.SUB_SVC_ID
AND P.PARM_ID = SSP.PARM_ID
AND S.SVC_ID = SS.SVC_ID
AND P.PARM_NM = 'sip_uri'
AND S.SVC_NM = 'voip_dial_tone'
AND SS.sub_svc_status_id not in (29, 33)) a
  where o.sub_id = a.sub_id and o.ORDR_STATUS_ID=s.status_id


CREATE OR REPLACE FORCE VIEW "V_SVC_PRO_CRM_TYPE_SEARCH" ("SUB_ID", "SVC_PROVIDER_NAME", "CRM_TYPE") AS 
select s.sub_id, sp1.val svc_provider_name, sp2.val crm_type from sub s, sub_parm sp1, sub_parm sp2 
where s.sub_id = sp1.sub_id 
and s.sub_id = sp2.sub_id
and sp1.parm_id = get_parm_id ('svc_provider_name', get_class_id ('SubSpec'))
and sp2.parm_id = get_parm_id ('crm_type', get_class_id ('SubSpec'));


