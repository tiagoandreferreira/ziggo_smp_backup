--============================================================================
--    $Id: vp2resmap.sql,v 1.4 2010/11/22 14:19:47 urmilk Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================


create table REF_REGIONAL_SVC_TYP (
REGIONAL_SVC_TYP_ID  number(12) NOT NULL,
REGIONAL_SVC_TYP_DESC varchar2(300) NOT NULL,
CREATED_DTM date NOT NULL,
CREATED_BY  varchar2(30),
MODIFIED_DTM date,
MODIFIED_BY  varchar2(30))
/
alter table REF_REGIONAL_SVC_TYP add constraint REGIONAL_PROD_SVC_TYP_PK primary key (REGIONAL_SVC_TYP_ID)
/

create table REGIONAL_SVC(
REGIONAL_SVC_ID  number(12) NOT NULL,
REGIONAL_SVC_CD varchar2(300) NOT NULL,
REGIONAL_SVC_TYP_ID  number(12) NOT NULL,
REGIONAL_SVC_DESC      varchar2(300) NOT NULL,
EXPIRY_DATETIME     timestamp,
CREATED_DTM date NOT NULL,
CREATED_BY  varchar2(30),
MODIFIED_DTM date,
MODIFIED_BY  varchar2(30))
/

alter table REGIONAL_SVC add constraint REGIONAL_SVC_PK primary key (REGIONAL_SVC_ID)
/

alter table REGIONAL_SVC add constraint REGIONAL_SVC_UK UNIQUE (REGIONAL_SVC_CD)
/

alter table REGIONAL_SVC add constraint REGIONAL_SVC_FK foreign key (REGIONAL_SVC_TYP_ID) references REF_REGIONAL_SVC_TYP(REGIONAL_SVC_TYP_ID)
/

create sequence  REGIONAL_SVC_SEQ  minvalue 1 maxvalue 999999999999999999999999999 increment by 1 start with 1 cache 10 noorder  nocycle 
/

create table SVC_RSRC_MAP (
SVC_RSRC_MAP_ID        number(12) NOT NULL,
SVC_RSRC_MAP_NM        varchar2(300) NOT NULL,
SVC_RSRC_MAP_DESC      varchar2(300) NOT NULL,
SSN_SUBNTWK_ID      number(12) NOT NULL,
CREATED_DTM     date NOT NULL,
CREATED_BY      varchar2(30) ,
MODIFIED_DTM        date,
MODIFIED_BY     varchar2(30))
/


alter table SVC_RSRC_MAP add constraint SVC_RSRC_MAP_PK primary key (SVC_RSRC_MAP_ID)
/

alter table SVC_RSRC_MAP add constraint SVC_RSRC_MAP_UK unique (SVC_RSRC_MAP_NM)
/


alter table SVC_RSRC_MAP add constraint SVC_RSRC_MAP_FK foreign key (SSN_SUBNTWK_ID) references SUBNTWK(SUBNTWK_ID)
/


create sequence  SVC_RSRC_SEQ  minvalue 1 maxvalue 999999999999999999999999999 increment by 1 start with 1 cache 10 noorder  nocycle 
/



create table SVC_RSRC_MAP_SDP (
SVC_RSRC_MAP_ID    number(12) NOT NULL,
SDP_SUBNTWK_ID      number(12) NOT NULL,
CREATED_DTM     date NOT NULL,
CREATED_BY      varchar2(30),
MODIFIED_DTM        date,
MODIFIED_BY     varchar2(30))
/


alter table SVC_RSRC_MAP_SDP add constraint SVC_RSRC_MAP_SDP_FK1 foreign key (SVC_RSRC_MAP_ID) references SVC_RSRC_MAP(SVC_RSRC_MAP_ID)
/
alter table SVC_RSRC_MAP_SDP add constraint SVC_RSRC_MAP_SDP_FK2 foreign key (SDP_SUBNTWK_ID) references SUBNTWK(SUBNTWK_ID)
/
alter table SVC_RSRC_MAP_SDP add constraint SVC_RSRC_MAP_SDP_UK UNIQUE (SVC_RSRC_MAP_ID,SDP_SUBNTWK_ID)
/


create table SVC_RSRC_MAP_DTL (
SVC_RSRC_MAP_ID    number(12) NOT NULL,
SDP_SUBNTWK_ID      number(12) NOT NULL,
REGIONAL_SVC_ID    number(12) NOT NULL,
RSRC_ID                 number(12) NOT NULL,
CREATED_DTM     date NOT NULL,
CREATED_BY      varchar2(30), 
MODIFIED_DTM        date,
MODIFIED_BY     varchar2(30))
/


alter table SVC_RSRC_MAP_DTL add constraint SVC_RSRC_MAP_DTL_FK foreign key (SVC_RSRC_MAP_ID) references SVC_RSRC_MAP(SVC_RSRC_MAP_ID)
/

alter table SVC_RSRC_MAP_DTL add constraint SVC_RSRC_MAP_DTL_FK2 foreign key (SDP_SUBNTWK_ID) references SUBNTWK(SUBNTWK_ID)
/


alter table SVC_RSRC_MAP_DTL add constraint SVC_RSRC_MAP_DTL_FK3 foreign key (REGIONAL_SVC_ID ) references REGIONAL_SVC(REGIONAL_SVC_ID)
/

alter table SVC_RSRC_MAP_DTL add constraint SVC_RSRC_MAP_DTL_FK4 foreign key (RSRC_ID) references NTWK_RESOURCE(RSRC_ID)
/


alter table SVC_RSRC_MAP_DTL add constraint SVC_RSRC_MAP_DTL_UK UNIQUE (SVC_RSRC_MAP_ID , REGIONAL_SVC_ID, RSRC_ID )
/



create table REGIONAL_EVENT (
REGIONAL_EVENT_CD       varchar2(30) NOT NULL,
SDP_SUBNTWK_ID  number(12) NOT NULL,
REGIONAL_EVENT_DESC     varchar2(300),
RESOURCE_TAG        varchar2(300) NOT NULL,
SERVICE_HANDLE		varchar2(300),
EVENT_START_TIME        timestamp NOT NULL,
EVENT_END_TIME      timestamp NOT NULL,
CREATED_DTM     date NOT NULL,
CREATED_BY      varchar2(30) ,
MODIFIED_DTM        date,
MODIFIED_BY     varchar2(30))
/

alter table REGIONAL_EVENT add constraint REGIONAL_EVENT_PK primary key (REGIONAL_EVENT_CD, SDP_SUBNTWK_ID)
/

create index IDX_REGIONAL_EVENT on Regional_Event(RESOURCE_TAG)
/

alter table REGIONAL_EVENT add constraint REGIONAL_EVENT_FK foreign key (SDP_SUBNTWK_ID) references SUBNTWK(SUBNTWK_ID)
/


insert into REF_REGIONAL_SVC_TYP(REGIONAL_SVC_TYP_ID, REGIONAL_SVC_TYP_DESC, CREATED_DTM) values (1, 'SUBSCRIPTION SERVICE', SYSDATE)
/

insert into REF_REGIONAL_SVC_TYP(REGIONAL_SVC_TYP_ID, REGIONAL_SVC_TYP_DESC, CREATED_DTM) values (2, 'SUBSCRIPTION WITH EXPIRY', SYSDATE)
/

insert into REF_CONSUMABLE_RSRC_TYP (CONSUMABLE_RSRC_TYP_NM, CREATED_DTM, CREATED_BY) values ('Video Resource', sysdate, 'init')
/

insert into Parm (parm_id, parm_nm, data_typ_nm, class_id, created_dtm, created_by) (select max(parm_id)+1, 'ntwk_resource_desc', 'String', 100, sysdate, 'INIT' from parm)
/

insert into Parm (parm_id, parm_nm, data_typ_nm, class_id, created_dtm, created_by) (select max(parm_id)+1, 'resource_code', 'String', 100, sysdate, 'INIT' from parm)
/

insert into Parm (parm_id, parm_nm, data_typ_nm, class_id, created_dtm, created_by) (select max(parm_id)+1, 'has_vod', 'String', 100, sysdate, 'INIT' from parm)
/

insert into Parm (parm_id, parm_nm, data_typ_nm, class_id, created_dtm, created_by) (select max(parm_id)+1, 'resource_type', 'String', 100, sysdate, 'INIT' from parm)
/

commit
/


