#!/usr/bin/ksh
#--============================================================================
#--    $Id: ziggo_am.sh,v 1.3 2012/05/03 07:49:28 poonamm Exp $
#--		$Id: ziggo_am.sh,v 1.4 2012/09/23 07:49:28 Jalay Exp $
#--		$Id: ziggo_am.sh for Drop3,v 1.5 2012/12/23 07:49:28 Rujuta Exp $
#--		$Id: ziggo_am.sh for DTV Drop3,v 1.6 2013/07/25 07:49:28 Sarmistha Exp $
#--		$Id: ziggo_am.sh for HRZ,v 1.7 2014/12/31 05:30:28 Divya Exp $
          
#--  DESCRIPTION
#--
#--  RELATED SCRIPTS
#--
#--  REVISION HISTORY
#--  * Based on CVS log
#--============================================================================

CUR_DIR=`pwd`
LOG_DATE=`date '+%Y%m%d.%H%M%S'`
LOG_FILE=${CUR_DIR}/ziggo_am.$LOG_DATE.log

. ../../util/setEnv.sh

if [ -z $SAMP_DB_USER_ROOT ] || [ -z $SAMP_DB_PASSWORD ] || [ -z $SAMP_DB ]
then
    echo Cannot find oracle connection information login/pass/sid
    exit 1
fi

LOGIN=${SAMP_DB_USER_ROOT}am/${SAMP_DB_PASSWORD}@${SAMP_DB}

echo
echo Starting creation of Ziggo AM groups and users | tee -a $LOG_FILE


echo Executing ziggo_secure_objects.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_secure_objects.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing am_permissions.sql | tee -a $LOG_FILE
cat  am_permissions.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing ziggo_super_admin.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_super_admin.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing ziggo_tsr.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_tsr.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing ziggo_tsr_ro.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_tsr_ro.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing ziggo_stm.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_stm.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing ziggo_am_admin.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_am_admin.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing ziggo_tibco.sql | tee -a $LOG_FILE
cat  ziggo_am/ziggo_tibco.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing jsr264_am_permission.sql | tee -a $LOG_FILE
cat  ziggo_am/jsr264_am_permission.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing tibco_user_permissions.sql | tee -a $LOG_FILE
cat  ziggo_am/tibco_user_permissions.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing consolidated_UFO_AM_Permissions.sql | tee -a $LOG_FILE
cat  ziggo_am/consolidated_UFO_AM_Permissions.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing DTV_AM_Permissions.sql | tee -a $LOG_FILE
cat  ziggo_am/DTV_AM_Permissions.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing TVIX_AM_Permissions.sql | tee -a $LOG_FILE
cat  ziggo_am/TVIX_AM_Permissions.sql | sqlplus $LOGIN >> $LOG_FILE
echo Executing HRZ_Permissions.sql | tee -a $LOG_FILE
cat  ziggo_am/HRZ_Permissions.sql | sqlplus $LOGIN >> $LOG_FILE

#-------------------------------------------------------------------------
# Verify the result
#-------------------------------------------------------------------------

if egrep -s '^ERROR' $LOG_FILE || egrep -s '^Error' $LOG_FILE
then
    echo Errors found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s 'Warning' $LOG_FILE
then
    echo Warnings found. See \'$LOG_FILE\' for details
    exit 0
elif egrep -s  ORA_* $LOG_FILE
then
    echo Oracle errors found. See \'$LOG_FILE\' for details
    exit 1
else
echo  Script execution completed successfully. | tee -a $LOG_FILE
	  echo See \'$LOG_FILE\' for details
      exit 0
fi


