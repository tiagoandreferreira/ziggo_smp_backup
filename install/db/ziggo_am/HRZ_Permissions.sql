spool HRZ_Permissions.log

-- ============================================================================
--  Id: HRZ_Permissions.sql,v 1.0 2014/12/31 03:49:28 Nidhin Exp $

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.device_nickname', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.device_nickname', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.device_nickname', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.device_id2', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.device_id2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.device_id2', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.num_of_voice_lines', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.num_of_voice_lines', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.num_of_voice_lines', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.cpe_ip', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.cpe_ip', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.cpe_ip', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.mta_fqdn', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.mta_fqdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.mta_fqdn', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.mta_profile', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.mta_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.mta_profile', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_device_control.cms_fqdn', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.cms_fqdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_device_control.cms_fqdn', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.device_and_port_combination', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.device_and_port_combination', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.device_and_port_combination', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.dflt_cm_dhcp_rules', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.dflt_cm_dhcp_rules', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.dflt_cm_dhcp_rules', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.dflt_cpe_dhcp_rules', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.dflt_cpe_dhcp_rules', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.dflt_cpe_dhcp_rules', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.dflt_quality_of_svc', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.dflt_quality_of_svc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.dflt_quality_of_svc', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_data_port.display_id', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.display_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_data_port.display_id', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_cas.parental_control_entitled', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.parental_control_entitled', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.parental_control_entitled', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_cas.colour', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.colour', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.colour', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_cas.instance_id', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.instance_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.instance_id', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_stb_cas.smartcardguid', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.smartcardguid', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_stb_cas.smartcardguid', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_internet_access.num_of_ips', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.num_of_ips', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.num_of_ips', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_internet_access.bottom_up_provisioned', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.bottom_up_provisioned', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.bottom_up_provisioned', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_internet_access.rate_codes', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.rate_codes', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.rate_codes', 'y', 'csr_admin', 'edit');

exec am_add_secure_obj('samp.web.svcparm.hrz_internet_access.provision_reason_codes', 'Service Parameter');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.provision_reason_codes', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.hrz_internet_access.provision_reason_codes', 'y', 'csr_admin', 'edit');

commit;

spool off

exit
