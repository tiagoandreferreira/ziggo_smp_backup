spool TVIX_AM_Permissions.log
-- ============================================================================
--  Id: TVIX_AM_Permissions.sql,v 1.0 2014/07/25 07:49:28 Sarmistha Exp $

exec am_add_secure_obj('samp.web.svcparm.video_cpe_dummy.smartcardguid', 'Service Parameter');

-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.video_cpe_dummy.smartcardguid', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.video_cpe_dummy.smartcardguid', 'y', 'csr_admin', 'edit');

commit;

spool off

exit