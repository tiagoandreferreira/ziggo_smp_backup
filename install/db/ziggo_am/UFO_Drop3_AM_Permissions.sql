spool UFO_Drop3_AM_Permissions.log
-- ============================================================================
--  Id: ziggo_am.sh,v 1.0 2013/12/19 07:49:28 Rujuta Exp $
-- 	Secure Objects for internet service added.
-- Updated to include the secure object filter and actual filter
-- Internet_access Service

exec am_add_secure_obj('samp.web.svcparm.internet_access.filter', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.actual_filter', 'Service Parameter');

-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.internet_access.filter', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.filter', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.actual_filter', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.actual_filter', 'y', 'csr_admin', 'edit');

commit;

spool off

exit

