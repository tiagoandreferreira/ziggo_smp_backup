spool UFO_Drop4_AM_Permissions.log
-- ============================================================================
--  Id: ziggo_am.sh,v 1.0 2014/01/21 07:49:28 Jalay Exp $
-- 	Secure Objects for Voice Feature service added.
-- Updated to include the secure object filter and actual filter

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cndb', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cnd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cw', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnpps', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.bw_user_id', 'Service Parameter');



-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cndb', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cndb', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cnd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cw', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnpps', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnpps', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.bw_user_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.bw_user_id', 'y', 'csr_admin', 'edit');

commit;

spool off

exit

