spool UFO_Drop5_AM_Permissions.log
-- ============================================================================
--  Id: ziggo_am.sh,v 1.0 2014/02/19 07:49:28 Jalay Exp $


exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cndb', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cw', 'Service Parameter');


-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cndb', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cndb', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cw', 'y', 'csr_admin', 'edit');

commit;

spool off

exit

