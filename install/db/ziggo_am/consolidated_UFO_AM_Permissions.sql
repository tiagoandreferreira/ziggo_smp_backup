spool consolidated_UFO_AM_Permissions.log

-- Secure Objects for services added.
-- Updated to remove the secure object internet_status, named query
-- Dial Tone Service

exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.move_to', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.sip_password', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.iso_4217', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.period', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.period_description', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.cnd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.primary_ring', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.cos', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.call_display_number', 'Service Parameter');

exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.lec_commitment_date', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.dt_admin_status', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.cnam', 'Service Parameter');

exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.announce_language', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.announce_time_zone', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.inter_cic', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.intra_cic', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.intl_cic', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.device_id', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.tn_change_counterpart', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.switch_clli', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.ltg', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.lataname', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.primary_telephone_number', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.line_attr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.rate_area', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.xlaplan', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.line_type', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.gitn_ind', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.gitn_npa', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.gitn_nxx', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.gitn_state', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.gitn_city', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.calling_party_display', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.gitn_zip', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.lcc', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.uri_sip_id', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.sip_username', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.len', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.sip_subscriber_profile', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.sip_uri', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.technology', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.service_delivery_point', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.home_mobile_network', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.simring_accountid', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.international_dialing_prefix', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.emergency_numbers', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.country_code', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.ziggo_product', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.exceptions', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.prefix_for_domestic_call', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.national_exception', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.i_account', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.i_customer', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.activation_date', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.simring_customerid', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.i_followme_number', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.requested_activation_date', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.is_primary', 'Service Parameter');

-- eMTA Device Control

exec am_add_secure_obj('samp.web.svcparm.emta_device_control.cms_fqdn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.clli', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.rack', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.shelf', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.slot', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.port', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.access_cpe_id', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.sub_info_1', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.sub_info_2', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.sip_client_profile', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.serial_number', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.max_video_port_num', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.primary_cms_addr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.secondary_cms_addr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.action_on_delete', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.ownership', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.dev_prov_req', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_device_control.account_id_and_nickname', 'Service Parameter');

-- eMTA Voice Port

exec am_add_secure_obj('samp.web.svcparm.emta_voice_port.port_line_type', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_voice_port.device_and_port_combination', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_voice_port.device_id', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_voice_port.num_of_voice_lines', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_voice_port.device_nickname', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_voice_port.dflt_quality_of_svc', 'Service Parameter');

-- eMTA Data Port

exec am_add_secure_obj('samp.web.svcparm.emta_data_port.device_and_port_combination', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_data_port.dflt_cm_dhcp_rules', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_data_port.dflt_cpe_dhcp_rules', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.emta_data_port.dflt_quality_of_svc', 'Service Parameter');


-- Voice Service-Primary feature package
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac_per_use', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_abr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_abr_name', 'Service Parameter');

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ain', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ain_group', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aindn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aindn_group', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar_per_use', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul_dn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_bcl', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ccw', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_always','ServiceParameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_unrest', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_always','Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_ring_period', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_unrest', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_numcalls', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidcw', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnam', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot_per_use', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cpcm', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cwr', 'Service Parameter');

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cxr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ac', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ar', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cidssup', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cnb', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cot', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_drcw', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_sca', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_scf', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_scrj', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ucfv', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_utwc', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_rcc', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_voice', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_voice_digits', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_int', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_fani_digits', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_fani', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lnr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_operator_assistance_flag', 'Service Parameter');

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_pin', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_mct', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_msb', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ocaa', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf_pin', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rchd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_dndonly', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_simrg_pin', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_simrg_redirect', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sippkg', 'Service Parameter');

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcall', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcal2', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_tdn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_twc', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmwi', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_active', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_timeout', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_dn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn_dn', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vm', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vm_ring_period', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.forward_mode', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.timeout', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.default_answering_mode', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.present_caller_info', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.disable_call_waiting', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.allowed_identity', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.default_valid_identitiy', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.allowed_cli', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.display_name', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.call_barring_enabled', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.destination', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.override_display_name', 'Service Parameter');

-- internet_access

exec am_add_secure_obj('samp.web.svcparm.internet_access.aup', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.rate_codes', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.config_file_override', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.bill_ack', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.bottom_up_provisioned', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.provision_reason_codes', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.allowed_num_logins', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.email_cos', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.allowed_num_web_users', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.pweb_cos', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.downstream_bw_profile', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.upstream_bw_profile', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.num_of_ips', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.internet_status', 'Service Parameter');


-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.move_to', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.move_to', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.cos', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.cos', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.call_display_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.call_display_number', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_password', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_password', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.iso_4217', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.iso_4217', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.period', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.period', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.period_description', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.period_description', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.cnd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.primary_ring', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.primary_ring', 'y', 'csr_admin', 'edit');

-- EMTA device control
exec am_add_privilege('samp.web.svcparm.emta_device_control.ownership', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.ownership', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.dev_prov_req', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.dev_prov_req', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.account_id_and_nickname', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.account_id_and_nickname', 'y', 'csr_admin', 'edit');


exec am_add_privilege('samp.web.svcparm.emta_device_control.cms_fqdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.cms_fqdn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.clli', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.clli', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.rack', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.rack', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.shelf', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.shelf', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.slot', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.slot', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.port', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.port', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.access_cpe_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.access_cpe_id', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.sub_info_1', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.sub_info_1', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.sub_info_2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.sub_info_2', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.sip_client_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.sip_client_profile', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.serial_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.serial_number', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.max_video_port_num', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.max_video_port_num', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.primary_cms_addr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.primary_cms_addr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.secondary_cms_addr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.secondary_cms_addr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_device_control.action_on_delete', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_device_control.action_on_delete', 'y', 'csr_admin', 'edit');

-- EMTA voice port
 
exec am_add_privilege('samp.web.svcparm.emta_voice_port.port_line_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_voice_port.port_line_type', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_voice_port.device_and_port_combination', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_voice_port.device_and_port_combination', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_voice_port.device_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_voice_port.device_id', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_voice_port.num_of_voice_lines', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_voice_port.num_of_voice_lines', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_voice_port.device_nickname', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_voice_port.device_nickname', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_voice_port.dflt_quality_of_svc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_voice_port.dflt_quality_of_svc', 'y', 'csr_admin', 'edit');

-- eMTA Data Port

exec am_add_privilege('samp.web.svcparm.emta_data_port.device_and_port_combination', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_data_port.device_and_port_combination', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_data_port.dflt_cm_dhcp_rules', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_data_port.dflt_cm_dhcp_rules', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_data_port.dflt_cpe_dhcp_rules', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_data_port.dflt_cpe_dhcp_rules', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.emta_data_port.dflt_quality_of_svc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.emta_data_port.dflt_quality_of_svc', 'y', 'csr_admin', 'edit');


-- Voice Service-Primary feature package

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_abr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_abr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_abr_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_abr_name', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ac_per_use', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_acr_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ain', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ain', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ain_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ain_group', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aindn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aindn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aindn_group', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aindn_group', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ar_per_use', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_aul_dn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_bcl', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_bcl', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ccw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ccw', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_always', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_forward_to_dn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_unrest', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfbl_numcalls', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_always', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_always', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_ring_period', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_forward_to_dn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_unrest', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_unrest', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_numcalls', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfda_numcalls', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cfv_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidb_pub_anon', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cidcw', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnam', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnam', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot_per_use', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cot_per_use', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cpcm', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cpcm', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cwr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cwr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cxr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cxr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ac', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ar', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cidsdlv', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cidssup', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cidssup', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cnb', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cnb', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cot', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_cot', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_drcw', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_sca', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_scf', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_scrj', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_scrj', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_ucfv', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_deny_utwc', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnd_dndgrp', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_drcw_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_rcc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_rcc', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_voice', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_voice', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_voice_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_idp_voice_digits', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_int', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_int', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_fani_digits', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_fani_digits', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_fani', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_fani', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lnr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lnr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_domestic_long_distance', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_international_long_distance', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_pay_per_call', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_operator_assistance_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_operator_assistance_flag', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_directory_assistance_flag', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_block_toll_free_flag', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_lsr_pin', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_mct', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_mct', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_msb', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_msb', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ocaa', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_ocaa', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_racf_pin', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rchd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rchd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_rmi_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sca_dndonly', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scf_forward_to_dn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_simrg_pin', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_simrg_pin', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_simrg_redirect', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_simrg_redirect', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sippkg', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_sippkg', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcall', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcall', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcal2', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_spcal2', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_tdn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_tdn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_twc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_twc', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmwi', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmwi', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_active', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_active', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_timeout', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_wml_dn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn_dn', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vmeadn_dn', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vm', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vm', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vm_ring_period', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_vm_ring_period', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.forward_mode', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.forward_mode', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.timeout', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.timeout', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.default_answering_mode', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.default_answering_mode', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.present_caller_info', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.present_caller_info', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.disable_call_waiting', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.disable_call_waiting', 'y', 'csr_admin', 'edit');


exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.allowed_identity', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.allowed_identity', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.default_valid_identitiy', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.default_valid_identitiy', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.allowed_cli', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.allowed_cli', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.display_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.display_name', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.call_barring_enabled', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.call_barring_enabled', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.destination', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.destination', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.override_display_name', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.override_display_name', 'y', 'csr_admin', 'edit');



exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.lec_commitment_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.lec_commitment_date', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.dt_admin_status', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.dt_admin_status', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.cnam', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.cnam', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.announce_language', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.announce_language', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.announce_time_zone', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.announce_time_zone', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.inter_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.inter_cic', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.intra_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.intra_cic', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.intl_cic', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.intl_cic', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.device_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.device_id', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.tn_change_counterpart', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.tn_change_counterpart', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.switch_clli', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.switch_clli', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.ltg', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.ltg', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.lataname', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.lataname', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.primary_telephone_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.primary_telephone_number', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.line_attr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.line_attr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.rate_area', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.rate_area', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.xlaplan', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.xlaplan', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.line_type', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.line_type', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_ind', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_ind', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_npa', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_npa', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_nxx', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_nxx', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_state', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_state', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_city', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_city', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.calling_party_display', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.calling_party_display', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_zip', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.gitn_zip', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.lcc', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.lcc', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.uri_sip_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.uri_sip_id', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_username', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_username', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.len', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.len', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_subscriber_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_subscriber_profile', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_uri', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.sip_uri', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.technology', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.technology', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.service_delivery_point', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.service_delivery_point', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.home_mobile_network', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.home_mobile_network', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.simring_accountid', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.simring_accountid', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.international_dialing_prefix', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.international_dialing_prefix', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.emergency_numbers', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.emergency_numbers', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.country_code', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.country_code', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.ziggo_product', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.ziggo_product', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.exceptions', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.exceptions', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.prefix_for_domestic_call', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.prefix_for_domestic_call', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.national_exception', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.national_exception', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.i_account', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.i_account', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.i_customer', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.i_customer', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.activation_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.activation_date', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.simring_customerid', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.simring_customerid', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.i_followme_number', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.i_followme_number', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.requested_activation_date', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.requested_activation_date', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.is_primary', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.is_primary', 'y', 'csr_admin', 'edit');

-- Internet Access


exec am_add_privilege('samp.web.svcparm.internet_access.internet_status', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.internet_status', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.num_of_ips', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.num_of_ips', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.aup', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.aup', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.rate_codes', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.rate_codes', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.config_file_override', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.config_file_override', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.bill_ack', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.bill_ack', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.bottom_up_provisioned', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.bottom_up_provisioned', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.provision_reason_codes', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.provision_reason_codes', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.allowed_num_logins', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.allowed_num_logins', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.email_cos', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.email_cos', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.allowed_num_web_users', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.allowed_num_web_users', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.pweb_cos', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.pweb_cos', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.downstream_bw_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.downstream_bw_profile', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.upstream_bw_profile', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.upstream_bw_profile', 'y', 'csr_admin', 'edit');

-- Added to remove the secure object internet_status

exec am_delete_privilege('samp.web.svcparm.internet_access.internet_status', 'csr_admin', 'view');
exec am_delete_privilege('samp.web.svcparm.internet_access.internet_status', 'csr_admin', 'edit');

exec am_delete_secure_obj('samp.web.svcparm.internet_access.internet_status');

-- Secure objects for named query .

exec am_add_secure_obj('smp.query.subbrief.QueryByCmMac', 'Query Object for cm mac search.');
exec am_add_secure_obj('smp.query.subbrief.QueryByCmMac.cm_mac', 'Query Object for cm mac search.');
exec am_add_secure_obj('smp.query.subbrief.SubPNumberSearch', 'Query Object for pnumber search.');
exec am_add_secure_obj('smp.query.subbrief.SearchBySipUriQuery', 'Query Object for sip uri search.');

exec am_add_privilege('smp.query.subbrief.QueryByCmMac', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByCmMac', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.QueryByCmMac.cm_mac', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByCmMac.cm_mac', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SubPNumberSearch', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SubPNumberSearch', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.SearchBySipUriQuery', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.SearchBySipUriQuery', 'y', 'csr_admin', 'view');

-- Secure objects for add edit and new subscriber.

exec am_add_secure_obj('samp.web.menu.subscriber.add_edit', 'Subscriber add edit menu.');
exec am_add_secure_obj('samp.web.menu.subscriber.new_sub', 'Subscriber new menu.');

exec am_add_privilege('samp.web.menu.subscriber.new_sub', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.subscriber.new_sub', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.add_edit', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.menu.subscriber.add_edit', 'y', 'csr_admin', 'edit');

-- Internet_access Service

exec am_add_secure_obj('samp.web.svcparm.internet_access.filter', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.internet_access.actual_filter', 'Service Parameter');

-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.internet_access.filter', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.filter', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.internet_access.actual_filter', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.internet_access.actual_filter', 'y', 'csr_admin', 'edit');


exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cndb', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cnd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature.sw_cw', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnpps', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_dial_tone_access.bw_user_id', 'Service Parameter');



-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cndb', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cndb', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cnd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature.sw_cw', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnpps', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_dnpps', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_scr', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.bw_user_id', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_dial_tone_access.bw_user_id', 'y', 'csr_admin', 'edit');


--UFO Drop5

exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cndb', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnd', 'Service Parameter');
exec am_add_secure_obj('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cw', 'Service Parameter');


-- Secure objects for csr_admin added here. 

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cndb', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cndb', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnd', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cnd', 'y', 'csr_admin', 'edit');

exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cw', 'y', 'csr_admin', 'view');
exec am_add_privilege('samp.web.svcparm.smp_switch_feature_pkg_std.sw_cw', 'y', 'csr_admin', 'edit');

commit;

spool off

exit
