spool derby_am_permissions.log

-- ============================================================================
--  Id: derby_am_permissions.sql,v 1.0 2015/06/12 14:45:00 Rahul Exp $

exec am_add_secure_obj('smp.query.subbrief.QueryByDesiredCmMac', 'Query Object for desired Device Id search.');
exec am_add_secure_obj('smp.query.subbrief.QueryByDesiredCmMac.desired_device_id', 'Query Object for desired Device Id search.');

exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac', 'y', 'csr_admin', 'view');

exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac.desired_device_id', 'y', 'csr_admin', 'edit');
exec am_add_privilege('smp.query.subbrief.QueryByDesiredCmMac.desired_device_id', 'y', 'csr_admin', 'view');

commit;

spool off

exit
