--============================================================================
--    $Id: tibco_user_permissions.sql,v 1.1 2012/17/10 10:08:36  Exp $
--   updated as per the CONTST-1593_tibco_user
--============================================================================
-- Privileges
exec am_add_privilege('samp.web.menu.app_cfg','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.app_cfg', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.manual_task_search', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.manual_task_search', 'y', 'ziggo_tibco', 'edit');
exec am_delete_privilege('samp.web.menu.admin','ziggo_tibco','edit');
exec am_delete_privilege('samp.web.menu.admin','ziggo_tibco','view');
exec am_add_privilege('samp.web.menu.admin','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.admin', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.view_cart','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.view_cart', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.actions','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.actions', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.add_edit','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.add_edit', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.new_sub','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.new_sub', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.svc_migrate','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.svc_migrate', 'y', 'ziggo_tibco', 'edit');
commit;