--============================================================================
--    $Id: ziggo_am_admin.sql,v 1.1 2012/02/16 14:20:01 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Creation of AM Admin Group
exec am_add_grp('ziggo_am_admin', 'Ziggo AM Admin Group');
exec am_add_grp_parm('ziggo_am_admin','new_sub_service_provider_id','Ziggo');
exec am_add_grp_parm('ziggo_am_admin','service_provider_id','Ziggo');
exec am_add_grp_grp('ziggo_am_admin','am_mgr');

-- Creation of AM Admin User
exec am_create_user('amadmin', 'pwamadmin', 'AM Admin User');
exec am_add_user_parm('amadmin', 'country', 'CA');
exec am_add_user_parm('amadmin', 'language', 'en');

-- Membership of group for user
exec am_add_grp_user('amadmin', 'ziggo_am_admin');

Commit;
