--============================================================================
--    $Id: ziggo_secure_objects.sql,v 1.4 2012/04/03 17:09:35 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Addition of Secure Objects
exec am_add_secure_obj('samp.web.menu.admin', 'Access Control');
exec am_add_secure_obj('samp.web.menu.app_cfg', 'Configuration');
exec am_add_secure_obj('samp.web.menu.subscriber.add_edit', 'add/edit');
exec am_add_secure_obj('samp.web.menu.subscriber.new_sub', 'New');
exec am_add_secure_obj('samp.web.menu.subscriber.actions', 'Actions');
exec am_add_secure_obj('samp.web.menu.subscriber.svc_migrate', 'Migration');
exec am_add_secure_obj('samp.web.menu.subscriber.refresh', 'Refresh');
exec am_add_secure_obj('samp.web.menu.subscriber.view_cart', 'View Cart');
exec am_add_secure_obj('samp.web.menu.subscriber.help', 'Help');
exec am_add_secure_obj('samp.web.menu.manual_task_search', 'Manual Task Search');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.queryResponseHlrAdditional' , 'Query parms');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.queryResponseHlrPrimary' , 'Query parms');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.queryResponseMio' , 'Query parms');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.description' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.samp_sub_key' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.err_code' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.err_reason' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.start_datetime' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.has_groups' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.order_ext_key' , 'Order Search Labels');
exec am_add_secure_obj('smp.query.order.OrderQueryBySubId.resolved_indicator' , 'Order Search Labels');
exec am_add_secure_obj('samp.web.orders.capture_header_parm.latest_repairing_ordr_id' , 'Order Search Labels');
