--============================================================================
--    $Id: ziggo_stm.sql,v 1.2 2012/02/17 15:39:41 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Creation of STM Group

exec am_add_grp('ziggo_stm', 'Ziggo STM Group');
exec am_add_grp_parm('ziggo_stm','new_sub_service_provider_id','Ziggo');
exec am_add_grp_parm('ziggo_stm','service_provider_id','Ziggo');
exec am_add_grp_grp('ziggo_stm','stm.SuperUser');


-- Creation of STM User
exec am_create_user('stmuser', 'pwstmuser', 'STM User');
exec am_add_user_parm('stmuser', 'country', 'CA');
exec am_add_user_parm('stmuser', 'language', 'en');

-- Membership of group for user
exec am_add_grp_user('stmuser', 'ziggo_stm');

Commit;