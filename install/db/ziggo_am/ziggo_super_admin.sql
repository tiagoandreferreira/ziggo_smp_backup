--============================================================================
--    $Id: ziggo_super_admin.sql,v 1.5 2012/04/03 17:09:35 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Creation of Super Admin Group
exec am_add_grp('ziggo_super_admin', 'Ziggo Super Admin Group');
exec am_add_grp_parm('ziggo_super_admin','new_sub_service_provider_id','Ziggo');
exec am_add_grp_parm('ziggo_super_admin','service_provider_id','Ziggo');
exec am_add_grp_grp('ziggo_super_admin','csr_admin');
exec am_add_grp_grp('ziggo_super_admin','Administrators');
exec am_add_grp_grp('ziggo_super_admin','stm.SuperUser');

-- Creation of Super Admin User
exec am_create_user('superadmin', 'pwsuperadmin', 'Super Admin User');
exec am_add_user_parm('superadmin', 'country', 'CA');
exec am_add_user_parm('superadmin', 'language', 'en');

-- Membership of group for user
exec am_add_grp_user('superadmin', 'ziggo_super_admin');

-- Privileges
exec am_add_privilege('samp.web.menu.admin', 'n', 'ziggo_super_admin', 'view');
exec am_add_privilege('samp.web.menu.admin', 'n', 'ziggo_super_admin', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'ziggo_super_admin', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'ziggo_super_admin', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'ziggo_super_admin', 'view');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'ziggo_super_admin', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'ziggo_super_admin', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'ziggo_super_admin', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'ziggo_super_admin', 'view');
Commit;
