--============================================================================
--    $Id: ziggo_tibco.sql,v 1.7 2012/04/03 17:09:35 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Creation of Super Admin Group
exec am_add_grp('ziggo_tibco', 'Ziggo Tibco Group');
exec am_add_grp_parm('ziggo_tibco','new_sub_service_provider_id','Ziggo');
exec am_add_grp_parm('ziggo_tibco','service_provider_id','Ziggo');
exec am_add_grp_grp('ziggo_tibco','csr_admin');
exec am_add_grp_grp('ziggo_tibco','Administrators');
--exec am_add_grp_grp('ziggo_tibco','stm.SuperUser');

-- Creation of Super Admin User
exec am_create_user('tibcouser', 'pwtibcouser', 'Tibco User');
exec am_add_user_parm('tibcouser', 'country', 'CA');
exec am_add_user_parm('tibcouser', 'language', 'en');

-- Membership of group for user
exec am_add_grp_user('tibcouser', 'ziggo_tibco');

-- Privileges
exec am_add_privilege('samp.web.menu.admin', 'n', 'ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.admin', 'n', 'ziggo_tibco', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'ziggo_tibco', 'view');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'ziggo_tibco', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'ziggo_tibco', 'view');

Commit;

--Tibco permissions and changes added in tibco sql file 29 october 2012(new)
exec am_add_privilege('samp.web.menu.app_cfg','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.app_cfg', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.manual_task_search', 'y', 'ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.manual_task_search', 'y', 'ziggo_tibco', 'edit');
--exec am_delete_privilege('samp.web.menu.admin','ziggo_tibco','edit');
--exec am_delete_privilege('samp.web.menu.admin','ziggo_tibco','view');
exec am_add_privilege('samp.web.menu.admin','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.admin', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.view_cart','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.view_cart', 'y', 'ziggo_tibco', 'edit');
exec am_add_privilege('samp.web.menu.subscriber.actions','y','ziggo_tibco', 'view');
exec am_add_privilege('samp.web.menu.subscriber.actions', 'y', 'ziggo_tibco', 'edit');
commit;
 
