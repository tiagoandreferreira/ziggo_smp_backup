--============================================================================
--    $Id: ziggo_tsr_ro.sql,v 1.6 2012/04/03 17:09:35 poonamm Exp $
--
--  REVISION HISTORY
--  * Based on CVS log
--============================================================================
-- Creation of TSR

exec am_add_grp('ziggo_tsr_ro', 'Ziggo TSR Read Only Group');
exec am_add_grp_parm('ziggo_tsr_ro','new_sub_service_provider_id','Ziggo');
exec am_add_grp_parm('ziggo_tsr_ro','service_provider_id','Ziggo');
exec am_add_grp_grp('ziggo_tsr_ro','csr_admin');
exec am_add_grp_grp('ziggo_tsr_ro','Administrators');

-- Creation of TSR Read Only User
exec am_create_user('tsrrouser', 'pwtsrrouser', 'TSR Read Only User');
exec am_add_user_parm('tsrrouser', 'country', 'CA');
exec am_add_user_parm('tsrrouser', 'language', 'en');

-- Membership of group for user
exec am_add_grp_user('tsrrouser', 'ziggo_tsr_ro');

-- Privileges
exec am_add_privilege('samp.web.menu.app_cfg', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.app_cfg', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.add_edit', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.add_edit', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.new_sub', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.new_sub', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.actions', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.actions', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.svc_migrate', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.svc_migrate', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.refresh', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.refresh', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.view_cart', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.view_cart', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.subscriber.help', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.subscriber.help', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.manual_task_search', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.manual_task_search', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.menu.admin', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.menu.admin', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'ziggo_tsr_ro', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'ziggo_tsr_ro', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrAdditional', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseHlrPrimary', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('samp.web.orders.capture_header_parm.queryResponseMio', 'y', 'ziggo_tsr_ro', 'view');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.description', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.samp_sub_key', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_code', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.err_reason', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.start_datetime', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.has_groups', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.order_ext_key', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'ziggo_tsr_ro', 'view');
exec am_add_privilege('smp.query.order.OrderQueryBySubId.resolved_indicator', 'y', 'ziggo_tsr_ro', 'edit');

exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'ziggo_tsr_ro', 'edit');
exec am_add_privilege('samp.web.orders.capture_header_parm.latest_repairing_ordr_id', 'y', 'ziggo_tsr_ro', 'view');
Commit;