<!--
//=============================================================================
//
//  FILE INFO
//    $Id: env_user.properties,v 1.8 2012/02/02 05:15:19 poonamm Exp $
//    $Id: env_user.properties,v 1.9 2012/03/07 10:00:00 sdijk ziggo $
//    $Id: env_user.properties,v 1.10 2012/09/01 10:00:00 jsteur ziggo $
//    SAMP 1.0
//    Copyright (c) 2001 Sigma Systems Group (Canada) Inc.
//    All rights reserved.
//
//  DESCRIPTION
//
//    This is the properties file to configure the weblogic installation
//    of SAMP
//
//  REVISION HISTORY
//  *  Based on CVS log
//=============================================================================
-->

//-------------------------------------------------------------------
// Environment installation
//-------------------------------------------------------------------
//  These are usually also exported in the .profile (autoexec.bat)
//
//  If using ant_build.cmd/sh to run  the installation,  it will overwrite
// the ones here with the ones exported in the environment variables
// at the time of running the script.
//
//  Explanation:
//   java.home
//      - should point to the location of the JDK
//   j2ee.home
//      - should point to the location of the J2EE
//   ant.home
//      - should point to the location of ANT
//   extjars.dir
//      - is the location of the extjars directory
//-------------------------------------------------------------------

  java.home=/opt/app/fulfillment/jdk/jdk1.6.0_29
  ant.home=/opt/app/bea/domains/DIST/install/util/ant
  extjars.dir=/opt/app/bea/domains/DIST/extjars

///-----------------------------------------------------------------
// Performance Tuning Options
//------------------------------------------------------------------
//  These options are used to configure the JVM for optimum
// performance. The settings below are for development environments,
// and shouldn't be used for demo or production systems.
//
//  More resonable numbers for demo systems are the following
//     perf.jvm=-server
//     perf.memory.initial=512m
//     perf.memory.max=512m
//     perf.memory.permsize=128m
//     perf.threads=60
//     perf.misc.options=
//     perf.log.refresh_interval=60
//
//  Explaination:
//    perf.jvm
//      - tells java which VM to use (-server,-client)
//    perf.memory.initial
//      - is the initial amout of memory for the JVM Heap
//    perf.memory.max
//      - is the max memeory to be used for the JVM Heap
//    perf.memory.permsize
//      - the amount of memeory in the Heap for perminate objects.
//    perf.threads
//      - is the number of threads. Don't set this to high as it can
//        actually degrade performance.
//    perf.misc.options
//      - is used for any additional options you want to pass to JVM
//    perf.log.refresh_interval
//------------------------------------------------------------------
perf.jvm=-d64 -server
perf.memory.initial=1024m
perf.memory.max=1560m
perf.memory.permsize=512m
perf.memory.nativestacksize=4m
perf.memory.javastacksize=4m
perf.threads=15
perf.misc.options=
perf.log.refresh_interval=60

///-----------------------------------------------------------------
// Debug Options
//------------------------------------------------------------------
debug.db.profiling=false

//-----------------------------------------------------------------
// AM User lockout settings
// "am.local.resetDuration" is in minutes
//------------------------------------------------------------------
am.local.lockoutEnabled=true
am.local.lockoutThreshold=3
am.local.resetDuration=5

//-------------------------------------------------------------------
// weblogic installation
//-------------------------------------------------------------------
//  These variables are used to tell SMP where Weblogic is installed.
//
//  Explaination:
//    weblogic.home
//       - Location of the Weblogic Installation
//    domain.home
//       - Directory which contains the domain directory
//    weblogic.domain
//       - The name of the SMP Domain
//    smp.server
//       - A logical name for the SMP Server
//    smp.host
//       - Host where SMP is running
//    smp.port
//       - HTTP/T3 Port for the SMP Server
//    smp.ssl.port
//       - SSL Port for the SMP Server
//    smp.password
//       - System password
//-------------------------------------------------------------------
weblogic.home=/opt/app/bea/wlserver_10.3
domain.home=/opt/app/bea/domains
weblogic.domain=smpd_voip1
// Make sure you have a trailing "/" at the end of this variable

smp.cfg.path=cfg/com/sigma/samp/imp/ziggo/xmlcfg/
smp.server=admin_svr
smp.host=emn-fulap-ts1-01.tst.it.zesko.nl
smp.port=28002
smp.ssl.port=13014
smp.password=demoadmin
smp.user=system

// for the protocol maximum message size
protocol.max_message_size=20000000

//-------------------------------------------------------------------
// Cluster Options
//-------------------------------------------------------------------
//  If you are not using cluster, you don't need to change the
// variables below. You can ignore this section
//
// NOTE: For internal Sigma testing _DON'T_ change the multicast
//       address!
//-------------------------------------------------------------------
nodemgr.port=5555

smp.cluster.name=smpcluster
smp.cluster.url=smpc1:8001,smpc2:8001
smp.cluster.multicast.port=2001
smp.cluster.multicast.address=237.0.8.1
smp.jms.prefix=smp

// Node1 Server
smp.node1.server=node1_svr
smp.node1.host=smpc1
smp.node1.threads=60
smp.node1.mem=-server -Xms512m -Xmx1024m -XX:MaxPermSize=512m -XX:PermSize=512m  -Xss4m -Xoss4m
smp.node1.port=8001
smp.node1.ssl.port=9001

// Node2 Server
smp.node2.server=node2_svr
smp.node2.host=smpc2
smp.node2.threads=60
smp.node2.mem=-server -Xms512m -Xmx1024m -XX:MaxPermSize=512m -XX:PermSize=512m  -Xss4m -Xoss4m
smp.node2.port=8001
smp.node2.ssl.port=9001

// Node3 Server
smp.node3.server=node3_svr
smp.node3.host=smpc3
smp.node3.threads=60
smp.node3.mem=-server -Xms512m -Xmx1024m -XX:MaxPermSize=512m -XX:PermSize=512m  -Xss4m -Xoss4m
smp.node3.port=8001
smp.node3.ssl.port=9001

//-------------------------------------------------------------------
// Extra Setting for Perf Cluster
//-------------------------------------------------------------------
//  If you are not using performance cluster, you don't need to change the
// variables below. You can ignore this section
//-------------------------------------------------------------------

workflow.cluster.name=wfcluster
workflow.cluster.url=smpc1:8001,smpc2:8001
workflow.cluster.multicast.port=3001
workflow.cluster.multicast.address=237.0.8.2
workflow.jms.prefix=wf

// Engine properties
workflow.JVTWFSession.initBeansInPool=3
workflow.JVTWFSession.maxBeansInPool=15
workflow.SOReqMDB.initBeansInPool=1
workflow.SOReqMDB.maxBeansInPool=3
workflow.replyTaskMDB.initBeansInPool=3
workflow.replyTaskMDB.maxBeansInPool=15
workflow.persistentMgr.batch_update_size=10
workflow.persistentMgr.sequenceCache=1000
workflow.instanceCache.capacity=100

// Node1 Server
workflow.node1.server=wf_svr1
workflow.node1.host=smpc1
workflow.node1.threads=60
workflow.node1.mem=-server -Xms512m -Xmx1024m -XX:MaxPermSize=512m -XX:PermSize=512m  -Xss4m -Xoss4m
workflow.node1.port=8002
workflow.node1.ssl.port=9002

// Node2 Server
workflow.node2.server=wf_svr2
workflow.node2.host=smpc2
workflow.node2.threads=60
workflow.node2.mem=-server -Xms512m -Xmx1024m -XX:MaxPermSize=512m -XX:PermSize=512m  -Xss4m -Xoss4m
workflow.node2.port=8002
workflow.node2.ssl.port=9002

// Node3 Server
workflow.node3.server=wf_svr3
workflow.node3.host=smpc3
workflow.node3.threads=60
workflow.node3.mem=-server -Xms512m -Xmx1024m -XX:MaxPermSize=512m -XX:PermSize=512m  -Xss4m -Xoss4m
workflow.node3.port=8002
workflow.node3.ssl.port=9002

//-------------------------------------------------------------------
// oracle database installation
//-------------------------------------------------------------------
ora.home=/opt/oracle/client/11.2.0/
ora.tns.admin=/var/opt/oracle
ora.env=smp
ora.password=smp3futcm5
ora.sid=FUTOTP1T1
ora.thin.url=emn-odadb-ts1-01-vip:1522:FUTOTP1T1
nls.lang=AMERICAN_AMERICA.UTF8
ora.cbhome=$HOME/samp


//-------------------------------------------------------------------
// jasperserver oracle database installation
//-------------------------------------------------------------------
// js.ora.env=ziggodev4
// js.ora.password=ziggodev4
// js.ora.sid=SIDEV11G
// js.ora.thin.url=oracle11g.sigma.com:1521:SIDEV11G
// js.nls.lang=AMERICAN_AMERICA.UTF8


//-------------------------------------------------------------------
// oracle transactional connections
//-------------------------------------------------------------------
ora_tx_driver.class=oracle.jdbc.xa.client.OracleXADataSource
//ora_tx_driver.url=jdbc:oracle:thin:@emn-odadb-ts1-01-//vip.tst.it.zesko.nl:1522:FUTOTP1T1
ora_tx_driver.url=jdbc:oracle:thin:@emn-odadb-ts1-01-vip:1522:FUTOTP1T1

ora_tx_driver.samp.properties=user=smpcm;password=smp3futcm5;dataSourceName=sampXAConnPool


ora_tx_driver.samp.initial=1
ora_tx_driver.samp.maxcapacity=15

// JEA TX poolsize
ora_tx_driver.jea_tx.initial=1
ora_tx_driver.jea_tx.maxcapacity=15

// JWF TX poolsize
ora_tx_driver.jwf_tx.initial=1
ora_tx_driver.jwf_tx.maxcapacity=15

//-------------------------------------------------------------------
// oracle non-transactional connections
//-------------------------------------------------------------------
ora_nontx_driver.class=oracle.jdbc.driver.OracleDriver
//ora_nontx_driver.url=jdbc:oracle:thin:@emn-odadb-ts1-01-//vip.tst.it.zesko.nl:1522:FUTOTP1T1

ora_nontx_driver.url=jdbc:oracle:thin:@emn-odadb-ts1-01-vip:1522:FUTOTP1T1

ora_nontx_driver.samp.properties=user=smpcm;password=smp3futcm5;dataSourceName=sampNonXAConnPool


ora_nontx_driver.samp.initial=1
ora_nontx_driver.samp.maxcapacity=15

ora_nontx_driver.jmsserver.initial=1
ora_nontx_driver.jmsserver.maxcapacity=5

// ora_nontx_driver.stm.reprov.properties=user=smpcm;password=smp3futcm5;dataSourceName=sampSTMReprovConnPool
ora_nontx_driver.stm.reprov.properties=user=smpcm;password=smp3futcm5;dataSourceName=sampSTMReprovConnPool

ora_nontx_driver.stm.reprov.initial=1
ora_nontx_driver.stm.reprov.maxcapacity=20
ora_nontx_driver.stm.reprov.debug.profiling=false

// ora_nontx_driver.am.properties=user=smpam;password=smp2futam7;dataSourceName=sampAMNonTxConnPool
ora_nontx_driver.am.properties=user=smpam;password=smp3futcm5;dataSourceName=sampAMNonTxConnPool

ora_nontx_driver.am.initial=1
ora_nontx_driver.am.maxcapacity=10

// ora_nontx_driver.cm.properties=user=smpcm;password=smp3futcm5;dataSourceName=sampCMNonTxConnPool
ora_nontx_driver.cm.properties=user=smpcm;password=smp3futcm5;dataSourceName=sampCMNonTxConnPool

ora_nontx_driver.cm.initial=1
ora_nontx_driver.cm.maxcapacity=15
ora_nontx_driver.cm.debug.profiling=false

// JEA Non-TX poolsize
ora_nontx_driver.jea_non_tx.initial=1
ora_nontx_driver.jea_non_tx.maxcapacity=15

// JWF Non-TX poolsize
ora_nontx_driver.jwf_non_tx.initial=1
ora_nontx_driver.jwf_non_tx.maxcapacity=15

//-----------------------------------------------------------------
// Oracle Thin Driver
//-----------------------------------------------------------------
ora_thin_driver.class=oracle.jdbc.driver.OracleDriver

// ora_thin_driver.stmSync.properties=user=@ora.env@cm;password=@ora.env@
// ora_thin_driver.stmSync.url=jdbc:oracle:thin:@@ora.thin.url@
ora_thin_driver.stmSync.properties=user=smpcm;password=smp3futcm5
ora_thin_driver.stmSync.url=jdbc:oracle:thin:@emn-odadb-ts1-01-vip:1522:FUTOTP1T1
ora_thin_driver.stmSync.initial=1
ora_thin_driver.stmSync.maxcapacity=1

//-----------------------------------------
// SMTP server for licensing
//-----------------------------------------
smtp.server=catomail1

install.dir=@domain.home@/@weblogic.domain@

//SmpMdBridge1.port=99411
//SmpMdBridge2.port=12039
//md.sbjeaipunityv2759.host=nebula
//md.sbjeaipunityv2759.port=12067
//md.sbjeaipunityv2759.msgtype=2240



//-----------------------------------------------------------------------------------
// Ericssonemamio installation JEA settings
//----------------------------------------------------------------------------------


// Mode to be set to false
sbjeaericssonemamio.soap_simulation_mode=false

// IP and Port details of Ericson simulator as per our env followed by cai3g12/SessionControl

sbjeaericssonemamio.soap_endpoint_uri=http://172.20.177.14:8998/cai3g12/Provisioning

//usename and pwd will to be set as set in the response xml file

sbjeaericssonemamio.login_nm=sigma
sbjeaericssonemamio.password=sigma123

//sbjeaericssonemamio.timeout=80
sbjeaericssonemamio.cai3_namespace=http://schemas.ericsson.com/cai3g1.2/

// IP and Port details of Ericson simulator as per our env followed by cai3g12/SessionControl

sbjeaericssonemamio.session_control_uri=http://172.20.177.14:8998/cai3g12/SessionControl
sbjeaericssonemamio.soap_message_factory=default.soap11
sbjeaericssonemamio.soap_connection_factory=default
sbjeaericssonemamio.call_timeout=10
sbjeaericssonemamio.redelivery_limit=3
sbjeaericssonemamio.redelivery_delay=60000
//conn idle timeout in seconds. idle conns will be removed from pool. -1 means no idle timeout.
sbjeaericssonemamio.idle_timeout=-1

//----------------------------------------------------------------------------------
// Ericssonemahlr installation JEA settings
//----------------------------------------------------------------------------------
// Replicate the same settings as above 


sbjeaericssonemahlr.soap_simulation_mode=false
sbjeaericssonemahlr.soap_endpoint_uri=http://172.20.177.14:8998/cai3g12/Provisioning
sbjeaericssonemahlr.login_nm=sigma
sbjeaericssonemahlr.password=sigma123
//sbjeaericssonemahlr.timeout=80
sbjeaericssonemahlr.cai3_namespace=http://schemas.ericsson.com/cai3g1.2/
sbjeaericssonemahlr.session_control_uri=http://172.20.177.14:8998/cai3g12/SessionControl
sbjeaericssonemahlr.soap_message_factory=default.soap11
sbjeaericssonemahlr.soap_connection_factory=default
sbjeaericssonemahlr.call_timeout=10
sbjeaericssonemahlr.redelivery_limit=3
sbjeaericssonemahlr.redelivery_delay=60000
//conn idle timeout in seconds. idle conns will be removed from pool. -1 means no idle timeout.
sbjeaericssonemahlr.idle_timeout=-1

//----------------------------------------------------------------------------------
// Netmanager installation JEA settings
// Note : in case of Net Manager we have timeout handler at tmplt level, for which the timeout
//  value is 240 secs. ((call_timeout*2)+(call_timeout*2)*3+redelivery_delay(in sec)*3) should 
//  never go above 240 secs. else the task will go to timeout handler rather than Failure handler
//----------------------------------------------------------------------------------

sbjeansnnetmanager.soap_simulation_mode=true
sbjeansnnetmanager.soap_endpoint_uri=http://pyramid:8002/StubProject/StubServlet
sbjeansnnetmanager.login_nm=sigma
sbjeansnnetmanager.password=sigma123
sbjeansnnetmanager.user_grp=sigmagroup
sbjeansnnetmanager.terminal=sigmaterminal
sbjeansnnetmanager.xsd_url=http://tempuri.org/ns2.xsd
//sbjeansnnetmanager.soap_message_factory=com.sun.xml.internal.messaging.saaj.soap.ver1_2.SOAPMessageFactory1_2Impl
sbjeansnnetmanager.soap_message_factory=default.soap12
sbjeansnnetmanager.soap_connection_factory=default
//sbjeansnnetmanager.timeout=80
sbjeansnnetmanager.soap_url=http://www.w3.org/2003/05/soap-envelope

// to be used for soap 1.1
//sbjeansnnetmanager.soap_url=http://schemas.xmlsoap.org/soap/envelope/  
sbjeansnnetmanager.call_timeout=10
sbjeansnnetmanager.redelivery_limit=3
sbjeansnnetmanager.redelivery_delay=40000
//conn idle timeout in seconds. idle conns will be removed from pool. -1 means no idle timeout.
sbjeansnnetmanager.idle_timeout=-1
sbjeansnnetmanager.header_url=http://www.w3.org/2005/08/addressing
sbjeansnnetmanager.listener_url=http://172.22.229.23:9366/NetmanagerReceiver

//settings for portaone
//settings for portaone
sbjeaportaone.soap_simulation_mode=false
sbjeaportaone.soap_endpoint_uri=https://stag-pbs.sipnl.net:8444/soap/
sbjeaportaone.login_nm=smpadmin
sbjeaportaone.password=Z16605810lampe5W
sbjeaportaone.soap_message_factory=default.soap11
sbjeaportaone.soap_connection_factory=default
sbjeaportaone.call_timeout=10
sbjeaportaone.redelivery_limit=3
sbjeaportaone.redelivery_delay=40000
sbjeaportaone.idle_timeout=-1
sbjeaportaone.session_service_uri=https://stag-pbs.sipnl.net:8444/soap/
sbjeaportaone.account_service_uri=https://stag-pbs.sipnl.net:8444/soap/


// Seimens Jea Properties
sbjeahiq8000soap.soap_endpoint_uri=http://172.22.227.163:8767/
sbjeahiq8000soap.soap_simulation_mode=true
sbjeahiq8000soap.http_cookies_support=true
//http_basic_authentication=sigma:sigma
sbjeahiq8000soap.call_timeout=30
sbjeahiq8000soap.simulate=none
sbjeahiq8000soap.redelivery_limit=3
sbjeahiq8000soap.redelivery_delay=40000
sbjeahiq8000soap.soap_message_factory=default.soap11
sbjeahiq8000soap.soap_connection_factory=default

//sbjeahiqinstance Properties
sbjeahiqinstance.soap_endpoint_uri=http://172.22.227.163:8767/
sbjeahiqinstance.soap_simulation_mode=true
sbjeahiqinstance.call_timeout=30
sbjeahiqinstance.simulate=none
sbjeahiqinstance.redelivery_limit=3
sbjeahiqinstance.redelivery_delay=40000
sbjeahiqinstance.idle_timeout=-1
sbjeahiqinstance.soap_message_factory=com.sun.xml.messaging.saaj.soap.ver1_1.SOAPMessageFactory1_1Impl
sbjeahiqinstance.soap_connection_factory=default
sbjeahiqinstance.http_basic_authentication=agent:agent
